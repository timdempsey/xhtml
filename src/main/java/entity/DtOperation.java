/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtOperation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtOperation.findAll", query = "SELECT d FROM DtOperation d")
    , @NamedQuery(name = "DtOperation.findByOperationId", query = "SELECT d FROM DtOperation d WHERE d.operationId = :operationId")
    , @NamedQuery(name = "DtOperation.findByOperationName", query = "SELECT d FROM DtOperation d WHERE d.operationName = :operationName")})
public class DtOperation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "operationId")
    private Integer operationId;
    @Size(max = 50)
    @Column(name = "operationName")
    private String operationName;

    public DtOperation() {
    }

    public DtOperation(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (operationId != null ? operationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtOperation)) {
            return false;
        }
        DtOperation other = (DtOperation) object;
        if ((this.operationId == null && other.operationId != null) || (this.operationId != null && !this.operationId.equals(other.operationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtOperation[ operationId=" + operationId + " ]";
    }
    
}
