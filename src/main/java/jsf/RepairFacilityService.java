/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Dealer;
import entity.DealerGroup;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "repairFacilityService")
@ApplicationScoped
public class RepairFacilityService {
    private static final Logger LOGGER = LogManager.getLogger(RepairFacilityService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of RepairFacilityService
     */
    public RepairFacilityService() {
        LOGGER.info("in empty contructor");
    }
    
    List<RepairFacilities> rfList;
    
    private Dealer dealer;
    private DealerGroup dealerGroup;
    
    private RepairFacilities selectedRF;
    private RepairFacilities rfs;
    
    

    public RepairFacilities getRfs() {
        return rfs;
    }

    public void setRfs(RepairFacilities rfs) {
        this.rfs = rfs;
    }
    
    

    public RepairFacilities getSelectedRF() {
        return selectedRF;
    }

    public void setSelectedRF(RepairFacilities selectedRF) {
        this.selectedRF = selectedRF;
    }
    
    

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public DealerGroup getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(DealerGroup dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    
    
    @PostConstruct
    public void init() {
        LOGGER.info("in init");
        dealerGroup = null;
        dealer = null;
        rfs = new RepairFacilities();
        selectedRF = new RepairFacilities();
    }

    public List<RepairFacilities> getRfList() {
        return rfList;
    }

    public void setRfList(List<RepairFacilities> rfList) {
        this.rfList = rfList;
    }
    
    public void reset() {
        //dealer = null;
        //dealerGroup = null;
        selectedRF = null;
        rfs.reset();
    }
    
    /*
    * methods
    */
    
    
    public void generateRFListByDG(DealerGroup dg) {
        LOGGER.info("in generateRFListByDG, gruopId=" + dg.getDealerGroupId());
        rfList = claimMisc.getRepairFacilitiesByDGId(dg.getDealerGroupId());
    }
    
    public void generateRFListByDealer(Dealer dealer) {
        LOGGER.info("in generateRFListByDealer, dealerId=" + dealer.getDealerId());
        rfList = claimMisc.getRepairFacilitiesByDealerId(dealer.getDealerId());
    }
    
    public void addRF() {
        LOGGER.info("in addRF");
        reset();
        selectedRF = null;
        
        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);
        
        //RequestContext.getCurrentInstance().openDialog("AddRepairFacility");options, null
        RequestContext.getCurrentInstance().openDialog("AddRepairFacility", options, null);
    }
    
    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        if (dealer != null) {
            LOGGER.info("RF for dealer=" + dealer.getDealerId());
            rfList = claimMisc.getRepairFacilitiesByDealerId(dealer.getDealerId());
        } else if( dealerGroup != null ) {
            LOGGER.info("RF for DG=" + dealerGroup.getDealerGroupId());
             rfList = claimMisc.getRepairFacilitiesByDGId(dealerGroup.getDealerGroupId());
        }
    }
    
    
    public void cancel() {
        LOGGER.info("in cancel");
        reset();
        RequestContext.getCurrentInstance().closeDialog("AddRepairFacility");
    }
    
    public void saveRF() {
        LOGGER.info("in saveRF, selectedRF=" + selectedRF);
        
        if( dealer != null ) {
            LOGGER.info("dealerId=" + dealer.getDealerId());
            rfs.setDealerId(dealer.getDealerId());
        } 
        
        if (selectedRF != null) {  // update
            LOGGER.info("updating RF=" + selectedRF.getName());
            claimMisc.saveRepairFacility(rfs, selectedRF.getRepairFacilityId());
        } else {
            LOGGER.info("save new RF");
            claimMisc.saveRepairFacility(rfs, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddRepairFacility");
    }
    
    public void onRowSelect(SelectEvent event) {
        selectedRF = (RepairFacilities) event.getObject();
        // new
        /*
        LOGGER.info("selectedDealer=" + selectedRF.getName());
        paddr = claimMisc.printAddress(selectedDealer.getDealerId(), "P");
        baddr = claimMisc.printAddress(selectedDealer.getDealerId(), "B");
        dealer = claimMisc.getDealerById(selectedDealer.getDealerId());
        rfs.generateRFListByDealer(dealer);
        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedDealer.getDealerId());
        if (ak.getActive()) {
            dlStatus = "DeActivate";
        } else {
            dlStatus = "Activate";
        }
        */
        
    }
    
    public void verifyRFName() {
        LOGGER.info("in verifyRFName, RFName=" + rfs.getName());
        if( claimMisc.getAccountKeeperByNameType(rfs.getName(), 7) != null ) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "the RepairFacility name already exist: ", rfs.getName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    
    
}
