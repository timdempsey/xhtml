/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Markup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Markup.findAll", query = "SELECT m FROM Markup m")
    , @NamedQuery(name = "Markup.findByMarkupId", query = "SELECT m FROM Markup m WHERE m.markupId = :markupId")
    , @NamedQuery(name = "Markup.findByDescription", query = "SELECT m FROM Markup m WHERE m.description = :description")
    , @NamedQuery(name = "Markup.findByUpdateUserName", query = "SELECT m FROM Markup m WHERE m.updateUserName = :updateUserName")
    , @NamedQuery(name = "Markup.findByUpdateLast", query = "SELECT m FROM Markup m WHERE m.updateLast = :updateLast")})
public class Markup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "markupId")
    private Integer markupId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "markupIdFk")
    private Collection<MarkupDetail> markupDetailCollection;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne
    private Product productIdFk;

    public Markup() {
    }

    public Markup(Integer markupId) {
        this.markupId = markupId;
    }

    public Integer getMarkupId() {
        return markupId;
    }

    public void setMarkupId(Integer markupId) {
        this.markupId = markupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<MarkupDetail> getMarkupDetailCollection() {
        return markupDetailCollection;
    }

    public void setMarkupDetailCollection(Collection<MarkupDetail> markupDetailCollection) {
        this.markupDetailCollection = markupDetailCollection;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (markupId != null ? markupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Markup)) {
            return false;
        }
        Markup other = (Markup) object;
        if ((this.markupId == null && other.markupId != null) || (this.markupId != null && !this.markupId.equals(other.markupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Markup[ markupId=" + markupId + " ]";
    }
    
}
