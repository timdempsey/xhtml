/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "InspectionCompany")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InspectionCompany.findAll", query = "SELECT i FROM InspectionCompany i")
    , @NamedQuery(name = "InspectionCompany.findByInspectionCompanyId", query = "SELECT i FROM InspectionCompany i WHERE i.inspectionCompanyId = :inspectionCompanyId")
    , @NamedQuery(name = "InspectionCompany.findByDefaultInspectionRate", query = "SELECT i FROM InspectionCompany i WHERE i.defaultInspectionRate = :defaultInspectionRate")
    , @NamedQuery(name = "InspectionCompany.findByRateFixedInd", query = "SELECT i FROM InspectionCompany i WHERE i.rateFixedInd = :rateFixedInd")
    , @NamedQuery(name = "InspectionCompany.findByInspectionCompanyTypeInd", query = "SELECT i FROM InspectionCompany i WHERE i.inspectionCompanyTypeInd = :inspectionCompanyTypeInd")
    , @NamedQuery(name = "InspectionCompany.findByUpdateUserName", query = "SELECT i FROM InspectionCompany i WHERE i.updateUserName = :updateUserName")
    , @NamedQuery(name = "InspectionCompany.findByUpdateLast", query = "SELECT i FROM InspectionCompany i WHERE i.updateLast = :updateLast")})
public class InspectionCompany implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "inspectionCompanyId")
    private Integer inspectionCompanyId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "defaultInspectionRate")
    private BigDecimal defaultInspectionRate;
    @Column(name = "rateFixedInd")
    private Boolean rateFixedInd;
    @Column(name = "inspectionCompanyTypeInd")
    private Integer inspectionCompanyTypeInd;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "inspectionCompanyId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @JoinColumn(name = "formIdFk", referencedColumnName = "formId")
    @ManyToOne
    private CfForm formIdFk;

    public InspectionCompany() {
    }

    public InspectionCompany(Integer inspectionCompanyId) {
        this.inspectionCompanyId = inspectionCompanyId;
    }

    public InspectionCompany(Integer inspectionCompanyId, BigDecimal defaultInspectionRate) {
        this.inspectionCompanyId = inspectionCompanyId;
        this.defaultInspectionRate = defaultInspectionRate;
    }

    public Integer getInspectionCompanyId() {
        return inspectionCompanyId;
    }

    public void setInspectionCompanyId(Integer inspectionCompanyId) {
        this.inspectionCompanyId = inspectionCompanyId;
    }

    public BigDecimal getDefaultInspectionRate() {
        return defaultInspectionRate;
    }

    public void setDefaultInspectionRate(BigDecimal defaultInspectionRate) {
        this.defaultInspectionRate = defaultInspectionRate;
    }

    public Boolean getRateFixedInd() {
        return rateFixedInd;
    }

    public void setRateFixedInd(Boolean rateFixedInd) {
        this.rateFixedInd = rateFixedInd;
    }

    public Integer getInspectionCompanyTypeInd() {
        return inspectionCompanyTypeInd;
    }

    public void setInspectionCompanyTypeInd(Integer inspectionCompanyTypeInd) {
        this.inspectionCompanyTypeInd = inspectionCompanyTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    public CfForm getFormIdFk() {
        return formIdFk;
    }

    public void setFormIdFk(CfForm formIdFk) {
        this.formIdFk = formIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inspectionCompanyId != null ? inspectionCompanyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InspectionCompany)) {
            return false;
        }
        InspectionCompany other = (InspectionCompany) object;
        if ((this.inspectionCompanyId == null && other.inspectionCompanyId != null) || (this.inspectionCompanyId != null && !this.inspectionCompanyId.equals(other.inspectionCompanyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.InspectionCompany[ inspectionCompanyId=" + inspectionCompanyId + " ]";
    }
    
}
