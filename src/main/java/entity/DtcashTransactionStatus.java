/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtcashTransactionStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtcashTransactionStatus.findAll", query = "SELECT d FROM DtcashTransactionStatus d")
    , @NamedQuery(name = "DtcashTransactionStatus.findByCashTransactionStatusId", query = "SELECT d FROM DtcashTransactionStatus d WHERE d.cashTransactionStatusId = :cashTransactionStatusId")
    , @NamedQuery(name = "DtcashTransactionStatus.findByDescription", query = "SELECT d FROM DtcashTransactionStatus d WHERE d.description = :description")})
public class DtcashTransactionStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cashTransactionStatusId")
    private Integer cashTransactionStatusId;
    @Size(max = 60)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cashTransactionStatusInd")
    private Collection<CashTransaction> cashTransactionCollection;

    public DtcashTransactionStatus() {
    }

    public DtcashTransactionStatus(Integer cashTransactionStatusId) {
        this.cashTransactionStatusId = cashTransactionStatusId;
    }

    public Integer getCashTransactionStatusId() {
        return cashTransactionStatusId;
    }

    public void setCashTransactionStatusId(Integer cashTransactionStatusId) {
        this.cashTransactionStatusId = cashTransactionStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CashTransaction> getCashTransactionCollection() {
        return cashTransactionCollection;
    }

    public void setCashTransactionCollection(Collection<CashTransaction> cashTransactionCollection) {
        this.cashTransactionCollection = cashTransactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cashTransactionStatusId != null ? cashTransactionStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtcashTransactionStatus)) {
            return false;
        }
        DtcashTransactionStatus other = (DtcashTransactionStatus) object;
        if ((this.cashTransactionStatusId == null && other.cashTransactionStatusId != null) || (this.cashTransactionStatusId != null && !this.cashTransactionStatusId.equals(other.cashTransactionStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtcashTransactionStatus[ cashTransactionStatusId=" + cashTransactionStatusId + " ]";
    }
    
}
