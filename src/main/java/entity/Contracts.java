/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Contracts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contracts.findAll", query = "SELECT c FROM Contracts c")
    , @NamedQuery(name = "Contracts.findByContractId", query = "SELECT c FROM Contracts c WHERE c.contractId = :contractId")
    , @NamedQuery(name = "Contracts.findByTpaCode", query = "SELECT c FROM Contracts c WHERE c.tpaCode = :tpaCode")
    , @NamedQuery(name = "Contracts.findByContractNo", query = "SELECT c FROM Contracts c WHERE c.contractNo = :contractNo")
    , @NamedQuery(name = "Contracts.findByContractStatusReason", query = "SELECT c FROM Contracts c WHERE c.contractStatusReason = :contractStatusReason")
    , @NamedQuery(name = "Contracts.findByEffectiveDate", query = "SELECT c FROM Contracts c WHERE c.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "Contracts.findByExpirationDate", query = "SELECT c FROM Contracts c WHERE c.expirationDate = :expirationDate")
    , @NamedQuery(name = "Contracts.findByContractPurchasePrice", query = "SELECT c FROM Contracts c WHERE c.contractPurchasePrice = :contractPurchasePrice")
    , @NamedQuery(name = "Contracts.findByTransferFee", query = "SELECT c FROM Contracts c WHERE c.transferFee = :transferFee")
    , @NamedQuery(name = "Contracts.findByDealerPackTotal", query = "SELECT c FROM Contracts c WHERE c.dealerPackTotal = :dealerPackTotal")
    , @NamedQuery(name = "Contracts.findByContractCoBuyerIdFk", query = "SELECT c FROM Contracts c WHERE c.contractCoBuyerIdFk = :contractCoBuyerIdFk")
    , @NamedQuery(name = "Contracts.findByExternalUserName", query = "SELECT c FROM Contracts c WHERE c.externalUserName = :externalUserName")
    , @NamedQuery(name = "Contracts.findByClassCode", query = "SELECT c FROM Contracts c WHERE c.classCode = :classCode")
    , @NamedQuery(name = "Contracts.findByIsEsigned", query = "SELECT c FROM Contracts c WHERE c.isEsigned = :isEsigned")
    , @NamedQuery(name = "Contracts.findByOdometer", query = "SELECT c FROM Contracts c WHERE c.odometer = :odometer")
    , @NamedQuery(name = "Contracts.findByClaimStartDate", query = "SELECT c FROM Contracts c WHERE c.claimStartDate = :claimStartDate")
    , @NamedQuery(name = "Contracts.findByClaimStartUsage", query = "SELECT c FROM Contracts c WHERE c.claimStartUsage = :claimStartUsage")
    , @NamedQuery(name = "Contracts.findByExpirationUsage", query = "SELECT c FROM Contracts c WHERE c.expirationUsage = :expirationUsage")
    , @NamedQuery(name = "Contracts.findByPlanExpireUsageTypeIdFk", query = "SELECT c FROM Contracts c WHERE c.planExpireUsageTypeIdFk = :planExpireUsageTypeIdFk")
    , @NamedQuery(name = "Contracts.findByWaitMiles", query = "SELECT c FROM Contracts c WHERE c.waitMiles = :waitMiles")
    , @NamedQuery(name = "Contracts.findByWaitDays", query = "SELECT c FROM Contracts c WHERE c.waitDays = :waitDays")
    , @NamedQuery(name = "Contracts.findByTermExpireUsage", query = "SELECT c FROM Contracts c WHERE c.termExpireUsage = :termExpireUsage")
    , @NamedQuery(name = "Contracts.findByTermExpireTime", query = "SELECT c FROM Contracts c WHERE c.termExpireTime = :termExpireTime")
    , @NamedQuery(name = "Contracts.findByTermUpperTime", query = "SELECT c FROM Contracts c WHERE c.termUpperTime = :termUpperTime")
    , @NamedQuery(name = "Contracts.findByTermLowerTime", query = "SELECT c FROM Contracts c WHERE c.termLowerTime = :termLowerTime")
    , @NamedQuery(name = "Contracts.findByTermMonth", query = "SELECT c FROM Contracts c WHERE c.termMonth = :termMonth")
    , @NamedQuery(name = "Contracts.findByScheduledStartIdFk", query = "SELECT c FROM Contracts c WHERE c.scheduledStartIdFk = :scheduledStartIdFk")
    , @NamedQuery(name = "Contracts.findByVinFull", query = "SELECT c FROM Contracts c WHERE c.vinFull = :vinFull")
    , @NamedQuery(name = "Contracts.findByVehiclePurchaseDate", query = "SELECT c FROM Contracts c WHERE c.vehiclePurchaseDate = :vehiclePurchaseDate")
    , @NamedQuery(name = "Contracts.findByVehiclePurchasePrice", query = "SELECT c FROM Contracts c WHERE c.vehiclePurchasePrice = :vehiclePurchasePrice")
    , @NamedQuery(name = "Contracts.findByPurchaseTypeIdFk", query = "SELECT c FROM Contracts c WHERE c.purchaseTypeIdFk = :purchaseTypeIdFk")
    , @NamedQuery(name = "Contracts.findByVehiclePurchaseTypeIdFk", query = "SELECT c FROM Contracts c WHERE c.vehiclePurchaseTypeIdFk = :vehiclePurchaseTypeIdFk")
    , @NamedQuery(name = "Contracts.findByVehicleInServiceDate", query = "SELECT c FROM Contracts c WHERE c.vehicleInServiceDate = :vehicleInServiceDate")
    , @NamedQuery(name = "Contracts.findByMsrp", query = "SELECT c FROM Contracts c WHERE c.msrp = :msrp")
    , @NamedQuery(name = "Contracts.findByValidInd", query = "SELECT c FROM Contracts c WHERE c.validInd = :validInd")
    , @NamedQuery(name = "Contracts.findByEarningMethodInd", query = "SELECT c FROM Contracts c WHERE c.earningMethodInd = :earningMethodInd")
    , @NamedQuery(name = "Contracts.findByEarningWaitMonths", query = "SELECT c FROM Contracts c WHERE c.earningWaitMonths = :earningWaitMonths")
    , @NamedQuery(name = "Contracts.findByPlanExpireTimeIdFk", query = "SELECT c FROM Contracts c WHERE c.planExpireTimeIdFk = :planExpireTimeIdFk")
    , @NamedQuery(name = "Contracts.findByExtendExpirationWait", query = "SELECT c FROM Contracts c WHERE c.extendExpirationWait = :extendExpirationWait")
    , @NamedQuery(name = "Contracts.findByUpdatePendingEarning", query = "SELECT c FROM Contracts c WHERE c.updatePendingEarning = :updatePendingEarning")
    , @NamedQuery(name = "Contracts.findByFinanceTypeIdFk", query = "SELECT c FROM Contracts c WHERE c.financeTypeIdFk = :financeTypeIdFk")
    , @NamedQuery(name = "Contracts.findByFinanceAmount", query = "SELECT c FROM Contracts c WHERE c.financeAmount = :financeAmount")
    , @NamedQuery(name = "Contracts.findByFinanceMonth", query = "SELECT c FROM Contracts c WHERE c.financeMonth = :financeMonth")
    , @NamedQuery(name = "Contracts.findByRewardNumber", query = "SELECT c FROM Contracts c WHERE c.rewardNumber = :rewardNumber")
    , @NamedQuery(name = "Contracts.findByExceptionInd", query = "SELECT c FROM Contracts c WHERE c.exceptionInd = :exceptionInd")
    , @NamedQuery(name = "Contracts.findByFormCode", query = "SELECT c FROM Contracts c WHERE c.formCode = :formCode")
    , @NamedQuery(name = "Contracts.findByAlternateContractNumber", query = "SELECT c FROM Contracts c WHERE c.alternateContractNumber = :alternateContractNumber")
    , @NamedQuery(name = "Contracts.findByExpiredManuallyInd", query = "SELECT c FROM Contracts c WHERE c.expiredManuallyInd = :expiredManuallyInd")
    , @NamedQuery(name = "Contracts.findByFullyPostedPostingHistoryIdFk", query = "SELECT c FROM Contracts c WHERE c.fullyPostedPostingHistoryIdFk = :fullyPostedPostingHistoryIdFk")
    , @NamedQuery(name = "Contracts.findByBatchIdFk", query = "SELECT c FROM Contracts c WHERE c.batchIdFk = :batchIdFk")
    , @NamedQuery(name = "Contracts.findByUpdateUserName", query = "SELECT c FROM Contracts c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "Contracts.findByUpdateLast", query = "SELECT c FROM Contracts c WHERE c.updateLast = :updateLast")})
public class Contracts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractId")
    private Integer contractId;
    @Size(max = 30)
    @Column(name = "tpaCode")
    private String tpaCode;
    @Size(max = 30)
    @Column(name = "contractNo")
    private String contractNo;
    @Size(max = 100)
    @Column(name = "contractStatusReason")
    private String contractStatusReason;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "expirationDate")
    @Temporal(TemporalType.DATE)
    private Date expirationDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "contractPurchasePrice")
    private BigDecimal contractPurchasePrice;
    @Column(name = "transferFee")
    private BigDecimal transferFee;
    @Column(name = "dealerPackTotal")
    private BigDecimal dealerPackTotal;
    @Column(name = "contractCoBuyerIdFk")
    private Integer contractCoBuyerIdFk;
    @Size(max = 100)
    @Column(name = "externalUserName")
    private String externalUserName;
    @Size(max = 20)
    @Column(name = "classCode")
    private String classCode;
    @Size(max = 5)
    @Column(name = "isEsigned")
    private String isEsigned;
    @Column(name = "odometer")
    private Integer odometer;
    @Column(name = "claimStartDate")
    @Temporal(TemporalType.DATE)
    private Date claimStartDate;
    @Column(name = "claimStartUsage")
    private Integer claimStartUsage;
    @Column(name = "expirationUsage")
    private Integer expirationUsage;
    @Column(name = "planExpireUsageTypeIdFk")
    private Integer planExpireUsageTypeIdFk;
    @Column(name = "waitMiles")
    private Integer waitMiles;
    @Column(name = "waitDays")
    private Integer waitDays;
    @Column(name = "termExpireUsage")
    private Integer termExpireUsage;
    @Column(name = "termExpireTime")
    private Integer termExpireTime;
    @Column(name = "termUpperTime")
    private Integer termUpperTime;
    @Column(name = "termLowerTime")
    private Integer termLowerTime;
    @Column(name = "termMonth")
    private Integer termMonth;
    @Column(name = "scheduledStartIdFk")
    private Integer scheduledStartIdFk;
    @Size(max = 17)
    @Column(name = "vinFull")
    private String vinFull;
    @Column(name = "vehiclePurchaseDate")
    @Temporal(TemporalType.DATE)
    private Date vehiclePurchaseDate;
    @Column(name = "vehiclePurchasePrice")
    private BigDecimal vehiclePurchasePrice;
    @Size(max = 20)
    @Column(name = "purchaseTypeIdFk")
    private String purchaseTypeIdFk;
    @Column(name = "vehiclePurchaseTypeIdFk")
    private Integer vehiclePurchaseTypeIdFk;
    @Column(name = "vehicleInServiceDate")
    @Temporal(TemporalType.DATE)
    private Date vehicleInServiceDate;
    @Column(name = "msrp")
    private BigDecimal msrp;
    @Column(name = "validInd")
    private Boolean validInd;
    @Column(name = "earningMethodInd")
    private Integer earningMethodInd;
    @Column(name = "earningWaitMonths")
    private Integer earningWaitMonths;
    @Column(name = "planExpireTimeIdFk")
    private Integer planExpireTimeIdFk;
    @Column(name = "extendExpirationWait")
    private Boolean extendExpirationWait;
    @Column(name = "updatePendingEarning")
    private Boolean updatePendingEarning;
    @Column(name = "financeTypeIdFk")
    private Integer financeTypeIdFk;
    @Column(name = "financeAmount")
    private BigDecimal financeAmount;
    @Column(name = "financeMonth")
    private Integer financeMonth;
    @Size(max = 50)
    @Column(name = "rewardNumber")
    private String rewardNumber;
    @Column(name = "exceptionInd")
    private Boolean exceptionInd;
    @Size(max = 50)
    @Column(name = "formCode")
    private String formCode;
    @Size(max = 20)
    @Column(name = "alternateContractNumber")
    private String alternateContractNumber;
    @Column(name = "expiredManuallyInd")
    private Boolean expiredManuallyInd;
    @Column(name = "fullyPostedPostingHistoryIdFk")
    private Integer fullyPostedPostingHistoryIdFk;
    @Column(name = "BatchIdFk")
    private Integer batchIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractCoverageGroup> contractCoverageGroupCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<Note> noteCollection;
    @JoinColumn(name = "contractStatusIdFk", referencedColumnName = "contractStatusId")
    @ManyToOne
    private ContractStatus contractStatusIdFk;
    @JoinColumn(name = "agentIdFk", referencedColumnName = "agentId")
    @ManyToOne
    private Agent agentIdFk;
    @JoinColumn(name = "departmentIdFk", referencedColumnName = "departmentId")
    @ManyToOne
    private CfDepartment departmentIdFk;
    @JoinColumn(name = "customerIdFk", referencedColumnName = "customerNameId")
    @ManyToOne
    private CustomerName customerIdFk;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "dealerGroupIdFk", referencedColumnName = "dealerGroupId")
    @ManyToOne
    private DealerGroup dealerGroupIdFk;
    @JoinColumn(name = "termIdFk", referencedColumnName = "TermId")
    @ManyToOne
    private Term termIdFk;
    @JoinColumn(name = "soldByUserIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember soldByUserIdFk;
    @JoinColumn(name = "deductibleIdFk", referencedColumnName = "deductibleId")
    @ManyToOne
    private Deductible deductibleIdFk;
    @JoinColumn(name = "vinDescIdFk", referencedColumnName = "vinDescId")
    @ManyToOne
    private VinDesc vinDescIdFk;
    @JoinColumn(name = "lienHolderIdFk", referencedColumnName = "lienHolderId")
    @ManyToOne
    private LienHolder lienHolderIdFk;
    @JoinColumn(name = "limitIdFk", referencedColumnName = "limitId")
    @ManyToOne
    private Limit limitIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne
    private Product productIdFk;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractAdjustment> contractAdjustmentCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<RerateSessionContract> rerateSessionContractCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractSurcharge> contractSurchargeCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractPPMDetail> contractPPMDetailCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;
    @OneToMany(mappedBy = "relatedContractIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractCancel> contractCancelCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractPdfFile> contractPdfFileCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<HistoryLog> historyLogCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractHold> contractHoldCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contractIdFk")
    private Collection<ContractField> contractFieldCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contractIdFk")
    private Collection<CancelRequest> cancelRequestCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "newContractIdFk")
    private ContractReplacement contractReplacement;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "oldContractIdFk")
    private ContractReplacement contractReplacement1;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<ContractAmendment> contractAmendmentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contractIdFk")
    private Collection<ContractFile> contractFileCollection;
    @OneToMany(mappedBy = "contractIdFk")
    private Collection<Claim> claimCollection;

    public Contracts() {
    }

    public Contracts(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getTpaCode() {
        return tpaCode;
    }

    public void setTpaCode(String tpaCode) {
        this.tpaCode = tpaCode;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractStatusReason() {
        return contractStatusReason;
    }

    public void setContractStatusReason(String contractStatusReason) {
        this.contractStatusReason = contractStatusReason;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public BigDecimal getContractPurchasePrice() {
        return contractPurchasePrice;
    }

    public void setContractPurchasePrice(BigDecimal contractPurchasePrice) {
        this.contractPurchasePrice = contractPurchasePrice;
    }

    public BigDecimal getTransferFee() {
        return transferFee;
    }

    public void setTransferFee(BigDecimal transferFee) {
        this.transferFee = transferFee;
    }

    public BigDecimal getDealerPackTotal() {
        return dealerPackTotal;
    }

    public void setDealerPackTotal(BigDecimal dealerPackTotal) {
        this.dealerPackTotal = dealerPackTotal;
    }

    public Integer getContractCoBuyerIdFk() {
        return contractCoBuyerIdFk;
    }

    public void setContractCoBuyerIdFk(Integer contractCoBuyerIdFk) {
        this.contractCoBuyerIdFk = contractCoBuyerIdFk;
    }

    public String getExternalUserName() {
        return externalUserName;
    }

    public void setExternalUserName(String externalUserName) {
        this.externalUserName = externalUserName;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getIsEsigned() {
        return isEsigned;
    }

    public void setIsEsigned(String isEsigned) {
        this.isEsigned = isEsigned;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public Date getClaimStartDate() {
        return claimStartDate;
    }

    public void setClaimStartDate(Date claimStartDate) {
        this.claimStartDate = claimStartDate;
    }

    public Integer getClaimStartUsage() {
        return claimStartUsage;
    }

    public void setClaimStartUsage(Integer claimStartUsage) {
        this.claimStartUsage = claimStartUsage;
    }

    public Integer getExpirationUsage() {
        return expirationUsage;
    }

    public void setExpirationUsage(Integer expirationUsage) {
        this.expirationUsage = expirationUsage;
    }

    public Integer getPlanExpireUsageTypeIdFk() {
        return planExpireUsageTypeIdFk;
    }

    public void setPlanExpireUsageTypeIdFk(Integer planExpireUsageTypeIdFk) {
        this.planExpireUsageTypeIdFk = planExpireUsageTypeIdFk;
    }

    public Integer getWaitMiles() {
        return waitMiles;
    }

    public void setWaitMiles(Integer waitMiles) {
        this.waitMiles = waitMiles;
    }

    public Integer getWaitDays() {
        return waitDays;
    }

    public void setWaitDays(Integer waitDays) {
        this.waitDays = waitDays;
    }

    public Integer getTermExpireUsage() {
        return termExpireUsage;
    }

    public void setTermExpireUsage(Integer termExpireUsage) {
        this.termExpireUsage = termExpireUsage;
    }

    public Integer getTermExpireTime() {
        return termExpireTime;
    }

    public void setTermExpireTime(Integer termExpireTime) {
        this.termExpireTime = termExpireTime;
    }

    public Integer getTermUpperTime() {
        return termUpperTime;
    }

    public void setTermUpperTime(Integer termUpperTime) {
        this.termUpperTime = termUpperTime;
    }

    public Integer getTermLowerTime() {
        return termLowerTime;
    }

    public void setTermLowerTime(Integer termLowerTime) {
        this.termLowerTime = termLowerTime;
    }

    public Integer getTermMonth() {
        return termMonth;
    }

    public void setTermMonth(Integer termMonth) {
        this.termMonth = termMonth;
    }

    public Integer getScheduledStartIdFk() {
        return scheduledStartIdFk;
    }

    public void setScheduledStartIdFk(Integer scheduledStartIdFk) {
        this.scheduledStartIdFk = scheduledStartIdFk;
    }

    public String getVinFull() {
        return vinFull;
    }

    public void setVinFull(String vinFull) {
        this.vinFull = vinFull;
    }

    public Date getVehiclePurchaseDate() {
        return vehiclePurchaseDate;
    }

    public void setVehiclePurchaseDate(Date vehiclePurchaseDate) {
        this.vehiclePurchaseDate = vehiclePurchaseDate;
    }

    public BigDecimal getVehiclePurchasePrice() {
        return vehiclePurchasePrice;
    }

    public void setVehiclePurchasePrice(BigDecimal vehiclePurchasePrice) {
        this.vehiclePurchasePrice = vehiclePurchasePrice;
    }

    public String getPurchaseTypeIdFk() {
        return purchaseTypeIdFk;
    }

    public void setPurchaseTypeIdFk(String purchaseTypeIdFk) {
        this.purchaseTypeIdFk = purchaseTypeIdFk;
    }

    public Integer getVehiclePurchaseTypeIdFk() {
        return vehiclePurchaseTypeIdFk;
    }

    public void setVehiclePurchaseTypeIdFk(Integer vehiclePurchaseTypeIdFk) {
        this.vehiclePurchaseTypeIdFk = vehiclePurchaseTypeIdFk;
    }

    public Date getVehicleInServiceDate() {
        return vehicleInServiceDate;
    }

    public void setVehicleInServiceDate(Date vehicleInServiceDate) {
        this.vehicleInServiceDate = vehicleInServiceDate;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public Boolean getValidInd() {
        return validInd;
    }

    public void setValidInd(Boolean validInd) {
        this.validInd = validInd;
    }

    public Integer getEarningMethodInd() {
        return earningMethodInd;
    }

    public void setEarningMethodInd(Integer earningMethodInd) {
        this.earningMethodInd = earningMethodInd;
    }

    public Integer getEarningWaitMonths() {
        return earningWaitMonths;
    }

    public void setEarningWaitMonths(Integer earningWaitMonths) {
        this.earningWaitMonths = earningWaitMonths;
    }

    public Integer getPlanExpireTimeIdFk() {
        return planExpireTimeIdFk;
    }

    public void setPlanExpireTimeIdFk(Integer planExpireTimeIdFk) {
        this.planExpireTimeIdFk = planExpireTimeIdFk;
    }

    public Boolean getExtendExpirationWait() {
        return extendExpirationWait;
    }

    public void setExtendExpirationWait(Boolean extendExpirationWait) {
        this.extendExpirationWait = extendExpirationWait;
    }

    public Boolean getUpdatePendingEarning() {
        return updatePendingEarning;
    }

    public void setUpdatePendingEarning(Boolean updatePendingEarning) {
        this.updatePendingEarning = updatePendingEarning;
    }

    public Integer getFinanceTypeIdFk() {
        return financeTypeIdFk;
    }

    public void setFinanceTypeIdFk(Integer financeTypeIdFk) {
        this.financeTypeIdFk = financeTypeIdFk;
    }

    public BigDecimal getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(BigDecimal financeAmount) {
        this.financeAmount = financeAmount;
    }

    public Integer getFinanceMonth() {
        return financeMonth;
    }

    public void setFinanceMonth(Integer financeMonth) {
        this.financeMonth = financeMonth;
    }

    public String getRewardNumber() {
        return rewardNumber;
    }

    public void setRewardNumber(String rewardNumber) {
        this.rewardNumber = rewardNumber;
    }

    public Boolean getExceptionInd() {
        return exceptionInd;
    }

    public void setExceptionInd(Boolean exceptionInd) {
        this.exceptionInd = exceptionInd;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getAlternateContractNumber() {
        return alternateContractNumber;
    }

    public void setAlternateContractNumber(String alternateContractNumber) {
        this.alternateContractNumber = alternateContractNumber;
    }

    public Boolean getExpiredManuallyInd() {
        return expiredManuallyInd;
    }

    public void setExpiredManuallyInd(Boolean expiredManuallyInd) {
        this.expiredManuallyInd = expiredManuallyInd;
    }

    public Integer getFullyPostedPostingHistoryIdFk() {
        return fullyPostedPostingHistoryIdFk;
    }

    public void setFullyPostedPostingHistoryIdFk(Integer fullyPostedPostingHistoryIdFk) {
        this.fullyPostedPostingHistoryIdFk = fullyPostedPostingHistoryIdFk;
    }

    public Integer getBatchIdFk() {
        return batchIdFk;
    }

    public void setBatchIdFk(Integer batchIdFk) {
        this.batchIdFk = batchIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractCoverageGroup> getContractCoverageGroupCollection() {
        return contractCoverageGroupCollection;
    }

    public void setContractCoverageGroupCollection(Collection<ContractCoverageGroup> contractCoverageGroupCollection) {
        this.contractCoverageGroupCollection = contractCoverageGroupCollection;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    public ContractStatus getContractStatusIdFk() {
        return contractStatusIdFk;
    }

    public void setContractStatusIdFk(ContractStatus contractStatusIdFk) {
        this.contractStatusIdFk = contractStatusIdFk;
    }

    public Agent getAgentIdFk() {
        return agentIdFk;
    }

    public void setAgentIdFk(Agent agentIdFk) {
        this.agentIdFk = agentIdFk;
    }

    public CfDepartment getDepartmentIdFk() {
        return departmentIdFk;
    }

    public void setDepartmentIdFk(CfDepartment departmentIdFk) {
        this.departmentIdFk = departmentIdFk;
    }

    public CustomerName getCustomerIdFk() {
        return customerIdFk;
    }

    public void setCustomerIdFk(CustomerName customerIdFk) {
        this.customerIdFk = customerIdFk;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public DealerGroup getDealerGroupIdFk() {
        return dealerGroupIdFk;
    }

    public void setDealerGroupIdFk(DealerGroup dealerGroupIdFk) {
        this.dealerGroupIdFk = dealerGroupIdFk;
    }

    public Term getTermIdFk() {
        return termIdFk;
    }

    public void setTermIdFk(Term termIdFk) {
        this.termIdFk = termIdFk;
    }

    public UserMember getSoldByUserIdFk() {
        return soldByUserIdFk;
    }

    public void setSoldByUserIdFk(UserMember soldByUserIdFk) {
        this.soldByUserIdFk = soldByUserIdFk;
    }

    public Deductible getDeductibleIdFk() {
        return deductibleIdFk;
    }

    public void setDeductibleIdFk(Deductible deductibleIdFk) {
        this.deductibleIdFk = deductibleIdFk;
    }

    public VinDesc getVinDescIdFk() {
        return vinDescIdFk;
    }

    public void setVinDescIdFk(VinDesc vinDescIdFk) {
        this.vinDescIdFk = vinDescIdFk;
    }

    public LienHolder getLienHolderIdFk() {
        return lienHolderIdFk;
    }

    public void setLienHolderIdFk(LienHolder lienHolderIdFk) {
        this.lienHolderIdFk = lienHolderIdFk;
    }

    public Limit getLimitIdFk() {
        return limitIdFk;
    }

    public void setLimitIdFk(Limit limitIdFk) {
        this.limitIdFk = limitIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<ContractAdjustment> getContractAdjustmentCollection() {
        return contractAdjustmentCollection;
    }

    public void setContractAdjustmentCollection(Collection<ContractAdjustment> contractAdjustmentCollection) {
        this.contractAdjustmentCollection = contractAdjustmentCollection;
    }

    @XmlTransient
    public Collection<RerateSessionContract> getRerateSessionContractCollection() {
        return rerateSessionContractCollection;
    }

    public void setRerateSessionContractCollection(Collection<RerateSessionContract> rerateSessionContractCollection) {
        this.rerateSessionContractCollection = rerateSessionContractCollection;
    }

    @XmlTransient
    public Collection<ContractSurcharge> getContractSurchargeCollection() {
        return contractSurchargeCollection;
    }

    public void setContractSurchargeCollection(Collection<ContractSurcharge> contractSurchargeCollection) {
        this.contractSurchargeCollection = contractSurchargeCollection;
    }

    @XmlTransient
    public Collection<ContractPPMDetail> getContractPPMDetailCollection() {
        return contractPPMDetailCollection;
    }

    public void setContractPPMDetailCollection(Collection<ContractPPMDetail> contractPPMDetailCollection) {
        this.contractPPMDetailCollection = contractPPMDetailCollection;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<ContractCancel> getContractCancelCollection() {
        return contractCancelCollection;
    }

    public void setContractCancelCollection(Collection<ContractCancel> contractCancelCollection) {
        this.contractCancelCollection = contractCancelCollection;
    }

    @XmlTransient
    public Collection<ContractPdfFile> getContractPdfFileCollection() {
        return contractPdfFileCollection;
    }

    public void setContractPdfFileCollection(Collection<ContractPdfFile> contractPdfFileCollection) {
        this.contractPdfFileCollection = contractPdfFileCollection;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    @XmlTransient
    public Collection<ContractHold> getContractHoldCollection() {
        return contractHoldCollection;
    }

    public void setContractHoldCollection(Collection<ContractHold> contractHoldCollection) {
        this.contractHoldCollection = contractHoldCollection;
    }

    @XmlTransient
    public Collection<ContractField> getContractFieldCollection() {
        return contractFieldCollection;
    }

    public void setContractFieldCollection(Collection<ContractField> contractFieldCollection) {
        this.contractFieldCollection = contractFieldCollection;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    public ContractReplacement getContractReplacement() {
        return contractReplacement;
    }

    public void setContractReplacement(ContractReplacement contractReplacement) {
        this.contractReplacement = contractReplacement;
    }

    public ContractReplacement getContractReplacement1() {
        return contractReplacement1;
    }

    public void setContractReplacement1(ContractReplacement contractReplacement1) {
        this.contractReplacement1 = contractReplacement1;
    }

    @XmlTransient
    public Collection<ContractAmendment> getContractAmendmentCollection() {
        return contractAmendmentCollection;
    }

    public void setContractAmendmentCollection(Collection<ContractAmendment> contractAmendmentCollection) {
        this.contractAmendmentCollection = contractAmendmentCollection;
    }

    @XmlTransient
    public Collection<ContractFile> getContractFileCollection() {
        return contractFileCollection;
    }

    public void setContractFileCollection(Collection<ContractFile> contractFileCollection) {
        this.contractFileCollection = contractFileCollection;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractId != null ? contractId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contracts)) {
            return false;
        }
        Contracts other = (Contracts) object;
        if ((this.contractId == null && other.contractId != null) || (this.contractId != null && !this.contractId.equals(other.contractId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Contracts[ contractId=" + contractId + " ]";
    }
    
}
