/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.*;
import entity.AdminCompany;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named
@Dependent
public class ClaimMisc implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(ClaimMisc.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;
    @Inject
    Login dowclogin;

    public ClaimMisc() {
        //BasicConfigurator.configure();
    }

    public final BigDecimal BIGZERO = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

    String page = "ContractSearch";

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        LOGGER.info("setPage=" + page);
        this.page = page;
    }

    public String getCurrentUser() {
        LOGGER.info("login=" + dowclogin.getLoginUser());
        return dowclogin.getLoginUser();
    }

    public Claim getClaimByNumber(String num) {
        try {
            return em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getClaimByNumber is null");
            return null;
        }
    }

    public Claim getClaimById(Integer id) {
        try {
            return em.createNamedQuery("Claim.findByClaimId", Claim.class).setParameter("claimId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getClaimById is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return List
     */
    public List<Claim> getClaimByContractNo(String num) {
        try {
            return (List<Claim>) em.createNativeQuery("select b.* from Contracts a, Claim b where contractNo=?1 and a.contractId=b.contractIdFk order by b.claimId desc", Claim.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getClaimByContractNo is null");
            return null;
        }
    }

    public BigDecimal getPaidClaimsByContractNo(String no) {
        try {
            List<Claim> claimList = getClaimByContractNo(no);
            BigDecimal paidClaims = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
            if (claimList != null && claimList.size() > 0) {
                for (Claim claim : claimList) {
                    if (claim.getAmount() != null) {
                        paidClaims = paidClaims.add(claim.getAmount());
                    }
                }
            }
            return paidClaims;
        } catch (NoResultException e) {
            LOGGER.info("getPaidClaimsByContractNo is null");
            return null;
        }
    }

    public BigDecimal getUnPaidClaimsByContractNo(String no) {
        BigDecimal unPaidClaim = BIGZERO;
        try {
            List<Claim> claimList = getClaimByContractNo(no);
            if (claimList != null && claimList.size() > 0) {
                for (Claim claim : claimList) {
                    if (claim.getAmount() != null && !claim.getClaimStatus().getClaimStatusDesc().contains("Closed")) {
                        unPaidClaim = unPaidClaim.add(claim.getAmount());
                    }
                }
            }
            return unPaidClaim;
        } catch (NoResultException e) {
            LOGGER.info("getPaidClaimsByContractNo is null");
            return null;
        }
    }

    /**
     *
     * @param cg
     */
    public void createClaim(ClaimGeneral cg) {
        try {
            utx.begin();
            Integer akid = null;
            Integer repairFacilityContactIdFk = null;
            RepairFacilityContactPerson rfcp = null;
            Integer userMemberId = 1; // in the future, need to be get from Login
            int ret;

            if (cg.getContactName() != null && cg.getContactName().length() > 0) {

                List<RepairFacilityContactPerson> rfcps = (List<RepairFacilityContactPerson>) em.createNativeQuery("select b.* from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk", RepairFacilityContactPerson.class).setParameter(1, akid).getResultList();
                LOGGER.info("length of rfcps=" + rfcps.size());
                if (!rfcps.isEmpty()) {
                    for (RepairFacilityContactPerson rp : rfcps) {
                        LOGGER.info("rp=" + rp.getPhoneNumber());
                        rp.setName(cg.getContactName());
                        rp.setPhoneNumber(cg.getContactPhone());
                        em.merge(rp);
                        rfcp = rp;
                        repairFacilityContactIdFk = rfcp.getRepairFacilityContactPersonId();
                        LOGGER.info("merge rfcp");
                        LOGGER.info("rfcp=" + repairFacilityContactIdFk);
                        break;
                    }
                } else {    // add new record in RepairFacilityContactPerson
                    LOGGER.info("new rfcp, 2=" + cg.getContactPhone());
                    Query query = em.createNativeQuery("insert into RepairFacilityContactPerson (name, PhoneNumber, repairFacilityIdFk) values (?1, ?2, ?3)");
                    query.setParameter(1, cg.getContactName());
                    query.setParameter(2, cg.getContactPhone());
                    query.setParameter(3, akid);
                    ret = query.executeUpdate();
                    LOGGER.info("insert to rfcp, ret=" + ret);
                    repairFacilityContactIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('RepairFacilityContactPerson')").getSingleResult()).intValueExact();
                    rfcp = em.createNamedQuery("RepairFacilityContactPerson.findByRepairFacilityContactPersonId", RepairFacilityContactPerson.class).setParameter("repairFacilityContactPersonId", repairFacilityContactIdFk).getSingleResult();
                    LOGGER.info("latest id=" + repairFacilityContactIdFk);
                }
            }

            Query query = em.createNativeQuery("insert into Claim (currentOdometer, repairOrderNumber, repairOrderDate, repairFacilityIdFk, repairFacilityContactIdFk, productTypeIdFk, claimStatus, claimSubStatus, contractIdFk, paidToFk, inceptionDate, ownedByFk) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)");
            query.setParameter(1, cg.getOdometer());
            query.setParameter(2, cg.getRoNumber());
            query.setParameter(3, convSTDateWS(cg.getRoDate()));
            query.setParameter(4, akid);
            query.setParameter(5, repairFacilityContactIdFk);
            query.setParameter(6, 1);   // VSC
            query.setParameter(7, "1");   //  1, Pending
            query.setParameter(8, "1");   //  1, Pending Approval
            query.setParameter(9, cg.getContractIdFk());
            query.setParameter(10, akid);
            query.setParameter(11, getCurrnetDateTime());
            query.setParameter(12, userMemberId);
            query.executeUpdate();

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    /**
     *
     * @param cg
     */
    void updateClaim(ClaimGeneral cg) {
        try {
            utx.begin();
            Integer akid = null;
            Integer repairFacilityContactIdFk = null;
            RepairFacilityContactPerson rfcp = null;
            if (cg.getRfName() != null && cg.getRfName().length() > 0) {
                AccountKeeper ak = getAccoutKeeperByRFname(cg.getRfName());
                akid = ak.getAccountKeeperId();
            }
            Claim claim = getClaimByNumber(cg.getClaimNumber());
            if (cg.getOdometer() != null && cg.getOdometer() > 0) {
                claim.setCurrentOdometer(cg.getOdometer());
            }
            if (cg.getRoNumber() != null && cg.getRoNumber().length() > 0) {
                claim.setRepairOrderNumber(cg.getRoNumber());
            }
            if (akid != null) {
                RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                claim.setRepairFacilityIdFk(rf);

                //claim.setRepairFacilityContactIdFk(rfcp);
                claim.setPaidToFk(getAccountKeeperById(akid));
            }
            em.merge(claim);
            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void updateAddress(Address addr) {
        try {
            utx.begin();

            em.merge(addr);
            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    void updateClaimByAmount(String claimNumber, BigDecimal cost) {
        try {
            utx.begin();

            Claim claim = getClaimByNumber(claimNumber);
            claim.setAmount(cost);

            em.merge(claim);
            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    void updateObject(Object obj) {
        try {
            utx.begin();
            em.merge(obj);
            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public Contracts getContractsByClaimNum(String num) {
        try {
            return (Contracts) em.createQuery("select b from  Claim a join a.contractIdFk b where  a.claimNumber= :num").setParameter("num", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractsByClaimNum is null");
            return null;
        }
    }

    public Contracts getContractsByNo(String no) {
        try {
            LOGGER.info("in getContractsByNo=" + no);
            return em.createNamedQuery("Contracts.findByContractNo", Contracts.class).setParameter("contractNo", no).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractsByNo is null");
            return null;
        }
    }

    public Contracts getContractsById(Integer id) {
        try {
            return em.createNamedQuery("Contracts.findByContractId", Contracts.class).setParameter("contractId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractsById is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return AccountKeeper
     */
    public AccountKeeper getAccountKeeperByClaimNumber(String num) {
        try {
            return (AccountKeeper) em.createQuery("select b from Claim a join  a.paidToFk b where a.claimNumber= :claimNo").setParameter("claimNo", num).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByClaimNumber is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByContractNumber(String num) {
        try {
            //LOGGER.info("getAccountKeeperByContractNumber, contractNo=" + num);
            // TCSUB00031649 cause multiple returns, only select the first one
            return (AccountKeeper) em.createNativeQuery("select b.* from Contracts a, AccountKeeper b where a.customerIdFk=b.accountKeeperId and a.contractNo=?1", AccountKeeper.class).setParameter(1, num).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByContractNumber is null");
            return null;
        }
    }

    public AccountKeeper getCustomerNameByAccountKeeperUsingContractId(Integer id) {
        try {
            return (AccountKeeper) em.createNativeQuery("select b.* from Contracts a, AccountKeeper b where a.customerIdFk=b.accountKeeperId and a.contractid=?1", AccountKeeper.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCustomerNameByAccountKeeperUsingContractId is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByContractDealer(String num) {
        try {
            return (AccountKeeper) em.createNativeQuery("select b.* from Contracts a, AccountKeeper b where a.contractNo=?1 and a.dealerIdFk=b.accountKeeperId", AccountKeeper.class).setParameter(1, num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByContractDealer is null");
            return null;
        }
    }

    public AccountKeeper getDealerByAccountKeeperByContractId(Integer id) {
        try {
            return (AccountKeeper) em.createNativeQuery("select b.* from Contracts a, AccountKeeper b where a.contractId=?1 and a.dealerIdFk=b.accountKeeperId", AccountKeeper.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDealerByAccountKeeperByContractId is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByClaimOwnedBy(String num) {
        try {
            return (AccountKeeper) em.createNativeQuery("select b.* from Claim a, AccountKeeper b where  ownedByFk=b.accountKeeperId  and claimNumber=?1", AccountKeeper.class).setParameter(1, num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByClaimOwnedBy is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByClaimEnteredId(String num) {
        try {
            return (AccountKeeper) em.createNativeQuery("select b.* from Claim a, AccountKeeper b where  a.enteredIdFk=b.accountKeeperId  and claimNumber=?1", AccountKeeper.class).setParameter(1, num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByClaimEnteredId is null");
            return null;
        }
    }

    public AccountKeeper getAgentByAccountKeeperByContractId(Integer id) {
        try {
            return (AccountKeeper) em.createNativeQuery("select c.* from Contracts a, Agent b, AccountKeeper c  where a.agentIdFk=b.agentId and b.agentId=c.accountKeeperId and a.contractId=?1", AccountKeeper.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAgentByAccountKeeperByContractId is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperForCustomer(String cname) {
        try {
            return (AccountKeeper) em.createNativeQuery("select * from AccountKeeper where active=1 and accountKeeperTypeIdFk=6 and accountKeeperName=?1 ", AccountKeeper.class).setParameter(1, cname).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperForCustomer is null");
            return null;
        }
    }

    public List<AccountKeeper> getAccountKeeperByInsurer() {
        try {
            return (List<AccountKeeper>) em.createNativeQuery("select b.* from Insurer a, AccountKeeper b where a.insurerId=b.accountKeeperId order by b.accountKeeperName", AccountKeeper.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByInsurer is null");
            return null;
        }
    }

    public Insurer getInsurerByAccountKeeperName(String name) {
        try {
            return (Insurer) em.createNativeQuery("select a.* from Insurer a, AccountKeeper b where a.insurerId=b.accountKeeperId and b.accountKeeperTypeIdFk=8 and b.accountKeeperName=?1", Insurer.class).setParameter(1, name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getInsurerByAccountKeeperName is null");
            return null;
        }
    }

    /**
     *
     * @param id
     * @return AccountKeeper
     */
    public AccountKeeper getAccountKeeperById(Integer id) {
        try {
            return em.createNamedQuery("AccountKeeper.findByAccountKeeperId", AccountKeeper.class).setParameter("accountKeeperId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperById is null");
            return null;
        }
    }

    public List<AccountKeeper> getAccountKeeperByType(Integer typeId) {
        try {
            return (List<AccountKeeper>) em.createNativeQuery("select * from AccountKeeper where active=1 and accountKeeperTypeIdFk=?1", AccountKeeper.class).setParameter(1, typeId).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByType is null");
            return null;
        }
    }

    public List<AccountKeeper> getAccountKeeperExceptCustomer() {
        try {
            return (List<AccountKeeper>) em.createNativeQuery("select * from AccountKeeper where active=1 and accountKeeperTypeIdFk!=6", AccountKeeper.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByType is null");
            return null;
        }
    }

    public HashMap<String, Integer> getAccountKeeperListHM() {
        try {
            HashMap<String, Integer> akhm = new HashMap<>();
            List<AccountKeeper> akList = getAccountKeeperExceptCustomer();
            for (AccountKeeper ak : akList) {
                String displayName = ak.getAccountKeeperName() + "(" + getAccountKeeperTypeById(ak.getAccountKeeperId()).getAccountKeeperTypeDesc() + ")";
                akhm.put(displayName, ak.getAccountKeeperId());
            }
            return akhm;
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByType is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByNameType(String name, Integer typeId) {
        try {
            //return (AccountKeeper) em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=?1 and accountKeeperName = ?2", AccountKeeper.class).setParameter(1, typeId).setParameter(2, name).getResultList().get(0);
            List<AccountKeeper> akList = (List<AccountKeeper>) em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=?1 and accountKeeperName = ?2", AccountKeeper.class).setParameter(1, typeId).setParameter(2, name).getResultList();
            //return (AccountKeeper) em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=?1 and accountKeeperName = ?2", AccountKeeper.class).setParameter(1, typeId).setParameter(2, name).getSingleResult();
            if (akList.size() > 0) {
                return akList.get(0);
            } else {
                return null;
            }
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByNameType is null");
            return null;
        }
    }

    public RateBasedRuleGroup getRateBasedRuleGroupByName(String name) {
        try {
            List<RateBasedRuleGroup> rbrgList = (List<RateBasedRuleGroup>) em.createNativeQuery("select * from RateBasedRuleGroup where name=?1", RateBasedRuleGroup.class).setParameter(1, name).getResultList();
            if (rbrgList.size() > 0) {
                return rbrgList.get(0);
            } else {
                return null;
            }
        } catch (NoResultException e) {
            LOGGER.info("getRateBasedRuleGroupByName is null");
            return null;
        }
    }

    public RateBasedRuleGroup getRateBasedRuleGroupById(Integer id) {
        try {
            return em.createNamedQuery("RateBasedRuleGroup.findByRateBasedRuleGroupId", RateBasedRuleGroup.class).setParameter("rateBasedRuleGroupId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRateBasedRuleGroupByName is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return UserMember
     */
    public UserMember getUserMemberByClaimOwnedBy(String num) {
        try {
            return (UserMember) em.createQuery("select b from Claim a join  a.ownedByFk b where a.claimNumber= :claimNo").setParameter("claimNo", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getUserMemberByClaimOwnedBy is null");
            return null;
        }
    }

    public UserMember getUserMemberByClaimEnteredId(String num) {
        try {
            return (UserMember) em.createQuery("select b from Claim a join  a.enteredIdFk b where a.claimNumber= :claimNo").setParameter("claimNo", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getUserMemberByClaimEnteredId is null");
            return null;
        }
    }

    public UserMember getUserMemberByName(String num) {
        try {
            return (UserMember) em.createNamedQuery("UserMember.findByUserName", UserMember.class).setParameter("userName", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getUserMemberByName is null");
            return null;
        }
    }

    public UserMember getUserMemberById(Integer id) {
        try {
            return (UserMember) em.createNamedQuery("UserMember.findByUserMemberId", UserMember.class).setParameter("userMemberId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getUserMemberById is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return Address
     */
    public Address getAddressByClaimNumber(String num) {
        try {
            AccountKeeper ak = getAccountKeeperByClaimNumber(num);
            return (Address) em.createQuery("select b from AccountKeeper a join a.physicalAddressIdFk b where a.accountKeeperId=:id ").setParameter("id", ak.getAccountKeeperId()).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAddressByClaimNumber is null");
            return null;
        }
    }

    public Address getAddressByClaimNumber(int id) {
        try {
            return (Address) em.createQuery("select b from AccountKeeper a join a.physicalAddressIdFk b where a.accountKeeperId=:id ").setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAddressByClaimNumber is null");
            return null;
        }
    }

    public Address getPhysicalAddressByContractNo(String cno) {
        try {
            return (Address) em.createQuery("select  d.* from Contracts a, AccountKeeper b, Dealer c, Address d where  a.dealerId=c.dealerId and c.dealerId=b.accountKeeperId and  d.addressId=b.physicalAddressIdFk and a.contractNo=?1 ", Address.class).setParameter(1, cno).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getPhysicalAddressByContractNo is null");
            return null;
        }
    }

    public Address getBillAddressByContractNo(String cno) {
        try {
            return (Address) em.createQuery("select  d.* from Contracts a, AccountKeeper b, Dealer c, Address d where  a.dealerId=c.dealerId and c.dealerId=b.accountKeeperId and  d.addressId=b.billingAddressIdFk and a.contractNo=?1 ", Address.class).setParameter(1, cno).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getBillAddressByContractNo is null");
            return null;
        }
    }

    public List<RepairFacilityToDealerRel> getRepairFacilityToDealerRelByContractNo(String num) {
        try {
            return (List<RepairFacilityToDealerRel>) em.createNativeQuery("select b.* from Contracts a, RepairFacilityToDealerRel b where a.contractNo=?1 and a.dealerIdFk=b.dealerId", RepairFacilityToDealerRel.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("RepairFacilityToDealerRel is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return RepairFacilityContactPerson
     */
    public RepairFacilityContactPerson getRepairFacilityContactPersonByClaimNumber(String num) {
        try {
            return (RepairFacilityContactPerson) em.createQuery("select b from Claim a join a.repairFacilityContactIdFk b where a.claimNumber=:no").setParameter("no", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityContactPersonByClaimNumber is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return RepairFacility
     */
    public RepairFacility getRepairFacilityByClaimNumber(String num) {
        try {
            return (RepairFacility) em.createQuery("select b from Claim a join a.repairFacilityIdFk b where a.claimNumber=:no").setParameter("no", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityByClaimNumber is null");
            return null;
        }
    }

    public RepairFacility getRepairFacilityById(Integer id) {
        try {
            return (RepairFacility) em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityById is null");
            return null;
        }
    }

    /**
     *
     * @param rpid
     * @return List of RepairFacilityContactPerson
     */
    public List<RepairFacilityContactPerson> getRepairFacilityContactPersonById(Integer rpid) {
        try {
            return (List<RepairFacilityContactPerson>) em.createNativeQuery("select * from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk", RepairFacilityContactPerson.class).setParameter(1, rpid).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityContactPersonById is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return CfRegion
     */
    public CfRegion getRegionByClaimNumber(String num) {
        //TypedQuery<Region> query = em.createQuery("select d.* from AccountKeeper a, Address b, Claim c, CfRegion d where c.claimNumber=:no and c.repairFacilityIdFk=a.accountKeeperId  and a.physicalAddressIdFk=b.addressId and b.regionIdFk=d.regionId", Region.class);
        try {
            return (CfRegion) em.createQuery("select d from Claim a join a.paidToFk b join b.physicalAddressIdFk c join c.regionIdFk d where a.claimNumber=:no").setParameter("no", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRegionByClaimNumber is null");
            return null;
        }
    }

    public List<ClaimDetailSub> getClaimDetailSub(String num) {
        try {
            return (List<ClaimDetailSub>) em.createNativeQuery("select c.* from Claim a, ClaimDetail b, ClaimDetailSub c where a.claimNumber= ?1 and a.claimId=b.claimIdFk and b.claimDetailSubIdFk=c.claimDetailSubId order by b.claimDetailId", ClaimDetailSub.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getClaimDetailSub is null");
            return null;
        }
    }

    public List<ClaimLabor> getClaimLabor(String num) {
        try {
            return (List<ClaimLabor>) em.createNativeQuery("select d.* from Claim a, ClaimDetail b, ClaimDetailItem c, ClaimLabor d where a.claimNumber= ?1 and a.claimId=b.claimIdFk and b.claimDetailId=c.claimDetailIdFk and c.claimLaborIdFk=d.claimLaborId order by b.claimDetailId, c.detailItemId", ClaimLabor.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getClaimLabor is null");
            return null;
        }
    }

    public List<ClaimDetailItem> getClaimDetailItem(String num) {
        try {
            return (List<ClaimDetailItem>) em.createNativeQuery("select c.* from Claim a, ClaimDetail b, ClaimDetailItem c where a.claimNumber= ?1 and a.claimId=b.claimIdFk and b.claimDetailId=c.claimDetailIdFk order by b.claimDetailId, c.detailItemId", ClaimDetailItem.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("ClaimDetailItem is null");
            return null;
        }
    }

    public List<ClaimDetail> getClaimDetail(String num) {
        try {
            return (List<ClaimDetail>) em.createNativeQuery("select b.* from Claim a, ClaimDetail b where a.claimNumber= ?1 and  a.claimId=b.claimIdFk order by b.claimDetailId", ClaimDetail.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("ClaimDetail is null");
            return null;
        }
    }

    /**
     * from ClaimDetail
     *
     * @param id
     * @return ClaimDetailSub
     */
    public ClaimDetailSub getClaimDetailSub(Integer id) {
        try {
            return (ClaimDetailSub) em.createNativeQuery("select c.* from ClaimDetail b, ClaimDetailSub c where b.claimDetailId = ?1 and b.claimDetailSubIdFk=c.claimDetailSubId order by b.claimDetailId", ClaimDetailSub.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("ClaimDetailSub is null");
            return null;
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public List<ClaimDetailItem> getClaimDetailItem(Integer id) {
        try {
            return (List<ClaimDetailItem>) em.createNativeQuery("select c.* from  ClaimDetail b, ClaimDetailItem c where b.claimDetailId = ?1 and b.claimDetailId=c.claimDetailIdFk  order by b.claimDetailId, c.detailItemId", ClaimDetailItem.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("ClaimDetailItem is null");
            return null;
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public ClaimLabor getClaimLaborByItemId(Integer id) {
        try {
            return (ClaimLabor) em.createNativeQuery("select  d.* from ClaimDetailItem c, ClaimLabor d where c.detailItemId = ?1 and c.claimLaborIdFk=d.claimLaborId order by c.detailItemId", ClaimLabor.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("ClaimLabor is null");
            return null;
        }
    }

    /**
     *
     * @param id
     * @return List<>
     */
    public List<ClaimPart> getClaimPartByItemId(Integer id) {
        try {
            LOGGER.info("in getClaimPartByItemId");
            return (List<ClaimPart>) em.createNativeQuery("select  d.* from  ClaimDetailItem c, ClaimPart d where c.detailItemId = ?1 and c.detailItemId=d.detailItemIdFk order by  c.detailItemId", ClaimPart.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("ClaimPart is null");
            return null;
        }
    }

    /**
     *
     * @param num
     * @return List<>
     */
    public List<Note> getNoteByClaimNumber(String num) {
        try {
            return (List<Note>) em.createNativeQuery("select b.* from Claim a, Note b where claimNumber=?1 and a.claimId=b.claimIdFk order by noteId desc", Note.class).setParameter(1, num).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("Note is null");
            return null;
        }
    }

    /**
     * for auto completion of RF name field in claim screen
     *
     * @param rfname
     * @return List
     */
    public CfRegion getRegionByRFname(String rfname) {
        try {
            return (CfRegion) em.createNativeQuery("select d.* from AccountKeeper a, RepairFacility b, Address c, CfRegion d where a.accountKeeperName = ?1 and a.accountKeeperId=b.repairFacilityId and c.regionIdFk= d.regionId and  c.addressId=a.physicalAddressIdFk", CfRegion.class).setParameter(1, rfname).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRegionByRFname is null");
            return null;
        }
    }

    /**
     *
     * @param id
     * @return CfRegion
     */
    public CfRegion getRegionByAddressId(Integer id) {
        try {
            return (CfRegion) em.createNativeQuery("select b.* from Address a, CfRegion b where a.addressId=?1 and  a.regionIdFk=b.regionId", CfRegion.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRegionByAddressId is null");
            return null;
        }
    }

    /**
     *
     * @param rfname
     * @return List
     */
    public AccountKeeper getAccoutKeeperByRFname(String rfname) {
        try {
            return (AccountKeeper) em.createNativeQuery("select a.* from AccountKeeper a, RepairFacility b, Address c, CfRegion d where a.accountKeeperName like ?1 and a.accountKeeperId=b.repairFacilityId and c.regionIdFk= d.regionId and  c.addressId=a.physicalAddressIdFk", AccountKeeper.class).setParameter(1, rfname + "%").getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("AccountKeeper is null");
            return null;
        }
    }

    /**
     *
     * @param rfname
     * @return List
     */
    public RepairFacility getRepairFacilityByRFname(String rfname) {
        try {
            return (RepairFacility) em.createNativeQuery("select b.* from AccountKeeper a, RepairFacility b, Address c, Region d where a.accountKeeperName=?1 and a.accountKeeperId=b.repairFacilityId and c.regionIdFk= d.regionId and  c.addressId=a.physicalAddressIdFk", RepairFacility.class).setParameter(1, rfname).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("RepairFacility is null");
            return null;
        }
    }

    /**
     *
     * @param rfname
     * @return List
     */
    public Address getAddressByRFname(String rfname) {
        try {
            return (Address) em.createNativeQuery("select c.* from AccountKeeper a, RepairFacility b, Address c, Region d where a.accountKeeperName=?1 and a.accountKeeperId=b.repairFacilityId and c.regionIdFk= d.regionId and  c.addressId=a.physicalAddressIdFk", Address.class).setParameter(1, rfname).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("Address is null");
            return null;
        }
    }

    public ClaimAddress getClaimAddressByContractNo(String cno) {
        try {
            Object[] tmp = (Object[]) em.createNativeQuery("select  d.address1, d.address2, d.city, d.zipCode, d.phoneNumber, d.fax, e.regionCode from Contracts a, AccountKeeper b, Dealer c, Address d, CfRegion e where  a.dealerId=c.dealerId and c.dealerId=b.accountKeeperId and  d.addressId=b.physicalAddressIdFk and d.regionIdFk=e.regionId and   a.contractNo=?1").setParameter(1, cno).getSingleResult();
            ClaimAddress claimAddress = new ClaimAddress();
            claimAddress.setAddress1((String) tmp[0]);
            claimAddress.setAddress2((String) tmp[1]);
            claimAddress.setCity((String) tmp[2]);
            claimAddress.setRegionCode((String) tmp[6]);
            claimAddress.setZipCode((String) tmp[3]);
            claimAddress.setPhoneNumber((String) tmp[4]);
            claimAddress.setFax((String) tmp[5]);

            return claimAddress;

        } catch (NoResultException e) {
            LOGGER.info("getClaimAddressByContractNo is null");
            return null;
        }
    }

    public ClaimAddress getPhysicalAddressById(Integer id) {
        try {
            Object[] tmp = (Object[]) em.createNativeQuery("select  d.address1, d.address2, d.city, d.zipCode, d.phoneNumber, d.fax, e.regionCode from AccountKeeper b,  Address d, CfRegion e where   d.addressId=b.physicalAddressIdFk and d.regionIdFk=e.regionId and   b.accountKeeperId=?1").setParameter(1, id).getSingleResult();
            ClaimAddress claimAddress = new ClaimAddress();
            claimAddress.setAddress1((String) tmp[0]);
            claimAddress.setAddress2((String) tmp[1]);
            claimAddress.setCity((String) tmp[2]);
            claimAddress.setRegionCode((String) tmp[6]);
            claimAddress.setZipCode((String) tmp[3]);
            claimAddress.setPhoneNumber((String) tmp[4]);
            claimAddress.setFax((String) tmp[5]);

            return claimAddress;

        } catch (NoResultException e) {
            LOGGER.info("getPhysicalAddressById is null");
            return null;
        }
    }

    public ClaimAddress getBillingAddressById(Integer id) {
        try {
            Object[] tmp = (Object[]) em.createNativeQuery("select  d.address1, d.address2, d.city, d.zipCode, d.phoneNumber, d.fax, e.regionCode from AccountKeeper b, Address d, CfRegion e where  d.addressId=b.billingAddressIdFk and d.regionIdFk=e.regionId and  b.accountKeeperId=?1").setParameter(1, id).getSingleResult();
            ClaimAddress claimAddress = new ClaimAddress();
            claimAddress.setAddress1((String) tmp[0]);
            claimAddress.setAddress2((String) tmp[1]);
            claimAddress.setCity((String) tmp[2]);
            claimAddress.setRegionCode((String) tmp[6]);
            claimAddress.setZipCode((String) tmp[3]);
            claimAddress.setPhoneNumber((String) tmp[4]);
            claimAddress.setFax((String) tmp[5]);

            return claimAddress;

        } catch (NoResultException e) {
            LOGGER.info("getBillingAddressById is null");
            return null;
        }
    }

    public ProductType getProductTypeByClaimNumber(String num) {
        try {
            return (ProductType) em.createQuery("select b from Claim a join  a.productTypeIdFk b where a.claimNumber= :claimNo").setParameter("claimNo", num).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getProductTypeByClaimNumber is null");
            return null;
        }
    }

    public List<ProductType> getProductTypeByAll() {
        try {
            return (List<ProductType>) em.createNamedQuery("ProductType.findAll", ProductType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getProductTypeByAll is null");
            return null;
        }
    }

    public ProductType getProductTypeByName(String name) {
        try {
            return em.createNamedQuery("ProductType.findByProductTypeName", ProductType.class).setParameter("productTypeName", name).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getProductTypeByName is null");
            return null;
        }
    }

    public ProductType getProductTypeById(Integer id) {
        try {
            return em.createNamedQuery("ProductType.findByProductTypeId", ProductType.class).setParameter("productTypeId", id).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getProductTypeById is null");
            return null;
        }
    }

    public List<ProductSubType> getProductSubTypeByAll() {
        try {
            return (List<ProductSubType>) em.createNamedQuery("ProductSubType.findAll", ProductSubType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getProductSubTypeByAll is null");
            return null;
        }
    }

    public ProductSubType getProductSubTypeByName(String name) {
        LOGGER.info("in getProductSubTypeByName, name=" + name);
        try {
            return em.createNamedQuery("ProductSubType.findByDescription", ProductSubType.class).setParameter("description", name).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getProductSubTypeByName is null");
            return null;
        }
    }

    public List<ContractForm> getContractFormByAll() {
        try {
            return (List<ContractForm>) em.createNamedQuery("ContractForm.findAll", ContractForm.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getContractFormByAll is null");
            return null;
        }
    }

    public ContractForm getContractFormByName(String desc) {
        try {
            return em.createNamedQuery("ContractForm.findByDescription", ContractForm.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractFormByName is null");
            return null;
        }
    }

    public List<ProductSalesGroup> getProductSalesGroupByAll() {
        try {
            return (List<ProductSalesGroup>) em.createNativeQuery("select * from ProductSalesGroup", ProductSalesGroup.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getProductSalesGroupByAll is null");
            return null;
        }
    }

    public ClaimStatus getClaimStatusByDesc(String name) {
        try {
            return em.createNamedQuery("ClaimStatus.findByClaimStatusDesc", ClaimStatus.class).setParameter("claimStatusDesc", name).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getClaimStatusByDesc is null");
            return null;
        }
    }

    public ClaimSubstatus getClaimSubStatusByDesc(String name) {
        try {
            return em.createNamedQuery("ClaimSubstatus.findByClaimSubstatusDesc", ClaimSubstatus.class).setParameter("claimSubstatusDesc", name).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getClaimSubStatusByDesc is null");
            return null;
        }
    }

    public VinDesc getVinDescByContract(String cno) {
        try {
            LOGGER.info("in getVinDescByContract=" + cno);
            return (VinDesc) em.createNativeQuery("select b.* from Contracts a, VinDesc b where a.vinDescIdFk=b.vinDescId and a.contractNo=?1", VinDesc.class).setParameter(1, cno).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getVinDescByContract is null");
            return null;
        }
    }

    public List<ContractField> getContractFieldByContractNo(String cno) {
        try {
            List<ContractField> cfList = new ArrayList<>();

            List<Object[]> rawList;

            rawList = em.createNativeQuery("select b.contractFieldId, c.name, b.stringValue from Contracts a, ContractField b, CfField c where a.contractNo=?1 and a.contractId=b.contractIdFk and b.fieldIdFk=c.FieldId").setParameter(1, cno).getResultList();
            for (Object[] elem : rawList) {
                cfList.add(new ContractField((Integer) elem[0], (String) elem[1], (String) elem[2]));
            }

            return cfList;

        } catch (NoResultException e) {
            LOGGER.info("getContractFieldByContractNo is null");
            return null;
        }
    }

    public List<Users> getUsers() {
        LOGGER.info("in getUsers");
        try {
            List<Users> userList = new ArrayList<>();

            List<Object[]> rawList;

            rawList = em.createNativeQuery("select a.userMemberId, a.userName, a.lockedOut, a.IsFiltered, a.dealerIdFk, a.dealerGroupIdFk, a.agentIdFk, b.accountKeeperName, b.active, b.emailAddress, c.description, b.emailOptIn, b.systemCode, b.taxId from UserMember a, AccountKeeper b, UserType c where b.active=1 and a.userMemberId=b.accountKeeperId and a.userTypeInd=c.userTypeInd order by a.userMemberId desc").getResultList();
            Integer dealerIdFk;
            Integer dealerGroupIdFk;
            Integer agentIdFk;
            String company;
            String group = "";
            String phone = "";

            for (Object[] elem : rawList) {
                dealerIdFk = (Integer) elem[4];
                dealerGroupIdFk = (Integer) elem[5];
                agentIdFk = (Integer) elem[6];

                if (dealerIdFk != null) {
                    company = getAccountKeeperById(dealerIdFk).getAccountKeeperName();
                } else if (dealerGroupIdFk != null) {
                    company = getAccountKeeperById(dealerGroupIdFk).getAccountKeeperName();
                } else if (agentIdFk != null) {
                    company = getAccountKeeperById(agentIdFk).getAccountKeeperName();
                } else {
                    company = "";
                }
                userList.add(new Users((Integer) elem[0], (String) elem[7], (String) elem[1], (String) elem[10], company, group, (String) elem[9], phone, (Boolean) elem[8], (Boolean) elem[3], (Boolean) elem[2], (Boolean) elem[11], (String) elem[12], (String) elem[13]));
            }

            LOGGER.info("size of userList=" + userList.size());

            return userList;

        } catch (NoResultException e) {
            LOGGER.info("getUsers is null");
            return null;
        }
    }

    public RateVisibility getRateVisibilityById(Integer id) {
        try {
            return em.createNamedQuery("RateVisibility.findByRateVisibilityId", RateVisibility.class).setParameter("rateVisibilityId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getRateVisibilityById is null");
            return null;
        }
    }

    public List<RateVisibility> getRateVisibilityByAll() {
        try {
            return em.createNamedQuery("RateVisibility.findAll", RateVisibility.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getRateVisibilityByAll is null");
            return null;
        }
    }

    public CfRemitRule getCfRemitRuleById(Integer id) {
        try {
            return em.createNamedQuery("CfRemitRule.findByRemitRuleId", CfRemitRule.class).setParameter("remitRuleId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCfRemitRuleById is null");
            return null;
        }
    }

    public CfRemitRule getCfRemitRuleByDesc(String desc) {
        try {
            LOGGER.info("desc=" + desc);
            return em.createNamedQuery("CfRemitRule.findByDescription", CfRemitRule.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCfRemitRuleBydesc is null");
            return null;
        }
    }

    public CfRemitRule getCfRemitRuleByName(String name) {
        try {
            return em.createNamedQuery("CfRemitRule.findByName", CfRemitRule.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCfRemitRuleByName is null");
            return null;
        }
    }

    public Address getAddressById(Integer id) {
        try {
            return em.createNamedQuery("Address.findByAddressId", Address.class).setParameter("addressId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAddressById is null");
            return null;
        }
    }

    public List<Dealer> getDealerByAll() {
        try {
            return (List<Dealer>) em.createNativeQuery("select * from Dealer order by dealerId desc", Dealer.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDealerByAll is null");
            return null;
        }
    }

    public List<Dealer> getDealerByGroupFkId(Integer id) {
        try {
            return (List<Dealer>) em.createNativeQuery("select * from Dealer where dealerGroupIdFk=?1", Dealer.class).setParameter(1, id).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDealerGroupByFkId is null");
            return null;
        }
    }

    public List<DealerGroup> getDealerGroupByAll() {
        try {
            return (List<DealerGroup>) em.createNativeQuery("select * from DealerGroup order by dealerGroupId desc", DealerGroup.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDealerGroupByAll is null");
            return null;
        }
    }

    public List<Dealer> getDealerByRVFkId(Integer id) {
        try {
            return (List<Dealer>) em.createNativeQuery("select * from Dealer where rateVisibilityIdFk=?1", Dealer.class).setParameter(1, id).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDealerByRVFkId is null");
            return null;
        }
    }

    public List<CfRemitRule> getCfRemitRuleByAll() {
        try {
            return (List<CfRemitRule>) em.createNativeQuery("select * from CfRemitRule order by remitRuleId", CfRemitRule.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getRemitRuleByAll is null");
            return null;
        }
    }

    public List<Dealers> getDealers() {

        LOGGER.info("in getDealers");

        try {

            List<Dealers> dealersList = new ArrayList<>();

            List<Dealer> dealers = getDealerByAll();

            for (Dealer dealer : dealers) {
                String dealerName = "";
                String dealerNumber = "";
                String directAgent = "";
                String dealerGroup = "";
                String remitRule = "";
                String rateVisibility = "";
                String phoneNumber = "";
                String dealerType = "";
                Boolean isActive = true;
                String prefMethodPay = "";

                AccountKeeper ak = getAccountKeeperById(dealer.getDealerId());
                /*
                if (!ak.getActive()) {
                    continue;
                }
                 */
                dealerName = ak.getAccountKeeperName();
                if (ak.getRemitRuleIdFk() != null) {
                    remitRule = getCfRemitRuleById(ak.getRemitRuleIdFk().getRemitRuleId()).getDescription();
                }
                if (ak.getPhysicalAddressIdFk() != null) {
                    phoneNumber = ak.getPhysicalAddressIdFk().getPhoneNumber();
                }
                dealerNumber = dealer.getDealerNumber();
                if (dealer.getAgentIdFk() != null) {
                    directAgent = getAccountKeeperById(dealer.getAgentIdFk().getAgentId()).getAccountKeeperName();
                }
                if (dealer.getDealerGroupIdFk() != null) {
                    dealerGroup = getAccountKeeperById(dealer.getDealerGroupIdFk().getDealerGroupId()).getAccountKeeperName();
                }
                if (dealer.getRateVisibilityIdFk() != null) {
                    rateVisibility = getRateVisibilityById(dealer.getRateVisibilityIdFk().getRateVisibilityId()).getDescription();
                }
                if (dealer.getDealerTypeIdFk() != null) {
                    dealerType = getDtDealerTypeById(dealer.getDealerTypeIdFk()).getName();
                }
                isActive = ak.getActive();

                dealersList.add(new Dealers(dealer.getDealerId(), dealerName, dealerNumber, directAgent, dealerGroup, remitRule, rateVisibility, phoneNumber, ak.getTaxId(), ak.getEmailAddress(), prefMethodPay, dealerType, isActive));

            }

            LOGGER.info("size of dealersList=" + dealersList.size());

            return dealersList;

        } catch (Exception e) {
            LOGGER.info("getDealers is null");
            return null;
        }

    }

    public List<Dealers> getDealersByGroupFkId(Integer id) {

        LOGGER.info("in getDealersByGroupFkId");

        try {

            List<Dealers> dealersList = new ArrayList<>();

            String dealerName = "";
            String dealerNumber = "";
            String directAgent = "";
            String dealerGroup = "";
            String remitRule = "";
            String rateVisibility = "";
            String phoneNumber = "";

            List<Dealer> dealers = getDealerByGroupFkId(id);

            for (Dealer dealer : dealers) {
                AccountKeeper ak = getAccountKeeperById(dealer.getDealerId());
                if (!ak.getActive()) {
                    continue;
                }
                dealerName = ak.getAccountKeeperName();
                if (ak.getRemitRuleIdFk() != null) {
                    remitRule = getCfRemitRuleById(ak.getRemitRuleIdFk().getRemitRuleId()).getDescription();
                }
                if (ak.getPhysicalAddressIdFk() != null) {
                    phoneNumber = ak.getPhysicalAddressIdFk().getPhoneNumber();
                }
                dealerNumber = dealer.getDealerNumber();
                if (dealer.getAgentIdFk() != null) {
                    directAgent = getAccountKeeperById(dealer.getAgentIdFk().getAgentId()).getAccountKeeperName();
                }
                if (dealer.getDealerGroupIdFk() != null) {
                    dealerGroup = getAccountKeeperById(dealer.getDealerGroupIdFk().getDealerGroupId()).getAccountKeeperName();
                }
                if (dealer.getRateVisibilityIdFk() != null) {
                    rateVisibility = getRateVisibilityById(dealer.getRateVisibilityIdFk().getRateVisibilityId()).getDescription();
                }
                dealersList.add(new Dealers(dealer.getDealerId(), dealerName, dealerNumber, directAgent, dealerGroup, remitRule, rateVisibility, phoneNumber));

            }

            LOGGER.info("size of dealersList=" + dealersList.size());

            return dealersList;

        } catch (Exception e) {
            LOGGER.info("getDealers is null");
            return null;
        }

    }

    public List<DealerGroups> getDealerGroups() {

        LOGGER.info("in getDealerGroups");

        try {

            List<DealerGroups> dgList = new ArrayList<>();

            String dgName = "";
            String remitRule = "";
            String phoneNumber = "";

            List<DealerGroup> dgs = getDealerGroupByAll();

            for (DealerGroup dg : dgs) {
                AccountKeeper ak = getAccountKeeperById(dg.getDealerGroupId());
                /*
                if (!ak.getActive()) {
                    continue;
                }
                 */
                dgName = ak.getAccountKeeperName();
                if (ak.getRemitRuleIdFk() != null) {
                    remitRule = getCfRemitRuleById(ak.getRemitRuleIdFk().getRemitRuleId()).getDescription();
                }
                if (ak.getPhysicalAddressIdFk() != null) {
                    phoneNumber = ak.getPhysicalAddressIdFk().getPhoneNumber();
                }

                dgList.add(new DealerGroups(dg.getDealerGroupId(), dgName, remitRule, phoneNumber, ak.getActive()));

            }

            LOGGER.info("size of dgList=" + dgList.size());

            return dgList;

        } catch (Exception e) {
            LOGGER.info("getDealerGroups is null");
            return null;
        }

    }

    public List<AccountKeeper> getAccountKeeperByCustomerType(int first, int pageSize) {
        LOGGER.info("in getAccountKeeperByCustomerType");
        try {
            //Query query = em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=6 and active=1 and accountKeeperName like '%test 2%'", AccountKeeper.class);
            Query query = em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=6 and active=1  order by accountKeeperId desc", AccountKeeper.class);
            LOGGER.info("111111111111");
            query.setFirstResult(first);
            query.setMaxResults(pageSize);
            List<AccountKeeper> list;
            list = (List<AccountKeeper>) query.getResultList();
            LOGGER.info("size = " + list.size());
            return list;
            //return (List<AccountKeeper>) em.createNativeQuery("select * from AccountKeeper where accountKeeperTypeIdFk=6", AccountKeeper.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByCustomerType is null");
            return null;
        }
    }

    public int getAKByCTCount() {
        try {
            Query query = em.createNativeQuery("select count(*) from AccountKeeper where accountKeeperTypeIdFk=6 and active=1");
            Integer count = (Integer) query.getSingleResult();
            LOGGER.info("count=" + count);

            return count;

        } catch (NoResultException e) {
            LOGGER.info("getAKByCTCount is null");
            return 0;
        }
    }

    /**
     **** Misc
     */
    /**
     *
     * @param s
     * @return Date
     */
    /*
    return own Object from query
     */
    public List<RFSearch> getRFSearch(RFSearch rfs) {
        try {
            List<RFSearch> rfList = new ArrayList<>();
            String qs = "select a.accountKeeperId, a.accountKeeperName,  c.phoneNumber, c.city, d.regionCode, c.zipCode, b.laborTaxPct, b.partsTaxPct, b.laborRate, c.address1, c.address2 from AccountKeeper a, RepairFacility b, Address c, CfRegion d where a.accountKeeperId=b.repairFacilityId and a.physicalAddressIdFk=c.addressId and c.regionIdFk=d.regionId  ";
            String astr = "";
            String pstr = "";
            String cstr = "";
            String rstr = "";
            String zstr = "";
            String lbtstr = "";
            String ptstr = "";
            String lbrstr = "";

            if (rfs.getRfName() != null) {
                astr = "  and a.accountKeeperName like '%" + rfs.getRfName() + "%'  ";
            }
            if (rfs.getRfPhone() != null) {
                pstr = " and c.phoneNumber like '%" + rfs.getRfPhone() + "%'  ";
            }
            if (rfs.getCity() != null) {
                cstr = " and c.city like '%" + rfs.getCity() + "%'  ";
            }
            if (rfs.getState() != null) {
                rstr = " and d.regionCode like '%" + rfs.getState() + "%'  ";
            }
            if (rfs.getZip() != null) {
                zstr = " and c.zipCode like '%" + rfs.getZip() + "%'  ";
            }
            if (rfs.getZip() != null) {
                zstr = " and c.zipCode like '%" + rfs.getZip() + "%'  ";
            }
            if (rfs.getLaborTax() != null) {
                lbtstr = " and b.laborTaxPct like '%" + rfs.getLaborTax() + "%'  ";
            }
            if (rfs.getPartTax() != null) {
                ptstr = " and b.partsTaxPct like '%" + rfs.getPartTax() + "%'  ";
            }
            if (rfs.getLaborRate() != null) {
                lbrstr = " and b.laborRate like '%" + rfs.getLaborRate() + "%' ";
            }

            String endstr = " order by a.accountKeeperName";

            String query = qs + astr + pstr + cstr + rstr + zstr + lbtstr + ptstr + lbrstr + endstr;

            LOGGER.info("query=" + query);

            List<Object[]> rawRFList;

            rawRFList = em.createNativeQuery(query).getResultList();
            for (Object[] elem : rawRFList) {
                //LOGGER.info("id=" + (Integer) elem[0]);
                //LOGGER.info("name=" + (String)elem[1]);

                rfList.add(new RFSearch((Integer) elem[0], (String) elem[1], (String) elem[2], (String) elem[3], (String) elem[4], (String) elem[5], (BigDecimal) elem[6], (BigDecimal) elem[7], (BigDecimal) elem[8], (String) elem[9], (String) elem[10]));
            }

            return rfList;
        } catch (NoResultException e) {
            LOGGER.info("getProductTypeByClaimNumber is null");
            return null;
        }
    }

    public List<MemberSearch> getMemberSearch(MemberSearch ms) {
        try {
            List<MemberSearch> msList = new ArrayList<>();
            String qs = "select a.userMemberId, a.userName, b.accountKeeperId, b.accountKeeperName from UserMember a, AccountKeeper b where a.userMemberId = b.accountKeeperId   and b.active=1 ";
            String lnstr = "";
            String snstr = "";

            if (ms.getLongName() != null && ms.getLongName().length() > 0) {
                lnstr = "  and b.accountKeeperName like '%" + ms.getLongName() + "%'  ";
            }
            if (ms.getShortName() != null && ms.getShortName().length() > 0) {
                snstr = " and a.userName like '%" + ms.getShortName() + "%'  ";
            }

            String endstr = " order by a.userName";

            String query = qs + lnstr + snstr + endstr;

            LOGGER.info("query=" + query);

            List<Object[]> rawMSList;

            rawMSList = em.createNativeQuery(query).getResultList();
            for (Object[] elem : rawMSList) {
                //LOGGER.info("id=" + (Integer) elem[0]);
                //LOGGER.info("name=" + (String)elem[1]);
                msList.add(new MemberSearch((Integer) elem[0], (String) elem[1], (Integer) elem[2], (String) elem[3]));
            }

            return msList;
        } catch (NoResultException e) {
            LOGGER.info("getProductTypeByClaimNumber is null");
            return null;
        }
    }

    /**
     *
     * @param cs
     * @return List
     */
    public List<Claim> searchClaims(ClaimSearch cs) {
        try {
            List<Claim> cl = new ArrayList<>();

            String qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerId=e.dealerId and  a.productTypeIdFk=g.productTypeId ";
            LOGGER.info("qs=" + qs);
            String vinStr = "";
            String claimStr = "";
            String assignedToStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                LOGGER.info("vin=" + cs.getVinFull());
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }
            if (cs.getClaimNumber() != null && cs.getClaimNumber().length() > 0) {
                LOGGER.info("claimNumber=" + cs.getClaimNumber());
                claimStr = " and a.claimNumber= '" + cs.getClaimNumber() + "'  ";
            }
            if (cs.getUserName() != null && cs.getUserName().length() > 0) {
                qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e, UserMember f, ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerId=e.dealerId and a.ownedByFk=f.userMemberId and a.productTypeIdFk=g.productTypeId ";
                assignedToStr = " and f.userName= '" + cs.getUserName() + "'  ";
            }
            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                LOGGER.info("effect date=" + cs.getEffectiveDate());
                String sd = getJavaDate(cs.getEffectiveDate());
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = getJavaDate(cs.getContractExpirationDate());
                endStr = " and b.expirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select accountKeeperId from AccountKeeper where accountKeeperName=?1 and  accountHolderTypeInd=1").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by a.claimId desc";
            LOGGER.info("endstr=" + endstr);

            String query = qs + vinStr + claimStr + assignedToStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr + endstr;

            LOGGER.info("query=" + query);

            // new
            Query q = em.createNativeQuery(query, Claim.class);
            /*
            q.setFirstResult(0);
            q.setMaxResults(35);
             */
            return (List<Claim>) q.getResultList();

            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchClaims is null");
            return null;
        }
    }

    public int getSearchClaimsCount(ClaimSearch cs) {
        try {
            List<Claim> cl = new ArrayList<>();

            String qs = "select count(*) from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and a.productTypeIdFk=g.productTypeId ";
            String vinStr = "";
            String claimStr = "";
            String assignedToStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }
            if (cs.getClaimNumber() != null && cs.getClaimNumber().length() > 0) {
                LOGGER.info("claimNumber=" + cs.getClaimNumber());
                claimStr = " and a.claimNumber= '" + cs.getClaimNumber() + "'  ";
            }
            if (cs.getUserName() != null && cs.getUserName().length() > 0) {
                qs = "select count(*) from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e, UserMember f, ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and a.ownedByFk=f.userMemberId and a.productTypeIdFk=g.productTypeId ";
                assignedToStr = " and f.userName= '" + cs.getUserName() + "'  ";
            }
            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                String sd = getJavaDate(cs.getEffectiveDate());
                LOGGER.info("effectiveDate=" + cs.getEffectiveDate());
                LOGGER.info("sd=" + sd);
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = getJavaDate(cs.getContractExpirationDate());
                LOGGER.info("ed=" + ed);
                endStr = " and b.expirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Dealer'").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by a.claimId desc";

            String query = qs + vinStr + claimStr + assignedToStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr;

            LOGGER.info("query=" + query);

            // new
            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;

            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchClaims is null");
            return 0;
        }
    }

    public List<Claim> searchClaims(ClaimSearch cs, int first, int pageSize) {
        try {
            List<Claim> cl = new ArrayList<>();

            String qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and  a.productTypeIdFk=g.productTypeId ";

            //LOGGER.info("qs=" + qs);
            String vinStr = "";
            String claimStr = "";
            String assignedToStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";

            if (cs.getUserName() != null && cs.getUserName().length() > 0) {
                qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e, UserMember f, ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and a.ownedByFk=f.userMemberId and a.productTypeIdFk=g.productTypeId ";
                assignedToStr = " and f.userName= '" + cs.getUserName() + "'  ";
            }

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                LOGGER.info("vin=" + cs.getVinFull());
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }
            if (cs.getClaimNumber() != null && cs.getClaimNumber().length() > 0) {
                LOGGER.info("claimNumber=" + cs.getClaimNumber());
                claimStr = " and a.claimNumber= '" + cs.getClaimNumber() + "'  ";
            }

            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                LOGGER.info("effect date=" + cs.getEffectiveDate());
                String sd = getJavaDate(cs.getEffectiveDate());
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = getJavaDate(cs.getContractExpirationDate());
                endStr = " and b.expirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Dealer'").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by a.claimId desc";
            //LOGGER.info("endstr=" + endstr);

            String query = qs + vinStr + claimStr + assignedToStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr + endstr;

            LOGGER.info("query=" + query);

            // new
            if (em == null) {
                LOGGER.info("em is null, why");
            }

            Query q = em.createNativeQuery(query, Claim.class);

            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return (List<Claim>) q.getResultList();
            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchClaims is null");
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<ContractNote> getContractNotesByContractNo(String no) {
        try {
            LOGGER.info("getContractNotesByContractNo=" + no);
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select  b.claimNumber, c.noteId, c.note, c.enteredByIdFk, c.enteredDate from Contracts a, Claim b, Note c where contractNo=?1 and a.contractId=b.contractIdFk and b.claimId=c.claimIdFk order by b.claimNumber").setParameter(1, no).getResultList();

            List<ContractNote> cList = new ArrayList<>();
            for (Object[] elem : rawList) {

                cList.add(new ContractNote((Integer) elem[1], (String) elem[0], (String) elem[2], getAccountKeeperById((Integer) elem[3]).getAccountKeeperName(), (Timestamp) elem[4]));

            }
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getContractNotesByContractNo is null");
            return null;
        }
    }

    public boolean isVSC(Claim claim) {
        if (claim.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("VSC") != 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isPPM(Claim claim) {
        if (claim.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("PPM") != 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isANC(Claim claim) {
        if (claim.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("ANC") != 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isGAP(Claim claim) {
        if (claim.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("GAP") != 0) {
            return false;
        } else {
            return true;
        }
    }

    public String getClaimType(Claim claim) {
        return claim.getProductTypeIdFk().getProductTypeName();
    }

    public String getClaimType(Contracts contract) {
        return contract.getProductIdFk().getProductTypeIdFk().getProductTypeName();
    }

    public boolean isVSC(Contracts contract) {
        if (contract.getProductIdFk().getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("VSC") == 0) {
            return true;
        }
        return false;
    }

    public boolean isPPM(Contracts contract) {
        if (contract.getProductIdFk().getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("PPM") == 0) {
            return true;
        }
        return false;
    }

    public boolean isANC(Contracts contract) {
        if (contract.getProductIdFk().getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("ANC") == 0) {
            return true;
        }
        return false;
    }

    public boolean isGAP(Contracts contract) {
        if (contract.getProductIdFk().getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("GAP") == 0) {
            return true;
        }
        return false;
    }

    public void approvedClaim(Claim claim) {
        try {

            LOGGER.info("in approvedClaim, claimId=" + claim.getClaimId());
            utx.begin();

            claim.setClaimStatus(getClaimStatusByDesc("Approved"));

            em.merge(claim);

            // add to Ledger table
            // now insert to Ledger 
            Integer ledgerTypeInd = 14; // claim
            Integer exemptInd = 0;  // later, meaning?
            Integer adminCompanyFkId = 1;  // later 

            AccountKeeper enteredByFkId = getAccountKeeperById(getUserMemberByName(getCurrentUser()).getUserMemberId());

            LOGGER.info("enteredByFkId = " + enteredByFkId);

            Query insQuery = em.createNativeQuery("insert into Ledger ("
                    + "amount, "
                    + "currentAmount, "
                    + "enteredDate, "
                    + "ledgerDate, "
                    + "ledgerTypeInd, "
                    + "relatedLedgerTypeInd, "
                    + "setLedgerDate, "
                    + "exemptInd, "
                    + "adminCompanyIdFk, "
                    + "CurrentAccountKeeperIdFk, "
                    + "originalAccountKeeperIdFk, "
                    + "lowersAdminCompanyBalanceInd, "
                    + "relatedContractIdFk, "
                    + "enteredByIdFk  ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14 )");

            insQuery.setParameter(1, claim.getAmount());
            insQuery.setParameter(2, claim.getAmount());
            insQuery.setParameter(3, getCurrentDate());
            insQuery.setParameter(4, getCurrentDate());
            insQuery.setParameter(5, ledgerTypeInd);
            insQuery.setParameter(6, ledgerTypeInd);
            insQuery.setParameter(7, getCurrentDate());
            insQuery.setParameter(8, exemptInd);
            insQuery.setParameter(9, adminCompanyFkId);
            insQuery.setParameter(10, claim.getPaidToFk().getAccountKeeperId());
            insQuery.setParameter(11, claim.getPaidToFk().getAccountKeeperId());
            insQuery.setParameter(12, 0);
            insQuery.setParameter(13, claim.getContractIdFk().getContractId());
            insQuery.setParameter(14, enteredByFkId.getAccountKeeperId());

            Integer retIns = insQuery.executeUpdate();
            LOGGER.info("Insert into Ledger successful, retIns = " + retIns);

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void voidedClaim(Claim claim) {
        try {
            utx.begin();

            claim.setClaimSubStatus(getClaimSubStatusByDesc("Voided"));

            em.merge(claim);

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deniedClaim(Claim claim) {
        try {
            utx.begin();

            claim.setClaimSubStatus(getClaimSubStatusByDesc("Denied"));

            em.merge(claim);

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void pendingClaim(Claim claim) {
        try {
            utx.begin();

            claim.setClaimStatus(getClaimStatusByDesc("Pending"));

            em.merge(claim);

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public List<TermDetail> getTermDetailByFk(Integer id) {
        try {
            LOGGER.info("in getTermDetailByFk, term id=" + id);
            return (List<TermDetail>) em.createNativeQuery("select b.* from Term a, TermDetail b where a.TermId=b.termIdFk and a.TermId=?1", TermDetail.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getTermDetailByFk is null");
            return null;
        }
    }

    public TermDetail getTermDetailByTermIdFk(Integer id) {
        try {
            return (TermDetail) em.createNativeQuery("select b.* from Term a, TermDetail b where a.TermId=b.termIdFk and a.TermId=?1 order by termDetailId desc", TermDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getTermDetailByTermIdFk is null");
            return null;
        }
    }

    public LimitDetail getLimitDetailByLimitIdFk(Integer id) {
        try {
            return (LimitDetail) em.createNativeQuery("select b.* from Limit a, LimitDetail b where a.limitId=b.limitIdFk and a.limitId=?1 order by limitDetailId desc", LimitDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getLimitDetailByLimitIdFk is null");
            return null;
        }
    }

    public Deductible getDeductibleById(Integer id) {
        try {
            return em.createNamedQuery("Deductible.findByDeductibleId", Deductible.class).setParameter("deductibleId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDeductibleById is null");
            return null;
        }
    }

    public List<ClaimPPMTbl> getPPMTblList(String contractNo) {
        try {
            LOGGER.info("in getPPMTblList, contractNo=" + contractNo);
            Contracts contract = getContractsByNo(contractNo);
            List<ClaimPPMTbl> cptl = new ArrayList<>();
            List<ContractPPMDetail> cppmdList = (List<ContractPPMDetail>) em.createNativeQuery("select b.* from Contracts a, ContractPPMDetail b where a.contractId=b.contractIdFk and a.contractNo=?1", ContractPPMDetail.class).setParameter(1, contractNo).getResultList();

            LOGGER.info("in getPPMTblList, size of ContractPPMDetail=" + cppmdList.size());
            //BigDecimal cost;
            String claimNumber;
            String roNumber;
            for (ContractPPMDetail cppmd : cppmdList) {
                //cost = cppmd.getCost();
                claimNumber = null;
                roNumber = null;
                List<Claim> claims = (List<Claim>) em.createNativeQuery("select a.* from Claim a, ClaimEventPPM b, ContractPPMDetail c where  c.contractPPMDetailId=?1 and c.contractPPMDetailId=b.contractPPMDetailIdFk and a.claimId=b.claimIdFk", Claim.class).setParameter(1, cppmd.getContractPPMDetailId()).getResultList();

                if (claims.size() > 0) {

                    //cost = claims.get(0).getAmount();
                    claimNumber = claims.get(0).getClaimNumber();
                    roNumber = claims.get(0).getRepairOrderNumber();
                    LOGGER.info("claims: detailId=" + cppmd.getContractPPMDetailId());
                    LOGGER.info("claims: claimNumber=" + claimNumber);
                    LOGGER.info("claims: roNumber=" + roNumber);
                }

                cptl.add(new ClaimPPMTbl(
                        cppmd.getContractPPMDetailId(),
                        contractNo,
                        contract.getContractId(),
                        cppmd.getDescription(),
                        (cppmd.getLowerDate() != null ? getJavaDate(cppmd.getLowerDate()) : null),
                        (cppmd.getUpperDate() != null ? getJavaDate(cppmd.getUpperDate()) : null),
                        "OR",
                        cppmd.getLowerMile(),
                        cppmd.getUpperMile(),
                        cppmd.getCost(),
                        claimNumber,
                        roNumber
                ));
            }
            LOGGER.info("cptl size=" + cptl.size());
            return cptl;

        } catch (NoResultException e) {
            LOGGER.info("getPPMTblList is null");
            return null;
        }
    }

    public ContractPPMDetail getContractPPMDetailById(Integer id) {
        try {
            return em.createNamedQuery("ContractPPMDetail.findByContractPPMDetailId", ContractPPMDetail.class).setParameter("contractPPMDetailId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractPPMDetailById is null");
            return null;
        }
    }

    public Integer getMaxOdometerByClaimNumber(String claimNumber) {
        try {
            return (Integer) em.createNativeQuery("select max(currentOdometer) from Claim where contractIdFk in ( select contractIdFk from Claim where claimNumber = ?1 )").setParameter(1, claimNumber).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getMaxOdometerByClaimNumber is null");
            return null;
        }
    }

    public Integer getMaxOdometerByContractNo(String contractNo) {
        try {
            Contracts contract = getContractsByNo(contractNo);
            Integer count = (Integer) em.createNativeQuery("select count(*) from Claim where claim.contractIdFk=?1").setParameter(1, contract.getContractId()).getSingleResult();
            LOGGER.info("count=" + count);
            if (count == 0) {
                return contract.getOdometer();
            } else {
                return (Integer) em.createNativeQuery("select max(currentOdometer) from Contracts a, Claim b where a.contractId=b.contractIdFk and a.contractNo = ?1").setParameter(1, contractNo).getSingleResult();
            }
        } catch (NoResultException e) {
            LOGGER.info("getMaxOdometerByContractNo is null");
            return null;
        }
    }

    public List<Dealer> getDealerByContractNo(String contractNo) {
        try {
            return (List<Dealer>) em.createQuery("select b from  Contracts a join a.dealerIdFk b where  a.contractNo :num").setParameter("num", contractNo).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDealerByContractNo is null");
            return null;
        }
    }

    public List<CfRegion> getRegionByAll() {
        try {
            return (List<CfRegion>) em.createNamedQuery("CfRegion.findAll", CfRegion.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getRegionByAll is null");
            return null;
        }
    }

    public CfRegion getRegionByRegionCode(String code) {
        try {
            return em.createNamedQuery("CfRegion.findByRegionCode", CfRegion.class).setParameter("regionCode", code).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getRegionByRegionCode is null");
            return null;
        }
    }

    public CfRegion getRegionById(Integer id) {
        try {
            return em.createNamedQuery("CfRegion.findByRegionId", CfRegion.class).setParameter("regionId", id).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getRegionById is null");
            return null;
        }
    }

    public Integer getAccountKeeperNameCount(String name) {
        try {
            return (Integer) em.createNativeQuery("select count(*) from AccountKeeper where accountKeeperName =  ?1").setParameter(1, name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperNameCount is null");
            return null;
        }
    }

    // contract
    public List<Contracts> searchContracts(ContractSearch cs, int first, int pageSize) {
        try {
            List<Contracts> cl = new ArrayList<>();

            String qs = "select b.* from Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g, Product f where b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and  b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and b.contractStatusIdFk!=6 ";

            //LOGGER.info("qs=" + qs);
            String vinStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";
            String agentStr = "";

            if (cs.getAgent() != null && cs.getAgent().length() > 0) {
                Integer agentId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Agent'").setParameter(1, cs.getAgent()).getSingleResult();
                qs = "select b.* from Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g, Product f, Agent h where b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and  b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and b.contractStatusIdFk!=6 and b.agentIdFk=h.agentId ";
                agentStr = " and h.agentId= " + agentId + "  ";
            }

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                LOGGER.info("vin=" + cs.getVinFull());
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }

            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                LOGGER.info("effect date=" + cs.getEffectiveDate());
                String sd = getJavaDate(cs.getEffectiveDate());
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = getJavaDate(cs.getContractExpirationDate());
                endStr = " and b.expirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Dealer'").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by b.contractId desc";
            //LOGGER.info("endstr=" + endstr);

            String query = qs + vinStr + agentStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr + endstr;

            LOGGER.info("query=" + query);

            // new
            if (em == null) {
                LOGGER.info("em is null, why");
            }

            Query q = em.createNativeQuery(query, Contracts.class);

            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return (List<Contracts>) q.getResultList();
            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchContracts is null");
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public int getSearchContractsCount(ContractSearch cs) {
        try {
            List<Contracts> cl = new ArrayList<>();

            String qs = "select count(*) from Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g, Product f where b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and  b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and b.contractStatusIdFk!=6 ";
            String vinStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";
            String agentStr = "";

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }

            if (cs.getAgent() != null && cs.getAgent().length() > 0) {
                Integer agentId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Agent'").setParameter(1, cs.getAgent()).getSingleResult();
                qs = "select count(*) from Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g, Product f, Agent h where b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and  b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerIdFk=e.dealerId and b.productIdFk=f.productId and f.productTypeIdFk=g.productTypeId and b.contractStatusIdFk!=6 and b.agentIdFk=h.agentId ";
                agentStr = " and h.agentId= " + agentId + "  ";
            }

            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                String sd = getJavaDate(cs.getEffectiveDate());
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = getJavaDate(cs.getContractExpirationDate());
                endStr = " and b.expirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select a.accountKeeperId from AccountKeeper a, AccountKeeperType b where accountKeeperName=?1 and  a.accountKeeperTypeIdFk=b.AccountKeeperTypeId and b.AccountKeeperTypeDesc='Dealer'").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by b.contractId desc";

            String query = qs + vinStr + agentStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr;

            LOGGER.info("query=" + query);

            // new
            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;

            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getSearchContractsCount is null");
            return 0;
        }
    }

    public DriveType getDriveTypeById(int id) {
        try {
            return (DriveType) em.createNamedQuery("DriveType.findByDriveTypeId", DriveType.class).setParameter("driveTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDriveTypeById is null");
            return null;
        }
    }

    public List<ProgramSearch> getProgramSearchs() {
        try {
            LOGGER.info("in getProgramSearchs");
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select distinct a.name, a.code, c.disbursementType, b.effectiveDate, a.programId, b.description from Program a, ProgramDetail b,  DisbursementCenter c where a.programId=b.programIdFk and  ( b.disbursementCenterIdFk is not null and  b.disbursementCenterIdFk=c.disbursementCenterId ) order by a.name").getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<ProgramSearch> cList = new ArrayList<>();

            String adminCompany = "Dealer Owned";
            String expireDate = "";

            List<Integer> listI = new ArrayList<>();

            Integer programId = null;
            for (Object[] elem : rawList) {
                programId = (Integer) elem[4]; // has to deal with bad data in ProgradDetail
                if (!listI.contains(programId)) {
                    listI.add(programId);
                    cList.add(new ProgramSearch(programId, (String) elem[0], (String) elem[1], (String) elem[2], adminCompany, ((Date) elem[3]), expireDate, (String) elem[5]));
                }
            }
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getProgramSearchs is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getProgramSearchs: " + ex.getMessage());
            return null;
        }
    }

    public List<VINClassCode> getVINClassCodeByAll() {
        try {
            return (List<VINClassCode>) em.createNamedQuery("VINClassCode.findAll", VINClassCode.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getVINClassCodeByAll is null");
            return null;
        }
    }

    public List<Program> getProgramByAll() {
        try {
            return (List<Program>) em.createNamedQuery("Program.findAll", Program.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getProgramByAll is null");
            return null;
        }
    }

    public Program getProgramByName(String name) {
        try {
            return (Program) em.createNamedQuery("Program.findByName", Program.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProgramByName is null");
            return null;
        }
    }

    public Program getProgramByCode(String code) {
        try {
            return (Program) em.createNamedQuery("Program.findByCode", Program.class).setParameter("code", code).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProgramByCode is null");
            return null;
        }
    }

    public Program getProgramById(Integer id) {
        try {
            return (Program) em.createNamedQuery("Program.findByProgramId", Program.class).setParameter("programId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProgramById is null");
            return null;
        }
    }

    public ProgramDetail getProgramDetailByFkId(Integer id) {
        try {
            return (ProgramDetail) em.createNativeQuery("select * from ProgramDetail where programIdFk=?1", ProgramDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getProgramDetailByFkId is null");
            return null;
        }
    }

    public void deleteProgram(Integer id) {
        try {
            LOGGER.info("in deleteProgram, programId=" + id);

            utx.begin();

            Program program = getProgramById(id);
            ProgramDetail pd = getProgramDetailByFkId(id);

            em.remove(pd);
            em.remove(program);

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();
            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deleteUserMemeber(Integer id) {
        deleteAccountKeeper(id);
    }

    public void deleteDealer(Integer id) {
        deleteAccountKeeper(id);
    }

    public void deleteCustomer(Integer id) {
        deleteAccountKeeper(id);
    }

    public void deleteAccountKeeper(Integer id) {
        try {
            LOGGER.info("in deleteAccountKeeper, id=" + id);

            utx.begin();
            /*
            UserMember user = getUserMemberById(id);
            AccountKeeper ak = getAccountKeeperById(id);
            ProgramDetail pd = getProgramDetailByFkId(id);

            em.remove(ak);
            em.remove(user);
             */
            AccountKeeper ak = getAccountKeeperById(id);
            ak.setActive(false);
            em.merge(ak);
            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();
            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public List<ProductSearch> getProductSearchs() {
        try {
            LOGGER.info("in getProductSearchs");
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select distinct  b.productId, b.productName, b.productCode, c.productTypeName, e.accountKeeperName, d.effectiveDate, a.name, d.description,  g.Description, h.disbursementType "
                    + "from Program a, Product b, ProductType c, ProductDetail d, AccountKeeper e, Insurer f,  ProductSubType g,  DisbursementCenter h "
                    + "where a.programId=b.programIdFk and b.productTypeIdFk=c.productTypeId and b.productId=d.productIdFk and b.insurerIdFk=f.insurerId and f.insurerId=e.accountKeeperId and b.productSubTypeFk=g.ProductSubTypeId and d.disbursementCenterIdFk=h.disbursementCenterId order by d.effectiveDate desc").getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<ProductSearch> cList = new ArrayList<>();

            String expireDate = ""; // later
            String adminCompany = "DOWC";     //later
            boolean financeBasedTerm = false;
            boolean mileageBasedTerm = false;
            boolean isRenewable = false;
            String claimSetting = "";
            String salesGroup = "";
            String contractForm = "";

            List<Integer> listI = new ArrayList<>();

            Integer productId = null;
            for (Object[] elem : rawList) {
                productId = (Integer) elem[0]; // has to deal with bad data in ProductDetail
                if (!listI.contains(productId)) {
                    listI.add(productId);
                    cList.add(new ProductSearch(productId, (String) elem[1], (String) elem[2], (String) elem[3], (String) elem[8], (String) elem[9], adminCompany, (Date) elem[5], expireDate, (String) elem[4], (String) elem[6], financeBasedTerm, mileageBasedTerm, isRenewable, claimSetting, salesGroup, contractForm, (String) elem[7]));
                }
            }
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getProductSearchs is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getProductSearchs: " + ex.getMessage());
            return null;
        }
    }

    public List<ProductSearch> getProductSearchByProgramId(Integer id) {
        try {
            LOGGER.info("in getProductSearchByProgramId, programId=" + id);
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select distinct  b.productId, b.productName, b.productCode, c.productTypeName, e.accountKeeperName, d.effectiveDate, a.name, d.description, g.description,  h.disbursementType "
                    + "from Program a, Product b, ProductType c, ProductDetail d, AccountKeeper e, Insurer f, ProductSubType  g, DisbursementCenter h "
                    + "where a.programId=b.programIdFk and b.productTypeIdFk=c.productTypeId and b.productId=d.productIdFk and b.insurerIdFk=f.insurerId and f.insurerId=e.accountKeeperId and b.productSubTypeFk=g.ProductSubTypeId and d.disbursementCenterIdFk=h.disbursementCenterId and a.programId= ?1 order by d.effectiveDate desc").setParameter(1, id).getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<ProductSearch> cList = new ArrayList<>();

            String expireDate = ""; // later
            String disbursementType = "";     // later
            String adminCompany = "DOWC";     //later
            boolean financeBasedTerm = false;
            boolean mileageBasedTerm = false;
            boolean isRenewable = false;
            String claimSetting = "";
            String salesGroup = "";
            String contractForm = "";

            List<Integer> listI = new ArrayList<>();

            Integer productId = null;
            for (Object[] elem : rawList) {
                productId = (Integer) elem[0]; // has to deal with bad data in ProductDetail
                if (!listI.contains(productId)) {
                    listI.add(productId);
                    cList.add(new ProductSearch(productId, (String) elem[1], (String) elem[2], (String) elem[3], (String) elem[8], disbursementType, adminCompany, (Date) elem[5], expireDate, (String) elem[4], (String) elem[6], financeBasedTerm, mileageBasedTerm, isRenewable, claimSetting, salesGroup, contractForm, (String) elem[8]));
                }
            }
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getProductSearchs is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getProductSearchs: " + ex.getMessage());
            return null;
        }
    }

    public List<PlanSearch> getPlanSearchByProductId(Integer id) {
        try {
            LOGGER.info("in getPlanSearchByProductId, productId=" + id);
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select distinct  g.planId, g.planName, g.planCode, h.defaultDeductibleIdFk, h.effectiveDate, b.productName, b.productCode, c.productTypeName, e.accountKeeperName, a.name,  d.description, i.name "
                    + "from Program a, Product b, ProductType c, ProductDetail d, AccountKeeper e, Insurer f, PlanTable g, PlanDetail h, ClassTable i "
                    + "where a.programId=b.programIdFk and b.productTypeIdFk=c.productTypeId and b.productId=d.productIdFk and b.insurerIdFk=f.insurerId and f.insurerId=e.accountKeeperId and g.productIdFk=b.productId and h.planIdFk=g.planId  and h.classTableIdFk=i.classTableId and b.productId=?1 order by h.effectiveDate desc ").setParameter(1, id).getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<PlanSearch> cList = new ArrayList<>();

            String planType = "";     // later
            String vinClassingTable = "";
            Integer defaultDeductibleIdFk = null;
            String deductibleDescription = null;
            Date expireDate = null; // later
            String adminCompany = "DOWC";     //later
            String planSalesGroup = "";
            String disbursementType = "";
            String minimumTermMonths = "";
            String maxFinance = "";
            String vehiclePurchaseType = "";
            String availableFinanceType = "";

            List<Integer> listI = new ArrayList<>();

            Integer planId = null;
            for (Object[] elem : rawList) {
                planId = (Integer) elem[0]; // has to deal with bad data in ProductDetail
                vinClassingTable = (String) elem[11];
                if (!listI.contains(planId)) {
                    listI.add(planId);
                    if ((defaultDeductibleIdFk = (Integer) elem[3]) != null) {
                        deductibleDescription = em.createNamedQuery("Deductible.findByDeductibleId", Deductible.class).setParameter("deductibleId", defaultDeductibleIdFk).getSingleResult().getDescription();
                    }
                    cList.add(new PlanSearch(planId, (String) elem[1], (String) elem[2], planType, vinClassingTable, deductibleDescription, (Date) elem[4], expireDate, adminCompany, (String) elem[9], (String) elem[8], (String) elem[7], (String) elem[10], planSalesGroup, disbursementType, minimumTermMonths, maxFinance, vehiclePurchaseType, availableFinanceType));
                }
            }
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getPlanSearchByProductId is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getPlanSearchByProductId: " + ex.getMessage());
            return null;
        }
    }

    public List<PlanSearch> getPlanSearchs() {
        try {
            LOGGER.info("in getPlanSearchs");
            List<Object[]> rawList;
            rawList = em.createNativeQuery("select distinct  g.planId, g.planName, g.planCode, h.defaultDeductibleIdFk, h.effectiveDate, b.productName, b.productCode, c.productTypeName, e.accountKeeperName, a.name,  d.description, i.name "
                    + "from Program a, Product b, ProductType c, ProductDetail d, AccountKeeper e, Insurer f, PlanTable g, PlanDetail h, ClassTable i "
                    + "where a.programId=b.programIdFk and b.productTypeIdFk=c.productTypeId and b.productId=d.productIdFk and b.insurerIdFk=f.insurerId and f.insurerId=e.accountKeeperId and g.productIdFk=b.productId and h.planIdFk=g.planId  and h.classTableIdFk=i.classTableId order by h.effectiveDate desc ").getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<PlanSearch> cList = new ArrayList<>();

            String planType = "";     // later
            String vinClassingTable = "";
            Integer defaultDeductibleIdFk = null;
            String deductibleDescription = null;
            Date expireDate = null; // later
            String adminCompany = "DOWC";     //later
            String planSalesGroup = ""; // later
            String disbursementType = "";
            String minimumTermMonths = "";
            String maxFinance = "";
            String vehiclePurchaseType = "";
            String availableFinanceType = "";

            List<Integer> listI = new ArrayList<>();

            Integer planId = null;
            for (Object[] elem : rawList) {
                planId = (Integer) elem[0]; // has to deal with bad data in ProductDetail
                vinClassingTable = (String) elem[11];
                if (!listI.contains(planId)) {
                    listI.add(planId);
                    if ((defaultDeductibleIdFk = (Integer) elem[3]) != null) {
                        deductibleDescription = em.createNamedQuery("Deductible.findByDeductibleId", Deductible.class).setParameter("deductibleId", defaultDeductibleIdFk).getSingleResult().getDescription();
                    }
                    cList.add(new PlanSearch(planId, (String) elem[1], (String) elem[2], planType, vinClassingTable, deductibleDescription, (Date) elem[4], expireDate, adminCompany, (String) elem[9], (String) elem[8], (String) elem[7], (String) elem[10], planSalesGroup, disbursementType, minimumTermMonths, maxFinance, vehiclePurchaseType, availableFinanceType));
                }
            }
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getPlanSearchByProductId is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getPlanSearchByProductId: " + ex.getMessage());
            return null;
        }
    }

    //jxwww
    public List<Product> getProductByAll() {
        try {
            return (List<Product>) em.createNamedQuery("Product.findAll", Product.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getProductByAll is null");
            return null;
        }
    }

    public Product getProductByName(String name) {
        LOGGER.info("in getProductByName=" + name);
        try {
            return (Product) em.createNamedQuery("Product.findByProductName", Product.class).setParameter("productName", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProductByName is null");
            return null;
        }
    }

    public Product getProductByCode(String code) {
        try {
            return (Product) em.createNamedQuery("Product.findByProductCode", Product.class).setParameter("code", code).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProductByCode is null");
            return null;
        }
    }

    public Product getProductById(Integer id) {
        try {
            return (Product) em.createNamedQuery("Product.findByProductId", Product.class).setParameter("productId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getProductById is null");
            return null;
        }
    }

    public List<Product> getProductListByProgId(Integer progId) {
        try {
            return (List<Product>) em.createNativeQuery("select * from Product where programIdFk=?1", Product.class).setParameter(1, progId).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getProductListByProgId is null");
            return null;
        }
    }

    public ProductDetail getProductDetailByFkId(Integer id) {
        try {
            return (ProductDetail) em.createNativeQuery("select * from ProductDetail where productIdFk=?1", ProductDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getProductDetailByFkId is null");
            return null;
        }
    }

    public void deleteProduct(Integer id) {
        try {
            LOGGER.info("in deleteProduct, productId=" + id);

            utx.begin();

            Product product = getProductById(id);
            ProductDetail pd = getProductDetailByFkId(id);

            em.remove(pd);
            em.remove(product);

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();
            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deletePlan(Integer id) {
        try {
            LOGGER.info("in deletePlan, planId=" + id);

            utx.begin();

            PlanTable planTable = getPlanById(id);
            PlanDetail pd = getPlanDetailByFkId(id);

            em.remove(pd);
            em.remove(planTable);

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();
            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public List<PlanType> getPlanTypeByAll() {
        try {
            return (List<PlanType>) em.createNamedQuery("PlanType.findAll", PlanType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getPlanTypeByAll is null");
            return null;
        }
    }

    public List<ClassTable> getClassTableByAll() {
        try {
            return (List<ClassTable>) em.createNamedQuery("ClassTable.findAll", ClassTable.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getClassTableByAll is null");
            return null;
        }
    }

    public List<VehiclePurchaseType> getVehiclePurchaseTypeByAll() {
        LOGGER.info("in getVehiclePurchaseTypeByAll");
        try {
            List<VehiclePurchaseType> vptList = new ArrayList<>();
            vptList = (List<VehiclePurchaseType>) em.createNamedQuery("VehiclePurchaseType.findAll", VehiclePurchaseType.class).getResultList();
            LOGGER.info("length of vptList=" + vptList.size());
            return vptList;

        } catch (NoResultException e) {
            LOGGER.info("getVehiclePurchaseTypeByAll is null");
            return null;
        }
    }

    public List<FinanceType> getFinanceTypeByAll() {
        try {
            return (List<FinanceType>) em.createNamedQuery("FinanceType.findAll", FinanceType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getFinanceTypeByAll is null");
            return null;
        }
    }

    public List<PPMSchedule> getPPMScheduleByAll() {
        try {
            return (List<PPMSchedule>) em.createNamedQuery("PPMSchedule.findAll", PPMSchedule.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getPPMScheduleByAll is null");
            return null;
        }
    }

    public List<DtPlanStartSchedule> getDtPlanStartScheduleByAll() {
        try {
            return (List<DtPlanStartSchedule>) em.createNamedQuery("DtPlanStartSchedule.findAll", DtPlanStartSchedule.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanStartScheduleByAll is null");
            return null;
        }
    }

    public List<DtPlanEarningMethod> getDtPlanEarningMethodByAll() {
        try {
            return (List<DtPlanEarningMethod>) em.createNamedQuery("DtPlanEarningMethod.findAll", DtPlanEarningMethod.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanEarningMethodByAll is null");
            return null;
        }
    }

    public List<DtPlanLimitDeductibleDefault> getDtPlanLimitDeductibleDefaultByAll() {
        try {
            return (List<DtPlanLimitDeductibleDefault>) em.createNamedQuery("DtPlanLimitDeductibleDefault.findAll", DtPlanLimitDeductibleDefault.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanLimitDeductibleDefaultByAll is null");
            return null;
        }
    }

    public List<DtPlanExpireTime> getDtPlanExpireTimeByAll() {
        try {
            return (List<DtPlanExpireTime>) em.createNamedQuery("DtPlanExpireTime.findAll", DtPlanExpireTime.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanExpireTimeByAll is null");
            return null;
        }
    }

    public VehiclePurchaseType getVehiclePurchaseTypeByName(String desc) {
        try {
            return (VehiclePurchaseType) em.createNamedQuery("VehiclePurchaseType.findByDescription", VehiclePurchaseType.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getVehiclePurchaseTypeByName is null");
            return null;
        }
    }

    public FinanceType getFinanceTypeByName(String desc) {
        try {
            return (FinanceType) em.createNamedQuery("FinanceType.findByDescription", FinanceType.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getFinanceTypeByName is null");
            return null;
        }
    }

    public DtPlanEarningMethod getDtPlanEarningMethodByName(String desc) {
        try {
            return (DtPlanEarningMethod) em.createNamedQuery("DtPlanEarningMethod.findByDescription", DtPlanEarningMethod.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanEarningMethodByName is null");
            return null;
        }
    }

    public PPMSchedule getPPMScheduleByName(String name) {
        try {
            return (PPMSchedule) em.createNamedQuery("PPMSchedule.findByName", PPMSchedule.class).setParameter("Name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getPPMScheduleByName is null");
            return null;
        }
    }

    public DtPlanStartSchedule getDtPlanStartScheduleByName(String desc) {
        try {
            return (DtPlanStartSchedule) em.createNamedQuery("DtPlanStartSchedule.findByDescription", DtPlanStartSchedule.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanStartScheduleByName is null");
            return null;
        }
    }

    public DtPlanExpireTime getDtPlanExpireTimeByName(String desc) {
        try {
            return (DtPlanExpireTime) em.createNamedQuery("DtPlanExpireTime.findByDescription", DtPlanExpireTime.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtPlanExpireTimeByName is null");
            return null;
        }
    }

    public ClassTable getClassTableByName(String name) {
        try {
            return (ClassTable) em.createNamedQuery("ClassTable.findByName", ClassTable.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getClassTableByName is null");
            return null;
        }
    }

    public PlanTable getPlanById(Integer id) {
        try {
            return (PlanTable) em.createNamedQuery("PlanTable.findByPlanId", PlanTable.class).setParameter("planId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getPlanTableById is null");
            return null;
        }
    }

    public PlanTable getPlanByName(String name) {
        try {
            return (PlanTable) em.createNamedQuery("PlanTable.findByPlanName", PlanTable.class).setParameter("planName", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getPlanTableById is null");
            return null;
        }
    }

    public PlanTable getPlanByNameId(String name, Integer productId) {
        try {
            LOGGER.info("name=" + name);
            LOGGER.info("productId=" + productId);
            return (PlanTable) em.createNativeQuery("select * from PlanTable where planName=?1 and productIdFk=?2", PlanTable.class).setParameter(1, name).setParameter(2, productId).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getPlanByNameId is null");
            return null;
        }
    }

    public List<PlanTable> getPlanByProdId(Integer id) {
        try {
            return (List<PlanTable>) em.createNativeQuery("select * from PlanTable where productIdFk=?1", PlanTable.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getPlanByProdId is null");
            return null;
        }
    }

    public PlanDetail getPlanDetailByFkId(Integer id) {
        try {
            return (PlanDetail) em.createNativeQuery("select * from PlanDetail where planIdFk=?1", PlanDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getPlanDetailByFkId is null");
            return null;
        }
    }

    public Integer getNumberOfClaimsByUserName(String userName, Date dt) {
        try {
            String query = "select count(*) from Claim a, UserMember b where a.ownedByFk=b.userMemberId and b.userName='" + userName + "' and inceptionDate >='" + dt + "'";

            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;
        } catch (NoResultException e) {
            LOGGER.info("getNumberOfClaimsByUserName is null");
            return null;
        }
    }

    public HashMap<String, Integer> getNumberOfClaimsByUserDt(String userName, Date dt) {
        try {
            HashMap<String, Integer> hm = new HashMap<>();
            String query = "select a.productTypeIdFk, count(*) from Claim a, UserMember b where a.ownedByFk=b.userMemberId and b.userName='" + userName + "' and inceptionDate >='" + dt + "' group by a.productTypeIdFk";

            //Query q = em.createNativeQuery(query);
            List<Object[]> rawList = em.createNativeQuery(query).getResultList();

            for (Object[] elem : rawList) {
                String typeName = getProductTypeById((Integer) elem[0]).getProductTypeName();
                hm.put(typeName, (Integer) elem[1]);
            }

            return hm;
        } catch (NoResultException e) {
            LOGGER.info("getNumberOfClaimsByUserDt is null");
            return null;
        }
    }

    public Integer getNumberOfClaimsToDate(Date dt) {
        try {
            String query = "select count(*) from Claim a where  inceptionDate >='" + dt + "'";

            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;
        } catch (NoResultException e) {
            LOGGER.info("getNumberOfClaimsToDate is null");
            return null;
        }
    }

    public Integer getNumberOfContractsByUserName(String userName, Date dt) {
        try {
            String query = "select count(*) from AccountKeeper c,  (select distinct a.accountKeeperName from AccountKeeper a,  UserMember b  where userName='" + userName + "' and a.accountKeeperId=b.userMemberId) d, Contracts e where c.accountKeeperName=d.accountKeeperName and c.accountKeeperTypeIdFk in (1,3) and e.agentIdFk = c.accountKeeperId and e.effectiveDate>='" + dt + "'";

            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;
        } catch (NoResultException e) {
            LOGGER.info("getNumberOfContractsByUserName is null");
            return null;
        }
    }

    public Integer getNumberOfContractsToDate(Date dt) {
        try {
            String query = "select count(*) from Contracts e where  e.effectiveDate>='" + dt + "'";

            Query q = em.createNativeQuery(query);
            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;
        } catch (NoResultException e) {
            LOGGER.info("getNumberOfContractsToDate is null");
            return null;
        }
    }

    public boolean verifyUserLogin(String user, String pswd) {
        String q = "select count(*) from UserMember where userName='" + user + "' and password='" + pswd + "'";
        Query query = em.createNativeQuery(q);
        Integer ret = (Integer) query.getSingleResult();
        if (ret == 0) {
            return false;
        }
        return true;
    }

    public List<PBDForClaim> getPBDforClaim(PBDForClaim pbd) {
        try {
            LOGGER.info("in getPBDforClaim");
            List<Object[]> rawList;
            String q = "select  b.dealerIdFk, a.productTypeIdFk, b.productIdFk, count(*),  SUM(a.amount) "
                    + "from Claim a, Contracts b, Product c, Dealer d "
                    + "where a.contractIdFk=b.contractId and b.productIdFk=c.productId   and a.inceptionDate>='" + getJavaDate(pbd.getStartDate()) + "' and a.inceptionDate<='" + getJavaDate(pbd.getEndDate()) + "' and b.dealerIdFk=d.dealerId "
                    + "group by b.dealerIdFk, a.productTypeIdFk, b.productIdFk order by b.dealerIdFk, a.productTypeIdFk, b.productIdFk ";
            LOGGER.info("q=" + q);
            rawList = em.createNativeQuery(q).getResultList();
            LOGGER.info("rawList size=" + rawList.size());
            List<PBDForClaim> cList = new ArrayList<>();

            PBDForClaim tmpPBD = new PBDForClaim();

            Integer dealerId = null;
            String dealerName = null;
            Integer pDealerId = null;
            Integer productTypeId;
            Integer productId;
            Integer count;
            BigDecimal amount;

            Integer ancCount = 0;
            Integer ppmCount = 0;
            Integer vscCount = 0;
            Integer gapCount = 0;
            Integer totalCount = 0;
            BigDecimal totalAmount = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
            BigDecimal avgAmount = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

            for (Object[] elem : rawList) {

                dealerId = (Integer) elem[0];
                productTypeId = (Integer) elem[1];
                productId = (Integer) elem[2];
                count = (Integer) elem[3];
                amount = (BigDecimal) elem[4];

                dealerName = getAccountKeeperById(dealerId).getAccountKeeperName();

                LOGGER.info("dealerName=" + dealerName);
                LOGGER.info("count=" + count);
                LOGGER.info("productTypeId=" + productTypeId);
                LOGGER.info("productId=" + productId);

                if (pDealerId != null && pDealerId.intValue() != dealerId.intValue()) {

                    avgAmount = totalAmount.divide(new BigDecimal(totalCount), 2, BigDecimal.ROUND_HALF_UP);
                    cList.add(new PBDForClaim(dealerId, ancCount == 0 ? null : ancCount, tmpPBD.getBun(), tmpPBD.getRht(), tmpPBD.getThreeFORone(), tmpPBD.getPdr(), tmpPBD.getLux(), tmpPBD.getEtch(), tmpPBD.getEwt(), tmpPBD.getKey(), ppmCount == 0 ? null : ppmCount, gapCount == 0 ? null : gapCount, vscCount == 0 ? null : vscCount, totalCount, avgAmount, totalAmount, pbd.getStartDate(), pbd.getEndDate(), dealerName));
                    LOGGER.info("inside, totalCount=" + totalCount);

                    tmpPBD.reset();
                    ancCount = 0;
                    ppmCount = 0;
                    vscCount = 0;
                    gapCount = 0;
                    totalCount = 0;
                    totalAmount = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

                }

                pDealerId = dealerId;

                tmpPBD.setDealerId(dealerId);
                totalAmount = totalAmount.add(amount).setScale(2, RoundingMode.HALF_UP);
                totalCount += count;

                switch (productTypeId) {
                    case 1:
                        vscCount += count;
                        break;
                    case 2:
                        gapCount += count;
                        break;
                    case 4:
                        ancCount += count;
                        switch (productId) {
                            case 32:
                                tmpPBD.setBun(count);
                                break;
                            case 8:
                                tmpPBD.setRht(count);
                                break;
                            case 14:
                                tmpPBD.setThreeFORone(count);
                                break;
                            case 9:
                                tmpPBD.setPdr(count);
                                break;
                            case 11:
                                tmpPBD.setLux(count);
                                break;
                            case 24:
                                tmpPBD.setEtch(count);
                                break;
                            case 3:
                                tmpPBD.setEwt(count);
                                break;
                            case 10:
                                tmpPBD.setKey(count);
                                break;
                        }
                        break;

                    case 8:
                        ppmCount += count;
                        break;
                }
            }
            LOGGER.info("create new instance, totalCount=" + totalCount);
            cList.add(new PBDForClaim(dealerId, ancCount == 0 ? null : ancCount, tmpPBD.getBun(), tmpPBD.getRht(), tmpPBD.getThreeFORone(), tmpPBD.getPdr(), tmpPBD.getLux(), tmpPBD.getEtch(), tmpPBD.getEwt(), tmpPBD.getKey(), ppmCount == 0 ? null : ppmCount, gapCount == 0 ? null : gapCount, vscCount == 0 ? null : vscCount, totalCount, avgAmount, totalAmount, pbd.getStartDate(), pbd.getEndDate(), dealerName));
            LOGGER.info("size of cList=" + cList.size());
            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getPBDforClaim is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("error in getPBDforClaim: " + ex.getMessage());
            return null;
        }
    }

    public VinDesc getVinDescByVinNo(String vin) {
        System.out.println("in getVinDescByVinNo, vin=" + vin);
        try {
            String partialVin = vin.substring(0, 8) + "_" + vin.substring(9, 11) + "______";
            System.out.println("partialVin=" + partialVin);
            return em.createNamedQuery("VinDesc.findByVinPartial", VinDesc.class).setParameter("vinPartial", partialVin).getResultList().get(0);

        } catch (NoResultException e) {
            LOGGER.info("getVinDescByVinNo is null");
            return null;
        }
    }

    public VinDesc getVinDescByVinFull(String vin) {
        LOGGER.info("in getVinDescByVinFull, vin=" + vin);
        try {
            String vinPartial = vin.substring(0, 8) + "%" + vin.substring(9, 11) + "%";
            return (VinDesc) em.createNativeQuery("select * from VinDesc where vinPartial like ?1 and valid = 1", VinDesc.class).setParameter(1, vinPartial).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getVinDescByVinFull is null");
            return null;
        }
    }

    public PlanTable getPlanTableByPlanNameDesc(String planName) {
        try {
            return (PlanTable) em.createNativeQuery("select * from PlanTable where planName=?1 order by planId desc", PlanTable.class).setParameter(1, planName).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getPlanTableByPlanNameDesc is null");
            return null;
        }
    }

    public PlanTable getPlanTableByPlanName(String planName, Integer productId) {
        try {
            return (PlanTable) em.createNativeQuery("select * from PlanTable where planName=?1 and productIdFk=?2 order by planId desc", PlanTable.class).setParameter(1, planName).setParameter(2, productId).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getPlanTableByPlanName is null");
            return null;
        }
    }

    public Integer getPlanDetailByPlanIdDesc(Integer planId) {
        try {
            return (Integer) em.createNativeQuery("select distinct classTableIdFk from PlanDetail where planIdFk=?1 order by classTableIdFk desc").setParameter(1, planId).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getPlanDetailByPlanIdDesc is null");
            return null;
        }
    }

    public ClassTable getClassTableById(Integer id) {
        try {
            return (ClassTable) em.createNamedQuery("ClassTable.findByClassTableId", ClassTable.class).setParameter("classTableId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getClassTableById is null");
            return null;
        }
    }

    public List<ClassRule> getClassRuleByClassTableId(Integer id) {
        try {
            return (List<ClassRule>) em.createNativeQuery("select * from ClassRule where classTableIdFk = ?1 order by classPriorityInd", ClassRule.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getClassRuleByClassTableId is null");
            return null;
        }
    }

    public ClassItem getClassItemByClassRule(ClassRule classRule, VinDesc vinDesc) {

        try {

            String qs = "select * from ClassItem where classRuleIdFk= " + classRule.getClassRuleId() + " ";
            String vehicleTypeStr = "";
            String makeIdStr = "";
            String modelIdStr = "";
            String seriesStr = "";
            String cylinderStr = "";
            String fuelTypeStr = "";
            String fuelDeliveryStr = "";
            String driveTypeStr = "";
            String tonRatingStr = "";
            String countryStr = "";

            if (classRule.getVehicleTypeInd() == true) {
                makeIdStr = "  and vehicleTypeInd=" + vinDesc.getVehicleType() + " ";
            }
            if (classRule.getMakeInd() == true) {
                makeIdStr = "  and makeIdFk=" + vinDesc.getMakeIdFk().getMakeId() + " ";
            }
            if (classRule.getModelInd() == true) {
                modelIdStr = "  and modelIdFk=" + vinDesc.getModelIdFk().getModelId() + " ";
            }
            if (classRule.getSeriesInd() == true) {
                seriesStr = "  and seriesIdFk=" + vinDesc.getSeriesIdFk().getSeriesId() + " ";
            }
            if (classRule.getCylindersInd() == true) {
                cylinderStr = "  and cylinders=" + vinDesc.getCylinders() + " ";
            }
            if (classRule.getFuelTypeInd() == true) {
                fuelTypeStr = "  and fuelTypeIdFk=" + vinDesc.getFuelTypeIdFk().getFuelTypeId() + " ";
            }
            if (classRule.getFuelDeliveryInd() == true) {
                fuelDeliveryStr = "  and fuelDeliveryIdFk=" + vinDesc.getFuelDeliveryIdFk().getFuelDeliveryId() + " ";
            }
            if (classRule.getDriveTypeInd() == true) {
                driveTypeStr = "  and driveTypeInd=" + vinDesc.getDriveType() + " ";
            }
            if (classRule.getTonRatingInd() == true) {
                // later
            }
            if (classRule.getCountryOfOriginInd() == true) {
                countryStr = "  and countryStr=" + vinDesc.getCountryOfOriginIdFk().getCountryOfOriginId() + " ";
            }

            String endstr = " order by classItemId desc";

            String query = qs + vehicleTypeStr + makeIdStr + modelIdStr + seriesStr + cylinderStr + fuelTypeStr + fuelDeliveryStr + driveTypeStr + tonRatingStr + countryStr + endstr;

            LOGGER.info("query=" + query);

            // new
            Query q = em.createNativeQuery(query, ClassItem.class);

            if (q.getResultList().isEmpty()) {
                System.out.println("is empty");
                return null;
            } else {
                return (ClassItem) q.getResultList().get(0);
            }
        } catch (NoResultException e) {
            LOGGER.info("getClassItemByClassRule is null");
            return null;
        }
    }

    public boolean isVSC(Product product) {
        if (product.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("VSC") == 0) {
            return true;
        }
        return false;
    }

    public boolean isPPM(Product product) {
        if (product.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("PPM") == 0) {
            return true;
        }
        return false;
    }

    public boolean isANC(Product product) {
        if (product.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("ANC") == 0) {
            return true;
        }
        return false;
    }

    public boolean isGAP(Product product) {
        if (product.getProductTypeIdFk().getProductTypeName().compareToIgnoreCase("GAP") == 0) {
            return true;
        }
        return false;
    }

    public Integer getClassCode(ContractRating cr) {
        try {
            String vinNo = cr.getVinNo();
            String planName = cr.getPlanName();
            String productName = cr.getProductName();

            LOGGER.info("in getClassCode, vinNo=" + vinNo);
            LOGGER.info("planName=" + planName);
            LOGGER.info("productId=" + cr.getProductId());
            LOGGER.info("programName=" + cr.getProgramName());

            VinDesc vinDesc = getVinDescByVinNo(vinNo);

            PlanTable planTable = getPlanTableByPlanName(planName, cr.getProductId());
            //Integer planId = getPlanTableByPlanNameDesc(planName).getPlanId();
            Integer planId = planTable.getPlanId();
            LOGGER.info("planId=" + planId);
            if (planId != null) {
                Integer classTableIdFk = getPlanDetailByPlanIdDesc(planId);
                LOGGER.info("classTableIdFk=" + classTableIdFk);

                List<ClassRule> classRuleList = getClassRuleByClassTableId(classTableIdFk);

                for (ClassRule classRule : classRuleList) {
                    LOGGER.info("classRuleId=" + classRule.getClassRuleId());
                    ClassItem classItem = getClassItemByClassRule(classRule, vinDesc);
                    if (classItem != null && classItem.getVinClassCodeIdFk() != null) {
                        return Integer.parseInt(classItem.getVinClassCodeIdFk().getVinCode());
                    }
                }
            }
            LOGGER.info("classCode not found...");
            return null;
        } catch (Exception ex) {
            LOGGER.info(ex.getStackTrace());
            LOGGER.info("getClassCode is null");
            return null;
        }
    }

    public Integer getTermId(ContractRating cr) {
        LOGGER.info("in getTermId");
        try {
            Product product = getProductById(cr.getProductId());

            if (isANC(product)) {
                return getTermANC(cr.getPlanId(), cr.getAncMonths());
            } else if (isVSC(product)) {
                LOGGER.info("is VSC");
                return getTermVSC(cr.getPlanId(), cr.getExpTime(), cr.getExpUsage());
            } else if (isPPM(product)) {
                return getTermPPM(cr.getPlanId(), cr.getPpmTime(), cr.getPpmUsage());
            }
            LOGGER.info("no termId found");
            return null;
        } catch (Exception ex) {
            ex.getMessage();
            LOGGER.info("getTermId is null");
            return null;
        }
    }

    public Integer getTermANC(Integer planId, Integer ancMonths) {
        try {
            TermDetail termDetail = (TermDetail) em.createNativeQuery("select * from TermDetail where termIdFk in ( select termId from Term where planIdFk=?1 ) and  ancMonths=?2 order by termDetailId desc", TermDetail.class).setParameter(1, planId).setParameter(2, ancMonths).getResultList().get(0);
            return termDetail.getTermIdFk().getTermId();
        } catch (NoResultException e) {
            LOGGER.info("getTermANC is null");
            return null;
        }
    }

    public Integer getTermVSC(Integer planId, Integer expTime, Integer expUsage) {
        try {
            LOGGER.info("planId=" + planId);
            LOGGER.info("expTime=" + expTime);
            LOGGER.info("expUsage=" + expUsage);
            TermDetail termDetail = (TermDetail) em.createNativeQuery("select * from TermDetail where termIdFk in ( select termId from Term where planIdFk=?1 ) and vscExpireTime=?2 and vscExpireUsage=?3 order by termDetailId desc", TermDetail.class).setParameter(1, planId).setParameter(2, expTime).setParameter(3, expUsage).getResultList().get(0);

            return termDetail.getTermIdFk().getTermId();
        } catch (NoResultException e) {
            LOGGER.info("getTermVSC is null");
            return null;
        }
    }

    public Integer getTermPPM(Integer planId, Integer ppmTime, Integer ppmUsage) {
        try {
            TermDetail termDetail = (TermDetail) em.createNativeQuery("select * from TermDetail where termIdFk in ( select termId from Term where planIdFk=?1 ) and ppmTime=?2 and ppmUsage=?3 order by termDetailId desc", TermDetail.class).setParameter(1, planId).setParameter(2, ppmTime).setParameter(3, ppmUsage).getResultList().get(0);
            return termDetail.getTermIdFk().getTermId();
        } catch (NoResultException e) {
            LOGGER.info("getTermPPM is null");
            return null;
        }
    }

    public Integer getTermGAP(Integer planId, Integer ppmTime, Integer ppmUsage) {
        try {
            TermDetail termDetail = (TermDetail) em.createNativeQuery("select * from TermDetail where termIdFk in ( select termId from Term where planIdFk=?1 ) and ppmTime=?2 and ppmUsage=?3 order by termDetailId desc", TermDetail.class).setParameter(1, planId).setParameter(2, ppmTime).setParameter(3, ppmUsage).getResultList().get(0);
            return termDetail.getTermIdFk().getTermId();
        } catch (NoResultException e) {
            LOGGER.info("getTermPPM is null");
            return null;
        }
    }

    public List<Integer> getTermIdListByPlanId(Integer id) {
        try {
            return (List<Integer>) em.createNativeQuery("select TermId from Term where planIdFk=?1").setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getTermIdListByPlanId is null");
            return null;
        }
    }

    public List<Term> getTermListByPlanId(Integer id) {
        try {
            return (List<Term>) em.createNativeQuery("select * from Term where planIdFk=?1", Term.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getTermListByPlanId is null");
            return null;
        }
    }

    public List<Limit> getLimitListByPlanId(Integer id) {
        try {
            return (List<Limit>) em.createNativeQuery("select * from Limit where planIdFk=?1", Limit.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getLimitListByPlanId is null");
            return null;
        }
    }

    public Integer getLimitId(ContractRating cr) {
        try {
            LOGGER.info("in getLimitId, productId=" + cr.getProductId());
            Product product = getProductById(cr.getProductId());
            Integer planId = cr.getPlanId();
            Integer mileage = cr.getMileage();
            LOGGER.info("planId=" + cr.getPlanId());
            LOGGER.info("mileage=" + mileage);
            if (isVSC(product)) {
                /* original one 
                LimitDetail ld = (LimitDetail) em.createNativeQuery("select * from LimitDetail where limitIdFk in ( select limitId from Limit where planIdFk=?1 ) order by limitIdFk desc", LimitDetail.class).setParameter(1, planId).getResultList().get(0);
                Integer lowerLimitUsage = ld.getLowerLimitUsage();
                Integer upperLimitUsage = ld.getUpperLimitUsage();
                if (lowerLimitUsage <= mileage && (upperLimitUsage == null || upperLimitUsage >= mileage)) {
                    return ld.getLimitIdFk().getLimitId();
                }
                 */
                List<LimitDetail> ldList = (List<LimitDetail>) em.createNativeQuery("select * from LimitDetail where limitIdFk in ( select limitId from Limit where planIdFk=?1 ) order by limitIdFk desc", LimitDetail.class).setParameter(1, planId).getResultList();
                LOGGER.info("size of ldList=" + ldList.size());
                Integer lowerLimitUsage = null;
                Integer upperLimitUsage = null;
                for (LimitDetail ld : ldList) {
                    lowerLimitUsage = ld.getLowerLimitUsage();
                    upperLimitUsage = ld.getUpperLimitUsage();
                    if (lowerLimitUsage <= mileage && (upperLimitUsage == null || upperLimitUsage >= mileage)) {
                        return ld.getLimitIdFk().getLimitId();
                    }
                }
                return null;
                //return (Integer) em.createNativeQuery("select limitIdFk from LimitDetail where limitIdFk in ( select limitId from Limit where planIdFk=?1 ) and lowerLimitUsage<=?2 and upperLimitUsage>=?3 order by limitIdFk desc").setParameter(1, planId).setParameter(2, mileage).setParameter(3, mileage).getResultList().get(0);
            } else {
                return (Integer) em.createNativeQuery("select limitIdFk from LimitDetail where limitIdFk in ( select limitId from Limit where planIdFk=?1 )  order by limitIdFk desc").setParameter(1, planId).getResultList().get(0);
            }
        } catch (NoResultException e) {
            LOGGER.info("getLimitId is null");
            return null;
        }
    }

    public Rate getRateByPlanTermLimitId(Integer classCode, Integer planId, Integer termId, Integer limitId) {
        LOGGER.info("classCode=" + classCode);
        LOGGER.info("planId=" + planId);
        LOGGER.info("termId=" + termId);
        LOGGER.info("limitId=" + limitId);
        try {
            List<Rate> rateList = (List<Rate>) em.createNativeQuery("select * from Rate where  planIdFk=?1 and termIdFk=?2 and limitIdFk=?3 and ( classCodeIdFk=?4 or classCodeIdFk is null )", Rate.class).setParameter(1, planId).setParameter(2, termId).setParameter(3, limitId).setParameter(4, classCode).getResultList();
            if (rateList != null && rateList.size() > 0) {
                return rateList.get(0);
            } else {
                return null;
            }
        } catch (NoResultException e) {
            LOGGER.info("getRateByPlanTermLimitId is null");
            return null;
        }
    }

    public RateDetail getRateDetailByRateId(Integer rateId) {
        try {
            return (RateDetail) em.createNativeQuery("select * from RateDetail where rateIdFk=?1 order by disbursementCenterIdFk desc", RateDetail.class).setParameter(1, rateId).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getRateDetailByRateId is null");
            return null;
        }
    }

    public Integer getDisbursementByCenterId(Integer id) {
        try {
            return (Integer) em.createNativeQuery("select DisbursementId from Disbursement where disbursementCenterIdFk=?1 order by DisbursementId desc").setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementByCenterId is null");
            return null;
        }
    }

    public BigDecimal getDisbursementAmountById(Integer id) {
        try {
            return (BigDecimal) em.createNativeQuery("select disbursementAmount from DisbursementDetail where DisbursementIdFk=?1 and deletedInd=0 order by disbursementDetailId desc").setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementAmountById is null");
            return null;
        }
    }

    public BigDecimal getContractReserve(ContractRating cr) {
        try {
            LOGGER.info("in getContractReserve, plan= " + cr.getPlanName());
            LOGGER.info("classCode = " + cr.getVehicleClassCode());
            BigDecimal reserveRate = null;

            Integer classCode = getClassCode(cr);
            LOGGER.info("classCode=" + classCode);
            cr.setVehicleClassCode(classCode);

            Product product = getProductById(cr.getProductId());
            Integer termId = getTermId(cr);
            if (termId != null) {
                LOGGER.info("termId=" + termId);
                Integer limitId = getLimitId(cr);
                if (limitId != null) {
                    LOGGER.info("limitId=" + limitId);
                    Rate rate = getRateByPlanTermLimitId(classCode, cr.getPlanId(), termId, limitId);
                    if (rate != null) {
                        LOGGER.info("rateId=" + rate.getRateId());
                        Integer disbursementCenterIdFk = getRateDetailByRateId(rate.getRateId()).getDisbursementCenterIdFk().getDisbursementCenterId();
                        LOGGER.info("disbursementCenterIdFk=" + disbursementCenterIdFk);
                        Integer disbursementId = getDisbursementByCenterId(disbursementCenterIdFk);
                        LOGGER.info("disbursementId=" + disbursementId);
                        reserveRate = getDisbursementAmountById(disbursementId).setScale(2, RoundingMode.HALF_UP);
                        LOGGER.info("reserverRate=" + reserveRate);
                    } else {
                        return null;
                    }
                }
            }
            return reserveRate;
        } catch (NoResultException e) {
            LOGGER.info("getContractRerserve is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("Unknow error: ");
            ex.printStackTrace();
            return null;
        }
    }

    public Dealer getDealerByName(String name) {
        try {
            return (Dealer) em.createNativeQuery("select a.* from Dealer a, AccountKeeper b where a.dealerId=b.accountKeeperId  and b.accountKeeperName = ?1", Dealer.class).setParameter(1, name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDealerByName is null");
            return null;
        }
    }

    public Dealer getDealerById(Integer id) {
        try {
            return em.createNamedQuery("Dealer.findByDealerId", Dealer.class).setParameter("dealerId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDealerById is null");
            return null;
        }
    }

    public DealerGroup getDealerGroupById(Integer id) {
        try {
            return (DealerGroup) em.createNativeQuery("select a.* from DealerGroup a, AccountKeeper b where a.dealerGroupId=b.accountKeeperId and a.dealerGroupId=?1", DealerGroup.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDealerGroupById is null");
            return null;
        }
    }

    public List<Disbursement> getDisbursementByFkId(Integer id, Integer dealerId) {
        try {
            return (List<Disbursement>) em.createNativeQuery("select * from Disbursement where disbursementCenterIdFk=?1 and dealerConditionIdFk=?2", Disbursement.class).setParameter(1, id).setParameter(2, dealerId).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementByFkId is null");
            return null;
        }
    }

    public List<Disbursement> getDisbursementByFkId(Integer id) {
        try {
            return (List<Disbursement>) em.createNativeQuery("select * from Disbursement where disbursementCenterIdFk=?1", Disbursement.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementByFkId is null");
            return null;
        }
    }

    public DisbursementDetail getDisbursementDetailByFkId(Integer id) {
        LOGGER.info("in DisbursementDetail, id=" + id);
        try {
            List<DisbursementDetail> ddList = (List<DisbursementDetail>) em.createNativeQuery("select * from DisbursementDetail where disbursementIdFk=?1 and deletedInd=0  order by disbursementDetailId desc", DisbursementDetail.class).setParameter(1, id).getResultList();
            if (ddList.isEmpty()) {
                return null;
            } else {
                LOGGER.info("ddList size of =" + ddList.size());
                return ddList.get(0);
            }
            //return (DisbursementDetail) em.createNativeQuery("select * from DisbursementDetail where disbursementIdFk=?1 and deletedInd=0  order by disbursementDetailId desc", DisbursementDetail.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementDetailByFkId is null");
            return null;
        }
    }

    public CfCriterionGroup getCfCriterionGroupById(Integer id) {
        try {
            return em.createNamedQuery("CfCriterionGroup.findByCriterionGroupId", CfCriterionGroup.class).setParameter("criterionGroupId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCfCriterionGroupById is null");
            return null;
        }
    }

    public List<DtCriterionRule> getDtCriterionRuleByAll() {
        try {
            return (List<DtCriterionRule>) em.createNamedQuery("DtCriterionRule.findAll", DtCriterionRule.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtCriterionRuleByAll is null");
            return null;
        }
    }

    public DtCriterionRule getDtCriterionRuleById(Integer id) {
        try {
            return (DtCriterionRule) em.createNamedQuery("DtCriterionRule.findByCriterionRuleId", DtCriterionRule.class).setParameter("criterionRuleId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtCriterionRuleById is null");
            return null;
        }
    }

    public DtCriterionRule getDtCriterionRuleByName(String name) {
        try {
            return (DtCriterionRule) em.createNamedQuery("DtCriterionRule.findByCriterionRuleName", DtCriterionRule.class).setParameter("criterionRuleName", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtCriterionRuleById is null");
            return null;
        }
    }

    // complicated logics
    public boolean isMatchedDisbursement(ContractRating cr, CfCriterionGroup cfg) {
        String desc = cfg.getDescription();
        // (Product Equal [All Products > Pre-paid Maintenance] AND Dealer Equal BMW & Mini of Southampton AND )
        // (Dealer Equal Viva Kia AND (Program Equal Vehicle Service Contracts OR Product Equal [All Products > Vehicle Service Contract] OR ))
        //(Product Not Equal [All Products > Pre-owned Warranty] AND Product Not Equal [All Products > Key/Remote Replacement] AND Product Not Equal [All Products > Key DOWC] AND Product Not Equal [All Products > UV Basic Etch] AND )
        LOGGER.info("description=" + desc);

        String[] criterions;
        boolean isOR = false;
        boolean isAND = false;
        if (desc.contains("OR")) {
            criterions = desc.split("OR");
            isOR = true;
        } else if (desc.contains("AND")) {
            criterions = desc.split("AND");
            isAND = true;
        } else {
            LOGGER.info("no criterion");
            return false;
        }
        String program;
        String product;
        boolean isGOOD = false;
        for (String s : criterions) {
            String line = s.trim();
            LOGGER.info("line=|" + s.trim() + "|");

            if (!line.equalsIgnoreCase(")")) {
                if (line.contains("Dealer")) {
                    String[] dealers = line.split("Equal");
                    String dealer = dealers[1].trim();
                    System.out.println("dealer=" + dealer);
                    if (dealer.equalsIgnoreCase(cr.getDealerName())) {
                        if (isOR) {
                            return true;
                        }
                        isGOOD = true;
                    } else {
                        if (isAND) {
                            return false;
                        }
                    }
                } else if (line.contains("Program")) {
                    String[] programs = line.split("Equal");
                    String pg = programs[1].trim();
                    System.out.println("program=" + pg);
                    if (pg.equalsIgnoreCase(cr.getProgramName())) {
                        if (isOR) {
                            return true;
                        }
                        isGOOD = true;
                    } else {
                        if (isAND) {
                            return false;
                        }
                    }
                } else {
                    Matcher m = Pattern.compile("\\[([^)]+)\\]").matcher(line);
                    while (m.find()) {
                        LOGGER.info(m.group(1));
                        String[] sub = m.group(1).split(">");
                        program = sub[0].trim();
                        product = sub[1].trim();
                        LOGGER.info("program=" + program);
                        LOGGER.info("product=" + product);
                        LOGGER.info("cr.program=" + cr.getProgramName());
                        LOGGER.info("cr.product=" + cr.getProductName());
                        if (program.equalsIgnoreCase(cr.getProgramName()) && product.equalsIgnoreCase(cr.getProductName())) {
                            if (isOR) {
                                return true;
                            }
                            isGOOD = true;
                        } else {
                            if (isAND) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return isGOOD;
    }

    public boolean isMatchedCriterionGroup(SearchCriterion sc, String desc) {
        //String desc = cfg.getDescription();
        // (Product Equal [All Products > Pre-paid Maintenance] AND Dealer Equal BMW & Mini of Southampton AND )
        // (Dealer Equal Viva Kia AND (Program Equal Vehicle Service Contracts OR Product Equal [All Products > Vehicle Service Contract] OR ))
        //(Product Not Equal [All Products > Pre-owned Warranty] AND Product Not Equal [All Products > Key/Remote Replacement] AND Product Not Equal [All Products > Key DOWC] AND Product Not Equal [All Products > UV Basic Etch] AND )
        // (Product Equal [All Products > GAP B] AND Dealer Not Equal 21st Century Auto Group Inc. AND )
        LOGGER.info("description=" + desc);

        String[] criterions;
        boolean isOR = false;
        boolean isAND = false;
        if (desc.contains("OR")) {
            criterions = desc.split("OR");
            isOR = true;
        } else if (desc.contains("AND")) {
            criterions = desc.split("AND");
            isAND = true;
        } else {
            LOGGER.info("no criterion");
            return false;
        }
        String program;
        String product;
        boolean isGOOD = false;
        for (String s : criterions) {
            String line = s.trim();
            LOGGER.info("line=|" + s.trim() + "|");

            if (!line.equalsIgnoreCase(")")) {
                if (line.contains("Dealer")) {
                    String[] dealers = line.split("Equal");
                    String dealer = dealers[1].trim();
                    System.out.println("dealer=" + dealer);
                    if (dealer.equalsIgnoreCase(sc.getDealerName())) {
                        if (isOR) {
                            return true;
                        }
                        isGOOD = true;
                    } else {
                        if (isAND) {
                            return false;
                        }
                    }
                } else if (line.contains("Program")) {
                    String[] programs = line.split("Equal");
                    String pg = programs[1].trim();
                    System.out.println("program=" + pg);
                    if (pg.equalsIgnoreCase(sc.getProgramName())) {
                        if (isOR) {
                            return true;
                        }
                        isGOOD = true;
                    } else {
                        if (isAND) {
                            return false;
                        }
                    }
                } else if (line.contains("Days")) {
                    if (line.contains("Less Than")) {
                        String dayStr[] = line.split("Days Effective Less Than or Equal");
                        Long ldd = Long.parseLong(dayStr[1].trim());
                        LOGGER.info("ldd=" + ldd);
                        if (sc.getDays() <= ldd) {
                            isGOOD = true;
                        }
                    } else if (line.contains("Greater Than")) {
                        String dayStr[] = null;
                        if (line.contains("Days Effective Greater Than or Equal")) {
                            dayStr = line.split("Days Effective Greater Than or Equal");
                        } else {
                            dayStr = line.split("Days Effective Greater Than");
                        }
                        Long gdd = Long.parseLong(dayStr[1].trim());
                        LOGGER.info("gdd=" + gdd);
                        if (sc.getDays() > gdd) {
                            isGOOD = true;
                        }
                    }

                } else {
                    Matcher m = Pattern.compile("\\[([^)]+)\\]").matcher(line);
                    while (m.find()) {
                        LOGGER.info(m.group(1));
                        String[] sub = m.group(1).split(">");
                        program = sub[0].trim();
                        product = sub[1].trim();
                        LOGGER.info("program=" + program);
                        LOGGER.info("product=" + product);
                        LOGGER.info("sc.program=" + sc.getProgramName());
                        LOGGER.info("sc.product=" + sc.getProductName());
                        if (program.equalsIgnoreCase(sc.getProgramName()) && product.equalsIgnoreCase(sc.getProductName())) {
                            if (isOR) {
                                return true;
                            }
                            isGOOD = true;
                        } else {
                            if (isAND) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return isGOOD;
    }

    public DtDisbursementType getDtDisbursementTypeById(Integer id) {
        try {
            return (DtDisbursementType) em.createNativeQuery("select * from DtDisbursementType where DisbursementTypeId=?1", DtDisbursementType.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtDisbursementTypeById is null");
            return null;
        }
    }

    public List<DtDisbursementType> getDtDisbursementTypeByAll() {
        try {
            return (List<DtDisbursementType>) em.createNamedQuery("DtDisbursementType.findAll", DtDisbursementType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDtDisbursementTypeByAll is null");
            return null;
        }
    }

    public DisbursementGroup getDisbursementGroupById(Integer id) {
        try {
            return (DisbursementGroup) em.createNativeQuery("select * from DisbursementGroup where DisbursementGroupId=?1", DisbursementGroup.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDisbursementGroupById is null");
            return null;
        }
    }

    public List<DisbursementGroup> getDisbursementGroupByAll() {
        try {
            return (List<DisbursementGroup>) em.createNamedQuery("DisbursementGroup.findAll", DisbursementGroup.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDisbursementGroupByAll is null");
            return null;
        }
    }

    public List<DisbursementCode> getDisbursementCodeByAll() {
        try {
            return (List<DisbursementCode>) em.createNamedQuery("DisbursementCode.findAll", DisbursementCode.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDisbursementCodeByAll is null");
            return null;
        }
    }

    public DisbursementCode getDisbursementCodeById(Integer id) {
        try {
            return (DisbursementCode) em.createNamedQuery("DisbursementCode.findByDisbursementCodeId", DisbursementCode.class).setParameter("disbursementCodeId", id).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getDisbursementCodeById is null");
            return null;
        }
    }

    public List<DealerSRP> getDealerSRPByFkId(Integer id) {
        try {
            return (List<DealerSRP>) em.createNativeQuery("select * from DealerSRP where dealerIdFk=?1", DealerSRP.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDealerSRPByFkId is null");
            return null;
        }
    }

    public List<RateBasedRule> getRateBasedRuleByFkId(Integer id) {
        try {
            return (List<RateBasedRule>) em.createNativeQuery("select * from RateBasedRule where  rateBasedRuleGroupIdFk=?1 order by programIdFk, productIdFk, planIdFk, termIdFk, limitIdFk, classCode", RateBasedRule.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getRateBasedRuleByFkId is null");
            return null;
        }
    }

    public List<Product> getProductListFromRateBasedRule(Integer id) {
        try {
            return (List<Product>) em.createNativeQuery("select * from Product where productId in  ( select distinct productIdFk from RateBasedRule where rateBasedRuleGroupIdFk=?1 )", Product.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getProductListFromRateBasedRule is null");
            return null;
        }
    }

    public List<DealerProduct> createDealerProdList(DealerRating dr) {
        try {
            LOGGER.info("in getDealerProdList, dealerName=" + dr.getDealerName());
            Dealer dealer = getDealerByName(dr.getDealerName());
            Integer rateBasedRuleGroupIdFk = dealer.getRateVisibilityIdFk().getRateVisibilityId();
            LOGGER.info("ratevisibilityId=" + rateBasedRuleGroupIdFk);
            /*
            List<Product> dpList =  getProductListFromRateBasedRule(rateBasedRuleGroupIdFk);
            List<DealerProduct> dlpList = new ArrayList<>();
            for( Product prod : dpList ) {
                dlpList.add(new DealerProduct(prod.getProductId(), prod.getProductName(), null, null));
            } 
            LOGGER.info("size of dlpList=" + dlpList.size());
            return dlpList;
             */

            List<RateBasedRule> rbrList = getRateBasedRuleByFkId(rateBasedRuleGroupIdFk);

            Integer productId;
            Integer currProductId = null;
            List<DealerProduct> dprodList = new ArrayList<>();
            //List<DealerPlan> dplanList = new ArrayList<>();
            //
            HashMap<String, DealerPlan> dplanList = new HashMap<>();
            //
            List<String> classCodeList = new ArrayList<>();
            List<Integer> termIdList = new ArrayList<>();
            boolean termIdDone = false;
            boolean classCodeDone = false;
            boolean isSameProduct = true;
            Integer currPlanId = null;
            Integer planId;
            for (RateBasedRule rbr : rbrList) {
                productId = rbr.getProductIdFk().getProductId();
                LOGGER.info("productId=" + productId);
                LOGGER.info("currProductId=" + currProductId);
                if (currProductId == null || currProductId.intValue() != productId.intValue()) {   // starts new product
                    isSameProduct = false;
                    if (currProductId != null) {
                        LOGGER.info("size of dplanList=" + dplanList.size());
                        //if (dplanList.size() > 0) {
                        // add the previous plan when productId changes, missed during the test
                        //DealerPlan dpnew = new DealerPlan(currPlanId, getPlanById(currPlanId).getPlanName(), classCodeList, termIdList);
                        if (currPlanId != null) {
                            LOGGER.info("add the following currPlanId to the list=" + currPlanId);
                            dplanList.put(getPlanById(currPlanId).getPlanName(), new DealerPlan(currPlanId, getPlanById(currPlanId).getPlanName(), classCodeList, termIdList));
                        }
                        dprodList.add(new DealerProduct(currProductId, getProductById(currProductId).getProductName(), null, "", null, null, null, null, null, null, null, null, dplanList));
                        //}
                    }
                    currProductId = productId;
                    dplanList = new HashMap<>();   // reset for new product
                    termIdList = new ArrayList<>();
                    classCodeList = new ArrayList<>();
                    //currPlanId = null;
                } else {
                    isSameProduct = true;
                }

                if (rbr.getPlanIdFk() != null) {
                    planId = rbr.getPlanIdFk().getPlanId();
                } else {
                    // get all the plan under this product
                    List<PlanTable> planList = getPlanByProdId(productId);
                    LOGGER.info("size of planList=" + planList.size());
                    for (PlanTable pt : planList) {
                        LOGGER.info("planId=" + pt.getPlanId());
                        classCodeList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
                        termIdList = getTermIdListByPlanId(pt.getPlanId());
                        dplanList.put(pt.getPlanName(), new DealerPlan(pt.getPlanId(), pt.getPlanName(), classCodeList, termIdList));
                    }
                    /*
                    dprodList.add(new DealerProduct(productId, getProductById(productId).getProductName(), null, dplanList));
                    dplanList = new HashMap<>();  
                    termIdList = new ArrayList<>();
                    classCodeList = new ArrayList<>();
                     */
                    currPlanId = null;
                    continue;
                }
                LOGGER.info("currPlanId=" + currPlanId);
                LOGGER.info("planId=" + planId);
                if (currPlanId == null || currPlanId.intValue() != planId.intValue()) { // starts new plan
                    if (currPlanId != null && isSameProduct) {
                        LOGGER.info("size of termIdList=" + termIdList.size());
                        LOGGER.info("size of classCodeList=" + classCodeList.size());
                        String planName = getPlanById(currPlanId).getPlanName();
                        DealerPlan dpnew = new DealerPlan(currPlanId, getPlanById(currPlanId).getPlanName(), classCodeList, termIdList);
                        dplanList.put(planName, dpnew);
                        //dplanList.add(dpnew);
                    }
                    currPlanId = planId;
                    termIdList = new ArrayList<>();
                    classCodeList = new ArrayList<>();
                    // termIdFk
                    if (rbr.getTermIdFk() != null) {
                        Integer termId = rbr.getTermIdFk().getTermId();
                        LOGGER.info("11111111111111, termId=" + termId);
                        termIdList.add(termId);
                    } else {
                        termIdList = getTermIdListByPlanId(rbr.getPlanIdFk().getPlanId());
                        //termIdDone = true;
                    }
                    // classCode
                    String classCode = rbr.getClassCode();
                    if (classCode != null) {  // 6,5,4,3,2,1
                        classCodeList.add(classCode);
                    } else { // all the classCode 0-9
                        classCodeList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
                        //classCodeDone = true;
                    }
                } else {
                    if (rbr.getTermIdFk() != null) {
                        Integer termId = rbr.getTermIdFk().getTermId();
                        termIdList.add(termId);

                    }
                    // classCode
                    String classCode = rbr.getClassCode();
                    LOGGER.info("classCode=" + classCode);
                    if (classCode != null) {  // 6,5,4,3,2,1
                        if (classCodeList.size() == 10) {
                            LOGGER.info("has been set before with term!=null");
                            classCodeList = new ArrayList<>();
                        }
                        classCodeList.add(classCode);
                    }
                }
            }
            LOGGER.info("last productId=" + currProductId);
            LOGGER.info("currPlanId=" + currPlanId);
            // add the last plan during the test
            //dplanList.add(new DealerPlan(currPlanId, getPlanById(currPlanId).getPlanName(), classCodeList, termIdList));
            if (currPlanId != null) {
                dplanList.put(getPlanById(currPlanId).getPlanName(), new DealerPlan(currPlanId, getPlanById(currPlanId).getPlanName(), classCodeList, termIdList));
            }
            LOGGER.info("size of dplanList=" + dplanList.size());
            dprodList.add(new DealerProduct(currProductId, getProductById(currProductId).getProductName(), null, "", null, null, null, null, null, null, null, null, dplanList));

            return dprodList;

        } catch (NoResultException e) {
            LOGGER.info("getProductByDealerName is null");
            return null;
        }
    }

    public DtLedgerType getDtLedgerTypeById(Integer id) {
        try {
            return em.createNamedQuery("DtLedgerType.findByLedgerTypeId", DtLedgerType.class).setParameter("ledgerTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtLedgerTypeById is null");
            return null;
        }
    }

    public List<DtLedgerType> getDtLedgerTypeByAll() {
        try {
            return (List<DtLedgerType>) em.createNamedQuery("DtLedgerType.findAll", DtLedgerType.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtLedgerTypeByAll is null");
            return null;
        }
    }

    public TreeMap<String, String> loadStateMap() {
        TreeMap<String, String> stateMap = new TreeMap<>();
        stateMap.put("AK", "AK");
        stateMap.put("AL", "AL");
        stateMap.put("AR", "AR");
        stateMap.put("AZ", "AZ");
        stateMap.put("CA", "CA");
        stateMap.put("CO", "CO");
        stateMap.put("CT", "CT");
        stateMap.put("DE", "DE");
        stateMap.put("FL", "FL");
        stateMap.put("GA", "GA");
        stateMap.put("HI", "HI");
        stateMap.put("IA", "IA");
        stateMap.put("ID", "ID");
        stateMap.put("IL", "IL");
        stateMap.put("IN", "IN");
        stateMap.put("IA", "IA");
        stateMap.put("KS", "KS");
        stateMap.put("KY", "KY");
        stateMap.put("LA", "LA");
        stateMap.put("ME", "ME");
        stateMap.put("MD", "MD");
        stateMap.put("MA", "MA");
        stateMap.put("MI", "MI");
        stateMap.put("MN", "MN");
        stateMap.put("MS", "MS");
        stateMap.put("MO", "MO");
        stateMap.put("MT", "MT");
        stateMap.put("NE", "NE");
        stateMap.put("NV", "NV");
        stateMap.put("NH", "NH");
        stateMap.put("NJ", "NJ");
        stateMap.put("NM", "NM");
        stateMap.put("NY", "NY");
        stateMap.put("NC", "NC");
        stateMap.put("ND", "ND");
        stateMap.put("OH", "OH");
        stateMap.put("OK", "OK");
        stateMap.put("OR", "OR");
        stateMap.put("PA", "PA");
        stateMap.put("RI", "RI");
        stateMap.put("SC", "SC");
        stateMap.put("SD", "SD");
        stateMap.put("TN", "TN");
        stateMap.put("TX", "TX");
        stateMap.put("UT", "UT");
        stateMap.put("VT", "VT");
        stateMap.put("VA", "VA");
        stateMap.put("WA", "WA");
        stateMap.put("WV", "WV");
        stateMap.put("WI", "WI");
        stateMap.put("WY", "WY");
        return stateMap;
    }

    public Integer insertAddress(Address addr) {
        Integer addressId;

        try {
            //utx.begin();

            LOGGER.info("in insertAddress, line1=" + addr.getAddress1());

            Integer akidFk = null;
            Integer regionIdFk = null;
            if (addr.getAccountKeeperIdFk() != null) {
                akidFk = addr.getAccountKeeperIdFk();
            }
            if (addr.getRegionIdFk() != null) {
                regionIdFk = addr.getRegionIdFk().getRegionId();
            }

            Query query = em.createNativeQuery("insert into Address (addressName, attentionTo, address1, address2, city, zipCode, phoneNumber, extension, fax, accountKeeperIdFk, regionIdFk, addressTypeIdFk) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12 )");
            query.setParameter(1, addr.getAddressName());
            query.setParameter(2, addr.getAttentionTo());
            query.setParameter(3, addr.getAddress1());
            query.setParameter(4, addr.getAddress2());
            query.setParameter(5, addr.getCity());
            query.setParameter(6, addr.getZipCode());
            query.setParameter(7, addr.getPhoneNumber());
            query.setParameter(8, addr.getExtension());
            query.setParameter(9, addr.getFax());
            query.setParameter(10, akidFk);
            query.setParameter(11, regionIdFk);
            query.setParameter(12, addr.getAddressTypeIdFk());
            int ret = query.executeUpdate();

            /*em.persist(addr);
            addressId = getLatestInsertedId("Address");
            utx.commit();
            return addressId;*/
            if (ret == 1) {
                addressId = getLatestInsertedId("Address");
                //utx.commit();
                return addressId;

            } else {
                //utx.rollback();
                return null;
            }

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages=" + violation.getMessage() + " key=" + key);
                }

            }

            return null;
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            return null;

        }
    }

    public AccountKeeperType getAccountKeeperTypeByName(String name) {
        LOGGER.info("in AccountKeeperType, name=" + name);
        try {
            return (AccountKeeperType) em.createNativeQuery("SELECT * FROM AccountKeeperType  WHERE accountKeeperTypeDesc =?1", AccountKeeperType.class).setParameter(1, name).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperTypeByName is null");
            return null;
        }
    }

    public AccountKeeperType getAccountKeeperTypeById(Integer id) {
        try {
            return (AccountKeeperType) em.createNamedQuery("AccountKeeperType.findByAccountKeeperTypeId", AccountKeeperType.class).setParameter("accountKeeperTypeId", id).getSingleResult();

        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperTypeById is null");
            return null;
        }
    }

    public UserType getUserTypeByDesc(String desc) {
        LOGGER.info("in getUserTypeByDesc, desc=" + desc);
        try {
            return em.createNamedQuery("UserType.findByDescription", UserType.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getUserTypeByDesc is null");
            return null;
        }
    }

    public void saveUser(Users user, Integer uid) {
        LOGGER.info("in claimisc saveUser, id=" + user.getUserName());
        try {

            utx.begin();

            if (uid == null) {

                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, emailAddress, emailOptIn, systemCode, taxId, accountKeeperTypeIdFk, billingAddressIdFk, physicalAddressIdFk) values (?1, ?2, ?3,?4,?5,?6,?7, ?8, ?9)");

                Integer accountKeeperTypeIdFk = 4;

                query.setParameter(1, user.getFullName());
                query.setParameter(2, true);
                query.setParameter(3, user.getEmail());
                query.setParameter(4, user.getEmailOptIn());
                query.setParameter(5, user.getSystemCode());
                query.setParameter(6, user.getTaxId());
                query.setParameter(7, accountKeeperTypeIdFk);
                query.setParameter(8, null);
                query.setParameter(9, null);
                int ret = query.executeUpdate();

                if (ret == 1) {
                    utx.commit();

                    utx.begin();
                    Integer akid = getLatestInsertedId("AccountKeeper");
                    LOGGER.info("akid=" + akid);

                    UserType ut = getUserTypeByDesc(user.getUserType());
                    LOGGER.info("ind=" + ut.getUserTypeInd());

                    Query q = em.createNativeQuery("insert into UserMember(userMemberId, userName, userTypeInd, isFiltered ) values (?1, ?2, ?3, ?4)");

                    q.setParameter(1, akid);
                    q.setParameter(2, user.getUserName());
                    q.setParameter(3, ut.getUserTypeInd());
                    q.setParameter(4, user.getIsFiltered());

                    LOGGER.info("2=" + user.getUserName());
                    LOGGER.info("3=" + ut.getUserTypeInd());

                    q.executeUpdate();

                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }

            } else {    // update
                UserMember um = getUserMemberById(uid);
                AccountKeeper ak = getAccountKeeperById(uid);

                um.setUserName(user.getUserName());
                um.setIsFiltered(user.getIsFiltered());

                em.merge(um);

                ak.setAccountKeeperName(user.getFullName());
                ak.setEmailAddress(user.getEmail());
                ak.setEmailOptIn(user.getEmailOptIn());
                ak.setTaxId(user.getTaxId());
                ak.setSystemCode(user.getSystemCode());

                em.merge(ak);

            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public MyAddress createAddress(MyAddress myAddress) {
        MyAddress ma = new MyAddress();
        String line1 = myAddress.getPaddr().getAddress1();
        LOGGER.info("line1=" + line1);
        Address paddr = myAddress.getPaddr();
        Address baddr = myAddress.getBaddr();
        CfRegion rp;
        CfRegion rb;
        Integer paddrFkId = null;
        Integer baddrFkId = null;
        String regionCodeP = myAddress.getRegionP();

        if (line1 != null && line1.length() > 0) {  // adding Address and Region information
            if (regionCodeP.length() > 0) {
                LOGGER.info("regionCodeP=" + regionCodeP);
                rp = getRegionByRegionCode(regionCodeP);
                paddr.setRegionIdFk(getRegionById(rp.getRegionId()));
            }
            paddr.setAddressName("Physical");
            paddrFkId = insertAddress(paddr);
            LOGGER.info("paddrFkId=" + paddrFkId);
            LOGGER.info("paddr.line1=" + paddr.getAddress1());
            if (myAddress.getSameAsPaddr()) {
                baddrFkId = paddrFkId;
            } else {
                String regionCodeB = myAddress.getRegionB();
                if (regionCodeB.length() > 0) {
                    rb = getRegionByRegionCode(regionCodeB);
                    baddr.setRegionIdFk(getRegionById(rb.getRegionId()));
                }
                baddr.setAddressName("Billing");
                baddrFkId = insertAddress(baddr);
                LOGGER.info("baddrFkId=" + baddrFkId);
            }
        }
        ma.setPaddrFkId(paddrFkId);
        ma.setBaddrFkId(baddrFkId);

        return ma;
    }

    public void saveDealer(Dealers dealer, Integer dealerId) {
        LOGGER.info("in claimmisc saveDealer, id=" + dealerId);
        try {

            utx.begin();

            // no matter new or old customer, update the addres anyway
            MyAddress ma = createAddress(dealer.getMyAddress());

            if (dealerId == null) { // new dealer
                Integer remitRuleIdFk = null;
                if (dealer.getRemitRule() != null) {
                    remitRuleIdFk = getCfRemitRuleByName(dealer.getRemitRule()).getRemitRuleId();
                }
                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, emailAddress, emailOptIn, systemCode, taxId, accountKeeperTypeIdFk, billingAddressIdFk, physicalAddressIdFk, legalName, remitRuleIdFk, enteredByIdFk, enteredDate) values (?1, ?2, ?3,?4,?5,?6,?7, ?8, ?9, ?10, ?11, ?12, ?13)");

                Integer accountKeeperTypeIdFk = 1;
                //String dealerName = getAccountKeeperById(dealer.getDealerId()).getAccountKeeperName();
                Integer enteredByIdFk = getUserMemberByName(getCurrentUser()).getUserMemberId();

                query.setParameter(1, dealer.getDealerName());
                query.setParameter(2, true);
                query.setParameter(3, dealer.getEmail());
                query.setParameter(4, dealer.getEmailOptIn());
                query.setParameter(5, dealer.getSystemCode());
                query.setParameter(6, dealer.getTaxId());
                query.setParameter(7, accountKeeperTypeIdFk);
                query.setParameter(8, ma.getBaddrFkId());
                query.setParameter(9, ma.getPaddrFkId());
                query.setParameter(10, dealer.getLegalName());
                query.setParameter(11, remitRuleIdFk);
                query.setParameter(12, enteredByIdFk);
                query.setParameter(13, getCurrnetDateTime());
                int ret = query.executeUpdate();

                if (ret == 1) {
                    //utx.commit();

                    //utx.begin();
                    Integer akid = getLatestInsertedId("AccountKeeper");
                    LOGGER.info("akid=" + akid);

                    query = em.createNativeQuery("insert into DisbursementCenter(disbursementType) values (?1)");
                    query.setParameter(1, "[Direct Disbursements]");

                    query.executeUpdate();

                    int DisbursementCenterIdFk = getLatestInsertedId("DisbursementCenter");
                    LOGGER.info("DisbursementCenterIdFk=" + DisbursementCenterIdFk);

                    Integer dealerGroupIdFk = null;
                    if (!dealer.getDealerGroup().equalsIgnoreCase("None")) {
                        dealerGroupIdFk = getAccountKeeperByNameType(dealer.getDealerGroup(), 2).getAccountKeeperId();
                    }
                    LOGGER.info("dealerGroupIdFk=" + dealerGroupIdFk);
                    Integer agentIdFk = null;
                    LOGGER.info("agent=" + dealer.getDirectAgent());

                    if (!dealer.getDirectAgent().equalsIgnoreCase("None")) {
                        //agentIdFk = getAccountKeeperByNameType(dealer.getDirectAgent(), 3).getAccountKeeperId();
                        agentIdFk = Integer.valueOf(dealer.getDirectAgent());
                    }

                    LOGGER.info("agentIdFk=" + agentIdFk);

                    Integer dealerTypeIdFk = null;
                    if (dealer.getDealerType().length() > 0) {
                        dealerTypeIdFk = getDtDealerTypeByName(dealer.getDealerType()).getDealerTypeId();
                    }
                    LOGGER.info("dealerTypeIdFk=" + dealerTypeIdFk);

                    Query q = em.createNativeQuery("insert into Dealer(dealerId, producerId, clientCodeIdFk, DisbursementCenterIdFk,  insurerIdFk, dealerGroupIdFk, agentIdFk, rateVisibilityIdFk, effectiveDate, dealerTypeIdFk ) values (?1, ?2, ?3, ?4,?5, ?6, ?7, ?8, ?9, ?10)");

                    q.setParameter(1, akid);
                    q.setParameter(2, dealer.getProducerId());
                    q.setParameter(3, null);
                    q.setParameter(4, DisbursementCenterIdFk);
                    q.setParameter(5, null);
                    q.setParameter(6, dealerGroupIdFk);
                    q.setParameter(7, agentIdFk);
                    q.setParameter(8, null);
                    q.setParameter(9, dealer.getEffectiveDate());
                    q.setParameter(10, dealerTypeIdFk);
                    ret = q.executeUpdate();
                    LOGGER.info("ret of insert = " + ret);
                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }
            } else {    // updating dealer
                AccountKeeper ak = getAccountKeeperById(dealerId);
                Dealer ud = getDealerById(dealerId);

                ud.setDealerNumber(dealer.getDealerNumber());
                em.merge(ud);
                utx.commit();

                ak.setEmailAddress(dealer.getEmail());
                ak.setEmailOptIn(dealer.getEmailOptIn());
                ak.setSystemCode(dealer.getSystemCode());
                ak.setTaxId(dealer.getTaxId());
                ak.setLegalName(dealer.getLegalName());
                em.merge(ak);

            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveRV(RV rv, Integer rvId) {
        LOGGER.info("in  saveRV, id=" + rvId);
        try {

            utx.begin();

            // no matter new or old customer, update the addres anyway
            //MyAddress ma = createAddress(dealer.getMyAddress());
            if (rvId == null) {
                Integer remitRuleIdFk = null;

                Query query = em.createNativeQuery("insert into RateBasedRuleGroup (name, rateBasedRuleGroupTypeInd) values (?1, ?2)");

                Integer rateBasedRuleGroupTypeInd = 1;

                query.setParameter(1, rv.getName());
                query.setParameter(2, rateBasedRuleGroupTypeInd);

                int ret = query.executeUpdate();

                if (ret == 1) {
                    //utx.commit();

                    //utx.begin();
                    Integer rateBasedRuleGroupId = getLatestInsertedId("RateBasedRuleGroup");
                    LOGGER.info("rateBasedRuleGroupId=" + rateBasedRuleGroupId);

                    Integer allowedProductTypes = 15;

                    query = em.createNativeQuery("insert into RateVisibility(rateVisibilityId, description, allowedProductTypes, deletedInd, effectiveDate, expireDate) values (?1, ?2, ?3, ?4, ?5, ?6)");

                    query.setParameter(1, rateBasedRuleGroupId);
                    query.setParameter(2, rv.getDescription());
                    query.setParameter(3, allowedProductTypes);
                    query.setParameter(4, 0);
                    query.setParameter(5, rv.getEffectiveDate());
                    query.setParameter(6, rv.getExpiredDate());

                    ret = query.executeUpdate();

                    LOGGER.info("ret of insert = " + ret);
                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }
            } else {

                RateBasedRuleGroup rbrg = getRateBasedRuleGroupById(rvId);
                RateVisibility pv = getRateVisibilityById(rvId);
                rbrg.setName(rv.getName());
                em.merge(rbrg);

                pv.setDescription(rv.getDescription());
                pv.setDeletedInd(rv.getDeletedInd());
                pv.setEffectiveDate(rv.getEffectiveDate());
                pv.setExpireDate(rv.getExpiredDate());
                em.merge(pv);
            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

    }
    
    public void deleteRateBasedRule(Integer rvId) {
        try {
            LOGGER.info("in deleteRateBasedRule, rvId=" + rvId);

            utx.begin();
            
            List<RateBasedRule> rbrList = getRateBasedRuleByFkId(rvId);
            if( rbrList.size() > 0 ) {
                for( RateBasedRule rbr : rbrList ) {
                    em.remove(rbr);
                }
            }

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();
            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveTreeNode(String node, Integer rvId) {
        LOGGER.info("in  saveTreeNode, node=" + node);
        LOGGER.info("in  saveTreeNode, rvId=" + rvId);
        try {

            utx.begin();

            String[] arr = node.split("\\|");

            Integer programId = null;
            Integer productId = null;
            Integer planId = null;
            Integer termId = null;
            Integer limitId = null;
            String classCode = null;

            Integer i = 0;
            for (String s : arr) {
                switch (i) {
                    case 0:
                        programId = Integer.parseInt(s);
                        break;
                    case 1:
                        productId = Integer.parseInt(s);
                        break;
                    case 2:
                        planId = Integer.parseInt(s);
                        break;
                    case 3:
                        termId = Integer.parseInt(s);
                        break;
                    case 4:
                        limitId = Integer.parseInt(s);
                        break;
                    case 5:
                        classCode = s;
                        break;
                }
                ++i;
            }
           

            Query query = em.createNativeQuery("insert into RateBasedRule (rateBasedRuleGroupIdFk, programIdFk, productIdFk, planIdFk, termIdFk, limitIdFk, classCode) values (?1, ?2, ?3, ?4, ?5, ?6, ?7 )");

            query.setParameter(1, rvId);
            query.setParameter(2, programId);
            query.setParameter(3, productId);
            query.setParameter(4, planId);
            query.setParameter(5, termId);
            query.setParameter(6, limitId);
            query.setParameter(7, classCode);

            int ret = query.executeUpdate();

            LOGGER.info("ret of insert = " + ret);

            if (ret == 1) {
                utx.commit();

            } else {
                LOGGER.info("rollback");
                utx.rollback();
            }

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

    }

    public void saveDisbursement(DetailedDisbursements dd, Integer ddId) {

        LOGGER.info("in construction........ saveDisbursement, id=" + ddId);
        /*
        try {

            utx.begin();

            
            
            if (ddId == null) { // new disbursement
                Integer remitRuleIdFk  = null;
                if( dealer.getRemitRule() != null ) {
                 remitRuleIdFk = getCfRemitRuleByName(dealer.getRemitRule()).getRemitRuleId();
                }
                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, emailAddress, emailOptIn, systemCode, taxId, accountKeeperTypeIdFk, billingAddressIdFk, physicalAddressIdFk, legalName, remitRuleIdFk, enteredByIdFk, enteredDate) values (?1, ?2, ?3,?4,?5,?6,?7, ?8, ?9, ?10, ?11, ?12, ?13)");

                Integer accountKeeperTypeIdFk = 1;
                Integer enteredByIdFk = getUserMemberByName(getCurrentUser()).getUserMemberId();

                query.setParameter(1, dealer.getDealerName());
                query.setParameter(2, true);
                query.setParameter(3, dealer.getEmail());
                query.setParameter(4, dealer.getEmailOptIn());
                query.setParameter(5, dealer.getSystemCode());
                query.setParameter(6, dealer.getTaxId());
                query.setParameter(7, accountKeeperTypeIdFk);
                query.setParameter(8, ma.getBaddrFkId());
                query.setParameter(9, ma.getPaddrFkId());
                query.setParameter(10, dealer.getLegalName());
                query.setParameter(11, remitRuleIdFk);
                query.setParameter(12, enteredByIdFk);
                query.setParameter(13, getCurrnetDateTime());
                int ret = query.executeUpdate();

                if (ret == 1) {
                    //utx.commit();

                    //utx.begin();
                    Integer akid = getLatestInsertedId("AccountKeeper");
                    LOGGER.info("akid=" + akid);

                    query = em.createNativeQuery("insert into DisbursementCenter(disbursementType) values (?1)");
                    query.setParameter(1, "[Direct Disbursements]");

                    query.executeUpdate();

                    int DisbursementCenterIdFk = getLatestInsertedId("DisbursementCenter");
                    LOGGER.info("DisbursementCenterIdFk=" + DisbursementCenterIdFk);

                    Integer dealerGroupIdFk = null;
                    if (!dealer.getDealerGroup().equalsIgnoreCase("None")) {
                        dealerGroupIdFk = getAccountKeeperByNameType(dealer.getDealerGroup(), 2).getAccountKeeperId();
                    }
                    LOGGER.info("dealerGroupIdFk=" + dealerGroupIdFk);
                    Integer agentIdFk = null;
                    LOGGER.info("agent=" + dealer.getDirectAgent());

                    if (!dealer.getDirectAgent().equalsIgnoreCase("None")) {
                        //agentIdFk = getAccountKeeperByNameType(dealer.getDirectAgent(), 3).getAccountKeeperId();
                        agentIdFk = Integer.valueOf(dealer.getDirectAgent());
                    }

                    LOGGER.info("agentIdFk=" + agentIdFk);

                    Integer dealerTypeIdFk = null;
                    if (dealer.getDealerType().length() > 0) {
                        dealerTypeIdFk = getDtDealerTypeByName(dealer.getDealerType()).getDealerTypeId();
                    }
                    LOGGER.info("dealerTypeIdFk=" + dealerTypeIdFk);

                    Query q = em.createNativeQuery("insert into Dealer(dealerId, producerId, clientCodeIdFk, DisbursementCenterIdFk,  insurerIdFk, dealerGroupIdFk, agentIdFk, rateVisibilityIdFk, effectiveDate, dealerTypeIdFk ) values (?1, ?2, ?3, ?4,?5, ?6, ?7, ?8, ?9, ?10)");

                    q.setParameter(1, akid);
                    q.setParameter(2, dealer.getProducerId());
                    q.setParameter(3, null);
                    q.setParameter(4, DisbursementCenterIdFk);
                    q.setParameter(5, null);
                    q.setParameter(6, dealerGroupIdFk);
                    q.setParameter(7, agentIdFk);
                    q.setParameter(8, null);
                    q.setParameter(9, dealer.getEffectiveDate());
                    q.setParameter(10, dealerTypeIdFk);
                    q.executeUpdate();

                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }
            } else {    // updating dealer
                AccountKeeper ak = getAccountKeeperById(dealerId);
                Dealer ud = getDealerById(dealerId);

                ud.setDealerNumber(dealer.getDealerNumber());
                em.merge(ud);
                utx.commit();

                ak.setEmailAddress(dealer.getEmail());
                ak.setEmailOptIn(dealer.getEmailOptIn());
                ak.setSystemCode(dealer.getSystemCode());
                ak.setTaxId(dealer.getTaxId());
                ak.setLegalName(dealer.getLegalName());
                em.merge(ak);

            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
         */
    }

    public List<DetailedDisbursements> getDetaildDisbursement(Integer dealerId) {
        List<DetailedDisbursements> ddList = new ArrayList<>();

        return ddList;
    }

    /**
     *
     * @param rf
     * @param rfid
     */
    public void saveRepairFacility(RepairFacilities rf, Integer rfid) {
        LOGGER.info("in saveRepairFacility, id=" + rfid);
        LOGGER.info("in saveRepairFacility, dealerId=" + rf.getDealerId());
        LOGGER.info("in saveRepairFacility, dealerGroupId=" + rf.getDealerGroupId());

        try {

            utx.begin();

            // no matter new or old customer, update the addres anyway
            MyAddress ma = createAddress(rf.getMyAddress());

            if (rfid == null) { // new RF

                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, emailAddress, emailOptIn, taxId, accountKeeperTypeIdFk, billingAddressIdFk, physicalAddressIdFk, enteredByIdFk, enteredDate) values (?1, ?2, ?3,?4,?5,?6,?7, ?8, ?9, ?10)");

                Integer accountKeeperTypeIdFk = 7;
                //String dealerName = getAccountKeeperById(dealer.getDealerId()).getAccountKeeperName();
                Integer enteredByIdFk = getUserMemberByName(getCurrentUser()).getUserMemberId();

                query.setParameter(1, rf.getName());
                query.setParameter(2, true);
                query.setParameter(3, rf.getEmail());
                query.setParameter(4, rf.getEmailOptIn());
                query.setParameter(5, rf.getTaxId());
                query.setParameter(6, accountKeeperTypeIdFk);
                query.setParameter(7, ma.getBaddrFkId());
                query.setParameter(8, ma.getPaddrFkId());
                query.setParameter(9, enteredByIdFk);
                query.setParameter(10, getCurrnetDateTime());
                int ret = query.executeUpdate();

                if (ret == 1) {
                    //utx.commit();

                    //utx.begin();
                    Integer akid = getLatestInsertedId("AccountKeeper");
                    LOGGER.info("akid=" + akid);

                    // RepairFacility
                    LOGGER.info("insert into RepairFacility");
                    query = em.createNativeQuery("insert into RepairFacility (repairFacilityId, partWarrantyMonths, partWarrantyMiles, laborTaxPct, partsTaxPct, laborRate ) values (?1, ?2, ?3, ?4, ?5, ?6 )");
                    query.setParameter(1, akid);
                    query.setParameter(2, rf.getPartWarrantyMonths());
                    query.setParameter(3, rf.getPartWarrantyMiles());
                    query.setParameter(4, rf.getLaborTax());
                    query.setParameter(5, rf.getPartTax());
                    query.setParameter(6, rf.getLaborRate());
                    query.executeUpdate();

                    if (rf.getDealerId() != null) {
                        // RepairFacilityToDealerRel
                        LOGGER.info("insert into RepairFacilityToDealerRel");
                        query = em.createNativeQuery("insert into RepairFacilityToDealerRel (repairFacilityId, dealerId ) values (?1, ?2 )");
                        query.setParameter(1, akid);
                        query.setParameter(2, rf.getDealerId());
                        query.executeUpdate();
                    }

                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }
            } else {    // updating RepairFacility
                AccountKeeper ak = getAccountKeeperById(rfid);
                RepairFacility ud = getRepairFacilityById(rfid);

                ud.setLaborRate(rf.getLaborRate());
                ud.setLaborTaxPct(rf.getLaborTax());
                ud.setPartsTaxPct(rf.getPartTax());
                ud.setPartWarrantyMiles(rf.getPartWarrantyMiles());
                ud.setPartWarrantyMonths(rf.getPartWarrantyMonths());
                em.merge(ud);
                utx.commit();

                ak.setEmailAddress(rf.getEmail());
                ak.setEmailOptIn(rf.getEmailOptIn());
                ak.setTaxId(rf.getTaxId());
                em.merge(ak);

            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveDealerGroup(DealerGroups dg, Integer dealerGroupId) {
        LOGGER.info("in claimmisc saveDealerGroup, dgid=" + dealerGroupId);
        try {

            utx.begin();

            // no matter new or old customer, update the addres anyway
            MyAddress ma = createAddress(dg.getMyAddress());

            if (dealerGroupId == null) { // new dealerGroup

                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, systemCode, taxId, accountKeeperTypeIdFk, billingAddressIdFk, physicalAddressIdFk, remitRuleIdFk, enteredByIdFk, enteredDate) values (?1, ?2, ?3,?4,?5,?6,?7, ?8, ?9, ?10)");

                Integer accountKeeperTypeIdFk = 2;  // dealerGroupType
                LOGGER.info("rimitRule=" + dg.getRemitRule());
                CfRemitRule remitRule = getCfRemitRuleByDesc(dg.getRemitRule());
                //String dealerName = getAccountKeeperById(dealer.getDealerId()).getAccountKeeperName();
                Integer enteredByIdFk = getUserMemberByName(getCurrentUser()).getUserMemberId();

                query.setParameter(1, dg.getDealerGroupName());
                query.setParameter(2, true);
                query.setParameter(3, dg.getSystemCode());
                query.setParameter(4, dg.getTaxId());
                query.setParameter(5, accountKeeperTypeIdFk);
                query.setParameter(6, null);
                query.setParameter(7, null);
                query.setParameter(8, remitRule.getRemitRuleId());
                query.setParameter(9, enteredByIdFk);
                query.setParameter(10, getCurrnetDateTime());

                int ret = query.executeUpdate();

                if (ret == 1) {

                    Integer akid = getLatestInsertedId("AccountKeeper");
                    LOGGER.info("akid=" + akid);

                    query = em.createNativeQuery("insert into DisbursementCenter(disbursementType) values (?1)");
                    query.setParameter(1, "[Direct Disbursements]");

                    int retc = query.executeUpdate();
                    if (retc == 1) {

                        int DisbursementCenterIdFk = getLatestInsertedId("DisbursementCenter");
                        LOGGER.info("DisbursementCenterIdFk=" + DisbursementCenterIdFk);

                        Query q = em.createNativeQuery("insert into DealerGroup(dealerGroupId, DisbursementCenterIdFk ) values (?1, ?2)");

                        q.setParameter(1, akid);
                        q.setParameter(2, DisbursementCenterIdFk);

                        q.executeUpdate();
                    } else {
                        LOGGER.info("rollback, intc");
                        utx.rollback();
                    }

                } else {
                    LOGGER.info("rollback");
                    utx.rollback();
                }
            } else {    // updating dealerGroup
                AccountKeeper ak = getAccountKeeperById(dealerGroupId);

                ak.setEmailAddress(dg.getEmail());
                ak.setEmailOptIn(dg.getEmailOptIn());
                ak.setSystemCode(dg.getSystemCode());
                ak.setTaxId(dg.getTaxId());
                em.merge(ak);

            }

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public DriveType getDriveTypeById(Integer id) {
        try {
            return em.createNamedQuery("DriveType.findByDriveTypeId", DriveType.class).setParameter("driveTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDriveTypeById is null");
            return null;
        }
    }

    public VehicleType getVehicleTypeById(Integer id) {
        try {
            return em.createNamedQuery("VehicleType.findByVehicleTypeId", VehicleType.class).setParameter("vehicleTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getVehicleTypeById is null");
            return null;
        }
    }

    public ClaimVehicleInfo getClaimVehicleInfo(String cno) {

        LOGGER.info("in getClaimVehicleInfo=" + cno);
        Contracts contract = getContractsByNo(cno);

        ClaimVehicleInfo claimVehicleInfo = new ClaimVehicleInfo();

        VinDesc vinDesc = getVinDescByContract(cno);

        String vehicleType = getVehicleTypeById(Integer.valueOf(vinDesc.getVehicleType())).getName();
        String driveType = getDriveTypeById(Integer.valueOf(vinDesc.getDriveType())).getName();

        claimVehicleInfo.setVinFull(contract.getVinFull());
        claimVehicleInfo.setMake(vinDesc.getMakeIdFk().getName());
        claimVehicleInfo.setModel(vinDesc.getModelIdFk().getName());
        claimVehicleInfo.setSeries(vinDesc.getSeriesIdFk().getName());
        claimVehicleInfo.setVehicleYear(vinDesc.getVehicleYear());
        claimVehicleInfo.setVehicleType(vehicleType);
        claimVehicleInfo.setCylinders(vinDesc.getCylinders());
        claimVehicleInfo.setCubicInchDisplacement(vinDesc.getCubicInchDisplacement());
        claimVehicleInfo.setCubicCentimeterDisplacement(vinDesc.getCubicCentimeterDisplacement());
        claimVehicleInfo.setLiterDisplacemant(vinDesc.getLiterDisplacemant());
        claimVehicleInfo.setVehicleWeightRating(vinDesc.getVehicleWeightRating());
        //claimVehicleInfo.setFuelType(vinDesc.getFuelType());
        claimVehicleInfo.setFuelType(vinDesc.getFuelTypeIdFk().getDescription());
        claimVehicleInfo.setFuelDelivery(vinDesc.getFuelDeliveryIdFk().getDescription());
        claimVehicleInfo.setTonRating(vinDesc.getTonRating());
        claimVehicleInfo.setDriveType(driveType);
        claimVehicleInfo.setBasicWarranty(vinDesc.getBasicManufacturerWarrantyMileage() + "/" + vinDesc.getBasicManufacturerWarrantyTerm());
        claimVehicleInfo.setPowertrainWarranty(vinDesc.getPowerTrainWarrantyMiles() + "/" + vinDesc.getPowerTrainWarrantyMonths());
        claimVehicleInfo.setRustWarranty(vinDesc.getRustWarrantyMiles() + "/" + vinDesc.getRustWarrantyMonths());

        return claimVehicleInfo;

    }

    public String convertVinPartialFromVinFull(String vinFull) {
        String vinPartial = vinFull.substring(0, 8) + "%" + vinFull.substring(9, 11) + "%";
        return vinPartial;
    }

    public ClaimVehicleInfo setClaimVehicleInfo(VinDesc vinDesc) {
        ClaimVehicleInfo claimVehicleInfo = new ClaimVehicleInfo();

        String vehicleType = getVehicleTypeById(Integer.valueOf(vinDesc.getVehicleType())).getName();
        String driveType = getDriveTypeById(Integer.valueOf(vinDesc.getDriveType())).getName();

        claimVehicleInfo.setMake(vinDesc.getMake());
        claimVehicleInfo.setModel(vinDesc.getModel());
        claimVehicleInfo.setSeries(vinDesc.getSeries());
        claimVehicleInfo.setVehicleYear(vinDesc.getVehicleYear());
        claimVehicleInfo.setVehicleType(vehicleType);
        claimVehicleInfo.setCylinders(vinDesc.getCylinders());
        claimVehicleInfo.setCubicInchDisplacement(vinDesc.getCubicInchDisplacement());
        claimVehicleInfo.setCubicCentimeterDisplacement(vinDesc.getCubicCentimeterDisplacement());
        claimVehicleInfo.setLiterDisplacemant(vinDesc.getLiterDisplacemant());
        claimVehicleInfo.setVehicleWeightRating(vinDesc.getVehicleWeightRating());
        claimVehicleInfo.setFuelType(vinDesc.getFuelType());
        claimVehicleInfo.setFuelDelivery(vinDesc.getFuelDelivery());
        claimVehicleInfo.setTonRating(vinDesc.getTonRating());
        claimVehicleInfo.setVehicleType(vinDesc.getVehicleType());
        claimVehicleInfo.setBasicWarranty(vinDesc.getBasicManufacturerWarrantyMileage() + "/" + vinDesc.getBasicManufacturerWarrantyTerm());
        claimVehicleInfo.setPowertrainWarranty(vinDesc.getPowerTrainWarrantyMiles() + "/" + vinDesc.getPowerTrainWarrantyMonths());
        claimVehicleInfo.setRustWarranty(vinDesc.getRustWarrantyMiles() + "/" + vinDesc.getRustWarrantyMonths());
        claimVehicleInfo.setDriveType(driveType);

        return claimVehicleInfo;

    }

    public ClaimVehicleInfo setClaimVehicleInfo(String vinFull) {
        VinDesc vinDesc = getVinDescByVinFull(vinFull);
        ClaimVehicleInfo cvi = setClaimVehicleInfo(vinDesc);
        return cvi;

    }

    public List<CashTransaction> searchTransactions(AcctTrans cs, int first, int pageSize) {
        LOGGER.info("in searchTransactions, Number=" + cs.getTransactionNumber() + ", sentDate=" + cs.getTransactionSent());
        try {
            List<CashTransaction> cl = new ArrayList<>();

            String qs = "select * from CashTransaction ";

            //LOGGER.info("qs=" + qs);
            String transNumber = "";
            String accountName = "";
            String contractNo = "";
            String claimNo = "";
            Boolean setAnd = false;
            Boolean setBoth = false;
            String type = "";
            String status = "";
            String amountStr = "";
            String amtApplied = "";
            String overUnder = "";
            String adminCompany = "";
            String payeeName = "";
            String legalName = "";
            String sentDate = "";
            String recDate = "";
            String entDate = "";
            String transId = "";

            if ((cs.getContractNo() != null && cs.getContractNo().length() > 0) || (cs.getClaimNo() != null && cs.getClaimNo().length() > 0) || (cs.getTransactionAmount() != null) || (cs.getAdminCompany() != null && cs.getAdminCompany().length() > 0)) {
                if (!setBoth) {
                    qs = "select a.* from CashTransaction a, Ledger b  where b.cashTransactionIdFk=a.cashTransactionId ";
                    setBoth = true;
                }
                if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                    Integer contractId = getContractsByNo(cs.getContractNo()).getContractId();
                    contractNo = " and b.relatedContractIdFk=" + contractId;
                }
                if (cs.getClaimNo() != null && cs.getClaimNo().length() > 0) {
                    Integer claimId = getClaimByNumber(cs.getClaimNo()).getClaimId();
                    claimNo = " and b.relatedClaimIdFk=" + claimId;
                }
                if (cs.getTransactionAmount() != null) {
                    amountStr = " and b.currentAmount like '%" + cs.getTransactionAmount() + "%' ";
                }
                if (cs.getAdminCompany() != null && cs.getAdminCompany().length() > 0) {
                    AdminCompany ac = getAdminCompanybyLike(cs.getAdminCompany());
                    if (ac != null) {
                        Integer adminFkId = ac.getAdminCompanyId();
                        amountStr = " and b.adminCompanyIdFk like'%" + adminFkId + "%' ";
                    } else {
                        amountStr = " and b.adminCompanyIdFk like'%" + -1 + "%' ";
                    }
                }
                setAnd = true;
            }

            if (cs.getTransactionNumber() != null && cs.getTransactionNumber().length() > 0) {
                if (setAnd) {
                    transNumber = "  and transactionNumber like  '%" + cs.getTransactionNumber() + "%'  ";
                } else {
                    transNumber = "  where transactionNumber like '%" + cs.getTransactionNumber() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionId() != null) {
                if (setAnd) {
                    transNumber = "  and cashTransactionId like  '%" + cs.getTransactionId() + "%'  ";
                } else {
                    transNumber = "  where cashTransactionId like '%" + cs.getTransactionId() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountName() != null && cs.getAccountName().length() > 0) {
                if (setAnd) {
                    accountName = " and payeeName like '%" + cs.getAccountName() + "%'  ";
                } else {
                    accountName = " where payeeName like '%" + cs.getAccountName() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionType() != null && cs.getTransactionType().length() > 0) {
                Integer typeInd = getDtcashTransactionTypeByDesc(cs.getTransactionType()).getCashTransactionTypeId();
                if (setAnd) {
                    type = " and cashTransactionTypeInd like '%" + typeInd + "%'  ";
                } else {
                    type = " where cashTransactionTypeInd like '%" + typeInd + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTrasactionStatus() != null && cs.getTrasactionStatus().length() > 0) {
                Integer statusInd = getDtcashTransactionStatusByDesc(cs.getTrasactionStatus()).getCashTransactionStatusId();
                if (setAnd) {
                    status = " and cashTransactionStatusInd like '%" + statusInd + "%'  ";
                } else {
                    status = " where cashTransactionStatusInd like '%" + statusInd + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAmountApplied() != null) {
                if (setAnd) {
                    amtApplied = " and appliedAmount like '%" + cs.getAmountApplied() + "%'  ";
                } else {
                    status = " where appliedAmount like '%" + cs.getAmountApplied() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getOverUnder() != null) {
                if (setAnd) {
                    overUnder = " and overUnder like '%" + cs.getOverUnder() + "%'  ";
                } else {
                    overUnder = " where overUnder like '%" + cs.getOverUnder() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountName() != null && cs.getAccountName().length() > 0) {
                if (setAnd) {
                    payeeName = " and payeeName like '%" + cs.getAccountName() + "%'  ";
                } else {
                    payeeName = " where payeeName like '%" + cs.getAccountName() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountLegal() != null && cs.getAccountLegal().length() > 0) {
                legalName = getAccountKeeperByLegal(cs.getAccountLegal()).getAccountKeeperName();
                if (setAnd) {
                    legalName = " and overUnder like '%" + cs.getOverUnder() + "%'  ";
                } else {
                    legalName = " where overUnder like '%" + cs.getOverUnder() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionSent() != null && cs.getTransactionSent().length() > 0) {
                String dt = getDateDashString(convSTDateWS(cs.getTransactionSent()));
                LOGGER.info("dt=" + dt);
                if (setAnd) {
                    sentDate = " and sentDate like '%" + dt + "%'  ";
                } else {
                    sentDate = " where sentDate like '%" + dt + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionReceived() != null && cs.getTransactionReceived().length() > 0) {
                String recdt = getDateDashString(convSTDateWS(cs.getTransactionReceived()));
                if (setAnd) {
                    recDate = " and sentDate like '%" + recdt + "%'  ";
                } else {
                    recDate = " where sentDate like '%" + recdt + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionEntered() != null && cs.getTransactionEntered().length() > 0) {
                String entdt = getDateDashString(convSTDateWS(cs.getTransactionEntered()));
                if (setAnd) {
                    entDate = " and sentDate like '%" + entdt + "%'  ";
                } else {
                    entDate = " where sentDate like '%" + entdt + "%'  ";
                }
                setAnd = true;
            }

            String endstr = " order by cashTransactionId desc";
            //LOGGER.info("endstr=" + endstr);

            String query = qs + transNumber + accountName + contractNo + claimNo + type + status + amountStr + amtApplied + overUnder + adminCompany + payeeName + sentDate + recDate + entDate + transId + endstr;

            LOGGER.info("query=" + query);

            // new
            if (em == null) {
                LOGGER.info("em is null, why");
            }

            Query q = em.createNativeQuery(query, CashTransaction.class);

            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return (List<CashTransaction>) q.getResultList();
            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchTransactions is null");
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Ledger getLedgerById(Integer id) {
        try {
            return em.createNamedQuery("Ledger.findByLedgerId", Ledger.class).setParameter("ledgerId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getLedgerById is null");
            return null;
        }
    }

    public List<Ledger> searchLedger(Integer id, int first, int pageSize) {
        LOGGER.info("in searchLedger, acctId=" + id);
        try {
            List<Ledger> cl = new ArrayList<>();

            String query = "select * from Ledger where CurrentAccountKeeperIdFk = " + id + " and ledgerTypeInd in ( 1,2,3,4,5,8,9,10,14)  and cashTransactionIdFk is null";

            Query q = em.createNativeQuery(query, Ledger.class);
            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return (List<Ledger>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.info("searchLedger is null");
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public int getSearchLedgerCount(Integer id) {
        LOGGER.info("in getSearchLedgerCount, id=" + id);
        try {
            List<Ledger> cl = new ArrayList<>();

            String query = "select count(*) from Ledger where CurrentAccountKeeperIdFk = " + id + " and ledgerTypeInd in ( 1,2,3,4,5,8,9,10,14)";

            Query q = em.createNativeQuery(query);

            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;

        } catch (NoResultException e) {
            LOGGER.info("getSearchLedgerCount is null");
            return 0;
        }

    }

    public List<Ledger> searchLedgerByTransId(Integer id, int first, int pageSize) {
        LOGGER.info("in searchLedgerByTransId, transactionId=" + id);
        try {
            List<Ledger> cl = new ArrayList<>();

            String query = "select * from Ledger where cashTransactionIdFk=" + id + " and ledgerTypeInd in ( 1,2,3,4,5,8,9,10,14)";

            Query q = em.createNativeQuery(query, Ledger.class);
            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return (List<Ledger>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.info("searchLedgerByTransId is null");
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public int getSearchLedgerByTransIdCount(Integer id) {
        LOGGER.info("in getSearchLedgerByTransIdCount, id=" + id);
        try {
            List<Ledger> cl = new ArrayList<>();

            String query = "select count(*) from Ledger where cashTransactionIdFk=" + id + " and ledgerTypeInd in ( 1,2,3,4,5,8,9,10,14)";

            Query q = em.createNativeQuery(query);

            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;

        } catch (NoResultException e) {
            LOGGER.info("getSearchLedgerByTransIdCount is null");
            return 0;
        }

    }

    public List<Ledger> getLedgerByAKFKId(Integer id) {
        try {
            return (List<Ledger>) em.createNativeQuery("select * from Ledger where CurrentAccountKeeperIdFk = ?1 and ledgerTypeInd in ( 1,2,3,4,5,8,9,10,14)", Ledger.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getLedgerByAKFKId is null");
            return null;
        }
    }

    public int getSearchTransactionsCount(AcctTrans cs) {
        LOGGER.info("in getSearchTransactionsCount, Number=" + cs.getTransactionNumber() + ", sentDate=" + cs.getTransactionSent());

        try {
            List<CashTransaction> cl = new ArrayList<>();

            String qs = "select count(*) from CashTransaction ";

            //LOGGER.info("qs=" + qs);
            String transNumber = "";
            String accountName = "";
            String contractNo = "";
            String claimNo = "";
            Boolean setAnd = false;
            Boolean setBoth = false;
            String type = "";
            String status = "";
            String amountStr = "";
            String amtApplied = "";
            String overUnder = "";
            String adminCompany = "";
            String payeeName = "";
            String legalName = "";
            String sentDate = "";
            String recDate = "";
            String entDate = "";
            String transId = "";

            if ((cs.getContractNo() != null && cs.getContractNo().length() > 0) || (cs.getClaimNo() != null && cs.getClaimNo().length() > 0) || (cs.getTransactionAmount() != null) || (cs.getAdminCompany() != null && cs.getAdminCompany().length() > 0)) {
                if (!setBoth) {
                    qs = "select count(*) from CashTransaction a, Ledger b  where b.cashTransactionIdFk=a.cashTransactionId ";
                    setBoth = true;
                }
                if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                    Integer contractId = getContractsByNo(cs.getContractNo()).getContractId();
                    contractNo = " and b.relatedContractIdFk=" + contractId;
                }
                if (cs.getClaimNo() != null && cs.getClaimNo().length() > 0) {
                    Integer claimId = getClaimByNumber(cs.getClaimNo()).getClaimId();
                    claimNo = " and b.relatedClaimIdFk=" + claimId;
                }
                if (cs.getTransactionAmount() != null) {
                    amountStr = " and b.currentAmount like '%" + cs.getTransactionAmount() + "%' ";
                }
                if (cs.getAdminCompany() != null && cs.getAdminCompany().length() > 0) {
                    AdminCompany ac = getAdminCompanybyLike(cs.getAdminCompany());
                    if (ac != null) {
                        Integer adminFkId = ac.getAdminCompanyId();
                        amountStr = " and b.adminCompanyIdFk like'%" + adminFkId + "%' ";
                    } else {
                        amountStr = " and b.adminCompanyIdFk like'%" + -1 + "%' ";
                    }
                }
                setAnd = true;
            }

            if (cs.getTransactionNumber() != null && cs.getTransactionNumber().length() > 0) {
                if (setAnd) {
                    transNumber = "  and transactionNumber like  '%" + cs.getTransactionNumber() + "%'  ";
                } else {
                    transNumber = "  where transactionNumber like '%" + cs.getTransactionNumber() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionId() != null) {
                if (setAnd) {
                    transNumber = "  and cashTransactionId like  '%" + cs.getTransactionId() + "%'  ";
                } else {
                    transNumber = "  where cashTransactionId like '%" + cs.getTransactionId() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountName() != null && cs.getAccountName().length() > 0) {
                if (setAnd) {
                    accountName = " and payeeName like '%" + cs.getAccountName() + "%'  ";
                } else {
                    accountName = " where payeeName like '%" + cs.getAccountName() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionType() != null && cs.getTransactionType().length() > 0) {
                Integer typeInd = getDtcashTransactionTypeByDesc(cs.getTransactionType()).getCashTransactionTypeId();
                if (setAnd) {
                    type = " and cashTransactionTypeInd like '%" + typeInd + "%'  ";
                } else {
                    type = " where cashTransactionTypeInd like '%" + typeInd + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTrasactionStatus() != null && cs.getTrasactionStatus().length() > 0) {
                Integer statusInd = getDtcashTransactionStatusByDesc(cs.getTrasactionStatus()).getCashTransactionStatusId();
                if (setAnd) {
                    status = " and cashTransactionStatusInd like '%" + statusInd + "%'  ";
                } else {
                    status = " where cashTransactionStatusInd like '%" + statusInd + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAmountApplied() != null) {
                if (setAnd) {
                    amtApplied = " and appliedAmount like '%" + cs.getAmountApplied() + "%'  ";
                } else {
                    status = " where appliedAmount like '%" + cs.getAmountApplied() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getOverUnder() != null) {
                if (setAnd) {
                    overUnder = " and overUnder like '%" + cs.getOverUnder() + "%'  ";
                } else {
                    overUnder = " where overUnder like '%" + cs.getOverUnder() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountName() != null && cs.getAccountName().length() > 0) {
                if (setAnd) {
                    payeeName = " and payeeName like '%" + cs.getAccountName() + "%'  ";
                } else {
                    payeeName = " where payeeName like '%" + cs.getAccountName() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getAccountLegal() != null && cs.getAccountLegal().length() > 0) {
                legalName = getAccountKeeperByLegal(cs.getAccountLegal()).getAccountKeeperName();
                if (setAnd) {
                    legalName = " and ??? like '%" + cs.getOverUnder() + "%'  ";
                } else {
                    legalName = " where  ??? like '%" + cs.getOverUnder() + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionSent() != null && cs.getTransactionSent().length() > 0) {
                String dt = getDateDashString(convSTDateWS(cs.getTransactionSent()));
                LOGGER.info("dt=" + dt);
                if (setAnd) {
                    sentDate = " and sentDate like '%" + dt + "%'  ";
                } else {
                    sentDate = " where sentDate like '%" + dt + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionReceived() != null && cs.getTransactionReceived().length() > 0) {
                String recdt = getDateDashString(convSTDateWS(cs.getTransactionReceived()));
                if (setAnd) {
                    recDate = " and sentDate like '%" + recdt + "%'  ";
                } else {
                    recDate = " where sentDate like '%" + recdt + "%'  ";
                }
                setAnd = true;
            }

            if (cs.getTransactionEntered() != null && cs.getTransactionEntered().length() > 0) {
                String entdt = getDateDashString(convSTDateWS(cs.getTransactionEntered()));
                if (setAnd) {
                    entDate = " and sentDate like '%" + entdt + "%'  ";
                } else {
                    entDate = " where sentDate like '%" + entdt + "%'  ";
                }
                setAnd = true;
            }

            //String query = qs + transNumber + accountName + contractNo + claimNo;
            String query = qs + transNumber + accountName + contractNo + claimNo + type + status + amountStr + amtApplied + overUnder + adminCompany + sentDate + recDate + entDate + payeeName + transId;

            LOGGER.info("query=" + query);

            Query q = em.createNativeQuery(query);

            Integer count = (Integer) q.getSingleResult();
            LOGGER.info("count=" + count);

            return count;
        } catch (NoResultException e) {
            LOGGER.info("getSearchTransactionsCount is null");
            return 0;
        }
    }

    public DtcashTransactionType getDtcashTransactionTypeById(Integer id) {
        try {
            return em.createNamedQuery("DtcashTransactionType.findByCashTransactionTypeId", DtcashTransactionType.class).setParameter("cashTransactionTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtcashTransactionTypeById is null");
            return null;
        }
    }

    public DtcashTransactionType getDtcashTransactionTypeByDesc(String desc) {
        try {
            return em.createNamedQuery("DtcashTransactionType.findByDescription", DtcashTransactionType.class).setParameter("description", desc).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtcashTransactionTypeByDesc is null");
            return null;
        }
    }

    public List<DtcashTransactionType> getDtcashTransactionTypeByAll() {
        try {
            return (List<DtcashTransactionType>) em.createNamedQuery("DtcashTransactionType.findAll", DtcashTransactionType.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtcashTransactionTypeByAll is null");
            return null;
        }
    }

    public DtcashTransactionStatus getDtcashTransactionStatusById(Integer id) {
        try {
            return em.createNamedQuery("DtcashTransactionStatus.findByCashTransactionStatusId", DtcashTransactionStatus.class).setParameter("cashTransactionStatusId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtcashTransactionStatusById is null");
            return null;
        }
    }

    public DtcashTransactionStatus getDtcashTransactionStatusByDesc(String desc) {
        try {
            return em.createNamedQuery("DtcashTransactionStatus.findByDescription", DtcashTransactionStatus.class).setParameter("description", desc).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getDtcashTransactionStatusByDesc is null");
            return null;
        }
    }

    public CashTransaction getCashTransactionByNumber(String num) {
        try {
            return em.createNamedQuery("CashTransaction.findByTransactionNumber", CashTransaction.class).setParameter("transactionNumber", num).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCashTransactionByNum is null");
            return null;
        }
    }

    public CashTransaction getCashTransactionById(Integer id) {
        try {
            return em.createNamedQuery("CashTransaction.findByCashTransactionId", CashTransaction.class).setParameter("cashTransactionId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getCashTransactionById is null");
            return null;
        }
    }

    public AdminCompany getAdminCompanyByName(String name) {
        try {
            return em.createNamedQuery("AdminCompany.findByName", AdminCompany.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAdminCompanyByName is null");
            return null;
        }
    }

    public AdminCompany getAdminCompanyById(Integer id) {
        try {
            return em.createNamedQuery("AdminCompany.findByAdminCompanyId", AdminCompany.class).setParameter("adminCompanyId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getAdminCompanyById is null");
            return null;
        }
    }

    public AdminCompany getAdminCompanybyLike(String name) {
        try {
            List al = em.createNativeQuery("select * from AdminCompany where Name like ?1", AdminCompany.class).setParameter(1, " %" + name + "%").getResultList();
            if (al.size() > 0) {
                return (AdminCompany) al.get(0);
            } else {
                return null;
            }
        } catch (NoResultException e) {
            LOGGER.info("getAdminCompanyByLike is null");
            return null;
        }
    }

    public AccountKeeper getAccountKeeperByLegal(String name) {
        try {
            return em.createNamedQuery("AccountKeeper.findByLegalName", AccountKeeper.class).setParameter("legalName", name).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getAccountKeeperByLegal is null");
            return null;
        }
    }

    void createTransactionFromItems(AcctRecUpper aru, List<AcctRecItems> itemsList) {
        try {
            utx.begin();

            int ret;

            Integer statusInd = 5;
            if (aru.getReceivedDate() != null) {
                statusInd = 2;
            }

            Integer contractCount = 0;
            Integer amendmentCount = 0;
            Integer cancelCount = 0;
            Integer reinstatementCount = 0;
            Integer contractDisbursementCount = 0;
            Integer amendmentDisbursementCount = 0;
            Integer cancelDisbursementCount = 0;
            Integer reinstatementDisbursementCount = 0;
            Integer transferCount = 0;
            Integer claimInspectionCount = 0;
            Integer paymentAuthorizationCount = 0;
            Integer paybackCount = 0;
            Integer rewardPointCount = 0;
            Integer batchCorrectionCount = 0;

            for (AcctRecItems ari : itemsList) {
                LOGGER.info("type=" + ari.getLedgeType());
                LOGGER.info("contractNo=" + ari.getContractNo());
                switch (ari.getLedgeType()) {
                    case "Contract":
                        ++contractCount;
                        break;
                    case "Amendment":
                        ++amendmentCount;
                        break;
                    case "Cancel":
                        ++cancelCount;
                        break;
                    case "Reinstatement":
                        ++reinstatementCount;
                        break;
                    case "Payment Authorization":
                        ++paymentAuthorizationCount;
                        break;
                    default:
                        LOGGER.info("Something is wrong with the ledgerType=" + ari.getLedgeType());
                        break;
                }

            }

            // now insert to Ledger 
            Integer ledgerTypeInd = 17; // why, later
            Integer exemptInd = 0;  // later, meaning?
            AccountKeeper enteredByFkId = getAccountKeeperById(getUserMemberByName(getCurrentUser()).getUserMemberId());

            Query query = em.createNativeQuery("insert into Ledger ("
                    + "amount, "
                    + "currentAmount, "
                    + "enteredDate, "
                    + "ledgerDate, "
                    + "ledgerTypeInd, "
                    + "relatedLedgerTypeInd, "
                    + "setLedgerDate, "
                    + "exemptInd, "
                    + "adminCompanyIdFk, "
                    + "CurrentAccountKeeperIdFk, "
                    + "originalAccountKeeperIdFk, "
                    + "lowersAdminCompanyBalanceInd, "
                    + "enteredByIdFk  ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13 )");

            query.setParameter(1, aru.getTransactionAmount());
            query.setParameter(2, aru.getTransactionAmount());
            query.setParameter(3, getCurrentDate());
            query.setParameter(4, aru.getReceivedDate());
            query.setParameter(5, ledgerTypeInd);
            query.setParameter(6, ledgerTypeInd);
            query.setParameter(7, aru.getReceivedDate());
            query.setParameter(8, exemptInd);
            query.setParameter(9, getAdminCompanyByName(aru.getAdminCompany()).getAdminCompanyId());
            query.setParameter(10, aru.getCurrAcctId());
            query.setParameter(11, aru.getCurrAcctId());
            query.setParameter(12, 0);
            query.setParameter(13, enteredByFkId.getAccountKeeperId());

            query.executeUpdate();

            Integer ledgerId = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('Ledger')").getSingleResult()).intValueExact();
            LOGGER.info("transactionId created=" + ledgerId);

            // update Ledger for each of record with transactionId
            Ledger ledger;
            for (AcctRecItems ari : itemsList) {
                ledger = getLedgerById(ari.getLedgeId());
                ledger.setCashTransactionIdFk(ledgerId);
                em.merge(ledger);
            }

            // insert to CashTransaction 
            query = em.createNativeQuery("insert into CashTransaction ("
                    + "cashTransactionTypeInd, "
                    + "paymentForTypeInd, "
                    + "cashTransactionStatusInd, "
                    + "transactionNumber, "
                    + "sentDate, "
                    + "appliedAmount, "
                    + "overUnder, "
                    + "contractCount, "
                    + "amendmentCount, "
                    + "cancelCount, "
                    + "reinstatementCount, "
                    + "contractDisbursementCount, "
                    + "amendmentDisbursementCount, "
                    + "cancelDisbursementCount, "
                    + "reinstatementDisbursementCount, "
                    + "transferCount, "
                    + "claimInspectionCount, "
                    + "paymentAuthorizationCount, "
                    + "paybackCount, "
                    + "rewardPointCount, "
                    + "batchCorrectionCount, "
                    + "payeeName, "
                    + "cashTransactionId  ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23 )");
            query.setParameter(1, getDtcashTransactionTypeByDesc(aru.getTransType()).getCashTransactionTypeId());
            query.setParameter(2, 1);
            query.setParameter(3, statusInd);
            query.setParameter(4, aru.getTransactionNumber());
            query.setParameter(5, aru.getSentDate());
            query.setParameter(6, aru.getTransactionAmount());
            query.setParameter(7, 0);
            query.setParameter(8, contractCount);
            query.setParameter(9, amendmentCount);
            query.setParameter(10, cancelCount);
            query.setParameter(11, reinstatementCount);
            query.setParameter(12, contractDisbursementCount);
            query.setParameter(13, amendmentDisbursementCount);
            query.setParameter(14, cancelDisbursementCount);
            query.setParameter(15, reinstatementDisbursementCount);
            query.setParameter(16, transferCount);
            query.setParameter(17, claimInspectionCount);
            query.setParameter(18, paymentAuthorizationCount);
            query.setParameter(19, paybackCount);
            query.setParameter(20, rewardPointCount);
            query.setParameter(21, batchCorrectionCount);
            query.setParameter(22, aru.getAccountName());
            query.setParameter(23, ledgerId);

            query.executeUpdate();

            utx.commit();

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public List<String> getLedgerTypeList() {
        List<String> ledgerTypeList = new ArrayList<>();
        List<DtLedgerType> ledgerList = getDtLedgerTypeByAll();
        for (DtLedgerType dl : ledgerList) {
            ledgerTypeList.add(dl.getDescription());
        }
        return ledgerTypeList;
    }

    // contract cancllation
    Boolean isContractCancellable(String contractNo) {
        LOGGER.info("in isContractCancellable, contractNo=" + contractNo);

        Boolean cancelContract = true;

        Contracts contract = getContractsByNo(contractNo);
        if (!contract.getContractStatusIdFk().getContractStatusName().equalsIgnoreCase("Inforce")) {
            cancelContract = false;
        } else {
            List<Claim> claims = getClaimByContractNo(contractNo);
            if (claims.size() > 0) {
                for (Claim claim : claims) {
                    if (!claim.getClaimStatus().getClaimStatusDesc().equalsIgnoreCase("Closed")) {
                        cancelContract = false;
                        break;
                    }
                }
            }
        }

        LOGGER.info("cancelContract=" + cancelContract);
        return cancelContract;
    }

    public List<CfCancelReason> getCfCancelReasonByAll() {
        try {
            return (List<CfCancelReason>) em.createNamedQuery("CfCancelReason.findAll", CfCancelReason.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelReasonByAll is null");
            return null;
        }
    }

    public List<CfCancelMethod> getCfCancelMethodByAll() {
        try {
            return (List<CfCancelMethod>) em.createNamedQuery("CfCancelMethod.findAll", CfCancelMethod.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodByAll is null");
            return null;
        }
    }

    public List<CfCancelMethodGroup> getCfCancelMethodGroupByAll() {
        try {
            return (List<CfCancelMethodGroup>) em.createNamedQuery("CfCancelMethodGroup.findAll", CfCancelMethodGroup.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodGroupByAll is null");
            return null;
        }
    }

    public List<CfCancelMethodGroup> getCfCancelMethodGroupByAll(Integer id) {
        try {
            return (List<CfCancelMethodGroup>) em.createNamedQuery("CfCancelMethodGroup.findAll", CfCancelMethodGroup.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodGroupByAll is null");
            return null;
        }
    }

    public List<CfCancelMethod> getCfCancelMethodByProductTypeId(Integer id) {
        try {
            return (List<CfCancelMethod>) em.createNativeQuery("select * from CfCancelMethod where productTypeIdFk is not null and productTypeIdFk = ?1", CfCancelMethod.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodByProductTypeId is null");
            return null;
        }
    }

    public DtCancelMethodType getDtCancelMethodTypeById(Integer id) {
        try {
            return (DtCancelMethodType) em.createNamedQuery("DtCancelMethodType.findByCancelMethodTypeId", DtCancelMethodType.class).setParameter("cancelMethodTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtCancelMethodTypeById is null");
            return null;
        }
    }

    public DtCancelType getDtCancelTypeById(Integer id) {
        try {
            return (DtCancelType) em.createNamedQuery("DtCancelType.findByCancelTypeId", DtCancelType.class).setParameter("cancelTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtCancelTypeById is null");
            return null;
        }
    }

    public CfCancelMethodGroupRule getCfCancelMethodGroupRuleByFkId(Integer id) {
        try {
            return (CfCancelMethodGroupRule) em.createNativeQuery("select * from CfCancelMethodGroupRule where cancelMethodGroupIdFk = ?1", CfCancelMethodGroupRule.class).setParameter(1, id).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodGroupRuleByFkId is null");
            return null;
        }
    }

    public List<CfCancelMethodGroup> getCfCancelMethodGroupByFkId(Integer id) {
        try {
            LOGGER.info("in getCfCancelMethodGroupByFkId, id=" + id);
            List<CfCancelMethodGroup> cmgList = (List<CfCancelMethodGroup>) em.createNativeQuery("select * from CfCancelMethodGroup where cancelMethodIdFk=?1", CfCancelMethodGroup.class).setParameter(1, id).getResultList();
            LOGGER.info("cmgList=" + cmgList.size());
            return cmgList;
        } catch (NoResultException e) {
            LOGGER.info("getCfCancelMethodGroupByFkId is null");
            return null;
        }
    }

    public List<ContractDisbursement> getContractDisbursementByFkId(Integer id) {
        try {
            return (List<ContractDisbursement>) em.createNativeQuery("select * from ContractDisbursement where contractIdFk=?1", ContractDisbursement.class).setParameter(1, id).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getContractDisbursementByFkId is null");
            return null;
        }
    }

    public Ledger getLedgerByCDIdFk(Integer id) {
        try {
            return (Ledger) em.createNativeQuery("select * from Ledger where relatedContractDisbursementIdFk=?1", Ledger.class).setParameter(1, id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getLedgerByCDIdFk is null");
            return null;
        }
    }

    public BigDecimal getContractCoverageCost(String contractNo) {
        BigDecimal coverageCost = BIGZERO;
        List<ContractDisbursement> cdList = getContractDisbursementByFkId(getContractsByNo(contractNo).getContractId());
        for (ContractDisbursement cd : cdList) {
            coverageCost = coverageCost.add(getLedgerByCDIdFk(cd.getContractDisbursementId()).getCurrentAmount());
        }
        return coverageCost;
    }

    public ContractStatus getContractStatusById(Integer id) {
        try {
            return em.createNamedQuery("ContractStatus.findByContractStatusId", ContractStatus.class).setParameter("contractStatusId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractStatusById is null");
            return null;
        }
    }

    public ContractStatus getContractStatusByName(String name) {
        try {
            return em.createNamedQuery("ContractStatus.findByContractStatusName", ContractStatus.class).setParameter("contractStatusName", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getContractStatusByName is null");
            return null;
        }
    }

    public DtDealerType getDtDealerTypeById(Integer id) {
        try {
            return em.createNamedQuery("DtDealerType.findByDealerTypeId", DtDealerType.class).setParameter("dealerTypeId", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtDealerTypeById is null");
            return null;
        }
    }

    public DtDealerType getDtDealerTypeByName(String name) {
        try {
            return em.createNamedQuery("DtDealerType.findByName", DtDealerType.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("getDtDealerTypeByName is null");
            return null;
        }
    }

    public void cancelContract(String contractNo) {
        try {
            utx.begin();

            Contracts contract = getContractsByNo(contractNo);
            contract.setContractStatusIdFk(getContractStatusByName("Cancelled"));
            em.merge(contract);
            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public String printAddress(Integer accountKeeperId, String type) {
        String address = "";
        AccountKeeper ak = getAccountKeeperById(accountKeeperId);
        Address addr = null;
        if (type.equalsIgnoreCase("P") && ak.getPhysicalAddressIdFk() != null) {
            addr = getAddressById(ak.getPhysicalAddressIdFk().getAddressId());
        } else if (type.equalsIgnoreCase("B") && ak.getBillingAddressIdFk() != null) {
            addr = getAddressById(ak.getBillingAddressIdFk().getAddressId());
        }
        if (addr != null) {
            CfRegion region = getRegionById(addr.getRegionIdFk().getRegionId());
            address = addr.getAddress1() + "\n" + addr.getAddress2() + "\n" + addr.getCity() + ", " + region.getRegionCode() + " " + addr.getZipCode() + "\n" + "Phone: " + addr.getPhoneNumber() + "\n" + "Fax: " + addr.getFax();
        }
        return address;
    }

    public void changeAccountKeeperActiveStatus(Integer id, Boolean isActive) {
        try {
            utx.begin();

            AccountKeeper ak = getAccountKeeperById(id);
            ak.setActive(isActive);
            em.merge(ak);

            utx.commit();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public List<RepairFacilityToDealerRel> getRepairFacilityToDealerRelByDealerId(Integer dealerId) {
        try {
            return (List<RepairFacilityToDealerRel>) em.createNativeQuery("select * from RepairFacilityToDealerRel where dealerId=?1", RepairFacilityToDealerRel.class).setParameter(1, dealerId).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityToDealerRelByDealerId is null");
            return null;
        }
    }

    public List<RepairFacilities> getRepairFacilitiesByDealerId(Integer dealerId) {
        try {

            List<RepairFacilities> rfList = new ArrayList<>();

            String rfName = "";
            String physicalPhone = "";
            String paddr = "";
            Integer rfid = null;

            List<RepairFacilityToDealerRel> rfdrList = getRepairFacilityToDealerRelByDealerId(dealerId);

            for (RepairFacilityToDealerRel rfdr : rfdrList) {
                AccountKeeper ak = getAccountKeeperById(rfdr.getRepairFacilityId().getRepairFacilityId());

                rfName = ak.getAccountKeeperName();

                if (ak.getPhysicalAddressIdFk() != null) {
                    physicalPhone = ak.getPhysicalAddressIdFk().getPhoneNumber();
                    paddr = printAddress(ak.getAccountKeeperId(), "P");
                }

                rfList.add(new RepairFacilities(rfdr.getRepairFacilityId().getRepairFacilityId(), rfName, physicalPhone, paddr, ak.getActive()));

            }

            LOGGER.info("size of rfList=" + rfList.size());

            return rfList;

        } catch (Exception e) {
            LOGGER.info("getRepairFacilitiesByDealerId is null");
            return null;
        }
    }

    public List<Integer> getRepairFacilityIdByDGId(Integer dgid) {
        LOGGER.info("in getRepairFacilityIdByDGId, dgid=" + dgid);
        try {
            return em.createNativeQuery("select distinct repairFacilityId from RepairFacilityToDealerRel where dealerId in ( select dealerId from Dealer where dealerGroupIdFk=?1)").setParameter(1, dgid).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getRepairFacilityIdByDGId is null");
            return null;
        }
    }

    public List<DtDealerType> getDtDealerTypeByAll() {
        LOGGER.info("in getDtDealerTypeByAll");
        try {
            return (List<DtDealerType>) em.createNamedQuery("DtDealerType.findAll", DtDealerType.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("getDtDealerTypeByAll is null");
            return null;
        }
    }

    public List<RepairFacilities> getRepairFacilitiesByDGId(Integer dealerGroupId) {
        try {

            List<RepairFacilities> rfList = new ArrayList<>();

            String paddr = "";
            String rfName = "";
            String physicalPhone = "";

            List<Integer> rpidList = getRepairFacilityIdByDGId(dealerGroupId);

            for (Integer rpid : rpidList) {
                AccountKeeper ak = getAccountKeeperById(rpid);

                rfName = ak.getAccountKeeperName();

                if (ak.getPhysicalAddressIdFk() != null) {
                    physicalPhone = ak.getPhysicalAddressIdFk().getPhoneNumber();
                    paddr = printAddress(ak.getAccountKeeperId(), "P");
                }

                rfList.add(new RepairFacilities(rpid, rfName, physicalPhone, paddr, ak.getActive()));

            }

            LOGGER.info("size of rfList=" + rfList.size());

            return rfList;

        } catch (Exception e) {
            LOGGER.info("getRepairFacilitiesByDGId is null");
            return null;
        }
    }

    public List<Notes> getNotesByDealerId(Integer id) {
        try {
            LOGGER.info("getNotesByDealerId=" + id);
            List<Note> noteList = em.createNativeQuery("select * from Note where accountKeeperIdFk=?1", Note.class).setParameter(1, id).getResultList();

            List<Notes> cList = new ArrayList<>();
            for (Note note : noteList) {
                cList.add(new Notes(note.getNoteId(), note.getNote(), note.getAccountKeeperIdFk().getAccountKeeperName(), getInputJavaDate(note.getEnteredDate())));
            }

            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getNotesByDealerId is null");
            return null;
        }
    }

    public List<Notes> getNotesByDGId(Integer dealerGroupId) {
        try {

            List<Notes> notesList = new ArrayList<>();
            List<Notes> allNotes = new ArrayList<>();

            List<Dealer> dealers = getDealerByGroupFkId(dealerGroupId);
            for (Dealer dealer : dealers) {
                notesList = getNotesByDealerId(dealer.getDealerId());
                allNotes.addAll(notesList);
            }

            LOGGER.info("size of allNotes=" + allNotes.size());

            return allNotes;

        } catch (Exception e) {
            LOGGER.info("getNotesByDGId is null");
            return null;
        }
    }

    public List<HistoryLogs> getHistoryLogsByDealerId(Integer id) {
        try {
            LOGGER.info("getHistoryLogsByDealerId=" + id);
            List<HistoryLog> hlList = em.createNativeQuery("select * from HistoryLog where accountKeeperIdFk=?1", HistoryLog.class).setParameter(1, id).getResultList();

            List<HistoryLogs> cList = new ArrayList<>();
            for (HistoryLog log : hlList) {
                cList.add(new HistoryLogs(log.getHistoryLogId(), log.getHistoryLog(), log.getAccountKeeperIdFk().getAccountKeeperName(), getInputJavaDate(log.getEnteredDate())));
            }

            return cList;
        } catch (NoResultException e) {
            LOGGER.info("getHistoryLogsByDealerId is null");
            return null;
        }
    }

    public List<HistoryLogs> getHistoryLogsByDGId(Integer dealerGroupId) {
        try {

            List<HistoryLogs> notesList = new ArrayList<>();
            List<HistoryLogs> allHistoryLogs = new ArrayList<>();

            List<Dealer> dealers = getDealerByGroupFkId(dealerGroupId);
            for (Dealer dealer : dealers) {
                notesList = getHistoryLogsByDealerId(dealer.getDealerId());
                allHistoryLogs.addAll(notesList);
            }

            LOGGER.info("size of allHistoryLogs=" + allHistoryLogs.size());

            return allHistoryLogs;

        } catch (Exception e) {
            LOGGER.info("getHistoryLogsByDGId is null");
            return null;
        }
    }

    public List<DtDisbursementAmountType> getDtDisbursementAmountTypeByAll() {
        try {
            return (List<DtDisbursementAmountType>) em.createNamedQuery("DtDisbursementAmountType.findAll", DtDisbursementAmountType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDtDisbursementAmountTypeByAll is null");
            return null;
        }
    }

    public List<DtDisbursementRefundType> getDtDisbursementRefundTypeByAll() {
        try {
            return (List<DtDisbursementRefundType>) em.createNamedQuery("DtDisbursementRefundType.findAll", DtDisbursementRefundType.class).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDtDisbursementRefundTypeByAll is null");
            return null;
        }
    }

    public DtDisbursementRefundType getDtDisbursementRefundTypeById(Integer id) {
        try {
            return (DtDisbursementRefundType) em.createNamedQuery("DtDisbursementRefundType.findByDisbursementRefundTypeId", DtDisbursementRefundType.class).setParameter("disbursementRefundTypeId", id).getResultList();

        } catch (NoResultException e) {
            LOGGER.info("getDtDisbursementRefundTypeById is null");
            return null;
        }
    }

    public List<String> loadDealerTypes() {
        List<DtDealerType> dtList = getDtDealerTypeByAll();
        List<String> dealerTypes = new ArrayList<>();
        for (DtDealerType dt : dtList) {
            dealerTypes.add(dt.getName());
        }
        return dealerTypes;
    }

    public List<String> loadDealerGroups() {
        List<DealerGroup> dtList = getDealerGroupByAll();
        List<String> dealerGroups = new ArrayList<>();
        for (DealerGroup dealerGroup : dtList) {
            dealerGroups.add(getAccountKeeperById(dealerGroup.getDealerGroupId()).getAccountKeeperName());
        }
        return dealerGroups;
    }

    public List<String> loadRemitRules() {
        List<CfRemitRule> dtList = getCfRemitRuleByAll();
        List<String> remitRules = new ArrayList<>();
        for (CfRemitRule remitRule : dtList) {
            remitRules.add(remitRule.getName());
        }
        return remitRules;
    }

    public List<String> loadDisbursementGroup() {
        List<DisbursementGroup> dtList = getDisbursementGroupByAll();
        List<String> groups = new ArrayList<>();
        for (DisbursementGroup group : dtList) {
            groups.add(group.getName());
        }
        return groups;
    }

    public List<String> loadDtDisbursementType() {
        List<DtDisbursementType> dtList = getDtDisbursementTypeByAll();
        List<String> groups = new ArrayList<>();
        for (DtDisbursementType group : dtList) {
            groups.add(group.getDescription());
        }
        return groups;
    }

    public List<String> loadDtDisbursementAmountType() {
        List<DtDisbursementAmountType> dtList = getDtDisbursementAmountTypeByAll();
        List<String> groups = new ArrayList<>();
        for (DtDisbursementAmountType group : dtList) {
            groups.add(group.getDescription());
        }
        return groups;
    }

    public List<String> loadDtDisbursementRefundType() {
        List<DtDisbursementRefundType> dtList = getDtDisbursementRefundTypeByAll();
        List<String> groups = new ArrayList<>();
        for (DtDisbursementRefundType group : dtList) {
            groups.add(group.getDescription());
        }
        return groups;
    }

    public List<String> loadDisbursementCode() {
        List<DisbursementCode> dtList = getDisbursementCodeByAll();
        List<String> groups = new ArrayList<>();
        for (DisbursementCode group : dtList) {
            groups.add(group.getName());
        }
        return groups;
    }

    public List<DetailedDisbursements> getDetailedDisbursementByDealerId(Integer dealerId) {
        try {
            LOGGER.info("in getDetailedDisbursementByDealerId, dealerId=" + dealerId);
            List<DetailedDisbursements> ddList = new ArrayList<>();

            Dealer dealer = getDealerById(dealerId);
            List<Disbursement> disbursementList = getDisbursementByFkId(dealer.getDisbursementCenterIdFk().getDisbursementCenterId());
            for (Disbursement disbursement : disbursementList) {
                LOGGER.info("disbursementId=" + disbursement.getDisbursementId());
                DisbursementDetail dd = getDisbursementDetailByFkId(disbursement.getDisbursementId());

                String payTo = "";
                if (dd.getAccountKeeperIdFk() != null) {
                    payTo = getAccountKeeperById(dd.getAccountKeeperIdFk().getAccountKeeperId()).getAccountKeeperName();
                    LOGGER.info("dd, id=" + dd.getAccountKeeperIdFk().getAccountKeeperId() + ", name=" + dd.getAccountKeeperIdFk().getAccountKeeperName());
                }

                String agent = "";
                String dealerName = "";
                String dealerGroup = "";
                if (disbursement.getDirectAgentConditionIdFk() != null) {
                    agent = getAccountKeeperById(disbursement.getDirectAgentConditionIdFk().getAgentId()).getAccountKeeperName();
                }
                if (disbursement.getDealerConditionIdFk() != null) {
                    dealerName = getAccountKeeperById(disbursement.getDealerConditionIdFk().getDealerId()).getAccountKeeperName();
                }
                if (disbursement.getDealerGroupConditionIdFk() != null) {
                    dealerGroup = getAccountKeeperById(disbursement.getDealerGroupConditionIdFk().getDealerGroupId()).getAccountKeeperName();
                }
                String calculatedOnDisbursementCode = "";
                if (dd.getCalculatedOnDisbursementCodeIdFk() != null) {
                    calculatedOnDisbursementCode = getDisbursementCodeById(dd.getCalculatedOnDisbursementCodeIdFk().getDisbursementCodeId()).getDescription();
                }
                String desc = "";
                if (dd.getDisbursementTypeInd() != null) {
                    desc = getDtDisbursementTypeById(dd.getDisbursementTypeInd()).getDescription();
                }
                LOGGER.info("desc=" + desc);
                String group = "";
                if (dd.getDisbursementGroupIdFk() != null) {
                    group = getDisbursementGroupById(dd.getDisbursementGroupIdFk().getDisbursementGroupId()).getDescription();
                }
                LOGGER.info("group=" + group);
                String code = "";
                if (dd.getDisbursementCodeIdFk() != null) {
                    code = getDisbursementCodeById(dd.getDisbursementCodeIdFk().getDisbursementCodeId()).getDescription();
                }
                LOGGER.info("code=" + code);
                Integer criterionGroupId = null;
                if (dd.getCriterionGroupIdFk() != null) {
                    criterionGroupId = dd.getCriterionGroupIdFk().getCriterionGroupId();
                }
                ddList.add(new DetailedDisbursements(
                        dd.getDisbursementDetailId(),
                        //getDtDisbursementTypeById(dd.getDisbursementTypeInd()).getDescription(),
                        desc,
                        payTo,
                        "",
                        group,
                        dd.getDisbursementAmount(),
                        "",
                        code,
                        //getDtDisbursementTypeById(dd.getDisbursementRefundTypeInd()).getDescription(),
                        desc,
                        agent,
                        dealerName,
                        dealerGroup,
                        //getDtDisbursementTypeById(dd.getDisbursementAmountTypeInd()).getDescription(),
                        desc,
                        dd.getDisbursementAmount(),
                        calculatedOnDisbursementCode,
                        dd.getRenewableInd(),
                        dd.getDisbursementMemo(),
                        dd.getEffectiveDate(),
                        dd.getExpireDate(),
                        criterionGroupId
                ));
            }

            LOGGER.info("size of ddList=" + ddList.size());

            return ddList;

        } catch (Exception e) {
            LOGGER.info("getDetailedDisbursementByDealerId is null");
            return null;
        }
    }

    public List<RV> getRVByDealerId(Integer dealerId) {
        try {
            LOGGER.info("in getRVByDealerId, dealerId=" + dealerId);
            List<RV> rvList = new ArrayList<>();
            List<Dealer> dealers = new ArrayList();
            List<RateVisibility> rvs = new ArrayList<>();
            Dealer dr = null;
            if (dealerId != null && dealerId != -1) {    // -1 will come from searchRV
                dr = getDealerById(dealerId);
                if (dr.getRateVisibilityIdFk() != null) {
                    RateVisibility vr = getRateVisibilityById(dr.getRateVisibilityIdFk().getRateVisibilityId());
                    LOGGER.info("rvId=" + vr.getRateVisibilityId());
                    rvs.add(vr);
                    //dealers.add(getAccountKeeperById(dealerId).getAccountKeeperName());
                }
            } else {    // retrieve all rvs
                rvs = getRateVisibilityByAll();
            }
            for (RateVisibility rv : rvs) {
                String allowedProducts = "ANC,GAP, PPM, VSC";
                if (rv.getAllowedProductTypes() == 7) {
                    allowedProducts = "ANC, GAP, VSC";
                }

                RateBasedRuleGroup rbrg = getRateBasedRuleGroupById(rv.getRateVisibilityId());

                //LOGGER.info("getRateVisibilityId=" + rv.getRateVisibilityId());
                if (dealerId != null && dealerId != -1) {
                    dealers.add(dr);
                } else {
                    dealers = getDealerByRVFkId(rv.getRateVisibilityId());
                }
                //LOGGER.info("dealers=" + dealers.size());
                Date effectiveDate = null;
                Date expiredDate = null;
                if (rv.getEffectiveDate() != null) {
                    effectiveDate = convertFromUtilDate(rv.getEffectiveDate());
                }
                if (rv.getExpireDate() != null) {
                    expiredDate = convertFromUtilDate(rv.getExpireDate());
                }
                if (dealerId != null) {
                    if (dealers.size() > 0) {
                        for (Dealer dealer : dealers) {
                            String dealerName = getAccountKeeperById(dealer.getDealerId()).getAccountKeeperName();
                            rvList.add(new RV(
                                    rv.getRateVisibilityId(),
                                    rv.getDescription(),
                                    allowedProducts,
                                    rv.getDeletedInd(),
                                    effectiveDate,
                                    expiredDate,
                                    dealerName,
                                    dealer.getDealerId(),
                                    rbrg.getName()
                            ));
                        }
                    } else {
                        rvList.add(new RV(
                                rv.getRateVisibilityId(),
                                rv.getDescription(),
                                allowedProducts,
                                rv.getDeletedInd(),
                                effectiveDate,
                                expiredDate,
                                "",
                                null,
                                rbrg.getName()
                        ));
                    }

                } else {
                    rvList.add(new RV(
                            rv.getRateVisibilityId(),
                            rv.getDescription(),
                            allowedProducts,
                            rv.getDeletedInd(),
                            effectiveDate,
                            expiredDate,
                            "",
                            null,
                            rbrg.getName()
                    ));
                }
            }

            LOGGER.info("size of rvList=" + rvList.size());

            return rvList;

        } catch (Exception e) {
            LOGGER.info("getRVByDealerId is null");
            return null;
        }
    }

    public List<RV> connectDealerToRV(Integer oid, RV rv) {
        try {
            utx.begin();

            LOGGER.info("in connectDealerToRV, dealerId=" + rv.getDealerId());
            LOGGER.info("RV=" + rv.getDescription());

            if (oid != null) {
                List<DealerSRP> srpList = getDealerSRPByFkId(oid);
                if (srpList != null && srpList.size() > 0) {
                    for (DealerSRP srp : srpList) {
                        Query query = em.createNativeQuery("insert into DealerSRP (dealerIdFk, criterionGroupIdFk, description, priority, amount, SRPCalculationTypeInd, SRPTypeInd, dateEffective) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)");
                        query.setParameter(1, rv.getDealerId());
                        query.setParameter(2, srp.getCriterionGroupIdFk().getCriterionGroupId());
                        query.setParameter(3, srp.getDescription());
                        query.setParameter(4, srp.getPriority());
                        query.setParameter(5, srp.getAmount());
                        query.setParameter(6, srp.getSRPCalculationTypeInd());
                        query.setParameter(7, srp.getSRPTypeInd());
                        query.setParameter(8, getCurrentDate());
                        Integer ret = query.executeUpdate();
                        LOGGER.info("insert to DealerSRP, ret=" + ret);
                    }
                }
            }

            Dealer dealer = getDealerById(rv.getDealerId());

            dealer.setRateVisibilityIdFk(getRateVisibilityById(rv.getRateVisibilityId()));

            em.merge(dealer);

            utx.commit();

            List<RV> rvList = new ArrayList<>();

            rvList = getRVByDealerId(dealer.getDealerId());

            LOGGER.info("size of rvList=" + rvList.size());

            return rvList;

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();

                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
            return null;
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
            return null;
        }
    }

    /**
     *
     * @param s
     * @return Date
     */
    public Date convSTDateWS(String s) {
        try {
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            java.util.Date dt = formatter.parse(s);
            return new Date(dt.getTime());
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date convSTDateWD(String s) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date dt = formatter.parse(s);
            return new Date(dt.getTime());
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     *
     * @return Date
     */
    public Date getCurrentDate() {
        return new java.sql.Date(Calendar.getInstance().getTime().getTime());
    }

    /**
     *
     * @return java.sql.Timestamp
     */
    public Timestamp getCurrnetDateTime() {
        return new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    /**
     *
     * @param tblName
     * @return Integer id
     */
    public Integer getLatestInsertedId(String tblName) {
        try {
            return ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('" + tblName + "')").getSingleResult()).intValueExact();
        } catch (NoResultException e) {
            LOGGER.info("getLatestInsertedId is null");
            return null;
        }
    }

    /**
     *
     * @param uDate
     * @return
     */
    public String getJavaDate(java.util.Date uDate) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        return df.format(uDate);
    }

    public String getDateDashString(java.util.Date u) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(u);
    }

    public String getInputJavaDate(java.util.Date uDate) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy, HH:MM:SS");
        return df.format(uDate);
    }

    public String phoneTodb(String phone) { // (732)777-1000 to 7327771000
        return phone.replace("(", "").replace(")", "").replace("-", "");
    }

    public Date convertFromUtilDate(java.util.Date uDate) {
        return convSTDateWS(getJavaDate(uDate));
    }

    public boolean isLeapYear(int year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    public Date addYear(Date orginalDate, Integer years) {
        return new java.sql.Date(orginalDate.getTime() + years * 365l * 24l * 60l * 60l * 1000l);
    }

    public java.util.Date addMonth(java.util.Date initDate, Integer months) {
        //java.util.Date now = new java.util.Date( Calendar.getInstance().getTime().getTime());    

        Calendar myCal = Calendar.getInstance();
        myCal.setTime(initDate);
        myCal.add(Calendar.MONTH, +months);
        return myCal.getTime();
    }

    public Long getSqlDiffDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public Long getUtilDiffDays(java.util.Date d1, java.util.Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     *
     * @param base
     * @param pct
     * @return
     */
    public BigDecimal getDecimalPercent(BigDecimal base, BigDecimal pct) {
        return base.multiply(pct).divide(new BigDecimal(100));
    }
}
