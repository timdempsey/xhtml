/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimLabor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimLabor.findAll", query = "SELECT c FROM ClaimLabor c")
    , @NamedQuery(name = "ClaimLabor.findByClaimLaborId", query = "SELECT c FROM ClaimLabor c WHERE c.claimLaborId = :claimLaborId")
    , @NamedQuery(name = "ClaimLabor.findByRequestedLaborTime", query = "SELECT c FROM ClaimLabor c WHERE c.requestedLaborTime = :requestedLaborTime")
    , @NamedQuery(name = "ClaimLabor.findByLaborTime", query = "SELECT c FROM ClaimLabor c WHERE c.laborTime = :laborTime")
    , @NamedQuery(name = "ClaimLabor.findByLaborRate", query = "SELECT c FROM ClaimLabor c WHERE c.laborRate = :laborRate")
    , @NamedQuery(name = "ClaimLabor.findByRequestLaborRate", query = "SELECT c FROM ClaimLabor c WHERE c.requestLaborRate = :requestLaborRate")
    , @NamedQuery(name = "ClaimLabor.findByTaxRate", query = "SELECT c FROM ClaimLabor c WHERE c.taxRate = :taxRate")
    , @NamedQuery(name = "ClaimLabor.findByDeleted", query = "SELECT c FROM ClaimLabor c WHERE c.deleted = :deleted")
    , @NamedQuery(name = "ClaimLabor.findByPaid", query = "SELECT c FROM ClaimLabor c WHERE c.paid = :paid")
    , @NamedQuery(name = "ClaimLabor.findByPaymentAuthorizationIdFk", query = "SELECT c FROM ClaimLabor c WHERE c.paymentAuthorizationIdFk = :paymentAuthorizationIdFk")
    , @NamedQuery(name = "ClaimLabor.findByUpdateUserName", query = "SELECT c FROM ClaimLabor c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimLabor.findByUpdateLast", query = "SELECT c FROM ClaimLabor c WHERE c.updateLast = :updateLast")})
public class ClaimLabor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimLaborId")
    private Integer claimLaborId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "requestedLaborTime")
    private BigDecimal requestedLaborTime;
    @Column(name = "laborTime")
    private BigDecimal laborTime;
    @Column(name = "laborRate")
    private BigDecimal laborRate;
    @Column(name = "requestLaborRate")
    private BigDecimal requestLaborRate;
    @Column(name = "taxRate")
    private BigDecimal taxRate;
    @Column(name = "deleted")
    private Boolean deleted;
    @Column(name = "paid")
    private Boolean paid;
    @Column(name = "paymentAuthorizationIdFk")
    private Integer paymentAuthorizationIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "detailItemIdFk", referencedColumnName = "detailItemId")
    @ManyToOne
    private ClaimDetailItem detailItemIdFk;
    @JoinColumn(name = "enteredIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredIdFk;
    @OneToMany(mappedBy = "claimLaborIdFk")
    private Collection<ClaimDetailItem> claimDetailItemCollection;

    public ClaimLabor() {
    }

    public ClaimLabor(Integer claimLaborId) {
        this.claimLaborId = claimLaborId;
    }

    public Integer getClaimLaborId() {
        return claimLaborId;
    }

    public void setClaimLaborId(Integer claimLaborId) {
        this.claimLaborId = claimLaborId;
    }

    public BigDecimal getRequestedLaborTime() {
        return requestedLaborTime;
    }

    public void setRequestedLaborTime(BigDecimal requestedLaborTime) {
        this.requestedLaborTime = requestedLaborTime;
    }

    public BigDecimal getLaborTime() {
        return laborTime;
    }

    public void setLaborTime(BigDecimal laborTime) {
        this.laborTime = laborTime;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public BigDecimal getRequestLaborRate() {
        return requestLaborRate;
    }

    public void setRequestLaborRate(BigDecimal requestLaborRate) {
        this.requestLaborRate = requestLaborRate;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Integer getPaymentAuthorizationIdFk() {
        return paymentAuthorizationIdFk;
    }

    public void setPaymentAuthorizationIdFk(Integer paymentAuthorizationIdFk) {
        this.paymentAuthorizationIdFk = paymentAuthorizationIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ClaimDetailItem getDetailItemIdFk() {
        return detailItemIdFk;
    }

    public void setDetailItemIdFk(ClaimDetailItem detailItemIdFk) {
        this.detailItemIdFk = detailItemIdFk;
    }

    public UserMember getEnteredIdFk() {
        return enteredIdFk;
    }

    public void setEnteredIdFk(UserMember enteredIdFk) {
        this.enteredIdFk = enteredIdFk;
    }

    @XmlTransient
    public Collection<ClaimDetailItem> getClaimDetailItemCollection() {
        return claimDetailItemCollection;
    }

    public void setClaimDetailItemCollection(Collection<ClaimDetailItem> claimDetailItemCollection) {
        this.claimDetailItemCollection = claimDetailItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimLaborId != null ? claimLaborId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimLabor)) {
            return false;
        }
        ClaimLabor other = (ClaimLabor) object;
        if ((this.claimLaborId == null && other.claimLaborId != null) || (this.claimLaborId != null && !this.claimLaborId.equals(other.claimLaborId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimLabor[ claimLaborId=" + claimLaborId + " ]";
    }
    
}
