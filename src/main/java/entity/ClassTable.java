/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClassTable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassTable.findAll", query = "SELECT c FROM ClassTable c")
    , @NamedQuery(name = "ClassTable.findByClassTableId", query = "SELECT c FROM ClassTable c WHERE c.classTableId = :classTableId")
    , @NamedQuery(name = "ClassTable.findByName", query = "SELECT c FROM ClassTable c WHERE c.name = :name")
    , @NamedQuery(name = "ClassTable.findByDescription", query = "SELECT c FROM ClassTable c WHERE c.description = :description")
    , @NamedQuery(name = "ClassTable.findByUpdateUserName", query = "SELECT c FROM ClassTable c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClassTable.findByUpdateLast", query = "SELECT c FROM ClassTable c WHERE c.updateLast = :updateLast")})
public class ClassTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "classTableId")
    private Integer classTableId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "classTableIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "classTableIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(mappedBy = "classTableIdFk")
    private Collection<ClassRule> classRuleCollection;
    @JoinColumn(name = "ClassCodeNameIdFk", referencedColumnName = "classCodeNameId")
    @ManyToOne
    private ClassCodeName classCodeNameIdFk;

    public ClassTable() {
    }

    public ClassTable(Integer classTableId) {
        this.classTableId = classTableId;
    }

    public Integer getClassTableId() {
        return classTableId;
    }

    public void setClassTableId(Integer classTableId) {
        this.classTableId = classTableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<ClassRule> getClassRuleCollection() {
        return classRuleCollection;
    }

    public void setClassRuleCollection(Collection<ClassRule> classRuleCollection) {
        this.classRuleCollection = classRuleCollection;
    }

    public ClassCodeName getClassCodeNameIdFk() {
        return classCodeNameIdFk;
    }

    public void setClassCodeNameIdFk(ClassCodeName classCodeNameIdFk) {
        this.classCodeNameIdFk = classCodeNameIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classTableId != null ? classTableId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassTable)) {
            return false;
        }
        ClassTable other = (ClassTable) object;
        if ((this.classTableId == null && other.classTableId != null) || (this.classTableId != null && !this.classTableId.equals(other.classTableId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClassTable[ classTableId=" + classTableId + " ]";
    }
    
}
