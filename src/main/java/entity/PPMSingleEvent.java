/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMSingleEvent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMSingleEvent.findAll", query = "SELECT p FROM PPMSingleEvent p")
    , @NamedQuery(name = "PPMSingleEvent.findByPPMEventIdFk", query = "SELECT p FROM PPMSingleEvent p WHERE p.pPMEventIdFk = :pPMEventIdFk")
    , @NamedQuery(name = "PPMSingleEvent.findByLowerMonths", query = "SELECT p FROM PPMSingleEvent p WHERE p.lowerMonths = :lowerMonths")
    , @NamedQuery(name = "PPMSingleEvent.findByLowerMiles", query = "SELECT p FROM PPMSingleEvent p WHERE p.lowerMiles = :lowerMiles")
    , @NamedQuery(name = "PPMSingleEvent.findByUpperMonths", query = "SELECT p FROM PPMSingleEvent p WHERE p.upperMonths = :upperMonths")
    , @NamedQuery(name = "PPMSingleEvent.findByUpperMiles", query = "SELECT p FROM PPMSingleEvent p WHERE p.upperMiles = :upperMiles")
    , @NamedQuery(name = "PPMSingleEvent.findByUpdateUserName", query = "SELECT p FROM PPMSingleEvent p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMSingleEvent.findByUpdateLast", query = "SELECT p FROM PPMSingleEvent p WHERE p.updateLast = :updateLast")})
public class PPMSingleEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMEventIdFk")
    private Integer pPMEventIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LowerMonths")
    private int lowerMonths;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LowerMiles")
    private int lowerMiles;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UpperMonths")
    private int upperMonths;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UpperMiles")
    private int upperMiles;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "PPMEventIdFk", referencedColumnName = "PPMEventId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private PPMEvent pPMEvent;

    public PPMSingleEvent() {
    }

    public PPMSingleEvent(Integer pPMEventIdFk) {
        this.pPMEventIdFk = pPMEventIdFk;
    }

    public PPMSingleEvent(Integer pPMEventIdFk, int lowerMonths, int lowerMiles, int upperMonths, int upperMiles) {
        this.pPMEventIdFk = pPMEventIdFk;
        this.lowerMonths = lowerMonths;
        this.lowerMiles = lowerMiles;
        this.upperMonths = upperMonths;
        this.upperMiles = upperMiles;
    }

    public Integer getPPMEventIdFk() {
        return pPMEventIdFk;
    }

    public void setPPMEventIdFk(Integer pPMEventIdFk) {
        this.pPMEventIdFk = pPMEventIdFk;
    }

    public int getLowerMonths() {
        return lowerMonths;
    }

    public void setLowerMonths(int lowerMonths) {
        this.lowerMonths = lowerMonths;
    }

    public int getLowerMiles() {
        return lowerMiles;
    }

    public void setLowerMiles(int lowerMiles) {
        this.lowerMiles = lowerMiles;
    }

    public int getUpperMonths() {
        return upperMonths;
    }

    public void setUpperMonths(int upperMonths) {
        this.upperMonths = upperMonths;
    }

    public int getUpperMiles() {
        return upperMiles;
    }

    public void setUpperMiles(int upperMiles) {
        this.upperMiles = upperMiles;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public PPMEvent getPPMEvent() {
        return pPMEvent;
    }

    public void setPPMEvent(PPMEvent pPMEvent) {
        this.pPMEvent = pPMEvent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMEventIdFk != null ? pPMEventIdFk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMSingleEvent)) {
            return false;
        }
        PPMSingleEvent other = (PPMSingleEvent) object;
        if ((this.pPMEventIdFk == null && other.pPMEventIdFk != null) || (this.pPMEventIdFk != null && !this.pPMEventIdFk.equals(other.pPMEventIdFk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMSingleEvent[ pPMEventIdFk=" + pPMEventIdFk + " ]";
    }
    
}
