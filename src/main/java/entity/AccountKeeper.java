/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AccountKeeper")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountKeeper.findAll", query = "SELECT a FROM AccountKeeper a")
    , @NamedQuery(name = "AccountKeeper.findByAccountKeeperId", query = "SELECT a FROM AccountKeeper a WHERE a.accountKeeperId = :accountKeeperId")
    , @NamedQuery(name = "AccountKeeper.findByAccountKeeperName", query = "SELECT a FROM AccountKeeper a WHERE a.accountKeeperName = :accountKeeperName")
    , @NamedQuery(name = "AccountKeeper.findBySystemCode", query = "SELECT a FROM AccountKeeper a WHERE a.systemCode = :systemCode")
    , @NamedQuery(name = "AccountKeeper.findByAccountCode", query = "SELECT a FROM AccountKeeper a WHERE a.accountCode = :accountCode")
    , @NamedQuery(name = "AccountKeeper.findByTaxId", query = "SELECT a FROM AccountKeeper a WHERE a.taxId = :taxId")
    , @NamedQuery(name = "AccountKeeper.findByActive", query = "SELECT a FROM AccountKeeper a WHERE a.active = :active")
    , @NamedQuery(name = "AccountKeeper.findByEmailAddress", query = "SELECT a FROM AccountKeeper a WHERE a.emailAddress = :emailAddress")
    , @NamedQuery(name = "AccountKeeper.findByEmailOptIn", query = "SELECT a FROM AccountKeeper a WHERE a.emailOptIn = :emailOptIn")
    , @NamedQuery(name = "AccountKeeper.findByPreferredTransactionTypeInd", query = "SELECT a FROM AccountKeeper a WHERE a.preferredTransactionTypeInd = :preferredTransactionTypeInd")
    , @NamedQuery(name = "AccountKeeper.findByLegalName", query = "SELECT a FROM AccountKeeper a WHERE a.legalName = :legalName")
    , @NamedQuery(name = "AccountKeeper.findByEnteredDate", query = "SELECT a FROM AccountKeeper a WHERE a.enteredDate = :enteredDate")
    , @NamedQuery(name = "AccountKeeper.findByUpdateUserName", query = "SELECT a FROM AccountKeeper a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "AccountKeeper.findByUpdateLast", query = "SELECT a FROM AccountKeeper a WHERE a.updateLast = :updateLast")})
public class AccountKeeper implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "accountKeeperId")
    private Integer accountKeeperId;
    @Size(max = 100)
    @Column(name = "accountKeeperName")
    private String accountKeeperName;
    @Size(max = 50)
    @Column(name = "systemCode")
    private String systemCode;
    @Size(max = 50)
    @Column(name = "accountCode")
    private String accountCode;
    @Size(max = 20)
    @Column(name = "taxId")
    private String taxId;
    @Column(name = "active")
    private Boolean active;
    @Size(max = 255)
    @Column(name = "emailAddress")
    private String emailAddress;
    @Column(name = "EmailOptIn")
    private Boolean emailOptIn;
    @Column(name = "preferredTransactionTypeInd")
    private Integer preferredTransactionTypeInd;
    @Size(max = 255)
    @Column(name = "legalName")
    private String legalName;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "billToAccountKeeperIdFk")
    private Collection<AccountKeeper> accountKeeperCollection;
    @JoinColumn(name = "billToAccountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper billToAccountKeeperIdFk;
    @JoinColumn(name = "accountKeeperTypeIdFk", referencedColumnName = "AccountKeeperTypeId")
    @ManyToOne
    private AccountKeeperType accountKeeperTypeIdFk;
    @JoinColumn(name = "billingAddressIdFk", referencedColumnName = "addressId")
    @ManyToOne
    private Address billingAddressIdFk;
    @JoinColumn(name = "physicalAddressIdFk", referencedColumnName = "addressId")
    @ManyToOne
    private Address physicalAddressIdFk;
    @JoinColumn(name = "remitRuleIdFk", referencedColumnName = "remitRuleId")
    @ManyToOne
    private CfRemitRule remitRuleIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;
    @OneToMany(mappedBy = "cancelFeeAccountKeeperIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private RepairFacility repairFacility;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountKeeperIdFk")
    private Collection<AccountKeeperField> accountKeeperFieldCollection;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<DisbursementCode> disbursementCodeCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private UserMember userMember;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<ClaimFile> claimFileCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private CustomerName customerName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentAccountKeeperIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "originalAccountKeeperIdFk")
    private Collection<Ledger> ledgerCollection1;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private Agent agent;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @OneToMany(mappedBy = "accountKeeperIdFk")
    private Collection<HistoryLog> historyLogCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private Insurer insurer;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private InspectionCompany inspectionCompany;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private LienHolder lienHolder;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private DisbursementAccount disbursementAccount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountKeeperIdFk")
    private Collection<AccountKeeperBalance> accountKeeperBalanceCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private DealerGroup dealerGroup;
    @OneToMany(mappedBy = "paidToFk")
    private Collection<Claim> claimCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accountKeeper")
    private Dealer dealer;

    public AccountKeeper() {
    }

    public AccountKeeper(Integer accountKeeperId) {
        this.accountKeeperId = accountKeeperId;
    }

    public Integer getAccountKeeperId() {
        return accountKeeperId;
    }

    public void setAccountKeeperId(Integer accountKeeperId) {
        this.accountKeeperId = accountKeeperId;
    }

    public String getAccountKeeperName() {
        return accountKeeperName;
    }

    public void setAccountKeeperName(String accountKeeperName) {
        this.accountKeeperName = accountKeeperName;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Integer getPreferredTransactionTypeInd() {
        return preferredTransactionTypeInd;
    }

    public void setPreferredTransactionTypeInd(Integer preferredTransactionTypeInd) {
        this.preferredTransactionTypeInd = preferredTransactionTypeInd;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection() {
        return accountKeeperCollection;
    }

    public void setAccountKeeperCollection(Collection<AccountKeeper> accountKeeperCollection) {
        this.accountKeeperCollection = accountKeeperCollection;
    }

    public AccountKeeper getBillToAccountKeeperIdFk() {
        return billToAccountKeeperIdFk;
    }

    public void setBillToAccountKeeperIdFk(AccountKeeper billToAccountKeeperIdFk) {
        this.billToAccountKeeperIdFk = billToAccountKeeperIdFk;
    }

    public AccountKeeperType getAccountKeeperTypeIdFk() {
        return accountKeeperTypeIdFk;
    }

    public void setAccountKeeperTypeIdFk(AccountKeeperType accountKeeperTypeIdFk) {
        this.accountKeeperTypeIdFk = accountKeeperTypeIdFk;
    }

    public Address getBillingAddressIdFk() {
        return billingAddressIdFk;
    }

    public void setBillingAddressIdFk(Address billingAddressIdFk) {
        this.billingAddressIdFk = billingAddressIdFk;
    }

    public Address getPhysicalAddressIdFk() {
        return physicalAddressIdFk;
    }

    public void setPhysicalAddressIdFk(Address physicalAddressIdFk) {
        this.physicalAddressIdFk = physicalAddressIdFk;
    }

    public CfRemitRule getRemitRuleIdFk() {
        return remitRuleIdFk;
    }

    public void setRemitRuleIdFk(CfRemitRule remitRuleIdFk) {
        this.remitRuleIdFk = remitRuleIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    public RepairFacility getRepairFacility() {
        return repairFacility;
    }

    public void setRepairFacility(RepairFacility repairFacility) {
        this.repairFacility = repairFacility;
    }

    @XmlTransient
    public Collection<ClaimMiscPaymentDetail> getClaimMiscPaymentDetailCollection() {
        return claimMiscPaymentDetailCollection;
    }

    public void setClaimMiscPaymentDetailCollection(Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection) {
        this.claimMiscPaymentDetailCollection = claimMiscPaymentDetailCollection;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<AccountKeeperField> getAccountKeeperFieldCollection() {
        return accountKeeperFieldCollection;
    }

    public void setAccountKeeperFieldCollection(Collection<AccountKeeperField> accountKeeperFieldCollection) {
        this.accountKeeperFieldCollection = accountKeeperFieldCollection;
    }

    @XmlTransient
    public Collection<DisbursementCode> getDisbursementCodeCollection() {
        return disbursementCodeCollection;
    }

    public void setDisbursementCodeCollection(Collection<DisbursementCode> disbursementCodeCollection) {
        this.disbursementCodeCollection = disbursementCodeCollection;
    }

    public UserMember getUserMember() {
        return userMember;
    }

    public void setUserMember(UserMember userMember) {
        this.userMember = userMember;
    }

    @XmlTransient
    public Collection<ClaimFile> getClaimFileCollection() {
        return claimFileCollection;
    }

    public void setClaimFileCollection(Collection<ClaimFile> claimFileCollection) {
        this.claimFileCollection = claimFileCollection;
    }

    public CustomerName getCustomerName() {
        return customerName;
    }

    public void setCustomerName(CustomerName customerName) {
        this.customerName = customerName;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection1() {
        return ledgerCollection1;
    }

    public void setLedgerCollection1(Collection<Ledger> ledgerCollection1) {
        this.ledgerCollection1 = ledgerCollection1;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public InspectionCompany getInspectionCompany() {
        return inspectionCompany;
    }

    public void setInspectionCompany(InspectionCompany inspectionCompany) {
        this.inspectionCompany = inspectionCompany;
    }

    public LienHolder getLienHolder() {
        return lienHolder;
    }

    public void setLienHolder(LienHolder lienHolder) {
        this.lienHolder = lienHolder;
    }

    public DisbursementAccount getDisbursementAccount() {
        return disbursementAccount;
    }

    public void setDisbursementAccount(DisbursementAccount disbursementAccount) {
        this.disbursementAccount = disbursementAccount;
    }

    @XmlTransient
    public Collection<AccountKeeperBalance> getAccountKeeperBalanceCollection() {
        return accountKeeperBalanceCollection;
    }

    public void setAccountKeeperBalanceCollection(Collection<AccountKeeperBalance> accountKeeperBalanceCollection) {
        this.accountKeeperBalanceCollection = accountKeeperBalanceCollection;
    }

    public DealerGroup getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(DealerGroup dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountKeeperId != null ? accountKeeperId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountKeeper)) {
            return false;
        }
        AccountKeeper other = (AccountKeeper) object;
        if ((this.accountKeeperId == null && other.accountKeeperId != null) || (this.accountKeeperId != null && !this.accountKeeperId.equals(other.accountKeeperId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AccountKeeper[ accountKeeperId=" + accountKeeperId + " ]";
    }
    
}
