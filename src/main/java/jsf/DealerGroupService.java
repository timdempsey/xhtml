/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.DealerGroup;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "dealerGroupService")
@ApplicationScoped
public class DealerGroupService {

    private static final Logger LOGGER = LogManager.getLogger(DealerGroupService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    @Inject
    private DealerService dealerService;

    @Inject
    private RepairFacilityService rfs;
    
    @Inject
    private NoteService noteService;

    @Inject
    private HistoryLogService hlService;

    /**
     * Creates a new instance of DealerGroupService
     */
    public DealerGroupService() {
        selectedDGs = new DealerGroups();
        stateMap = new TreeMap<>();
        sameAsPaddr = true;
        dealerGroupId = null;
        dealerGroup = new DealerGroups();
        dg = new DealerGroup();
        //directAgents = new TreeMap<>();
    }

    List<DealerGroups> dgList;

    Integer dealerGroupId;
    DealerGroups selectedDGs;
    DealerGroups dealerGroup;

    private String regionCodeP;
    private String regionCodeB;
    private TreeMap<String, String> stateMap;

    Boolean sameAsPaddr;

    Integer activeIndex;

    private String paddr;
    private String baddr;

    private String dgStatus;

    List<Dealers> dgDealersList;

    private DealerGroup dg;
    
    private List<String> remitRules;

    public List<String> getRemitRules() {
        return remitRules;
    }

    public void setRemitRules(List<String> remitRules) {
        this.remitRules = remitRules;
    }

    public DealerGroups getSelectedDGs() {
        return selectedDGs;
    }

    public void setSelectedDGs(DealerGroups selectedDGs) {
        this.selectedDGs = selectedDGs;
    }
    
    

    public DealerGroup getDg() {
        return dg;
    }

    public void setDg(DealerGroup dg) {
        this.dg = dg;
    }

    public List<Dealers> getDgDealersList() {
        return dgDealersList;
    }

    public void setDgDealersList(List<Dealers> dgDealersList) {
        this.dgDealersList = dgDealersList;
    }

    public String getDgStatus() {
        return dgStatus;
    }

    public void setDgStatus(String dgStatus) {
        this.dgStatus = dgStatus;
    }

    public String getPaddr() {
        return paddr;
    }

    public void setPaddr(String paddr) {
        this.paddr = paddr;
    }

    public String getBaddr() {
        return baddr;
    }

    public void setBaddr(String baddr) {
        this.baddr = baddr;
    }

    public Integer getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(Integer activeIndex) {
        this.activeIndex = activeIndex;
    }

    public String getRegionCodeP() {
        return regionCodeP;
    }

    public void setRegionCodeP(String regionCodeP) {
        this.regionCodeP = regionCodeP;
    }

    public String getRegionCodeB() {
        return regionCodeB;
    }

    public void setRegionCodeB(String regionCodeB) {
        this.regionCodeB = regionCodeB;
    }

    public TreeMap<String, String> getStateMap() {
        return stateMap;
    }

    public void setStateMap(TreeMap<String, String> stateMap) {
        this.stateMap = stateMap;
    }

    public Boolean getSameAsPaddr() {
        return sameAsPaddr;
    }

    public void setSameAsPaddr(Boolean sameAsPaddr) {
        this.sameAsPaddr = sameAsPaddr;
    }

    public List<DealerGroups> getDgList() {
        return dgList;
    }

    public void setDgList(List<DealerGroups> dgList) {
        this.dgList = dgList;
    }

    public Integer getDealerGroupId() {
        return dealerGroupId;
    }

    public void setDealerGroupId(Integer dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }

    public DealerGroups getSelectedDG() {
        return selectedDGs;
    }

    public void setSelectedDG(DealerGroups selectedDGs) {
        this.selectedDGs = selectedDGs;
    }

    public DealerGroups getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(DealerGroups dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    /**
     * main
     */
    /**
     *
     *
     * @return
     */
    public String getDealerGroupSearch() {
        LOGGER.info(" in getDealerGroupSearch");
        reset();
        dgList = claimMisc.getDealerGroups();

        return "DealerGroups";

    }

    public void onRowSelect(SelectEvent event) {
        selectedDGs = (DealerGroups) event.getObject();
        LOGGER.info("selectedDGs=" + selectedDGs.getDealerGroupName());
        paddr = claimMisc.printAddress(selectedDGs.getDealerGroupId(), "P");
        baddr = claimMisc.printAddress(selectedDGs.getDealerGroupId(), "B");
        dg = claimMisc.getDealerGroupById(selectedDGs.getDealerGroupId());
        dealerService.loadDealers(dg);

        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedDGs.getDealerGroupId());
        if (ak.getActive()) {
            dgStatus = "DeActivate";
        } else {
            dgStatus = "Activate";
        }

        /*
        FacesMessage msg = new FacesMessage("Program Selected", ((ProgramSearch) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
         */
    }

    public void addDealerGroup(ActionEvent event) {
        dealerGroup.reset();
        selectedDGs = null;
        dg = null;
        
        remitRules = claimMisc.loadRemitRules();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDealerGroup", options, null);

    }

    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        dgList = claimMisc.getDealerGroups();
    }

    public void editDealerGroup() {
        LOGGER.info("dealerGroupId=" + selectedDGs.getDealerGroupId());

        dealerGroup = selectedDGs;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDealerGroup", options, null);

    }

    public String deleteDealerGroup() {
        dealerGroupId = selectedDGs.getDealerGroupId();
        LOGGER.info("in deleteDealerGroup, dealerGroupId=" + dealerGroupId);
        claimMisc.deleteAccountKeeper(dealerGroupId);
        dgList = claimMisc.getDealerGroups();
        return "DealerGroups";
    }

    public void saveDealerGroup() {
        LOGGER.info("in saveDealerGroup");

        if (selectedDGs != null) {  // update
            LOGGER.info("updating dealerGroup=" + selectedDGs.getDealerGroupId());
            claimMisc.saveDealerGroup(dealerGroup, selectedDGs.getDealerGroupId());
        } else {
            LOGGER.info("save new dealer group");
            claimMisc.saveDealerGroup(dealerGroup, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddDealerGroup");

    }

    public void cancel() {
        LOGGER.info("in cancel");
        reset();
        RequestContext.getCurrentInstance().closeDialog("AddDealer");
    }

    public String getDealerGroupGeneral() {
        LOGGER.info("in getDealerGroupGeneral");

        activeIndex = 0;

        return "DealerGroupGeneral";
    }

    public void tabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);
        switch (tabId) {
            case "dgdealersTab":
                createDGdealersTab();
                activeIndex = 0;
                break;
            case "dgrfTab":
                rfs.generateRFListByDG(dg);
                activeIndex = 1;
                break;
            case "dgNotesTab":
                createDGNotesTab();
                activeIndex = 2;
                break;
            case "dgHistoryTab":
                createDGHistoryLogsTab();
                activeIndex = 3;
            /*
                case "ccNotesTab":
                    contractNoteService.createContractNotesByContractNo(contractNo);
                    activeIndex = 1;
                    break;
                case "cddTab":
                    viewDisbursementTab();
                    activeIndex = 2;
                    break;
                case "cfTab":
                    contractFieldService.createContractFields(contractNo);
                    activeIndex = 3;
                    break;
             */
            default:
                createDGdealersTab();
                break;
        }
    }

    public void createDGdealersTab() {
        if (selectedDGs != null) {

            activeIndex = 0;

            dg = claimMisc.getDealerGroupById(selectedDGs.getDealerGroupId());

            //dgDealersList = claimMisc.getDealersByGroupFkId(selectedDGs.getDealerGroupId());
            dealerService.loadDealers(dg);
        }
    }

    public void changeDGstatus() {
        LOGGER.info("in changeDGstatus");
        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedDGs.getDealerGroupId());
        if (ak.getActive()) {
            dgStatus = "Activate";
            claimMisc.changeAccountKeeperActiveStatus(ak.getAccountKeeperId(), false);
        } else {
            dgStatus = "DeActivate";
            claimMisc.changeAccountKeeperActiveStatus(ak.getAccountKeeperId(), true);
        }
    }
    
    public void createDGNotesTab() {
        if (selectedDGs != null) {

            activeIndex = 2;

            dg = claimMisc.getDealerGroupById(selectedDGs.getDealerGroupId());

            noteService.generateNotesListByDG(dg);

        }
    }

    public void createDGHistoryLogsTab() {
        if (selectedDGs != null) {

            activeIndex = 3;

            dg = claimMisc.getDealerGroupById(selectedDGs.getDealerGroupId());

            hlService.generateHistoryLogsListByDG(dg);

        }
    }

    public void reset() {
        dealerGroupId = null;
        selectedDGs = null;
        dealerGroup = new DealerGroups();
        sameAsPaddr = true;
        paddr = "";
        baddr = "";
        dg = null;
    }

}
