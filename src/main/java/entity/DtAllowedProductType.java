/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtAllowedProductType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtAllowedProductType.findAll", query = "SELECT d FROM DtAllowedProductType d")
    , @NamedQuery(name = "DtAllowedProductType.findByAllowedProductTypeId", query = "SELECT d FROM DtAllowedProductType d WHERE d.allowedProductTypeId = :allowedProductTypeId")
    , @NamedQuery(name = "DtAllowedProductType.findByName", query = "SELECT d FROM DtAllowedProductType d WHERE d.name = :name")})
public class DtAllowedProductType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "allowedProductTypeId")
    private Integer allowedProductTypeId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;

    public DtAllowedProductType() {
    }

    public DtAllowedProductType(Integer allowedProductTypeId) {
        this.allowedProductTypeId = allowedProductTypeId;
    }

    public Integer getAllowedProductTypeId() {
        return allowedProductTypeId;
    }

    public void setAllowedProductTypeId(Integer allowedProductTypeId) {
        this.allowedProductTypeId = allowedProductTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (allowedProductTypeId != null ? allowedProductTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtAllowedProductType)) {
            return false;
        }
        DtAllowedProductType other = (DtAllowedProductType) object;
        if ((this.allowedProductTypeId == null && other.allowedProductTypeId != null) || (this.allowedProductTypeId != null && !this.allowedProductTypeId.equals(other.allowedProductTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtAllowedProductType[ allowedProductTypeId=" + allowedProductTypeId + " ]";
    }
    
}
