/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "WarrantyRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WarrantyRule.findAll", query = "SELECT w FROM WarrantyRule w")
    , @NamedQuery(name = "WarrantyRule.findByWarrantyRuleId", query = "SELECT w FROM WarrantyRule w WHERE w.warrantyRuleId = :warrantyRuleId")
    , @NamedQuery(name = "WarrantyRule.findByWarrantyTypeInd", query = "SELECT w FROM WarrantyRule w WHERE w.warrantyTypeInd = :warrantyTypeInd")
    , @NamedQuery(name = "WarrantyRule.findByWarrantyRuleTypeInd", query = "SELECT w FROM WarrantyRule w WHERE w.warrantyRuleTypeInd = :warrantyRuleTypeInd")
    , @NamedQuery(name = "WarrantyRule.findByMonthValue", query = "SELECT w FROM WarrantyRule w WHERE w.monthValue = :monthValue")
    , @NamedQuery(name = "WarrantyRule.findByMileValue", query = "SELECT w FROM WarrantyRule w WHERE w.mileValue = :mileValue")
    , @NamedQuery(name = "WarrantyRule.findByCheckLastName", query = "SELECT w FROM WarrantyRule w WHERE w.checkLastName = :checkLastName")
    , @NamedQuery(name = "WarrantyRule.findByUpdateUserName", query = "SELECT w FROM WarrantyRule w WHERE w.updateUserName = :updateUserName")
    , @NamedQuery(name = "WarrantyRule.findByUpdateLast", query = "SELECT w FROM WarrantyRule w WHERE w.updateLast = :updateLast")})
public class WarrantyRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "warrantyRuleId")
    private Integer warrantyRuleId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "warrantyTypeInd")
    private int warrantyTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "warrantyRuleTypeInd")
    private int warrantyRuleTypeInd;
    @Column(name = "monthValue")
    private Integer monthValue;
    @Column(name = "mileValue")
    private Integer mileValue;
    @Column(name = "checkLastName")
    private Boolean checkLastName;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "limitIdFk", referencedColumnName = "limitId")
    @ManyToOne
    private Limit limitIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne(optional = false)
    private PlanTable planIdFk;
    @JoinColumn(name = "rateBasedRuleGroupIdFk", referencedColumnName = "rateBasedRuleGroupId")
    @ManyToOne
    private RateBasedRuleGroup rateBasedRuleGroupIdFk;
    @JoinColumn(name = "termIdFk", referencedColumnName = "TermId")
    @ManyToOne
    private Term termIdFk;

    public WarrantyRule() {
    }

    public WarrantyRule(Integer warrantyRuleId) {
        this.warrantyRuleId = warrantyRuleId;
    }

    public WarrantyRule(Integer warrantyRuleId, int warrantyTypeInd, int warrantyRuleTypeInd) {
        this.warrantyRuleId = warrantyRuleId;
        this.warrantyTypeInd = warrantyTypeInd;
        this.warrantyRuleTypeInd = warrantyRuleTypeInd;
    }

    public Integer getWarrantyRuleId() {
        return warrantyRuleId;
    }

    public void setWarrantyRuleId(Integer warrantyRuleId) {
        this.warrantyRuleId = warrantyRuleId;
    }

    public int getWarrantyTypeInd() {
        return warrantyTypeInd;
    }

    public void setWarrantyTypeInd(int warrantyTypeInd) {
        this.warrantyTypeInd = warrantyTypeInd;
    }

    public int getWarrantyRuleTypeInd() {
        return warrantyRuleTypeInd;
    }

    public void setWarrantyRuleTypeInd(int warrantyRuleTypeInd) {
        this.warrantyRuleTypeInd = warrantyRuleTypeInd;
    }

    public Integer getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(Integer monthValue) {
        this.monthValue = monthValue;
    }

    public Integer getMileValue() {
        return mileValue;
    }

    public void setMileValue(Integer mileValue) {
        this.mileValue = mileValue;
    }

    public Boolean getCheckLastName() {
        return checkLastName;
    }

    public void setCheckLastName(Boolean checkLastName) {
        this.checkLastName = checkLastName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Limit getLimitIdFk() {
        return limitIdFk;
    }

    public void setLimitIdFk(Limit limitIdFk) {
        this.limitIdFk = limitIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public RateBasedRuleGroup getRateBasedRuleGroupIdFk() {
        return rateBasedRuleGroupIdFk;
    }

    public void setRateBasedRuleGroupIdFk(RateBasedRuleGroup rateBasedRuleGroupIdFk) {
        this.rateBasedRuleGroupIdFk = rateBasedRuleGroupIdFk;
    }

    public Term getTermIdFk() {
        return termIdFk;
    }

    public void setTermIdFk(Term termIdFk) {
        this.termIdFk = termIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (warrantyRuleId != null ? warrantyRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WarrantyRule)) {
            return false;
        }
        WarrantyRule other = (WarrantyRule) object;
        if ((this.warrantyRuleId == null && other.warrantyRuleId != null) || (this.warrantyRuleId != null && !this.warrantyRuleId.equals(other.warrantyRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.WarrantyRule[ warrantyRuleId=" + warrantyRuleId + " ]";
    }
    
}
