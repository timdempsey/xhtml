/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "UserType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserType.findAll", query = "SELECT u FROM UserType u")
    , @NamedQuery(name = "UserType.findByUserTypeInd", query = "SELECT u FROM UserType u WHERE u.userTypeInd = :userTypeInd")
    , @NamedQuery(name = "UserType.findByDescription", query = "SELECT u FROM UserType u WHERE u.description = :description")})
public class UserType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userTypeInd")
    private Integer userTypeInd;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "userTypeInd")
    private Collection<UserMember> userMemberCollection;

    public UserType() {
    }

    public UserType(Integer userTypeInd) {
        this.userTypeInd = userTypeInd;
    }

    public Integer getUserTypeInd() {
        return userTypeInd;
    }

    public void setUserTypeInd(Integer userTypeInd) {
        this.userTypeInd = userTypeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<UserMember> getUserMemberCollection() {
        return userMemberCollection;
    }

    public void setUserMemberCollection(Collection<UserMember> userMemberCollection) {
        this.userMemberCollection = userMemberCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userTypeInd != null ? userTypeInd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserType)) {
            return false;
        }
        UserType other = (UserType) object;
        if ((this.userTypeInd == null && other.userTypeInd != null) || (this.userTypeInd != null && !this.userTypeInd.equals(other.userTypeInd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserType[ userTypeInd=" + userTypeInd + " ]";
    }
    
}
