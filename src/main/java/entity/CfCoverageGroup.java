/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCoverageGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCoverageGroup.findAll", query = "SELECT c FROM CfCoverageGroup c")
    , @NamedQuery(name = "CfCoverageGroup.findByCoverageGroupId", query = "SELECT c FROM CfCoverageGroup c WHERE c.coverageGroupId = :coverageGroupId")
    , @NamedQuery(name = "CfCoverageGroup.findByName", query = "SELECT c FROM CfCoverageGroup c WHERE c.name = :name")
    , @NamedQuery(name = "CfCoverageGroup.findByCode", query = "SELECT c FROM CfCoverageGroup c WHERE c.code = :code")
    , @NamedQuery(name = "CfCoverageGroup.findByUpdateUserName", query = "SELECT c FROM CfCoverageGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCoverageGroup.findByUpdateLast", query = "SELECT c FROM CfCoverageGroup c WHERE c.updateLast = :updateLast")})
public class CfCoverageGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "coverageGroupId")
    private Integer coverageGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "code")
    private String code;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "coverageGroupIdFk")
    private Collection<ContractCoverageGroup> contractCoverageGroupCollection;
    @OneToMany(mappedBy = "coverageGroupIdFk")
    private Collection<AdjustmentDetail> adjustmentDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "coverageGroupId")
    private Collection<CfCoverageGroupComponentConn> cfCoverageGroupComponentConnCollection;
    @OneToMany(mappedBy = "coverageGroupIdFk")
    private Collection<SurchargeDetail> surchargeDetailCollection;

    public CfCoverageGroup() {
    }

    public CfCoverageGroup(Integer coverageGroupId) {
        this.coverageGroupId = coverageGroupId;
    }

    public CfCoverageGroup(Integer coverageGroupId, String name) {
        this.coverageGroupId = coverageGroupId;
        this.name = name;
    }

    public Integer getCoverageGroupId() {
        return coverageGroupId;
    }

    public void setCoverageGroupId(Integer coverageGroupId) {
        this.coverageGroupId = coverageGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractCoverageGroup> getContractCoverageGroupCollection() {
        return contractCoverageGroupCollection;
    }

    public void setContractCoverageGroupCollection(Collection<ContractCoverageGroup> contractCoverageGroupCollection) {
        this.contractCoverageGroupCollection = contractCoverageGroupCollection;
    }

    @XmlTransient
    public Collection<AdjustmentDetail> getAdjustmentDetailCollection() {
        return adjustmentDetailCollection;
    }

    public void setAdjustmentDetailCollection(Collection<AdjustmentDetail> adjustmentDetailCollection) {
        this.adjustmentDetailCollection = adjustmentDetailCollection;
    }

    @XmlTransient
    public Collection<CfCoverageGroupComponentConn> getCfCoverageGroupComponentConnCollection() {
        return cfCoverageGroupComponentConnCollection;
    }

    public void setCfCoverageGroupComponentConnCollection(Collection<CfCoverageGroupComponentConn> cfCoverageGroupComponentConnCollection) {
        this.cfCoverageGroupComponentConnCollection = cfCoverageGroupComponentConnCollection;
    }

    @XmlTransient
    public Collection<SurchargeDetail> getSurchargeDetailCollection() {
        return surchargeDetailCollection;
    }

    public void setSurchargeDetailCollection(Collection<SurchargeDetail> surchargeDetailCollection) {
        this.surchargeDetailCollection = surchargeDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coverageGroupId != null ? coverageGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCoverageGroup)) {
            return false;
        }
        CfCoverageGroup other = (CfCoverageGroup) object;
        if ((this.coverageGroupId == null && other.coverageGroupId != null) || (this.coverageGroupId != null && !this.coverageGroupId.equals(other.coverageGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCoverageGroup[ coverageGroupId=" + coverageGroupId + " ]";
    }
    
}
