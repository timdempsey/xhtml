/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.CashTransaction;
import entity.Claim;
import entity.Contracts;
import entity.Ledger;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
@Named(value = "acctTransService")
@ApplicationScoped
public class AcctTransService {

    private static final Logger LOGGER = LogManager.getLogger(AcctTransService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of AcctTransService
     */
    public AcctTransService() {
    }

    private AcctTrans trans;
    private LazyDataModel<AcctTrans> transactions;
    private AcctTrans selectedTransaction;
    
    private String transNumber;
    private Integer transId;
    
    private AcctTransDetail acctTransDetail;
    
    private AcctRecItems acctRecItem;
    
    private List<String> ledgerTypeList;

    public List<String> getLedgerTypeList() {
        return ledgerTypeList;
    }

    public void setLedgerTypeList(List<String> ledgerTypeList) {
        this.ledgerTypeList = ledgerTypeList;
    }
    
    

    public AcctRecItems getAcctRecItem() {
        return acctRecItem;
    }

    public void setAcctRecItem(AcctRecItems acctRecItem) {
        this.acctRecItem = acctRecItem;
    }
    
    
    
    private LazyDataModel<AcctRecItems> acctRecItemsList;

    public LazyDataModel<AcctRecItems> getAcctRecItemsList() {
        return acctRecItemsList;
    }

    public void setAcctRecItemsList(LazyDataModel<AcctRecItems> acctRecItemsList) {
        this.acctRecItemsList = acctRecItemsList;
    }
    
    

    public AcctTransDetail getAcctTransDetail() {
        return acctTransDetail;
    }

    public void setAcctTransDetail(AcctTransDetail acctTransDetail) {
        this.acctTransDetail = acctTransDetail;
    }

    public String getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(String transNumber) {
        this.transNumber = transNumber;
    }

    
    

   

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }
    
    

    public AcctTrans getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(AcctTrans selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }
    
    
    
    public LazyDataModel<AcctTrans> getTransactions() {
        return transactions;
    }

    public void setTransactions(LazyDataModel<AcctTrans> transactions) {
        this.transactions = transactions;
    }

    public AcctTransService(AcctTrans trans) {
        this.trans = trans;
    }

    public AcctTrans getTrans() {
        return trans;
    }

    public void setTrans(AcctTrans trans) {
        this.trans = trans;
    }

    
    
    @PostConstruct
    public void init() {
        selectedTransaction = new AcctTrans();
        trans = new AcctTrans();
        acctRecItem = new AcctRecItems();
        
        ledgerTypeList = claimMisc.getLedgerTypeList();
        
        acctTransDetail = new AcctTransDetail();
    }

    /**
     *
     * @return
     */
    public String getAcctTrans() {
        LOGGER.info("in getAcctTrans");
        return "AcctTrans";
    }

    public void findTransactions() {
        try {

            transactions = new LazyDataModel<AcctTrans>() {
                @Override
                public AcctTrans getRowData(String rowKey) {
                    int intRowKey = Integer.parseInt(rowKey);
                    for (AcctTrans at : transactions) {
                        if (at.getTransactionId().equals(intRowKey)) {
                            return at;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(AcctTrans at) {
                    return at.getTransactionId();
                }

                @Override
                public List<AcctTrans> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    List<CashTransaction> ct = claimMisc.searchTransactions(trans, first, pageSize);
                    return (List<AcctTrans>) createTransactionView(ct);
                }

            };

            transactions.setRowCount(claimMisc.getSearchTransactionsCount(trans));

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public List<?> createTransactionView(List<CashTransaction> cts) {

        List<AcctTrans> atList = new ArrayList<>();

        for (CashTransaction ct : cts) {

            Ledger ledger = claimMisc.getLedgerById(ct.getCashTransactionId());
            AccountKeeper ak = claimMisc.getAccountKeeperById(ledger.getCurrentAccountKeeperIdFk().getAccountKeeperId());
            String accountType = claimMisc.getAccountKeeperTypeById(ak.getAccountKeeperTypeIdFk().getAccountKeeperTypeId()).getAccountKeeperTypeDesc();
            String accountName = ak.getAccountKeeperName();
            String contractNo = "";
            String claimNo = "";
            if (ledger.getRelatedClaimIdFk() != null && ledger.getRelatedClaimIdFk().getClaimId() > 0) {
                claimNo = claimMisc.getClaimById(ledger.getRelatedClaimIdFk().getClaimId()).getClaimNumber();
            }
            if (ledger.getRelatedContractIdFk() != null && ledger.getRelatedContractIdFk().getContractId() > 0) {
                contractNo = claimMisc.getContractsById(ledger.getRelatedContractIdFk().getContractId()).getContractNo();
            }
            String ledgeDate = "";
            if( ledger.getLedgerDate() != null ) {
                ledgeDate = claimMisc.getJavaDate(ledger.getLedgerDate());
            }
            String sentDate = "";
            if( ct.getSentDate() != null ) {
                sentDate = claimMisc.getJavaDate(ct.getSentDate());
            }
            String enteredDate = "";
            if( ledger.getEnteredDate()!= null ) {
                enteredDate = claimMisc.getJavaDate(ledger.getEnteredDate());
            }
            

            atList.add(new AcctTrans(ct.getCashTransactionId(),
                    ct.getTransactionNumber(),
                    claimMisc.getDtcashTransactionTypeById(ct.getCashTransactionTypeInd().getCashTransactionTypeId()).getDescription(),
                    claimMisc.getDtcashTransactionStatusById(ct.getCashTransactionStatusInd().getCashTransactionStatusId()).getDescription(),
                    ct.getAppliedAmount(),
                    ct.getAppliedAmount(),
                    ct.getOverUnder(),
                    accountType,
                    accountName,
                    null,
                    enteredDate,
                    sentDate,
                    ledgeDate,
                    contractNo,
                    claimNo
            ));

        }

        LOGGER.info("size of list=" + atList.size());
        return atList;
    }
    
    public void onRowSelect(SelectEvent event) {
        selectedTransaction = (AcctTrans) event.getObject();

        FacesMessage msg = new FacesMessage("Transaction Selected", ((AcctTrans) event.getObject()).getTransactionNumber());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }
    
    public void editTransaction() {
        
    }
    
     public void loadAcctTransDetail() {
         LOGGER.info("in loadAcctTransDetaili, transactionNumber=" + selectedTransaction.getTransactionNumber());
         LOGGER.info("transactionId=" + selectedTransaction.getTransactionId());
         CashTransaction ct = claimMisc.getCashTransactionById(selectedTransaction.getTransactionId());
         //CashTransaction ct = claimMisc.getCashTransactionByNumber(selectedTransaction.getTransactionNumber());
         Ledger ledger = claimMisc.getLedgerById(ct.getCashTransactionId());
         
         transId = ct.getCashTransactionId();
         transNumber = ct.getTransactionNumber();
         
         String insurer="";
         if( ledger.getInsurerIdFk() != null ) {
             insurer = claimMisc.getAccountKeeperById(ledger.getInsurerIdFk().getInsurerId()).getAccountKeeperName();
         }
         
         String sentDate = "";
         String receivedDate = "";
         if( ct.getSentDate()!=null) {
             sentDate = claimMisc.getJavaDate(ct.getSentDate());
         }
         if( ledger.getLedgerDate()!=null ) {
             receivedDate = claimMisc.getJavaDate(ledger.getLedgerDate());
         }
         
         // upper parts
         acctTransDetail.setTransactionNumber(ct.getTransactionNumber());
         acctTransDetail.setTransactionId(ct.getCashTransactionId());
         acctTransDetail.setTransType(claimMisc.getDtcashTransactionTypeById(ct.getCashTransactionTypeInd().getCashTransactionTypeId()).getDescription());
         acctTransDetail.setTransStatus(claimMisc.getDtcashTransactionStatusById(ct.getCashTransactionStatusInd().getCashTransactionStatusId()).getDescription());
         acctTransDetail.setTransactionAmount(ledger.getCurrentAmount());
         acctTransDetail.setAppliedAmount(ct.getAppliedAmount());
         acctTransDetail.setAccountName(ct.getPayeeName());
         acctTransDetail.setAdminCompany(claimMisc.getAdminCompanyById(ledger.getAdminCompanyIdFk().getAdminCompanyId()).getName());
         acctTransDetail.setInsurer(insurer);
         acctTransDetail.setSentDate(sentDate);
         acctTransDetail.setReceivedDate(receivedDate);
         
         loadDetailTbl();   
     }
     
     public List<AcctRecItems> createAcctRecItemsList( List<Ledger> ledgerList) {
        try {
            LOGGER.info("in createAcctRecItemsList" );
            List<AcctRecItems> aritemList = new ArrayList<>();
            LOGGER.info("size of ledgerList=" + ledgerList.size());
            String ledgerType;
            Contracts contract;
            Claim claim;
            String claimNumber = "";
            BigDecimal adminCredit = claimMisc.BIGZERO;
            BigDecimal adminDebit = claimMisc.BIGZERO;
            BigDecimal adminCommCredit = claimMisc.BIGZERO;
            BigDecimal adminCommDebt = claimMisc.BIGZERO;
            String ledgerDate = "";
            String enteredDate = "";
            for (Ledger ledger : ledgerList) {
                ledgerType = claimMisc.getDtLedgerTypeById(ledger.getLedgerTypeInd()).getDescription();
                if( acctRecItem.getLedgeType()!=null && !acctRecItem.getLedgeType().equalsIgnoreCase("All") && !acctRecItem.getLedgeType().equalsIgnoreCase(ledgerType) ) {
                    continue;
                }
                contract = claimMisc.getContractsById(ledger.getRelatedContractIdFk().getContractId());
                if (acctRecItem.getContractNo() != null && acctRecItem.getContractNo().length() > 0 && !acctRecItem.getContractNo().equalsIgnoreCase(contract.getContractNo())) {
                    continue;
                }
                if (acctRecItem.getVin() != null && acctRecItem.getVin().length() > 0 && !acctRecItem.getVin().equalsIgnoreCase(contract.getVinFull())) {
                    continue;
                }
                if (acctRecItem.getCustLastName() != null && acctRecItem.getCustLastName().length() > 0 && !acctRecItem.getCustLastName().equalsIgnoreCase(contract.getCustomerIdFk().getLastName())) {
                    continue;
                }
                if (ledger.getRelatedClaimIdFk() != null) {
                    claim = ledger.getRelatedClaimIdFk();
                    claimNumber = claim.getClaimNumber();
                    if (acctRecItem.getClaimNo() != null && acctRecItem.getClaimNo().length() > 0 && !acctRecItem.getClaimNo().equalsIgnoreCase(claimNumber)) {
                        continue;
                    }
                }

                switch (ledger.getLedgerTypeInd()) {
                    case 1:
                    case 2:
                        adminDebit = ledger.getCurrentAmount();
                        if (acctRecItem.getAdminDebit() != null && acctRecItem.getAdminDebit() != adminDebit) {
                            continue;
                        }
                        break;
                    case 3:
                    case 14:
                        adminCredit = ledger.getCurrentAmount();
                        if (acctRecItem.getAdminCredit() != null && acctRecItem.getAdminCredit() != adminCredit) {
                            continue;
                        }
                        break;
                    default:
                        adminDebit = null;
                        adminCredit = null;
                        break;
                }
                if (ledger.getLedgerDate() != null) {
                    ledgerDate = claimMisc.getJavaDate(ledger.getLedgerDate());
                    if( acctRecItem.getPostedDate()!=null && acctRecItem.getPostedDate().length()>0 && !acctRecItem.getPostedDate().equalsIgnoreCase(ledgerDate) ) {
                        continue;
                    }
                }
                if( ledger.getEnteredDate() != null ) {
                    enteredDate = claimMisc.getJavaDate(ledger.getEnteredDate());
                    if( acctRecItem.getEditDate()!=null && acctRecItem.getEditDate().length()>0 && !acctRecItem.getEditDate().equalsIgnoreCase(enteredDate)) {
                        continue;
                    }
                }

                aritemList.add(new AcctRecItems(
                        ledger.getLedgerId(),
                        ledgerType,
                        contract.getContractNo(),
                        contract.getVinFull(),
                        contract.getCustomerIdFk().getLastName(),
                        claimNumber,
                        adminCredit,
                        adminDebit,
                        adminCommCredit,
                        adminCommDebt,
                        enteredDate,
                        ledgerDate
                ));
            }

            LOGGER.info("size of aritemList=" + aritemList.size());
            return aritemList;

        } catch (NoResultException e) {
            LOGGER.info("createAcctRecItemsList is null");
            return null;
        }
    }
     
     public void loadDetailTbl() {
         LOGGER.info("in loadDetailTbl, transactioinId=" + transId);
         
         // table parts
         acctRecItemsList = new LazyDataModel<AcctRecItems>() {
            @Override
            public AcctRecItems getRowData(String rowKey) {
                int intRowKey = Integer.parseInt(rowKey);
                for (AcctRecItems at : acctRecItemsList) {
                    if (at.getLedgeId().equals(intRowKey)) {
                        return at;
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(AcctRecItems at) {
                return at.getLedgeId();
            }

            @Override
            public List<AcctRecItems> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List<Ledger> ll = claimMisc.searchLedgerByTransId(transId, first, pageSize);
                return (List<AcctRecItems>) createAcctRecItemsList(ll);
            }

        };

        acctRecItemsList.setRowCount(claimMisc.getSearchLedgerByTransIdCount(transId));      
         
         
     }
    
    public void getTransDetail() {
        LOGGER.info("in getTransDetail, transactionNumber=" + selectedTransaction.getTransactionNumber());
        LOGGER.info("in getTransDetail, transactionId=" + selectedTransaction.getTransactionId());
        
        loadAcctTransDetail();
   
        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AcctTransDetail", options, null);

    }
    
    public void printTransaction() {
        String SSRS_IP = "https://seadmin-reporting.stoneeagle.com";
        String path = ""; 
                // rs:Format=Excel
        try {
        String url = "http://" + SSRS_IP + "/ReportServer?/" + path + "&rs:Format=PDF";

        FacesContext.getCurrentInstance().getExternalContext().redirect(url);

    } catch (IOException e) {
        throw new FacesException(e);
    }

    }

    public void reset() {
        trans.reset();
        transactions = null;
    }

}
