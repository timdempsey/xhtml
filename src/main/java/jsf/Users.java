/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class Users {

    private static final Logger LOGGER = LogManager.getLogger(ContractSearch.class);

    public Users() {
    }

    public Users(Integer userMemberId, String fullName, String userName, String userType, String company, String group, String email, String phone, Boolean active, Boolean isFiltered, Boolean lockedOut, Boolean emailOptIn, String systemCode, String taxId) {
        this.userMemberId = userMemberId;
        this.fullName = fullName;
        this.userName = userName;
        this.userType = userType;
        this.company = company;
        this.group = group;
        this.email = email;
        this.phone = phone;
        this.active = active;
        this.isFiltered = isFiltered;
        this.lockedOut = lockedOut;
        this.emailOptIn = emailOptIn;
        this.systemCode = systemCode;
        this.taxId = taxId;
    }

    
    

    

    
    

    private Integer userMemberId;
    private String fullName;
    private String userName;
    private String userType;
    private String company;
    private String group;
    private String email;
    private String phone;
    private Boolean active;
    private Boolean isFiltered;
    private Boolean lockedOut;
    private Boolean emailOptIn;
    private String systemCode;
    private String taxId;

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
    
    

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }
    
    

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }
    
    

    public Integer getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(Integer userMemberId) {
        this.userMemberId = userMemberId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getIsFiltered() {
        return isFiltered;
    }

    public void setIsFiltered(Boolean isFiltered) {
        this.isFiltered = isFiltered;
    }

    public Boolean getLockedOut() {
        return lockedOut;
    }

    public void setLockedOut(Boolean lockedOut) {
        this.lockedOut = lockedOut;
    }

    
    

    public void reset() {
        userMemberId = null;
        fullName = "";
        userName = "";
        userType = "";
        company = "";
        group = "";
        email = "";
        phone = "";
        active = true;
        isFiltered = false;
        lockedOut = false;
        emailOptIn = false;
        systemCode = "";
        taxId = "";
    }

}
