/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;

/**
 *
 * @author Jiepi
 */
public class ClaimGeneralANC implements Serializable{

    public ClaimGeneralANC() {
    }

    public ClaimGeneralANC(Integer claimId, String claimNumber, Integer odometer, String roNumber, String paidTo, String reason, String inceptionDate, BigDecimal amount, String assignedTo) {
        this.claimId = claimId;
        this.claimNumber = claimNumber;
        this.odometer = odometer;
        this.roNumber = roNumber;
        this.paidTo = paidTo;
        this.reason = reason;
        this.inceptionDate = inceptionDate;
        this.amount = amount;
        this.assignedTo = assignedTo;
    }
    
    public ClaimGeneralANC(ClaimGeneralANC cganc) {
        this.claimId = cganc.claimId;
        this.claimNumber = cganc.claimNumber;
        this.odometer = cganc.odometer;
        this.roNumber = cganc.roNumber;
        this.paidTo = cganc.paidTo;
        this.reason = cganc.reason;
        this.inceptionDate = cganc.inceptionDate;
        this.amount = cganc.amount;
        this.assignedTo = cganc.assignedTo;
    }


    
    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    @Column(name = "currentOdometer", table = "Claim")
    Integer odometer;
    @Column(name = "repairOrderNumber", table = "Claim")
    String roNumber;
    @Column(name = "paidToFk", table = "Claim")
    String paidTo;
    @Column(name = "reason", table = "Claim")
    String reason;
    @Column(name = "inceptionDate", table = "Claim")
    String inceptionDate;
    @Column(name = "amount", table = "Claim")
    BigDecimal amount;
    @Column(name = "ownedByFk", table = "Claim")
    String assignedTo;
    

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public String getRoNumber() {
        return roNumber;
    }

    public void setRoNumber(String roNumber) {
        this.roNumber = roNumber;
    }

    public String getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(String paidTo) {
        this.paidTo = paidTo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(String inceptionDate) {
        this.inceptionDate = inceptionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
    
    
    
    
}
