/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;

/**
 *
 * @author Jiepi
 */
public class AcctRec {

    public AcctRec() {
    }

    public AcctRec(String adminCompany, Integer acctId, String acctType, String acctName, BigDecimal adminToPay, BigDecimal adminToReceive, BigDecimal adminTotal) {
        this.adminCompany = adminCompany;
        this.acctId = acctId;
        this.acctType = acctType;
        this.acctName = acctName;
        this.adminToPay = adminToPay;
        this.adminToReceive = adminToReceive;
        this.adminTotal = adminTotal;
    }

   

    private String acctHolderIn;
    private String adminIn;
    private String adminCompany;
    private Integer acctId;
    private String acctType;
    private String acctName;
    private BigDecimal adminToPay;
    private BigDecimal adminToReceive;
    private BigDecimal adminTotal;

    public String getAcctHolderIn() {
        return acctHolderIn;
    }

    public void setAcctHolderIn(String acctHolderIn) {
        this.acctHolderIn = acctHolderIn;
    }

    public String getAdminIn() {
        return adminIn;
    }

    public void setAdminIn(String adminIn) {
        this.adminIn = adminIn;
    }

    public Integer getAcctId() {
        return acctId;
    }

    public void setAcctId(Integer acctId) {
        this.acctId = acctId;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public BigDecimal getAdminToPay() {
        return adminToPay;
    }

    public void setAdminToPay(BigDecimal adminToPay) {
        this.adminToPay = adminToPay;
    }

    public BigDecimal getAdminToReceive() {
        return adminToReceive;
    }

    public void setAdminToReceive(BigDecimal adminToReceive) {
        this.adminToReceive = adminToReceive;
    }

    public BigDecimal getAdminTotal() {
        return adminTotal;
    }

    public void setAdminTotal(BigDecimal adminTotal) {
        this.adminTotal = adminTotal;
    }

    public void reset() {
        acctHolderIn = "";
        adminIn = "";
        adminCompany = "";
        acctId = null;
        acctType = "";
        acctName = "";
        adminToPay = null;
        adminToReceive = null;
        adminTotal = null;
    }

}
