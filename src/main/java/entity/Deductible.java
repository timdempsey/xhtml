/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Deductible")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Deductible.findAll", query = "SELECT d FROM Deductible d")
    , @NamedQuery(name = "Deductible.findByDeductibleId", query = "SELECT d FROM Deductible d WHERE d.deductibleId = :deductibleId")
    , @NamedQuery(name = "Deductible.findByDeductibleTypeInd", query = "SELECT d FROM Deductible d WHERE d.deductibleTypeInd = :deductibleTypeInd")
    , @NamedQuery(name = "Deductible.findByInternalName", query = "SELECT d FROM Deductible d WHERE d.internalName = :internalName")
    , @NamedQuery(name = "Deductible.findByDescription", query = "SELECT d FROM Deductible d WHERE d.description = :description")
    , @NamedQuery(name = "Deductible.findByDeductibleAmount", query = "SELECT d FROM Deductible d WHERE d.deductibleAmount = :deductibleAmount")
    , @NamedQuery(name = "Deductible.findByUpdateUserName", query = "SELECT d FROM Deductible d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "Deductible.findByUpdateLast", query = "SELECT d FROM Deductible d WHERE d.updateLast = :updateLast")})
public class Deductible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "deductibleId")
    private Integer deductibleId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deductibleTypeInd")
    private int deductibleTypeInd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "internalName")
    private String internalName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "deductibleAmount")
    private BigDecimal deductibleAmount;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "defaultDeductibleIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "deductibleIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "deductibleIdFk")
    private Collection<DeductibleDetail> deductibleDetailCollection;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @OneToMany(mappedBy = "relatedDeductibleIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;

    public Deductible() {
    }

    public Deductible(Integer deductibleId) {
        this.deductibleId = deductibleId;
    }

    public Deductible(Integer deductibleId, int deductibleTypeInd, String internalName, String description, BigDecimal deductibleAmount) {
        this.deductibleId = deductibleId;
        this.deductibleTypeInd = deductibleTypeInd;
        this.internalName = internalName;
        this.description = description;
        this.deductibleAmount = deductibleAmount;
    }

    public Integer getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Integer deductibleId) {
        this.deductibleId = deductibleId;
    }

    public int getDeductibleTypeInd() {
        return deductibleTypeInd;
    }

    public void setDeductibleTypeInd(int deductibleTypeInd) {
        this.deductibleTypeInd = deductibleTypeInd;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDeductibleAmount() {
        return deductibleAmount;
    }

    public void setDeductibleAmount(BigDecimal deductibleAmount) {
        this.deductibleAmount = deductibleAmount;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<DeductibleDetail> getDeductibleDetailCollection() {
        return deductibleDetailCollection;
    }

    public void setDeductibleDetailCollection(Collection<DeductibleDetail> deductibleDetailCollection) {
        this.deductibleDetailCollection = deductibleDetailCollection;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deductibleId != null ? deductibleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deductible)) {
            return false;
        }
        Deductible other = (Deductible) object;
        if ((this.deductibleId == null && other.deductibleId != null) || (this.deductibleId != null && !this.deductibleId.equals(other.deductibleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Deductible[ deductibleId=" + deductibleId + " ]";
    }
    
}
