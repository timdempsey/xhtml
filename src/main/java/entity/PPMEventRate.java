/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMEventRate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMEventRate.findAll", query = "SELECT p FROM PPMEventRate p")
    , @NamedQuery(name = "PPMEventRate.findByPPMEventRateId", query = "SELECT p FROM PPMEventRate p WHERE p.pPMEventRateId = :pPMEventRateId")
    , @NamedQuery(name = "PPMEventRate.findByCost", query = "SELECT p FROM PPMEventRate p WHERE p.cost = :cost")
    , @NamedQuery(name = "PPMEventRate.findByUpdateUserName", query = "SELECT p FROM PPMEventRate p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMEventRate.findByUpdateLast", query = "SELECT p FROM PPMEventRate p WHERE p.updateLast = :updateLast")})
public class PPMEventRate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMEventRateId")
    private Integer pPMEventRateId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Cost")
    private BigDecimal cost;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "PPMEventIdFk", referencedColumnName = "PPMEventId")
    @ManyToOne
    private PPMEvent pPMEventIdFk;
    @JoinColumn(name = "rateIdFk", referencedColumnName = "RateId")
    @ManyToOne
    private Rate rateIdFk;

    public PPMEventRate() {
    }

    public PPMEventRate(Integer pPMEventRateId) {
        this.pPMEventRateId = pPMEventRateId;
    }

    public Integer getPPMEventRateId() {
        return pPMEventRateId;
    }

    public void setPPMEventRateId(Integer pPMEventRateId) {
        this.pPMEventRateId = pPMEventRateId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public PPMEvent getPPMEventIdFk() {
        return pPMEventIdFk;
    }

    public void setPPMEventIdFk(PPMEvent pPMEventIdFk) {
        this.pPMEventIdFk = pPMEventIdFk;
    }

    public Rate getRateIdFk() {
        return rateIdFk;
    }

    public void setRateIdFk(Rate rateIdFk) {
        this.rateIdFk = rateIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMEventRateId != null ? pPMEventRateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMEventRate)) {
            return false;
        }
        PPMEventRate other = (PPMEventRate) object;
        if ((this.pPMEventRateId == null && other.pPMEventRateId != null) || (this.pPMEventRateId != null && !this.pPMEventRateId.equals(other.pPMEventRateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMEventRate[ pPMEventRateId=" + pPMEventRateId + " ]";
    }
    
}
