/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfPDFDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfPDFDetail.findAll", query = "SELECT c FROM CfPDFDetail c")
    , @NamedQuery(name = "CfPDFDetail.findByPDFDetailId", query = "SELECT c FROM CfPDFDetail c WHERE c.pDFDetailId = :pDFDetailId")
    , @NamedQuery(name = "CfPDFDetail.findByFlattenPDFInd", query = "SELECT c FROM CfPDFDetail c WHERE c.flattenPDFInd = :flattenPDFInd")
    , @NamedQuery(name = "CfPDFDetail.findByEffectiveDate", query = "SELECT c FROM CfPDFDetail c WHERE c.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "CfPDFDetail.findByEnteredDate", query = "SELECT c FROM CfPDFDetail c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CfPDFDetail.findByDeletedInd", query = "SELECT c FROM CfPDFDetail c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfPDFDetail.findByUpdateUserName", query = "SELECT c FROM CfPDFDetail c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfPDFDetail.findByUpdateLast", query = "SELECT c FROM CfPDFDetail c WHERE c.updateLast = :updateLast")})
public class CfPDFDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PDFDetailId")
    private Integer pDFDetailId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "flattenPDFInd")
    private boolean flattenPDFInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "relatedPDFDetailIdFk")
    private Collection<ContractPdfFile> contractPdfFileCollection;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "fileIdFk", referencedColumnName = "fileId")
    @ManyToOne(optional = false)
    private CfFile fileIdFk;
    @JoinColumn(name = "PDFIdFk", referencedColumnName = "PDFId")
    @ManyToOne(optional = false)
    private CfPDF pDFIdFk;
    @JoinColumn(name = "stylesheetIdFk", referencedColumnName = "stylesheetId")
    @ManyToOne
    private CfStylesheet stylesheetIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public CfPDFDetail() {
    }

    public CfPDFDetail(Integer pDFDetailId) {
        this.pDFDetailId = pDFDetailId;
    }

    public CfPDFDetail(Integer pDFDetailId, boolean flattenPDFInd, Date effectiveDate, Date enteredDate, boolean deletedInd) {
        this.pDFDetailId = pDFDetailId;
        this.flattenPDFInd = flattenPDFInd;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.deletedInd = deletedInd;
    }

    public Integer getPDFDetailId() {
        return pDFDetailId;
    }

    public void setPDFDetailId(Integer pDFDetailId) {
        this.pDFDetailId = pDFDetailId;
    }

    public boolean getFlattenPDFInd() {
        return flattenPDFInd;
    }

    public void setFlattenPDFInd(boolean flattenPDFInd) {
        this.flattenPDFInd = flattenPDFInd;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractPdfFile> getContractPdfFileCollection() {
        return contractPdfFileCollection;
    }

    public void setContractPdfFileCollection(Collection<ContractPdfFile> contractPdfFileCollection) {
        this.contractPdfFileCollection = contractPdfFileCollection;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public CfFile getFileIdFk() {
        return fileIdFk;
    }

    public void setFileIdFk(CfFile fileIdFk) {
        this.fileIdFk = fileIdFk;
    }

    public CfPDF getPDFIdFk() {
        return pDFIdFk;
    }

    public void setPDFIdFk(CfPDF pDFIdFk) {
        this.pDFIdFk = pDFIdFk;
    }

    public CfStylesheet getStylesheetIdFk() {
        return stylesheetIdFk;
    }

    public void setStylesheetIdFk(CfStylesheet stylesheetIdFk) {
        this.stylesheetIdFk = stylesheetIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pDFDetailId != null ? pDFDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfPDFDetail)) {
            return false;
        }
        CfPDFDetail other = (CfPDFDetail) object;
        if ((this.pDFDetailId == null && other.pDFDetailId != null) || (this.pDFDetailId != null && !this.pDFDetailId.equals(other.pDFDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfPDFDetail[ pDFDetailId=" + pDFDetailId + " ]";
    }
    
}
