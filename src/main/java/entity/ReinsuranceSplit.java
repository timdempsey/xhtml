/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceSplit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceSplit.findAll", query = "SELECT r FROM ReinsuranceSplit r")
    , @NamedQuery(name = "ReinsuranceSplit.findByReinsuranceSplitId", query = "SELECT r FROM ReinsuranceSplit r WHERE r.reinsuranceSplitId = :reinsuranceSplitId")
    , @NamedQuery(name = "ReinsuranceSplit.findByFeePercent", query = "SELECT r FROM ReinsuranceSplit r WHERE r.feePercent = :feePercent")
    , @NamedQuery(name = "ReinsuranceSplit.findByFeeAsDollarAmount", query = "SELECT r FROM ReinsuranceSplit r WHERE r.feeAsDollarAmount = :feeAsDollarAmount")
    , @NamedQuery(name = "ReinsuranceSplit.findByAdminPaidCedingFee", query = "SELECT r FROM ReinsuranceSplit r WHERE r.adminPaidCedingFee = :adminPaidCedingFee")
    , @NamedQuery(name = "ReinsuranceSplit.findBySplitPercent", query = "SELECT r FROM ReinsuranceSplit r WHERE r.splitPercent = :splitPercent")
    , @NamedQuery(name = "ReinsuranceSplit.findByPremiumTaxPercent", query = "SELECT r FROM ReinsuranceSplit r WHERE r.premiumTaxPercent = :premiumTaxPercent")
    , @NamedQuery(name = "ReinsuranceSplit.findByFederalExciseTaxPercent", query = "SELECT r FROM ReinsuranceSplit r WHERE r.federalExciseTaxPercent = :federalExciseTaxPercent")
    , @NamedQuery(name = "ReinsuranceSplit.findByTakesRoundingErrors", query = "SELECT r FROM ReinsuranceSplit r WHERE r.takesRoundingErrors = :takesRoundingErrors")
    , @NamedQuery(name = "ReinsuranceSplit.findByUpdateUserName", query = "SELECT r FROM ReinsuranceSplit r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceSplit.findByUpdateLast", query = "SELECT r FROM ReinsuranceSplit r WHERE r.updateLast = :updateLast")})
public class ReinsuranceSplit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ReinsuranceSplitId")
    private Integer reinsuranceSplitId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "feePercent")
    private BigDecimal feePercent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "feeAsDollarAmount")
    private boolean feeAsDollarAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminPaidCedingFee")
    private boolean adminPaidCedingFee;
    @Column(name = "splitPercent")
    private BigDecimal splitPercent;
    @Column(name = "premiumTaxPercent")
    private BigDecimal premiumTaxPercent;
    @Column(name = "federalExciseTaxPercent")
    private BigDecimal federalExciseTaxPercent;
    @Column(name = "takesRoundingErrors")
    private Boolean takesRoundingErrors;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "insurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer insurerIdFk;
    @JoinColumn(name = "reinsuranceRuleDetailIdFk", referencedColumnName = "reinsuranceRuleDetailId")
    @ManyToOne
    private ReinsuranceRuleDetail reinsuranceRuleDetailIdFk;
    @JoinColumn(name = "reinsuranceVersionCodeIdFk", referencedColumnName = "reinsuranceVersionCodeId")
    @ManyToOne
    private ReinsuranceVersionCode reinsuranceVersionCodeIdFk;

    public ReinsuranceSplit() {
    }

    public ReinsuranceSplit(Integer reinsuranceSplitId) {
        this.reinsuranceSplitId = reinsuranceSplitId;
    }

    public ReinsuranceSplit(Integer reinsuranceSplitId, boolean feeAsDollarAmount, boolean adminPaidCedingFee) {
        this.reinsuranceSplitId = reinsuranceSplitId;
        this.feeAsDollarAmount = feeAsDollarAmount;
        this.adminPaidCedingFee = adminPaidCedingFee;
    }

    public Integer getReinsuranceSplitId() {
        return reinsuranceSplitId;
    }

    public void setReinsuranceSplitId(Integer reinsuranceSplitId) {
        this.reinsuranceSplitId = reinsuranceSplitId;
    }

    public BigDecimal getFeePercent() {
        return feePercent;
    }

    public void setFeePercent(BigDecimal feePercent) {
        this.feePercent = feePercent;
    }

    public boolean getFeeAsDollarAmount() {
        return feeAsDollarAmount;
    }

    public void setFeeAsDollarAmount(boolean feeAsDollarAmount) {
        this.feeAsDollarAmount = feeAsDollarAmount;
    }

    public boolean getAdminPaidCedingFee() {
        return adminPaidCedingFee;
    }

    public void setAdminPaidCedingFee(boolean adminPaidCedingFee) {
        this.adminPaidCedingFee = adminPaidCedingFee;
    }

    public BigDecimal getSplitPercent() {
        return splitPercent;
    }

    public void setSplitPercent(BigDecimal splitPercent) {
        this.splitPercent = splitPercent;
    }

    public BigDecimal getPremiumTaxPercent() {
        return premiumTaxPercent;
    }

    public void setPremiumTaxPercent(BigDecimal premiumTaxPercent) {
        this.premiumTaxPercent = premiumTaxPercent;
    }

    public BigDecimal getFederalExciseTaxPercent() {
        return federalExciseTaxPercent;
    }

    public void setFederalExciseTaxPercent(BigDecimal federalExciseTaxPercent) {
        this.federalExciseTaxPercent = federalExciseTaxPercent;
    }

    public Boolean getTakesRoundingErrors() {
        return takesRoundingErrors;
    }

    public void setTakesRoundingErrors(Boolean takesRoundingErrors) {
        this.takesRoundingErrors = takesRoundingErrors;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Insurer getInsurerIdFk() {
        return insurerIdFk;
    }

    public void setInsurerIdFk(Insurer insurerIdFk) {
        this.insurerIdFk = insurerIdFk;
    }

    public ReinsuranceRuleDetail getReinsuranceRuleDetailIdFk() {
        return reinsuranceRuleDetailIdFk;
    }

    public void setReinsuranceRuleDetailIdFk(ReinsuranceRuleDetail reinsuranceRuleDetailIdFk) {
        this.reinsuranceRuleDetailIdFk = reinsuranceRuleDetailIdFk;
    }

    public ReinsuranceVersionCode getReinsuranceVersionCodeIdFk() {
        return reinsuranceVersionCodeIdFk;
    }

    public void setReinsuranceVersionCodeIdFk(ReinsuranceVersionCode reinsuranceVersionCodeIdFk) {
        this.reinsuranceVersionCodeIdFk = reinsuranceVersionCodeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceSplitId != null ? reinsuranceSplitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceSplit)) {
            return false;
        }
        ReinsuranceSplit other = (ReinsuranceSplit) object;
        if ((this.reinsuranceSplitId == null && other.reinsuranceSplitId != null) || (this.reinsuranceSplitId != null && !this.reinsuranceSplitId.equals(other.reinsuranceSplitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceSplit[ reinsuranceSplitId=" + reinsuranceSplitId + " ]";
    }
    
}
