/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Note")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Note.findAll", query = "SELECT n FROM Note n")
    , @NamedQuery(name = "Note.findByNoteId", query = "SELECT n FROM Note n WHERE n.noteId = :noteId")
    , @NamedQuery(name = "Note.findByNote", query = "SELECT n FROM Note n WHERE n.note = :note")
    , @NamedQuery(name = "Note.findByEnteredDate", query = "SELECT n FROM Note n WHERE n.enteredDate = :enteredDate")
    , @NamedQuery(name = "Note.findByNoteTypeInd", query = "SELECT n FROM Note n WHERE n.noteTypeInd = :noteTypeInd")
    , @NamedQuery(name = "Note.findByUpdateUserName", query = "SELECT n FROM Note n WHERE n.updateUserName = :updateUserName")
    , @NamedQuery(name = "Note.findByUpdateLast", query = "SELECT n FROM Note n WHERE n.updateLast = :updateLast")})
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "noteId")
    private Integer noteId;
    @Size(max = 2147483647)
    @Column(name = "note")
    private String note;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "noteTypeInd")
    private int noteTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "classTableIdFk", referencedColumnName = "classTableId")
    @ManyToOne
    private ClassTable classTableIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "disbursementDetailIdFk", referencedColumnName = "disbursementDetailId")
    @ManyToOne
    private DisbursementDetail disbursementDetailIdFk;
    @JoinColumn(name = "ledgerIdFk", referencedColumnName = "ledgerId")
    @ManyToOne
    private Ledger ledgerIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne
    private Product productIdFk;
    @JoinColumn(name = "rateDetailIdFk", referencedColumnName = "rateDetailId")
    @ManyToOne
    private RateDetail rateDetailIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public Note() {
    }

    public Note(Integer noteId) {
        this.noteId = noteId;
    }

    public Note(Integer noteId, int noteTypeInd) {
        this.noteId = noteId;
        this.noteTypeInd = noteTypeInd;
    }

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public int getNoteTypeInd() {
        return noteTypeInd;
    }

    public void setNoteTypeInd(int noteTypeInd) {
        this.noteTypeInd = noteTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public ClassTable getClassTableIdFk() {
        return classTableIdFk;
    }

    public void setClassTableIdFk(ClassTable classTableIdFk) {
        this.classTableIdFk = classTableIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public DisbursementDetail getDisbursementDetailIdFk() {
        return disbursementDetailIdFk;
    }

    public void setDisbursementDetailIdFk(DisbursementDetail disbursementDetailIdFk) {
        this.disbursementDetailIdFk = disbursementDetailIdFk;
    }

    public Ledger getLedgerIdFk() {
        return ledgerIdFk;
    }

    public void setLedgerIdFk(Ledger ledgerIdFk) {
        this.ledgerIdFk = ledgerIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    public RateDetail getRateDetailIdFk() {
        return rateDetailIdFk;
    }

    public void setRateDetailIdFk(RateDetail rateDetailIdFk) {
        this.rateDetailIdFk = rateDetailIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteId != null ? noteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Note)) {
            return false;
        }
        Note other = (Note) object;
        if ((this.noteId == null && other.noteId != null) || (this.noteId != null && !this.noteId.equals(other.noteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Note[ noteId=" + noteId + " ]";
    }
    
}
