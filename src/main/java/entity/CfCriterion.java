/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCriterion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCriterion.findAll", query = "SELECT c FROM CfCriterion c")
    , @NamedQuery(name = "CfCriterion.findByCriterionId", query = "SELECT c FROM CfCriterion c WHERE c.criterionId = :criterionId")
    , @NamedQuery(name = "CfCriterion.findByCriterionRuleInd", query = "SELECT c FROM CfCriterion c WHERE c.criterionRuleInd = :criterionRuleInd")
    , @NamedQuery(name = "CfCriterion.findByOperationInd", query = "SELECT c FROM CfCriterion c WHERE c.operationInd = :operationInd")
    , @NamedQuery(name = "CfCriterion.findByCriterionValue", query = "SELECT c FROM CfCriterion c WHERE c.criterionValue = :criterionValue")
    , @NamedQuery(name = "CfCriterion.findByUpdateUserName", query = "SELECT c FROM CfCriterion c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCriterion.findByUpdateLast", query = "SELECT c FROM CfCriterion c WHERE c.updateLast = :updateLast")})
public class CfCriterion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "criterionId")
    private Integer criterionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "criterionRuleInd")
    private int criterionRuleInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "operationInd")
    private int operationInd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "criterionValue")
    private String criterionValue;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne(optional = false)
    private CfCriterionGroup criterionGroupIdFk;

    public CfCriterion() {
    }

    public CfCriterion(Integer criterionId) {
        this.criterionId = criterionId;
    }

    public CfCriterion(Integer criterionId, int criterionRuleInd, int operationInd, String criterionValue) {
        this.criterionId = criterionId;
        this.criterionRuleInd = criterionRuleInd;
        this.operationInd = operationInd;
        this.criterionValue = criterionValue;
    }

    public Integer getCriterionId() {
        return criterionId;
    }

    public void setCriterionId(Integer criterionId) {
        this.criterionId = criterionId;
    }

    public int getCriterionRuleInd() {
        return criterionRuleInd;
    }

    public void setCriterionRuleInd(int criterionRuleInd) {
        this.criterionRuleInd = criterionRuleInd;
    }

    public int getOperationInd() {
        return operationInd;
    }

    public void setOperationInd(int operationInd) {
        this.operationInd = operationInd;
    }

    public String getCriterionValue() {
        return criterionValue;
    }

    public void setCriterionValue(String criterionValue) {
        this.criterionValue = criterionValue;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterionId != null ? criterionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCriterion)) {
            return false;
        }
        CfCriterion other = (CfCriterion) object;
        if ((this.criterionId == null && other.criterionId != null) || (this.criterionId != null && !this.criterionId.equals(other.criterionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCriterion[ criterionId=" + criterionId + " ]";
    }
    
}
