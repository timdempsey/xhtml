/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateBasedRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateBasedRule.findAll", query = "SELECT r FROM RateBasedRule r")
    , @NamedQuery(name = "RateBasedRule.findByRateBasedRuleId", query = "SELECT r FROM RateBasedRule r WHERE r.rateBasedRuleId = :rateBasedRuleId")
    , @NamedQuery(name = "RateBasedRule.findByClassCode", query = "SELECT r FROM RateBasedRule r WHERE r.classCode = :classCode")
    , @NamedQuery(name = "RateBasedRule.findByUpdateUserName", query = "SELECT r FROM RateBasedRule r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateBasedRule.findByUpdateLast", query = "SELECT r FROM RateBasedRule r WHERE r.updateLast = :updateLast")})
public class RateBasedRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RateBasedRuleId")
    private Integer rateBasedRuleId;
    @Size(max = 20)
    @Column(name = "classCode")
    private String classCode;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "limitIdFk", referencedColumnName = "limitId")
    @ManyToOne
    private Limit limitIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne
    private Product productIdFk;
    @JoinColumn(name = "programIdFk", referencedColumnName = "programId")
    @ManyToOne
    private Program programIdFk;
    @JoinColumn(name = "rateBasedRuleGroupIdFk", referencedColumnName = "rateBasedRuleGroupId")
    @ManyToOne
    private RateBasedRuleGroup rateBasedRuleGroupIdFk;
    @JoinColumn(name = "termIdFk", referencedColumnName = "TermId")
    @ManyToOne
    private Term termIdFk;

    public RateBasedRule() {
    }

    public RateBasedRule(Integer rateBasedRuleId) {
        this.rateBasedRuleId = rateBasedRuleId;
    }

    public Integer getRateBasedRuleId() {
        return rateBasedRuleId;
    }

    public void setRateBasedRuleId(Integer rateBasedRuleId) {
        this.rateBasedRuleId = rateBasedRuleId;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Limit getLimitIdFk() {
        return limitIdFk;
    }

    public void setLimitIdFk(Limit limitIdFk) {
        this.limitIdFk = limitIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    public Program getProgramIdFk() {
        return programIdFk;
    }

    public void setProgramIdFk(Program programIdFk) {
        this.programIdFk = programIdFk;
    }

    public RateBasedRuleGroup getRateBasedRuleGroupIdFk() {
        return rateBasedRuleGroupIdFk;
    }

    public void setRateBasedRuleGroupIdFk(RateBasedRuleGroup rateBasedRuleGroupIdFk) {
        this.rateBasedRuleGroupIdFk = rateBasedRuleGroupIdFk;
    }

    public Term getTermIdFk() {
        return termIdFk;
    }

    public void setTermIdFk(Term termIdFk) {
        this.termIdFk = termIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateBasedRuleId != null ? rateBasedRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateBasedRule)) {
            return false;
        }
        RateBasedRule other = (RateBasedRule) object;
        if ((this.rateBasedRuleId == null && other.rateBasedRuleId != null) || (this.rateBasedRuleId != null && !this.rateBasedRuleId.equals(other.rateBasedRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateBasedRule[ rateBasedRuleId=" + rateBasedRuleId + " ]";
    }
    
}
