/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */


public class ClaimGeneral implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public ClaimGeneral() {
    }

    public ClaimGeneral(Integer claimId, String rfPhone, String contactName, String contactPhone, String rfFax, String claimNumber, Integer odometer, String roNumber, String roDate, String rfName, String line1, String line2, String zip, String city, String state, BigDecimal laborRate, BigDecimal laborTax, BigDecimal partTax, Integer partWarrantyMonths, Integer partWarrantyMiles, Integer contractIdFk, String assignedTo) {
        this.claimId = claimId;
        this.rfPhone = rfPhone;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.rfFax = rfFax;
        this.claimNumber = claimNumber;
        this.odometer = odometer;
        this.roNumber = roNumber;
        this.roDate = roDate;
        this.rfName = rfName;
        this.line1 = line1;
        this.line2 = line2;
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.laborRate = laborRate;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
        this.contractIdFk = contractIdFk;
        this.assignedTo = assignedTo;
    }

    public ClaimGeneral(Integer claimId, String rfPhone, String contactName, String contactPhone, String rfFax, String claimNumber, Integer odometer, String roNumber, String roDate, String rfName, String line1, String line2, String zip, String city, String state, BigDecimal laborRate, BigDecimal laborTax, BigDecimal partTax, Integer partWarrantyMonths, Integer partWarrantyMiles) {
        this.claimId = claimId;
        this.rfPhone = rfPhone;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.rfFax = rfFax;
        this.claimNumber = claimNumber;
        this.odometer = odometer;
        this.roNumber = roNumber;
        this.roDate = roDate;
        this.rfName = rfName;
        this.line1 = line1;
        this.line2 = line2;
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.laborRate = laborRate;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
    }

    

    public ClaimGeneral(Integer claimId, String rfPhone, String contactName, String contactPhone, String rfFax, String claimNumber, Integer odometer, String roNumber, String roDate, String rfName, String line1, String line2, String zip, String city, String state, BigDecimal laborRate, BigDecimal laborTax, BigDecimal partTax, Integer partWarrantyMonths, Integer partWarrantyMiles, Integer contractIdFk) {
        this.claimId = claimId;
        this.rfPhone = rfPhone;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.rfFax = rfFax;
        this.claimNumber = claimNumber;
        this.odometer = odometer;
        this.roNumber = roNumber;
        this.roDate = roDate;
        this.rfName = rfName;
        this.line1 = line1;
        this.line2 = line2;
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.laborRate = laborRate;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
        this.contractIdFk = contractIdFk;
    }

    public ClaimGeneral(Integer claimId, String rfPhone, String contactName, String contactPhone, String rfFax, String claimNumber, Integer odometer, String roNumber, String roDate, String rfName, String line1, String line2, String zip, String city, String state, BigDecimal laborRate, BigDecimal laborTax, BigDecimal partTax, Integer partWarrantyMonths, Integer partWarrantyMiles, Integer contractIdFk, String assignedTo, String emailAddress, String taxId) {
        this.claimId = claimId;
        this.rfPhone = rfPhone;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.rfFax = rfFax;
        this.claimNumber = claimNumber;
        this.odometer = odometer;
        this.roNumber = roNumber;
        this.roDate = roDate;
        this.rfName = rfName;
        this.line1 = line1;
        this.line2 = line2;
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.laborRate = laborRate;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
        this.contractIdFk = contractIdFk;
        this.assignedTo = assignedTo;
        this.emailAddress = emailAddress;
        this.taxId = taxId;
    }
    
    public ClaimGeneral(ClaimGeneral cg) {
        this.claimId = cg.claimId;
        this.rfPhone = cg.rfPhone;
        this.contactName = cg.contactName;
        this.contactPhone = cg.contactPhone;
        this.rfFax = cg.rfFax;
        this.claimNumber = cg.claimNumber;
        this.odometer = cg.odometer;
        this.roNumber = cg.roNumber;
        this.roDate = cg.roDate;
        this.rfName = cg.rfName;
        this.line1 = cg.line1;
        this.line2 = cg.line2;
        this.zip = cg.zip;
        this.city = cg.city;
        this.state = cg.state;
        this.laborRate = cg.laborRate;
        this.laborTax = cg.laborTax;
        this.partTax = cg.partTax;
        this.partWarrantyMonths = cg.partWarrantyMonths;
        this.partWarrantyMiles = cg.partWarrantyMiles;
        this.contractIdFk = cg.contractIdFk;
        this.assignedTo = cg.assignedTo;
        this.emailAddress = cg.emailAddress;
        this.taxId = cg.taxId;
    }
    

    /**
     *
     */
    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "phoneNumber", table = "Address")
    String rfPhone;
    @Column(name = "name", table = "RepairFacilityContactPerson")
    String contactName;
    @Column(name = "phoneNumber", table = "RepairFacilityContactPerson")
    String contactPhone;
    @Column(name = "fax", table = "Address")
    String rfFax;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    @Column(name = "currentOdometer", table = "Claim")
    Integer odometer;
    @Column(name = "repairOrderNumber", table = "Claim")
    String roNumber;
    @Column(name = "updateLast", table = "Claim")
    String roDate;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    String rfName;
    @Column(name = "address1", table = "Address")
    String line1;
    @Column(name = "address2", table = "Address")
    String line2;
    @Column(name = "zipCode", table = "Address")
    String zip;
    @Column(name = "city", table = "Address")
    String city;
    @Column(name = "regionName", table = "Region")
    String state;
    @Column(name = "laborRate", table = "RepairFacility")
    BigDecimal laborRate;
    @Column(name = "laborTaxPct", table = "RepairFacility")
    BigDecimal laborTax;
    @Column(name = "partsTaxPct", table = "RepairFacility")
    BigDecimal partTax;
    @Column(name = "partWarrantyMonths", table = "RepairFacility")
    Integer partWarrantyMonths;
    @Column(name = "partWarrantyMiles", table = "RepairFacility")
    Integer partWarrantyMiles;
    @Column(name = "contractIdFk", table = "Claim")
    Integer contractIdFk;
     @Column(name = "assignedTo", table = "Claim")
    String assignedTo;
     @Column(name = "emailAddress", table = "Address")
    String emailAddress;
     @Column(name = "taxId", table = "AccountKeeper")
    String taxId;

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
     
     

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
     
     

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
     
     

    public Integer getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Integer contractIdFk) {
        this.contractIdFk = contractIdFk;
    }
    
    

    
    public Integer getClaimId() {
        return claimId;
    }

    
    public void setClaimId(Integer claimId) {    
        this.claimId = claimId;
    }

    public String getRfPhone() {
        return rfPhone;
    }

    public void setRfPhone(String rfPhone) {
        this.rfPhone = rfPhone;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getRfFax() {
        return rfFax;
    }

    public void setRfFax(String rfFax) {
        this.rfFax = rfFax;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public String getRoNumber() {
        return roNumber;
    }

    public void setRoNumber(String roNumber) {
        this.roNumber = roNumber;
    }

    public String getRoDate() {
        return roDate;
    }

    public void setRoDate(String roDate) {
        this.roDate = roDate;
    }

    public String getRfName() {
        return rfName;
    }

    public void setRfName(String rfName) {
        this.rfName = rfName;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public BigDecimal getLaborTax() {
        return laborTax;
    }

    public void setLaborTax(BigDecimal laborTax) {
        this.laborTax = laborTax;
    }

    public BigDecimal getPartTax() {
        return partTax;
    }

    public void setPartTax(BigDecimal patTax) {
        this.partTax = patTax;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }
    
    public void reset() {
     claimId=null;
     rfPhone="";
     contactName="";
     contactPhone="";
     rfFax="";
     claimNumber="";
     odometer=null;
     roNumber="";
     roDate="";
     rfName="";
     line1="";
     line2="";
     zip="";
     city="";
     state="";
     laborRate=null;
     laborTax=null;
     partTax=null;
     partWarrantyMonths=null;
     partWarrantyMiles=null;
     contractIdFk=null;
     assignedTo="";
     emailAddress="";
     taxId="";
    }
    
    public void rfreset() {
     rfPhone="";
     contactName="";
     contactPhone="";
     rfFax="";
     rfName="";
     line1="";
     line2="";
     zip="";
     city="";
     state="";
     laborRate=null;
     laborTax=null;
     partTax=null;
     partWarrantyMonths=null;
     partWarrantyMiles=null;
     contractIdFk=null;
     emailAddress="";
     taxId="";
    }
    

}
