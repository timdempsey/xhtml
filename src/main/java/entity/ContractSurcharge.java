/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractSurcharge")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractSurcharge.findAll", query = "SELECT c FROM ContractSurcharge c")
    , @NamedQuery(name = "ContractSurcharge.findByContractSurchargeId", query = "SELECT c FROM ContractSurcharge c WHERE c.contractSurchargeId = :contractSurchargeId")
    , @NamedQuery(name = "ContractSurcharge.findByAffectsCoverageCost", query = "SELECT c FROM ContractSurcharge c WHERE c.affectsCoverageCost = :affectsCoverageCost")
    , @NamedQuery(name = "ContractSurcharge.findByUpdateUserName", query = "SELECT c FROM ContractSurcharge c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractSurcharge.findByUpdateLast", query = "SELECT c FROM ContractSurcharge c WHERE c.updateLast = :updateLast")})
public class ContractSurcharge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractSurchargeId")
    private Integer contractSurchargeId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "affectsCoverageCost")
    private boolean affectsCoverageCost;
    @Size(max = 30)
    @Column(name = "update_user_name")
    private String updateUserName;
    @Column(name = "update_last")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ContractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "surchargeIdFk", referencedColumnName = "surchargeId")
    @ManyToOne
    private Surcharge surchargeIdFk;

    public ContractSurcharge() {
    }

    public ContractSurcharge(Integer contractSurchargeId) {
        this.contractSurchargeId = contractSurchargeId;
    }

    public ContractSurcharge(Integer contractSurchargeId, boolean affectsCoverageCost) {
        this.contractSurchargeId = contractSurchargeId;
        this.affectsCoverageCost = affectsCoverageCost;
    }

    public Integer getContractSurchargeId() {
        return contractSurchargeId;
    }

    public void setContractSurchargeId(Integer contractSurchargeId) {
        this.contractSurchargeId = contractSurchargeId;
    }

    public boolean getAffectsCoverageCost() {
        return affectsCoverageCost;
    }

    public void setAffectsCoverageCost(boolean affectsCoverageCost) {
        this.affectsCoverageCost = affectsCoverageCost;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Surcharge getSurchargeIdFk() {
        return surchargeIdFk;
    }

    public void setSurchargeIdFk(Surcharge surchargeIdFk) {
        this.surchargeIdFk = surchargeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractSurchargeId != null ? contractSurchargeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractSurcharge)) {
            return false;
        }
        ContractSurcharge other = (ContractSurcharge) object;
        if ((this.contractSurchargeId == null && other.contractSurchargeId != null) || (this.contractSurchargeId != null && !this.contractSurchargeId.equals(other.contractSurchargeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractSurcharge[ contractSurchargeId=" + contractSurchargeId + " ]";
    }
    
}
