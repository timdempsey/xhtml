/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DeductibleDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DeductibleDetail.findAll", query = "SELECT d FROM DeductibleDetail d")
    , @NamedQuery(name = "DeductibleDetail.findByDeductibleDetailId", query = "SELECT d FROM DeductibleDetail d WHERE d.deductibleDetailId = :deductibleDetailId")
    , @NamedQuery(name = "DeductibleDetail.findBySuggestedRetailPrice", query = "SELECT d FROM DeductibleDetail d WHERE d.suggestedRetailPrice = :suggestedRetailPrice")
    , @NamedQuery(name = "DeductibleDetail.findByEffectiveDate", query = "SELECT d FROM DeductibleDetail d WHERE d.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "DeductibleDetail.findByEnteredDate", query = "SELECT d FROM DeductibleDetail d WHERE d.enteredDate = :enteredDate")
    , @NamedQuery(name = "DeductibleDetail.findByDeletedInd", query = "SELECT d FROM DeductibleDetail d WHERE d.deletedInd = :deletedInd")
    , @NamedQuery(name = "DeductibleDetail.findByUpdateUserName", query = "SELECT d FROM DeductibleDetail d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DeductibleDetail.findByUpdateLlast", query = "SELECT d FROM DeductibleDetail d WHERE d.updateLlast = :updateLlast")})
public class DeductibleDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "deductibleDetailId")
    private Integer deductibleDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "suggestedRetailPrice")
    private BigDecimal suggestedRetailPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLlast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLlast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "deductibleIdFk", referencedColumnName = "deductibleId")
    @ManyToOne(optional = false)
    private Deductible deductibleIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public DeductibleDetail() {
    }

    public DeductibleDetail(Integer deductibleDetailId) {
        this.deductibleDetailId = deductibleDetailId;
    }

    public DeductibleDetail(Integer deductibleDetailId, Date effectiveDate, Date enteredDate, boolean deletedInd) {
        this.deductibleDetailId = deductibleDetailId;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.deletedInd = deletedInd;
    }

    public Integer getDeductibleDetailId() {
        return deductibleDetailId;
    }

    public void setDeductibleDetailId(Integer deductibleDetailId) {
        this.deductibleDetailId = deductibleDetailId;
    }

    public BigDecimal getSuggestedRetailPrice() {
        return suggestedRetailPrice;
    }

    public void setSuggestedRetailPrice(BigDecimal suggestedRetailPrice) {
        this.suggestedRetailPrice = suggestedRetailPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLlast() {
        return updateLlast;
    }

    public void setUpdateLlast(Date updateLlast) {
        this.updateLlast = updateLlast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public Deductible getDeductibleIdFk() {
        return deductibleIdFk;
    }

    public void setDeductibleIdFk(Deductible deductibleIdFk) {
        this.deductibleIdFk = deductibleIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deductibleDetailId != null ? deductibleDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DeductibleDetail)) {
            return false;
        }
        DeductibleDetail other = (DeductibleDetail) object;
        if ((this.deductibleDetailId == null && other.deductibleDetailId != null) || (this.deductibleDetailId != null && !this.deductibleDetailId.equals(other.deductibleDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DeductibleDetail[ deductibleDetailId=" + deductibleDetailId + " ]";
    }
    
}
