/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfUserMemberAndDealer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfUserMemberAndDealer.findAll", query = "SELECT c FROM CfUserMemberAndDealer c")
    , @NamedQuery(name = "CfUserMemberAndDealer.findByUserMemberAndDealerId", query = "SELECT c FROM CfUserMemberAndDealer c WHERE c.userMemberAndDealerId = :userMemberAndDealerId")})
public class CfUserMemberAndDealer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userMemberAndDealerId")
    private Integer userMemberAndDealerId;
    @JoinColumn(name = "DealerId", referencedColumnName = "dealerId")
    @ManyToOne(optional = false)
    private Dealer dealerId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public CfUserMemberAndDealer() {
    }

    public CfUserMemberAndDealer(Integer userMemberAndDealerId) {
        this.userMemberAndDealerId = userMemberAndDealerId;
    }

    public Integer getUserMemberAndDealerId() {
        return userMemberAndDealerId;
    }

    public void setUserMemberAndDealerId(Integer userMemberAndDealerId) {
        this.userMemberAndDealerId = userMemberAndDealerId;
    }

    public Dealer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Dealer dealerId) {
        this.dealerId = dealerId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userMemberAndDealerId != null ? userMemberAndDealerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfUserMemberAndDealer)) {
            return false;
        }
        CfUserMemberAndDealer other = (CfUserMemberAndDealer) object;
        if ((this.userMemberAndDealerId == null && other.userMemberAndDealerId != null) || (this.userMemberAndDealerId != null && !this.userMemberAndDealerId.equals(other.userMemberAndDealerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfUserMemberAndDealer[ userMemberAndDealerId=" + userMemberAndDealerId + " ]";
    }
    
}
