/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanExpireUsageType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanExpireUsageType.findAll", query = "SELECT d FROM DtPlanExpireUsageType d")
    , @NamedQuery(name = "DtPlanExpireUsageType.findByExpireUsageTypeId", query = "SELECT d FROM DtPlanExpireUsageType d WHERE d.expireUsageTypeId = :expireUsageTypeId")
    , @NamedQuery(name = "DtPlanExpireUsageType.findByDescription", query = "SELECT d FROM DtPlanExpireUsageType d WHERE d.description = :description")})
public class DtPlanExpireUsageType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "expireUsageTypeId")
    private Integer expireUsageTypeId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public DtPlanExpireUsageType() {
    }

    public DtPlanExpireUsageType(Integer expireUsageTypeId) {
        this.expireUsageTypeId = expireUsageTypeId;
    }

    public Integer getExpireUsageTypeId() {
        return expireUsageTypeId;
    }

    public void setExpireUsageTypeId(Integer expireUsageTypeId) {
        this.expireUsageTypeId = expireUsageTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (expireUsageTypeId != null ? expireUsageTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanExpireUsageType)) {
            return false;
        }
        DtPlanExpireUsageType other = (DtPlanExpireUsageType) object;
        if ((this.expireUsageTypeId == null && other.expireUsageTypeId != null) || (this.expireUsageTypeId != null && !this.expireUsageTypeId.equals(other.expireUsageTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanExpireUsageType[ expireUsageTypeId=" + expireUsageTypeId + " ]";
    }
    
}
