/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ContractNote {
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    public ContractNote() {
    }
    
    

    public ContractNote(Integer noteId, String claimNumber, String note, String enteredBy, Date enteredDate) {
        this.noteId = noteId;
        this.claimNumber = claimNumber;
        this.note = note;
        this.enteredBy = enteredBy;
        this.enteredDate = enteredDate;
    }
    
    
    
    @Id
    @Column(name = "noteId", table = "Note")
    Integer noteId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    @Column(name = "note", table = "Note")
    String note;
    String enteredBy;
    Date enteredDate;

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }
    
    
    
}
