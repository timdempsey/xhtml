/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.CfCriterionGroup;
import entity.Contracts;
import entity.Dealer;
import entity.DealerSRP;
import entity.Disbursement;
import entity.DisbursementDetail;
import entity.PlanTable;
import entity.Product;
import entity.Program;
import entity.VinDesc;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractRatingService")
@ApplicationScoped
public class ContractRatingService {

    /**
     * Creates a new instance of ContractRatingService
     */
    public ContractRatingService() {
        System.out.println("default constructor");
        cr = new ContractRating();
        dr = new DealerRating();
        ddList = new ArrayList<>();
        prodList = new ArrayList<>();
    }

    private static final Logger LOGGER = LogManager.getLogger(ContractRatingService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    @Inject
    private ContractPDF contractPDF;

    private ContractRating cr;
    private DealerRating dr;

    private List<DetailedDisbursements> ddList;
    private DetailedDisbursements ddment;
    private List<ContractRating> prodList;

    private DealerProduct selectedDP;
    private Integer selectedProdId;
    private List<DealerProduct> selectedDPs;

    private DealerPlan selectedPlan;
    private HashMap<String, String> spMap;
    private HashMap<String, Integer> ancTermMap;
    private HashMap<String, Integer> ppmTermMap;
    private HashMap<String, Integer> ppmUsageMap;
    private HashMap<String, Integer> vscTermMap;
    private HashMap<String, Integer> vscUsageMap;

    private boolean saveBtn;

    static final BigDecimal BIGZ = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

    public ContractPDF getContractPDF() {
        return contractPDF;
    }

    public void setContractPDF(ContractPDF contractPDF) {
        this.contractPDF = contractPDF;
    }

    public HashMap<String, Integer> getVscTermMap() {
        return vscTermMap;
    }

    public void setVscTermMap(HashMap<String, Integer> vscTermMap) {
        this.vscTermMap = vscTermMap;
    }

    public HashMap<String, Integer> getVscUsageMap() {
        return vscUsageMap;
    }

    public void setVscUsageMap(HashMap<String, Integer> vscUsageMap) {
        this.vscUsageMap = vscUsageMap;
    }

    public HashMap<String, Integer> getPpmUsageMap() {
        return ppmUsageMap;
    }

    public void setPpmUsageMap(HashMap<String, Integer> ppmUsageMap) {
        this.ppmUsageMap = ppmUsageMap;
    }

    public List<DealerProduct> getSelectedDPs() {
        return selectedDPs;
    }

    public void setSelectedDPs(List<DealerProduct> selectedDPs) {
        this.selectedDPs = selectedDPs;
    }

    public HashMap<String, Integer> getPpmTermMap() {
        return ppmTermMap;
    }

    public void setPpmTermMap(HashMap<String, Integer> ppmTermMap) {
        this.ppmTermMap = ppmTermMap;
    }

    public HashMap<String, Integer> getAncTermMap() {
        return ancTermMap;
    }

    public void setAncTermMap(HashMap<String, Integer> ancTermMap) {
        this.ancTermMap = ancTermMap;
    }

    public HashMap<String, String> getSpMap() {
        return spMap;
    }

    public void setSpMap(HashMap<String, String> spMap) {
        this.spMap = spMap;
    }

    public DealerPlan getSelectedPlan() {
        return selectedPlan;
    }

    public void setSelectedPlan(DealerPlan selectedPlan) {
        this.selectedPlan = selectedPlan;
    }

    public boolean isSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(boolean saveBtn) {
        this.saveBtn = saveBtn;
    }

    public Integer getSelectedProdId() {
        return selectedProdId;
    }

    public void setSelectedProdId(Integer selectedProdId) {
        this.selectedProdId = selectedProdId;
    }

    public DealerProduct getSelectedDP() {
        return selectedDP;
    }

    public void setSelectedDP(DealerProduct selectedDP) {
        this.selectedDP = selectedDP;
    }

    public List<ContractRating> getProdList() {
        return prodList;
    }

    public void setProdList(List<ContractRating> prodList) {
        this.prodList = prodList;
    }

    public List<DetailedDisbursements> getDdList() {
        return ddList;
    }

    public void setDdList(List<DetailedDisbursements> ddList) {
        this.ddList = ddList;
    }

    public DetailedDisbursements getDdment() {
        return ddment;
    }

    public void setDdment(DetailedDisbursements ddment) {
        this.ddment = ddment;
    }

    public ContractRating getCr() {
        return cr;
    }

    public void setCr(ContractRating cr) {
        this.cr = cr;
    }

    public DealerRating getDr() {
        return dr;
    }

    public void setDr(DealerRating dr) {
        this.dr = dr;
    }

    public void reset() {
        cr.reset();
        ddList = new ArrayList<>();
        prodList = new ArrayList<>();
        dr.reset();
        selectedDP = null;
        selectedPlan = null;
        saveBtn = true;
        spMap = new HashMap<>();
        ancTermMap = new HashMap<>();
        selectedDPs = new ArrayList<>();
        ppmTermMap = new HashMap<>();
        ppmUsageMap = new HashMap<>();
        vscTermMap = new HashMap<>();
        vscUsageMap = new HashMap<>();
    }

    public String getContractRating() {
        cr.reset();
        ddList = new ArrayList<>();
        System.out.println("in getContractRating");
        return "ContractRating";
    }

    public String getDealerRating() {
        if (dr != null) {
            dr.reset();
        }

        prodList = new ArrayList<>();
        return "DealerRating";
    }

    public void findCode() {
        try {
            LOGGER.info("in findCode..., ");

            LOGGER.info("vin=" + cr.getVinNo());
            LOGGER.info("dealerName=" + cr.getDealerName());
            LOGGER.info("plan=" + cr.getPlanName());

            Integer classCode = claimMisc.getClassCode(cr);
            System.out.println("classCode=" + classCode);
            cr.setVehicleClassCode(classCode);

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void findRates() {
        try {
            LOGGER.info("in findRates.., ");

            LOGGER.info("vin=" + cr.getVinNo());
            LOGGER.info("dealerName=" + cr.getDealerName());
            LOGGER.info("plan=" + cr.getPlanName());
            LOGGER.info("expTime=" + cr.getExpTime());
            LOGGER.info("ppmTime=" + cr.getPpmTime());
            LOGGER.info("ancMonths=" + cr.getAncMonths());
            LOGGER.info("getContractCoverageCost=" + cr.getContractCoverageCost());

            ddList = new ArrayList<>();    // to prevent from reusing this list while on same rating screen

            Product product = claimMisc.getProductByName(cr.getProductName());
            cr.setProductId(product.getProductId());

            Program program = claimMisc.getProgramById(product.getProgramIdFk().getProgramId());
            cr.setProgramName(program.getName());

            LOGGER.info("productId=" + cr.getProductId());
            LOGGER.info("programName=" + program.getName());

            PlanTable plan = claimMisc.getPlanByNameId(cr.getPlanName(), cr.getProductId());
            cr.setPlanId(plan.getPlanId());

            LOGGER.info("planId=" + cr.getPlanId());

            BigDecimal contractReserve = claimMisc.getContractReserve(cr);
            LOGGER.info("11111111111, contractReserve=" + contractReserve);
            if (contractReserve != null) {
                cr.setContractReserve(contractReserve);

                // adding reseve in the ddList
                ddment = new DetailedDisbursements();
                ddment.setDisbursementType("Reserve");
                ddment.setAccountHolder(cr.getDealerName());
                ddment.setDescription("Reserve");
                ddment.setDisbursementGroup("Reserve");
                ddment.setDisbursementAmount(contractReserve);
                ddList.add(ddment);

                //
                // now need to get other fees
                //
                LOGGER.info("other disbursements for dealer: " + cr.getDealerName());
                Integer disbursementCenterIdFk = null;
                Dealer dealer = claimMisc.getDealerByName(cr.getDealerName());

                //LOGGER.info("dealerGroupIdFk=" + dealer.getDealerGroupIdFk().getAccountKeeper().getAccountKeeperId());
                LOGGER.info("dealerId=" + dealer.getDealerId());
                cr.setDealerId(dealer.getDealerId());

                List<Disbursement> disbursementList;
                disbursementList = new ArrayList<>();
                boolean groupIsEmpty = true;
                if (dealer.getDealerGroupIdFk() != null) {
                    disbursementCenterIdFk = claimMisc.getDealerGroupById(dealer.getDealerGroupIdFk().getDealerGroupId()).getDisbursementCenterIdFk().getDisbursementCenterId();
                    disbursementList = claimMisc.getDisbursementByFkId(disbursementCenterIdFk);
                    if (!disbursementList.isEmpty()) {
                        groupIsEmpty = false;
                    }
                }
                if (groupIsEmpty) {
                    disbursementCenterIdFk = dealer.getDisbursementCenterIdFk().getDisbursementCenterId();
                    disbursementList = claimMisc.getDisbursementByFkId(disbursementCenterIdFk);
                }
                LOGGER.info("disbursementCenterIdFk=" + disbursementCenterIdFk);

                //disbursementList = claimMisc.getDisbursementByFkId(disbursementCenterIdFk);
                //List<Disbursement> disbursementList = claimMisc.getDisbursementByFkId(disbursementCenterIdFk, cr.getDealerId());
                //LOGGER.info("size of disbursementList=" + disbursementList.size());
                DisbursementDetail disbursementDetail;
                //List<DisbursementDetail> fees = new ArrayList<>();
                boolean ignoreDesc;
                BigDecimal contractCoverageCost = BIGZ;
                BigDecimal amount;
                for (Disbursement disbursement : disbursementList) {
                    ignoreDesc = false;
                    ddment = new DetailedDisbursements();

                    disbursementDetail = claimMisc.getDisbursementDetailByFkId(disbursement.getDisbursementId());
                    if (disbursementDetail == null) {
                        LOGGER.info("disbursementDetail is null");
                        continue;
                    }
                    LOGGER.info("criterionId=" + disbursementDetail.getCriterionGroupIdFk().getCriterionGroupId());
                    CfCriterionGroup cfCriterionGroup = claimMisc.getCfCriterionGroupById(disbursementDetail.getCriterionGroupIdFk().getCriterionGroupId());
                    if (claimMisc.isMatchedDisbursement(cr, cfCriterionGroup)) {
                        LOGGER.info("matched=" + disbursementDetail.getDisbursementMemo());
                        LOGGER.info("disbursementId=" + disbursement.getDisbursementId());
                        LOGGER.info("detailedDisbursementId=" + disbursementDetail.getDisbursementDetailId());
                        Integer dealerConditionIdFk = null;
                        if (disbursement.getDealerConditionIdFk() != null) {
                            dealerConditionIdFk = disbursement.getDealerConditionIdFk().getDealerId();
                        }
                        LOGGER.info("dealerId=" + cr.getDealerId());
                        LOGGER.info("dealerConditionIdFk=" + dealerConditionIdFk);
                        if (dealerConditionIdFk != null && dealerConditionIdFk.intValue() != cr.getDealerId().intValue()) {
                            LOGGER.info("not the dealer processed");
                            continue;
                        }
                        //fees.add(disbursementDetail);
                        // create dataTable here
                        LOGGER.info("detailId=" + disbursementDetail.getDisbursementDetailId());

                        // to prevent same Description/disbursementMemo
                        for (DetailedDisbursements dd : ddList) {
                            if (dd.getDescription().equalsIgnoreCase(disbursementDetail.getDisbursementMemo())) {
                                ignoreDesc = true;
                                break;
                            }
                        }
                        if (ignoreDesc) {
                            continue;
                        }

                        ddment.setDisbursementDetailId(disbursementDetail.getDisbursementDetailId());
                        LOGGER.info("type=" + claimMisc.getDtDisbursementTypeById(disbursementDetail.getDisbursementTypeInd()).getDescription());
                        ddment.setDisbursementType(claimMisc.getDtDisbursementTypeById(disbursementDetail.getDisbursementTypeInd()).getDescription());
                        if (disbursementDetail.getAccountKeeperIdFk() == null) {
                            ddment.setAccountHolder(cr.getDealerName());
                        } else {
                            LOGGER.info("holder=" + claimMisc.getAccountKeeperById(disbursementDetail.getAccountKeeperIdFk().getAccountKeeperId()).getAccountKeeperName());
                            ddment.setAccountHolder(claimMisc.getAccountKeeperById(disbursementDetail.getAccountKeeperIdFk().getAccountKeeperId()).getAccountKeeperName());
                        }
                        ddment.setDescription(disbursementDetail.getDisbursementMemo());
                        LOGGER.info("group=" + claimMisc.getDisbursementGroupById(disbursementDetail.getDisbursementGroupIdFk().getDisbursementGroupId()).getName());
                        ddment.setDisbursementGroup(claimMisc.getDisbursementGroupById(disbursementDetail.getDisbursementGroupIdFk().getDisbursementGroupId()).getName());
                        amount = disbursementDetail.getDisbursementAmount().setScale(2, RoundingMode.HALF_UP);
                        LOGGER.info("amount=" + amount);
                        ddment.setDisbursementAmount(amount);
                        contractCoverageCost = contractCoverageCost.add(amount).setScale(2, RoundingMode.HALF_UP);
                        LOGGER.info("contractCoverageCost=" + contractCoverageCost);
                        LOGGER.info("type=" + ddment.getDisbursementGroup());
                        ddList.add(ddment);
                        LOGGER.info("=== size of ddList ===" + ddList.size());
                    }
                }
                LOGGER.info("other fees=" + contractCoverageCost);
                LOGGER.info("cr.reserve=" + cr.getContractReserve());
                contractCoverageCost = contractCoverageCost.add(cr.getContractReserve());
                cr.setContractCoverageCost(contractCoverageCost.setScale(2, RoundingMode.HALF_UP));
                LOGGER.info("contractCoverageCost=" + contractCoverageCost);
                LOGGER.info("size of ddList=" + ddList.size());

                //
                // get SRP
                //
                LOGGER.info("getting SRP");
                List<DealerSRP> dsrpList = claimMisc.getDealerSRPByFkId(cr.getDealerId());
                BigDecimal contractRate = BIGZ;
                if (!dsrpList.isEmpty()) {
                    LOGGER.info("size of dsrpList=" + dsrpList.size());
                    for (DealerSRP dsrp : dsrpList) {
                        LOGGER.info("dealerSRPId=" + dsrp.getDealerSRPId());
                        LOGGER.info("criterionGroupIdFK=" + dsrp.getCriterionGroupIdFk());
                        CfCriterionGroup cfCriterionGroup = claimMisc.getCfCriterionGroupById(dsrp.getCriterionGroupIdFk().getCriterionGroupId());
                        if (claimMisc.isMatchedDisbursement(cr, cfCriterionGroup)) {
                            LOGGER.info("SRP matched=" + dsrp.getAmount());
                            int srpTypeInd = dsrp.getSRPTypeInd();
                            int calculationTypeInd = dsrp.getSRPCalculationTypeInd();
                            LOGGER.info("srpTypeInd=" + srpTypeInd);
                            LOGGER.info("calculationTypeInd=" + calculationTypeInd);
                            BigDecimal deltaValue = null;
                            switch (calculationTypeInd) {
                                case 1: // Percent Of SRP
                                    deltaValue = claimMisc.getDecimalPercent(cr.getContractSRP(), dsrp.getAmount()).setScale(2, RoundingMode.HALF_UP);
                                    break;
                                case 2: // Percent Of Coverage Cost
                                    deltaValue = claimMisc.getDecimalPercent(cr.getContractCoverageCost(), dsrp.getAmount()).setScale(2, RoundingMode.HALF_UP);
                                    LOGGER.info("case 2, ccc=" + cr.getContractCoverageCost());
                                    LOGGER.info("dsrp=" + dsrp.getAmount());
                                    break;
                                case 3: //Fixed Amount
                                    deltaValue = dsrp.getAmount().setScale(2, RoundingMode.HALF_UP);
                                    break;
                                default:
                                    LOGGER.info("ERROR to get deltaValue");
                            }
                            LOGGER.info("deltaValue = " + deltaValue);
                            cr.setContractSRP(deltaValue);

                            switch (srpTypeInd) {
                                case 1: // above coverage cost
                                    contractRate = cr.getContractCoverageCost().add(deltaValue);
                                    LOGGER.info("case 1, ccc=" + cr.getContractCoverageCost());
                                    break;
                                case 2:     // above srp
                                    contractRate = cr.getContractSRP().add(deltaValue);
                                    break;
                                case 3: // above zero
                                    contractRate = deltaValue;
                                    break;
                                default:
                                    LOGGER.info("ERROR: contractRate=" + contractRate);
                                    break;
                            }
                            LOGGER.info("contractRate=" + contractRate);
                            cr.setContractRate(contractRate);

                        }
                    }
                } else {
                    LOGGER.info("no DealerSRP");
                    cr.setContractRate(cr.getContractCoverageCost());
                }
            }
        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    // for tab in ContractGeneral
    public void findRates(ContractGeneral cg) {
        LOGGER.info("in findRates with contractNo=" + cg.getContractNo());
        Contracts contract = claimMisc.getContractsByNo(cg.getContractNo());
        cr.setMileage(cg.getOdometer());
        cr.setVinNo(cg.getVinFull());
        cr.setDealerName(cg.getDealer());
        cr.setPlanName(cg.getPlan());
        cr.setProductName(cg.getProduct());
        cr.setProgramName(cg.getProgram());
        String months = cg.getTermMonths();
        String miles = cg.getTermMiles();
        LOGGER.info("months=" + months);
        LOGGER.info("miles=" + miles);
        if (months != null && months.length() > 0) {
            cr.setAncMonths(Integer.parseInt(months));
            cr.setExpTime(Integer.parseInt(months));
            cr.setPpmTime(Integer.parseInt(months));
        }
        if (miles != null && miles.length() > 0) {
            miles = miles.replace(",", "");
            cr.setExpUsage(Integer.parseInt(miles));
            cr.setPpmUsage(Integer.parseInt(miles));
        }

        findRates();

        LOGGER.info("the end of findRates with ContractGeneral");

    }

    public void createProductList() {
        LOGGER.info("in createProductList");
        LOGGER.info("for dealer=" + dr.getDealerName());
        //List<Product> lprod = claimMisc.getProductByDealerName(dr.getDealerName());
        List<DealerProduct> dlpList = claimMisc.createDealerProdList(dr);
        for (DealerProduct dp : dlpList) {
            //LOGGER.info("prodId=" + dp.getProdId());
            //LOGGER.info("size of dplanList=" + dp.getDplanList().size());

            HashMap<String, DealerPlan> dpHM = dp.getDplanList();

            Map<String, DealerPlan> dpList = dp.getDplanList();
            for (DealerPlan dpp : dpList.values()) {
                //LOGGER.info("dpp id=" + dpp.getDplanId());
            }
        }
        dr.setDprodList(dlpList);

    }

    public void rateIt(DealerProduct dp) {
        cr.reset();

        cr.setVinNo(dr.getVinNo());
        cr.setDealerName(dr.getDealerName());
        cr.setMileage(dr.getMileage());
        cr.setProductName(dp.getProdName());

        spMap = new HashMap<>();
        ancTermMap = new HashMap<>();
        ppmTermMap = new HashMap<>();
        ppmUsageMap = new HashMap<>();
        vscTermMap = new HashMap<>();
        vscUsageMap = new HashMap<>();

        //List<DealerPlan> dpList = dp.getDplanList();
        selectedDP = new DealerProduct(dp);
        for (String planName : selectedDP.getDplanList().keySet()) {
            spMap.put(planName, planName);
        }

        Integer productId = dp.getProdId();
        LOGGER.info("in rateIt, prodId=" + productId);
        LOGGER.info("selectedDP, prodId=" + selectedDP.getProdId());
        LOGGER.info("selectedDP, dplanList=" + selectedDP.getDplanList().size());
        LOGGER.info("VIN=" + dr.getVinNo());
        LOGGER.info("Mileage=" + dr.getMileage());

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "500");
        //options.put("modal", true);

        //RequestContext.getCurrentInstance().openDialog("Facility", options, null);
        if (claimMisc.isVSC(claimMisc.getProductById(productId))) {
            LOGGER.info("open VSContractRating");
            RequestContext.getCurrentInstance().openDialog("VSContractRating", options, null);
        } else if (claimMisc.isANC(claimMisc.getProductById(productId))) {
            LOGGER.info("open ANCContractRating");
            RequestContext.getCurrentInstance().openDialog("ANCContractRating", options, null);
        } else if (claimMisc.isPPM(claimMisc.getProductById(productId))) {
            LOGGER.info("open PPMContractRating");
            RequestContext.getCurrentInstance().openDialog("PPMContractRating", options, null);
        } else if (claimMisc.isGAP(claimMisc.getProductById(productId))) {
            LOGGER.info("open GAPContractRating");
            RequestContext.getCurrentInstance().openDialog("GAPContractRating", options, null);
        }

    }

    public void generateContract(DealerProduct dp) {
        LOGGER.info("in generateContract, productName=" + dp.getProdName());
        try {
            utx.begin();

            // a couple of things will be done
            // 1. insert into Contracts table
            int ret;
            /*
            LOGGER.info("insert Ledger first");
            Query insQuery = em.createNativeQuery("INSERT INTO ledger ( amount,currentAmount,enteredDate,lowersAdminCompanyBalanceInd,payableInd,exemptInd,adminCompanyIdFk,CurrentAccountKeeperIdFk,originalAccountKeeperIdFk,enteredByIdFk) VALUES (17,17,'2017-05-31 20:56:13.387',0,0,1,1,19,19,708)");
            ret = insQuery.executeUpdate();
            Integer ledgerId = null;
            if( ret == 1 ) {
                ledgerId =  claimMisc.getLatestInsertedId("Ledger");
            }
            LOGGER.info("ledgerId=" + ledgerId);
             */

            LOGGER.info("inserting new Contracts...");
            Dealer dealer = claimMisc.getDealerByName(dr.getDealerName());
            Integer dealerGroupId = null;
            if (dealer.getDealerGroupIdFk() != null) {
                dealerGroupId = dealer.getDealerGroupIdFk().getDealerGroupId();
            }

            Query query = em.createNativeQuery("insert into Contracts ( contractStatusIdFk, effectiveDate, contractPurchasePrice, productIdFk, planIdFk, classCode, odometer, claimStartDate, claimStartUsage, customerIdFk, vinDescIdFk, vinFull, dealerIdFk, dealerGroupIdFk, termIdFk, limitIdFk, termExpireTime, termExpireUsage, termLowerTime, termUpperTime, termMonth, expirationDate, expirationUsage, soldByUserIdFk ) "
                    + "values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24)");

            Date currDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            Date expDate;
            if (dp.getAncMonthsSelected() != null) {
                expDate = claimMisc.addYear(currDate, dp.getAncMonthsSelected() / 12);
            } else {
                expDate = claimMisc.addYear(currDate, dp.getExpTimeSelected() / 12);
            }
            LOGGER.info("expDate=" + expDate);
            Integer expUsage = null;
            if (claimMisc.isVSC(claimMisc.getProductById(dp.getProdId())) && dp.getExpUsageSelected() != null) {
                expUsage = dr.getMileage() + dp.getExpUsageSelected();
            }

            Integer soldByUserIdFk = 1; // later

            query.setParameter(1, 7);
            query.setParameter(2, currDate);
            query.setParameter(3, dp.getRateAmount());
            query.setParameter(4, dp.getProdId());
            query.setParameter(5, cr.getPlanId());
            query.setParameter(6, cr.getVehicleClassCode()); // later
            query.setParameter(7, cr.getMileage());
            query.setParameter(8, claimMisc.getCurrentDate());
            query.setParameter(9, cr.getMileage());
            query.setParameter(10, 35271.); // need to be replaced
            query.setParameter(11, claimMisc.getVinDescByVinNo(dr.getVinNo()).getVinDescId());
            query.setParameter(12, cr.getVinNo());
            query.setParameter(13, dealer.getDealerId());
            query.setParameter(14, dealerGroupId);
            query.setParameter(15, dp.getTermIdSelectded());
            query.setParameter(16, dp.getLimitIdSelected());
            query.setParameter(17, dp.getExpTimeSelected());
            query.setParameter(18, dp.getExpUsageSelected());
            query.setParameter(19, dp.getLowerTimeSelected());
            query.setParameter(20, dp.getUpperTimeSelected());
            query.setParameter(21, dp.getAncMonthsSelected());
            query.setParameter(22, expDate);
            query.setParameter(23, expUsage);
            query.setParameter(24, soldByUserIdFk);

            ret = query.executeUpdate();

            LOGGER.info("ret=" + ret);
            Integer contractId;
            Contracts contract = null;
            if (ret == 1) {
                contractId = claimMisc.getLatestInsertedId("Contracts");
                LOGGER.info("contractId=" + contractId);

                // do update the contractNo
                contract = claimMisc.getContractsById(contractId);
                contract.setContractNo("DOWC" + contractId);
                em.merge(contract);

                // now insert to Ledger 
                Integer ledgerTypeInd = 1; // why, later
                Integer exemptInd = 0;  // later, meaning?
                AccountKeeper enteredByFkId = claimMisc.getAccountKeeperById(claimMisc.getUserMemberByName(claimMisc.getCurrentUser()).getUserMemberId());

                Query insQuery = em.createNativeQuery("insert into Ledger ("
                        + "amount, "
                        + "currentAmount, "
                        + "enteredDate, "
                        + "ledgerDate, "
                        + "ledgerTypeInd, "
                        + "relatedLedgerTypeInd, "
                        + "setLedgerDate, "
                        + "exemptInd, "
                        + "adminCompanyIdFk, "
                        + "CurrentAccountKeeperIdFk, "
                        + "originalAccountKeeperIdFk, "
                        + "lowersAdminCompanyBalanceInd, "
                        + "relatedContractIdFk, "
                        + "enteredByIdFk  ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14 )");
                
                insQuery.setParameter(1, contract.getContractPurchasePrice());
                insQuery.setParameter(2, contract.getContractPurchasePrice());
                insQuery.setParameter(3, claimMisc.getCurrentDate());
                insQuery.setParameter(4, claimMisc.getCurrentDate());
                insQuery.setParameter(5, ledgerTypeInd);
                insQuery.setParameter(6, ledgerTypeInd);
                insQuery.setParameter(7, claimMisc.getCurrentDate());
                insQuery.setParameter(8, exemptInd);
                insQuery.setParameter(9, 1);
                insQuery.setParameter(10, dealer.getDealerId());
                insQuery.setParameter(11, dealer.getDealerId());
                insQuery.setParameter(12, 0);
                insQuery.setParameter(13, contract.getContractId());
                insQuery.setParameter(14, enteredByFkId.getAccountKeeperId());

                insQuery.executeUpdate();

                Integer ledgerId = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('Ledger')").getSingleResult()).intValueExact();
                LOGGER.info("transactionId created=" + ledgerId);
                // end

                utx.commit();

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "new contractNo is created: ", contract.getContractNo());
                FacesContext.getCurrentInstance().addMessage(null, msg);

                int idx = 0;
                for (DealerProduct tdp : dr.getDprodList()) {

                    if (tdp.getProdId().intValue() == dp.getProdId().intValue()) {
                        LOGGER.info("contractNo=" + contract.getContractNo());
                        dr.getDprodList().get(idx).setContractNo(contract.getContractNo());
                        break;
                    }
                    ++idx;
                }

                // 2. generate a pdf file for this contract
                String destfile = "C:\\WebSite\\pdfContracts\\" + contract.getContractId() + ".pdf";
                LOGGER.info("destfile=" + destfile);
                contractPDF.setBuyerName("JP");
                contractPDF.setContractNo(contract.getContractNo());
                contractPDF.setContract_effectiveDate(claimMisc.getJavaDate(contract.getEffectiveDate()));
                contractPDF.setContract_plan(cr.getPlanName());
                contractPDF.setContract_purchasePrice(contract.getContractPurchasePrice().toEngineeringString());
                contractPDF.setDealer_name(cr.getDealerName());
                contractPDF.setDestfile(destfile);
                String termMonths = "";
                if (claimMisc.isANC(contract)) {
                    termMonths = cr.getAncMonths().toString();
                } else if (claimMisc.isPPM(contract)) {
                    termMonths = cr.getPpmTime().toString();
                } else if (claimMisc.isVSC(contract)) {
                    termMonths = cr.getExpTime().toString();
                } else {
                    LOGGER.info("something is wrong");
                }
                contractPDF.setTerm_months(termMonths);
                contractPDF.setVehicle_odometer(cr.getMileage().toString());
                contractPDF.setVin(contract.getVinFull());
                VinDesc vindesc = claimMisc.getVinDescByContract(contract.getContractNo());
                contractPDF.setVehicle_make(vindesc.getMake());
                contractPDF.setVehicle_model(vindesc.getModel());
                contractPDF.setVehicle_year(vindesc.getVehicleYear().toString());
                contractPDF.setVehicle_series(vindesc.getSeries());
                contractPDF.fillPDF();
                LOGGER.info("generate content");
                contractPDF.createStreamedContent();
            } else {
                LOGGER.info("ret=" + ret);
            }

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void ancTermChange() {
        LOGGER.info("ancMonths=" + cr.getAncMonths());
        String ancMonths = cr.getAncMonths().toString();
        cr.setContractRate(BIGZ);

        if (ancMonths == null || ancMonths.equals("")) {
            LOGGER.info("no selection");
        } else {
            findRates();
        }
    }

    public void ppmTermChange() {
        LOGGER.info("ppmTime=" + cr.getPpmTime());
        String ppmTime = cr.getPpmTime().toString();
        cr.setContractRate(BIGZ);

        if (ppmTime == null || ppmTime.equals("")) {
            LOGGER.info("no selection");
            ppmTermMap = new HashMap<>();
            ppmUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setPpmTime(null);
            cr.setPpmUsage(null);
        } else {
            /*
            int i=0;
            for( Integer value : ppmTermMap.values()) {
                if( value != cr.getPpmTime().intValue() ) {
                    ++i;
                }
            }
             */

            //findRates();
        }
    }

    public void vscTermChange() {
        LOGGER.info("expTime=" + cr.getExpTime());
        String expTime = cr.getExpTime().toString();
        cr.setContractRate(BIGZ);

        if (expTime == null || expTime.equals("")) {
            LOGGER.info("no selection");
            vscTermMap = new HashMap<>();
            vscUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setExpTime(null);
            cr.setExpUsage(null);
        } else {

        }
    }

    public void ppmUsageChange() {
        LOGGER.info("ppmUsage=" + cr.getPpmUsage());
        String ppmUsage = cr.getPpmUsage().toString();
        cr.setContractRate(BIGZ);

        if (ppmUsage == null || ppmUsage.equals("")) {
            LOGGER.info("no selection");
            ppmUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setPpmUsage(null);
        } else {

            findRates();
        }
    }

    public void vscUsageChange() {
        LOGGER.info("expUsage=" + cr.getExpUsage());
        String expUsage = cr.getExpUsage().toString();
        cr.setContractRate(BIGZ);

        if (expUsage == null || expUsage.equals("")) {
            LOGGER.info("no selection");
            vscUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setExpUsage(null);
        } else {

            findRates();
        }
    }

    public void cancelANCrating() {
        LOGGER.info("in cancelANCrating");
        cr.reset();
        RequestContext.getCurrentInstance().closeDialog("ANCContractRating");

    }

    public void saveANC() {
        LOGGER.info("in saveANC");

        RequestContext.getCurrentInstance().closeDialog("ANCContractRating");
    }

    public void cancelPPMrating() {
        LOGGER.info("in cancelPPMrating");
        cr.reset();
        RequestContext.getCurrentInstance().closeDialog("PPMContractRating");

    }

    public void savePPM() {
        LOGGER.info("in savePPM");

        RequestContext.getCurrentInstance().closeDialog("PPMContractRating");
    }

    public void cancelVSCrating() {
        LOGGER.info("in cancelVSCrating");
        cr.reset();
        RequestContext.getCurrentInstance().closeDialog("VSContractRating");

    }

    public void saveVSC() {
        LOGGER.info("in saveVSC");

        RequestContext.getCurrentInstance().closeDialog("VSContractRating");
    }

    public void rateItClosed(SelectEvent event) {
        LOGGER.info("in rateItClosed, prodId=" + cr.getProductId());

        if (cr.getProductId() != null) {    // closed from save

            int idx = 0;
            DealerProduct tmpdp = null;
            for (DealerProduct dp : dr.getDprodList()) {

                if (dp.getProdId().intValue() == cr.getProductId().intValue()) {
                    LOGGER.info("set rate=" + cr.getContractRate());
                    tmpdp = dr.getDprodList().get(idx);
                    //dr.getDprodList().get(idx).setRateAmount(cr.getContractRate().setScale(2, RoundingMode.HALF_UP));
                    break;
                }
                ++idx;
            }
            if (tmpdp != null) {
                tmpdp.setRateAmount(cr.getContractRate().setScale(2, RoundingMode.HALF_UP));
                tmpdp.setPlanIdSelcted(cr.getPlanId());
                Integer termId = claimMisc.getTermId(cr);
                Integer limitId = claimMisc.getLimitId(cr);
                tmpdp.setTermIdSelectded(termId);
                tmpdp.setLimitIdSelected(limitId);
                tmpdp.setAncMonthsSelected(cr.getAncMonths());
                if (claimMisc.isPPM(claimMisc.getProductById(cr.getProductId()))) {
                    tmpdp.setExpTimeSelected(cr.getPpmTime());
                    tmpdp.setExpUsageSelected(cr.getPpmUsage());
                } else {
                    tmpdp.setExpTimeSelected(cr.getExpTime());
                    tmpdp.setExpUsageSelected(cr.getExpUsage());
                }
                tmpdp.setLowerTimeSelected(cr.getLowerTime());
                tmpdp.setUpperTimeSelected(cr.getUpperTime());

            }

        }
        //cr.reset();     // to prevent from appear for the next product
    }

    public void ancPlanChange() {
        LOGGER.info("in createSelectedPlan...");

        String planName = cr.getPlanName();
        LOGGER.info("planName=" + planName);

        ancTermMap = new HashMap<>();

        if (planName != null && !planName.equals("")) {
            LOGGER.info("planName=" + planName);
            cr.setContractRate(BIGZ);
            cr.setAncMonths(null);
            selectedPlan = selectedDP.getDplanList().get(planName);

            for (Integer term : selectedPlan.getTermIdList()) {
                Integer ancMonths = claimMisc.getTermDetailByFk(term).get(0).getAncMonths();
                LOGGER.info("ancMonths=" + ancMonths);
                ancTermMap.put(ancMonths + " Months", ancMonths);
            }
        } else {
            LOGGER.info("no selection");
            ancTermMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setPlanName("");
            cr.setAncMonths(null);
        }
    }

    public void ppmPlanChange() {
        LOGGER.info("in ppmPlanChange...");

        String planName = cr.getPlanName();
        LOGGER.info("planName=" + planName);

        ppmTermMap = new HashMap<>();
        ppmUsageMap = new HashMap<>();

        if (planName != null && !planName.equals("")) {
            LOGGER.info("planName=" + planName);
            cr.setContractRate(BIGZ);
            cr.setPpmTime(null);
            cr.setPpmUsage(null);
            selectedPlan = selectedDP.getDplanList().get(planName);

            for (Integer term : selectedPlan.getTermIdList()) {
                Integer ppmTime = claimMisc.getTermDetailByFk(term).get(0).getPpmTime();
                Integer ppmUsage = claimMisc.getTermDetailByFk(term).get(0).getPpmUsage();
                LOGGER.info("ppmTime=" + ppmTime);
                LOGGER.info("ppmUsage=" + ppmUsage);
                ppmTermMap.put(ppmTime + " Months", ppmTime);
                ppmUsageMap.put(ppmUsage + " Miles", ppmUsage);
            }
        } else {
            LOGGER.info("no selection");
            ppmTermMap = new HashMap<>();
            ppmUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setPlanName("");
            cr.setPpmTime(null);
            cr.setPpmUsage(null);
        }
    }

    public void vscPlanChange() {
        LOGGER.info("in vscPlanChange...");

        String planName = cr.getPlanName();
        LOGGER.info("planName=" + planName);

        vscTermMap = new HashMap<>();
        vscUsageMap = new HashMap<>();

        if (planName != null && !planName.equals("")) {
            LOGGER.info("planName=" + planName);
            cr.setContractRate(BIGZ);
            cr.setExpTime(null);
            cr.setExpUsage(null);
            selectedPlan = selectedDP.getDplanList().get(planName);

            for (Integer term : selectedPlan.getTermIdList()) {
                Integer expTime = claimMisc.getTermDetailByFk(term).get(0).getVscExpireTime();
                Integer expUsage = claimMisc.getTermDetailByFk(term).get(0).getVscExpireUsage();
                LOGGER.info("expTime=" + expTime);
                LOGGER.info("expUsage=" + expUsage);
                vscTermMap.put(expTime + " Months", expTime);
                vscUsageMap.put(expUsage + " Miles", expUsage);
            }
        } else {
            LOGGER.info("no selection");
            vscTermMap = new HashMap<>();
            vscUsageMap = new HashMap<>();
            cr.setContractRate(BIGZ);
            cr.setPlanName("");
            cr.setExpTime(null);
            cr.setExpUsage(null);
        }
    }

    public void ancRowSelect(SelectEvent event) {
        LOGGER.info("in ancRowSelect");
        //selectedDP = (DealerProduct) event.getObject();
    }

    public void ancRowUnselect(UnselectEvent event) {
        LOGGER.info("in ancRowUnselect");
    }

    public boolean isCurrDP(DealerProduct dp) {
        LOGGER.info("dp.prodId=" + dp.getProdId());
        LOGGER.info("selectedDP.prodId=" + selectedDP.getProdId());
        if (dp.getProdId().intValue() == selectedDP.getProdId().intValue() && dp.getRateAmount() != null) {
            return true;
        }

        return false;
    }

    public void onSelectCheckbox(SelectEvent event) {
        LOGGER.info("in onSelectCheckbox");
        selectedDP = (DealerProduct) event.getObject();
        LOGGER.info("checkbox, productId=" + selectedDP.getProdId());
        LOGGER.info("productName=" + selectedDP.getProdName());
        BigDecimal bd = dr.getTotalAmount();
        if (bd == null) {
            bd = new BigDecimal(0);
        }
        if( selectedDP.getRateAmount() != null ) {
        bd = bd.add(selectedDP.getRateAmount());
        }
        LOGGER.info("bd=" + bd);
        dr.setTotalAmount(bd);

        for (DealerProduct dp : selectedDPs) {
            LOGGER.info("onSelect in DPs=" + dp.getProdName());
        }
    }

    public void onUnselectCheckbox(UnselectEvent event) {
        LOGGER.info("in onUnselectCheckbox");
        selectedDP = (DealerProduct) event.getObject();
        //selectedDPs.remove(selectedDP);
        LOGGER.info("productName=" + selectedDP.getProdName());
        BigDecimal bd = dr.getTotalAmount().setScale(2, RoundingMode.HALF_UP);
        LOGGER.info("totalAmount=" + bd);
        LOGGER.info("rateAmount=" + selectedDP.getRateAmount());
        bd = bd.subtract(selectedDP.getRateAmount());
        LOGGER.info("bd=" + bd);
        dr.setTotalAmount(bd);

        for (DealerProduct dp : selectedDPs) {
            LOGGER.info("onUnselect in DPs=" + dp.getProdName());
        }
    }

    private StreamedContent dfile;

    public StreamedContent getDfile() {
        LOGGER.info("getDfile");
        return dfile;
    }

    public void setDfile(StreamedContent dfile) {
        this.dfile = dfile;
    }

    public StreamedContent openPDF(DealerProduct dp) {

        try {
            LOGGER.info("in openPDF, contractNo=" + dp.getContractNo());

            Contracts contract = claimMisc.getContractsByNo(dp.getContractNo());
            String fileName = "C:\\WebSite\\pdfContracts\\" + contract.getContractId() + ".pdf";
            LOGGER.info("fileName=" + fileName);
            File tempFile = new File(fileName);

            //InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream(fileName);
            //InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream("/resources/search_rf_01.JPG");
            dfile = new DefaultStreamedContent(new FileInputStream(tempFile), "application/pdf", fileName);
            //dfile = new DefaultStreamedContent(stream, "image/jpg", "search_rf_01.JPG");

            return dfile;
            //return new DefaultStreamedContent(stream, "application/pdf", "fileName");
        } catch (Exception e) {
            LOGGER.info("File not found");
            return null;
        }

    }

}
