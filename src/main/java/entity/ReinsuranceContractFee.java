/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceContractFee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceContractFee.findAll", query = "SELECT r FROM ReinsuranceContractFee r")
    , @NamedQuery(name = "ReinsuranceContractFee.findByReinsuranceContractFeeId", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.reinsuranceContractFeeId = :reinsuranceContractFeeId")
    , @NamedQuery(name = "ReinsuranceContractFee.findByDescription", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.description = :description")
    , @NamedQuery(name = "ReinsuranceContractFee.findByDateEffective", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.dateEffective = :dateEffective")
    , @NamedQuery(name = "ReinsuranceContractFee.findByDateExpires", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.dateExpires = :dateExpires")
    , @NamedQuery(name = "ReinsuranceContractFee.findByAmount", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.amount = :amount")
    , @NamedQuery(name = "ReinsuranceContractFee.findByUpdateUserName", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceContractFee.findByUpdateLast", query = "SELECT r FROM ReinsuranceContractFee r WHERE r.updateLast = :updateLast")})
public class ReinsuranceContractFee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceContractFeeId")
    private Integer reinsuranceContractFeeId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Column(name = "dateEffective")
    @Temporal(TemporalType.DATE)
    private Date dateEffective;
    @Column(name = "dateExpires")
    @Temporal(TemporalType.DATE)
    private Date dateExpires;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "applicableProductIdFk", referencedColumnName = "rateBasedRuleGroupId")
    @ManyToOne
    private RateBasedRuleGroup applicableProductIdFk;
    @JoinColumn(name = "reinsuranceVersionCodeIdFk", referencedColumnName = "reinsuranceVersionCodeId")
    @ManyToOne
    private ReinsuranceVersionCode reinsuranceVersionCodeIdFk;

    public ReinsuranceContractFee() {
    }

    public ReinsuranceContractFee(Integer reinsuranceContractFeeId) {
        this.reinsuranceContractFeeId = reinsuranceContractFeeId;
    }

    public Integer getReinsuranceContractFeeId() {
        return reinsuranceContractFeeId;
    }

    public void setReinsuranceContractFeeId(Integer reinsuranceContractFeeId) {
        this.reinsuranceContractFeeId = reinsuranceContractFeeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public RateBasedRuleGroup getApplicableProductIdFk() {
        return applicableProductIdFk;
    }

    public void setApplicableProductIdFk(RateBasedRuleGroup applicableProductIdFk) {
        this.applicableProductIdFk = applicableProductIdFk;
    }

    public ReinsuranceVersionCode getReinsuranceVersionCodeIdFk() {
        return reinsuranceVersionCodeIdFk;
    }

    public void setReinsuranceVersionCodeIdFk(ReinsuranceVersionCode reinsuranceVersionCodeIdFk) {
        this.reinsuranceVersionCodeIdFk = reinsuranceVersionCodeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceContractFeeId != null ? reinsuranceContractFeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceContractFee)) {
            return false;
        }
        ReinsuranceContractFee other = (ReinsuranceContractFee) object;
        if ((this.reinsuranceContractFeeId == null && other.reinsuranceContractFeeId != null) || (this.reinsuranceContractFeeId != null && !this.reinsuranceContractFeeId.equals(other.reinsuranceContractFeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceContractFee[ reinsuranceContractFeeId=" + reinsuranceContractFeeId + " ]";
    }
    
}
