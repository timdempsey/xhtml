/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RepairFacility")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepairFacility.findAll", query = "SELECT r FROM RepairFacility r")
    , @NamedQuery(name = "RepairFacility.findByRepairFacilityId", query = "SELECT r FROM RepairFacility r WHERE r.repairFacilityId = :repairFacilityId")
    , @NamedQuery(name = "RepairFacility.findByPartWarrantyMonths", query = "SELECT r FROM RepairFacility r WHERE r.partWarrantyMonths = :partWarrantyMonths")
    , @NamedQuery(name = "RepairFacility.findByPartWarrantyMiles", query = "SELECT r FROM RepairFacility r WHERE r.partWarrantyMiles = :partWarrantyMiles")
    , @NamedQuery(name = "RepairFacility.findByLaborTaxPct", query = "SELECT r FROM RepairFacility r WHERE r.laborTaxPct = :laborTaxPct")
    , @NamedQuery(name = "RepairFacility.findByPartsTaxPct", query = "SELECT r FROM RepairFacility r WHERE r.partsTaxPct = :partsTaxPct")
    , @NamedQuery(name = "RepairFacility.findByLaborRate", query = "SELECT r FROM RepairFacility r WHERE r.laborRate = :laborRate")
    , @NamedQuery(name = "RepairFacility.findByRepairFacilityStatus", query = "SELECT r FROM RepairFacility r WHERE r.repairFacilityStatus = :repairFacilityStatus")
    , @NamedQuery(name = "RepairFacility.findByUpdateUserName", query = "SELECT r FROM RepairFacility r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RepairFacility.findByUpdateLast", query = "SELECT r FROM RepairFacility r WHERE r.updateLast = :updateLast")})
public class RepairFacility implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "repairFacilityId")
    private Integer repairFacilityId;
    @Column(name = "partWarrantyMonths")
    private Integer partWarrantyMonths;
    @Column(name = "partWarrantyMiles")
    private Integer partWarrantyMiles;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "laborTaxPct")
    private BigDecimal laborTaxPct;
    @Column(name = "partsTaxPct")
    private BigDecimal partsTaxPct;
    @Column(name = "laborRate")
    private BigDecimal laborRate;
    @Size(max = 10)
    @Column(name = "repairFacilityStatus")
    private String repairFacilityStatus;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "repairFacilityId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repairFacilityId")
    private Collection<RepairFacilityToDealerGroupRel> repairFacilityToDealerGroupRelCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repairFacilityId")
    private Collection<RepairFacilityToDealerRel> repairFacilityToDealerRelCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repairFacilityIdFk")
    private Collection<RepairFacilityContactPerson> repairFacilityContactPersonCollection;
    @OneToMany(mappedBy = "repairFacilityIdFk")
    private Collection<Claim> claimCollection;

    public RepairFacility() {
    }

    public RepairFacility(Integer repairFacilityId) {
        this.repairFacilityId = repairFacilityId;
    }

    public Integer getRepairFacilityId() {
        return repairFacilityId;
    }

    public void setRepairFacilityId(Integer repairFacilityId) {
        this.repairFacilityId = repairFacilityId;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }

    public BigDecimal getLaborTaxPct() {
        return laborTaxPct;
    }

    public void setLaborTaxPct(BigDecimal laborTaxPct) {
        this.laborTaxPct = laborTaxPct;
    }

    public BigDecimal getPartsTaxPct() {
        return partsTaxPct;
    }

    public void setPartsTaxPct(BigDecimal partsTaxPct) {
        this.partsTaxPct = partsTaxPct;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public String getRepairFacilityStatus() {
        return repairFacilityStatus;
    }

    public void setRepairFacilityStatus(String repairFacilityStatus) {
        this.repairFacilityStatus = repairFacilityStatus;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    @XmlTransient
    public Collection<RepairFacilityToDealerGroupRel> getRepairFacilityToDealerGroupRelCollection() {
        return repairFacilityToDealerGroupRelCollection;
    }

    public void setRepairFacilityToDealerGroupRelCollection(Collection<RepairFacilityToDealerGroupRel> repairFacilityToDealerGroupRelCollection) {
        this.repairFacilityToDealerGroupRelCollection = repairFacilityToDealerGroupRelCollection;
    }

    @XmlTransient
    public Collection<RepairFacilityToDealerRel> getRepairFacilityToDealerRelCollection() {
        return repairFacilityToDealerRelCollection;
    }

    public void setRepairFacilityToDealerRelCollection(Collection<RepairFacilityToDealerRel> repairFacilityToDealerRelCollection) {
        this.repairFacilityToDealerRelCollection = repairFacilityToDealerRelCollection;
    }

    @XmlTransient
    public Collection<RepairFacilityContactPerson> getRepairFacilityContactPersonCollection() {
        return repairFacilityContactPersonCollection;
    }

    public void setRepairFacilityContactPersonCollection(Collection<RepairFacilityContactPerson> repairFacilityContactPersonCollection) {
        this.repairFacilityContactPersonCollection = repairFacilityContactPersonCollection;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repairFacilityId != null ? repairFacilityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepairFacility)) {
            return false;
        }
        RepairFacility other = (RepairFacility) object;
        if ((this.repairFacilityId == null && other.repairFacilityId != null) || (this.repairFacilityId != null && !this.repairFacilityId.equals(other.repairFacilityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RepairFacility[ repairFacilityId=" + repairFacilityId + " ]";
    }
    
}
