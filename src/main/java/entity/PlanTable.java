/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PlanTable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanTable.findAll", query = "SELECT p FROM PlanTable p")
    , @NamedQuery(name = "PlanTable.findByPlanId", query = "SELECT p FROM PlanTable p WHERE p.planId = :planId")
    , @NamedQuery(name = "PlanTable.findByPlanName", query = "SELECT p FROM PlanTable p WHERE p.planName = :planName")
    , @NamedQuery(name = "PlanTable.findByPlanCode", query = "SELECT p FROM PlanTable p WHERE p.planCode = :planCode")
    , @NamedQuery(name = "PlanTable.findByPlanOrder", query = "SELECT p FROM PlanTable p WHERE p.planOrder = :planOrder")
    , @NamedQuery(name = "PlanTable.findByUpdateUserName", query = "SELECT p FROM PlanTable p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PlanTable.findByUpdateLast", query = "SELECT p FROM PlanTable p WHERE p.updateLast = :updateLast")})
public class PlanTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "planId")
    private Integer planId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "planName")
    private String planName;
    @Size(max = 60)
    @Column(name = "planCode")
    private String planCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "planOrder")
    private BigDecimal planOrder;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<RateBasedRule> rateBasedRuleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planIdFk")
    private Collection<RateWizardUpload> rateWizardUploadCollection;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<Term> termCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planIdFk")
    private Collection<WarrantyRule> warrantyRuleCollection;
    @OneToMany(mappedBy = "planIdFk")
    private Collection<Rate> rateCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planIdFk")
    private Collection<PPMPlanService> pPMPlanServiceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planIdFk")
    private Collection<Limit> limitCollection;

    public PlanTable() {
    }

    public PlanTable(Integer planId) {
        this.planId = planId;
    }

    public PlanTable(Integer planId, String planName) {
        this.planId = planId;
        this.planName = planName;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public BigDecimal getPlanOrder() {
        return planOrder;
    }

    public void setPlanOrder(BigDecimal planOrder) {
        this.planOrder = planOrder;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<RateBasedRule> getRateBasedRuleCollection() {
        return rateBasedRuleCollection;
    }

    public void setRateBasedRuleCollection(Collection<RateBasedRule> rateBasedRuleCollection) {
        this.rateBasedRuleCollection = rateBasedRuleCollection;
    }

    @XmlTransient
    public Collection<RateWizardUpload> getRateWizardUploadCollection() {
        return rateWizardUploadCollection;
    }

    public void setRateWizardUploadCollection(Collection<RateWizardUpload> rateWizardUploadCollection) {
        this.rateWizardUploadCollection = rateWizardUploadCollection;
    }

    @XmlTransient
    public Collection<Term> getTermCollection() {
        return termCollection;
    }

    public void setTermCollection(Collection<Term> termCollection) {
        this.termCollection = termCollection;
    }

    @XmlTransient
    public Collection<WarrantyRule> getWarrantyRuleCollection() {
        return warrantyRuleCollection;
    }

    public void setWarrantyRuleCollection(Collection<WarrantyRule> warrantyRuleCollection) {
        this.warrantyRuleCollection = warrantyRuleCollection;
    }

    @XmlTransient
    public Collection<Rate> getRateCollection() {
        return rateCollection;
    }

    public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }

    @XmlTransient
    public Collection<PPMPlanService> getPPMPlanServiceCollection() {
        return pPMPlanServiceCollection;
    }

    public void setPPMPlanServiceCollection(Collection<PPMPlanService> pPMPlanServiceCollection) {
        this.pPMPlanServiceCollection = pPMPlanServiceCollection;
    }

    @XmlTransient
    public Collection<Limit> getLimitCollection() {
        return limitCollection;
    }

    public void setLimitCollection(Collection<Limit> limitCollection) {
        this.limitCollection = limitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planId != null ? planId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanTable)) {
            return false;
        }
        PlanTable other = (PlanTable) object;
        if ((this.planId == null && other.planId != null) || (this.planId != null && !this.planId.equals(other.planId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PlanTable[ planId=" + planId + " ]";
    }
    
}
