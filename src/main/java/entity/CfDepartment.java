/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfDepartment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfDepartment.findAll", query = "SELECT c FROM CfDepartment c")
    , @NamedQuery(name = "CfDepartment.findByDepartmentId", query = "SELECT c FROM CfDepartment c WHERE c.departmentId = :departmentId")
    , @NamedQuery(name = "CfDepartment.findByName", query = "SELECT c FROM CfDepartment c WHERE c.name = :name")
    , @NamedQuery(name = "CfDepartment.findByDescription", query = "SELECT c FROM CfDepartment c WHERE c.description = :description")
    , @NamedQuery(name = "CfDepartment.findByEnteredDate", query = "SELECT c FROM CfDepartment c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CfDepartment.findByUpdateUserName", query = "SELECT c FROM CfDepartment c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfDepartment.findByUpdateLast", query = "SELECT c FROM CfDepartment c WHERE c.updateLast = :updateLast")})
public class CfDepartment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "departmentId")
    private Integer departmentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "departmentIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "departmentIdFk")
    private Collection<UserMember> userMemberCollection;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public CfDepartment() {
    }

    public CfDepartment(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public CfDepartment(Integer departmentId, String name, String description, Date enteredDate) {
        this.departmentId = departmentId;
        this.name = name;
        this.description = description;
        this.enteredDate = enteredDate;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<UserMember> getUserMemberCollection() {
        return userMemberCollection;
    }

    public void setUserMemberCollection(Collection<UserMember> userMemberCollection) {
        this.userMemberCollection = userMemberCollection;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departmentId != null ? departmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfDepartment)) {
            return false;
        }
        CfDepartment other = (CfDepartment) object;
        if ((this.departmentId == null && other.departmentId != null) || (this.departmentId != null && !this.departmentId.equals(other.departmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfDepartment[ departmentId=" + departmentId + " ]";
    }
    
}
