/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.Contracts;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimTabView")
@ApplicationScoped
public class ClaimTabView {

    private static final Logger LOGGER = LogManager.getLogger(ClaimTabView.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;

    @Inject
    transient private ClaimMisc claimMisc;

    @Inject
    ClaimGeneralService claimGeneralService;

    @Inject
    ClaimDTSService claimDTSService;

    @Inject
    ClaimNoteService claimNoteService;

    @Inject
    ContractNoteService contractNoteService;

    @Inject
    ClaimCVDService claimCVDService;

    @Inject
    ClaimPPMTblService claimPPMTblService;

    /**
     * Creates a new instance of claimTabView
     */
    public ClaimTabView() {
    }

    private String claimNumber;
    private Claim claim;
    private String contractNo;
    private Contracts contract;

    boolean ANCedit;

    public boolean isANCedit() {
        LOGGER.info("isANCedit=" + ANCedit);
        return ANCedit;
    }

    public void setANCedit(boolean ANCedit) {
        LOGGER.info("setANCedit");
        this.ANCedit = ANCedit;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Contracts getContract() {
        return contract;
    }

    public void setContract(Contracts contract) {
        this.contract = contract;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public void editClaimTab(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("editClaimTab, claimNumber=" + claimNumber);

        if (claimNumber != null) {
            claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
            claimGeneralService.setClaim(claim);
            claimGeneralService.setClaimNumber(claimNumber);
            
            contract = claimMisc.getContractsByClaimNum(claimNumber);
            contractNo = contract.getContractNo();
            claimGeneralService.setContract(contract);
            claimGeneralService.setContractNo(contractNo);
            LOGGER.info("contractNo=" + contractNo);
            
            
            if (claimMisc.isVSC(claim)) {
                claimGeneralService.setClaimType("VSC");
                claimGeneralService.reloadGT();
                reloadDT();
                claimGeneralService.setVSCeditable(1);
                claimGeneralService.setVSCreadonly(false);
            } else if (claimMisc.isANC(claim)) {
                claimGeneralService.setClaimType("ANC");
                claimGeneralService.reloadGTANC(claimNumber);
                claimGeneralService.setANCeditable(1);
                claimGeneralService.setANCreadonly(false);
            } else if (claimMisc.isPPM(claim)) {
                claimGeneralService.setClaimType("PPM");
                claimPPMTblService.setContractNo(contractNo);
                claimGeneralService.reloadGTANC(claimNumber);
                claimGeneralService.setPpmodometer(claimGeneralService.getCganc().getOdometer());
                claimGeneralService.setPPMeditable(1);
                claimGeneralService.setPPMreadonly(false);
            }

            reloadNT();
            reloadCNT();

            

            reloadCVD();

            Map<String, Object> options = new HashMap<>();
            options.put("resizable", true);
            options.put("draggable", true);
            options.put("contentHeight", "100%");
            options.put("contentWidth", "100%");
            options.put("height", "1000");
            options.put("width", "2000");
            options.put("modal", true);

            RequestContext.getCurrentInstance().openDialog("claimTabView", options, null);
            /*
            String typeOFclaim = claimMisc.getClaimType(claim);
            switch (typeOFclaim) {
                case "VSC":
                    RequestContext.getCurrentInstance().openDialog("vscClaimTabView", options, null);
                    break;
                case "ANC":
                    RequestContext.getCurrentInstance().openDialog("ancClaimTabView", options, null);
                    break;
                case "PPM":
                    RequestContext.getCurrentInstance().openDialog("ppmClaimTabView", options, null);
                    break;
                case "GAP":
                    RequestContext.getCurrentInstance().openDialog("gapClaimTabView", options, null);
                    break;
                default:
                    LOGGER.info("No claim type found, please verify......");
                    break;
            }
             */
        }
    }

    public void newClaimTab(ActionEvent event) {

        contractNo = (String) event.getComponent().getAttributes().get("contractNo");
        claimGeneralService.setContractNo(contractNo);
        claimPPMTblService.setContractNo(contractNo);
        LOGGER.info("newClaimTab, contractNo=" + contractNo);

        if (contractNo != null && ! contractNo.isEmpty()) {
            claimGeneralService.setClaimNumber(null);
            claimGeneralService.setClaim(null);
            contract = em.createNamedQuery("Contracts.findByContractNo", Contracts.class).setParameter("contractNo", contractNo).getSingleResult();
            claimGeneralService.setContract(contract);

            if (claimMisc.isVSC(contract)) {
                claimGeneralService.setClaimType("VSC");
                claimGeneralService.setVSCreadonly(false);
                claimGeneralService.setVSCeditable(0);
                List<ClaimGeneral> cgList = claimGeneralService.createClaimViewByContractNo(contractNo);
                if (cgList != null && cgList.size() > 0) {
                    claimGeneralService.setCg(cgList.get(0));    // visited later ........
                }
            } else if (claimMisc.isANC(contract)) {
                claimGeneralService.setClaimType("ANC");
                claimGeneralService.setANCreadonly(false);
                claimGeneralService.setANCeditable(0);
                List<ClaimGeneralANC> cgList = claimGeneralService.createClaimViewByContractNoANC(contractNo);
                if (cgList != null && cgList.size() > 0) {
                    claimGeneralService.setCganc(cgList.get(0));    // visited later ........
                }
            } else if (claimMisc.isPPM(contract)) {
                claimPPMTblService.setContractNo(contractNo);
                claimGeneralService.setClaimType("PPM");
                claimGeneralService.setPPMreadonly(false);
                claimGeneralService.setPPMeditable(0);
                List<ClaimGeneralANC> cgList = claimGeneralService.createClaimViewByContractNoANC(contractNo);
                if (cgList != null && cgList.size() > 0) {
                    claimGeneralService.setCganc(cgList.get(0));    // visited later ........
                }
            }

            reloadCVD();

            Map<String, Object> options = new HashMap<>();
            options.put("resizable", true);
            options.put("draggable", true);
            options.put("contentHeight", "100%");
            options.put("contentWidth", "100%");
            options.put("height", "1000");
            options.put("width", "2000");
            options.put("modal", true);

            RequestContext.getCurrentInstance().openDialog("claimTabView", options, null);
            
        }
    }
    
    
    public void viewClaimTab(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("viewClaimTab=" + claimNumber);

        if (claimNumber != null) {
            
            contract = claimMisc.getContractsByClaimNum(claimNumber);
            contractNo = contract.getContractNo();
            LOGGER.info("contractNo=" + contractNo);

            claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
            claimGeneralService.setClaim(claim);
            claimGeneralService.setClaimNumber(claimNumber);
            if (claimMisc.isVSC(claim)) {
                claimGeneralService.setClaimType("VSC");
                claimGeneralService.reloadGT();
                reloadDT();
                claimGeneralService.setVSCeditable(2);
                claimGeneralService.setVSCreadonly(true);
            } else if (claimMisc.isANC(claim)) {
                claimGeneralService.setClaimType("ANC");
                claimGeneralService.reloadGTANC(claimNumber);
                claimGeneralService.setANCeditable(2);
                claimGeneralService.setANCreadonly(true);;
            } else if (claimMisc.isPPM(claim)) {
                claimGeneralService.setClaimType("PPM");
                claimPPMTblService.setContractNo(contractNo);
                claimGeneralService.reloadGTANC(claimNumber);
                claimGeneralService.setPpmodometer(claimGeneralService.getCganc().getOdometer());
                claimGeneralService.setPPMeditable(2);
                claimGeneralService.setPPMreadonly(true);
            }

            reloadNT();
            reloadCNT();

            

            reloadCVD();

            Map<String, Object> options = new HashMap<>();
            options.put("resizable", true);
            options.put("draggable", true);
            options.put("contentHeight", "100%");
            options.put("contentWidth", "100%");
            options.put("height", "1000");
            options.put("width", "2000");
            options.put("modal", true);

            RequestContext.getCurrentInstance().openDialog("claimTabView", options, null);

        }
    }

    public void chooseClaimTab(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("chooseClaimTab=" + claimNumber);
        contractNo = (String) event.getComponent().getAttributes().get("contractNo");
        LOGGER.info("chooseClaimTab=" + contractNo);

        if (claimNumber != null) {
            claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
            if (claimMisc.isVSC(claim)) {
                claimGeneralService.reloadGT();
                reloadDT();
            } else if (claimMisc.isANC(claim)) {
                claimGeneralService.reloadGTANC(claimNumber);
            }

            reloadNT();
            reloadCNT();

            contract = claimMisc.getContractsByClaimNum(claimNumber);
            contractNo = contract.getContractNo();
            LOGGER.info("contractNo=" + contractNo);

        } else if (contractNo != null) {
            contract = em.createNamedQuery("Contracts.findByContractNo", Contracts.class).setParameter("contractNo", contractNo).getSingleResult();
            claimGeneralService.setContract(contract);
            if (claimMisc.isVSC(contract)) {
                List<ClaimGeneral> cgList = claimGeneralService.createClaimViewByContractNo(contractNo);
                if (cgList != null && cgList.size() > 0) {
                    claimGeneralService.setCg(cgList.get(0));    // visited later ........
                }
            } else if (claimMisc.isANC(contract)) {
                ANCedit = true;
                List<ClaimGeneralANC> cgList = claimGeneralService.createClaimViewByContractNoANC(contractNo);
                if (cgList != null && cgList.size() > 0) {
                    claimGeneralService.setCganc(cgList.get(0));    // visited later ........
                }
            }

        }

        reloadCVD();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("claimTabView", options, null);
    }

    public void tabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        //claimNumber = event.getTab().getAriaLabel();
        LOGGER.info("tabChange, claimNumber=" + claimNumber);
        //Contracts contract = claimMisc.getContractsByClaimNum(claimNumber);
        String contracNo = claimGeneralService.getContractNo();
        LOGGER.info("tabChange, contractNo=" + contractNo);
        Integer odometer = claimGeneralService.getPpmodometer();
        LOGGER.info("tabChange, odometer=" + odometer);
        if (claimNumber != null) {
            switch (tabId) {
                case "generalTab":
                    claimGeneralService.reloadGT();
                    break;
                case "generalTabANC":
                    claimGeneralService.reloadGTANC(claimNumber);
                    break;
                case "generalTabPPM":
                    claimGeneralService.reloadGTANC(claimNumber);
                    break;
                case "detailTab":
                    reloadDT();
                    break;
                case "ppmfullTab":                    
                    claimPPMTblService.createPPMTblView(contractNo);
                    break;
                case "ppmavailableTab":
                    claimPPMTblService.setClaimNumber(claimNumber);
                    claimPPMTblService.createPPMavailableView(contractNo, odometer);
                    break;
                case "ppmusedTab":
                    claimPPMTblService.createPPMusedView(contractNo);
                    break;
                case "ppmexpiredTab":
                    claimPPMTblService.createPPMexpiredView(contractNo, odometer);
                    break;
                case "ppmupTab":
                    claimPPMTblService.createPPMupView(contractNo, odometer);
                    break;
                case "notesTab":
                    reloadNT();
                    break;
                case "contractNotesTab":
                    reloadCNT();
                    break;
                default:
                    claimGeneralService.reloadGT();
                    reloadDT();
                    reloadNT();
                    reloadCNT();
                    break;
            }
        }
    }

    /**
     * establish ClaimGeneralService tab
     */
    public void reloadGT() {
        LOGGER.info("in reloadGT=" + claimNumber);
        claimGeneralService.setClaimNumber(claimNumber);
        claim = claimMisc.getClaimByNumber(claimNumber);
        claimGeneralService.setClaim(claim);
        ClaimGeneral cg = claimGeneralService.createClaimView(claimNumber);
        claimGeneralService.setCg(cg);
    }

    /*
    public void reloadGTANC() {
        LOGGER.info("in reloadGTANC=" + claimNumber);
        claimGeneralService.setClaimNumber(claimNumber);
        claim = claimMisc.getClaimByNumber(claimNumber);
        claimGeneralService.setClaim(claim);
        ClaimGeneralANC cganc = claimGeneralService.createClaimViewANC(claimNumber);
        claimGeneralService.setCganc(cganc);
    }
     */
    /**
     * establish ClaimDTSService tab
     */
    public void reloadDT() {
        if( claimNumber == null ) {
            claimNumber = claimGeneralService.getClaimNumber();
        } 
        LOGGER.info("in reloadDT=" + claimNumber);
        claimDTSService.setClaimNumber(claimNumber);
        claim = claimMisc.getClaimByNumber(claimNumber);
        claimDTSService.setClaim(claim);
        ClaimDts claimDts = claimDTSService.createClaimDtsView(claimNumber);
        claimDTSService.setClaimDts(claimDts);
        claimDTSService.reset();
    }

    /**
     * establish ClaimNoteService tab
     */
    public void reloadNT() {
        LOGGER.info("in reloadNT=" + claimNumber);
        claimNoteService.setClaimNumber(claimNumber);
        claim = claimMisc.getClaimByNumber(claimNumber);
        claimNoteService.setClaim(claim);
        ClaimNotes claimNotes = claimNoteService.createClaimNotesView(claimNumber);
        claimNoteService.setClaimNotes(claimNotes);
    }

    public void reloadCNT() {
        LOGGER.info("in reloadCNT=" + claimNumber);
        contractNoteService.setClaimNumber(claimNumber);
        claim = claimMisc.getClaimByNumber(claimNumber);
        contractNoteService.setClaim(claim);
        List<ContractNote> contractNotes = contractNoteService.createContractNotesView(claimNumber);
        contractNoteService.setContractNotes(contractNotes);
    }

    public void reloadCVD() {
        LOGGER.info("in reloadCVD=" + contractNo);

        claimCVDService.setContractNo(contractNo);
        claimCVDService.setClaimNumber(claimNumber);

        ClaimContract claimContract = claimCVDService.createClaimContractView(contractNo);
        claimCVDService.setClaimContract(claimContract);

        ClaimVehicleInfo claimVehicleInfo = claimCVDService.createClaimVehicleInfoView(contractNo);
        claimCVDService.setClaimVehicleInfo(claimVehicleInfo);

        ClaimDealer claimDealer = claimCVDService.createDealerView(contractNo);
        claimCVDService.setClaimDealer(claimDealer);
        
        LOGGER.info("end of reloadCVD");
    }

}
