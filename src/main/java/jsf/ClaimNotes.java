/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimNotes implements Serializable {
    
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    public ClaimNotes() {
    }

    public ClaimNotes(Integer claimId, String claimNumber, List<ClaimNote> claimNotes) {
        this.claimId = claimId;
        this.claimNumber = claimNumber;
        this.claimNotes = claimNotes;
    }
    
    
    
    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    List<ClaimNote> claimNotes;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public List<ClaimNote> getClaimNotes() {
        return claimNotes;
    }

    public void setClaimNotes(List<ClaimNote> claimNotes) {
        this.claimNotes = claimNotes;
    }
    
    
    
}
