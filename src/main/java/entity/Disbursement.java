/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Disbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Disbursement.findAll", query = "SELECT d FROM Disbursement d")
    , @NamedQuery(name = "Disbursement.findByDisbursementId", query = "SELECT d FROM Disbursement d WHERE d.disbursementId = :disbursementId")})
public class Disbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DisbursementId")
    private Integer disbursementId;
    @OneToMany(mappedBy = "disbursementIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @JoinColumn(name = "directAgentConditionIdFk", referencedColumnName = "agentId")
    @ManyToOne
    private Agent directAgentConditionIdFk;
    @JoinColumn(name = "dealerConditionIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerConditionIdFk;
    @JoinColumn(name = "dealerGroupConditionIdFk", referencedColumnName = "dealerGroupId")
    @ManyToOne
    private DealerGroup dealerGroupConditionIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;

    public Disbursement() {
    }

    public Disbursement(Integer disbursementId) {
        this.disbursementId = disbursementId;
    }

    public Integer getDisbursementId() {
        return disbursementId;
    }

    public void setDisbursementId(Integer disbursementId) {
        this.disbursementId = disbursementId;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    public Agent getDirectAgentConditionIdFk() {
        return directAgentConditionIdFk;
    }

    public void setDirectAgentConditionIdFk(Agent directAgentConditionIdFk) {
        this.directAgentConditionIdFk = directAgentConditionIdFk;
    }

    public Dealer getDealerConditionIdFk() {
        return dealerConditionIdFk;
    }

    public void setDealerConditionIdFk(Dealer dealerConditionIdFk) {
        this.dealerConditionIdFk = dealerConditionIdFk;
    }

    public DealerGroup getDealerGroupConditionIdFk() {
        return dealerGroupConditionIdFk;
    }

    public void setDealerGroupConditionIdFk(DealerGroup dealerGroupConditionIdFk) {
        this.dealerGroupConditionIdFk = dealerGroupConditionIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementId != null ? disbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Disbursement)) {
            return false;
        }
        Disbursement other = (Disbursement) object;
        if ((this.disbursementId == null && other.disbursementId != null) || (this.disbursementId != null && !this.disbursementId.equals(other.disbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Disbursement[ disbursementId=" + disbursementId + " ]";
    }
    
}
