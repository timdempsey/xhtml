/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "LienHolder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LienHolder.findAll", query = "SELECT l FROM LienHolder l")
    , @NamedQuery(name = "LienHolder.findByLienHolderId", query = "SELECT l FROM LienHolder l WHERE l.lienHolderId = :lienHolderId")
    , @NamedQuery(name = "LienHolder.findByLienHolderType", query = "SELECT l FROM LienHolder l WHERE l.lienHolderType = :lienHolderType")
    , @NamedQuery(name = "LienHolder.findByUpdateUserName", query = "SELECT l FROM LienHolder l WHERE l.updateUserName = :updateUserName")
    , @NamedQuery(name = "LienHolder.findByUpdateLast", query = "SELECT l FROM LienHolder l WHERE l.updateLast = :updateLast")})
public class LienHolder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "lienHolderId")
    private Integer lienHolderId;
    @Size(max = 25)
    @Column(name = "lienHolderType")
    private String lienHolderType;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "lienHolderIdFk")
    private Collection<Contracts> contractsCollection;
    @JoinColumn(name = "lienHolderId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;

    public LienHolder() {
    }

    public LienHolder(Integer lienHolderId) {
        this.lienHolderId = lienHolderId;
    }

    public Integer getLienHolderId() {
        return lienHolderId;
    }

    public void setLienHolderId(Integer lienHolderId) {
        this.lienHolderId = lienHolderId;
    }

    public String getLienHolderType() {
        return lienHolderType;
    }

    public void setLienHolderType(String lienHolderType) {
        this.lienHolderType = lienHolderType;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lienHolderId != null ? lienHolderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LienHolder)) {
            return false;
        }
        LienHolder other = (LienHolder) object;
        if ((this.lienHolderId == null && other.lienHolderId != null) || (this.lienHolderId != null && !this.lienHolderId.equals(other.lienHolderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LienHolder[ lienHolderId=" + lienHolderId + " ]";
    }
    
}
