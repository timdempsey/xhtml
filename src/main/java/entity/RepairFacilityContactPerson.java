/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RepairFacilityContactPerson")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepairFacilityContactPerson.findAll", query = "SELECT r FROM RepairFacilityContactPerson r")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByRepairFacilityContactPersonId", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.repairFacilityContactPersonId = :repairFacilityContactPersonId")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByName", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.name = :name")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByTitle", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.title = :title")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByPhoneNumber", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByFaxNumber", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.faxNumber = :faxNumber")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByExtension", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.extension = :extension")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByEmailAddress", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.emailAddress = :emailAddress")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByUpdateUserName", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RepairFacilityContactPerson.findByUpdateLast", query = "SELECT r FROM RepairFacilityContactPerson r WHERE r.updateLast = :updateLast")})
public class RepairFacilityContactPerson implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "repairFacilityContactPersonId")
    private Integer repairFacilityContactPersonId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 20)
    @Column(name = "title")
    private String title;
    @Size(max = 20)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Size(max = 20)
    @Column(name = "faxNumber")
    private String faxNumber;
    @Size(max = 10)
    @Column(name = "extension")
    private String extension;
    @Size(max = 100)
    @Column(name = "emailAddress")
    private String emailAddress;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "repairFacilityIdFk", referencedColumnName = "repairFacilityId")
    @ManyToOne(optional = false)
    private RepairFacility repairFacilityIdFk;
    @OneToMany(mappedBy = "repairFacilityContactIdFk")
    private Collection<Claim> claimCollection;

    public RepairFacilityContactPerson() {
    }

    public RepairFacilityContactPerson(Integer repairFacilityContactPersonId) {
        this.repairFacilityContactPersonId = repairFacilityContactPersonId;
    }

    public RepairFacilityContactPerson(Integer repairFacilityContactPersonId, String name) {
        this.repairFacilityContactPersonId = repairFacilityContactPersonId;
        this.name = name;
    }

    public Integer getRepairFacilityContactPersonId() {
        return repairFacilityContactPersonId;
    }

    public void setRepairFacilityContactPersonId(Integer repairFacilityContactPersonId) {
        this.repairFacilityContactPersonId = repairFacilityContactPersonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public RepairFacility getRepairFacilityIdFk() {
        return repairFacilityIdFk;
    }

    public void setRepairFacilityIdFk(RepairFacility repairFacilityIdFk) {
        this.repairFacilityIdFk = repairFacilityIdFk;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repairFacilityContactPersonId != null ? repairFacilityContactPersonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepairFacilityContactPerson)) {
            return false;
        }
        RepairFacilityContactPerson other = (RepairFacilityContactPerson) object;
        if ((this.repairFacilityContactPersonId == null && other.repairFacilityContactPersonId != null) || (this.repairFacilityContactPersonId != null && !this.repairFacilityContactPersonId.equals(other.repairFacilityContactPersonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RepairFacilityContactPerson[ repairFacilityContactPersonId=" + repairFacilityContactPersonId + " ]";
    }
    
}
