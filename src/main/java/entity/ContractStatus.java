/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractStatus.findAll", query = "SELECT c FROM ContractStatus c")
    , @NamedQuery(name = "ContractStatus.findByContractStatusId", query = "SELECT c FROM ContractStatus c WHERE c.contractStatusId = :contractStatusId")
    , @NamedQuery(name = "ContractStatus.findByContractStatusName", query = "SELECT c FROM ContractStatus c WHERE c.contractStatusName = :contractStatusName")})
public class ContractStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractStatusId")
    private Integer contractStatusId;
    @Size(max = 40)
    @Column(name = "contractStatusName")
    private String contractStatusName;
    @OneToMany(mappedBy = "contractStatusIdFk")
    private Collection<Contracts> contractsCollection;

    public ContractStatus() {
    }

    public ContractStatus(Integer contractStatusId) {
        this.contractStatusId = contractStatusId;
    }

    public Integer getContractStatusId() {
        return contractStatusId;
    }

    public void setContractStatusId(Integer contractStatusId) {
        this.contractStatusId = contractStatusId;
    }

    public String getContractStatusName() {
        return contractStatusName;
    }

    public void setContractStatusName(String contractStatusName) {
        this.contractStatusName = contractStatusName;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractStatusId != null ? contractStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractStatus)) {
            return false;
        }
        ContractStatus other = (ContractStatus) object;
        if ((this.contractStatusId == null && other.contractStatusId != null) || (this.contractStatusId != null && !this.contractStatusId.equals(other.contractStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractStatus[ contractStatusId=" + contractStatusId + " ]";
    }
    
}
