/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "dowclogin")
@ApplicationScoped
public class Login {
    
    // JP
    private static final Logger LOGGER = LogManager.getLogger(PlanService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;
    // JP

     private String loginUser;
    private String loginPassword;
    private String destinationPage;

    public Login() {
        setDestinationPage("index1");
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getDestinationPage() {
        return destinationPage;
    }

    public void setDestinationPage(String destinationPage) {
        this.destinationPage = destinationPage;
    }

    public void authen1() {
        try {
        String pswd = get_SecurePasswordWithoutSalt(getLoginPassword());
        /*
        if ((getLoginUser().equals("michael") && getLoginPassword().equals("dowc"))
                || (getLoginUser().equals("david") && getLoginPassword().equals("dowc"))
                || (getLoginUser().equals("alicia") && getLoginPassword().equals("dowc"))
                || (getLoginUser().equals("frank") && getLoginPassword().equals("dowc"))) {
            setDestinationPage("index2");
*/
        if( claimMisc.verifyUserLogin(getLoginUser(), pswd) ) {
            setDestinationPage("index");
        } else {
            setDestinationPage("index1");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Login Failure", "Your Credentials Incorrect!"));
            setLoginUser("");
            setLoginPassword("");
        }
    } catch ( Exception ex  ) {
    }
}
 
    public String goHome() {
        return getDestinationPage();
    }
    
    public static String get_SecurePasswordWithoutSalt(String passwordToHash) throws NoSuchAlgorithmException {
        byte[] salt = getSalt();
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
    
    public static byte[] getSalt() throws NoSuchAlgorithmException {
        // SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        for (int i = 0; i < 16; i++) {
            salt[i] = 20;
        }
        // sr.nextBytes(salt);
        return salt;
    }
    
}
