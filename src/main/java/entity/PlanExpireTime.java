/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "planExpireTime")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanExpireTime.findAll", query = "SELECT p FROM PlanExpireTime p")
    , @NamedQuery(name = "PlanExpireTime.findByPlanExpireTimeId", query = "SELECT p FROM PlanExpireTime p WHERE p.planExpireTimeId = :planExpireTimeId")
    , @NamedQuery(name = "PlanExpireTime.findByDescription", query = "SELECT p FROM PlanExpireTime p WHERE p.description = :description")})
public class PlanExpireTime implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "planExpireTimeId")
    private Integer planExpireTimeId;
    @Size(max = 30)
    @Column(name = "description")
    private String description;

    public PlanExpireTime() {
    }

    public PlanExpireTime(Integer planExpireTimeId) {
        this.planExpireTimeId = planExpireTimeId;
    }

    public Integer getPlanExpireTimeId() {
        return planExpireTimeId;
    }

    public void setPlanExpireTimeId(Integer planExpireTimeId) {
        this.planExpireTimeId = planExpireTimeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planExpireTimeId != null ? planExpireTimeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanExpireTime)) {
            return false;
        }
        PlanExpireTime other = (PlanExpireTime) object;
        if ((this.planExpireTimeId == null && other.planExpireTimeId != null) || (this.planExpireTimeId != null && !this.planExpireTimeId.equals(other.planExpireTimeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PlanExpireTime[ planExpireTimeId=" + planExpireTimeId + " ]";
    }
    
}
