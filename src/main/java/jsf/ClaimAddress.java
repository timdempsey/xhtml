/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import javax.persistence.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimAddress implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(ClaimAddress.class);

    public ClaimAddress() {
    }

    public ClaimAddress(String address1, String address2, String city, String zipCode, String countryCode, String phoneNumber, String fax, String regionCode, String attentionTo, boolean emailOptIn) {
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.zipCode = zipCode;
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
        this.fax = fax;
        this.regionCode = regionCode;
        this.attentionTo = attentionTo;
        this.emailOptIn = emailOptIn;
    }

    @Column(name = "address1", table = "Address")
    private String address1;
    @Column(name = "address2", table = "Address")
    private String address2;
    @Column(name = "city", table = "Address")
    private String city;
    @Column(name = "zipCode", table = "Address")
    private String zipCode;
    @Column(name = "countryCode", table = "Country")
    private String countryCode;
    @Column(name = "phoneNumber", table = "Address")
    private String phoneNumber;
    @Column(name = "fax", table = "Address")
    private String fax;
    @Column(name = "regionCode", table = "Region")
    private String regionCode;
    private String attentionTo;
    boolean emailOptIn;

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public String getAttentionTo() {
        return attentionTo;
    }

    public void setAttentionTo(String attentionTo) {
        this.attentionTo = attentionTo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    void reset() {
        this.address1 = "";
        this.address2 = "";
        this.city = "";
        this.zipCode = "";
        this.countryCode = "";
        this.phoneNumber = "";
        this.fax = "";
        this.regionCode = "";
        this.attentionTo = "";
        this.emailOptIn = false;

    }

}
