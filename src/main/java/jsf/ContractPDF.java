/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Stanley
 */
@Named(value = "contractPDF")
@ApplicationScoped
public class ContractPDF {

    private static final Logger LOGGER = LogManager.getLogger(ContractPDF.class);

    /**
     * Creates a new instance of ContractPDF
     */
    public ContractPDF() {
    }

    private String srcfile="C:\\WebSite\\pdfTemplates\\BNDL4-06.2016.1_M.pdf";
    private String destfile;// = "C:\\WebSite\\pdfContracts\\pdfGenerated1.pdf";

    private String buyerName;
    private String contractNo;
    private String vin;
    private String vehicle_year;
    private String vehicle_make;
    private String vehicle_model;
    private String vehicle_series;
    private String vehicle_odometer;
    private String contract_purchasePrice;
    private String contract_effectiveDate;
    private String dealer_name;
    private String term_months;
    private String contract_plan;
    
    private StreamedContent content;

    public StreamedContent getContent() {
        LOGGER.info("in getContent, content=" + content);
        return content;
    }

    public void setContent(StreamedContent content) {
        this.content = content;
    }
    
    

    public String getSrcfile() {
        return srcfile;
    }

    public void setSrcfile(String srcfile) {
        this.srcfile = srcfile;
    }

    public String getDestfile() {
        return destfile;
    }

    public void setDestfile(String destfile) {
        this.destfile = destfile;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public String getVehicle_make() {
        return vehicle_make;
    }

    public void setVehicle_make(String vehicle_make) {
        this.vehicle_make = vehicle_make;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_series() {
        return vehicle_series;
    }

    public void setVehicle_series(String vehicle_series) {
        this.vehicle_series = vehicle_series;
    }

    public String getVehicle_odometer() {
        return vehicle_odometer;
    }

    public void setVehicle_odometer(String vehicle_odometer) {
        this.vehicle_odometer = vehicle_odometer;
    }

    public String getContract_purchasePrice() {
        return contract_purchasePrice;
    }

    public void setContract_purchasePrice(String contract_purchasePrice) {
        this.contract_purchasePrice = contract_purchasePrice;
    }

    public String getContract_effectiveDate() {
        return contract_effectiveDate;
    }

    public void setContract_effectiveDate(String contract_effectiveDate) {
        this.contract_effectiveDate = contract_effectiveDate;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getTerm_months() {
        return term_months;
    }

    public void setTerm_months(String term_months) {
        this.term_months = term_months;
    }

    public String getContract_plan() {
        return contract_plan;
    }

    public void setContract_plan(String contract_plan) {
        this.contract_plan = contract_plan;
    }
    
    
    

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void fillPDF() throws DocumentException, IOException {
        LOGGER.info(getContractNo());
        LOGGER.info(getVin());

        File file = new File(destfile);
        file.getParentFile().mkdirs();
        manipulatePdf(srcfile, destfile);

    }

    public void manipulatePdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        AcroFields form = stamper.getAcroFields();

        form.setField("customer_fullName", getBuyerName());
        form.setField("contract_number", contractNo);
        form.setField("vehicle_year", vehicle_year);
        form.setField("vehicle_make", vehicle_make);
        form.setField("vehicle_model", vehicle_model);
        form.setField("vehicle_series", vehicle_series);
        form.setField("vehicle_odometer", vehicle_odometer);
        form.setField("contract_purchasePrice", contract_purchasePrice);
        form.setField("contract_effectiveDate", contract_effectiveDate);
        form.setField("dealer_name", dealer_name);
        form.setField("term_months", term_months);
        form.setField("contract_selectedPlanType_New", contract_plan);
        form.setField("vehicle_vin", vin);
        /*
        form.setFiled("coBuyer_fullName", coBuyer_fullName);
        form.setFiled("customer_billingAddress_addressLines", customer_billingAddress_addressLines);
        form.setFiled("coBuyer_billingAddress_addressLines", coBuyer_billingAddress_addressLines);
        form.setFiled("customer_billingAddress_cityStateZip", customer_billingAddress_cityStateZip);
        form.setFiled("coBuyer_billingAddress_cityStateZip", coBuyer_billingAddress_cityStateZip);
        form.setFiled("customer_billingAddress_phone", customer_billingAddress_phone);
        form.setFiled("customer_physicalAddress_phone", customer_physicalAddress_phone);
        form.setFiled("coBuyer_billingAddress_phone", coBuyer_billingAddress_phone);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);
        
        form.setFiled("customer_email", customer_email);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);
        form.setFiled("coBuyer_physicalAddress_phone", coBuyer_physicalAddress_phone);

        , coBuyer_email
        , vehicle_year
        , vehicle_make 
        , vehicle_model 
        , vehicle_series
        , contract_selectedPlanType_New 
        , contract_selectedPlanType_Used
        , contract_selectedPlanType_Certified 
        , vehicle_odometer 
        , dealer_name 
        , dealer_billingAddress_phone
        , dealer_billingAddress_addressLines
        dealer_billingAddress_cityStateZip 
        , lienHolder_name 
        , lienHolder_billingAddress_phone 
        , lienHolder_billingAddress_addressLines 
        , lienHolder_billingAddress_cityStateZip 
        , plan_selectedPlanCode_BUN4 
        , plan_selectedPlanCode_Bun45k 
        , WDS
        , PDR 
        , RSD 
        , contract_purchasePrice 
        , contract_effectiveDate 
        , term_months
         */
        stamper.setFormFlattening(true);
        stamper.close();
    }
    
    public void createStreamedContent() {
        LOGGER.info("To Show a PDF file on browser" );
        LOGGER.info("destfile=" + destfile);
        content = new DefaultStreamedContent(getData(destfile), "application/pdf", "downloaded" + destfile);
    }

    private InputStream getData(String fileName) {

        File file = new File(fileName);
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
        }
        return is;
    }

}
