/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimDetailSub")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimDetailSub.findAll", query = "SELECT c FROM ClaimDetailSub c")
    , @NamedQuery(name = "ClaimDetailSub.findByClaimDetailSubId", query = "SELECT c FROM ClaimDetailSub c WHERE c.claimDetailSubId = :claimDetailSubId")
    , @NamedQuery(name = "ClaimDetailSub.findByComplaintReason", query = "SELECT c FROM ClaimDetailSub c WHERE c.complaintReason = :complaintReason")
    , @NamedQuery(name = "ClaimDetailSub.findByCause", query = "SELECT c FROM ClaimDetailSub c WHERE c.cause = :cause")
    , @NamedQuery(name = "ClaimDetailSub.findByCorrection", query = "SELECT c FROM ClaimDetailSub c WHERE c.correction = :correction")
    , @NamedQuery(name = "ClaimDetailSub.findByTotalAmt", query = "SELECT c FROM ClaimDetailSub c WHERE c.totalAmt = :totalAmt")
    , @NamedQuery(name = "ClaimDetailSub.findByApproveStatus", query = "SELECT c FROM ClaimDetailSub c WHERE c.approveStatus = :approveStatus")
    , @NamedQuery(name = "ClaimDetailSub.findByUpdateUserName", query = "SELECT c FROM ClaimDetailSub c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimDetailSub.findByUpdateLast", query = "SELECT c FROM ClaimDetailSub c WHERE c.updateLast = :updateLast")})
public class ClaimDetailSub implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimDetailSubId")
    private Integer claimDetailSubId;
    @Size(max = 1000)
    @Column(name = "complaintReason")
    private String complaintReason;
    @Size(max = 1000)
    @Column(name = "cause")
    private String cause;
    @Size(max = 1000)
    @Column(name = "correction")
    private String correction;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "totalAmt")
    private BigDecimal totalAmt;
    @Size(max = 10)
    @Column(name = "approveStatus")
    private String approveStatus;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "claimDetailSubIdFk")
    private Collection<ClaimDetail> claimDetailCollection;
    @JoinColumn(name = "claimDetailIdFk", referencedColumnName = "claimDetailId")
    @ManyToOne
    private ClaimDetail claimDetailIdFk;

    public ClaimDetailSub() {
    }

    public ClaimDetailSub(Integer claimDetailSubId) {
        this.claimDetailSubId = claimDetailSubId;
    }

    public Integer getClaimDetailSubId() {
        return claimDetailSubId;
    }

    public void setClaimDetailSubId(Integer claimDetailSubId) {
        this.claimDetailSubId = claimDetailSubId;
    }

    public String getComplaintReason() {
        return complaintReason;
    }

    public void setComplaintReason(String complaintReason) {
        this.complaintReason = complaintReason;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getCorrection() {
        return correction;
    }

    public void setCorrection(String correction) {
        this.correction = correction;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimDetail> getClaimDetailCollection() {
        return claimDetailCollection;
    }

    public void setClaimDetailCollection(Collection<ClaimDetail> claimDetailCollection) {
        this.claimDetailCollection = claimDetailCollection;
    }

    public ClaimDetail getClaimDetailIdFk() {
        return claimDetailIdFk;
    }

    public void setClaimDetailIdFk(ClaimDetail claimDetailIdFk) {
        this.claimDetailIdFk = claimDetailIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimDetailSubId != null ? claimDetailSubId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimDetailSub)) {
            return false;
        }
        ClaimDetailSub other = (ClaimDetailSub) object;
        if ((this.claimDetailSubId == null && other.claimDetailSubId != null) || (this.claimDetailSubId != null && !this.claimDetailSubId.equals(other.claimDetailSubId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimDetailSub[ claimDetailSubId=" + claimDetailSubId + " ]";
    }
    
}
