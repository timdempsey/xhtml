/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RepairFacilityToDealerRel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepairFacilityToDealerRel.findAll", query = "SELECT r FROM RepairFacilityToDealerRel r")
    , @NamedQuery(name = "RepairFacilityToDealerRel.findByRepairFacilityToDealerRelId", query = "SELECT r FROM RepairFacilityToDealerRel r WHERE r.repairFacilityToDealerRelId = :repairFacilityToDealerRelId")
    , @NamedQuery(name = "RepairFacilityToDealerRel.findByUpdateUserName", query = "SELECT r FROM RepairFacilityToDealerRel r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RepairFacilityToDealerRel.findByUpdateLast", query = "SELECT r FROM RepairFacilityToDealerRel r WHERE r.updateLast = :updateLast")})
public class RepairFacilityToDealerRel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "repairFacilityToDealerRelId")
    private Integer repairFacilityToDealerRelId;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "dealerId", referencedColumnName = "dealerId")
    @ManyToOne(optional = false)
    private Dealer dealerId;
    @JoinColumn(name = "repairFacilityId", referencedColumnName = "repairFacilityId")
    @ManyToOne(optional = false)
    private RepairFacility repairFacilityId;

    public RepairFacilityToDealerRel() {
    }

    public RepairFacilityToDealerRel(Integer repairFacilityToDealerRelId) {
        this.repairFacilityToDealerRelId = repairFacilityToDealerRelId;
    }

    public Integer getRepairFacilityToDealerRelId() {
        return repairFacilityToDealerRelId;
    }

    public void setRepairFacilityToDealerRelId(Integer repairFacilityToDealerRelId) {
        this.repairFacilityToDealerRelId = repairFacilityToDealerRelId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Dealer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Dealer dealerId) {
        this.dealerId = dealerId;
    }

    public RepairFacility getRepairFacilityId() {
        return repairFacilityId;
    }

    public void setRepairFacilityId(RepairFacility repairFacilityId) {
        this.repairFacilityId = repairFacilityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repairFacilityToDealerRelId != null ? repairFacilityToDealerRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepairFacilityToDealerRel)) {
            return false;
        }
        RepairFacilityToDealerRel other = (RepairFacilityToDealerRel) object;
        if ((this.repairFacilityToDealerRelId == null && other.repairFacilityToDealerRelId != null) || (this.repairFacilityToDealerRelId != null && !this.repairFacilityToDealerRelId.equals(other.repairFacilityToDealerRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RepairFacilityToDealerRel[ repairFacilityToDealerRelId=" + repairFacilityToDealerRelId + " ]";
    }
    
}
