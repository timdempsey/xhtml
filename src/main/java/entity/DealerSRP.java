/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DealerSRP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DealerSRP.findAll", query = "SELECT d FROM DealerSRP d")
    , @NamedQuery(name = "DealerSRP.findByDealerSRPId", query = "SELECT d FROM DealerSRP d WHERE d.dealerSRPId = :dealerSRPId")
    , @NamedQuery(name = "DealerSRP.findByDescription", query = "SELECT d FROM DealerSRP d WHERE d.description = :description")
    , @NamedQuery(name = "DealerSRP.findByPriority", query = "SELECT d FROM DealerSRP d WHERE d.priority = :priority")
    , @NamedQuery(name = "DealerSRP.findByAmount", query = "SELECT d FROM DealerSRP d WHERE d.amount = :amount")
    , @NamedQuery(name = "DealerSRP.findBySRPCalculationTypeInd", query = "SELECT d FROM DealerSRP d WHERE d.sRPCalculationTypeInd = :sRPCalculationTypeInd")
    , @NamedQuery(name = "DealerSRP.findBySRPTypeInd", query = "SELECT d FROM DealerSRP d WHERE d.sRPTypeInd = :sRPTypeInd")
    , @NamedQuery(name = "DealerSRP.findByDateEffective", query = "SELECT d FROM DealerSRP d WHERE d.dateEffective = :dateEffective")
    , @NamedQuery(name = "DealerSRP.findByDateExpires", query = "SELECT d FROM DealerSRP d WHERE d.dateExpires = :dateExpires")
    , @NamedQuery(name = "DealerSRP.findByUpdateUserName", query = "SELECT d FROM DealerSRP d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DealerSRP.findByUpdateLast", query = "SELECT d FROM DealerSRP d WHERE d.updateLast = :updateLast")})
public class DealerSRP implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerSRPId")
    private Integer dealerSRPId;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @Column(name = "priority")
    private Integer priority;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "SRPCalculationTypeInd")
    private Integer sRPCalculationTypeInd;
    @Column(name = "SRPTypeInd")
    private Integer sRPTypeInd;
    @Column(name = "dateEffective")
    @Temporal(TemporalType.DATE)
    private Date dateEffective;
    @Column(name = "dateExpires")
    @Temporal(TemporalType.DATE)
    private Date dateExpires;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;

    public DealerSRP() {
    }

    public DealerSRP(Integer dealerSRPId) {
        this.dealerSRPId = dealerSRPId;
    }

    public Integer getDealerSRPId() {
        return dealerSRPId;
    }

    public void setDealerSRPId(Integer dealerSRPId) {
        this.dealerSRPId = dealerSRPId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getSRPCalculationTypeInd() {
        return sRPCalculationTypeInd;
    }

    public void setSRPCalculationTypeInd(Integer sRPCalculationTypeInd) {
        this.sRPCalculationTypeInd = sRPCalculationTypeInd;
    }

    public Integer getSRPTypeInd() {
        return sRPTypeInd;
    }

    public void setSRPTypeInd(Integer sRPTypeInd) {
        this.sRPTypeInd = sRPTypeInd;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dealerSRPId != null ? dealerSRPId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DealerSRP)) {
            return false;
        }
        DealerSRP other = (DealerSRP) object;
        if ((this.dealerSRPId == null && other.dealerSRPId != null) || (this.dealerSRPId != null && !this.dealerSRPId.equals(other.dealerSRPId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DealerSRP[ dealerSRPId=" + dealerSRPId + " ]";
    }
    
}
