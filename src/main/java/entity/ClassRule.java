/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClassRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassRule.findAll", query = "SELECT c FROM ClassRule c")
    , @NamedQuery(name = "ClassRule.findByClassRuleId", query = "SELECT c FROM ClassRule c WHERE c.classRuleId = :classRuleId")
    , @NamedQuery(name = "ClassRule.findByVehicleTypeInd", query = "SELECT c FROM ClassRule c WHERE c.vehicleTypeInd = :vehicleTypeInd")
    , @NamedQuery(name = "ClassRule.findByMakeInd", query = "SELECT c FROM ClassRule c WHERE c.makeInd = :makeInd")
    , @NamedQuery(name = "ClassRule.findByModelInd", query = "SELECT c FROM ClassRule c WHERE c.modelInd = :modelInd")
    , @NamedQuery(name = "ClassRule.findBySeriesInd", query = "SELECT c FROM ClassRule c WHERE c.seriesInd = :seriesInd")
    , @NamedQuery(name = "ClassRule.findByModelYearInd", query = "SELECT c FROM ClassRule c WHERE c.modelYearInd = :modelYearInd")
    , @NamedQuery(name = "ClassRule.findByCylindersInd", query = "SELECT c FROM ClassRule c WHERE c.cylindersInd = :cylindersInd")
    , @NamedQuery(name = "ClassRule.findByFuelTypeInd", query = "SELECT c FROM ClassRule c WHERE c.fuelTypeInd = :fuelTypeInd")
    , @NamedQuery(name = "ClassRule.findByFuelDeliveryInd", query = "SELECT c FROM ClassRule c WHERE c.fuelDeliveryInd = :fuelDeliveryInd")
    , @NamedQuery(name = "ClassRule.findByDriveTypeInd", query = "SELECT c FROM ClassRule c WHERE c.driveTypeInd = :driveTypeInd")
    , @NamedQuery(name = "ClassRule.findByTonRatingInd", query = "SELECT c FROM ClassRule c WHERE c.tonRatingInd = :tonRatingInd")
    , @NamedQuery(name = "ClassRule.findByCountryOfOriginInd", query = "SELECT c FROM ClassRule c WHERE c.countryOfOriginInd = :countryOfOriginInd")
    , @NamedQuery(name = "ClassRule.findByCubicInchDisplacementInd", query = "SELECT c FROM ClassRule c WHERE c.cubicInchDisplacementInd = :cubicInchDisplacementInd")
    , @NamedQuery(name = "ClassRule.findByWeightRatingInd", query = "SELECT c FROM ClassRule c WHERE c.weightRatingInd = :weightRatingInd")
    , @NamedQuery(name = "ClassRule.findByLiterDisplacementInd", query = "SELECT c FROM ClassRule c WHERE c.literDisplacementInd = :literDisplacementInd")
    , @NamedQuery(name = "ClassRule.findByCubicCentimeterDisplacementInd", query = "SELECT c FROM ClassRule c WHERE c.cubicCentimeterDisplacementInd = :cubicCentimeterDisplacementInd")
    , @NamedQuery(name = "ClassRule.findByPurchasePriceInd", query = "SELECT c FROM ClassRule c WHERE c.purchasePriceInd = :purchasePriceInd")
    , @NamedQuery(name = "ClassRule.findByVinPartialInd", query = "SELECT c FROM ClassRule c WHERE c.vinPartialInd = :vinPartialInd")
    , @NamedQuery(name = "ClassRule.findByGVWRInd", query = "SELECT c FROM ClassRule c WHERE c.gVWRInd = :gVWRInd")
    , @NamedQuery(name = "ClassRule.findByClassPriorityInd", query = "SELECT c FROM ClassRule c WHERE c.classPriorityInd = :classPriorityInd")
    , @NamedQuery(name = "ClassRule.findByUpdateUserName", query = "SELECT c FROM ClassRule c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClassRule.findByUpdateLast", query = "SELECT c FROM ClassRule c WHERE c.updateLast = :updateLast")})
public class ClassRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "classRuleId")
    private Integer classRuleId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vehicleTypeInd")
    private boolean vehicleTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "makeInd")
    private boolean makeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modelInd")
    private boolean modelInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seriesInd")
    private boolean seriesInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modelYearInd")
    private boolean modelYearInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cylindersInd")
    private boolean cylindersInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fuelTypeInd")
    private boolean fuelTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fuelDeliveryInd")
    private boolean fuelDeliveryInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "driveTypeInd")
    private boolean driveTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tonRatingInd")
    private boolean tonRatingInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "countryOfOriginInd")
    private boolean countryOfOriginInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cubicInchDisplacementInd")
    private boolean cubicInchDisplacementInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "weightRatingInd")
    private boolean weightRatingInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "literDisplacementInd")
    private boolean literDisplacementInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cubicCentimeterDisplacementInd")
    private boolean cubicCentimeterDisplacementInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "purchasePriceInd")
    private boolean purchasePriceInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinPartialInd")
    private boolean vinPartialInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GVWRInd")
    private boolean gVWRInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "classPriorityInd")
    private int classPriorityInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "classRuleIdFk")
    private Collection<ClassItem> classItemCollection;
    @JoinColumn(name = "classTableIdFk", referencedColumnName = "classTableId")
    @ManyToOne
    private ClassTable classTableIdFk;

    public ClassRule() {
    }

    public ClassRule(Integer classRuleId) {
        this.classRuleId = classRuleId;
    }

    public ClassRule(Integer classRuleId, boolean vehicleTypeInd, boolean makeInd, boolean modelInd, boolean seriesInd, boolean modelYearInd, boolean cylindersInd, boolean fuelTypeInd, boolean fuelDeliveryInd, boolean driveTypeInd, boolean tonRatingInd, boolean countryOfOriginInd, boolean cubicInchDisplacementInd, boolean weightRatingInd, boolean literDisplacementInd, boolean cubicCentimeterDisplacementInd, boolean purchasePriceInd, boolean vinPartialInd, boolean gVWRInd, int classPriorityInd) {
        this.classRuleId = classRuleId;
        this.vehicleTypeInd = vehicleTypeInd;
        this.makeInd = makeInd;
        this.modelInd = modelInd;
        this.seriesInd = seriesInd;
        this.modelYearInd = modelYearInd;
        this.cylindersInd = cylindersInd;
        this.fuelTypeInd = fuelTypeInd;
        this.fuelDeliveryInd = fuelDeliveryInd;
        this.driveTypeInd = driveTypeInd;
        this.tonRatingInd = tonRatingInd;
        this.countryOfOriginInd = countryOfOriginInd;
        this.cubicInchDisplacementInd = cubicInchDisplacementInd;
        this.weightRatingInd = weightRatingInd;
        this.literDisplacementInd = literDisplacementInd;
        this.cubicCentimeterDisplacementInd = cubicCentimeterDisplacementInd;
        this.purchasePriceInd = purchasePriceInd;
        this.vinPartialInd = vinPartialInd;
        this.gVWRInd = gVWRInd;
        this.classPriorityInd = classPriorityInd;
    }

    public Integer getClassRuleId() {
        return classRuleId;
    }

    public void setClassRuleId(Integer classRuleId) {
        this.classRuleId = classRuleId;
    }

    public boolean getVehicleTypeInd() {
        return vehicleTypeInd;
    }

    public void setVehicleTypeInd(boolean vehicleTypeInd) {
        this.vehicleTypeInd = vehicleTypeInd;
    }

    public boolean getMakeInd() {
        return makeInd;
    }

    public void setMakeInd(boolean makeInd) {
        this.makeInd = makeInd;
    }

    public boolean getModelInd() {
        return modelInd;
    }

    public void setModelInd(boolean modelInd) {
        this.modelInd = modelInd;
    }

    public boolean getSeriesInd() {
        return seriesInd;
    }

    public void setSeriesInd(boolean seriesInd) {
        this.seriesInd = seriesInd;
    }

    public boolean getModelYearInd() {
        return modelYearInd;
    }

    public void setModelYearInd(boolean modelYearInd) {
        this.modelYearInd = modelYearInd;
    }

    public boolean getCylindersInd() {
        return cylindersInd;
    }

    public void setCylindersInd(boolean cylindersInd) {
        this.cylindersInd = cylindersInd;
    }

    public boolean getFuelTypeInd() {
        return fuelTypeInd;
    }

    public void setFuelTypeInd(boolean fuelTypeInd) {
        this.fuelTypeInd = fuelTypeInd;
    }

    public boolean getFuelDeliveryInd() {
        return fuelDeliveryInd;
    }

    public void setFuelDeliveryInd(boolean fuelDeliveryInd) {
        this.fuelDeliveryInd = fuelDeliveryInd;
    }

    public boolean getDriveTypeInd() {
        return driveTypeInd;
    }

    public void setDriveTypeInd(boolean driveTypeInd) {
        this.driveTypeInd = driveTypeInd;
    }

    public boolean getTonRatingInd() {
        return tonRatingInd;
    }

    public void setTonRatingInd(boolean tonRatingInd) {
        this.tonRatingInd = tonRatingInd;
    }

    public boolean getCountryOfOriginInd() {
        return countryOfOriginInd;
    }

    public void setCountryOfOriginInd(boolean countryOfOriginInd) {
        this.countryOfOriginInd = countryOfOriginInd;
    }

    public boolean getCubicInchDisplacementInd() {
        return cubicInchDisplacementInd;
    }

    public void setCubicInchDisplacementInd(boolean cubicInchDisplacementInd) {
        this.cubicInchDisplacementInd = cubicInchDisplacementInd;
    }

    public boolean getWeightRatingInd() {
        return weightRatingInd;
    }

    public void setWeightRatingInd(boolean weightRatingInd) {
        this.weightRatingInd = weightRatingInd;
    }

    public boolean getLiterDisplacementInd() {
        return literDisplacementInd;
    }

    public void setLiterDisplacementInd(boolean literDisplacementInd) {
        this.literDisplacementInd = literDisplacementInd;
    }

    public boolean getCubicCentimeterDisplacementInd() {
        return cubicCentimeterDisplacementInd;
    }

    public void setCubicCentimeterDisplacementInd(boolean cubicCentimeterDisplacementInd) {
        this.cubicCentimeterDisplacementInd = cubicCentimeterDisplacementInd;
    }

    public boolean getPurchasePriceInd() {
        return purchasePriceInd;
    }

    public void setPurchasePriceInd(boolean purchasePriceInd) {
        this.purchasePriceInd = purchasePriceInd;
    }

    public boolean getVinPartialInd() {
        return vinPartialInd;
    }

    public void setVinPartialInd(boolean vinPartialInd) {
        this.vinPartialInd = vinPartialInd;
    }

    public boolean getGVWRInd() {
        return gVWRInd;
    }

    public void setGVWRInd(boolean gVWRInd) {
        this.gVWRInd = gVWRInd;
    }

    public int getClassPriorityInd() {
        return classPriorityInd;
    }

    public void setClassPriorityInd(int classPriorityInd) {
        this.classPriorityInd = classPriorityInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    public ClassTable getClassTableIdFk() {
        return classTableIdFk;
    }

    public void setClassTableIdFk(ClassTable classTableIdFk) {
        this.classTableIdFk = classTableIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classRuleId != null ? classRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassRule)) {
            return false;
        }
        ClassRule other = (ClassRule) object;
        if ((this.classRuleId == null && other.classRuleId != null) || (this.classRuleId != null && !this.classRuleId.equals(other.classRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClassRule[ classRuleId=" + classRuleId + " ]";
    }
    
}
