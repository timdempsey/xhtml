/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */

public class ClaimNote implements Serializable {
    
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);
    

    public ClaimNote() {
    }

    public ClaimNote(Integer noteId, String note, String enteredBy, Date enteredDate) {
        this.noteId = noteId;
        this.note = note;
        this.enteredBy = enteredBy;
        this.enteredDate = enteredDate;
    }

    
    
    @Id
    @Column(name = "noteId", table = "Note")
    Integer noteId;
    @Column(name = "note", table = "Note")
    String note;
    String enteredBy;
    Date enteredDate;

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }
    
    
}
