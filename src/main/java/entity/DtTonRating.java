/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtTonRating")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtTonRating.findAll", query = "SELECT d FROM DtTonRating d")
    , @NamedQuery(name = "DtTonRating.findByTonRatingId", query = "SELECT d FROM DtTonRating d WHERE d.tonRatingId = :tonRatingId")
    , @NamedQuery(name = "DtTonRating.findByDescription", query = "SELECT d FROM DtTonRating d WHERE d.description = :description")})
public class DtTonRating implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "tonRatingId")
    private Integer tonRatingId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;

    public DtTonRating() {
    }

    public DtTonRating(Integer tonRatingId) {
        this.tonRatingId = tonRatingId;
    }

    public Integer getTonRatingId() {
        return tonRatingId;
    }

    public void setTonRatingId(Integer tonRatingId) {
        this.tonRatingId = tonRatingId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tonRatingId != null ? tonRatingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtTonRating)) {
            return false;
        }
        DtTonRating other = (DtTonRating) object;
        if ((this.tonRatingId == null && other.tonRatingId != null) || (this.tonRatingId != null && !this.tonRatingId.equals(other.tonRatingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtTonRating[ tonRatingId=" + tonRatingId + " ]";
    }
    
}
