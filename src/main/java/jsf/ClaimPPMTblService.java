/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.ContractPPMDetail;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimPPMTblService")
@ApplicationScoped
public class ClaimPPMTblService {

    private static final Logger LOGGER = LogManager.getLogger(ClaimPPMTblService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimPPMTblService
     */
    public ClaimPPMTblService() {
    }

    @Inject
    transient private ClaimMisc claimMisc;

    @Inject
    ClaimGeneralService claimGeneralService;

    private List<ClaimPPMTbl> ppmTblList;
    private List<ClaimPPMTbl> ppmavailableList;
    private List<ClaimPPMTbl> ppmusedList;
    private List<ClaimPPMTbl> ppmexpiredList;
    private List<ClaimPPMTbl> ppmupList;
    ClaimPPMTbl selectedPPMtbl;
    ClaimPPMTbl claimPPMtbl;
    private List<ClaimPPMTbl> selectedPPMavailableList;

    String claimNumber;
    String contractNo;

    String description;
    String lowerDate;
    String uppderDate;
    String andor;
    Integer lowerMile;
    Integer upperMile;
    BigDecimal cost;
    String onClaim;
    String onRO;

    Integer ppmodometer;

    public Integer getPpmodometer() {
        return ppmodometer;
    }

    public void setPpmodometer(Integer ppmodometer) {
        this.ppmodometer = ppmodometer;
    }

    public List<ClaimPPMTbl> getSelectedPPMavailableList() {
        return selectedPPMavailableList;
    }

    public void setSelectedPPMavailableList(List<ClaimPPMTbl> selectedPPMavailableList) {
        this.selectedPPMavailableList = selectedPPMavailableList;
    }

    public List<ClaimPPMTbl> getPpmavailableList() {
        return ppmavailableList;
    }

    public void setPpmavailableList(List<ClaimPPMTbl> ppmavailableList) {
        this.ppmavailableList = ppmavailableList;
    }

    public List<ClaimPPMTbl> getPpmusedList() {
        return ppmusedList;
    }

    public void setPpmusedList(List<ClaimPPMTbl> ppmusedList) {
        this.ppmusedList = ppmusedList;
    }

    public List<ClaimPPMTbl> getPpmexpiredList() {
        return ppmexpiredList;
    }

    public void setPpmexpiredList(List<ClaimPPMTbl> ppmexpiredList) {
        this.ppmexpiredList = ppmexpiredList;
    }

    public List<ClaimPPMTbl> getPpmupList() {
        return ppmupList;
    }

    public void setPpmupList(List<ClaimPPMTbl> ppmupList) {
        this.ppmupList = ppmupList;
    }

    public List<ClaimPPMTbl> getPpmTblList() {
        return ppmTblList;
    }

    public void setPpmTblList(List<ClaimPPMTbl> ppmTblList) {
        this.ppmTblList = ppmTblList;
    }

    public ClaimPPMTbl getSelectedPPMtbl() {
        return selectedPPMtbl;
    }

    public void setSelectedPPMtbl(ClaimPPMTbl selectedPPMtbl) {
        this.selectedPPMtbl = selectedPPMtbl;
    }

    public ClaimPPMTbl getClaimPPMtbl() {
        return claimPPMtbl;
    }

    public void setClaimPPMtbl(ClaimPPMTbl claimPPMtbl) {
        this.claimPPMtbl = claimPPMtbl;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLowerDate() {
        return lowerDate;
    }

    public void setLowerDate(String lowerDate) {
        this.lowerDate = lowerDate;
    }

    public String getUppderDate() {
        return uppderDate;
    }

    public void setUppderDate(String uppderDate) {
        this.uppderDate = uppderDate;
    }

    public Integer getLowerMile() {
        return lowerMile;
    }

    public void setLowerMile(Integer lowerMile) {
        this.lowerMile = lowerMile;
    }

    public Integer getUpperMile() {
        return upperMile;
    }

    public void setUpperMile(Integer upperMile) {
        this.upperMile = upperMile;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getOnClaim() {
        return onClaim;
    }

    public void setOnClaim(String onClaim) {
        this.onClaim = onClaim;
    }

    public String getOnRO() {
        return onRO;
    }

    public void setOnRO(String onRO) {
        this.onRO = onRO;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("init from RFSearchService");
        ppmTblList = new ArrayList<>();
        ppmavailableList = new ArrayList<>();
        ppmusedList = new ArrayList<>();
        ppmexpiredList = new ArrayList<>();
        ppmupList = new ArrayList<>();

        selectedPPMtbl = new ClaimPPMTbl();
        claimPPMtbl = new ClaimPPMTbl();
    }

    public void reset() {
        description = "";
        lowerDate = "";
        uppderDate = "";
        andor = "";
        lowerMile = null;
        upperMile = null;
        cost = null;
        onClaim = null;
        onRO = null;
    }

    public void createPPMTblView(String cno) {
        try {
            LOGGER.info("in createPPMTblView, cno=" + cno);

            ppmTblList.clear();

            ppmTblList = claimMisc.getPPMTblList(cno);
            LOGGER.info("size of ppmTblList=" + ppmTblList.size());

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void createPPMavailableView(String cno, Integer odometer) {
        try {
            LOGGER.info("in createPPMavailableView, odometer=" + odometer);
            LOGGER.info("cno=" + cno);

            ppmavailableList.clear();
            if (odometer > 0) {
                ppmTblList = claimMisc.getPPMTblList(cno);
                for (ClaimPPMTbl ppmLine : ppmTblList) {
                    if (ppmLine.getOnClaim() == null) {
                        if (ppmLine.getLowerMile() != null && ppmLine.getUpperMile() != null) {
                            if (ppmLine.getLowerMile() <= odometer && ppmLine.getUpperMile() >= odometer || (ppmLine.getLowerMile() != null && ppmLine.getLowerDate().equalsIgnoreCase(claimMisc.getJavaDate(claimMisc.getCurrentDate())))) {
                                ppmavailableList.add(ppmLine);
                            }
                        } else {  // ppm with pre-paid maintenance, no date and mile specified
                            ppmavailableList.add(ppmLine);
                        }
                    }
                }
                LOGGER.info("size of ppmavailableList=" + ppmavailableList.size());
            }

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void createPPMusedView(String cno) {
        try {
            LOGGER.info("in createPPMusedView, cno=" + cno);

            ppmusedList.clear();

            ppmTblList = claimMisc.getPPMTblList(cno);
            for (ClaimPPMTbl ppmLine : ppmTblList) {
                if (ppmLine.getOnClaim() != null && ppmLine.getOnClaim().length() > 0) {
                    ppmusedList.add(ppmLine);
                }
            }
            LOGGER.info("size of ppmusedList=" + ppmusedList.size());

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void createPPMexpiredView(String cno, Integer odometer) {
        try {
            LOGGER.info("in createPPMexpiredView, odometer=" + odometer);

            ppmexpiredList.clear();

            if (odometer > 0) {
                ppmTblList = claimMisc.getPPMTblList(cno);
                for (ClaimPPMTbl ppmLine : ppmTblList) {
                    if (ppmLine.getOnClaim() == null && ppmLine.getUpperMile() < odometer) {
                        ppmexpiredList.add(ppmLine);
                    }
                }
                LOGGER.info("size of ppmexpiredList=" + ppmexpiredList.size());
            }

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void createPPMupView(String cno, Integer odometer) {
        try {
            LOGGER.info("in createPPMupView, odometer=" + odometer);

            ppmupList.clear();

            if (odometer > 0) {
                ppmTblList = claimMisc.getPPMTblList(cno);
                for (ClaimPPMTbl ppmLine : ppmTblList) {
                    if (ppmLine.getOnClaim() == null && ppmLine.getLowerMile() >= odometer) {
                        ppmupList.add(ppmLine);
                    }
                }
                LOGGER.info("size of ppmupList=" + ppmupList.size());
            }

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void savePPMavailable() {
        try {
            utx.begin();
            LOGGER.info("in savePPMavailable, claimNumber=" + claimNumber);
            if (claimNumber != null && claimNumber.length() > 0) {
                BigDecimal totalCost = new BigDecimal(0);
                ContractPPMDetail cppmd = null;
                Claim claim = claimMisc.getClaimByNumber(claimNumber);

                for (ClaimPPMTbl availableLine : selectedPPMavailableList) {
                    LOGGER.info("contractPPMDetailId=" + availableLine.getContractPPMDetailId());
                    LOGGER.info("cost=" + availableLine.getCost());
                    totalCost = totalCost.add(availableLine.getCost());

                    //1/3, update ContractPPMDetail for cost in case modifed from GUI
                    cppmd = claimMisc.getContractPPMDetailById(availableLine.getContractPPMDetailId());
                    LOGGER.info("original cost=" + cppmd.getCost());
                    cppmd.setCost(availableLine.getCost());
                    em.merge(cppmd);

                    // 2/3, insert new record in ClaimEventPPM              
                    LOGGER.info("1=" + claim.getClaimId());
                    LOGGER.info("2=" + cppmd.getContractPPMDetailId());
                    Query query = em.createNativeQuery("insert into ClaimEventPPM (claimIdFk, contractPPMDetailIdFk ) values (?1, ?2)");
                    query.setParameter(1, claim.getClaimId());
                    query.setParameter(2, cppmd.getContractPPMDetailId());
                    query.executeUpdate();
                }
                // 3/3, update Claim for amount
                LOGGER.info("totalCost=" + totalCost);
                claim.setAmount(totalCost);
                em.merge(claim);
                //claimMisc.updateClaimByAmount(claimNumber, totalCost);

                utx.commit();

                createPPMavailableView(contractNo, claimGeneralService.getPpmodometer());
            }
            LOGGER.info("end of savePPMavailable");

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }
}
