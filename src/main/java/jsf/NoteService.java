/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Dealer;
import entity.DealerGroup;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "noteService")
@ApplicationScoped
public class NoteService {
    
    private static final Logger LOGGER = LogManager.getLogger(NoteService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of NoteService
     */
    public NoteService() {
    }
    
    List<Notes> notesList;
    
    private Dealer dealer;
    private DealerGroup dealerGroup;

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public DealerGroup getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(DealerGroup dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    public List<Notes> getNotesList() {
        return notesList;
    }

    public void setNotesList(List<Notes> notesList) {
        this.notesList = notesList;
    }
    
    public void reset() {
        dealerGroup = null;
        dealer = null;
    }
    
    @PostConstruct
    public void init() {
        dealerGroup = null;
        dealer = null;
    }
    
    /*
    * methods
    */
    
    public void generateNotesListByDealer(Dealer dealer) {
        LOGGER.info("in generateNotesListByDealer, dealerId=" + dealer.getDealerId());
        notesList = claimMisc.getNotesByDealerId(dealer.getDealerId());
    }
    
    public void generateNotesListByDG(DealerGroup dg) {
        LOGGER.info("in generateNotesListByDG, id=" + dg.getDealerGroupId());
        notesList = claimMisc.getNotesByDGId(dg.getDealerGroupId());
    }
    
    
    
    
    
}
