/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.ClaimDetail;
import entity.ClaimDetailItem;
import entity.ClaimDetailSub;
import entity.ClaimLabor;
import entity.ClaimPart;
import entity.RepairFacility;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimDTSService")
@ApplicationScoped
public class ClaimDTSService {

    //private static final Logger LOGGER = LogManager.getLogger(ClaimDTSService.class);
    private static final Logger LOGGER = LogManager.getLogger(ClaimDTSService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimDTSService
     */
    public ClaimDTSService() {
    }

    private String claimNumber;
    private Claim claim;
    
    private ClaimDts claimDts;
    
    ClaimDTSLine selectedLine;
    ClaimDTSLabor selectedLabor;
    ClaimDTSPart selectedPart;

    ClaimDTSLine lineIns;
    ClaimDTSLabor laborIns;
    ClaimDTSPart partIns;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public ClaimDts getClaimDts() {
        return claimDts;
    }

    public void setClaimDts(ClaimDts claimDts) {
        this.claimDts = claimDts;
    }

    public ClaimDTSLine getSelectedLine() {
        return selectedLine;
    }

    public void setSelectedLine(ClaimDTSLine selectedLine) {
        this.selectedLine = selectedLine;
    }

    public ClaimDTSLabor getSelectedLabor() {
        return selectedLabor;
    }

    public void setSelectedLabor(ClaimDTSLabor selectedLabor) {
        this.selectedLabor = selectedLabor;
    }

    public ClaimDTSPart getSelectedPart() {
        return selectedPart;
    }

    public void setSelectedPart(ClaimDTSPart selectedPart) {
        this.selectedPart = selectedPart;
    }

    public ClaimDTSLine getLineIns() {
        return lineIns;
    }

    public void setLineIns(ClaimDTSLine lineIns) {
        this.lineIns = lineIns;
    }

    public ClaimDTSLabor getLaborIns() {
        return laborIns;
    }

    public void setLaborIns(ClaimDTSLabor laborIns) {
        this.laborIns = laborIns;
    }

    public ClaimDTSPart getPartIns() {
        return partIns;
    }

    public void setPartIns(ClaimDTSPart partIns) {
        this.partIns = partIns;
    }
    
     @PostConstruct
    public void init() {
        LOGGER.info("init from ClaimDTSService="+claimNumber);
        //claimDts = createClaimDtsView(claimNumber);
        reset();
      
    }

    public void reset() {
        RequestContext rc = RequestContext.getCurrentInstance();
        /*
        rc.reset("form:claimDetailPanel");
        rc.reset("form:claimLaborPanel");
        rc.reset("form:claimPartPanel");
        */
        selectedLine = new ClaimDTSLine();
        selectedLabor = new ClaimDTSLabor();
        lineIns = new ClaimDTSLine();
        laborIns = new ClaimDTSLabor();
        partIns = new ClaimDTSPart();
    }

    public void chooseClaimDetail(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("chooseClaimDetail=" + claimNumber);

        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
        
        claimDts = createClaimDtsView(claimNumber);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);

        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);
        LOGGER.info("claim id=" + claim.getClaimId());
        RequestContext.getCurrentInstance().openDialog("claimDetail", options, null);
    }

    public ClaimDts createClaimDtsView(String claimNumber) {
        LOGGER.info("in createClaimDtsView, claimNumber="+claimNumber);
        /*
        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
        LOGGER.info("claim=" + claim.getClaimNumber());
        */
        List<ClaimPart> cpList = new ArrayList<>();
        List<ClaimDetailItem> cdiList = new ArrayList<>();

        List<ClaimDTSLine> list = new ArrayList<>();

        //List<ClaimDetailSub> lcds = claimMisc.getClaimDetailSub(claimNumber);
        List<ClaimDetail> lcd = claimMisc.getClaimDetail(claimNumber);

        RepairFacility rf = claimMisc.getRepairFacilityByClaimNumber(claimNumber);
        BigDecimal laborTax = null;
        BigDecimal laborRate = null;
        BigDecimal partsTaxRate = null;
        if (rf != null) {
            laborTax = rf.getLaborTaxPct();
            laborRate = rf.getLaborRate();
            partsTaxRate = rf.getPartsTaxPct();
        }

        //for ( ClaimDetailSub cds : lcds) {
        for (ClaimDetail cd : lcd) {   // ClaimTopLine
            List<ClaimDTSLabor> cdlList = new ArrayList<>();

            int cdId = cd.getClaimDetailId();
            LOGGER.info("ClaimDetailId=" + cdId);

            cdiList = claimMisc.getClaimDetailItem(cdId);
            for (ClaimDetailItem cdi : cdiList) {   // ClaimLabor

                List<ClaimDTSPart> cdpList = new ArrayList<>();

                int cdiId = cdi.getDetailItemId();
                LOGGER.info("cdiId=" + cdiId);
                LOGGER.info("detailItemId=" + cdi.getDetailItemId());
                /*
                LOGGER.info("Labor Description=" + cdi.getDescription());
                LOGGER.info("approval=" + cdi.getApprovalStatus());
                LOGGER.info("claimDetailIdFK=" + cdi.getClaimDetailIdFk());
                LOGGER.info("claimLaborIdFk" + cdi.getClaimLaborIdFk());
                 */
                ClaimLabor cl = claimMisc.getClaimLaborByItemId(cdiId);

                cpList = claimMisc.getClaimPartByItemId(cdiId);
                LOGGER.info("=========================id=" + cl.getClaimLaborId());
                BigDecimal laborSubTotal = new BigDecimal(0);
                if (cpList != null) {
                    for (ClaimPart cp : cpList) {
                        int cpId = cp.getClaimPartId();
                        LOGGER.info("cpId=" + cpId);

                        BigDecimal partSubTotal = cp.getPartCost().multiply(new BigDecimal(cp.getPartQty()));
                        LOGGER.info("partSubTotal=" + partSubTotal);
                        if (partsTaxRate != null) {
                            BigDecimal partTax = partSubTotal.multiply(partsTaxRate.multiply(new BigDecimal(0.01)));
                            partSubTotal = partSubTotal.add(partTax);
                        }
                        cdpList.add(new ClaimDTSPart(cpId, cp.getUpdateUserName(), cp.getUpdateLast()!=null?cp.getUpdateLast().toString():null, cp.getPartQty(), cp.getPartNumber(), cp.getPartDesc(), cp.getPartCost(), partSubTotal));
                    }
                }

                laborSubTotal = cl.getLaborTime().multiply(cl.getLaborRate());
                if (laborTax != null) {
                    BigDecimal lt = laborSubTotal.multiply(laborTax.multiply(new BigDecimal(0.01)));
                    laborSubTotal = laborSubTotal.add(lt);
                }
                
                //LOGGER.info("cdiId=" + cdiId);
                /*
                LOGGER.info("getUpdateUserName=" + cl.getUpdateUserName());
                LOGGER.info("getUpdateLast=" + cl.getUpdateLast());
                LOGGER.info("getLaborTime=" + cl.getLaborTime());
                LOGGER.info("getSrtCodeLaborOp=" + cdi.getSrtCodeLaborOp());
                LOGGER.info("getDescription=" + cdi.getDescription());
                LOGGER.info("getLaborRate=" + cl.getLaborRate());
                LOGGER.info("laborSubTotal=" + laborSubTotal);
                */
                
                cdlList.add(new ClaimDTSLabor(cdiId, cl.getUpdateUserName(), cl.getUpdateLast()!=null?cl.getUpdateLast().toString():null, cl.getLaborTime(), cdi.getSrtCodeLaborOp(), cdi.getDescription(), cl.getLaborRate(), laborSubTotal, cdpList));
                LOGGER.info("end of ClaimDetailItem");
            }

            ClaimDetailSub cds = claimMisc.getClaimDetailSub(cdId);
                        list.add(new ClaimDTSLine(cdId, cds.getComplaintReason(), cds.getCause(), cds.getCorrection(), cdlList));

            LOGGER.info("end of ClaimDetail");
        }

        if( claim == null) {
            LOGGER.info("claim is null");
        }
        return new ClaimDts(claim.getClaimId(), claim.getClaimNumber(), list);

    }

    public void saveLine() {
        try {
            LOGGER.info("in saveLine");
            utx.begin();

            LOGGER.info("claimNumber=" + claimDts.claimNumber);
            LOGGER.info("claimId=" + claimDts.claimId);
            Query query = em.createNativeQuery("insert into ClaimDetailSub (complaintReason, cause, correction) values (?1, ?2, ?3)");

            query.setParameter(1, lineIns.complaintReason);
            query.setParameter(2, lineIns.cause);
            query.setParameter(3, lineIns.correction);
            int ret = query.executeUpdate();

            Integer claimDetailSubIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('ClaimDetailSub')").getSingleResult()).intValueExact();
            LOGGER.info("claimDetailSubIdFk=" + claimDetailSubIdFk);
            query = em.createNativeQuery("insert into ClaimDetail (claimIdFk, claimDetailSubIdFk) values (?1, ?2)");
            query.setParameter(1, claimDts.claimId);
            query.setParameter(2, claimDetailSubIdFk);
            ret = query.executeUpdate();

            utx.commit();

            LOGGER.info("regenerate the view from saveLine");
            claimDts = createClaimDtsView(claimNumber);

            if (lineIns != null) {
                lineIns.reset();
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void updateLine() {
        try {
            LOGGER.info("in updateLine");
            LOGGER.info("complaint from updateLine=" + lineIns.getComplaintReason());
            LOGGER.info("complaint from updateLine=" + selectedLine.getComplaintReason());
            utx.begin();
            Integer claimDetailId = lineIns.getClaimDetailId();
            if (claimDetailId != null) {
                LOGGER.info("claimDetailId=" + claimDetailId);
                ClaimDetailSub cds = claimMisc.getClaimDetailSub(claimDetailId);
                cds.setComplaintReason(lineIns.getComplaintReason());
                cds.setCause(lineIns.getCause());
                cds.setCorrection(lineIns.getCorrection());
                em.merge(cds);
                LOGGER.info("merge ClaimDetailSub");
            } else {
                LOGGER.info("no compalint has been changed.");
            }

            utx.commit();
            LOGGER.info("regenerate the view from updateLine");
            claimDts = createClaimDtsView(claimNumber);
            reset();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deleteLine() {
        try {
            LOGGER.info("in deleteLine");
            LOGGER.info("complaint from deleteLine=" + lineIns.getComplaintReason());

            utx.begin();
            Integer claimDetailId = lineIns.getClaimDetailId();

            if (claimDetailId != null) {
                LOGGER.info("claimDetailId=" + claimDetailId);

                ClaimDetail cd = em.createNamedQuery("ClaimDetail.findByClaimDetailId", ClaimDetail.class).setParameter("claimDetailId", claimDetailId).getSingleResult();

                List<ClaimDetailItem> cdiList = claimMisc.getClaimDetailItem(claimDetailId);
                if (cdiList != null) {
                    for (ClaimDetailItem cdi : cdiList) {
                        ClaimLabor cl = claimMisc.getClaimLaborByItemId(cdi.getDetailItemId());

                        List<ClaimPart> cpList = claimMisc.getClaimPartByItemId(cdi.getDetailItemId());
                        if (cpList != null) {
                            for (ClaimPart cp : cpList) {
                                // remove ClaimPart
                                em.remove(cp);
                            }
                        }
                        // remove ClaimDetailItem
                        em.remove(cdi);

                        // remove ClaimLabor
                        em.remove(cl);
                    }
                }
                // remove ClaimDetail
                em.remove(cd);

                ClaimDetailSub cds = claimMisc.getClaimDetailSub(claimDetailId);
                if (cds != null) {
                    // remove ClaimDetailSub
                    em.remove(cds);
                }

            } else {
                LOGGER.info("no compalint has been selected.");
            }

            utx.commit();
            LOGGER.info("regenerate the view from deleteLine");
            claimDts = createClaimDtsView(claimNumber);
            if (lineIns != null) {
                lineIns.reset();
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveLabor() {
        try {
            LOGGER.info("in saveLabor");
            utx.begin();

            LOGGER.info("claimDetailId=" + lineIns.claimDetailId);
            LOGGER.info("laborTime=" + laborIns.getLaborTime());
            LOGGER.info("laborRate=" + laborIns.getLaborRate());
            LOGGER.info("laborDesc=" + laborIns.getLaborDesc());

            Query query = em.createNativeQuery("insert into ClaimLabor (laborTime, laborRate) values (?1, ?2)");

            query.setParameter(1, laborIns.getLaborTime());
            query.setParameter(2, laborIns.getLaborRate());

            int ret = query.executeUpdate();
            LOGGER.info("ret of inserting ClaimLabor=" + ret);

            Integer claimLaborIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('ClaimLabor')").getSingleResult()).intValueExact();
            LOGGER.info("claimLaborIdFk=" + claimLaborIdFk);

            query = em.createNativeQuery("insert into ClaimDetailItem (description, srtCodeLaborOp, claimDetailIdFk, claimLaborIdFk) values (?1, ?2, ?3, ?4)");
            query.setParameter(1, laborIns.getLaborDesc());
            query.setParameter(2, laborIns.getSrtCodeLaborOp());
            query.setParameter(3, lineIns.claimDetailId);
            query.setParameter(4, claimLaborIdFk);
            ret = query.executeUpdate();

            LOGGER.info("ret of inserting ClaimDetailItem=" + ret);

            utx.commit();
            LOGGER.info("regenerate view from svaeLabor");
            claimDts = createClaimDtsView(claimNumber);
            if (laborIns != null) {
                laborIns.reset();
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void updateLabor() {
        try {
            LOGGER.info("in updateLabor");
            LOGGER.info("detailItemId=" + laborIns.getDetailItemId());

            utx.begin();
            Integer detailItemId = laborIns.getDetailItemId();
            if (detailItemId != null) {

                ClaimLabor cl = claimMisc.getClaimLaborByItemId(detailItemId);
                if (cl != null) {
                    LOGGER.info("merge ClaimLabor");
                    cl.setLaborRate(laborIns.getLaborRate());
                    cl.setLaborTime(laborIns.getLaborTime());
                    em.merge(cl);
                }

                ClaimDetailItem cdi = em.createNamedQuery("ClaimDetailItem.findByDetailItemId", ClaimDetailItem.class).setParameter("detailItemId", detailItemId).getSingleResult();
                if (cdi != null) {
                    LOGGER.info("merge ClaimDetailSub");
                    cdi.setSrtCodeLaborOp(laborIns.getSrtCodeLaborOp());
                    cdi.setDescription(laborIns.getLaborDesc());
                    em.merge(cdi);
                }
            } else {
                LOGGER.info("no compalint has been changed.");
            }

            utx.commit();
            LOGGER.info("regenerate the view from updateLabor");
            claimDts = createClaimDtsView(claimNumber);
            laborIns.reset();
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deleteLabor() {
        try {
            LOGGER.info("in deleteLabor");
            LOGGER.info("detailItemId from deleteLabor=" + laborIns.getDetailItemId());

            utx.begin();
            Integer detailItemId = laborIns.getDetailItemId();

            if (detailItemId != null) {

                ClaimLabor cl = claimMisc.getClaimLaborByItemId(detailItemId);

                ClaimDetailItem cdi = em.createNamedQuery("ClaimDetailItem.findByDetailItemId", ClaimDetailItem.class).setParameter("detailItemId", detailItemId).getSingleResult();

                List<ClaimPart> cpList = claimMisc.getClaimPartByItemId(detailItemId);
                if (cpList != null) {
                    for (ClaimPart cp : cpList) {
                        em.remove(cp);
                    }
                }

                em.remove(cdi);
                em.remove(cl);
            } else {
                LOGGER.info("no labor has been selected.");
            }

            utx.commit();
            LOGGER.info("regenerate the view from deleteLabor");
            claimDts = createClaimDtsView(claimNumber);
            if (laborIns != null) {
                laborIns.reset();
            }

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void savePart() {
        try {

            utx.begin();

            LOGGER.info("detailItemId=" + laborIns.getDetailItemId());
            LOGGER.info("Quantity=" + partIns.getPartQty());
            LOGGER.info("Part Number=" + partIns.getPartNumber());
            LOGGER.info("Part Description=" + partIns.getPartDesc());
            LOGGER.info("Part Price=" + partIns.getPartCost());

            Query query = em.createNativeQuery("insert into ClaimPart (partQty, partNumber, partDesc, partCost, detailItemIdFk) values (?1, ?2, ?3, ?4, ?5)");

            query.setParameter(1, partIns.getPartQty());
            query.setParameter(2, partIns.getPartNumber());
            query.setParameter(3, partIns.getPartDesc());
            query.setParameter(4, partIns.getPartCost());
            query.setParameter(5, laborIns.getDetailItemId());

            int ret = query.executeUpdate();

            utx.commit();
            LOGGER.info("regenerate view from svaePart");
            claimDts = createClaimDtsView(claimNumber);
            if (partIns != null) {
                partIns.reset();
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void updatePart() {
        try {
            LOGGER.info("in updatePart");
            LOGGER.info("claimPartId=" + partIns.getClaimPartId());

            if (partIns != null) {
                LOGGER.info("in updatePart");
                LOGGER.info("claimPartId from updatePart=" + partIns.getClaimPartId());

                utx.begin();

                ClaimPart cp = em.createNamedQuery("ClaimPart.findByClaimPartId", ClaimPart.class).setParameter("claimPartId", partIns.getClaimPartId()).getSingleResult();

                cp.setPartQty(partIns.getPartQty());
                cp.setPartNumber(partIns.getPartNumber());
                cp.setPartDesc(partIns.getPartDesc());
                cp.setPartCost(partIns.getPartCost());
                
                em.merge(cp);
                
                utx.commit();
                LOGGER.info("regenerate the view from updatePart");
                claimDts = createClaimDtsView(claimNumber);

                partIns.reset();

            } else {
                LOGGER.info("no ClaossPart has been selected.");
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void deletePart() {
        try {
            if (partIns != null) {
                LOGGER.info("in deletePart");
                LOGGER.info("claimPartId from deletePart=" + partIns.getClaimPartId());

                utx.begin();

                ClaimPart cp = em.createNamedQuery("ClaimPart.findByClaimPartId", ClaimPart.class).setParameter("claimPartId", partIns.getClaimPartId()).getSingleResult();
                if (cp != null) {
                    em.remove(cp);
                }

                utx.commit();
                LOGGER.info("regenerate the view from deletePart");
                claimDts = createClaimDtsView(claimNumber);
                if (partIns != null) {
                    partIns.reset();
                }
            } else {
                LOGGER.info("no ClaimPart has been selected.");
            }

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist...");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveLaborPart() {
        try {

            utx.begin();

            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveClaim() {
        try {

            utx.begin();

            // added code here
            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void onRowEdit(RowEditEvent event) {
        selectedLine = (ClaimDTSLine) event.getObject();

        lineIns = selectedLine;
        FacesMessage msg = new FacesMessage("Line Selected", ((ClaimDTSLine) event.getObject()).getClaimDetailId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onLineRowSelect(SelectEvent event) {
        LOGGER.info("selected" + ((ClaimDTSLine) event.getObject()).getClaimDetailId().toString());
        selectedLine = (ClaimDTSLine) event.getObject();

        lineIns = selectedLine;
        if (laborIns != null) {
            laborIns.reset();
        }
        if (partIns != null) {
            partIns.reset();
        }
        claimDts = createClaimDtsView(claimNumber);

        FacesMessage msg = new FacesMessage("Line Selected", ((ClaimDTSLine) event.getObject()).getClaimDetailId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onLineRowUnselect(UnselectEvent event) {

        if (lineIns != null) {
            lineIns.reset();
        }
        if (laborIns != null) {
            laborIns.reset();
        }
        if (partIns != null) {
            partIns.reset();
        }
        LOGGER.info("unselected" + ((ClaimDTSLine) event.getObject()).getClaimDetailId().toString());
        FacesMessage msg = new FacesMessage("Line Unselected", ((ClaimDTSLine) event.getObject()).getClaimDetailId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onLaborRowSelect(SelectEvent event) {
        selectedLabor = (ClaimDTSLabor) event.getObject();
        laborIns = selectedLabor;
        FacesMessage msg = new FacesMessage("Labor Selected", ((ClaimDTSLabor) event.getObject()).getDetailItemId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onLaborRowUnselect(UnselectEvent event) {

        if (laborIns != null) {
            laborIns.reset();
        }
        if (partIns != null) {
            partIns.reset();
        }

        FacesMessage msg = new FacesMessage("Labor Unselected", ((ClaimDTSLabor) event.getObject()).getDetailItemId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onPartRowSelect(SelectEvent event) {
        selectedPart = (ClaimDTSPart) event.getObject();
        partIns = selectedPart;
        FacesMessage msg = new FacesMessage("Part Selected", ((ClaimDTSPart) event.getObject()).getClaimPartId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onPartRowUnselect(UnselectEvent event) {

        if (partIns != null) {
            partIns.reset();
        }

        FacesMessage msg = new FacesMessage("Part Unselected", ((ClaimDTSPart) event.getObject()).getClaimPartId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }
    
    public void searchPN(ActionEvent event) {
        
        String partNumber = (String) event.getComponent().getAttributes().get("partNumber");
        LOGGER.info("searchPN, partNumber=" + partNumber);
        //contractNo = (String) event.getComponent().getAttributes().get("contractNo");

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("searchPN", options, null);
    }
    
    public void searchPD(ActionEvent event) {
        
        String partDesc = (String) event.getComponent().getAttributes().get("partDesc");
        LOGGER.info("searchPD, partDesc=" + partDesc);
        //contractNo = (String) event.getComponent().getAttributes().get("contractNo");

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("searchPD", options, null);
    }

    
}
