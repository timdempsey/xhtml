/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfManufacturerWarranty")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfManufacturerWarranty.findAll", query = "SELECT c FROM CfManufacturerWarranty c")
    , @NamedQuery(name = "CfManufacturerWarranty.findByManufacturerWarrantyId", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.manufacturerWarrantyId = :manufacturerWarrantyId")
    , @NamedQuery(name = "CfManufacturerWarranty.findByWarrantyTypeInd", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.warrantyTypeInd = :warrantyTypeInd")
    , @NamedQuery(name = "CfManufacturerWarranty.findByManufacturerWarrantyLevelInd", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.manufacturerWarrantyLevelInd = :manufacturerWarrantyLevelInd")
    , @NamedQuery(name = "CfManufacturerWarranty.findByYear", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.year = :year")
    , @NamedQuery(name = "CfManufacturerWarranty.findByVehicleTypeInd", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.vehicleTypeInd = :vehicleTypeInd")
    , @NamedQuery(name = "CfManufacturerWarranty.findByMonths", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.months = :months")
    , @NamedQuery(name = "CfManufacturerWarranty.findByMiles", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.miles = :miles")
    , @NamedQuery(name = "CfManufacturerWarranty.findByUpdateUserName", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfManufacturerWarranty.findByUpdateLast", query = "SELECT c FROM CfManufacturerWarranty c WHERE c.updateLast = :updateLast")})
public class CfManufacturerWarranty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "manufacturerWarrantyId")
    private Integer manufacturerWarrantyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "warrantyTypeInd")
    private int warrantyTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "manufacturerWarrantyLevelInd")
    private int manufacturerWarrantyLevelInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vehicleTypeInd")
    private int vehicleTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "months")
    private int months;
    @Basic(optional = false)
    @NotNull
    @Column(name = "miles")
    private int miles;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "fuelTypeIdFk", referencedColumnName = "fuelTypeId")
    @ManyToOne
    private FuelType fuelTypeIdFk;
    @JoinColumn(name = "makeIdFk", referencedColumnName = "makeId")
    @ManyToOne(optional = false)
    private Make makeIdFk;
    @JoinColumn(name = "modelIdFk", referencedColumnName = "ModelId")
    @ManyToOne(optional = false)
    private Model modelIdFk;

    public CfManufacturerWarranty() {
    }

    public CfManufacturerWarranty(Integer manufacturerWarrantyId) {
        this.manufacturerWarrantyId = manufacturerWarrantyId;
    }

    public CfManufacturerWarranty(Integer manufacturerWarrantyId, int warrantyTypeInd, int manufacturerWarrantyLevelInd, int year, int vehicleTypeInd, int months, int miles) {
        this.manufacturerWarrantyId = manufacturerWarrantyId;
        this.warrantyTypeInd = warrantyTypeInd;
        this.manufacturerWarrantyLevelInd = manufacturerWarrantyLevelInd;
        this.year = year;
        this.vehicleTypeInd = vehicleTypeInd;
        this.months = months;
        this.miles = miles;
    }

    public Integer getManufacturerWarrantyId() {
        return manufacturerWarrantyId;
    }

    public void setManufacturerWarrantyId(Integer manufacturerWarrantyId) {
        this.manufacturerWarrantyId = manufacturerWarrantyId;
    }

    public int getWarrantyTypeInd() {
        return warrantyTypeInd;
    }

    public void setWarrantyTypeInd(int warrantyTypeInd) {
        this.warrantyTypeInd = warrantyTypeInd;
    }

    public int getManufacturerWarrantyLevelInd() {
        return manufacturerWarrantyLevelInd;
    }

    public void setManufacturerWarrantyLevelInd(int manufacturerWarrantyLevelInd) {
        this.manufacturerWarrantyLevelInd = manufacturerWarrantyLevelInd;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getVehicleTypeInd() {
        return vehicleTypeInd;
    }

    public void setVehicleTypeInd(int vehicleTypeInd) {
        this.vehicleTypeInd = vehicleTypeInd;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public int getMiles() {
        return miles;
    }

    public void setMiles(int miles) {
        this.miles = miles;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public FuelType getFuelTypeIdFk() {
        return fuelTypeIdFk;
    }

    public void setFuelTypeIdFk(FuelType fuelTypeIdFk) {
        this.fuelTypeIdFk = fuelTypeIdFk;
    }

    public Make getMakeIdFk() {
        return makeIdFk;
    }

    public void setMakeIdFk(Make makeIdFk) {
        this.makeIdFk = makeIdFk;
    }

    public Model getModelIdFk() {
        return modelIdFk;
    }

    public void setModelIdFk(Model modelIdFk) {
        this.modelIdFk = modelIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (manufacturerWarrantyId != null ? manufacturerWarrantyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfManufacturerWarranty)) {
            return false;
        }
        CfManufacturerWarranty other = (CfManufacturerWarranty) object;
        if ((this.manufacturerWarrantyId == null && other.manufacturerWarrantyId != null) || (this.manufacturerWarrantyId != null && !this.manufacturerWarrantyId.equals(other.manufacturerWarrantyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfManufacturerWarranty[ manufacturerWarrantyId=" + manufacturerWarrantyId + " ]";
    }
    
}
