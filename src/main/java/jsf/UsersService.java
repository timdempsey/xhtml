/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Address;
import entity.CfRegion;
import entity.UserMember;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "usersService")
@ApplicationScoped
public class UsersService {

    private static final Logger LOGGER = LogManager.getLogger(UsersService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of UsersService
     */
    public UsersService() {

        selectedUser = new Users();
        stateMap = new TreeMap<>();
        //sameAsPaddr = true;
        userMemberId = null;
        user = new Users();
    }

    List<Users> usersList;

    Integer userMemberId;
    Users selectedUser;
    Users user;

    private String regionCodeP;
    private String regionCodeB;
    private TreeMap<String, String> stateMap;

    private String userGLCode;
    private String departmentName;
    private Boolean sameAsPhyAddr;

    public Boolean getSameAsPhyAddr() {
        return sameAsPhyAddr;
    }

    public void setSameAsPhyAddr(Boolean sameAsPhyAddr) {
        this.sameAsPhyAddr = sameAsPhyAddr;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getUserGLCode() {
        return userGLCode;
    }

    public void setUserGLCode(String userGLCode) {
        this.userGLCode = userGLCode;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public TreeMap<String, String> getStateMap() {
        return stateMap;
    }

    public void setStateMap(TreeMap<String, String> stateMap) {
        this.stateMap = stateMap;
    }

    public String getRegionCodeP() {
        return regionCodeP;
    }

    public void setRegionCodeP(String regionCodeP) {
        this.regionCodeP = regionCodeP;
    }

    public String getRegionCodeB() {
        return regionCodeB;
    }

    public void setRegionCodeB(String regionCodeB) {
        this.regionCodeB = regionCodeB;
    }

    public Integer getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(Integer userMemberId) {
        this.userMemberId = userMemberId;
    }

    public Users getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(Users selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @PostConstruct
    public void init() {
        loadStateMap();
    }

    public void loadStateMap() {
        stateMap.put("AK", "AK");
        stateMap.put("AL", "AL");
        stateMap.put("AR", "AR");
        stateMap.put("AZ", "AZ");
        stateMap.put("CA", "CA");
        stateMap.put("CO", "CO");
        stateMap.put("CT", "CT");
        stateMap.put("DE", "DE");
        stateMap.put("FL", "FL");
        stateMap.put("GA", "GA");
        stateMap.put("HI", "HI");
        stateMap.put("IA", "IA");
        stateMap.put("ID", "ID");
        stateMap.put("IL", "IL");
        stateMap.put("IN", "IN");
        stateMap.put("IA", "IA");
        stateMap.put("KS", "KS");
        stateMap.put("KY", "KY");
        stateMap.put("LA", "LA");
        stateMap.put("ME", "ME");
        stateMap.put("MD", "MD");
        stateMap.put("MA", "MA");
        stateMap.put("MI", "MI");
        stateMap.put("MN", "MN");
        stateMap.put("MS", "MS");
        stateMap.put("MO", "MO");
        stateMap.put("MT", "MT");
        stateMap.put("NE", "NE");
        stateMap.put("NV", "NV");
        stateMap.put("NH", "NH");
        stateMap.put("NJ", "NJ");
        stateMap.put("NM", "NM");
        stateMap.put("NY", "NY");
        stateMap.put("NC", "NC");
        stateMap.put("ND", "ND");
        stateMap.put("OH", "OH");
        stateMap.put("OK", "OK");
        stateMap.put("OR", "OR");
        stateMap.put("PA", "PA");
        stateMap.put("RI", "RI");
        stateMap.put("SC", "SC");
        stateMap.put("SD", "SD");
        stateMap.put("TN", "TN");
        stateMap.put("TX", "TX");
        stateMap.put("UT", "UT");
        stateMap.put("VT", "VT");
        stateMap.put("VA", "VA");
        stateMap.put("WA", "WA");
        stateMap.put("WV", "WV");
        stateMap.put("WI", "WI");
        stateMap.put("WY", "WY");

    }

    public void reset() {
        userMemberId = null;
        selectedUser = null;
        user = new Users();
    }

    public String getUsersSearch() {
        LOGGER.info(" in getUsersSearch");
        reset();
        usersList = claimMisc.getUsers();
        /*
        adminCompanies = setAdminCompanies();
        disbursementTypes = setDisbursmentTypes();
         */
        LOGGER.info("usersService, size=" + usersList.size());
        return "Users";
    }

    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        usersList = claimMisc.getUsers();
    }

    public String deleteUser() {
        userMemberId = selectedUser.getUserMemberId();
        LOGGER.info("in deleteUser, userMemberId=" + userMemberId);
        claimMisc.deleteUserMemeber(userMemberId);
        usersList = claimMisc.getUsers();
        return "Users";
    }

    public void addUser(ActionEvent event) {
        user.reset();
        selectedUser = null;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "800");
        options.put("width", "1500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddUser", options, null);

    }

    public void editUser() {
        LOGGER.info("userMemberId=" + selectedUser.getUserMemberId());
        user = selectedUser;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "800");
        options.put("width", "1500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddUser", options, null);
    }

    public void onRowSelect(SelectEvent event) {
        selectedUser = (Users) event.getObject();
        /*
        FacesMessage msg = new FacesMessage("Program Selected", ((ProgramSearch) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
         */
    }

    public void getUserMisc() {
        LOGGER.info("in getUserMisc, userMemberId=" + selectedUser.getUserMemberId());
    }

    public void getUserIdAttr(ActionEvent event) {
        userMemberId = (Integer) event.getComponent().getAttributes().get("userId");
        LOGGER.info("in getUserIdAttr, userMemberId=" + userMemberId);
    }

    public void saveUser() {
        LOGGER.info("in saveUSer");
        if (selectedUser != null) { // update
            claimMisc.saveUser(user, selectedUser.getUserMemberId());
        } else {
            claimMisc.saveUser(user, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddUser");

    }

    public void cancel() {
        LOGGER.info("in cancel");
        reset();
        RequestContext.getCurrentInstance().closeDialog("AddUser");
    }

    public void verifyUserName() {

        LOGGER.info("in verifyUserName=" + user.getUserName());

        UserMember um = claimMisc.getUserMemberByName(user.getUserName());

        LOGGER.info("33333");

        if (um != null) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "userName exist, please verify: ", user.getUserName());
            FacesContext.getCurrentInstance().addMessage(null, msg);

        }
    }

}
