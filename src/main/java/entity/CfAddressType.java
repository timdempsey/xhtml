/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfAddressType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfAddressType.findAll", query = "SELECT c FROM CfAddressType c")
    , @NamedQuery(name = "CfAddressType.findByAddressTypeId", query = "SELECT c FROM CfAddressType c WHERE c.addressTypeId = :addressTypeId")
    , @NamedQuery(name = "CfAddressType.findByName", query = "SELECT c FROM CfAddressType c WHERE c.name = :name")
    , @NamedQuery(name = "CfAddressType.findByDescription", query = "SELECT c FROM CfAddressType c WHERE c.description = :description")
    , @NamedQuery(name = "CfAddressType.findByEnteredDate", query = "SELECT c FROM CfAddressType c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CfAddressType.findByLastModifiedDate", query = "SELECT c FROM CfAddressType c WHERE c.lastModifiedDate = :lastModifiedDate")
    , @NamedQuery(name = "CfAddressType.findBySystemGeneratedInd", query = "SELECT c FROM CfAddressType c WHERE c.systemGeneratedInd = :systemGeneratedInd")
    , @NamedQuery(name = "CfAddressType.findByUpdateUserName", query = "SELECT c FROM CfAddressType c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfAddressType.findByUpdateLast", query = "SELECT c FROM CfAddressType c WHERE c.updateLast = :updateLast")})
public class CfAddressType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "addressTypeId")
    private Integer addressTypeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lastModifiedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "systemGeneratedInd")
    private boolean systemGeneratedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;
    @JoinColumn(name = "lastModifiedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember lastModifiedByIdFk;

    public CfAddressType() {
    }

    public CfAddressType(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public CfAddressType(Integer addressTypeId, String name, Date enteredDate, Date lastModifiedDate, boolean systemGeneratedInd) {
        this.addressTypeId = addressTypeId;
        this.name = name;
        this.enteredDate = enteredDate;
        this.lastModifiedDate = lastModifiedDate;
        this.systemGeneratedInd = systemGeneratedInd;
    }

    public Integer getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean getSystemGeneratedInd() {
        return systemGeneratedInd;
    }

    public void setSystemGeneratedInd(boolean systemGeneratedInd) {
        this.systemGeneratedInd = systemGeneratedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public UserMember getLastModifiedByIdFk() {
        return lastModifiedByIdFk;
    }

    public void setLastModifiedByIdFk(UserMember lastModifiedByIdFk) {
        this.lastModifiedByIdFk = lastModifiedByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressTypeId != null ? addressTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfAddressType)) {
            return false;
        }
        CfAddressType other = (CfAddressType) object;
        if ((this.addressTypeId == null && other.addressTypeId != null) || (this.addressTypeId != null && !this.addressTypeId.equals(other.addressTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfAddressType[ addressTypeId=" + addressTypeId + " ]";
    }
    
}
