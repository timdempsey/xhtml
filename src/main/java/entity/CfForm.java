/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfForm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfForm.findAll", query = "SELECT c FROM CfForm c")
    , @NamedQuery(name = "CfForm.findByFormId", query = "SELECT c FROM CfForm c WHERE c.formId = :formId")
    , @NamedQuery(name = "CfForm.findByFormName", query = "SELECT c FROM CfForm c WHERE c.formName = :formName")
    , @NamedQuery(name = "CfForm.findByCollectLienHolder", query = "SELECT c FROM CfForm c WHERE c.collectLienHolder = :collectLienHolder")
    , @NamedQuery(name = "CfForm.findByFormTypeIndFk", query = "SELECT c FROM CfForm c WHERE c.formTypeIndFk = :formTypeIndFk")
    , @NamedQuery(name = "CfForm.findByDeletedInd", query = "SELECT c FROM CfForm c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfForm.findByCollectCobuyer", query = "SELECT c FROM CfForm c WHERE c.collectCobuyer = :collectCobuyer")
    , @NamedQuery(name = "CfForm.findByUpdateUserName", query = "SELECT c FROM CfForm c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfForm.findByUpdateLast", query = "SELECT c FROM CfForm c WHERE c.updateLast = :updateLast")})
public class CfForm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "formId")
    private Integer formId;
    @Size(max = 50)
    @Column(name = "formName")
    private String formName;
    @Column(name = "collectLienHolder")
    private Boolean collectLienHolder;
    @Column(name = "formTypeIndFk")
    private Integer formTypeIndFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "collectCobuyer")
    private boolean collectCobuyer;
    @Size(max = 30)
    @Column(name = "update_user_name")
    private String updateUserName;
    @Column(name = "update_last")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formIdFk")
    private Collection<CfField> cfFieldCollection;
    @OneToMany(mappedBy = "formIdFk")
    private Collection<InspectionCompany> inspectionCompanyCollection;
    @OneToMany(mappedBy = "claimFormIdFk")
    private Collection<ProductDetail> productDetailCollection;
    @OneToMany(mappedBy = "formIdFk")
    private Collection<ProductDetail> productDetailCollection1;

    public CfForm() {
    }

    public CfForm(Integer formId) {
        this.formId = formId;
    }

    public CfForm(Integer formId, boolean deletedInd, boolean collectCobuyer) {
        this.formId = formId;
        this.deletedInd = deletedInd;
        this.collectCobuyer = collectCobuyer;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Boolean getCollectLienHolder() {
        return collectLienHolder;
    }

    public void setCollectLienHolder(Boolean collectLienHolder) {
        this.collectLienHolder = collectLienHolder;
    }

    public Integer getFormTypeIndFk() {
        return formTypeIndFk;
    }

    public void setFormTypeIndFk(Integer formTypeIndFk) {
        this.formTypeIndFk = formTypeIndFk;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public boolean getCollectCobuyer() {
        return collectCobuyer;
    }

    public void setCollectCobuyer(boolean collectCobuyer) {
        this.collectCobuyer = collectCobuyer;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfField> getCfFieldCollection() {
        return cfFieldCollection;
    }

    public void setCfFieldCollection(Collection<CfField> cfFieldCollection) {
        this.cfFieldCollection = cfFieldCollection;
    }

    @XmlTransient
    public Collection<InspectionCompany> getInspectionCompanyCollection() {
        return inspectionCompanyCollection;
    }

    public void setInspectionCompanyCollection(Collection<InspectionCompany> inspectionCompanyCollection) {
        this.inspectionCompanyCollection = inspectionCompanyCollection;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection() {
        return productDetailCollection;
    }

    public void setProductDetailCollection(Collection<ProductDetail> productDetailCollection) {
        this.productDetailCollection = productDetailCollection;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection1() {
        return productDetailCollection1;
    }

    public void setProductDetailCollection1(Collection<ProductDetail> productDetailCollection1) {
        this.productDetailCollection1 = productDetailCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formId != null ? formId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfForm)) {
            return false;
        }
        CfForm other = (CfForm) object;
        if ((this.formId == null && other.formId != null) || (this.formId != null && !this.formId.equals(other.formId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfForm[ formId=" + formId + " ]";
    }
    
}
