/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtvehicleWeightRating")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtvehicleWeightRating.findAll", query = "SELECT d FROM DtvehicleWeightRating d")
    , @NamedQuery(name = "DtvehicleWeightRating.findByVehicleWeightRatingId", query = "SELECT d FROM DtvehicleWeightRating d WHERE d.vehicleWeightRatingId = :vehicleWeightRatingId")
    , @NamedQuery(name = "DtvehicleWeightRating.findByDescription", query = "SELECT d FROM DtvehicleWeightRating d WHERE d.description = :description")})
public class DtvehicleWeightRating implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vehicleWeightRatingId")
    private Integer vehicleWeightRatingId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;

    public DtvehicleWeightRating() {
    }

    public DtvehicleWeightRating(Integer vehicleWeightRatingId) {
        this.vehicleWeightRatingId = vehicleWeightRatingId;
    }

    public Integer getVehicleWeightRatingId() {
        return vehicleWeightRatingId;
    }

    public void setVehicleWeightRatingId(Integer vehicleWeightRatingId) {
        this.vehicleWeightRatingId = vehicleWeightRatingId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleWeightRatingId != null ? vehicleWeightRatingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtvehicleWeightRating)) {
            return false;
        }
        DtvehicleWeightRating other = (DtvehicleWeightRating) object;
        if ((this.vehicleWeightRatingId == null && other.vehicleWeightRatingId != null) || (this.vehicleWeightRatingId != null && !this.vehicleWeightRatingId.equals(other.vehicleWeightRatingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtvehicleWeightRating[ vehicleWeightRatingId=" + vehicleWeightRatingId + " ]";
    }
    
}
