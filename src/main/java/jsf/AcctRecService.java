/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.AccountKeeperType;
import entity.CashTransaction;
import entity.Ledger;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
@Named(value = "acctRecService")
@ApplicationScoped
public class AcctRecService {

    private static final Logger LOGGER = LogManager.getLogger(AcctRecService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;

    @Resource
    UserTransaction utx;

    @Inject
    AcctRecItemService acctRecItemService;
    
    @Inject
    AcctTransService acctTransService;

    /**
     * Creates a new instance of AcctRecService
     */
    public AcctRecService() {
    }

    private AcctTrans rectrans;
    private LazyDataModel<AcctTrans> recvs;
    private AcctTrans selectedRec;
    private AcctTrans rec;

    private AcctRec acctRecTrans;
    private List<AcctRec> acctRecvs;
    private AcctRec selectedAcctRec;
    private AcctRec acctRec;

    private AcctTrans paytrans;
    private LazyDataModel<AcctTrans> pays;
    private AcctTrans selectedPay;
    
    /*
    private AcctRec acctPayTrans;
    private List<AcctRec> acctRecvs;
    private AcctRec selectedAcctRec;
    private AcctRec acctRec;

    public AcctRec getAcctPayTrans() {
        return acctPayTrans;
    }

    public void setAcctPayTrans(AcctRec acctPayTrans) {
        this.acctPayTrans = acctPayTrans;
    }
    */
    
    Boolean isReceivable;

    public Boolean getIsReceivable() {
        return isReceivable;
    }

    public void setIsReceivable(Boolean isReceivable) {
        this.isReceivable = isReceivable;
    }
    
    
    

    public AcctTrans getPaytrans() {
        return paytrans;
    }

    public void setPaytrans(AcctTrans paytrans) {
        this.paytrans = paytrans;
    }

    public LazyDataModel<AcctTrans> getPays() {
        return pays;
    }

    public void setPays(LazyDataModel<AcctTrans> pays) {
        this.pays = pays;
    }

    public AcctTrans getSelectedPay() {
        return selectedPay;
    }

    public void setSelectedPay(AcctTrans selectedPay) {
        this.selectedPay = selectedPay;
    }

    public AcctRec getAcctRecTrans() {
        return acctRecTrans;
    }

    public void setAcctRecTrans(AcctRec acctRecTrans) {
        this.acctRecTrans = acctRecTrans;
    }

    public List<AcctRec> getAcctRecvs() {
        return acctRecvs;
    }

    public void setAcctRecvs(List<AcctRec> acctRecvs) {
        this.acctRecvs = acctRecvs;
    }

    public AcctRec getSelectedAcctRec() {
        return selectedAcctRec;
    }

    public void setSelectedAcctRec(AcctRec selectedAcctRec) {
        this.selectedAcctRec = selectedAcctRec;
    }

    public AcctRec getAcctRec() {
        return acctRec;
    }

    public void setAcctRec(AcctRec acctRec) {
        this.acctRec = acctRec;
    }

    public AcctTrans getRec() {
        return rec;
    }

    public void setRec(AcctTrans rec) {
        this.rec = rec;
    }

    public AcctTrans getRectrans() {
        return rectrans;
    }

    public void setRectrans(AcctTrans rectrans) {
        this.rectrans = rectrans;
    }

    public LazyDataModel<AcctTrans> getRecvs() {
        return recvs;
    }

    public void setRecvs(LazyDataModel<AcctTrans> recvs) {
        this.recvs = recvs;
    }

    public AcctTrans getSelectedRec() {
        return selectedRec;
    }

    public void setSelectedRec(AcctTrans selectedRec) {
        this.selectedRec = selectedRec;
    }

    @PostConstruct
    public void init() {
        selectedRec = new AcctTrans();
        rectrans = new AcctTrans();
        rec = new AcctTrans();

        selectedAcctRec = new AcctRec();
        acctRecTrans = new AcctRec();
        acctRecTrans.setAdminCompany("Dealer Owned");
        acctRec = new AcctRec();

        selectedRec = new AcctTrans();
        paytrans = new AcctTrans();
        /*
        acctPayTrans = new AcctRec();
        acctPayTrans.setAdminCompany("Dealer Owned");
        */

    }

    /**
     *
     * @return
     */
    public String getReceivablesPage() {
        LOGGER.info("in getReceivablesPage");
        reset();
        getReceivables();
        return "Receivables";
    }

   
    public void getReceivables() {
        try {

            recvs = new LazyDataModel<AcctTrans>() {
                @Override
                public AcctTrans getRowData(String rowKey) {
                    int intRowKey = Integer.parseInt(rowKey);
                    for (AcctTrans at : recvs) {
                        if (at.getTransactionId().equals(intRowKey)) {
                            return at;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(AcctTrans at) {
                    return at.getTransactionId();
                }

                @Override
                public List<AcctTrans> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    List<CashTransaction> ctList = claimMisc.searchTransactions(rectrans, first, pageSize);
                    return (List<AcctTrans>) createReceivableView(ctList, true);
                }

            };

            recvs.setRowCount(claimMisc.getSearchTransactionsCount(rectrans));

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public List<?> createReceivableView(List<CashTransaction> cts, Boolean receivable) {
        List<AcctTrans> recList = new ArrayList<>();
        List<AcctTrans> payList = new ArrayList<>();

        LOGGER.info("size of cts=" + cts.size());

        for (CashTransaction ct : cts) {
            //LOGGER.info("pending=" + ct.getPendingExportUpdateInd() );

            //if (ct.getPendingExportUpdateInd()) {   
            Ledger ledger = claimMisc.getLedgerById(ct.getCashTransactionId());

            AccountKeeper ak = claimMisc.getAccountKeeperById(ledger.getCurrentAccountKeeperIdFk().getAccountKeeperId());
            String accountType = claimMisc.getAccountKeeperTypeById(ak.getAccountKeeperTypeIdFk().getAccountKeeperTypeId()).getAccountKeeperTypeDesc();
            String accountName = ak.getAccountKeeperName();
            String contractNo = "";
            String claimNo = "";
            if (ledger.getRelatedClaimIdFk() != null && ledger.getRelatedClaimIdFk().getClaimId() > 0) {
                claimNo = claimMisc.getClaimById(ledger.getRelatedClaimIdFk().getClaimId()).getClaimNumber();
            }
            if (ledger.getRelatedContractIdFk() != null && ledger.getRelatedContractIdFk().getContractId() > 0) {
                contractNo = claimMisc.getContractsById(ledger.getRelatedContractIdFk().getContractId()).getContractNo();
            }
            String adminCompany = "";
            if (ledger.getAdminCompanyIdFk().getAdminCompanyId() == 1) {
                adminCompany = "Dealer Owned";
            }
            String insurer = "";    // later
            String legalName = ak.getLegalName();
            String postedDate = "";
            String enteredDate = "";
            String sentDate = "";
            String recDate = "";
            if (ledger.getEnteredDate() != null) {
                enteredDate = claimMisc.getInputJavaDate(ledger.getEnteredDate());
            }
            if (ct.getSentDate() != null) {
                sentDate = claimMisc.getJavaDate(ct.getSentDate());
            }
            if (ledger.getLedgerDate() != null) {
                recDate = claimMisc.getJavaDate(ledger.getLedgerDate());
            }

            if (!ledger.getLowersAdminCompanyBalanceInd() && receivable) {    // indicate as receivables =0
                
                recList.add(new AcctTrans(ct.getCashTransactionId(),
                        ct.getTransactionNumber(),
                        claimMisc.getDtcashTransactionTypeById(ct.getCashTransactionTypeInd().getCashTransactionTypeId()).getDescription(),
                        claimMisc.getDtcashTransactionStatusById(ct.getCashTransactionStatusInd().getCashTransactionStatusId()).getDescription(),
                        ledger.getCurrentAmount(),
                        ct.getAppliedAmount(),
                        ct.getOverUnder(),
                        accountType,
                        accountName,
                        legalName,
                        enteredDate,
                        sentDate,
                        recDate,
                        contractNo,
                        claimNo,
                        insurer,
                        postedDate,
                        adminCompany
                ));
            } else if( ledger.getLowersAdminCompanyBalanceInd()) {
                
                payList.add(new AcctTrans(ct.getCashTransactionId(),
                        ct.getTransactionNumber(),
                        claimMisc.getDtcashTransactionTypeById(ct.getCashTransactionTypeInd().getCashTransactionTypeId()).getDescription(),
                        claimMisc.getDtcashTransactionStatusById(ct.getCashTransactionStatusInd().getCashTransactionStatusId()).getDescription(),
                        ledger.getCurrentAmount(),
                        ct.getAppliedAmount(),
                        ct.getOverUnder(),
                        accountType,
                        accountName,
                        legalName,
                        enteredDate,
                        sentDate,
                        recDate,
                        contractNo,
                        claimNo,
                        insurer,
                        postedDate,
                        adminCompany
                ));
            }
        }

        LOGGER.info("size of recList=" + recList.size());
        LOGGER.info("size of payList = " + payList.size());
        if (receivable) {
            return recList;
        } else {
            return payList;
        }
    }

    public void getPayables() {
        try {

            pays = new LazyDataModel<AcctTrans>() {
                @Override
                public AcctTrans getRowData(String rowKey) {
                    int intRowKey = Integer.parseInt(rowKey);
                    for (AcctTrans at : pays) {
                        if (at.getTransactionId().equals(intRowKey)) {
                            return at;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(AcctTrans at) {
                    return at.getTransactionId();
                }

                @Override
                public List<AcctTrans> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    List<CashTransaction> ctList = claimMisc.searchTransactions(paytrans, first, pageSize);
                    return (List<AcctTrans>) createReceivableView(ctList, false);   // reuse the createReceivableView
                }

            };

            pays.setRowCount(claimMisc.getSearchTransactionsCount(paytrans));

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void onRowSelect(SelectEvent event) {
        selectedRec = (AcctTrans) event.getObject();

    }

    public void editRec() {
        LOGGER.info("userMemberId=" + selectedRec.getTransactionId());
        rec = selectedRec;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "800");
        options.put("width", "1500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("EditRec", options, null);
    }

    public String getAcctReceivable() {
        LOGGER.info("in getAcctReceivable");
        
        isReceivable = true;
        acctRecItemService.setTitle("receivables");

        return "AcctReceivables";

    }

    public String getAcctPayable() {
        LOGGER.info("in getAcctPayable");
        
        isReceivable = false;
         acctRecItemService.setTitle("payables");

        return "AcctPayables";
    }

    public String getPayablesPage() {
        LOGGER.info("in getPayablesPage");
        reset();
        getPayables();
        return "Payables";
    }

    

    public void getBalances() {
        String acctName = acctRecTrans.getAcctHolderIn().split("\\(")[0];
        String acctType = acctRecTrans.getAcctHolderIn().split("\\(")[1].split("\\)")[0];
        LOGGER.info("in getAcctReceivable, acctName=" + acctName + ", acctType=" + acctType);

        AccountKeeperType akp = claimMisc.getAccountKeeperTypeByName(acctType);
        AccountKeeper ak = claimMisc.getAccountKeeperByNameType(acctName, akp.getAccountKeeperTypeId());

        acctRecvs = new ArrayList<>();

        acctRecvs.add(new AcctRec(acctRecTrans.getAdminCompany(), ak.getAccountKeeperId(), acctType, acctName, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
    }

    public void onAcctRecRowSelect(SelectEvent event) {
        LOGGER.info("in onAcctRecRowSelect");
        selectedAcctRec = (AcctRec) event.getObject();
        LOGGER.info("acctId=" + selectedAcctRec.getAcctId());

    }

    public void getSelectItems(ActionEvent event) {

        LOGGER.info("in getSelectItems, name=" + selectedAcctRec.getAcctName());
        LOGGER.info("acctHolderId=" + selectedAcctRec.getAcctId());

        acctRecItemService.setAcctHolderId(selectedAcctRec.getAcctId());
        acctRecItemService.setAcctHolderName(selectedAcctRec.getAcctName());

        acctRecItemService.initRecUpper();
        acctRecItemService.getAcctRecUpper().setAdminCompany(acctRecTrans.getAdminCompany());
        //acctRecItemService.reset();
        acctRecItemService.setSelectedAcctRecItems(null);
        acctRecItemService.loadOptItems(selectedAcctRec.getAcctId());
        acctRecItemService.setIsReceivable(isReceivable);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AcctRecItemsPage", options, null);
      

    }
    
    
    
    public void getTransDetail() {
        acctTransService.setSelectedTransaction(selectedRec);
        acctTransService.getTransDetail();
    }
    
    public void printTransaction() {
        
    }

    public void getAcctRecDetail() {
        // not neceissary
    }

    public void saveReceivable() {

    }

    public void saveANDprint() {

    }

    public void reset() {
        selectedRec = null;
        rectrans.reset();
        rec = new AcctTrans();

        selectedAcctRec = null;
        acctRecTrans.reset();
        acctRec = new AcctRec();
        acctRecvs = new ArrayList();

        selectedPay = null;
        paytrans.reset();
        paytrans.reset();
        
        acctRecTrans = new AcctRec();
        acctRecTrans.setAdminCompany("Dealer Owned");
        acctRecvs = new ArrayList<>();

    }

}
