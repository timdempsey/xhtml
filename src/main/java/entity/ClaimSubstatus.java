/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimSubstatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimSubstatus.findAll", query = "SELECT c FROM ClaimSubstatus c")
    , @NamedQuery(name = "ClaimSubstatus.findByClaimSubstatusInd", query = "SELECT c FROM ClaimSubstatus c WHERE c.claimSubstatusInd = :claimSubstatusInd")
    , @NamedQuery(name = "ClaimSubstatus.findByClaimSubstatusDesc", query = "SELECT c FROM ClaimSubstatus c WHERE c.claimSubstatusDesc = :claimSubstatusDesc")})
public class ClaimSubstatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "claimSubstatusInd")
    private String claimSubstatusInd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "claimSubstatusDesc")
    private String claimSubstatusDesc;
    @OneToMany(mappedBy = "claimSubStatus")
    private Collection<Claim> claimCollection;

    public ClaimSubstatus() {
    }

    public ClaimSubstatus(String claimSubstatusInd) {
        this.claimSubstatusInd = claimSubstatusInd;
    }

    public ClaimSubstatus(String claimSubstatusInd, String claimSubstatusDesc) {
        this.claimSubstatusInd = claimSubstatusInd;
        this.claimSubstatusDesc = claimSubstatusDesc;
    }

    public String getClaimSubstatusInd() {
        return claimSubstatusInd;
    }

    public void setClaimSubstatusInd(String claimSubstatusInd) {
        this.claimSubstatusInd = claimSubstatusInd;
    }

    public String getClaimSubstatusDesc() {
        return claimSubstatusDesc;
    }

    public void setClaimSubstatusDesc(String claimSubstatusDesc) {
        this.claimSubstatusDesc = claimSubstatusDesc;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimSubstatusInd != null ? claimSubstatusInd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimSubstatus)) {
            return false;
        }
        ClaimSubstatus other = (ClaimSubstatus) object;
        if ((this.claimSubstatusInd == null && other.claimSubstatusInd != null) || (this.claimSubstatusInd != null && !this.claimSubstatusInd.equals(other.claimSubstatusInd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimSubstatus[ claimSubstatusInd=" + claimSubstatusInd + " ]";
    }
    
}
