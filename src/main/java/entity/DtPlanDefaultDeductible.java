/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanDefaultDeductible")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanDefaultDeductible.findAll", query = "SELECT d FROM DtPlanDefaultDeductible d")
    , @NamedQuery(name = "DtPlanDefaultDeductible.findByDefaultDeductibleId", query = "SELECT d FROM DtPlanDefaultDeductible d WHERE d.defaultDeductibleId = :defaultDeductibleId")
    , @NamedQuery(name = "DtPlanDefaultDeductible.findByDescription", query = "SELECT d FROM DtPlanDefaultDeductible d WHERE d.description = :description")})
public class DtPlanDefaultDeductible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "defaultDeductibleId")
    private Integer defaultDeductibleId;
    @Size(max = 30)
    @Column(name = "description")
    private String description;

    public DtPlanDefaultDeductible() {
    }

    public DtPlanDefaultDeductible(Integer defaultDeductibleId) {
        this.defaultDeductibleId = defaultDeductibleId;
    }

    public Integer getDefaultDeductibleId() {
        return defaultDeductibleId;
    }

    public void setDefaultDeductibleId(Integer defaultDeductibleId) {
        this.defaultDeductibleId = defaultDeductibleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (defaultDeductibleId != null ? defaultDeductibleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanDefaultDeductible)) {
            return false;
        }
        DtPlanDefaultDeductible other = (DtPlanDefaultDeductible) object;
        if ((this.defaultDeductibleId == null && other.defaultDeductibleId != null) || (this.defaultDeductibleId != null && !this.defaultDeductibleId.equals(other.defaultDeductibleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanDefaultDeductible[ defaultDeductibleId=" + defaultDeductibleId + " ]";
    }
    
}
