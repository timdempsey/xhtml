/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractPdfFile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractPdfFile.findAll", query = "SELECT c FROM ContractPdfFile c")
    , @NamedQuery(name = "ContractPdfFile.findByContractPdfFileId", query = "SELECT c FROM ContractPdfFile c WHERE c.contractPdfFileId = :contractPdfFileId")
    , @NamedQuery(name = "ContractPdfFile.findByContractPdfFileTypeInd", query = "SELECT c FROM ContractPdfFile c WHERE c.contractPdfFileTypeInd = :contractPdfFileTypeInd")
    , @NamedQuery(name = "ContractPdfFile.findByGeneratedDate", query = "SELECT c FROM ContractPdfFile c WHERE c.generatedDate = :generatedDate")
    , @NamedQuery(name = "ContractPdfFile.findByGuid", query = "SELECT c FROM ContractPdfFile c WHERE c.guid = :guid")
    , @NamedQuery(name = "ContractPdfFile.findByDeletedInd", query = "SELECT c FROM ContractPdfFile c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "ContractPdfFile.findByUpdateUserName", query = "SELECT c FROM ContractPdfFile c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractPdfFile.findByUpdateLast", query = "SELECT c FROM ContractPdfFile c WHERE c.updateLast = :updateLast")})
public class ContractPdfFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractPdfFileId")
    private Integer contractPdfFileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractPdfFileTypeInd")
    private int contractPdfFileTypeInd;
    @Column(name = "generatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date generatedDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "guid")
    private String guid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "contractPdfFileId", referencedColumnName = "fileId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private CfFile cfFile;
    @JoinColumn(name = "relatedPDFDetailIdFk", referencedColumnName = "PDFDetailId")
    @ManyToOne
    private CfPDFDetail relatedPDFDetailIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;

    public ContractPdfFile() {
    }

    public ContractPdfFile(Integer contractPdfFileId) {
        this.contractPdfFileId = contractPdfFileId;
    }

    public ContractPdfFile(Integer contractPdfFileId, int contractPdfFileTypeInd, String guid, boolean deletedInd) {
        this.contractPdfFileId = contractPdfFileId;
        this.contractPdfFileTypeInd = contractPdfFileTypeInd;
        this.guid = guid;
        this.deletedInd = deletedInd;
    }

    public Integer getContractPdfFileId() {
        return contractPdfFileId;
    }

    public void setContractPdfFileId(Integer contractPdfFileId) {
        this.contractPdfFileId = contractPdfFileId;
    }

    public int getContractPdfFileTypeInd() {
        return contractPdfFileTypeInd;
    }

    public void setContractPdfFileTypeInd(int contractPdfFileTypeInd) {
        this.contractPdfFileTypeInd = contractPdfFileTypeInd;
    }

    public Date getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfFile getCfFile() {
        return cfFile;
    }

    public void setCfFile(CfFile cfFile) {
        this.cfFile = cfFile;
    }

    public CfPDFDetail getRelatedPDFDetailIdFk() {
        return relatedPDFDetailIdFk;
    }

    public void setRelatedPDFDetailIdFk(CfPDFDetail relatedPDFDetailIdFk) {
        this.relatedPDFDetailIdFk = relatedPDFDetailIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractPdfFileId != null ? contractPdfFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractPdfFile)) {
            return false;
        }
        ContractPdfFile other = (ContractPdfFile) object;
        if ((this.contractPdfFileId == null && other.contractPdfFileId != null) || (this.contractPdfFileId != null && !this.contractPdfFileId.equals(other.contractPdfFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractPdfFile[ contractPdfFileId=" + contractPdfFileId + " ]";
    }
    
}
