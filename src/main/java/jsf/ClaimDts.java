/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.log4j.Logger;

/**
 *
 * @author Jiepi
 */

public class ClaimDts implements Serializable{
    
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public  ClaimDts() {
    }

    public ClaimDts(Integer claimId, String claimNumber, List<ClaimDTSLine> claimDTSLine) {
        this.claimId = claimId;
        this.claimNumber = claimNumber;
        this.claimDTSLine = claimDTSLine;
    }

    
    
    

    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    List<ClaimDTSLine> claimDTSLine;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public List<ClaimDTSLine> getClaimDTSLine() {
        return claimDTSLine;
    }

    public void setClaimDTSLine(List<ClaimDTSLine> claimDTSLine) {
        this.claimDTSLine = claimDTSLine;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }
    
    
    

}







