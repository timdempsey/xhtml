/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClassItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassItem.findAll", query = "SELECT c FROM ClassItem c")
    , @NamedQuery(name = "ClassItem.findByClassItemId", query = "SELECT c FROM ClassItem c WHERE c.classItemId = :classItemId")
    , @NamedQuery(name = "ClassItem.findByYearStart", query = "SELECT c FROM ClassItem c WHERE c.yearStart = :yearStart")
    , @NamedQuery(name = "ClassItem.findByYearEnd", query = "SELECT c FROM ClassItem c WHERE c.yearEnd = :yearEnd")
    , @NamedQuery(name = "ClassItem.findByCylinders", query = "SELECT c FROM ClassItem c WHERE c.cylinders = :cylinders")
    , @NamedQuery(name = "ClassItem.findByTonRatingStartInd", query = "SELECT c FROM ClassItem c WHERE c.tonRatingStartInd = :tonRatingStartInd")
    , @NamedQuery(name = "ClassItem.findByTonRatingEndInd", query = "SELECT c FROM ClassItem c WHERE c.tonRatingEndInd = :tonRatingEndInd")
    , @NamedQuery(name = "ClassItem.findByCubicInchDisplacementStart", query = "SELECT c FROM ClassItem c WHERE c.cubicInchDisplacementStart = :cubicInchDisplacementStart")
    , @NamedQuery(name = "ClassItem.findByCubicInchDisplacementEnd", query = "SELECT c FROM ClassItem c WHERE c.cubicInchDisplacementEnd = :cubicInchDisplacementEnd")
    , @NamedQuery(name = "ClassItem.findByWeightRatingStart", query = "SELECT c FROM ClassItem c WHERE c.weightRatingStart = :weightRatingStart")
    , @NamedQuery(name = "ClassItem.findByWeightRatingEnd", query = "SELECT c FROM ClassItem c WHERE c.weightRatingEnd = :weightRatingEnd")
    , @NamedQuery(name = "ClassItem.findByLiterDisplacementStart", query = "SELECT c FROM ClassItem c WHERE c.literDisplacementStart = :literDisplacementStart")
    , @NamedQuery(name = "ClassItem.findByLiterDisplacementEnd", query = "SELECT c FROM ClassItem c WHERE c.literDisplacementEnd = :literDisplacementEnd")
    , @NamedQuery(name = "ClassItem.findByCubicCentimeterDisplacementStart", query = "SELECT c FROM ClassItem c WHERE c.cubicCentimeterDisplacementStart = :cubicCentimeterDisplacementStart")
    , @NamedQuery(name = "ClassItem.findByCubicCentimeterDisplacementEnd", query = "SELECT c FROM ClassItem c WHERE c.cubicCentimeterDisplacementEnd = :cubicCentimeterDisplacementEnd")
    , @NamedQuery(name = "ClassItem.findByPurchasePriceStart", query = "SELECT c FROM ClassItem c WHERE c.purchasePriceStart = :purchasePriceStart")
    , @NamedQuery(name = "ClassItem.findByPurchasePriceEnd", query = "SELECT c FROM ClassItem c WHERE c.purchasePriceEnd = :purchasePriceEnd")
    , @NamedQuery(name = "ClassItem.findByVINPartial", query = "SELECT c FROM ClassItem c WHERE c.vINPartial = :vINPartial")
    , @NamedQuery(name = "ClassItem.findByGVWRStart", query = "SELECT c FROM ClassItem c WHERE c.gVWRStart = :gVWRStart")
    , @NamedQuery(name = "ClassItem.findByGVWREnd", query = "SELECT c FROM ClassItem c WHERE c.gVWREnd = :gVWREnd")
    , @NamedQuery(name = "ClassItem.findByUpdateUserName", query = "SELECT c FROM ClassItem c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClassItem.findByUpdateLast", query = "SELECT c FROM ClassItem c WHERE c.updateLast = :updateLast")})
public class ClassItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "classItemId")
    private Integer classItemId;
    @Column(name = "yearStart")
    private Integer yearStart;
    @Column(name = "yearEnd")
    private Integer yearEnd;
    @Column(name = "cylinders")
    private Integer cylinders;
    @Column(name = "tonRatingStartInd")
    private Integer tonRatingStartInd;
    @Column(name = "tonRatingEndInd")
    private Integer tonRatingEndInd;
    @Column(name = "cubicInchDisplacementStart")
    private Integer cubicInchDisplacementStart;
    @Column(name = "cubicInchDisplacementEnd")
    private Integer cubicInchDisplacementEnd;
    @Column(name = "weightRatingStart")
    private Integer weightRatingStart;
    @Column(name = "weightRatingEnd")
    private Integer weightRatingEnd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "literDisplacementStart")
    private BigDecimal literDisplacementStart;
    @Column(name = "literDisplacementEnd")
    private BigDecimal literDisplacementEnd;
    @Column(name = "cubicCentimeterDisplacementStart")
    private Integer cubicCentimeterDisplacementStart;
    @Column(name = "cubicCentimeterDisplacementEnd")
    private Integer cubicCentimeterDisplacementEnd;
    @Column(name = "purchasePriceStart")
    private BigDecimal purchasePriceStart;
    @Column(name = "purchasePriceEnd")
    private BigDecimal purchasePriceEnd;
    @Size(max = 17)
    @Column(name = "VINPartial")
    private String vINPartial;
    @Column(name = "GVWRStart")
    private Integer gVWRStart;
    @Column(name = "GVWREnd")
    private Integer gVWREnd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "classRuleIdFk", referencedColumnName = "classRuleId")
    @ManyToOne
    private ClassRule classRuleIdFk;
    @JoinColumn(name = "countryOfOriginIdFk", referencedColumnName = "countryOfOriginId")
    @ManyToOne
    private CountryOfOrigin countryOfOriginIdFk;
    @JoinColumn(name = "driveTypeInd", referencedColumnName = "driveTypeId")
    @ManyToOne
    private DriveType driveTypeInd;
    @JoinColumn(name = "fuelDeliveryIdFk", referencedColumnName = "fuelDeliveryId")
    @ManyToOne
    private FuelDelivery fuelDeliveryIdFk;
    @JoinColumn(name = "fuelTypeIdFk", referencedColumnName = "fuelTypeId")
    @ManyToOne
    private FuelType fuelTypeIdFk;
    @JoinColumn(name = "makeIdFk", referencedColumnName = "makeId")
    @ManyToOne
    private Make makeIdFk;
    @JoinColumn(name = "modelIdFk", referencedColumnName = "ModelId")
    @ManyToOne
    private Model modelIdFk;
    @JoinColumn(name = "seriesIdFk", referencedColumnName = "seriesId")
    @ManyToOne
    private Series seriesIdFk;
    @JoinColumn(name = "vehicleTypeInd", referencedColumnName = "vehicleTypeId")
    @ManyToOne
    private VehicleType vehicleTypeInd;
    @JoinColumn(name = "vinClassCodeIdFk", referencedColumnName = "vinClassCodeId")
    @ManyToOne
    private VINClassCode vinClassCodeIdFk;

    public ClassItem() {
    }

    public ClassItem(Integer classItemId) {
        this.classItemId = classItemId;
    }

    public Integer getClassItemId() {
        return classItemId;
    }

    public void setClassItemId(Integer classItemId) {
        this.classItemId = classItemId;
    }

    public Integer getYearStart() {
        return yearStart;
    }

    public void setYearStart(Integer yearStart) {
        this.yearStart = yearStart;
    }

    public Integer getYearEnd() {
        return yearEnd;
    }

    public void setYearEnd(Integer yearEnd) {
        this.yearEnd = yearEnd;
    }

    public Integer getCylinders() {
        return cylinders;
    }

    public void setCylinders(Integer cylinders) {
        this.cylinders = cylinders;
    }

    public Integer getTonRatingStartInd() {
        return tonRatingStartInd;
    }

    public void setTonRatingStartInd(Integer tonRatingStartInd) {
        this.tonRatingStartInd = tonRatingStartInd;
    }

    public Integer getTonRatingEndInd() {
        return tonRatingEndInd;
    }

    public void setTonRatingEndInd(Integer tonRatingEndInd) {
        this.tonRatingEndInd = tonRatingEndInd;
    }

    public Integer getCubicInchDisplacementStart() {
        return cubicInchDisplacementStart;
    }

    public void setCubicInchDisplacementStart(Integer cubicInchDisplacementStart) {
        this.cubicInchDisplacementStart = cubicInchDisplacementStart;
    }

    public Integer getCubicInchDisplacementEnd() {
        return cubicInchDisplacementEnd;
    }

    public void setCubicInchDisplacementEnd(Integer cubicInchDisplacementEnd) {
        this.cubicInchDisplacementEnd = cubicInchDisplacementEnd;
    }

    public Integer getWeightRatingStart() {
        return weightRatingStart;
    }

    public void setWeightRatingStart(Integer weightRatingStart) {
        this.weightRatingStart = weightRatingStart;
    }

    public Integer getWeightRatingEnd() {
        return weightRatingEnd;
    }

    public void setWeightRatingEnd(Integer weightRatingEnd) {
        this.weightRatingEnd = weightRatingEnd;
    }

    public BigDecimal getLiterDisplacementStart() {
        return literDisplacementStart;
    }

    public void setLiterDisplacementStart(BigDecimal literDisplacementStart) {
        this.literDisplacementStart = literDisplacementStart;
    }

    public BigDecimal getLiterDisplacementEnd() {
        return literDisplacementEnd;
    }

    public void setLiterDisplacementEnd(BigDecimal literDisplacementEnd) {
        this.literDisplacementEnd = literDisplacementEnd;
    }

    public Integer getCubicCentimeterDisplacementStart() {
        return cubicCentimeterDisplacementStart;
    }

    public void setCubicCentimeterDisplacementStart(Integer cubicCentimeterDisplacementStart) {
        this.cubicCentimeterDisplacementStart = cubicCentimeterDisplacementStart;
    }

    public Integer getCubicCentimeterDisplacementEnd() {
        return cubicCentimeterDisplacementEnd;
    }

    public void setCubicCentimeterDisplacementEnd(Integer cubicCentimeterDisplacementEnd) {
        this.cubicCentimeterDisplacementEnd = cubicCentimeterDisplacementEnd;
    }

    public BigDecimal getPurchasePriceStart() {
        return purchasePriceStart;
    }

    public void setPurchasePriceStart(BigDecimal purchasePriceStart) {
        this.purchasePriceStart = purchasePriceStart;
    }

    public BigDecimal getPurchasePriceEnd() {
        return purchasePriceEnd;
    }

    public void setPurchasePriceEnd(BigDecimal purchasePriceEnd) {
        this.purchasePriceEnd = purchasePriceEnd;
    }

    public String getVINPartial() {
        return vINPartial;
    }

    public void setVINPartial(String vINPartial) {
        this.vINPartial = vINPartial;
    }

    public Integer getGVWRStart() {
        return gVWRStart;
    }

    public void setGVWRStart(Integer gVWRStart) {
        this.gVWRStart = gVWRStart;
    }

    public Integer getGVWREnd() {
        return gVWREnd;
    }

    public void setGVWREnd(Integer gVWREnd) {
        this.gVWREnd = gVWREnd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ClassRule getClassRuleIdFk() {
        return classRuleIdFk;
    }

    public void setClassRuleIdFk(ClassRule classRuleIdFk) {
        this.classRuleIdFk = classRuleIdFk;
    }

    public CountryOfOrigin getCountryOfOriginIdFk() {
        return countryOfOriginIdFk;
    }

    public void setCountryOfOriginIdFk(CountryOfOrigin countryOfOriginIdFk) {
        this.countryOfOriginIdFk = countryOfOriginIdFk;
    }

    public DriveType getDriveTypeInd() {
        return driveTypeInd;
    }

    public void setDriveTypeInd(DriveType driveTypeInd) {
        this.driveTypeInd = driveTypeInd;
    }

    public FuelDelivery getFuelDeliveryIdFk() {
        return fuelDeliveryIdFk;
    }

    public void setFuelDeliveryIdFk(FuelDelivery fuelDeliveryIdFk) {
        this.fuelDeliveryIdFk = fuelDeliveryIdFk;
    }

    public FuelType getFuelTypeIdFk() {
        return fuelTypeIdFk;
    }

    public void setFuelTypeIdFk(FuelType fuelTypeIdFk) {
        this.fuelTypeIdFk = fuelTypeIdFk;
    }

    public Make getMakeIdFk() {
        return makeIdFk;
    }

    public void setMakeIdFk(Make makeIdFk) {
        this.makeIdFk = makeIdFk;
    }

    public Model getModelIdFk() {
        return modelIdFk;
    }

    public void setModelIdFk(Model modelIdFk) {
        this.modelIdFk = modelIdFk;
    }

    public Series getSeriesIdFk() {
        return seriesIdFk;
    }

    public void setSeriesIdFk(Series seriesIdFk) {
        this.seriesIdFk = seriesIdFk;
    }

    public VehicleType getVehicleTypeInd() {
        return vehicleTypeInd;
    }

    public void setVehicleTypeInd(VehicleType vehicleTypeInd) {
        this.vehicleTypeInd = vehicleTypeInd;
    }

    public VINClassCode getVinClassCodeIdFk() {
        return vinClassCodeIdFk;
    }

    public void setVinClassCodeIdFk(VINClassCode vinClassCodeIdFk) {
        this.vinClassCodeIdFk = vinClassCodeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classItemId != null ? classItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassItem)) {
            return false;
        }
        ClassItem other = (ClassItem) object;
        if ((this.classItemId == null && other.classItemId != null) || (this.classItemId != null && !this.classItemId.equals(other.classItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClassItem[ classItemId=" + classItemId + " ]";
    }
    
}
