/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCancelMethodGroupRuleFee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCancelMethodGroupRuleFee.findAll", query = "SELECT c FROM CfCancelMethodGroupRuleFee c")
    , @NamedQuery(name = "CfCancelMethodGroupRuleFee.findByCancelMethodGroupRuleFeeId", query = "SELECT c FROM CfCancelMethodGroupRuleFee c WHERE c.cancelMethodGroupRuleFeeId = :cancelMethodGroupRuleFeeId")
    , @NamedQuery(name = "CfCancelMethodGroupRuleFee.findByFee", query = "SELECT c FROM CfCancelMethodGroupRuleFee c WHERE c.fee = :fee")
    , @NamedQuery(name = "CfCancelMethodGroupRuleFee.findByCancelFeeTypeInd", query = "SELECT c FROM CfCancelMethodGroupRuleFee c WHERE c.cancelFeeTypeInd = :cancelFeeTypeInd")
    , @NamedQuery(name = "CfCancelMethodGroupRuleFee.findByUpdateUserName", query = "SELECT c FROM CfCancelMethodGroupRuleFee c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCancelMethodGroupRuleFee.findByUpdateLast", query = "SELECT c FROM CfCancelMethodGroupRuleFee c WHERE c.updateLast = :updateLast")})
public class CfCancelMethodGroupRuleFee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodGroupRuleFeeId")
    private Integer cancelMethodGroupRuleFeeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "fee")
    private BigDecimal fee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelFeeTypeInd")
    private int cancelFeeTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "cancelMethodGroupRuleIdFk", referencedColumnName = "cancelMethodGroupRuleId")
    @ManyToOne(optional = false)
    private CfCancelMethodGroupRule cancelMethodGroupRuleIdFk;

    public CfCancelMethodGroupRuleFee() {
    }

    public CfCancelMethodGroupRuleFee(Integer cancelMethodGroupRuleFeeId) {
        this.cancelMethodGroupRuleFeeId = cancelMethodGroupRuleFeeId;
    }

    public CfCancelMethodGroupRuleFee(Integer cancelMethodGroupRuleFeeId, BigDecimal fee, int cancelFeeTypeInd) {
        this.cancelMethodGroupRuleFeeId = cancelMethodGroupRuleFeeId;
        this.fee = fee;
        this.cancelFeeTypeInd = cancelFeeTypeInd;
    }

    public Integer getCancelMethodGroupRuleFeeId() {
        return cancelMethodGroupRuleFeeId;
    }

    public void setCancelMethodGroupRuleFeeId(Integer cancelMethodGroupRuleFeeId) {
        this.cancelMethodGroupRuleFeeId = cancelMethodGroupRuleFeeId;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public int getCancelFeeTypeInd() {
        return cancelFeeTypeInd;
    }

    public void setCancelFeeTypeInd(int cancelFeeTypeInd) {
        this.cancelFeeTypeInd = cancelFeeTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCancelMethodGroupRule getCancelMethodGroupRuleIdFk() {
        return cancelMethodGroupRuleIdFk;
    }

    public void setCancelMethodGroupRuleIdFk(CfCancelMethodGroupRule cancelMethodGroupRuleIdFk) {
        this.cancelMethodGroupRuleIdFk = cancelMethodGroupRuleIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelMethodGroupRuleFeeId != null ? cancelMethodGroupRuleFeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCancelMethodGroupRuleFee)) {
            return false;
        }
        CfCancelMethodGroupRuleFee other = (CfCancelMethodGroupRuleFee) object;
        if ((this.cancelMethodGroupRuleFeeId == null && other.cancelMethodGroupRuleFeeId != null) || (this.cancelMethodGroupRuleFeeId != null && !this.cancelMethodGroupRuleFeeId.equals(other.cancelMethodGroupRuleFeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCancelMethodGroupRuleFee[ cancelMethodGroupRuleFeeId=" + cancelMethodGroupRuleFeeId + " ]";
    }
    
}
