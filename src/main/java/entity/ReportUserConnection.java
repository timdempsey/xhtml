/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReportUserConnection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportUserConnection.findAll", query = "SELECT r FROM ReportUserConnection r")
    , @NamedQuery(name = "ReportUserConnection.findByReportUserConnectionId", query = "SELECT r FROM ReportUserConnection r WHERE r.reportUserConnectionId = :reportUserConnectionId")
    , @NamedQuery(name = "ReportUserConnection.findByUpdateUserName", query = "SELECT r FROM ReportUserConnection r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReportUserConnection.findByUpdateLast", query = "SELECT r FROM ReportUserConnection r WHERE r.updateLast = :updateLast")})
public class ReportUserConnection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ReportUserConnectionId")
    private Integer reportUserConnectionId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ReportId", referencedColumnName = "reportId")
    @ManyToOne(optional = false)
    private Report reportId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public ReportUserConnection() {
    }

    public ReportUserConnection(Integer reportUserConnectionId) {
        this.reportUserConnectionId = reportUserConnectionId;
    }

    public Integer getReportUserConnectionId() {
        return reportUserConnectionId;
    }

    public void setReportUserConnectionId(Integer reportUserConnectionId) {
        this.reportUserConnectionId = reportUserConnectionId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Report getReportId() {
        return reportId;
    }

    public void setReportId(Report reportId) {
        this.reportId = reportId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportUserConnectionId != null ? reportUserConnectionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportUserConnection)) {
            return false;
        }
        ReportUserConnection other = (ReportUserConnection) object;
        if ((this.reportUserConnectionId == null && other.reportUserConnectionId != null) || (this.reportUserConnectionId != null && !this.reportUserConnectionId.equals(other.reportUserConnectionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReportUserConnection[ reportUserConnectionId=" + reportUserConnectionId + " ]";
    }
    
}
