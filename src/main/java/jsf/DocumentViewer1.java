package jsf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Stanley
 *
 */
@Named(value = "documentViewer1")
@SessionScoped

public class DocumentViewer1 implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(DocumentViewer1.class);
    
   
    @Inject
    transient private ClaimMisc claimMisc;
    

    private StreamedContent content;
    
    private String destfile;

    public String getDestfile() {
        return destfile;
    }

    public void setDestfile(String destfile) {
        this.destfile = destfile;
    }
    

    /**
     * The constructor gets ready for the StreamedContent:
     * Convert a PDF file to content
     */
    public DocumentViewer1() {
        //createStreamedContent();
    }

    public StreamedContent getContent() {
        LOGGER.info("in getContent");
        createStreamedContent();
        return content;
    }

    public void setContent(StreamedContent content) {
        this.content = content;
    }

    public void createStreamedContent() {
        
        
        LOGGER.info("To Show a PDF file on browser ...");
        
        Integer contractId = claimMisc.getLatestInsertedId("Contracts");
        destfile = "C:\\WebSite\\pdfContracts\\" + contractId + ".pdf";
        LOGGER.info("fileName=" + destfile);
        content = new DefaultStreamedContent(getData(destfile), "application/pdf", "downloaded" + destfile);
       
    }

    private InputStream getData(String fileName) {

        File file = new File(fileName);
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
        }
        return is;
    }
}