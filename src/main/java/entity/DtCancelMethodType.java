/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtCancelMethodType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtCancelMethodType.findAll", query = "SELECT d FROM DtCancelMethodType d")
    , @NamedQuery(name = "DtCancelMethodType.findByCancelMethodTypeId", query = "SELECT d FROM DtCancelMethodType d WHERE d.cancelMethodTypeId = :cancelMethodTypeId")
    , @NamedQuery(name = "DtCancelMethodType.findByDescription", query = "SELECT d FROM DtCancelMethodType d WHERE d.description = :description")})
public class DtCancelMethodType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodTypeId")
    private Integer cancelMethodTypeId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelMethodRuleTypeInd")
    private Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelMethodInd")
    private Collection<CancelRequest> cancelRequestCollection;

    public DtCancelMethodType() {
    }

    public DtCancelMethodType(Integer cancelMethodTypeId) {
        this.cancelMethodTypeId = cancelMethodTypeId;
    }

    public Integer getCancelMethodTypeId() {
        return cancelMethodTypeId;
    }

    public void setCancelMethodTypeId(Integer cancelMethodTypeId) {
        this.cancelMethodTypeId = cancelMethodTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroupRule> getCfCancelMethodGroupRuleCollection() {
        return cfCancelMethodGroupRuleCollection;
    }

    public void setCfCancelMethodGroupRuleCollection(Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection) {
        this.cfCancelMethodGroupRuleCollection = cfCancelMethodGroupRuleCollection;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelMethodTypeId != null ? cancelMethodTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtCancelMethodType)) {
            return false;
        }
        DtCancelMethodType other = (DtCancelMethodType) object;
        if ((this.cancelMethodTypeId == null && other.cancelMethodTypeId != null) || (this.cancelMethodTypeId != null && !this.cancelMethodTypeId.equals(other.cancelMethodTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtCancelMethodType[ cancelMethodTypeId=" + cancelMethodTypeId + " ]";
    }
    
}
