/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfHoldReason")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfHoldReason.findAll", query = "SELECT c FROM CfHoldReason c")
    , @NamedQuery(name = "CfHoldReason.findByHoldReasonId", query = "SELECT c FROM CfHoldReason c WHERE c.holdReasonId = :holdReasonId")
    , @NamedQuery(name = "CfHoldReason.findByReason", query = "SELECT c FROM CfHoldReason c WHERE c.reason = :reason")
    , @NamedQuery(name = "CfHoldReason.findByHoldTypeInd", query = "SELECT c FROM CfHoldReason c WHERE c.holdTypeInd = :holdTypeInd")
    , @NamedQuery(name = "CfHoldReason.findByUpdateUserName", query = "SELECT c FROM CfHoldReason c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfHoldReason.findByUpdateLast", query = "SELECT c FROM CfHoldReason c WHERE c.updateLast = :updateLast")})
public class CfHoldReason implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "holdReasonId")
    private Integer holdReasonId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "reason")
    private String reason;
    @Basic(optional = false)
    @NotNull
    @Column(name = "holdTypeInd")
    private int holdTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;

    public CfHoldReason() {
    }

    public CfHoldReason(Integer holdReasonId) {
        this.holdReasonId = holdReasonId;
    }

    public CfHoldReason(Integer holdReasonId, String reason, int holdTypeInd) {
        this.holdReasonId = holdReasonId;
        this.reason = reason;
        this.holdTypeInd = holdTypeInd;
    }

    public Integer getHoldReasonId() {
        return holdReasonId;
    }

    public void setHoldReasonId(Integer holdReasonId) {
        this.holdReasonId = holdReasonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getHoldTypeInd() {
        return holdTypeInd;
    }

    public void setHoldTypeInd(int holdTypeInd) {
        this.holdTypeInd = holdTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (holdReasonId != null ? holdReasonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfHoldReason)) {
            return false;
        }
        CfHoldReason other = (CfHoldReason) object;
        if ((this.holdReasonId == null && other.holdReasonId != null) || (this.holdReasonId != null && !this.holdReasonId.equals(other.holdReasonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfHoldReason[ holdReasonId=" + holdReasonId + " ]";
    }
    
}
