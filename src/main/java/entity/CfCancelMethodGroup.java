/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCancelMethodGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCancelMethodGroup.findAll", query = "SELECT c FROM CfCancelMethodGroup c")
    , @NamedQuery(name = "CfCancelMethodGroup.findByCancelMethodGroupId", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.cancelMethodGroupId = :cancelMethodGroupId")
    , @NamedQuery(name = "CfCancelMethodGroup.findByName", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.name = :name")
    , @NamedQuery(name = "CfCancelMethodGroup.findByCancelMethodGroupTypeInd", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.cancelMethodGroupTypeInd = :cancelMethodGroupTypeInd")
    , @NamedQuery(name = "CfCancelMethodGroup.findByPriority", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.priority = :priority")
    , @NamedQuery(name = "CfCancelMethodGroup.findByContractEffectiveDateStart", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.contractEffectiveDateStart = :contractEffectiveDateStart")
    , @NamedQuery(name = "CfCancelMethodGroup.findByContractEffectiveDateEnd", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.contractEffectiveDateEnd = :contractEffectiveDateEnd")
    , @NamedQuery(name = "CfCancelMethodGroup.findByCancelDateStart", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.cancelDateStart = :cancelDateStart")
    , @NamedQuery(name = "CfCancelMethodGroup.findByCancelDateEnd", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.cancelDateEnd = :cancelDateEnd")
    , @NamedQuery(name = "CfCancelMethodGroup.findByDeletedInd", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfCancelMethodGroup.findByUpdateUserName", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCancelMethodGroup.findByUpdateLast", query = "SELECT c FROM CfCancelMethodGroup c WHERE c.updateLast = :updateLast")})
public class CfCancelMethodGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodGroupId")
    private Integer cancelMethodGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodGroupTypeInd")
    private int cancelMethodGroupTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "priority")
    private int priority;
    @Column(name = "contractEffectiveDateStart")
    @Temporal(TemporalType.DATE)
    private Date contractEffectiveDateStart;
    @Column(name = "contractEffectiveDateEnd")
    @Temporal(TemporalType.DATE)
    private Date contractEffectiveDateEnd;
    @Column(name = "cancelDateStart")
    @Temporal(TemporalType.DATE)
    private Date cancelDateStart;
    @Column(name = "cancelDateEnd")
    @Temporal(TemporalType.DATE)
    private Date cancelDateEnd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelMethodGroupIdFk")
    private Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection;
    @JoinColumn(name = "cancelMethodIdFk", referencedColumnName = "cancelMethodId")
    @ManyToOne(optional = false)
    private CfCancelMethod cancelMethodIdFk;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;

    public CfCancelMethodGroup() {
    }

    public CfCancelMethodGroup(Integer cancelMethodGroupId) {
        this.cancelMethodGroupId = cancelMethodGroupId;
    }

    public CfCancelMethodGroup(Integer cancelMethodGroupId, String name, int cancelMethodGroupTypeInd, int priority, boolean deletedInd) {
        this.cancelMethodGroupId = cancelMethodGroupId;
        this.name = name;
        this.cancelMethodGroupTypeInd = cancelMethodGroupTypeInd;
        this.priority = priority;
        this.deletedInd = deletedInd;
    }

    public Integer getCancelMethodGroupId() {
        return cancelMethodGroupId;
    }

    public void setCancelMethodGroupId(Integer cancelMethodGroupId) {
        this.cancelMethodGroupId = cancelMethodGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCancelMethodGroupTypeInd() {
        return cancelMethodGroupTypeInd;
    }

    public void setCancelMethodGroupTypeInd(int cancelMethodGroupTypeInd) {
        this.cancelMethodGroupTypeInd = cancelMethodGroupTypeInd;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getContractEffectiveDateStart() {
        return contractEffectiveDateStart;
    }

    public void setContractEffectiveDateStart(Date contractEffectiveDateStart) {
        this.contractEffectiveDateStart = contractEffectiveDateStart;
    }

    public Date getContractEffectiveDateEnd() {
        return contractEffectiveDateEnd;
    }

    public void setContractEffectiveDateEnd(Date contractEffectiveDateEnd) {
        this.contractEffectiveDateEnd = contractEffectiveDateEnd;
    }

    public Date getCancelDateStart() {
        return cancelDateStart;
    }

    public void setCancelDateStart(Date cancelDateStart) {
        this.cancelDateStart = cancelDateStart;
    }

    public Date getCancelDateEnd() {
        return cancelDateEnd;
    }

    public void setCancelDateEnd(Date cancelDateEnd) {
        this.cancelDateEnd = cancelDateEnd;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroupRule> getCfCancelMethodGroupRuleCollection() {
        return cfCancelMethodGroupRuleCollection;
    }

    public void setCfCancelMethodGroupRuleCollection(Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection) {
        this.cfCancelMethodGroupRuleCollection = cfCancelMethodGroupRuleCollection;
    }

    public CfCancelMethod getCancelMethodIdFk() {
        return cancelMethodIdFk;
    }

    public void setCancelMethodIdFk(CfCancelMethod cancelMethodIdFk) {
        this.cancelMethodIdFk = cancelMethodIdFk;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelMethodGroupId != null ? cancelMethodGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCancelMethodGroup)) {
            return false;
        }
        CfCancelMethodGroup other = (CfCancelMethodGroup) object;
        if ((this.cancelMethodGroupId == null && other.cancelMethodGroupId != null) || (this.cancelMethodGroupId != null && !this.cancelMethodGroupId.equals(other.cancelMethodGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCancelMethodGroup[ cancelMethodGroupId=" + cancelMethodGroupId + " ]";
    }
    
}
