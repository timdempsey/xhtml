/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimViewLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimViewLog.findAll", query = "SELECT c FROM ClaimViewLog c")
    , @NamedQuery(name = "ClaimViewLog.findByClaimViewLogId", query = "SELECT c FROM ClaimViewLog c WHERE c.claimViewLogId = :claimViewLogId")
    , @NamedQuery(name = "ClaimViewLog.findByViewedDate", query = "SELECT c FROM ClaimViewLog c WHERE c.viewedDate = :viewedDate")
    , @NamedQuery(name = "ClaimViewLog.findByUpdateUserName", query = "SELECT c FROM ClaimViewLog c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimViewLog.findByUpdateLast", query = "SELECT c FROM ClaimViewLog c WHERE c.updateLast = :updateLast")})
public class ClaimViewLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimViewLogId")
    private Integer claimViewLogId;
    @Column(name = "viewedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date viewedDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "viewedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember viewedByIdFk;

    public ClaimViewLog() {
    }

    public ClaimViewLog(Integer claimViewLogId) {
        this.claimViewLogId = claimViewLogId;
    }

    public Integer getClaimViewLogId() {
        return claimViewLogId;
    }

    public void setClaimViewLogId(Integer claimViewLogId) {
        this.claimViewLogId = claimViewLogId;
    }

    public Date getViewedDate() {
        return viewedDate;
    }

    public void setViewedDate(Date viewedDate) {
        this.viewedDate = viewedDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public UserMember getViewedByIdFk() {
        return viewedByIdFk;
    }

    public void setViewedByIdFk(UserMember viewedByIdFk) {
        this.viewedByIdFk = viewedByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimViewLogId != null ? claimViewLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimViewLog)) {
            return false;
        }
        ClaimViewLog other = (ClaimViewLog) object;
        if ((this.claimViewLogId == null && other.claimViewLogId != null) || (this.claimViewLogId != null && !this.claimViewLogId.equals(other.claimViewLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimViewLog[ claimViewLogId=" + claimViewLogId + " ]";
    }
    
}
