/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCriterionGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCriterionGroup.findAll", query = "SELECT c FROM CfCriterionGroup c")
    , @NamedQuery(name = "CfCriterionGroup.findByCriterionGroupId", query = "SELECT c FROM CfCriterionGroup c WHERE c.criterionGroupId = :criterionGroupId")
    , @NamedQuery(name = "CfCriterionGroup.findByAndRelationshipInd", query = "SELECT c FROM CfCriterionGroup c WHERE c.andRelationshipInd = :andRelationshipInd")
    , @NamedQuery(name = "CfCriterionGroup.findByDescription", query = "SELECT c FROM CfCriterionGroup c WHERE c.description = :description")
    , @NamedQuery(name = "CfCriterionGroup.findByTranslated", query = "SELECT c FROM CfCriterionGroup c WHERE c.translated = :translated")
    , @NamedQuery(name = "CfCriterionGroup.findByUpdateUserName", query = "SELECT c FROM CfCriterionGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCriterionGroup.findByUpdateLast", query = "SELECT c FROM CfCriterionGroup c WHERE c.updateLast = :updateLast")})
public class CfCriterionGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "criterionGroupId")
    private Integer criterionGroupId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "andRelationshipInd")
    private boolean andRelationshipInd;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 2147483647)
    @Column(name = "translated")
    private String translated;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "criterionGroupIdFk")
    private Collection<CfCriterion> cfCriterionCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfCriterionGroup> cfCriterionGroupCollection;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfField> cfFieldCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<MarkupDetail> markupDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<DeductibleDetail> deductibleDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfNumberGeneration> cfNumberGenerationCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<AdjustmentDetail> adjustmentDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfCancelMethod> cfCancelMethodCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<DealerSRP> dealerSRPCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<SurchargeDetail> surchargeDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfPDFDetail> cfPDFDetailCollection;
    @OneToMany(mappedBy = "criterionGroupIdFk")
    private Collection<CfCancelMethodGroup> cfCancelMethodGroupCollection;

    public CfCriterionGroup() {
    }

    public CfCriterionGroup(Integer criterionGroupId) {
        this.criterionGroupId = criterionGroupId;
    }

    public CfCriterionGroup(Integer criterionGroupId, boolean andRelationshipInd) {
        this.criterionGroupId = criterionGroupId;
        this.andRelationshipInd = andRelationshipInd;
    }

    public Integer getCriterionGroupId() {
        return criterionGroupId;
    }

    public void setCriterionGroupId(Integer criterionGroupId) {
        this.criterionGroupId = criterionGroupId;
    }

    public boolean getAndRelationshipInd() {
        return andRelationshipInd;
    }

    public void setAndRelationshipInd(boolean andRelationshipInd) {
        this.andRelationshipInd = andRelationshipInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfCriterion> getCfCriterionCollection() {
        return cfCriterionCollection;
    }

    public void setCfCriterionCollection(Collection<CfCriterion> cfCriterionCollection) {
        this.cfCriterionCollection = cfCriterionCollection;
    }

    @XmlTransient
    public Collection<CfCriterionGroup> getCfCriterionGroupCollection() {
        return cfCriterionGroupCollection;
    }

    public void setCfCriterionGroupCollection(Collection<CfCriterionGroup> cfCriterionGroupCollection) {
        this.cfCriterionGroupCollection = cfCriterionGroupCollection;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    @XmlTransient
    public Collection<CfField> getCfFieldCollection() {
        return cfFieldCollection;
    }

    public void setCfFieldCollection(Collection<CfField> cfFieldCollection) {
        this.cfFieldCollection = cfFieldCollection;
    }

    @XmlTransient
    public Collection<MarkupDetail> getMarkupDetailCollection() {
        return markupDetailCollection;
    }

    public void setMarkupDetailCollection(Collection<MarkupDetail> markupDetailCollection) {
        this.markupDetailCollection = markupDetailCollection;
    }

    @XmlTransient
    public Collection<DeductibleDetail> getDeductibleDetailCollection() {
        return deductibleDetailCollection;
    }

    public void setDeductibleDetailCollection(Collection<DeductibleDetail> deductibleDetailCollection) {
        this.deductibleDetailCollection = deductibleDetailCollection;
    }

    @XmlTransient
    public Collection<CfNumberGeneration> getCfNumberGenerationCollection() {
        return cfNumberGenerationCollection;
    }

    public void setCfNumberGenerationCollection(Collection<CfNumberGeneration> cfNumberGenerationCollection) {
        this.cfNumberGenerationCollection = cfNumberGenerationCollection;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @XmlTransient
    public Collection<AdjustmentDetail> getAdjustmentDetailCollection() {
        return adjustmentDetailCollection;
    }

    public void setAdjustmentDetailCollection(Collection<AdjustmentDetail> adjustmentDetailCollection) {
        this.adjustmentDetailCollection = adjustmentDetailCollection;
    }

    @XmlTransient
    public Collection<CfCancelMethod> getCfCancelMethodCollection() {
        return cfCancelMethodCollection;
    }

    public void setCfCancelMethodCollection(Collection<CfCancelMethod> cfCancelMethodCollection) {
        this.cfCancelMethodCollection = cfCancelMethodCollection;
    }

    @XmlTransient
    public Collection<DealerSRP> getDealerSRPCollection() {
        return dealerSRPCollection;
    }

    public void setDealerSRPCollection(Collection<DealerSRP> dealerSRPCollection) {
        this.dealerSRPCollection = dealerSRPCollection;
    }

    @XmlTransient
    public Collection<SurchargeDetail> getSurchargeDetailCollection() {
        return surchargeDetailCollection;
    }

    public void setSurchargeDetailCollection(Collection<SurchargeDetail> surchargeDetailCollection) {
        this.surchargeDetailCollection = surchargeDetailCollection;
    }

    @XmlTransient
    public Collection<CfPDFDetail> getCfPDFDetailCollection() {
        return cfPDFDetailCollection;
    }

    public void setCfPDFDetailCollection(Collection<CfPDFDetail> cfPDFDetailCollection) {
        this.cfPDFDetailCollection = cfPDFDetailCollection;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroup> getCfCancelMethodGroupCollection() {
        return cfCancelMethodGroupCollection;
    }

    public void setCfCancelMethodGroupCollection(Collection<CfCancelMethodGroup> cfCancelMethodGroupCollection) {
        this.cfCancelMethodGroupCollection = cfCancelMethodGroupCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterionGroupId != null ? criterionGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCriterionGroup)) {
            return false;
        }
        CfCriterionGroup other = (CfCriterionGroup) object;
        if ((this.criterionGroupId == null && other.criterionGroupId != null) || (this.criterionGroupId != null && !this.criterionGroupId.equals(other.criterionGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCriterionGroup[ criterionGroupId=" + criterionGroupId + " ]";
    }
    
}
