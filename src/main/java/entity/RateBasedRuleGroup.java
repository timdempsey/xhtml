/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateBasedRuleGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateBasedRuleGroup.findAll", query = "SELECT r FROM RateBasedRuleGroup r")
    , @NamedQuery(name = "RateBasedRuleGroup.findByRateBasedRuleGroupId", query = "SELECT r FROM RateBasedRuleGroup r WHERE r.rateBasedRuleGroupId = :rateBasedRuleGroupId")
    , @NamedQuery(name = "RateBasedRuleGroup.findByName", query = "SELECT r FROM RateBasedRuleGroup r WHERE r.name = :name")
    , @NamedQuery(name = "RateBasedRuleGroup.findByRateBasedRuleGroupTypeInd", query = "SELECT r FROM RateBasedRuleGroup r WHERE r.rateBasedRuleGroupTypeInd = :rateBasedRuleGroupTypeInd")
    , @NamedQuery(name = "RateBasedRuleGroup.findByUpdateUserName", query = "SELECT r FROM RateBasedRuleGroup r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateBasedRuleGroup.findByUpdateLast", query = "SELECT r FROM RateBasedRuleGroup r WHERE r.updateLast = :updateLast")})
public class RateBasedRuleGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateBasedRuleGroupId")
    private Integer rateBasedRuleGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateBasedRuleGroupTypeInd")
    private int rateBasedRuleGroupTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "rateBasedRuleGroupIdFk")
    private Collection<RateBasedRule> rateBasedRuleCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "rateBasedRuleGroup")
    private CfDisbursementRemitRule cfDisbursementRemitRule;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "rateBasedRuleGroup")
    private CfNumberGeneration cfNumberGeneration;
    @OneToMany(mappedBy = "rateBasedRuleGroupIdFk")
    private Collection<WarrantyRule> warrantyRuleCollection;
    @OneToMany(mappedBy = "applicableProductIdFk")
    private Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection;
    @OneToMany(mappedBy = "applicableProductIdFk")
    private Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection;
    @OneToMany(mappedBy = "rateBasedRuleGroupIdFk")
    private Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "rateBasedRuleGroup")
    private RateVisibility rateVisibility;
    @OneToMany(mappedBy = "applicableProductIdFk")
    private Collection<ReinsuranceFee> reinsuranceFeeCollection;

    public RateBasedRuleGroup() {
    }

    public RateBasedRuleGroup(Integer rateBasedRuleGroupId) {
        this.rateBasedRuleGroupId = rateBasedRuleGroupId;
    }

    public RateBasedRuleGroup(Integer rateBasedRuleGroupId, String name, int rateBasedRuleGroupTypeInd) {
        this.rateBasedRuleGroupId = rateBasedRuleGroupId;
        this.name = name;
        this.rateBasedRuleGroupTypeInd = rateBasedRuleGroupTypeInd;
    }

    public Integer getRateBasedRuleGroupId() {
        return rateBasedRuleGroupId;
    }

    public void setRateBasedRuleGroupId(Integer rateBasedRuleGroupId) {
        this.rateBasedRuleGroupId = rateBasedRuleGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRateBasedRuleGroupTypeInd() {
        return rateBasedRuleGroupTypeInd;
    }

    public void setRateBasedRuleGroupTypeInd(int rateBasedRuleGroupTypeInd) {
        this.rateBasedRuleGroupTypeInd = rateBasedRuleGroupTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<RateBasedRule> getRateBasedRuleCollection() {
        return rateBasedRuleCollection;
    }

    public void setRateBasedRuleCollection(Collection<RateBasedRule> rateBasedRuleCollection) {
        this.rateBasedRuleCollection = rateBasedRuleCollection;
    }

    public CfDisbursementRemitRule getCfDisbursementRemitRule() {
        return cfDisbursementRemitRule;
    }

    public void setCfDisbursementRemitRule(CfDisbursementRemitRule cfDisbursementRemitRule) {
        this.cfDisbursementRemitRule = cfDisbursementRemitRule;
    }

    public CfNumberGeneration getCfNumberGeneration() {
        return cfNumberGeneration;
    }

    public void setCfNumberGeneration(CfNumberGeneration cfNumberGeneration) {
        this.cfNumberGeneration = cfNumberGeneration;
    }

    @XmlTransient
    public Collection<WarrantyRule> getWarrantyRuleCollection() {
        return warrantyRuleCollection;
    }

    public void setWarrantyRuleCollection(Collection<WarrantyRule> warrantyRuleCollection) {
        this.warrantyRuleCollection = warrantyRuleCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceCustodialAccountBalance> getReinsuranceCustodialAccountBalanceCollection() {
        return reinsuranceCustodialAccountBalanceCollection;
    }

    public void setReinsuranceCustodialAccountBalanceCollection(Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection) {
        this.reinsuranceCustodialAccountBalanceCollection = reinsuranceCustodialAccountBalanceCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceContractFee> getReinsuranceContractFeeCollection() {
        return reinsuranceContractFeeCollection;
    }

    public void setReinsuranceContractFeeCollection(Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection) {
        this.reinsuranceContractFeeCollection = reinsuranceContractFeeCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceRuleDetail> getReinsuranceRuleDetailCollection() {
        return reinsuranceRuleDetailCollection;
    }

    public void setReinsuranceRuleDetailCollection(Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection) {
        this.reinsuranceRuleDetailCollection = reinsuranceRuleDetailCollection;
    }

    public RateVisibility getRateVisibility() {
        return rateVisibility;
    }

    public void setRateVisibility(RateVisibility rateVisibility) {
        this.rateVisibility = rateVisibility;
    }

    @XmlTransient
    public Collection<ReinsuranceFee> getReinsuranceFeeCollection() {
        return reinsuranceFeeCollection;
    }

    public void setReinsuranceFeeCollection(Collection<ReinsuranceFee> reinsuranceFeeCollection) {
        this.reinsuranceFeeCollection = reinsuranceFeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateBasedRuleGroupId != null ? rateBasedRuleGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateBasedRuleGroup)) {
            return false;
        }
        RateBasedRuleGroup other = (RateBasedRuleGroup) object;
        if ((this.rateBasedRuleGroupId == null && other.rateBasedRuleGroupId != null) || (this.rateBasedRuleGroupId != null && !this.rateBasedRuleGroupId.equals(other.rateBasedRuleGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateBasedRuleGroup[ rateBasedRuleGroupId=" + rateBasedRuleGroupId + " ]";
    }
    
}
