/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ProductSubType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductSubType.findAll", query = "SELECT p FROM ProductSubType p")
    , @NamedQuery(name = "ProductSubType.findByProductSubTypeId", query = "SELECT p FROM ProductSubType p WHERE p.productSubTypeId = :productSubTypeId")
    , @NamedQuery(name = "ProductSubType.findByDescription", query = "SELECT p FROM ProductSubType p WHERE p.description = :description")})
public class ProductSubType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ProductSubTypeId")
    private Integer productSubTypeId;
    @Size(max = 30)
    @Column(name = "Description")
    private String description;

    public ProductSubType() {
    }

    public ProductSubType(Integer productSubTypeId) {
        this.productSubTypeId = productSubTypeId;
    }

    public Integer getProductSubTypeId() {
        return productSubTypeId;
    }

    public void setProductSubTypeId(Integer productSubTypeId) {
        this.productSubTypeId = productSubTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productSubTypeId != null ? productSubTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductSubType)) {
            return false;
        }
        ProductSubType other = (ProductSubType) object;
        if ((this.productSubTypeId == null && other.productSubTypeId != null) || (this.productSubTypeId != null && !this.productSubTypeId.equals(other.productSubTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProductSubType[ productSubTypeId=" + productSubTypeId + " ]";
    }
    
}
