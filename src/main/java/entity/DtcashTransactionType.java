/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtcashTransactionType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtcashTransactionType.findAll", query = "SELECT d FROM DtcashTransactionType d")
    , @NamedQuery(name = "DtcashTransactionType.findByCashTransactionTypeId", query = "SELECT d FROM DtcashTransactionType d WHERE d.cashTransactionTypeId = :cashTransactionTypeId")
    , @NamedQuery(name = "DtcashTransactionType.findByDescription", query = "SELECT d FROM DtcashTransactionType d WHERE d.description = :description")})
public class DtcashTransactionType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cashTransactionTypeId")
    private Integer cashTransactionTypeId;
    @Size(max = 60)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cashTransactionTypeInd")
    private Collection<CashTransaction> cashTransactionCollection;

    public DtcashTransactionType() {
    }

    public DtcashTransactionType(Integer cashTransactionTypeId) {
        this.cashTransactionTypeId = cashTransactionTypeId;
    }

    public Integer getCashTransactionTypeId() {
        return cashTransactionTypeId;
    }

    public void setCashTransactionTypeId(Integer cashTransactionTypeId) {
        this.cashTransactionTypeId = cashTransactionTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CashTransaction> getCashTransactionCollection() {
        return cashTransactionCollection;
    }

    public void setCashTransactionCollection(Collection<CashTransaction> cashTransactionCollection) {
        this.cashTransactionCollection = cashTransactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cashTransactionTypeId != null ? cashTransactionTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtcashTransactionType)) {
            return false;
        }
        DtcashTransactionType other = (DtcashTransactionType) object;
        if ((this.cashTransactionTypeId == null && other.cashTransactionTypeId != null) || (this.cashTransactionTypeId != null && !this.cashTransactionTypeId.equals(other.cashTransactionTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtcashTransactionType[ cashTransactionTypeId=" + cashTransactionTypeId + " ]";
    }
    
}
