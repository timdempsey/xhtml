/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "VehiclePurchaseType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehiclePurchaseType.findAll", query = "SELECT v FROM VehiclePurchaseType v")
    , @NamedQuery(name = "VehiclePurchaseType.findByVehiclePurchaseTypeId", query = "SELECT v FROM VehiclePurchaseType v WHERE v.vehiclePurchaseTypeId = :vehiclePurchaseTypeId")
    , @NamedQuery(name = "VehiclePurchaseType.findByDescription", query = "SELECT v FROM VehiclePurchaseType v WHERE v.description = :description")})
public class VehiclePurchaseType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vehiclePurchaseTypeId")
    private Integer vehiclePurchaseTypeId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public VehiclePurchaseType() {
    }

    public VehiclePurchaseType(Integer vehiclePurchaseTypeId) {
        this.vehiclePurchaseTypeId = vehiclePurchaseTypeId;
    }

    public Integer getVehiclePurchaseTypeId() {
        return vehiclePurchaseTypeId;
    }

    public void setVehiclePurchaseTypeId(Integer vehiclePurchaseTypeId) {
        this.vehiclePurchaseTypeId = vehiclePurchaseTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehiclePurchaseTypeId != null ? vehiclePurchaseTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehiclePurchaseType)) {
            return false;
        }
        VehiclePurchaseType other = (VehiclePurchaseType) object;
        if ((this.vehiclePurchaseTypeId == null && other.vehiclePurchaseTypeId != null) || (this.vehiclePurchaseTypeId != null && !this.vehiclePurchaseTypeId.equals(other.vehiclePurchaseTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VehiclePurchaseType[ vehiclePurchaseTypeId=" + vehiclePurchaseTypeId + " ]";
    }
    
}
