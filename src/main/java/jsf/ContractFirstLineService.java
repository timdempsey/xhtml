/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Contracts;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractFirstLineService")
@ApplicationScoped
public class ContractFirstLineService implements Serializable {

    /**
     * Creates a new instance of ContractFirstLineService
     */
    public ContractFirstLineService() {
    }

    private static final Logger LOGGER = LogManager.getLogger(ContractFirstLineService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;

    /**
     *
     * @param cl
     * @return
     */
    public List<?> createContractView(List<Contracts> cl) {

        List<ContractFirstLine> list = new ArrayList<>();

        for (Contracts contract : cl) {

            String contractNo = contract.getContractNo();
            //LOGGER.info("createClaimView claimNumber=" + claimNumber);

            Integer contractId = contract.getContractId();
            String product = null;
            String productType = null;
            if (contract.getProductIdFk() != null) {
                product = contract.getProductIdFk().getProductName();
                productType = contract.getProductIdFk().getProductTypeIdFk().getProductTypeName();
            }

            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String effetiveDate = null;
            String expirationDate = null;

            if (contract.getExpirationDate() != null) {
                expirationDate = df.format(contract.getExpirationDate());
            }
            if (contract.getEffectiveDate() != null) {
                effetiveDate = df.format(contract.getEffectiveDate());
            }
            
            String agent=null;
            AccountKeeper ak = claimMisc.getAgentByAccountKeeperByContractId(contractId);
            if ( ak != null ) {
                agent = ak.getAccountKeeperName();
            }

            
            list.add(new ContractFirstLine(
                    product, 
                    productType, 
                    effetiveDate, 
                    expirationDate, 
                    contractNo, 
                    contract.getContractStatusIdFk().getContractStatusName(), 
                    claimMisc.getCustomerNameByAccountKeeperUsingContractId(contractId).getAccountKeeperName(), 
                    contract.getVinFull(),
                    claimMisc.getDealerByAccountKeeperByContractId(contractId).getAccountKeeperName(), 
                    agent,
                    contract.getContractPurchasePrice()));
            //LOGGER.info("======end of loop for " + claimNumber);

        }

        LOGGER.info("size of list=" + list.size());
        return list;
    }

}
