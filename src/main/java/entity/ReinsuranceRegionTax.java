/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceRegionTax")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceRegionTax.findAll", query = "SELECT r FROM ReinsuranceRegionTax r")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByReinsuranceRegionTaxId", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.reinsuranceRegionTaxId = :reinsuranceRegionTaxId")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByPremiumTaxPercent", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.premiumTaxPercent = :premiumTaxPercent")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByFederalExciseTaxPercent", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.federalExciseTaxPercent = :federalExciseTaxPercent")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByDateEffective", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.dateEffective = :dateEffective")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByDateExpires", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.dateExpires = :dateExpires")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByUpdateUserName", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceRegionTax.findByUpdateLast", query = "SELECT r FROM ReinsuranceRegionTax r WHERE r.updateLast = :updateLast")})
public class ReinsuranceRegionTax implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceRegionTaxId")
    private Integer reinsuranceRegionTaxId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "premiumTaxPercent")
    private BigDecimal premiumTaxPercent;
    @Column(name = "federalExciseTaxPercent")
    private BigDecimal federalExciseTaxPercent;
    @Column(name = "dateEffective")
    @Temporal(TemporalType.DATE)
    private Date dateEffective;
    @Column(name = "DateExpires")
    @Temporal(TemporalType.DATE)
    private Date dateExpires;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "regionIdFk", referencedColumnName = "regionId")
    @ManyToOne
    private CfRegion regionIdFk;

    public ReinsuranceRegionTax() {
    }

    public ReinsuranceRegionTax(Integer reinsuranceRegionTaxId) {
        this.reinsuranceRegionTaxId = reinsuranceRegionTaxId;
    }

    public Integer getReinsuranceRegionTaxId() {
        return reinsuranceRegionTaxId;
    }

    public void setReinsuranceRegionTaxId(Integer reinsuranceRegionTaxId) {
        this.reinsuranceRegionTaxId = reinsuranceRegionTaxId;
    }

    public BigDecimal getPremiumTaxPercent() {
        return premiumTaxPercent;
    }

    public void setPremiumTaxPercent(BigDecimal premiumTaxPercent) {
        this.premiumTaxPercent = premiumTaxPercent;
    }

    public BigDecimal getFederalExciseTaxPercent() {
        return federalExciseTaxPercent;
    }

    public void setFederalExciseTaxPercent(BigDecimal federalExciseTaxPercent) {
        this.federalExciseTaxPercent = federalExciseTaxPercent;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfRegion getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(CfRegion regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceRegionTaxId != null ? reinsuranceRegionTaxId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceRegionTax)) {
            return false;
        }
        ReinsuranceRegionTax other = (ReinsuranceRegionTax) object;
        if ((this.reinsuranceRegionTaxId == null && other.reinsuranceRegionTaxId != null) || (this.reinsuranceRegionTaxId != null && !this.reinsuranceRegionTaxId.equals(other.reinsuranceRegionTaxId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceRegionTax[ reinsuranceRegionTaxId=" + reinsuranceRegionTaxId + " ]";
    }
    
}
