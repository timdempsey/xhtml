/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractAdjustment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractAdjustment.findAll", query = "SELECT c FROM ContractAdjustment c")
    , @NamedQuery(name = "ContractAdjustment.findByContractAdjustmentId", query = "SELECT c FROM ContractAdjustment c WHERE c.contractAdjustmentId = :contractAdjustmentId")
    , @NamedQuery(name = "ContractAdjustment.findByAffectsCoverageCost", query = "SELECT c FROM ContractAdjustment c WHERE c.affectsCoverageCost = :affectsCoverageCost")
    , @NamedQuery(name = "ContractAdjustment.findByUpdateUserName", query = "SELECT c FROM ContractAdjustment c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractAdjustment.findByUpdateLast", query = "SELECT c FROM ContractAdjustment c WHERE c.updateLast = :updateLast")})
public class ContractAdjustment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractAdjustmentId")
    private Integer contractAdjustmentId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "affectsCoverageCost")
    private boolean affectsCoverageCost;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "adjustmentIdFk", referencedColumnName = "adjustmentId")
    @ManyToOne
    private Adjustment adjustmentIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;

    public ContractAdjustment() {
    }

    public ContractAdjustment(Integer contractAdjustmentId) {
        this.contractAdjustmentId = contractAdjustmentId;
    }

    public ContractAdjustment(Integer contractAdjustmentId, boolean affectsCoverageCost) {
        this.contractAdjustmentId = contractAdjustmentId;
        this.affectsCoverageCost = affectsCoverageCost;
    }

    public Integer getContractAdjustmentId() {
        return contractAdjustmentId;
    }

    public void setContractAdjustmentId(Integer contractAdjustmentId) {
        this.contractAdjustmentId = contractAdjustmentId;
    }

    public boolean getAffectsCoverageCost() {
        return affectsCoverageCost;
    }

    public void setAffectsCoverageCost(boolean affectsCoverageCost) {
        this.affectsCoverageCost = affectsCoverageCost;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Adjustment getAdjustmentIdFk() {
        return adjustmentIdFk;
    }

    public void setAdjustmentIdFk(Adjustment adjustmentIdFk) {
        this.adjustmentIdFk = adjustmentIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractAdjustmentId != null ? contractAdjustmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractAdjustment)) {
            return false;
        }
        ContractAdjustment other = (ContractAdjustment) object;
        if ((this.contractAdjustmentId == null && other.contractAdjustmentId != null) || (this.contractAdjustmentId != null && !this.contractAdjustmentId.equals(other.contractAdjustmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractAdjustment[ contractAdjustmentId=" + contractAdjustmentId + " ]";
    }
    
}
