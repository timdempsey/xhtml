/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCancelReason")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCancelReason.findAll", query = "SELECT c FROM CfCancelReason c")
    , @NamedQuery(name = "CfCancelReason.findByCancelReasonId", query = "SELECT c FROM CfCancelReason c WHERE c.cancelReasonId = :cancelReasonId")
    , @NamedQuery(name = "CfCancelReason.findByReason", query = "SELECT c FROM CfCancelReason c WHERE c.reason = :reason")
    , @NamedQuery(name = "CfCancelReason.findByCode", query = "SELECT c FROM CfCancelReason c WHERE c.code = :code")
    , @NamedQuery(name = "CfCancelReason.findByMessage", query = "SELECT c FROM CfCancelReason c WHERE c.message = :message")
    , @NamedQuery(name = "CfCancelReason.findByUpdateUserName", query = "SELECT c FROM CfCancelReason c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCancelReason.findByUpdateLast", query = "SELECT c FROM CfCancelReason c WHERE c.updateLast = :updateLast")})
public class CfCancelReason implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CancelReasonId")
    private Integer cancelReasonId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Reason")
    private String reason;
    @Size(max = 50)
    @Column(name = "Code")
    private String code;
    @Size(max = 2147483647)
    @Column(name = "Message")
    private String message;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelReasonIdFk")
    private Collection<CancelRequest> cancelRequestCollection;
    @OneToMany(mappedBy = "cancelReasonIdFk")
    private Collection<CfCancelMethod> cfCancelMethodCollection;

    public CfCancelReason() {
    }

    public CfCancelReason(Integer cancelReasonId) {
        this.cancelReasonId = cancelReasonId;
    }

    public CfCancelReason(Integer cancelReasonId, String reason) {
        this.cancelReasonId = cancelReasonId;
        this.reason = reason;
    }

    public Integer getCancelReasonId() {
        return cancelReasonId;
    }

    public void setCancelReasonId(Integer cancelReasonId) {
        this.cancelReasonId = cancelReasonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @XmlTransient
    public Collection<CfCancelMethod> getCfCancelMethodCollection() {
        return cfCancelMethodCollection;
    }

    public void setCfCancelMethodCollection(Collection<CfCancelMethod> cfCancelMethodCollection) {
        this.cfCancelMethodCollection = cfCancelMethodCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelReasonId != null ? cancelReasonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCancelReason)) {
            return false;
        }
        CfCancelReason other = (CfCancelReason) object;
        if ((this.cancelReasonId == null && other.cancelReasonId != null) || (this.cancelReasonId != null && !this.cancelReasonId.equals(other.cancelReasonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCancelReason[ cancelReasonId=" + cancelReasonId + " ]";
    }
    
}
