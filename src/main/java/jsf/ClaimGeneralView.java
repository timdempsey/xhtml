/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimGeneralView")
@ApplicationScoped
public class ClaimGeneralView {

    private static final Logger LOGGER = LogManager.getLogger(ClaimGeneralView.class);

    @PersistenceContext // JSF m
    private EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimGeneralView
     */
    public ClaimGeneralView() {
    }

    @Inject
    ClaimGeneralService service;
    @Inject
    transient private ClaimMisc claimMisc;
    ClaimGeneral cg;
    

    @PostConstruct
    public void init() {
        LOGGER.info("init from ClaimGeneralView, claimNumber=" + service.getClaimNumber());

        cg = service.createClaimView(service.getClaimNumber());
    }

    public ClaimGeneral getCg() {
        return cg;
    }

    public void setCg(ClaimGeneral cg) {
        this.cg = cg;
    }

    public void save() {
        LOGGER.info("in saving...");
        //Claim claim = new Claim();

        try {

            utx.begin();
            Integer akid = null;
            Integer repairFacilityContactIdFk = null;
            RepairFacilityContactPerson rfcp = null;
            if (cg.getRfName() != null && cg.getRfName().length() > 0) {
                LOGGER.info("rfname=" + cg.getRfName());

                AccountKeeper ak = claimMisc.getAccoutKeeperByRFname(cg.getRfName());
                akid = ak.getAccountKeeperId();
                LOGGER.info("accountKeeperId=" + akid);

                if (cg.getContactName() != null && cg.getContactName().length() > 0) {
                    LOGGER.info("inside rfcp length=" + cg.getContactName().length());
                    List<RepairFacilityContactPerson> rfcps = (List<RepairFacilityContactPerson>) em.createNativeQuery("select b.* from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk", RepairFacilityContactPerson.class).setParameter(1, akid).getResultList();
                    LOGGER.info("length of rfcps=" + rfcps.size());
                    if (rfcps.size() != 0) {
                        for (RepairFacilityContactPerson rp : rfcps) {
                            LOGGER.info("rp=" + rp.getPhoneNumber());
                            rp.setName(cg.getContactName());
                            rp.setPhoneNumber(cg.getContactPhone());
                            em.merge(rp);
                            rfcp = rp;
                            repairFacilityContactIdFk = rfcp.getRepairFacilityContactPersonId();
                            LOGGER.info("merge rfcp");
                            LOGGER.info("rfcp=" + repairFacilityContactIdFk);
                            break;
                        }
                    } else {    // add new record in RepairFacilityContactPerson
                        LOGGER.info("new rfcp, 2=" + cg.getContactPhone());
                        Query query = em.createNativeQuery("insert into RepairFacilityContactPerson (name, PhoneNumber, repairFacilityIdFk) values (?1, ?2, ?3)");
                        query.setParameter(1, cg.getContactName());
                        query.setParameter(2, cg.getContactPhone());
                        query.setParameter(3, akid);
                        int ret = query.executeUpdate();
                        LOGGER.info("insert to rfcp, ret=" + ret);
                        repairFacilityContactIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('RepairFacilityContactPerson')").getSingleResult()).intValueExact();
                        rfcp = em.createNamedQuery("RepairFacilityContactPerson.findByRepairFacilityContactPersonId", RepairFacilityContactPerson.class).setParameter("repairFacilityContactPersonId", repairFacilityContactIdFk).getSingleResult();
                        LOGGER.info("latest id=" + repairFacilityContactIdFk);
                    }
                }
            }

            if (cg.getClaimNumber().contains("TBD")) {  // New Claim
                int ret;
                LOGGER.info("inserting new claim...");
                Query query = em.createNativeQuery("insert into Claim (currentOdometer, repairOrderNumber, repairOrderDate, repairFacilityIdFk, repairFacilityContactIdFk, productTypeIdFk, claimStatus, claimSubStatus) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)");
                query.setParameter(1, cg.getOdometer());
                query.setParameter(2, cg.getRoNumber());
                query.setParameter(3, claimMisc.getCurrentDate());
                LOGGER.info("akid=" + akid);
                LOGGER.info("repairFacilityContactIdFk=" + repairFacilityContactIdFk);
                query.setParameter(4, akid);
                query.setParameter(5, repairFacilityContactIdFk);
                query.setParameter(6, 1);   // VSC
                query.setParameter(7, 1);   //  1, Pending
                query.setParameter(8, 1);   //  1, Pending Approval
                ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
            } else {    // Existing Claim
                Claim claim = claimMisc.getClaimByNumber(cg.getClaimNumber());
                LOGGER.info("id=" + claim.getClaimId());
                claim.setCurrentOdometer(cg.getOdometer());
                claim.setRepairOrderNumber(cg.getRoNumber());
                claim.setRepairOrderDate(claimMisc.convSTDateWS(cg.getRoDate()));
                RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                claim.setRepairFacilityIdFk(rf);
                claim.setRepairFacilityContactIdFk(rfcp);
                em.merge(claim);
                LOGGER.info("merge claim");
            }

            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

    }

    public List<ClaimGeneral> getCGsByRFname(String rfname) {

        try {
            List<ClaimGeneral> lcg = new ArrayList<>();
            Claim claim = claimMisc.getClaimByNumber(cg.getClaimNumber());

            List<Object[]> ls = new ArrayList<>();
            ls = em.createNativeQuery("select * from AccountKeeper a, RepairFacility b, Address c, Region d where a.accountKeeperName like ?1 and a.accountKeeperId=b.repairFacilityId and c.regionIdFk= d.regionId and  c.addressId=a.physicalAddressIdFk").setParameter(1, rfname + "%").getResultList();
            for (Object obj[] : ls) {
                AccountKeeper ak = (AccountKeeper) obj[0];
                RepairFacility rf = (RepairFacility) obj[1];
                Address addr = (Address) obj[2];
                CfRegion region = (CfRegion) obj[3];

                List<RepairFacilityContactPerson> rfcps = em.createNativeQuery("select b.* from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk").setParameter(1, rf.getRepairFacilityId()).getResultList();
                if (rfcps != null) {
                    for (RepairFacilityContactPerson rfcp : rfcps) {
                        LOGGER.info("rfcp=" + rfcp.getPhoneNumber());
                        cg.setContactName(rfcp.getName());
                        cg.setContactPhone(rfcp.getPhoneNumber());
                        break;
                    }
                }

                cg.setRfPhone(addr.getPhoneNumber());
                cg.setRfName(ak.getAccountKeeperName());
                cg.setLine1(addr.getAddress1());
                cg.setLine2(addr.getAddress2());
                cg.setZip(addr.getZipCode());
                cg.setCity(addr.getCity());
                cg.setState(region.getRegionCode());
                cg.setLaborRate(rf.getLaborRate());
                cg.setLaborTax(rf.getLaborTaxPct());
                cg.setPartTax(rf.getPartsTaxPct());
                cg.setPartWarrantyMiles(rf.getPartWarrantyMiles());
                cg.setPartWarrantyMonths(rf.getPartWarrantyMonths());

                lcg.add(new ClaimGeneral(
                        (claim != null ? claim.getClaimId() : null),
                        Optional.ofNullable(addr.getPhoneNumber()).orElse(""),
                        cg.getContactName(),
                        cg.getContactPhone(),
                        Optional.ofNullable(addr.getFax()).orElse(""),
                        (claim != null ? claim.getClaimNumber() : null),
                        cg.getOdometer(),
                        cg.getRoNumber(),
                        cg.getRoDate(),
                        Optional.ofNullable(ak.getAccountKeeperName()).orElse(""),
                        Optional.ofNullable(addr.getAddress1()).orElse(""),
                        Optional.ofNullable(addr.getAddress2()).orElse(""),
                        Optional.ofNullable(addr.getZipCode()).orElse(""),
                        Optional.ofNullable(addr.getCity()).orElse(""),
                        Optional.ofNullable(region.getRegionCode()).orElse(""),
                        Optional.ofNullable(rf.getLaborRate()).orElse(null),
                        Optional.ofNullable(rf.getLaborTaxPct()).orElse(null),
                        Optional.ofNullable(rf.getPartsTaxPct()).orElse(null),
                        Optional.ofNullable(rf.getPartWarrantyMonths()).orElse(null),
                        Optional.ofNullable(rf.getPartWarrantyMiles()).orElse(null)
                ));

            }
            return lcg;
        } catch (NoResultException e) {
            LOGGER.info("getRFNames is null");
            return null;
        }
    }

    public void onSave(SelectEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "save button pressed", "Id:");

        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
