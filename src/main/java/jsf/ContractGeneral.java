/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ContractGeneral implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    public ContractGeneral(String contractNo, String contractStatus, String effectiveDate, String claimStartDate, String expireType, String contractExpirationDate, BigDecimal contractPurchasePrice, BigDecimal coverageCost, String adminCompany, String program, String insurer, String product, String plan, String classCode, String dealer, String customer, String lienHolder, String enteredBy, String externalUserName, BigDecimal paidClaims, String iseSigned, Integer odometer, String termMonths, String termMiles, BigDecimal deductible, Integer claimWaitDays, Integer claimWaitMiles, Integer claimStartUsage, Integer expirationUsage, String vinFull, Integer vehicleYear, String make, String model, String series, String driveType, String cylinders, String literDisplacemant, String cubicInchDisplacement, String basicWarranty, String powertrainWarranty, String rustWarranty) {
        this.contractNo = contractNo;
        this.contractStatus = contractStatus;
        this.effectiveDate = effectiveDate;
        this.claimStartDate = claimStartDate;
        this.expireType = expireType;
        this.contractExpirationDate = contractExpirationDate;
        this.contractPurchasePrice = contractPurchasePrice;
        this.coverageCost = coverageCost;
        this.adminCompany = adminCompany;
        this.program = program;
        this.insurer = insurer;
        this.product = product;
        this.plan = plan;
        this.classCode = classCode;
        this.dealer = dealer;
        this.customer = customer;
        this.lienHolder = lienHolder;
        this.enteredBy = enteredBy;
        this.externalUserName = externalUserName;
        this.paidClaims = paidClaims;
        this.iseSigned = iseSigned;
        this.odometer = odometer;
        this.termMonths = termMonths;
        this.termMiles = termMiles;
        this.deductible = deductible;
        this.claimWaitDays = claimWaitDays;
        this.claimWaitMiles = claimWaitMiles;
        this.claimStartUsage = claimStartUsage;
        this.expirationUsage = expirationUsage;
        this.vinFull = vinFull;
        this.vehicleYear = vehicleYear;
        this.make = make;
        this.model = model;
        this.series = series;
        this.driveType = driveType;
        this.cylinders = cylinders;
        this.literDisplacemant = literDisplacemant;
        this.cubicInchDisplacement = cubicInchDisplacement;
        this.basicWarranty = basicWarranty;
        this.powertrainWarranty = powertrainWarranty;
        this.rustWarranty = rustWarranty;
    }

    

    

    @Column(name = "contractNo", table = "Contracts")
    private String contractNo;
    @Column(name = "contractStatus", table = "Contracts")
    private String contractStatus;
    @Column(name = "effectiveDate", table = "Contracts")
    private String effectiveDate;
    @Column(name = "claimStartDate", table = "Contracts")
    private String claimStartDate;
    @Column(name = "expireUsageType", table = "Contracts")
    private String expireType;
    @Column(name = "contractExpirationDate", table = "Contracts")
    private String contractExpirationDate;
    @Column(name = "contractPurchasePrice", table = "Contracts")
    private BigDecimal contractPurchasePrice;
    private BigDecimal coverageCost;

    String adminCompany;
    private String program;
    private String insurer;
    private String product;
    private String plan;
    private String classCode;
    private String dealer;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    private String customer;
    private String lienHolder;
    private String enteredBy;
    @Column(name = "externalUserName", table = "Contracts")
    private String externalUserName;
    private BigDecimal paidClaims;
    private String iseSigned;

    @Column(name = "odometer", table = "Contracts")
    private Integer odometer;
    @Column(name = "description", table = "TermDetail")
    private String termMonths;
    @Column(name = "description", table = "TermDetail")
    private String termMiles;
    private BigDecimal deductible;
    @Column(name = "waitDays", table = "TermDetail")
    private Integer claimWaitDays;
    @Column(name = "waitMiles", table = "TermDetail")
    private Integer claimWaitMiles;
    @Column(name = "claimStartUsage", table = "Contracts")
    private Integer claimStartUsage;
    @Column(name = "expirationUsage", table = "Contracts")
    private Integer expirationUsage;    // for VSC

    @Column(name = "vinFull", table = "Contracts")
    private String vinFull;
    @Column(name = "vehicleYear", table = "VinDesc")
    private Integer vehicleYear;
    @Column(name = "make", table = "VinDesc")
    private String make;
    @Column(name = "model", table = "VinDesc")
    private String model;
    @Column(name = "series", table = "VinDesc")
    private String series;
    @Column(name = "driveType", table = "VinDesc")
    private String driveType;
    @Column(name = "cylinders", table = "VinDesc")
    private String cylinders;
    @Column(name = "literDisplacemant", table = "VinDesc")
    private String literDisplacemant;
    @Column(name = "cubicInchDisplacement", table = "VinDesc")
    private String cubicInchDisplacement;
    private String basicWarranty;
    private String powertrainWarranty;
    private String rustWarranty;

    public Integer getClaimWaitMiles() {
        return claimWaitMiles;
    }

    public void setClaimWaitMiles(Integer claimWaitMiles) {
        this.claimWaitMiles = claimWaitMiles;
    }
    
    

    public String getExternalUserName() {
        return externalUserName;
    }

    public void setExternalUserName(String externalUserName) {
        this.externalUserName = externalUserName;
    }

    public String getIseSigned() {
        return iseSigned;
    }

    public void setIseSigned(String iseSigned) {
        this.iseSigned = iseSigned;
    }
    
    

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getClaimStartDate() {
        return claimStartDate;
    }

    public void setClaimStartDate(String claimStartDate) {
        this.claimStartDate = claimStartDate;
    }

    public String getExpireType() {
        return expireType;
    }

    public void setExpireType(String expireType) {
        this.expireType = expireType;
    }

    public String getContractExpirationDate() {
        return contractExpirationDate;
    }

    public void setContractExpirationDate(String contractExpirationDate) {
        this.contractExpirationDate = contractExpirationDate;
    }

    public BigDecimal getContractPurchasePrice() {
        return contractPurchasePrice;
    }

    public void setContractPurchasePrice(BigDecimal contractPurchasePrice) {
        this.contractPurchasePrice = contractPurchasePrice;
    }

    public BigDecimal getCoverageCost() {
        return coverageCost;
    }

    public void setCoverageCost(BigDecimal coverageCost) {
        this.coverageCost = coverageCost;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getLienHolder() {
        return lienHolder;
    }

    public void setLienHolder(String lienHolder) {
        this.lienHolder = lienHolder;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public BigDecimal getPaidClaims() {
        return paidClaims;
    }

    public void setPaidClaims(BigDecimal paidClaims) {
        this.paidClaims = paidClaims;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public String getTermMonths() {
        return termMonths;
    }

    public void setTermMonths(String termMonths) {
        this.termMonths = termMonths;
    }

    public String getTermMiles() {
        return termMiles;
    }

    public void setTermMiles(String termMiles) {
        this.termMiles = termMiles;
    }

    public BigDecimal getDeductible() {
        return deductible;
    }

    public void setDeductible(BigDecimal deductible) {
        this.deductible = deductible;
    }

    public Integer getClaimWaitDays() {
        return claimWaitDays;
    }

    public void setClaimWaitDays(Integer claimWaitDays) {
        this.claimWaitDays = claimWaitDays;
    }

    public Integer getClaimStartUsage() {
        return claimStartUsage;
    }

    public void setClaimStartUsage(Integer claimStartUsage) {
        this.claimStartUsage = claimStartUsage;
    }

    public Integer getExpirationUsage() {
        return expirationUsage;
    }

    public void setExpirationUsage(Integer expirationUsage) {
        this.expirationUsage = expirationUsage;
    }

    public String getVinFull() {
        return vinFull;
    }

    public void setVinFull(String vinFull) {
        this.vinFull = vinFull;
    }

    public Integer getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getLiterDisplacemant() {
        return literDisplacemant;
    }

    public void setLiterDisplacemant(String literDisplacemant) {
        this.literDisplacemant = literDisplacemant;
    }

    public String getCubicInchDisplacement() {
        return cubicInchDisplacement;
    }

    public void setCubicInchDisplacement(String cubicInchDisplacement) {
        this.cubicInchDisplacement = cubicInchDisplacement;
    }

    public String getBasicWarranty() {
        return basicWarranty;
    }

    public void setBasicWarranty(String basicWarranty) {
        this.basicWarranty = basicWarranty;
    }

    public String getPowertrainWarranty() {
        return powertrainWarranty;
    }

    public void setPowertrainWarranty(String powertrainWarranty) {
        this.powertrainWarranty = powertrainWarranty;
    }

    public String getRustWarranty() {
        return rustWarranty;
    }

    public void setRustWarranty(String rustWarranty) {
        this.rustWarranty = rustWarranty;
    }

    public void reset() {
        contractNo = "";
        contractStatus = "";
        effectiveDate = "";
        claimStartDate = "";
        expireType = "";
        contractExpirationDate = "";
        contractPurchasePrice = null;
        coverageCost = null;

        adminCompany = "";
        program = "";
        insurer = "";
        product = "";
        plan = "";
        classCode = "";
        dealer = "";
        String customer = "";
        lienHolder = "";
        enteredBy = "";
        paidClaims = null;
        externalUserName="";
        iseSigned = "";

        odometer = null;
        termMonths = "";
        termMiles = "";
        deductible = null;
        claimWaitDays = null;
        claimWaitMiles = null;
        claimStartUsage = null;
        expirationUsage = null;    // for VSC

        vinFull = "";
        vehicleYear=null;
        make = "";
        model = "";
        series = "";
        driveType = "";
        cylinders = "";
        literDisplacemant = "";
        cubicInchDisplacement = "";
        basicWarranty = "";
        powertrainWarranty = "";
        rustWarranty = "";

    }

}
