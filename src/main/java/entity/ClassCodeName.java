/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClassCodeName")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassCodeName.findAll", query = "SELECT c FROM ClassCodeName c")
    , @NamedQuery(name = "ClassCodeName.findByClassCodeNameId", query = "SELECT c FROM ClassCodeName c WHERE c.classCodeNameId = :classCodeNameId")
    , @NamedQuery(name = "ClassCodeName.findByName", query = "SELECT c FROM ClassCodeName c WHERE c.name = :name")
    , @NamedQuery(name = "ClassCodeName.findByUpdateUserName", query = "SELECT c FROM ClassCodeName c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClassCodeName.findByUpdateLast", query = "SELECT c FROM ClassCodeName c WHERE c.updateLast = :updateLast")})
public class ClassCodeName implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "classCodeNameId")
    private Integer classCodeNameId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "classCodeNameIdFk")
    private Collection<ClassTable> classTableCollection;
    @OneToMany(mappedBy = "classCodeNameIdFk")
    private Collection<VINClassCode> vINClassCodeCollection;

    public ClassCodeName() {
    }

    public ClassCodeName(Integer classCodeNameId) {
        this.classCodeNameId = classCodeNameId;
    }

    public Integer getClassCodeNameId() {
        return classCodeNameId;
    }

    public void setClassCodeNameId(Integer classCodeNameId) {
        this.classCodeNameId = classCodeNameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClassTable> getClassTableCollection() {
        return classTableCollection;
    }

    public void setClassTableCollection(Collection<ClassTable> classTableCollection) {
        this.classTableCollection = classTableCollection;
    }

    @XmlTransient
    public Collection<VINClassCode> getVINClassCodeCollection() {
        return vINClassCodeCollection;
    }

    public void setVINClassCodeCollection(Collection<VINClassCode> vINClassCodeCollection) {
        this.vINClassCodeCollection = vINClassCodeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classCodeNameId != null ? classCodeNameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassCodeName)) {
            return false;
        }
        ClassCodeName other = (ClassCodeName) object;
        if ((this.classCodeNameId == null && other.classCodeNameId != null) || (this.classCodeNameId != null && !this.classCodeNameId.equals(other.classCodeNameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClassCodeName[ classCodeNameId=" + classCodeNameId + " ]";
    }
    
}
