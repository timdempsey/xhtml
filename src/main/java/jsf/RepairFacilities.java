/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class RepairFacilities {

    public RepairFacilities() {
         myAddress = new MyAddress();
    }

    public RepairFacilities(Integer repairFacilityId, String name, String phone, String paddr, Boolean activeStatus) {
        this.repairFacilityId = repairFacilityId;
        this.name = name;
        this.phone = phone;
        this.paddr = paddr;
        this.activeStatus = activeStatus;
    }

    public RepairFacilities(Integer repairFacilityId, String name, String phone, String paddr, Boolean activeStatus, String taxId, Date effectiveDate, String email, Boolean emailOptIn, Boolean holdPayables, String prefpayMethod, String billTo, MyAddress myAddress, BigDecimal laborTax, BigDecimal partTax, BigDecimal laborRate, Integer partWarrantyMonths, Integer partWarrantyMiles, Integer dealerId, Integer dealerGroupId) {
        this.repairFacilityId = repairFacilityId;
        this.name = name;
        this.phone = phone;
        this.paddr = paddr;
        this.activeStatus = activeStatus;
        this.taxId = taxId;
        this.effectiveDate = effectiveDate;
        this.email = email;
        this.emailOptIn = emailOptIn;
        this.holdPayables = holdPayables;
        this.prefpayMethod = prefpayMethod;
        this.billTo = billTo;
        this.myAddress = myAddress;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.laborRate = laborRate;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
        this.dealerId = dealerId;
        this.dealerGroupId = dealerGroupId;
    }

    

    private Integer repairFacilityId;
    private String name;
    private String phone;
    private String paddr;
    private Boolean activeStatus;
    private String taxId;
    private Date effectiveDate;
    private String email;
    private Boolean emailOptIn;
    private Boolean holdPayables;
    private String prefpayMethod;
    private String billTo;
    private MyAddress myAddress;
    private BigDecimal laborTax;
    private BigDecimal partTax;
    private BigDecimal laborRate;
    private Integer partWarrantyMonths;
    private Integer partWarrantyMiles;
    private Integer dealerId;
    private Integer dealerGroupId;

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getDealerGroupId() {
        return dealerGroupId;
    }

    public void setDealerGroupId(Integer dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }
    
    

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Boolean getHoldPayables() {
        return holdPayables;
    }

    public void setHoldPayables(Boolean holdPayables) {
        this.holdPayables = holdPayables;
    }

    public String getPrefpayMethod() {
        return prefpayMethod;
    }

    public void setPrefpayMethod(String prefpayMethod) {
        this.prefpayMethod = prefpayMethod;
    }

    public String getBillTo() {
        return billTo;
    }

    public void setBillTo(String billTo) {
        this.billTo = billTo;
    }

    public MyAddress getMyAddress() {
        return myAddress;
    }

    public void setMyAddress(MyAddress myAddress) {
        this.myAddress = myAddress;
    }

    public BigDecimal getLaborTax() {
        return laborTax;
    }

    public void setLaborTax(BigDecimal laborTax) {
        this.laborTax = laborTax;
    }

    public BigDecimal getPartTax() {
        return partTax;
    }

    public void setPartTax(BigDecimal partTax) {
        this.partTax = partTax;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }
    
    

    public Integer getRepairFacilityId() {
        return repairFacilityId;
    }

    public void setRepairFacilityId(Integer repairFacilityId) {
        this.repairFacilityId = repairFacilityId;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPaddr() {
        return paddr;
    }

    public void setPaddr(String paddr) {
        this.paddr = paddr;
    }

    public void reset() {
        name = "";
        phone = "";
        paddr = "";
        activeStatus = true;
        repairFacilityId = null;
        taxId = "";
        effectiveDate = null;
        email = "";
        emailOptIn = false;
        holdPayables = false;
        prefpayMethod = "";
        billTo = "";
        laborTax = null;
        partTax = null;
        laborRate = null;
        partWarrantyMonths = null;
        partWarrantyMiles = null;
        dealerId = null;
        dealerGroupId = null;
    }

}
