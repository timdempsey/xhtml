/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateDetail.findAll", query = "SELECT r FROM RateDetail r")
    , @NamedQuery(name = "RateDetail.findByRateDetailId", query = "SELECT r FROM RateDetail r WHERE r.rateDetailId = :rateDetailId")
    , @NamedQuery(name = "RateDetail.findBySuggestedRetailPrice", query = "SELECT r FROM RateDetail r WHERE r.suggestedRetailPrice = :suggestedRetailPrice")
    , @NamedQuery(name = "RateDetail.findByEffectiveDate", query = "SELECT r FROM RateDetail r WHERE r.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "RateDetail.findByEnteredDate", query = "SELECT r FROM RateDetail r WHERE r.enteredDate = :enteredDate")
    , @NamedQuery(name = "RateDetail.findByDeletedInd", query = "SELECT r FROM RateDetail r WHERE r.deletedInd = :deletedInd")
    , @NamedQuery(name = "RateDetail.findByIgnoredInd", query = "SELECT r FROM RateDetail r WHERE r.ignoredInd = :ignoredInd")
    , @NamedQuery(name = "RateDetail.findByUpdateUserName", query = "SELECT r FROM RateDetail r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateDetail.findByUpdateLast", query = "SELECT r FROM RateDetail r WHERE r.updateLast = :updateLast")})
public class RateDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateDetailId")
    private Integer rateDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "suggestedRetailPrice")
    private BigDecimal suggestedRetailPrice;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Column(name = "ignoredInd")
    private Boolean ignoredInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "rateDetailIdFk")
    private Collection<Note> noteCollection;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "rateIdFk", referencedColumnName = "RateId")
    @ManyToOne
    private Rate rateIdFk;
    @JoinColumn(name = "rateWizardUploadIdFk", referencedColumnName = "RateWizardUploadId")
    @ManyToOne
    private RateWizardUpload rateWizardUploadIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public RateDetail() {
    }

    public RateDetail(Integer rateDetailId) {
        this.rateDetailId = rateDetailId;
    }

    public Integer getRateDetailId() {
        return rateDetailId;
    }

    public void setRateDetailId(Integer rateDetailId) {
        this.rateDetailId = rateDetailId;
    }

    public BigDecimal getSuggestedRetailPrice() {
        return suggestedRetailPrice;
    }

    public void setSuggestedRetailPrice(BigDecimal suggestedRetailPrice) {
        this.suggestedRetailPrice = suggestedRetailPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Boolean getIgnoredInd() {
        return ignoredInd;
    }

    public void setIgnoredInd(Boolean ignoredInd) {
        this.ignoredInd = ignoredInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Rate getRateIdFk() {
        return rateIdFk;
    }

    public void setRateIdFk(Rate rateIdFk) {
        this.rateIdFk = rateIdFk;
    }

    public RateWizardUpload getRateWizardUploadIdFk() {
        return rateWizardUploadIdFk;
    }

    public void setRateWizardUploadIdFk(RateWizardUpload rateWizardUploadIdFk) {
        this.rateWizardUploadIdFk = rateWizardUploadIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateDetailId != null ? rateDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateDetail)) {
            return false;
        }
        RateDetail other = (RateDetail) object;
        if ((this.rateDetailId == null && other.rateDetailId != null) || (this.rateDetailId != null && !this.rateDetailId.equals(other.rateDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateDetail[ rateDetailId=" + rateDetailId + " ]";
    }
    
}
