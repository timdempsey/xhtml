/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfUserMemberGUITableViewConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfUserMemberGUITableViewConn.findAll", query = "SELECT c FROM CfUserMemberGUITableViewConn c")
    , @NamedQuery(name = "CfUserMemberGUITableViewConn.findByCfUserMemberGUITableViewConnId", query = "SELECT c FROM CfUserMemberGUITableViewConn c WHERE c.cfUserMemberGUITableViewConnId = :cfUserMemberGUITableViewConnId")
    , @NamedQuery(name = "CfUserMemberGUITableViewConn.findByDefaultInd", query = "SELECT c FROM CfUserMemberGUITableViewConn c WHERE c.defaultInd = :defaultInd")
    , @NamedQuery(name = "CfUserMemberGUITableViewConn.findByUpdateUserName", query = "SELECT c FROM CfUserMemberGUITableViewConn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfUserMemberGUITableViewConn.findByUpdateLast", query = "SELECT c FROM CfUserMemberGUITableViewConn c WHERE c.updateLast = :updateLast")})
public class CfUserMemberGUITableViewConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CfUserMemberGUITableViewConnId")
    private Integer cfUserMemberGUITableViewConnId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "defaultInd")
    private boolean defaultInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "GUITableViewId", referencedColumnName = "GUITableViewId")
    @ManyToOne(optional = false)
    private CfGUITableView gUITableViewId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public CfUserMemberGUITableViewConn() {
    }

    public CfUserMemberGUITableViewConn(Integer cfUserMemberGUITableViewConnId) {
        this.cfUserMemberGUITableViewConnId = cfUserMemberGUITableViewConnId;
    }

    public CfUserMemberGUITableViewConn(Integer cfUserMemberGUITableViewConnId, boolean defaultInd) {
        this.cfUserMemberGUITableViewConnId = cfUserMemberGUITableViewConnId;
        this.defaultInd = defaultInd;
    }

    public Integer getCfUserMemberGUITableViewConnId() {
        return cfUserMemberGUITableViewConnId;
    }

    public void setCfUserMemberGUITableViewConnId(Integer cfUserMemberGUITableViewConnId) {
        this.cfUserMemberGUITableViewConnId = cfUserMemberGUITableViewConnId;
    }

    public boolean getDefaultInd() {
        return defaultInd;
    }

    public void setDefaultInd(boolean defaultInd) {
        this.defaultInd = defaultInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfGUITableView getGUITableViewId() {
        return gUITableViewId;
    }

    public void setGUITableViewId(CfGUITableView gUITableViewId) {
        this.gUITableViewId = gUITableViewId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cfUserMemberGUITableViewConnId != null ? cfUserMemberGUITableViewConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfUserMemberGUITableViewConn)) {
            return false;
        }
        CfUserMemberGUITableViewConn other = (CfUserMemberGUITableViewConn) object;
        if ((this.cfUserMemberGUITableViewConnId == null && other.cfUserMemberGUITableViewConnId != null) || (this.cfUserMemberGUITableViewConnId != null && !this.cfUserMemberGUITableViewConnId.equals(other.cfUserMemberGUITableViewConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfUserMemberGUITableViewConn[ cfUserMemberGUITableViewConnId=" + cfUserMemberGUITableViewConnId + " ]";
    }
    
}
