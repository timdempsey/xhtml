/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "FuelType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FuelType.findAll", query = "SELECT f FROM FuelType f")
    , @NamedQuery(name = "FuelType.findByFuelTypeId", query = "SELECT f FROM FuelType f WHERE f.fuelTypeId = :fuelTypeId")
    , @NamedQuery(name = "FuelType.findByDescription", query = "SELECT f FROM FuelType f WHERE f.description = :description")
    , @NamedQuery(name = "FuelType.findByUpdateUserName", query = "SELECT f FROM FuelType f WHERE f.updateUserName = :updateUserName")
    , @NamedQuery(name = "FuelType.findByUpdateLast", query = "SELECT f FROM FuelType f WHERE f.updateLast = :updateLast")})
public class FuelType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fuelTypeId")
    private Integer fuelTypeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "fuelTypeIdFk")
    private Collection<CfManufacturerWarranty> cfManufacturerWarrantyCollection;
    @OneToMany(mappedBy = "fuelTypeIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "fuelTypeIdFk")
    private Collection<VinDesc> vinDescCollection;

    public FuelType() {
    }

    public FuelType(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public FuelType(Integer fuelTypeId, String description) {
        this.fuelTypeId = fuelTypeId;
        this.description = description;
    }

    public Integer getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfManufacturerWarranty> getCfManufacturerWarrantyCollection() {
        return cfManufacturerWarrantyCollection;
    }

    public void setCfManufacturerWarrantyCollection(Collection<CfManufacturerWarranty> cfManufacturerWarrantyCollection) {
        this.cfManufacturerWarrantyCollection = cfManufacturerWarrantyCollection;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<VinDesc> getVinDescCollection() {
        return vinDescCollection;
    }

    public void setVinDescCollection(Collection<VinDesc> vinDescCollection) {
        this.vinDescCollection = vinDescCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fuelTypeId != null ? fuelTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FuelType)) {
            return false;
        }
        FuelType other = (FuelType) object;
        if ((this.fuelTypeId == null && other.fuelTypeId != null) || (this.fuelTypeId != null && !this.fuelTypeId.equals(other.fuelTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.FuelType[ fuelTypeId=" + fuelTypeId + " ]";
    }
    
}
