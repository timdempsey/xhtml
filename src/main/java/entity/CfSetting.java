/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfSetting")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfSetting.findAll", query = "SELECT c FROM CfSetting c")
    , @NamedQuery(name = "CfSetting.findBySettingId", query = "SELECT c FROM CfSetting c WHERE c.settingId = :settingId")
    , @NamedQuery(name = "CfSetting.findByName", query = "SELECT c FROM CfSetting c WHERE c.name = :name")
    , @NamedQuery(name = "CfSetting.findByValue", query = "SELECT c FROM CfSetting c WHERE c.value = :value")
    , @NamedQuery(name = "CfSetting.findByTypeInd", query = "SELECT c FROM CfSetting c WHERE c.typeInd = :typeInd")
    , @NamedQuery(name = "CfSetting.findBySystemSettingInd", query = "SELECT c FROM CfSetting c WHERE c.systemSettingInd = :systemSettingInd")
    , @NamedQuery(name = "CfSetting.findByDescription", query = "SELECT c FROM CfSetting c WHERE c.description = :description")
    , @NamedQuery(name = "CfSetting.findByUpdateUserName", query = "SELECT c FROM CfSetting c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfSetting.findByUpdateLast", query = "SELECT c FROM CfSetting c WHERE c.updateLast = :updateLast")})
public class CfSetting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "settingId")
    private Integer settingId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @NotNull
    @Column(name = "typeInd")
    private int typeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "systemSettingInd")
    private boolean systemSettingInd;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "settingIdFk")
    private Collection<CfSettingListItem> cfSettingListItemCollection;

    public CfSetting() {
    }

    public CfSetting(Integer settingId) {
        this.settingId = settingId;
    }

    public CfSetting(Integer settingId, String name, String value, int typeInd, boolean systemSettingInd) {
        this.settingId = settingId;
        this.name = name;
        this.value = value;
        this.typeInd = typeInd;
        this.systemSettingInd = systemSettingInd;
    }

    public Integer getSettingId() {
        return settingId;
    }

    public void setSettingId(Integer settingId) {
        this.settingId = settingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getTypeInd() {
        return typeInd;
    }

    public void setTypeInd(int typeInd) {
        this.typeInd = typeInd;
    }

    public boolean getSystemSettingInd() {
        return systemSettingInd;
    }

    public void setSystemSettingInd(boolean systemSettingInd) {
        this.systemSettingInd = systemSettingInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfSettingListItem> getCfSettingListItemCollection() {
        return cfSettingListItemCollection;
    }

    public void setCfSettingListItemCollection(Collection<CfSettingListItem> cfSettingListItemCollection) {
        this.cfSettingListItemCollection = cfSettingListItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (settingId != null ? settingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfSetting)) {
            return false;
        }
        CfSetting other = (CfSetting) object;
        if ((this.settingId == null && other.settingId != null) || (this.settingId != null && !this.settingId.equals(other.settingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfSetting[ settingId=" + settingId + " ]";
    }
    
}
