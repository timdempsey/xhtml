/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "HistoryLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoryLog.findAll", query = "SELECT h FROM HistoryLog h")
    , @NamedQuery(name = "HistoryLog.findByHistoryLogId", query = "SELECT h FROM HistoryLog h WHERE h.historyLogId = :historyLogId")
    , @NamedQuery(name = "HistoryLog.findByHistoryLog", query = "SELECT h FROM HistoryLog h WHERE h.historyLog = :historyLog")
    , @NamedQuery(name = "HistoryLog.findByEnteredDate", query = "SELECT h FROM HistoryLog h WHERE h.enteredDate = :enteredDate")
    , @NamedQuery(name = "HistoryLog.findByHistoryLogTypeInd", query = "SELECT h FROM HistoryLog h WHERE h.historyLogTypeInd = :historyLogTypeInd")
    , @NamedQuery(name = "HistoryLog.findByUpdateUserName", query = "SELECT h FROM HistoryLog h WHERE h.updateUserName = :updateUserName")
    , @NamedQuery(name = "HistoryLog.findByUpdateLast", query = "SELECT h FROM HistoryLog h WHERE h.updateLast = :updateLast")})
public class HistoryLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "historyLogId")
    private Integer historyLogId;
    @Size(max = 2147483647)
    @Column(name = "historyLog")
    private String historyLog;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "historyLogTypeInd")
    private int historyLogTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "remitRuleIdFk", referencedColumnName = "remitRuleId")
    @ManyToOne
    private CfRemitRule remitRuleIdFk;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "disbursementDetailIdFk", referencedColumnName = "disbursementDetailId")
    @ManyToOne
    private DisbursementDetail disbursementDetailIdFk;
    @JoinColumn(name = "ledgerIdFk", referencedColumnName = "ledgerId")
    @ManyToOne
    private Ledger ledgerIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public HistoryLog() {
    }

    public HistoryLog(Integer historyLogId) {
        this.historyLogId = historyLogId;
    }

    public HistoryLog(Integer historyLogId, int historyLogTypeInd) {
        this.historyLogId = historyLogId;
        this.historyLogTypeInd = historyLogTypeInd;
    }

    public Integer getHistoryLogId() {
        return historyLogId;
    }

    public void setHistoryLogId(Integer historyLogId) {
        this.historyLogId = historyLogId;
    }

    public String getHistoryLog() {
        return historyLog;
    }

    public void setHistoryLog(String historyLog) {
        this.historyLog = historyLog;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public int getHistoryLogTypeInd() {
        return historyLogTypeInd;
    }

    public void setHistoryLogTypeInd(int historyLogTypeInd) {
        this.historyLogTypeInd = historyLogTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public CfRemitRule getRemitRuleIdFk() {
        return remitRuleIdFk;
    }

    public void setRemitRuleIdFk(CfRemitRule remitRuleIdFk) {
        this.remitRuleIdFk = remitRuleIdFk;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public DisbursementDetail getDisbursementDetailIdFk() {
        return disbursementDetailIdFk;
    }

    public void setDisbursementDetailIdFk(DisbursementDetail disbursementDetailIdFk) {
        this.disbursementDetailIdFk = disbursementDetailIdFk;
    }

    public Ledger getLedgerIdFk() {
        return ledgerIdFk;
    }

    public void setLedgerIdFk(Ledger ledgerIdFk) {
        this.ledgerIdFk = ledgerIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historyLogId != null ? historyLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoryLog)) {
            return false;
        }
        HistoryLog other = (HistoryLog) object;
        if ((this.historyLogId == null && other.historyLogId != null) || (this.historyLogId != null && !this.historyLogId.equals(other.historyLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.HistoryLog[ historyLogId=" + historyLogId + " ]";
    }
    
}
