/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ContractSearch {
    

    
    private static final Logger LOGGER = LogManager.getLogger(ContractSearch.class);

    public ContractSearch() {
    }

    public ContractSearch(Integer contractId, String contractNo, String agent, Date effectiveDate, Date contractExpirationDate, String vinFull, String customer, String phoneNumber, String dealerName, String dealerNumber, String productTypeName) {
        this.contractId = contractId;
        this.contractNo = contractNo;
        this.agent = agent;
        this.effectiveDate = effectiveDate;
        this.contractExpirationDate = contractExpirationDate;
        this.vinFull = vinFull;
        this.customer = customer;
        this.phoneNumber = phoneNumber;
        this.dealerName = dealerName;
        this.dealerNumber = dealerNumber;
        this.productTypeName = productTypeName;
    }

   
    
    

    
    @Id
    @Column(name = "contractId", table = "Contracts")
    Integer contractId;
     @Column(name = "contractNo", table = "Contracts")
    private String contractNo;
     @Column(name = "accountKeepName", table = "AccountKeeper")
    private String agent;
     @Column(name = "effectiveDate", table = "Contracts")
    private Date effectiveDate;
    @Column(name = "contractExpirationDate", table = "Contracts")
    private Date contractExpirationDate;
    @Column(name = "vinFull", table = "Contracts")
    private String vinFull;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    private String customer;
    @Column(name = "phoneNumber", table = "Address")
    private String phoneNumber;
    String dealerName;
    @Column(name = "dealerNumber", table = "Dealer")
    private String dealerNumber;
    @Column(name = "productTypeName", table = "ProductType")
    private String productTypeName;

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
    
    

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }
    
    

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
   

    public String getVinFull() {
        return vinFull;
    }

    public void setVinFull(String vinFull) {
        this.vinFull = vinFull;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getContractExpirationDate() {
        return contractExpirationDate;
    }

    public void setContractExpirationDate(Date contractExpirationDate) {
        this.contractExpirationDate = contractExpirationDate;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }
    
    
    
    
}

