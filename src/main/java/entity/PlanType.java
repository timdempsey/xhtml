/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PlanType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanType.findAll", query = "SELECT p FROM PlanType p")
    , @NamedQuery(name = "PlanType.findByPlanTypeId", query = "SELECT p FROM PlanType p WHERE p.planTypeId = :planTypeId")
    , @NamedQuery(name = "PlanType.findByDescription", query = "SELECT p FROM PlanType p WHERE p.description = :description")})
public class PlanType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "planTypeId")
    private Integer planTypeId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public PlanType() {
    }

    public PlanType(Integer planTypeId) {
        this.planTypeId = planTypeId;
    }

    public Integer getPlanTypeId() {
        return planTypeId;
    }

    public void setPlanTypeId(Integer planTypeId) {
        this.planTypeId = planTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planTypeId != null ? planTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanType)) {
            return false;
        }
        PlanType other = (PlanType) object;
        if ((this.planTypeId == null && other.planTypeId != null) || (this.planTypeId != null && !this.planTypeId.equals(other.planTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PlanType[ planTypeId=" + planTypeId + " ]";
    }
    
}
