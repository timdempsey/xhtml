/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class HistoryLogs {

    public HistoryLogs() {
    }

    public HistoryLogs(Integer historyLogId, String historyLog, String enteredBy, String enteredDate) {
        this.historyLogId = historyLogId;
        this.historyLog = historyLog;
        this.enteredBy = enteredBy;
        this.enteredDate = enteredDate;
    }

    
    private Integer historyLogId;
    private String historyLog;
    private String enteredBy;
    private String enteredDate;

    public Integer getHistoryLogId() {
        return historyLogId;
    }

    public void setHistoryLogId(Integer historyLogId) {
        this.historyLogId = historyLogId;
    }

    public String getHistoryLog() {
        return historyLog;
    }

    public void setHistoryLog(String historyLog) {
        this.historyLog = historyLog;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public String getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }

    

    public void reset() {
        historyLogId = null;
        historyLog = "";
        enteredBy = "";
        enteredDate = "";
    }

}
