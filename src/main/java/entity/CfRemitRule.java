/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfRemitRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfRemitRule.findAll", query = "SELECT c FROM CfRemitRule c")
    , @NamedQuery(name = "CfRemitRule.findByRemitRuleId", query = "SELECT c FROM CfRemitRule c WHERE c.remitRuleId = :remitRuleId")
    , @NamedQuery(name = "CfRemitRule.findByName", query = "SELECT c FROM CfRemitRule c WHERE c.name = :name")
    , @NamedQuery(name = "CfRemitRule.findByDescription", query = "SELECT c FROM CfRemitRule c WHERE c.description = :description")
    , @NamedQuery(name = "CfRemitRule.findByContract", query = "SELECT c FROM CfRemitRule c WHERE c.contract = :contract")
    , @NamedQuery(name = "CfRemitRule.findByAmendment", query = "SELECT c FROM CfRemitRule c WHERE c.amendment = :amendment")
    , @NamedQuery(name = "CfRemitRule.findByCancel", query = "SELECT c FROM CfRemitRule c WHERE c.cancel = :cancel")
    , @NamedQuery(name = "CfRemitRule.findByReinstatement", query = "SELECT c FROM CfRemitRule c WHERE c.reinstatement = :reinstatement")
    , @NamedQuery(name = "CfRemitRule.findByContractDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.contractDisbursement = :contractDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByExemptContractDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.exemptContractDisbursement = :exemptContractDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByAmendmentDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.amendmentDisbursement = :amendmentDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByCancelDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.cancelDisbursement = :cancelDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByNetCancelDealerProfit", query = "SELECT c FROM CfRemitRule c WHERE c.netCancelDealerProfit = :netCancelDealerProfit")
    , @NamedQuery(name = "CfRemitRule.findByExemptCancelDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.exemptCancelDisbursement = :exemptCancelDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByReinstatementDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.reinstatementDisbursement = :reinstatementDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByNetReinstateDealerProfit", query = "SELECT c FROM CfRemitRule c WHERE c.netReinstateDealerProfit = :netReinstateDealerProfit")
    , @NamedQuery(name = "CfRemitRule.findByExemptReinstatementDisbursement", query = "SELECT c FROM CfRemitRule c WHERE c.exemptReinstatementDisbursement = :exemptReinstatementDisbursement")
    , @NamedQuery(name = "CfRemitRule.findByTransfer", query = "SELECT c FROM CfRemitRule c WHERE c.transfer = :transfer")
    , @NamedQuery(name = "CfRemitRule.findByClaimInspection", query = "SELECT c FROM CfRemitRule c WHERE c.claimInspection = :claimInspection")
    , @NamedQuery(name = "CfRemitRule.findByPaymentAuthorization", query = "SELECT c FROM CfRemitRule c WHERE c.paymentAuthorization = :paymentAuthorization")
    , @NamedQuery(name = "CfRemitRule.findByRemitPendingContractsAfterXDays", query = "SELECT c FROM CfRemitRule c WHERE c.remitPendingContractsAfterXDays = :remitPendingContractsAfterXDays")
    , @NamedQuery(name = "CfRemitRule.findByBatchCorrection", query = "SELECT c FROM CfRemitRule c WHERE c.batchCorrection = :batchCorrection")
    , @NamedQuery(name = "CfRemitRule.findByDefaultInd", query = "SELECT c FROM CfRemitRule c WHERE c.defaultInd = :defaultInd")
    , @NamedQuery(name = "CfRemitRule.findByPayback", query = "SELECT c FROM CfRemitRule c WHERE c.payback = :payback")
    , @NamedQuery(name = "CfRemitRule.findByUpdateUserName", query = "SELECT c FROM CfRemitRule c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfRemitRule.findByUpdateLast", query = "SELECT c FROM CfRemitRule c WHERE c.updateLast = :updateLast")})
public class CfRemitRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "remitRuleId")
    private Integer remitRuleId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contract")
    private int contract;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amendment")
    private int amendment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancel")
    private int cancel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatement")
    private int reinstatement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractDisbursement")
    private int contractDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exemptContractDisbursement")
    private int exemptContractDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amendmentDisbursement")
    private int amendmentDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelDisbursement")
    private int cancelDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "netCancelDealerProfit")
    private int netCancelDealerProfit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exemptCancelDisbursement")
    private int exemptCancelDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatementDisbursement")
    private int reinstatementDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "netReinstateDealerProfit")
    private int netReinstateDealerProfit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exemptReinstatementDisbursement")
    private int exemptReinstatementDisbursement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transfer")
    private int transfer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimInspection")
    private int claimInspection;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paymentAuthorization")
    private int paymentAuthorization;
    @Column(name = "remitPendingContractsAfterXDays")
    private Integer remitPendingContractsAfterXDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "batchCorrection")
    private int batchCorrection;
    @Basic(optional = false)
    @NotNull
    @Column(name = "defaultInd")
    private boolean defaultInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payback")
    private int payback;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "remitRuleIdFk")
    private Collection<AccountKeeper> accountKeeperCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "remitRuleIdFk")
    private Collection<CfDisbursementRemitRule> cfDisbursementRemitRuleCollection;
    @OneToMany(mappedBy = "remitRuleIdFk")
    private Collection<HistoryLog> historyLogCollection;

    public CfRemitRule() {
    }

    public CfRemitRule(Integer remitRuleId) {
        this.remitRuleId = remitRuleId;
    }

    public CfRemitRule(Integer remitRuleId, int contract, int amendment, int cancel, int reinstatement, int contractDisbursement, int exemptContractDisbursement, int amendmentDisbursement, int cancelDisbursement, int netCancelDealerProfit, int exemptCancelDisbursement, int reinstatementDisbursement, int netReinstateDealerProfit, int exemptReinstatementDisbursement, int transfer, int claimInspection, int paymentAuthorization, int batchCorrection, boolean defaultInd, int payback) {
        this.remitRuleId = remitRuleId;
        this.contract = contract;
        this.amendment = amendment;
        this.cancel = cancel;
        this.reinstatement = reinstatement;
        this.contractDisbursement = contractDisbursement;
        this.exemptContractDisbursement = exemptContractDisbursement;
        this.amendmentDisbursement = amendmentDisbursement;
        this.cancelDisbursement = cancelDisbursement;
        this.netCancelDealerProfit = netCancelDealerProfit;
        this.exemptCancelDisbursement = exemptCancelDisbursement;
        this.reinstatementDisbursement = reinstatementDisbursement;
        this.netReinstateDealerProfit = netReinstateDealerProfit;
        this.exemptReinstatementDisbursement = exemptReinstatementDisbursement;
        this.transfer = transfer;
        this.claimInspection = claimInspection;
        this.paymentAuthorization = paymentAuthorization;
        this.batchCorrection = batchCorrection;
        this.defaultInd = defaultInd;
        this.payback = payback;
    }

    public Integer getRemitRuleId() {
        return remitRuleId;
    }

    public void setRemitRuleId(Integer remitRuleId) {
        this.remitRuleId = remitRuleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getContract() {
        return contract;
    }

    public void setContract(int contract) {
        this.contract = contract;
    }

    public int getAmendment() {
        return amendment;
    }

    public void setAmendment(int amendment) {
        this.amendment = amendment;
    }

    public int getCancel() {
        return cancel;
    }

    public void setCancel(int cancel) {
        this.cancel = cancel;
    }

    public int getReinstatement() {
        return reinstatement;
    }

    public void setReinstatement(int reinstatement) {
        this.reinstatement = reinstatement;
    }

    public int getContractDisbursement() {
        return contractDisbursement;
    }

    public void setContractDisbursement(int contractDisbursement) {
        this.contractDisbursement = contractDisbursement;
    }

    public int getExemptContractDisbursement() {
        return exemptContractDisbursement;
    }

    public void setExemptContractDisbursement(int exemptContractDisbursement) {
        this.exemptContractDisbursement = exemptContractDisbursement;
    }

    public int getAmendmentDisbursement() {
        return amendmentDisbursement;
    }

    public void setAmendmentDisbursement(int amendmentDisbursement) {
        this.amendmentDisbursement = amendmentDisbursement;
    }

    public int getCancelDisbursement() {
        return cancelDisbursement;
    }

    public void setCancelDisbursement(int cancelDisbursement) {
        this.cancelDisbursement = cancelDisbursement;
    }

    public int getNetCancelDealerProfit() {
        return netCancelDealerProfit;
    }

    public void setNetCancelDealerProfit(int netCancelDealerProfit) {
        this.netCancelDealerProfit = netCancelDealerProfit;
    }

    public int getExemptCancelDisbursement() {
        return exemptCancelDisbursement;
    }

    public void setExemptCancelDisbursement(int exemptCancelDisbursement) {
        this.exemptCancelDisbursement = exemptCancelDisbursement;
    }

    public int getReinstatementDisbursement() {
        return reinstatementDisbursement;
    }

    public void setReinstatementDisbursement(int reinstatementDisbursement) {
        this.reinstatementDisbursement = reinstatementDisbursement;
    }

    public int getNetReinstateDealerProfit() {
        return netReinstateDealerProfit;
    }

    public void setNetReinstateDealerProfit(int netReinstateDealerProfit) {
        this.netReinstateDealerProfit = netReinstateDealerProfit;
    }

    public int getExemptReinstatementDisbursement() {
        return exemptReinstatementDisbursement;
    }

    public void setExemptReinstatementDisbursement(int exemptReinstatementDisbursement) {
        this.exemptReinstatementDisbursement = exemptReinstatementDisbursement;
    }

    public int getTransfer() {
        return transfer;
    }

    public void setTransfer(int transfer) {
        this.transfer = transfer;
    }

    public int getClaimInspection() {
        return claimInspection;
    }

    public void setClaimInspection(int claimInspection) {
        this.claimInspection = claimInspection;
    }

    public int getPaymentAuthorization() {
        return paymentAuthorization;
    }

    public void setPaymentAuthorization(int paymentAuthorization) {
        this.paymentAuthorization = paymentAuthorization;
    }

    public Integer getRemitPendingContractsAfterXDays() {
        return remitPendingContractsAfterXDays;
    }

    public void setRemitPendingContractsAfterXDays(Integer remitPendingContractsAfterXDays) {
        this.remitPendingContractsAfterXDays = remitPendingContractsAfterXDays;
    }

    public int getBatchCorrection() {
        return batchCorrection;
    }

    public void setBatchCorrection(int batchCorrection) {
        this.batchCorrection = batchCorrection;
    }

    public boolean getDefaultInd() {
        return defaultInd;
    }

    public void setDefaultInd(boolean defaultInd) {
        this.defaultInd = defaultInd;
    }

    public int getPayback() {
        return payback;
    }

    public void setPayback(int payback) {
        this.payback = payback;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection() {
        return accountKeeperCollection;
    }

    public void setAccountKeeperCollection(Collection<AccountKeeper> accountKeeperCollection) {
        this.accountKeeperCollection = accountKeeperCollection;
    }

    @XmlTransient
    public Collection<CfDisbursementRemitRule> getCfDisbursementRemitRuleCollection() {
        return cfDisbursementRemitRuleCollection;
    }

    public void setCfDisbursementRemitRuleCollection(Collection<CfDisbursementRemitRule> cfDisbursementRemitRuleCollection) {
        this.cfDisbursementRemitRuleCollection = cfDisbursementRemitRuleCollection;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (remitRuleId != null ? remitRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfRemitRule)) {
            return false;
        }
        CfRemitRule other = (CfRemitRule) object;
        if ((this.remitRuleId == null && other.remitRuleId != null) || (this.remitRuleId != null && !this.remitRuleId.equals(other.remitRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfRemitRule[ remitRuleId=" + remitRuleId + " ]";
    }
    
}
