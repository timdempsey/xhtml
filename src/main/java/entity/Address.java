/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Address")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
    , @NamedQuery(name = "Address.findByAddressId", query = "SELECT a FROM Address a WHERE a.addressId = :addressId")
    , @NamedQuery(name = "Address.findByAddressName", query = "SELECT a FROM Address a WHERE a.addressName = :addressName")
    , @NamedQuery(name = "Address.findByAttentionTo", query = "SELECT a FROM Address a WHERE a.attentionTo = :attentionTo")
    , @NamedQuery(name = "Address.findByAddress1", query = "SELECT a FROM Address a WHERE a.address1 = :address1")
    , @NamedQuery(name = "Address.findByAddress2", query = "SELECT a FROM Address a WHERE a.address2 = :address2")
    , @NamedQuery(name = "Address.findByCity", query = "SELECT a FROM Address a WHERE a.city = :city")
    , @NamedQuery(name = "Address.findByZipCode", query = "SELECT a FROM Address a WHERE a.zipCode = :zipCode")
    , @NamedQuery(name = "Address.findByPhoneNumber", query = "SELECT a FROM Address a WHERE a.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Address.findByExtension", query = "SELECT a FROM Address a WHERE a.extension = :extension")
    , @NamedQuery(name = "Address.findByFax", query = "SELECT a FROM Address a WHERE a.fax = :fax")
    , @NamedQuery(name = "Address.findByLatitude", query = "SELECT a FROM Address a WHERE a.latitude = :latitude")
    , @NamedQuery(name = "Address.findByLongitude", query = "SELECT a FROM Address a WHERE a.longitude = :longitude")
    , @NamedQuery(name = "Address.findByDescription", query = "SELECT a FROM Address a WHERE a.description = :description")
    , @NamedQuery(name = "Address.findByAccountKeeperIdFk", query = "SELECT a FROM Address a WHERE a.accountKeeperIdFk = :accountKeeperIdFk")
    , @NamedQuery(name = "Address.findByAddressTypeIdFk", query = "SELECT a FROM Address a WHERE a.addressTypeIdFk = :addressTypeIdFk")
    , @NamedQuery(name = "Address.findByUpdateUserName", query = "SELECT a FROM Address a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "Address.findByUpdateLast", query = "SELECT a FROM Address a WHERE a.updateLast = :updateLast")})
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "addressId")
    private Integer addressId;
    @Size(max = 50)
    @Column(name = "addressName")
    private String addressName;
    @Size(max = 50)
    @Column(name = "attentionTo")
    private String attentionTo;
    @Size(max = 100)
    @Column(name = "address1")
    private String address1;
    @Size(max = 100)
    @Column(name = "address2")
    private String address2;
    @Size(max = 50)
    @Column(name = "city")
    private String city;
    @Size(max = 9)
    @Column(name = "zipCode")
    private String zipCode;
    @Size(max = 30)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Size(max = 10)
    @Column(name = "extension")
    private String extension;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "fax")
    private String fax;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private BigDecimal latitude;
    @Column(name = "longitude")
    private BigDecimal longitude;
    @Lob
    @Column(name = "coordinates")
    private byte[] coordinates;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Column(name = "accountKeeperIdFk")
    private Integer accountKeeperIdFk;
    @Column(name = "addressTypeIdFk")
    private Integer addressTypeIdFk;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "billingAddressIdFk")
    private Collection<AccountKeeper> accountKeeperCollection;
    @OneToMany(mappedBy = "physicalAddressIdFk")
    private Collection<AccountKeeper> accountKeeperCollection1;
    @OneToMany(mappedBy = "addressIdFk")
    private Collection<CashTransaction> cashTransactionCollection;
    @JoinColumn(name = "regionIdFk", referencedColumnName = "regionId")
    @ManyToOne
    private CfRegion regionIdFk;

    public Address() {
    }

    public Address(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAttentionTo() {
        return attentionTo;
    }

    public void setAttentionTo(String attentionTo) {
        this.attentionTo = attentionTo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public byte[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(byte[] coordinates) {
        this.coordinates = coordinates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(Integer accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public Integer getAddressTypeIdFk() {
        return addressTypeIdFk;
    }

    public void setAddressTypeIdFk(Integer addressTypeIdFk) {
        this.addressTypeIdFk = addressTypeIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection() {
        return accountKeeperCollection;
    }

    public void setAccountKeeperCollection(Collection<AccountKeeper> accountKeeperCollection) {
        this.accountKeeperCollection = accountKeeperCollection;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection1() {
        return accountKeeperCollection1;
    }

    public void setAccountKeeperCollection1(Collection<AccountKeeper> accountKeeperCollection1) {
        this.accountKeeperCollection1 = accountKeeperCollection1;
    }

    @XmlTransient
    public Collection<CashTransaction> getCashTransactionCollection() {
        return cashTransactionCollection;
    }

    public void setCashTransactionCollection(Collection<CashTransaction> cashTransactionCollection) {
        this.cashTransactionCollection = cashTransactionCollection;
    }

    public CfRegion getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(CfRegion regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Address)) {
            return false;
        }
        Address other = (Address) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Address[ addressId=" + addressId + " ]";
    }
    
}
