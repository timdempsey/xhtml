/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AdminCompany")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminCompany.findAll", query = "SELECT a FROM AdminCompany a")
    , @NamedQuery(name = "AdminCompany.findByAdminCompanyId", query = "SELECT a FROM AdminCompany a WHERE a.adminCompanyId = :adminCompanyId")
    , @NamedQuery(name = "AdminCompany.findByName", query = "SELECT a FROM AdminCompany a WHERE a.name = :name")
    , @NamedQuery(name = "AdminCompany.findByCode", query = "SELECT a FROM AdminCompany a WHERE a.code = :code")
    , @NamedQuery(name = "AdminCompany.findByCancelPostingRuleInd", query = "SELECT a FROM AdminCompany a WHERE a.cancelPostingRuleInd = :cancelPostingRuleInd")
    , @NamedQuery(name = "AdminCompany.findByUpdateUserName", query = "SELECT a FROM AdminCompany a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "AdminCompany.findByUpdateLast", query = "SELECT a FROM AdminCompany a WHERE a.updateLast = :updateLast")})
public class AdminCompany implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminCompanyId")
    private Integer adminCompanyId;
    @Size(max = 50)
    @Column(name = "Name")
    private String name;
    @Size(max = 50)
    @Column(name = "Code")
    private String code;
    @Column(name = "CancelPostingRuleInd")
    private Integer cancelPostingRuleInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminCompanyIdFk")
    private Collection<Ledger> ledgerCollection;

    public AdminCompany() {
    }

    public AdminCompany(Integer adminCompanyId) {
        this.adminCompanyId = adminCompanyId;
    }

    public Integer getAdminCompanyId() {
        return adminCompanyId;
    }

    public void setAdminCompanyId(Integer adminCompanyId) {
        this.adminCompanyId = adminCompanyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCancelPostingRuleInd() {
        return cancelPostingRuleInd;
    }

    public void setCancelPostingRuleInd(Integer cancelPostingRuleInd) {
        this.cancelPostingRuleInd = cancelPostingRuleInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminCompanyId != null ? adminCompanyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminCompany)) {
            return false;
        }
        AdminCompany other = (AdminCompany) object;
        if ((this.adminCompanyId == null && other.adminCompanyId != null) || (this.adminCompanyId != null && !this.adminCompanyId.equals(other.adminCompanyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AdminCompany[ adminCompanyId=" + adminCompanyId + " ]";
    }
    
}
