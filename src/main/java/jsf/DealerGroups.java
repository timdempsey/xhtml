/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class DealerGroups {

    private static final Logger LOGGER = LogManager.getLogger(DealerGroups.class);

    public DealerGroups() {
        myAddress = new MyAddress();
    }

    public DealerGroups(Integer dealerGroupId, String dealerGroupName, String remitRule, String phoneNumber, String GLcode, String systemCode, String taxId, String email, Boolean emailOptIn, Boolean holdPayables, String prefpayMethod, MyAddress myAddress) {
        this.dealerGroupId = dealerGroupId;
        this.dealerGroupName = dealerGroupName;
        this.remitRule = remitRule;
        this.phoneNumber = phoneNumber;
        this.GLcode = GLcode;
        this.systemCode = systemCode;
        this.taxId = taxId;
        this.email = email;
        this.emailOptIn = emailOptIn;
        this.holdPayables = holdPayables;
        this.prefpayMethod = prefpayMethod;
        this.myAddress = myAddress;
    }

    

    public DealerGroups(Integer dealerGroupId, String dealerGroupName, String remitRule, String phoneNumber) {
        this.dealerGroupId = dealerGroupId;
        this.dealerGroupName = dealerGroupName;
        this.remitRule = remitRule;
        this.phoneNumber = phoneNumber;
    }

    public DealerGroups(Integer dealerGroupId, String dealerGroupName, String remitRule, String phoneNumber, Boolean isActive) {
        this.dealerGroupId = dealerGroupId;
        this.dealerGroupName = dealerGroupName;
        this.remitRule = remitRule;
        this.phoneNumber = phoneNumber;
        this.isActive = isActive;
    }
    
    

   

    private Integer dealerGroupId;
    private String dealerGroupName;
    private String remitRule;
    private String phoneNumber;
    private String GLcode;
    private String systemCode;
    private String taxId;
    private String email;
    private Boolean emailOptIn;
    private Boolean holdPayables;
    private String prefpayMethod;
    private MyAddress myAddress;
    private Boolean isActive;

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
    
    

    public MyAddress getMyAddress() {
        return myAddress;
    }

    public void setMyAddress(MyAddress myAddress) {
        this.myAddress = myAddress;
    }

    public String getPrefpayMethod() {
        return prefpayMethod;
    }

    public void setPrefpayMethod(String prefpayMethod) {
        this.prefpayMethod = prefpayMethod;
    }
    
    

    public String getGLcode() {
        return GLcode;
    }

    public void setGLcode(String GLcode) {
        this.GLcode = GLcode;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Boolean getHoldPayables() {
        return holdPayables;
    }

    public void setHoldPayables(Boolean holdPayables) {
        this.holdPayables = holdPayables;
    }

    public Integer getDealerGroupId() {
        return dealerGroupId;
    }

    public void setDealerGroupId(Integer dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }

    public String getDealerGroupName() {
        return dealerGroupName;
    }

    public void setDealerGroupName(String dealerGroupName) {
        this.dealerGroupName = dealerGroupName;
    }

    public String getRemitRule() {
        return remitRule;
    }

    public void setRemitRule(String remitRule) {
        this.remitRule = remitRule;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void reset() {
        dealerGroupId = null;
        dealerGroupName = "";
        remitRule = "";
        phoneNumber = "";
        GLcode = "";
        systemCode = "";
        taxId = "";
        email = "";
        emailOptIn = false;
        holdPayables = false;
        prefpayMethod = "";
        isActive = true;
    }

}
