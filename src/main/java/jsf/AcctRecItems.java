/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class AcctRecItems {

    private static final Logger LOGGER = LogManager.getLogger(AcctTrans.class);

    public AcctRecItems() {
    }

    public AcctRecItems(Integer ledgeId, String ledgeType, String contractNo, String vin, String custLastName, String claimNo, BigDecimal adminCredit, BigDecimal adminDebit, BigDecimal adminCommCredit, BigDecimal adminCommDebit, String editDate, String postedDate) {
        this.ledgeId = ledgeId;
        this.ledgeType = ledgeType;
        this.contractNo = contractNo;
        this.vin = vin;
        this.custLastName = custLastName;
        this.claimNo = claimNo;
        this.adminCredit = adminCredit;
        this.adminDebit = adminDebit;
        this.adminCommCredit = adminCommCredit;
        this.adminCommDebit = adminCommDebit;
        this.editDate = editDate;
        this.postedDate = postedDate;
    }

    
    private Integer ledgeId;
    private String ledgeType;
    private String contractNo;
    private String vin;
    private String custLastName;
    private String claimNo;
    private BigDecimal adminCredit;
    private BigDecimal adminDebit;
    private BigDecimal adminCommCredit;
    private BigDecimal adminCommDebit;
    private String editDate;
    private String postedDate;
    
    

    public Integer getLedgeId() {
        return ledgeId;
    }

    public void setLedgeId(Integer ledgeId) {
        this.ledgeId = ledgeId;
    }

    public String getLedgeType() {
        return ledgeType;
    }

    public void setLedgeType(String ledgeType) {
        this.ledgeType = ledgeType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getCustLastName() {
        return custLastName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public BigDecimal getAdminCredit() {
        return adminCredit;
    }

    public void setAdminCredit(BigDecimal adminCredit) {
        this.adminCredit = adminCredit;
    }

    public BigDecimal getAdminDebit() {
        return adminDebit;
    }

    public void setAdminDebit(BigDecimal adminDebit) {
        this.adminDebit = adminDebit;
    }

    public BigDecimal getAdminCommCredit() {
        return adminCommCredit;
    }

    public void setAdminCommCredit(BigDecimal adminCommCredit) {
        this.adminCommCredit = adminCommCredit;
    }

    public BigDecimal getAdminCommDebit() {
        return adminCommDebit;
    }

    public void setAdminCommDebit(BigDecimal adminCommDebit) {
        this.adminCommDebit = adminCommDebit;
    }

    

    public String getEditDate() {
        return editDate;
    }

    public void setEditDate(String editDate) {
        this.editDate = editDate;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public void reset() {
        ledgeId = null;
        ledgeType = "";
        contractNo = "";
        vin = "";
        custLastName = "";
        claimNo = "";
        adminCredit = null;
        adminDebit = null;
        adminCommCredit = null;
        adminCommDebit = null;
        editDate = "";
        postedDate = "";
    }

}
