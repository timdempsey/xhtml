/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

/**
 *
 * @author Jiepi
 */
public class ClaimGeneralPPM implements Serializable{

    public ClaimGeneralPPM() {
    }
    
    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    String rfName;
    @Column(name = "name", table = "RepairFacilityContactPerson")
    String contactName;
    @Column(name = "currentOdometer", table = "Claim")
    Integer odometer;
    @Column(name = "paidToFk", table = "Claim")
    String paidTo;
    @Column(name = "assignedTo", table = "Claim")
    String assignedTo;
    @Column(name = "repairOrderNumber", table = "Claim")
    String roNumber;
    @Column(name = "inceptionDate", table = "Claim")
    String inceptionDate;

    public ClaimGeneralPPM(Integer claimId, String claimNumber, String rfName, String contactName, Integer odometer, String paidTo, String assignedTo, String roNumber, String inceptionDate) {
        this.claimId = claimId;
        this.claimNumber = claimNumber;
        this.rfName = rfName;
        this.contactName = contactName;
        this.odometer = odometer;
        this.paidTo = paidTo;
        this.assignedTo = assignedTo;
        this.roNumber = roNumber;
        this.inceptionDate = inceptionDate;
    }

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getRfName() {
        return rfName;
    }

    public void setRfName(String rfName) {
        this.rfName = rfName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public String getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(String paidTo) {
        this.paidTo = paidTo;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getRoNumber() {
        return roNumber;
    }

    public void setRoNumber(String roNumber) {
        this.roNumber = roNumber;
    }

    public String getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(String inceptionDate) {
        this.inceptionDate = inceptionDate;
    }
    
    
    
    
    
    
}
