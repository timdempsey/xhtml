/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.CfCancelMethod;
import entity.CfCancelMethodGroup;
import entity.CfCancelMethodGroupRule;
import entity.CfCancelReason;
import entity.CfCriterionGroup;
import entity.Contracts;
import entity.Dealer;
import entity.DtCancelMethodType;
import entity.DtCancelType;
import entity.PlanTable;
import entity.Product;
import entity.ProductType;
import entity.Program;
import entity.TermDetail;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractCancelService")
@ApplicationScoped
public class ContractCancelService {

    private static final Logger LOGGER = LogManager.getLogger(ContractCancelService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ContractCancelService
     */
    public ContractCancelService() {

    }

    private String contractNo;
    private ContractCancelQuote contractCancelQuote;
    private List<String> cancelReasonList;
    private List<String> cancelMethodList;
    private ContractCancelBreakdown contractCancelBreakdown;

    private SearchCriterion searchCriterion;

    private Dealer dealer;
    private Program program;
    private Product product;
    private PlanTable plan;
    private Contracts contract;
    private CfCancelMethod cfCancelMethod;

    public SearchCriterion getSearchCriterion() {
        return searchCriterion;
    }

    public void setSearchCriterion(SearchCriterion searchCriterion) {
        this.searchCriterion = searchCriterion;
    }

    public ContractCancelBreakdown getContractCancelBreakdown() {
        return contractCancelBreakdown;
    }

    public void setContractCancelBreakdown(ContractCancelBreakdown contractCancelBreakdown) {
        this.contractCancelBreakdown = contractCancelBreakdown;
    }

    public List<String> getCancelReasonList() {
        return cancelReasonList;
    }

    public void setCancelReasonList(List<String> cancelReasonList) {
        this.cancelReasonList = cancelReasonList;
    }

    public List<String> getCancelMethodList() {
        return cancelMethodList;
    }

    public void setCancelMethodList(List<String> cancelMethodList) {
        this.cancelMethodList = cancelMethodList;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public ContractCancelQuote getContractCancelQuote() {
        return contractCancelQuote;
    }

    public void setContractCancelQuote(ContractCancelQuote contractCancelQuote) {
        this.contractCancelQuote = contractCancelQuote;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("in ContractCancelService init");

        contractCancelQuote = new ContractCancelQuote();
        contractCancelQuote.reset();

        List<CfCancelReason> crList = claimMisc.getCfCancelReasonByAll();
        cancelReasonList = new ArrayList<>();
        for (CfCancelReason cr : crList) {
            cancelReasonList.add(cr.getReason());
        }

        List<CfCancelMethod> cmList = claimMisc.getCfCancelMethodByAll();
        cancelMethodList = new ArrayList<>();
        /*
        for (CfCancelMethod cm : cmList) {
            cancelMethodList.add(cm.getDescription());
        }
         */

        contractCancelBreakdown = new ContractCancelBreakdown();
        contractCancelBreakdown.reset();

        searchCriterion = new SearchCriterion();

        reset();
    }

    /*
    *   main methods
     */
    public void overrideCanccelMethod() {
        LOGGER.info("in overrideCancelMethod");
    }

    public Boolean checkCancelConditions() {
        contract = claimMisc.getContractsByNo(contractNo);
        Boolean cancelConditionGood = true;
        Integer planExpireUsageType = contract.getPlanExpireUsageTypeIdFk();
        Integer expirationUsage = null;
        
        if( contractCancelQuote.getOdometer() == null ) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Please enter odometer greater than ", contract.getOdometer().toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            cancelConditionGood = false;
        }
        if(  contractCancelQuote.getCancelDate() == null ) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Please enter cancel  date before expire date ", claimMisc.getJavaDate(contract.getExpirationDate()) );
            FacesContext.getCurrentInstance().addMessage(null, msg);
            cancelConditionGood = false;
        }
   
        if (contractCancelQuote.getOdometer() < contract.getOdometer()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cannot cancel a contract with a cancel odometer less than start mileage ", contract.getOdometer().toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            cancelConditionGood = false;
        } else if ( contract.getExpirationUsage()!=null && contractCancelQuote.getOdometer() > contract.getExpirationUsage() ) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cannot cancel a contract with a cancel odometer greater than expire mileage ", contract.getOdometer().toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            cancelConditionGood = false;
        } else if( claimMisc.getUtilDiffDays(contract.getExpirationDate(), contractCancelQuote.getCancelDate() ) > 0 ) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cannot cancel a contract with a cancel date after it expires ", claimMisc.getJavaDate(contract.getExpirationDate()));
            FacesContext.getCurrentInstance().addMessage(null, msg);
            cancelConditionGood = false;
        }
        return cancelConditionGood;
    }

    public void openCancelQuoteBreakdown() {
        LOGGER.info("in openCancelQuoteBreakdown");
        //RequestContext.getCurrentInstance().closeDialog("contractCancelQuote");

        // check where some of conditions met
        // odometer
        if (checkCancelConditions()) {

            createBreakdownView();

            Map<String, Object> options = new HashMap<>();
            options.put("resizable", true);
            options.put("draggable", true);
            options.put("contentHeight", "100%");
            options.put("contentWidth", "100%");
            options.put("height", "1200");
            options.put("width", "1000");
            options.put("modal", true);

            LOGGER.info("opendialog");

            RequestContext.getCurrentInstance().openDialog("ContractCancelBreakdown", options, null);
            //RequestContext.getCurrentInstance().closeDialog("contractCancelQuote");
        }
    }

    public void createBreakdownView() {
        LOGGER.info("in createBreakdownView, contractNo=" + contractNo);

        contractCancelBreakdown.setContractNo(contractNo);
        contractCancelBreakdown.setProduct(product.getProductName());
        contractCancelBreakdown.setPlan(plan.getPlanName());
        contractCancelBreakdown.setEffectiveDate(claimMisc.getJavaDate(contract.getEffectiveDate()));
        contractCancelBreakdown.setExpiredDate(claimMisc.getJavaDate(contract.getExpirationDate()));
        contractCancelBreakdown.setContractPurchasePrice(contract.getContractPurchasePrice());
        if (contractCancelQuote.getCancelDate() != null) {
            contractCancelBreakdown.setCancelDate(claimMisc.getJavaDate(contractCancelQuote.getCancelDate()));
        }
        if (contractCancelQuote.getReceivedDate() != null) {
            contractCancelBreakdown.setCancelReceivedDate(claimMisc.getJavaDate(contractCancelQuote.getReceivedDate()));
        }
        contractCancelBreakdown.setCancelReason(contractCancelQuote.getCancelReason());
        contractCancelBreakdown.setCancelMethod(contractCancelQuote.getCancelMethod());
        contractCancelBreakdown.setCurrentOdometer(contractCancelQuote.getOdometer());
        
        contractCancelBreakdown.setUnPaidClaim(claimMisc.getUnPaidClaimsByContractNo(contractNo));

        LOGGER.info("cancelMethodId===" + cfCancelMethod.getCancelMethodId());
        List<CfCancelMethodGroup> cancelMethodGroupList = claimMisc.getCfCancelMethodGroupByFkId(cfCancelMethod.getCancelMethodId());
        LOGGER.info("size of cfCancelMethodGroupList=" + cancelMethodGroupList.size());

        Integer termMileage = null;
        Long termDays = null;
        Integer coveredMileage = null;
        Long coveredDays = null;
        BigDecimal refundRate = claimMisc.BIGZERO;
        Integer usedMileage = null;
        Long usedDays = null;

        Date d1 = claimMisc.getCurrentDate();
        Date d2 = new Date(contract.getEffectiveDate().getTime());

        Long effectiveDays = claimMisc.getUtilDiffDays(d2, d1);
        LOGGER.info("effectDays=" + effectiveDays);
        searchCriterion.setDays(effectiveDays);

        usedMileage = contractCancelQuote.getOdometer() - contract.getOdometer();
        usedDays = effectiveDays;
        LOGGER.info("usedMileage=" + usedMileage);
        LOGGER.info("usedDays=" + usedDays);

        TermDetail termDetail = claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0);
        if (claimMisc.isVSC(product)) {
            termMileage = termDetail.getVscExpireUsage();
            Date endDate = claimMisc.addMonth(d2, termDetail.getVscExpireTime());
            termDays = claimMisc.getUtilDiffDays(d2, endDate);
            coveredMileage = termMileage - usedMileage;
            coveredDays = termDays - usedDays;
        } else if (claimMisc.isANC(product)) {
            Date endDate = claimMisc.addMonth(d2, termDetail.getAncMonths());
            termDays = claimMisc.getUtilDiffDays(d2, endDate);
            coveredDays = termDays - usedDays;
        } else if (claimMisc.isPPM(product)) {
            termMileage = termDetail.getPpmUsage();
            Date endDate = claimMisc.addMonth(d2, termDetail.getPpmTime());
            termDays = claimMisc.getUtilDiffDays(d2, endDate);
            coveredMileage = termMileage - usedMileage;
            coveredDays = termDays - usedDays;
        } else if (claimMisc.isGAP(product)) {
            // later
        }
        LOGGER.info("termMileage=" + termMileage);
        LOGGER.info("termDays=" + termDays);
        LOGGER.info("coveredMileage=" + coveredMileage);
        LOGGER.info("coveredDays=" + coveredDays);

        String ruleType = "Prorated";
        String cancelType = "Term Days";

        for (CfCancelMethodGroup cmg : cancelMethodGroupList) {
            CfCriterionGroup cfCriterionGroup = claimMisc.getCfCriterionGroupById(cmg.getCriterionGroupIdFk().getCriterionGroupId());
            if (claimMisc.isMatchedCriterionGroup(searchCriterion, cfCriterionGroup.getDescription())) {
                LOGGER.info("CancelMethodGroupId=" + cmg.getCancelMethodGroupId());
                CfCancelMethodGroupRule cmgr = claimMisc.getCfCancelMethodGroupRuleByFkId(cmg.getCancelMethodGroupId());
                DtCancelMethodType cmt = claimMisc.getDtCancelMethodTypeById(cmgr.getCancelMethodRuleTypeInd().getCancelMethodTypeId());
                DtCancelType ct = claimMisc.getDtCancelTypeById(cmgr.getCancelTypeInd().getCancelTypeId());
                ruleType = cmt.getDescription();
                cancelType = ct.getDescription();
                LOGGER.info("ruleType=" + ruleType);
                LOGGER.info("cancelType=" + cancelType);

                contractCancelBreakdown.setCancelMethodGroup(cfCriterionGroup.getDescription());
                contractCancelBreakdown.setCancelGroupRuleMethod(ruleType);
                contractCancelBreakdown.setCancelGroupRuleType(cancelType);

               
                break;
            }
        }

        switch (ruleType) {
            case "Prorated":
                switch (cancelType) {
                    case "Term Days":
                        LOGGER.info("Term Days");
                        BigDecimal bg1 = new BigDecimal(coveredDays);
                        BigDecimal bg2 = new BigDecimal(termDays);
                        refundRate = bg1.divide(bg2, 2, RoundingMode.CEILING);
                        break;
                    case "Term Mileage":
                        LOGGER.info("Term Mileage");
                        bg1 = new BigDecimal(coveredMileage);
                        bg2 = new BigDecimal(termMileage);
                        refundRate = bg1.divide(bg2, 2, RoundingMode.CEILING);
                    default:
                        LOGGER.info("in default mode");
                        break;
                }
                break;
            case "Fixed Percent":
                break;
            case "Full Refund":
                refundRate = new BigDecimal(100);
                break;
            case "No Refund":
                refundRate = new BigDecimal(0);
                break;
            default:
                break;
        }
        LOGGER.info("refundRate=" + refundRate);

        // fill the admin refund data
        BigDecimal coverageCost = claimMisc.getContractCoverageCost(contractNo);
        contractCancelBreakdown.setCoverageCost(coverageCost);
        BigDecimal displayRefundRatio = refundRate.multiply(new BigDecimal(100));
        contractCancelBreakdown.setRefundRatio(displayRefundRatio);
        BigDecimal coverageRefund = coverageCost.multiply(refundRate).setScale(2, RoundingMode.HALF_UP);
        contractCancelBreakdown.setAdminRefund(coverageRefund);
        BigDecimal adminFee = new BigDecimal(50);
        BigDecimal adminPortion = coverageRefund.subtract(adminFee);
        contractCancelBreakdown.setAdminFee(adminFee);
        contractCancelBreakdown.setAdminPortion(adminPortion);
        // fill the dealer profit refund data
        BigDecimal dealerProfit = contract.getContractPurchasePrice().subtract(coverageCost);
        contractCancelBreakdown.setDealerProfit(dealerProfit);
        BigDecimal dpRefund = dealerProfit.multiply(refundRate).setScale(2, RoundingMode.HALF_UP);
        contractCancelBreakdown.setDpRefund(dpRefund);
        BigDecimal dealerRetainedFee = new BigDecimal(-50);
        contractCancelBreakdown.setDealerRetainedFee(dealerRetainedFee);
        BigDecimal dealerPortion = dpRefund.subtract(dealerRetainedFee);
        contractCancelBreakdown.setDealerPortion(dealerPortion);
        BigDecimal customerRefund = adminPortion.add(dealerPortion);
        contractCancelBreakdown.setCustomerRefund(customerRefund);

    }

    public void cancelContract() {
        LOGGER.info("in cancelContract");
        
        claimMisc.cancelContract(contractNo);
        
        RequestContext.getCurrentInstance().closeDialog("ContractCancelBreakdown");
    }
    
    public void printCustomerCancelContract() {
        LOGGER.info("in printCustomerCancelContract");
    }
    
    public void printDealerCancelContract() {
        LOGGER.info("printDealerCancelContract");
    }

    public void generateCancelMethodList() {
        LOGGER.info("in generateCancelMethodList");
        String cancelMethod = findCancelMethod();
        LOGGER.info("cancelMethod=" + cancelMethod);
        cancelMethodList.add(cancelMethod);
    }

    public String findCancelMethod() {
        String retMethod = "";
        contract = claimMisc.getContractsByNo(contractNo);
        product = claimMisc.getProductById(contract.getProductIdFk().getProductId());
        plan = claimMisc.getPlanById(contract.getPlanIdFk().getPlanId());

        dealer = claimMisc.getDealerById(contract.getDealerIdFk().getDealerId());

        program = claimMisc.getProgramById(product.getProgramIdFk().getProgramId());

        ProductType productType = claimMisc.getProductTypeById(product.getProductTypeIdFk().getProductTypeId());
        List<CfCancelMethod> cmList = claimMisc.getCfCancelMethodByProductTypeId(productType.getProductTypeId());

        searchCriterion.setDealerName(claimMisc.getAccountKeeperByContractDealer(contractNo).getAccountKeeperName());
        searchCriterion.setProgramName(program.getName());
        searchCriterion.setProductName(product.getProductName());
        searchCriterion.setPlanName(plan.getPlanName());

        for (CfCancelMethod cancelMethod : cmList) {

            CfCriterionGroup cfCriterionGroup = claimMisc.getCfCriterionGroupById(cancelMethod.getCriterionGroupIdFk().getCriterionGroupId());

            if (claimMisc.isMatchedCriterionGroup(searchCriterion, cfCriterionGroup.getDescription())) {
                LOGGER.info("matched");
                LOGGER.info("criterion=" + cfCriterionGroup.getDescription());
                LOGGER.info("cancelMethod=" + cancelMethod.getDescription());
                retMethod = cancelMethod.getDescription();
                cfCancelMethod = cancelMethod;
                break;
            }
        }
        return retMethod;
    }

    public void reset() {
        contract = null;
        dealer = null;
        program = null;
        product = null;
        plan = null;
        cfCancelMethod = null;
        searchCriterion.reset();
    }

}
