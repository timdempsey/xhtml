/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtDisbursementAmountType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtDisbursementAmountType.findAll", query = "SELECT d FROM DtDisbursementAmountType d")
    , @NamedQuery(name = "DtDisbursementAmountType.findByDisbursementAmountTypeId", query = "SELECT d FROM DtDisbursementAmountType d WHERE d.disbursementAmountTypeId = :disbursementAmountTypeId")
    , @NamedQuery(name = "DtDisbursementAmountType.findByDescription", query = "SELECT d FROM DtDisbursementAmountType d WHERE d.description = :description")})
public class DtDisbursementAmountType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DisbursementAmountTypeId")
    private Integer disbursementAmountTypeId;
    @Size(max = 60)
    @Column(name = "Description")
    private String description;

    public DtDisbursementAmountType() {
    }

    public DtDisbursementAmountType(Integer disbursementAmountTypeId) {
        this.disbursementAmountTypeId = disbursementAmountTypeId;
    }

    public Integer getDisbursementAmountTypeId() {
        return disbursementAmountTypeId;
    }

    public void setDisbursementAmountTypeId(Integer disbursementAmountTypeId) {
        this.disbursementAmountTypeId = disbursementAmountTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementAmountTypeId != null ? disbursementAmountTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtDisbursementAmountType)) {
            return false;
        }
        DtDisbursementAmountType other = (DtDisbursementAmountType) object;
        if ((this.disbursementAmountTypeId == null && other.disbursementAmountTypeId != null) || (this.disbursementAmountTypeId != null && !this.disbursementAmountTypeId.equals(other.disbursementAmountTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtDisbursementAmountType[ disbursementAmountTypeId=" + disbursementAmountTypeId + " ]";
    }
    
}
