/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimContract implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    public ClaimContract() {
    }

    public ClaimContract(String contractNo, String contractStatus, String customer, String phone, String program, String product, String plan, String term, String classCode, String deductible, String effectiveDate, String contractExpirationDate,  Integer odometer, Integer claimStartUsage, Integer expirationusage, BigDecimal contractPurchasePrice) {
        this.contractNo = contractNo;
        this.contractStatus = contractStatus;
        this.customer = customer;
        this.phone = phone;
        this.program = program;
        this.product = product;
        this.plan = plan;
        this.term = term;
        this.classCode = classCode;
        this.deductible = deductible;
        this.effectiveDate = effectiveDate;
        this.contractExpirationDate = contractExpirationDate;
        this.odometer = odometer;
        this.claimStartUsage = claimStartUsage;
        this.expirationusage = expirationusage;
        this.contractPurchasePrice = contractPurchasePrice;
    }

    
    

    @Column(name = "contractNo", table = "Contracts")
    private String contractNo;
    @Column(name = "contractStatus", table = "Contracts")
    private String contractStatus;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    private String customer;
    @Column(name = "phoneNumber", table = "Address")
    String phone;
    private String program;
    private String product;
    private String plan;
    private String term;
    @Column(name = "classCode", table = "Contracts")
    private String classCode;
    private String deductible;
    @Column(name = "effectiveDate", table = "Contracts")
    private String effectiveDate;
    @Column(name = "contractExpirationDate", table = "Contracts")
    private String contractExpirationDate;
    @Column(name = "odometer", table = "Contracts")
    private Integer odometer;
    @Column(name = "claimStartUsage", table = "Contracts")
    private Integer claimStartUsage;
    @Column(name = "expirationUsage", table = "Contracts")
    private Integer expirationusage;
    @Column(name = "contractPurchasePrice", table = "Contracts")
    private BigDecimal contractPurchasePrice;

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
    
    

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getContractExpirationDate() {
        return contractExpirationDate;
    }

    public void setContractExpirationDate(String contractExpirationDate) {
        this.contractExpirationDate = contractExpirationDate;
    }

   
    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public Integer getClaimStartUsage() {
        return claimStartUsage;
    }

    public void setClaimStartUsage(Integer claimStartUsage) {
        this.claimStartUsage = claimStartUsage;
    }

    public Integer getExpirationusage() {
        return expirationusage;
    }

    public void setExpirationusage(Integer expirationusage) {
        this.expirationusage = expirationusage;
    }

    public BigDecimal getContractPurchasePrice() {
        return contractPurchasePrice;
    }

    public void setContractPurchasePrice(BigDecimal contractPurchasePrice) {
        this.contractPurchasePrice = contractPurchasePrice;
    }
    
    
    
}
