/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DealerRateVisibilityRel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DealerRateVisibilityRel.findAll", query = "SELECT d FROM DealerRateVisibilityRel d")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByDealerRateVisibilityRelId", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.dealerRateVisibilityRelId = :dealerRateVisibilityRelId")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByComment", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.comment = :comment")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByEffectiveDate", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByExpireDate", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.expireDate = :expireDate")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByUpdateUserName", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DealerRateVisibilityRel.findByUpdateLast", query = "SELECT d FROM DealerRateVisibilityRel d WHERE d.updateLast = :updateLast")})
public class DealerRateVisibilityRel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerRateVisibilityRelId")
    private Integer dealerRateVisibilityRelId;
    @Size(max = 200)
    @Column(name = "comment")
    private String comment;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveDate;
    @Column(name = "expireDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expireDate;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "rateVisibilityIdFk", referencedColumnName = "rateVisibilityId")
    @ManyToOne
    private RateVisibility rateVisibilityIdFk;

    public DealerRateVisibilityRel() {
    }

    public DealerRateVisibilityRel(Integer dealerRateVisibilityRelId) {
        this.dealerRateVisibilityRelId = dealerRateVisibilityRelId;
    }

    public Integer getDealerRateVisibilityRelId() {
        return dealerRateVisibilityRelId;
    }

    public void setDealerRateVisibilityRelId(Integer dealerRateVisibilityRelId) {
        this.dealerRateVisibilityRelId = dealerRateVisibilityRelId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public RateVisibility getRateVisibilityIdFk() {
        return rateVisibilityIdFk;
    }

    public void setRateVisibilityIdFk(RateVisibility rateVisibilityIdFk) {
        this.rateVisibilityIdFk = rateVisibilityIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dealerRateVisibilityRelId != null ? dealerRateVisibilityRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DealerRateVisibilityRel)) {
            return false;
        }
        DealerRateVisibilityRel other = (DealerRateVisibilityRel) object;
        if ((this.dealerRateVisibilityRelId == null && other.dealerRateVisibilityRelId != null) || (this.dealerRateVisibilityRelId != null && !this.dealerRateVisibilityRelId.equals(other.dealerRateVisibilityRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DealerRateVisibilityRel[ dealerRateVisibilityRelId=" + dealerRateVisibilityRelId + " ]";
    }

}
