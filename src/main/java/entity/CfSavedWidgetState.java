/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfSavedWidgetState")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfSavedWidgetState.findAll", query = "SELECT c FROM CfSavedWidgetState c")
    , @NamedQuery(name = "CfSavedWidgetState.findBySavedWidgetStateId", query = "SELECT c FROM CfSavedWidgetState c WHERE c.savedWidgetStateId = :savedWidgetStateId")
    , @NamedQuery(name = "CfSavedWidgetState.findByWidgetGuid", query = "SELECT c FROM CfSavedWidgetState c WHERE c.widgetGuid = :widgetGuid")
    , @NamedQuery(name = "CfSavedWidgetState.findByState", query = "SELECT c FROM CfSavedWidgetState c WHERE c.state = :state")
    , @NamedQuery(name = "CfSavedWidgetState.findByPosition", query = "SELECT c FROM CfSavedWidgetState c WHERE c.position = :position")
    , @NamedQuery(name = "CfSavedWidgetState.findByUpdateUserName", query = "SELECT c FROM CfSavedWidgetState c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfSavedWidgetState.findByUpdateLast", query = "SELECT c FROM CfSavedWidgetState c WHERE c.updateLast = :updateLast")})
public class CfSavedWidgetState implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "savedWidgetStateId")
    private Integer savedWidgetStateId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "widgetGuid")
    private String widgetGuid;
    @Size(max = 2147483647)
    @Column(name = "state")
    private String state;
    @Column(name = "position")
    private Integer position;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "widgetGroupIdFk", referencedColumnName = "widgetGroupId")
    @ManyToOne
    private CfWidgetGroup widgetGroupIdFk;
    @JoinColumn(name = "userMemberIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember userMemberIdFk;

    public CfSavedWidgetState() {
    }

    public CfSavedWidgetState(Integer savedWidgetStateId) {
        this.savedWidgetStateId = savedWidgetStateId;
    }

    public CfSavedWidgetState(Integer savedWidgetStateId, String widgetGuid) {
        this.savedWidgetStateId = savedWidgetStateId;
        this.widgetGuid = widgetGuid;
    }

    public Integer getSavedWidgetStateId() {
        return savedWidgetStateId;
    }

    public void setSavedWidgetStateId(Integer savedWidgetStateId) {
        this.savedWidgetStateId = savedWidgetStateId;
    }

    public String getWidgetGuid() {
        return widgetGuid;
    }

    public void setWidgetGuid(String widgetGuid) {
        this.widgetGuid = widgetGuid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfWidgetGroup getWidgetGroupIdFk() {
        return widgetGroupIdFk;
    }

    public void setWidgetGroupIdFk(CfWidgetGroup widgetGroupIdFk) {
        this.widgetGroupIdFk = widgetGroupIdFk;
    }

    public UserMember getUserMemberIdFk() {
        return userMemberIdFk;
    }

    public void setUserMemberIdFk(UserMember userMemberIdFk) {
        this.userMemberIdFk = userMemberIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (savedWidgetStateId != null ? savedWidgetStateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfSavedWidgetState)) {
            return false;
        }
        CfSavedWidgetState other = (CfSavedWidgetState) object;
        if ((this.savedWidgetStateId == null && other.savedWidgetStateId != null) || (this.savedWidgetStateId != null && !this.savedWidgetStateId.equals(other.savedWidgetStateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfSavedWidgetState[ savedWidgetStateId=" + savedWidgetStateId + " ]";
    }
    
}
