/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtDisbursementRefundType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtDisbursementRefundType.findAll", query = "SELECT d FROM DtDisbursementRefundType d")
    , @NamedQuery(name = "DtDisbursementRefundType.findByDisbursementRefundTypeId", query = "SELECT d FROM DtDisbursementRefundType d WHERE d.disbursementRefundTypeId = :disbursementRefundTypeId")
    , @NamedQuery(name = "DtDisbursementRefundType.findByDescription", query = "SELECT d FROM DtDisbursementRefundType d WHERE d.description = :description")})
public class DtDisbursementRefundType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementRefundTypeId")
    private Integer disbursementRefundTypeId;
    @Size(max = 60)
    @Column(name = "Description")
    private String description;

    public DtDisbursementRefundType() {
    }

    public DtDisbursementRefundType(Integer disbursementRefundTypeId) {
        this.disbursementRefundTypeId = disbursementRefundTypeId;
    }

    public Integer getDisbursementRefundTypeId() {
        return disbursementRefundTypeId;
    }

    public void setDisbursementRefundTypeId(Integer disbursementRefundTypeId) {
        this.disbursementRefundTypeId = disbursementRefundTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementRefundTypeId != null ? disbursementRefundTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtDisbursementRefundType)) {
            return false;
        }
        DtDisbursementRefundType other = (DtDisbursementRefundType) object;
        if ((this.disbursementRefundTypeId == null && other.disbursementRefundTypeId != null) || (this.disbursementRefundTypeId != null && !this.disbursementRefundTypeId.equals(other.disbursementRefundTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtDisbursementRefundType[ disbursementRefundTypeId=" + disbursementRefundTypeId + " ]";
    }
    
}
