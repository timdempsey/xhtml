/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "VINClassCode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VINClassCode.findAll", query = "SELECT v FROM VINClassCode v")
    , @NamedQuery(name = "VINClassCode.findByVinClassCodeId", query = "SELECT v FROM VINClassCode v WHERE v.vinClassCodeId = :vinClassCodeId")
    , @NamedQuery(name = "VINClassCode.findByVinCode", query = "SELECT v FROM VINClassCode v WHERE v.vinCode = :vinCode")
    , @NamedQuery(name = "VINClassCode.findByVinOrder", query = "SELECT v FROM VINClassCode v WHERE v.vinOrder = :vinOrder")})
public class VINClassCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinClassCodeId")
    private Integer vinClassCodeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "vinCode")
    private String vinCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinOrder")
    private int vinOrder;
    @OneToMany(mappedBy = "vinClassCodeIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "classCodeIdFk")
    private Collection<Rate> rateCollection;
    @JoinColumn(name = "ClassCodeNameIdFk", referencedColumnName = "classCodeNameId")
    @ManyToOne
    private ClassCodeName classCodeNameIdFk;

    public VINClassCode() {
    }

    public VINClassCode(Integer vinClassCodeId) {
        this.vinClassCodeId = vinClassCodeId;
    }

    public VINClassCode(Integer vinClassCodeId, String vinCode, int vinOrder) {
        this.vinClassCodeId = vinClassCodeId;
        this.vinCode = vinCode;
        this.vinOrder = vinOrder;
    }

    public Integer getVinClassCodeId() {
        return vinClassCodeId;
    }

    public void setVinClassCodeId(Integer vinClassCodeId) {
        this.vinClassCodeId = vinClassCodeId;
    }

    public String getVinCode() {
        return vinCode;
    }

    public void setVinCode(String vinCode) {
        this.vinCode = vinCode;
    }

    public int getVinOrder() {
        return vinOrder;
    }

    public void setVinOrder(int vinOrder) {
        this.vinOrder = vinOrder;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<Rate> getRateCollection() {
        return rateCollection;
    }

    public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }

    public ClassCodeName getClassCodeNameIdFk() {
        return classCodeNameIdFk;
    }

    public void setClassCodeNameIdFk(ClassCodeName classCodeNameIdFk) {
        this.classCodeNameIdFk = classCodeNameIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vinClassCodeId != null ? vinClassCodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VINClassCode)) {
            return false;
        }
        VINClassCode other = (VINClassCode) object;
        if ((this.vinClassCodeId == null && other.vinClassCodeId != null) || (this.vinClassCodeId != null && !this.vinClassCodeId.equals(other.vinClassCodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VINClassCode[ vinClassCodeId=" + vinClassCodeId + " ]";
    }
    
}
