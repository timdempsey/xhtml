/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimMiscPayment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimMiscPayment.findAll", query = "SELECT c FROM ClaimMiscPayment c")
    , @NamedQuery(name = "ClaimMiscPayment.findByClaimMiscPaymentId", query = "SELECT c FROM ClaimMiscPayment c WHERE c.claimMiscPaymentId = :claimMiscPaymentId")
    , @NamedQuery(name = "ClaimMiscPayment.findByEnteredDate", query = "SELECT c FROM ClaimMiscPayment c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "ClaimMiscPayment.findByClaimMiscPaymentDetailIdFk", query = "SELECT c FROM ClaimMiscPayment c WHERE c.claimMiscPaymentDetailIdFk = :claimMiscPaymentDetailIdFk")
    , @NamedQuery(name = "ClaimMiscPayment.findByUpdateUserName", query = "SELECT c FROM ClaimMiscPayment c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimMiscPayment.findByUpdateLast", query = "SELECT c FROM ClaimMiscPayment c WHERE c.updateLast = :updateLast")})
public class ClaimMiscPayment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimMiscPaymentId")
    private Integer claimMiscPaymentId;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "claimMiscPaymentDetailIdFk")
    private Integer claimMiscPaymentDetailIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "claimMiscPaymentIdFk")
    private Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "claimDetailIdFk", referencedColumnName = "claimDetailId")
    @ManyToOne
    private ClaimDetail claimDetailIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public ClaimMiscPayment() {
    }

    public ClaimMiscPayment(Integer claimMiscPaymentId) {
        this.claimMiscPaymentId = claimMiscPaymentId;
    }

    public Integer getClaimMiscPaymentId() {
        return claimMiscPaymentId;
    }

    public void setClaimMiscPaymentId(Integer claimMiscPaymentId) {
        this.claimMiscPaymentId = claimMiscPaymentId;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Integer getClaimMiscPaymentDetailIdFk() {
        return claimMiscPaymentDetailIdFk;
    }

    public void setClaimMiscPaymentDetailIdFk(Integer claimMiscPaymentDetailIdFk) {
        this.claimMiscPaymentDetailIdFk = claimMiscPaymentDetailIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimMiscPaymentDetail> getClaimMiscPaymentDetailCollection() {
        return claimMiscPaymentDetailCollection;
    }

    public void setClaimMiscPaymentDetailCollection(Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection) {
        this.claimMiscPaymentDetailCollection = claimMiscPaymentDetailCollection;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public ClaimDetail getClaimDetailIdFk() {
        return claimDetailIdFk;
    }

    public void setClaimDetailIdFk(ClaimDetail claimDetailIdFk) {
        this.claimDetailIdFk = claimDetailIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimMiscPaymentId != null ? claimMiscPaymentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimMiscPayment)) {
            return false;
        }
        ClaimMiscPayment other = (ClaimMiscPayment) object;
        if ((this.claimMiscPaymentId == null && other.claimMiscPaymentId != null) || (this.claimMiscPaymentId != null && !this.claimMiscPaymentId.equals(other.claimMiscPaymentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimMiscPayment[ claimMiscPaymentId=" + claimMiscPaymentId + " ]";
    }
    
}
