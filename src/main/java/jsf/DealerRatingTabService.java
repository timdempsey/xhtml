/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "dealerRatingTabService")
@ApplicationScoped
public class DealerRatingTabService {
    private static final Logger LOGGER = LogManager.getLogger(DealerService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of DealerRatingTabService
     */
    public DealerRatingTabService() {
    }
    
    private Integer activeIndex;

    public Integer getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(Integer activeIndex) {
        this.activeIndex = activeIndex;
    }
    
    
    
     public void ratingTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);
        switch (tabId) {
            case "dldTab":  // Disbursements
                activeIndex = 0;
                break;
            case "drrTab":  // Rate Visibilities
                activeIndex = 1;
                break;
            case "drgTab":  // Global SRPs
                activeIndex = 2;
                break;
            default:
                break;
        }
    }
    
}
