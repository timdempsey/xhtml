/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfSettingListItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfSettingListItem.findAll", query = "SELECT c FROM CfSettingListItem c")
    , @NamedQuery(name = "CfSettingListItem.findBySettingListItemId", query = "SELECT c FROM CfSettingListItem c WHERE c.settingListItemId = :settingListItemId")
    , @NamedQuery(name = "CfSettingListItem.findByValue", query = "SELECT c FROM CfSettingListItem c WHERE c.value = :value")
    , @NamedQuery(name = "CfSettingListItem.findByUpdateUserName", query = "SELECT c FROM CfSettingListItem c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfSettingListItem.findByUpdateLast", query = "SELECT c FROM CfSettingListItem c WHERE c.updateLast = :updateLast")})
public class CfSettingListItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "settingListItemId")
    private Integer settingListItemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "value")
    private String value;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "settingIdFk", referencedColumnName = "settingId")
    @ManyToOne(optional = false)
    private CfSetting settingIdFk;

    public CfSettingListItem() {
    }

    public CfSettingListItem(Integer settingListItemId) {
        this.settingListItemId = settingListItemId;
    }

    public CfSettingListItem(Integer settingListItemId, String value) {
        this.settingListItemId = settingListItemId;
        this.value = value;
    }

    public Integer getSettingListItemId() {
        return settingListItemId;
    }

    public void setSettingListItemId(Integer settingListItemId) {
        this.settingListItemId = settingListItemId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfSetting getSettingIdFk() {
        return settingIdFk;
    }

    public void setSettingIdFk(CfSetting settingIdFk) {
        this.settingIdFk = settingIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (settingListItemId != null ? settingListItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfSettingListItem)) {
            return false;
        }
        CfSettingListItem other = (CfSettingListItem) object;
        if ((this.settingListItemId == null && other.settingListItemId != null) || (this.settingListItemId != null && !this.settingListItemId.equals(other.settingListItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfSettingListItem[ settingListItemId=" + settingListItemId + " ]";
    }
    
}
