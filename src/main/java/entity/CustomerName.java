/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CustomerName")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerName.findAll", query = "SELECT c FROM CustomerName c")
    , @NamedQuery(name = "CustomerName.findByCustomerNameId", query = "SELECT c FROM CustomerName c WHERE c.customerNameId = :customerNameId")
    , @NamedQuery(name = "CustomerName.findByFirstName", query = "SELECT c FROM CustomerName c WHERE c.firstName = :firstName")
    , @NamedQuery(name = "CustomerName.findByLastName", query = "SELECT c FROM CustomerName c WHERE c.lastName = :lastName")
    , @NamedQuery(name = "CustomerName.findByBusinessName", query = "SELECT c FROM CustomerName c WHERE c.businessName = :businessName")
    , @NamedQuery(name = "CustomerName.findByUpdateUserName", query = "SELECT c FROM CustomerName c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CustomerName.findByUpdateLast", query = "SELECT c FROM CustomerName c WHERE c.updateLast = :updateLast")})
public class CustomerName implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "customerNameId")
    private Integer customerNameId;
    @Size(max = 50)
    @Column(name = "firstName")
    private String firstName;
    @Size(max = 50)
    @Column(name = "lastName")
    private String lastName;
    @Size(max = 101)
    @Column(name = "BusinessName")
    private String businessName;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "customerIdFk")
    private Collection<Contracts> contractsCollection;
    @JoinColumn(name = "customerNameId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;

    public CustomerName() {
    }

    public CustomerName(Integer customerNameId) {
        this.customerNameId = customerNameId;
    }

    public Integer getCustomerNameId() {
        return customerNameId;
    }

    public void setCustomerNameId(Integer customerNameId) {
        this.customerNameId = customerNameId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerNameId != null ? customerNameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerName)) {
            return false;
        }
        CustomerName other = (CustomerName) object;
        if ((this.customerNameId == null && other.customerNameId != null) || (this.customerNameId != null && !this.customerNameId.equals(other.customerNameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CustomerName[ customerNameId=" + customerNameId + " ]";
    }
    
}
