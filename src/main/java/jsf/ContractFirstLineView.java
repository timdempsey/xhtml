/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Contracts;
import entity.ProductType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractFirstLineView")
@ApplicationScoped
public class ContractFirstLineView implements Serializable  {

    /**
     * Creates a new instance of contractFirstLineView
     */
    public ContractFirstLineView() {
        LOGGER.info("default constructor from ContractFirstLineView");

        selectedContract = new ContractFirstLine();
        cs = new ContractSearch();
        /*
        List<ProductType> ptList = claimMisc.getProductTypeByAll();
        contractTypes = new ArrayList<String>();
        for (ProductType pt : ptList) {
            contractTypes.add(pt.getProductTypeName());
        }
        */
    }
    


    //private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLineView.class.getName());
    private static final Logger LOGGER = LogManager.getLogger(ContractFirstLineView.class);

    @PersistenceContext // JSF m
    private EntityManager em;

    /*
    * bean property
     */
    String contractNo;
    String claimNumber;
    ContractFirstLine selectedContract;

    EMTestBean emTestBean;

    public EMTestBean getEmTestBean() {
        return emTestBean;
    }

    public void setEmTestBean(EMTestBean emTestBean) {
        this.emTestBean = emTestBean;
    }

    /*
    String sproduct;
    String sclaimnumber;
    String sassignedto;
    String sinception;
    String sstatus;
    String ssubstatus;
    String scontractnumber;
    String scontractstatus;
    String scustomer;
    String spaidto;
    String samount;
     */
    private ContractSearch cs;
    private List<String> contractTypes;

    public ContractSearch getCs() {
        return cs;
    }

    public void setCs(ContractSearch cs) {
        this.cs = cs;
    }

    public List<String> getContractTypes() {
        return contractTypes;
    }

    public void setContractTypes(List<String> contractTypes) {
        this.contractTypes = contractTypes;
    }


    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public ContractFirstLine getSelectedContract() {
        return selectedContract;
    }

    public void setSelectedContract(ContractFirstLine selectedContract) {
        this.selectedContract = selectedContract;
    }

    
    private LazyDataModel<ContractFirstLine> contracts;
    private List<String> contractNos;

    @Inject
    private ContractFirstLineService service;

    @Inject
    private ClaimMisc claimMisc;


    public LazyDataModel<ContractFirstLine> getContracts() {
        return contracts;
    }

    public void setContracts(LazyDataModel<ContractFirstLine> contracts) {
        this.contracts = contracts;
    }

    
    @PostConstruct
    public void init() {
        LOGGER.info("init from ContractFirstLineView");

        //selectedContract = new ContractFirstLine();
        //cs = new ContractSearch();
        List<ProductType> ptList = claimMisc.getProductTypeByAll();
        contractTypes = new ArrayList<String>();
        for (ProductType pt : ptList) {
            contractTypes.add(pt.getProductTypeName());
        }
    }
    

    public void findContracts() {
        try {
            LOGGER.info("in findContracts..., ");
            contractNos = new ArrayList<>();

            LOGGER.info("vin=" + cs.getVinFull());
            LOGGER.info("Agent=" + cs.getAgent());
            LOGGER.info("Number=" + cs.getContractNo());
            LOGGER.info("Start Date=" + cs.getEffectiveDate());
            LOGGER.info("End Date=" + cs.getContractExpirationDate());
            LOGGER.info("Name=" + cs.getCustomer());
            LOGGER.info("Phone=" + cs.getPhoneNumber());
            LOGGER.info("Name=" + cs.getDealerName());
            LOGGER.info("Phone=" + cs.getDealerNumber());

          
            contracts = new LazyDataModel<ContractFirstLine>() {
                @Override
                public ContractFirstLine getRowData(String rowKey) {
                    for (ContractFirstLine contractFirstLine : contracts) {
                        if (contractFirstLine.getContractNo().equals(rowKey)) {
                            return contractFirstLine;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(ContractFirstLine contractFirstLine) {
                    return contractFirstLine.getContractNo();
                }
                @Override
                public List<ContractFirstLine> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    List<Contracts> cl = claimMisc.searchContracts(cs, first, pageSize);
                    return (List<ContractFirstLine>) service.createContractView(cl);
                }

            };

            contracts.setRowCount(claimMisc.getSearchContractsCount(cs));
            
        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void onRowSelect(SelectEvent event) {
        selectedContract = (ContractFirstLine) event.getObject();

        FacesMessage msg = new FacesMessage("Contract Selected", ((ContractFirstLine) event.getObject()).getContractNo());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        /*
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        
        String date = format.format(event.getObject());
         */
        Date dt = (Date) event.getObject();
        LOGGER.info("onDateSelect, date=" + dt.toString());
        LOGGER.info("before cs date=" + cs.getContractExpirationDate());
        cs.setContractExpirationDate(dt);
        LOGGER.info("after cs date=" + cs.getContractExpirationDate());
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", dt.toString()));
    }
    
    public String getContractSearch()  {
        LOGGER.info("getContractSearch");
        reset();
        contracts = new LazyDataModel<ContractFirstLine>() {
                @Override
                public ContractFirstLine getRowData(String rowKey) {
                    return null;
                }

                @Override
                public Object getRowKey(ContractFirstLine contractFirstLine) {
                    return null;
                }

                @Override
                public List<ContractFirstLine> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    return null;
                }

            };

            contracts.setRowCount(0);
        
        return "ContractSearch";
    }
    
    public void reset() {
        cs.setVinFull(null);
        cs.setContractExpirationDate(null);
        cs.setContractNo(null);
        cs.setCustomer(null);
        cs.setDealerName(null);
        cs.setDealerNumber(null);
        cs.setEffectiveDate(null);
        cs.setPhoneNumber(null);
        cs.setProductTypeName(null);
        cs.setAgent(null);
        
    }

}

