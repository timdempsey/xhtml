/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class AcctTrans {

    private static final Logger LOGGER = LogManager.getLogger(AcctTrans.class);

    public AcctTrans(Integer transactionId, String transactionNumber, String transactionType, String trasactionStatus, BigDecimal transactionAmount, BigDecimal amountApplied, BigDecimal overUnder, String accountType, String accountName, String accountLegal, String transactionEntered, String transactionSent, String transactionReceived, String contractNo, String claimNo) {
        this.transactionId = transactionId;
        this.transactionNumber = transactionNumber;
        this.transactionType = transactionType;
        this.trasactionStatus = trasactionStatus;
        this.transactionAmount = transactionAmount;
        this.amountApplied = amountApplied;
        this.overUnder = overUnder;
        this.accountType = accountType;
        this.accountName = accountName;
        this.accountLegal = accountLegal;
        this.transactionEntered = transactionEntered;
        this.transactionSent = transactionSent;
        this.transactionReceived = transactionReceived;
        this.contractNo = contractNo;
        this.claimNo = claimNo;
    }

    public AcctTrans(Integer transactionId, String transactionNumber, String transactionType, String trasactionStatus, BigDecimal transactionAmount, BigDecimal amountApplied, BigDecimal overUnder, String accountType, String accountName, String accountLegal, String transactionEntered, String transactionSent, String transactionReceived, String contractNo, String claimNo, String insurer, String postedDate, String adminCompany) {
        this.transactionId = transactionId;
        this.transactionNumber = transactionNumber;
        this.transactionType = transactionType;
        this.trasactionStatus = trasactionStatus;
        this.transactionAmount = transactionAmount;
        this.amountApplied = amountApplied;
        this.overUnder = overUnder;
        this.accountType = accountType;
        this.accountName = accountName;
        this.accountLegal = accountLegal;
        this.transactionEntered = transactionEntered;
        this.transactionSent = transactionSent;
        this.transactionReceived = transactionReceived;
        this.contractNo = contractNo;
        this.claimNo = claimNo;
        this.insurer = insurer;
        this.postedDate = postedDate;
        this.adminCompany = adminCompany;
    }
    
    

    public AcctTrans() {
    }

    private Integer transactionId;
    private String transactionNumber;
    private String transactionType;
    private String trasactionStatus;
    private BigDecimal transactionAmount;
    private BigDecimal amountApplied;
    private BigDecimal overUnder;
    private String accountType;
    private String accountName;
    private String accountLegal;
    private String transactionEntered;
    private String transactionSent;
    private String transactionReceived;
    private String contractNo;
    private String claimNo;
    private String insurer;
    private String postedDate;
    private String adminCompany;

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTrasactionStatus() {
        return trasactionStatus;
    }

    public void setTrasactionStatus(String trasactionStatus) {
        this.trasactionStatus = trasactionStatus;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getAmountApplied() {
        return amountApplied;
    }

    public void setAmountApplied(BigDecimal amountApplied) {
        this.amountApplied = amountApplied;
    }

    public BigDecimal getOverUnder() {
        return overUnder;
    }

    public void setOverUnder(BigDecimal overUnder) {
        this.overUnder = overUnder;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountLegal() {
        return accountLegal;
    }

    public void setAccountLegal(String accountLegal) {
        this.accountLegal = accountLegal;
    }

    public String getTransactionEntered() {
        return transactionEntered;
    }

    public void setTransactionEntered(String transactionEntered) {
        this.transactionEntered = transactionEntered;
    }

    public String getTransactionSent() {
        return transactionSent;
    }

    public void setTransactionSent(String transactionSent) {
        this.transactionSent = transactionSent;
    }

    public String getTransactionReceived() {
        return transactionReceived;
    }

    public void setTransactionReceived(String transactionReceived) {
        this.transactionReceived = transactionReceived;
    }

    public void reset() {
        transactionId = null;
        transactionNumber = "";
        transactionType = "";
        trasactionStatus = "";
        transactionAmount = null;
        amountApplied = null;
        overUnder = null;
        accountType = "";
        accountName = "";
        accountLegal = "";
        transactionEntered = "";
        transactionSent = "";
        transactionReceived = "";
        contractNo = "";
        claimNo = "";
        insurer = "";
        postedDate = "";
        adminCompany = "";
    }

}
