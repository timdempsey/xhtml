/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.ClaimDetail;
import entity.Contracts;
import entity.RepairFacility;
import entity.RepairFacilityContactPerson;
import entity.UserMember;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.log4j.Logger;

/**
 *
 * @author Jiepi
 */
/*
@Entity
@Table(name = "Claim")
@SecondaryTables({
    @SecondaryTable(name = "Contracts")
    ,@SecondaryTable(name = "CustomerName")
    , @SecondaryTable(name = "AccountKeeper")
    ,  @SecondaryTable(name = "Region")
    , @SecondaryTable(name = "RepairFacility")})
*/
public class ClaimContracts implements Serializable {

    @Size(max = 30)
    @Column(name = "claimNumber")
    private String claimNumber;
    @Column(name = "inceptionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inceptionDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @Size(max = 1024)
    @Column(name = "reason")
    private String reason;
    @Column(name = "currentOdometer")
    private Integer currentOdometer;
    @Size(max = 10)
    @Column(name = "claimStartOdometer")
    private String claimStartOdometer;
    @Size(max = 10)
    @Column(name = "claimExpirationOdometer")
    private String claimExpirationOdometer;
    @Size(max = 30)
    @Column(name = "assignedTo")
    private String assignedTo;
    @Size(max = 50)
    @Column(name = "repairOrderNumber")
    private String repairOrderNumber;
    @Column(name = "repairOrderDate")
    @Temporal(TemporalType.DATE)
    private Date repairOrderDate;
    @Size(max = 20)
    @Column(name = "tpaApprovalCode")
    private String tpaApprovalCode;
    @Column(name = "manualDeductible")
    private Boolean manualDeductible;
    @Column(name = "deductibleAmt")
    private BigDecimal deductibleAmt;
    @Column(name = "fullDeductibleAmt")
    private BigDecimal fullDeductibleAmt;
    @Column(name = "laborTaxRate")
    private BigDecimal laborTaxRate;
    @Column(name = "partTaxRate")
    private BigDecimal partTaxRate;
    @Column(name = "laborRate")
    private BigDecimal laborRate;
    @Column(name = "partWarrantyMonths")
    private Integer partWarrantyMonths;
    @Column(name = "partWarrantyMiles")
    private Integer partWarrantyMiles;
    @Size(max = 20)
    @Column(name = "claimStatus")
    private String claimStatus;
    @Size(max = 20)
    @Column(name = "claimSubStatus")
    private String claimSubStatus;
    @Column(name = "productTypeIdFk")
    private Integer productTypeIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimDetail> claimDetailCollection;
    @JoinColumn(name = "paidToFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper paidToFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "repairFacilityIdFk", referencedColumnName = "repairFacilityId")
    @ManyToOne
    private RepairFacility repairFacilityIdFk;
    @JoinColumn(name = "repairFacilityContactIdFk", referencedColumnName = "repairFacilityContactPersonId")
    @ManyToOne
    private RepairFacilityContactPerson repairFacilityContactIdFk;
    @JoinColumn(name = "enteredIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredIdFk;
    @JoinColumn(name = "ownedByFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember ownedByFk;

    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public ClaimContracts() {
    }

    @Id
    @GeneratedValue
    @Column(name = "claimId", table = "Claim")
    Integer claimId;    
    @Column(name="contractNo", table="Contracts")
    String contractNo; 
    @Column(name="contractStatus", table="Contracts")
    String contractStatus;
    @Column(name="firstName", table="CustomerName")
    String firstName;
    @Column(name="lastName", table="CustomerName")
    String lastName;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Date getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(Date inceptionDate) {
        this.inceptionDate = inceptionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getCurrentOdometer() {
        return currentOdometer;
    }

    public void setCurrentOdometer(Integer currentOdometer) {
        this.currentOdometer = currentOdometer;
    }

    public String getClaimStartOdometer() {
        return claimStartOdometer;
    }

    public void setClaimStartOdometer(String claimStartOdometer) {
        this.claimStartOdometer = claimStartOdometer;
    }

    public String getClaimExpirationOdometer() {
        return claimExpirationOdometer;
    }

    public void setClaimExpirationOdometer(String claimExpirationOdometer) {
        this.claimExpirationOdometer = claimExpirationOdometer;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getRepairOrderNumber() {
        return repairOrderNumber;
    }

    public void setRepairOrderNumber(String repairOrderNumber) {
        this.repairOrderNumber = repairOrderNumber;
    }

    public Date getRepairOrderDate() {
        return repairOrderDate;
    }

    public void setRepairOrderDate(Date repairOrderDate) {
        this.repairOrderDate = repairOrderDate;
    }

    public String getTpaApprovalCode() {
        return tpaApprovalCode;
    }

    public void setTpaApprovalCode(String tpaApprovalCode) {
        this.tpaApprovalCode = tpaApprovalCode;
    }

    public Boolean getManualDeductible() {
        return manualDeductible;
    }

    public void setManualDeductible(Boolean manualDeductible) {
        this.manualDeductible = manualDeductible;
    }

    public BigDecimal getDeductibleAmt() {
        return deductibleAmt;
    }

    public void setDeductibleAmt(BigDecimal deductibleAmt) {
        this.deductibleAmt = deductibleAmt;
    }

    public BigDecimal getFullDeductibleAmt() {
        return fullDeductibleAmt;
    }

    public void setFullDeductibleAmt(BigDecimal fullDeductibleAmt) {
        this.fullDeductibleAmt = fullDeductibleAmt;
    }

    public BigDecimal getLaborTaxRate() {
        return laborTaxRate;
    }

    public void setLaborTaxRate(BigDecimal laborTaxRate) {
        this.laborTaxRate = laborTaxRate;
    }

    public BigDecimal getPartTaxRate() {
        return partTaxRate;
    }

    public void setPartTaxRate(BigDecimal partTaxRate) {
        this.partTaxRate = partTaxRate;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public String getClaimSubStatus() {
        return claimSubStatus;
    }

    public void setClaimSubStatus(String claimSubStatus) {
        this.claimSubStatus = claimSubStatus;
    }

    public Integer getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(Integer productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimDetail> getClaimDetailCollection() {
        return claimDetailCollection;
    }

    public void setClaimDetailCollection(Collection<ClaimDetail> claimDetailCollection) {
        this.claimDetailCollection = claimDetailCollection;
    }

    public AccountKeeper getPaidToFk() {
        return paidToFk;
    }

    public void setPaidToFk(AccountKeeper paidToFk) {
        this.paidToFk = paidToFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public RepairFacility getRepairFacilityIdFk() {
        return repairFacilityIdFk;
    }

    public void setRepairFacilityIdFk(RepairFacility repairFacilityIdFk) {
        this.repairFacilityIdFk = repairFacilityIdFk;
    }

    public RepairFacilityContactPerson getRepairFacilityContactIdFk() {
        return repairFacilityContactIdFk;
    }

    public void setRepairFacilityContactIdFk(RepairFacilityContactPerson repairFacilityContactIdFk) {
        this.repairFacilityContactIdFk = repairFacilityContactIdFk;
    }

    public UserMember getEnteredIdFk() {
        return enteredIdFk;
    }

    public void setEnteredIdFk(UserMember enteredIdFk) {
        this.enteredIdFk = enteredIdFk;
    }

    public UserMember getOwnedByFk() {
        return ownedByFk;
    }

    public void setOwnedByFk(UserMember ownedByFk) {
        this.ownedByFk = ownedByFk;
    }
  
    
    
}
