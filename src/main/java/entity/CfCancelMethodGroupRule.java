/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCancelMethodGroupRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCancelMethodGroupRule.findAll", query = "SELECT c FROM CfCancelMethodGroupRule c")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByCancelMethodGroupRuleId", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.cancelMethodGroupRuleId = :cancelMethodGroupRuleId")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByDayToFinishMonth", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.dayToFinishMonth = :dayToFinishMonth")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByCancelFeeAppliesTypeInd", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.cancelFeeAppliesTypeInd = :cancelFeeAppliesTypeInd")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByReduceReserveByClaimWithholdings", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.reduceReserveByClaimWithholdings = :reduceReserveByClaimWithholdings")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByUpdateUserName", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCancelMethodGroupRule.findByUpdateLast", query = "SELECT c FROM CfCancelMethodGroupRule c WHERE c.updateLast = :updateLast")})
public class CfCancelMethodGroupRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodGroupRuleId")
    private Integer cancelMethodGroupRuleId;
    @Column(name = "dayToFinishMonth")
    private Integer dayToFinishMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelFeeAppliesTypeInd")
    private int cancelFeeAppliesTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reduceReserveByClaimWithholdings")
    private boolean reduceReserveByClaimWithholdings;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "cancelMethodGroupIdFk", referencedColumnName = "cancelMethodGroupId")
    @ManyToOne(optional = false)
    private CfCancelMethodGroup cancelMethodGroupIdFk;
    @JoinColumn(name = "cancelMethodRuleTypeInd", referencedColumnName = "cancelMethodTypeId")
    @ManyToOne(optional = false)
    private DtCancelMethodType cancelMethodRuleTypeInd;
    @JoinColumn(name = "cancelTypeInd", referencedColumnName = "cancelTypeId")
    @ManyToOne(optional = false)
    private DtCancelType cancelTypeInd;
    @JoinColumn(name = "claimWithholdMethodInd", referencedColumnName = "claimWithholdMethodId")
    @ManyToOne(optional = false)
    private DtClaimWithholdMethod claimWithholdMethodInd;
    @OneToMany(mappedBy = "cancelMethodGroupRuleIdFk")
    private Collection<CancelRequest> cancelRequestCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelMethodGroupRuleIdFk")
    private Collection<CfCancelMethodGroupRuleFee> cfCancelMethodGroupRuleFeeCollection;

    public CfCancelMethodGroupRule() {
    }

    public CfCancelMethodGroupRule(Integer cancelMethodGroupRuleId) {
        this.cancelMethodGroupRuleId = cancelMethodGroupRuleId;
    }

    public CfCancelMethodGroupRule(Integer cancelMethodGroupRuleId, int cancelFeeAppliesTypeInd, boolean reduceReserveByClaimWithholdings) {
        this.cancelMethodGroupRuleId = cancelMethodGroupRuleId;
        this.cancelFeeAppliesTypeInd = cancelFeeAppliesTypeInd;
        this.reduceReserveByClaimWithholdings = reduceReserveByClaimWithholdings;
    }

    public Integer getCancelMethodGroupRuleId() {
        return cancelMethodGroupRuleId;
    }

    public void setCancelMethodGroupRuleId(Integer cancelMethodGroupRuleId) {
        this.cancelMethodGroupRuleId = cancelMethodGroupRuleId;
    }

    public Integer getDayToFinishMonth() {
        return dayToFinishMonth;
    }

    public void setDayToFinishMonth(Integer dayToFinishMonth) {
        this.dayToFinishMonth = dayToFinishMonth;
    }

    public int getCancelFeeAppliesTypeInd() {
        return cancelFeeAppliesTypeInd;
    }

    public void setCancelFeeAppliesTypeInd(int cancelFeeAppliesTypeInd) {
        this.cancelFeeAppliesTypeInd = cancelFeeAppliesTypeInd;
    }

    public boolean getReduceReserveByClaimWithholdings() {
        return reduceReserveByClaimWithholdings;
    }

    public void setReduceReserveByClaimWithholdings(boolean reduceReserveByClaimWithholdings) {
        this.reduceReserveByClaimWithholdings = reduceReserveByClaimWithholdings;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCancelMethodGroup getCancelMethodGroupIdFk() {
        return cancelMethodGroupIdFk;
    }

    public void setCancelMethodGroupIdFk(CfCancelMethodGroup cancelMethodGroupIdFk) {
        this.cancelMethodGroupIdFk = cancelMethodGroupIdFk;
    }

    public DtCancelMethodType getCancelMethodRuleTypeInd() {
        return cancelMethodRuleTypeInd;
    }

    public void setCancelMethodRuleTypeInd(DtCancelMethodType cancelMethodRuleTypeInd) {
        this.cancelMethodRuleTypeInd = cancelMethodRuleTypeInd;
    }

    public DtCancelType getCancelTypeInd() {
        return cancelTypeInd;
    }

    public void setCancelTypeInd(DtCancelType cancelTypeInd) {
        this.cancelTypeInd = cancelTypeInd;
    }

    public DtClaimWithholdMethod getClaimWithholdMethodInd() {
        return claimWithholdMethodInd;
    }

    public void setClaimWithholdMethodInd(DtClaimWithholdMethod claimWithholdMethodInd) {
        this.claimWithholdMethodInd = claimWithholdMethodInd;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroupRuleFee> getCfCancelMethodGroupRuleFeeCollection() {
        return cfCancelMethodGroupRuleFeeCollection;
    }

    public void setCfCancelMethodGroupRuleFeeCollection(Collection<CfCancelMethodGroupRuleFee> cfCancelMethodGroupRuleFeeCollection) {
        this.cfCancelMethodGroupRuleFeeCollection = cfCancelMethodGroupRuleFeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelMethodGroupRuleId != null ? cancelMethodGroupRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCancelMethodGroupRule)) {
            return false;
        }
        CfCancelMethodGroupRule other = (CfCancelMethodGroupRule) object;
        if ((this.cancelMethodGroupRuleId == null && other.cancelMethodGroupRuleId != null) || (this.cancelMethodGroupRuleId != null && !this.cancelMethodGroupRuleId.equals(other.cancelMethodGroupRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCancelMethodGroupRule[ cancelMethodGroupRuleId=" + cancelMethodGroupRuleId + " ]";
    }
    
}
