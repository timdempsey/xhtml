/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Address;
import entity.CfRegion;

/**
 *
 * @author Jiepi
 */
public class CustomerInfo {

    public CustomerInfo() {
        customer = new AccountKeeper();
        paddr = new Address();
        baddr = new Address();
        regionP = new CfRegion();
        regionB = new CfRegion();
    }

    public CustomerInfo(AccountKeeper customer, Address paddr, Address baddr, String firstName, String lastName, CfRegion regionP, CfRegion regionB, Boolean sameAsPaddr, String businessName, String fullName) {
        this.customer = customer;
        this.paddr = paddr;
        this.baddr = baddr;
        this.firstName = firstName;
        this.lastName = lastName;
        this.regionP = regionP;
        this.regionB = regionB;
        this.sameAsPaddr = sameAsPaddr;
        this.businessName = businessName;
        this.fullName = fullName;
    }
    
    
    
    private AccountKeeper customer;
    private Address paddr;
    private Address baddr;
    private String firstName;
    private String lastName;
    private CfRegion regionP;
    private CfRegion regionB;
    private Boolean sameAsPaddr;
    private String businessName;
    private String fullName;

    public AccountKeeper getCustomer() {
        return customer;
    }

    public void setCustomer(AccountKeeper customer) {
        this.customer = customer;
    }

    public Address getPaddr() {
        return paddr;
    }

    public void setPaddr(Address paddr) {
        this.paddr = paddr;
    }

    public Address getBaddr() {
        return baddr;
    }

    public void setBaddr(Address baddr) {
        this.baddr = baddr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CfRegion getRegionP() {
        return regionP;
    }

    public void setRegionP(CfRegion regionP) {
        this.regionP = regionP;
    }

    public CfRegion getRegionB() {
        return regionB;
    }

    public void setRegionB(CfRegion regionB) {
        this.regionB = regionB;
    }

    public Boolean getSameAsPaddr() {
        return sameAsPaddr;
    }

    public void setSameAsPaddr(Boolean sameAsPaddr) {
        this.sameAsPaddr = sameAsPaddr;
    }

    

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    
    
    public void reset() {
        customer = new AccountKeeper();
        paddr = new Address();
        baddr = new Address();
        sameAsPaddr = true;
        regionB = new CfRegion();
        regionP = new CfRegion();
        businessName = "";
        fullName = "";
    }
    
}
