/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMPlanService")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMPlanService.findAll", query = "SELECT p FROM PPMPlanService p")
    , @NamedQuery(name = "PPMPlanService.findByPPMPlanServiceId", query = "SELECT p FROM PPMPlanService p WHERE p.pPMPlanServiceId = :pPMPlanServiceId")
    , @NamedQuery(name = "PPMPlanService.findByMaxCost", query = "SELECT p FROM PPMPlanService p WHERE p.maxCost = :maxCost")
    , @NamedQuery(name = "PPMPlanService.findByUpdateUserName", query = "SELECT p FROM PPMPlanService p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMPlanService.findByUpdateLast", query = "SELECT p FROM PPMPlanService p WHERE p.updateLast = :updateLast")})
public class PPMPlanService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMPlanServiceId")
    private Integer pPMPlanServiceId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxCost")
    private BigDecimal maxCost;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pPMPlanServiceIdFk")
    private Collection<PPMTermService> pPMTermServiceCollection;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne(optional = false)
    private PlanTable planIdFk;
    @JoinColumn(name = "PPMProductServiceIdFk", referencedColumnName = "PPMProductServiceId")
    @ManyToOne(optional = false)
    private PPMProductService pPMProductServiceIdFk;

    public PPMPlanService() {
    }

    public PPMPlanService(Integer pPMPlanServiceId) {
        this.pPMPlanServiceId = pPMPlanServiceId;
    }

    public PPMPlanService(Integer pPMPlanServiceId, BigDecimal maxCost) {
        this.pPMPlanServiceId = pPMPlanServiceId;
        this.maxCost = maxCost;
    }

    public Integer getPPMPlanServiceId() {
        return pPMPlanServiceId;
    }

    public void setPPMPlanServiceId(Integer pPMPlanServiceId) {
        this.pPMPlanServiceId = pPMPlanServiceId;
    }

    public BigDecimal getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(BigDecimal maxCost) {
        this.maxCost = maxCost;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PPMTermService> getPPMTermServiceCollection() {
        return pPMTermServiceCollection;
    }

    public void setPPMTermServiceCollection(Collection<PPMTermService> pPMTermServiceCollection) {
        this.pPMTermServiceCollection = pPMTermServiceCollection;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public PPMProductService getPPMProductServiceIdFk() {
        return pPMProductServiceIdFk;
    }

    public void setPPMProductServiceIdFk(PPMProductService pPMProductServiceIdFk) {
        this.pPMProductServiceIdFk = pPMProductServiceIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMPlanServiceId != null ? pPMPlanServiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMPlanService)) {
            return false;
        }
        PPMPlanService other = (PPMPlanService) object;
        if ((this.pPMPlanServiceId == null && other.pPMPlanServiceId != null) || (this.pPMPlanServiceId != null && !this.pPMPlanServiceId.equals(other.pPMPlanServiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMPlanService[ pPMPlanServiceId=" + pPMPlanServiceId + " ]";
    }
    
}
