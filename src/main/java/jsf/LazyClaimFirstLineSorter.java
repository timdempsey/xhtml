/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.lang.reflect.Field;
import java.util.Comparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
public class LazyClaimFirstLineSorter implements Comparator<ClaimFirstLine>{
    
    private static final Logger LOGGER = LogManager.getLogger(LazyClaimFirstLineSorter.class);
    private String sortField;
     
    private SortOrder sortOrder;
     
    public LazyClaimFirstLineSorter(String sortField, SortOrder sortOrder) {
        LOGGER.info("in Laze, sortField=" + sortField);
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    @Override
    public int compare(ClaimFirstLine cfl1, ClaimFirstLine cfl2) {
        try {
            
            LOGGER.info("cfl1=" + cfl1.getClaimNumber());
            LOGGER.info("sortField=" + this.sortField);
            /*
            Object value1 = cfl1.getClaimNumber();
            Object value2 = cfl2.getClaimNumber();
            */
            
                    Field field1 = cfl1.getClass().getDeclaredField(this.sortField);
                    field1.setAccessible(true);
                    Object value1 = field1.get(cfl1);
                    
                    Field field2 = cfl2.getClass().getDeclaredField(this.sortField);
                    field2.setAccessible(true);
                    Object value2 = field2.get(cfl2);
                    
            //Object value1 = ClaimFirstLine.class.getDeclaredField(this.sortField).get(cfl1);
            //Object value2 = ClaimFirstLine.class.getDeclaredField(this.sortField).get(cfl2);
            //Object value2 = cfl2.getClass().getDeclaredField(this.sortField).get(cfl2);
            
            
            LOGGER.info("value1=" + value1);
            LOGGER.info("value2=" + value2);
            
            int value = ((Comparable)value1).compareTo(value2);
            
            //LOGGER.info("value=" + value);
             return value;
            //return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    
    
}
