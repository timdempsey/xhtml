/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfRoleGroupConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfRoleGroupConn.findAll", query = "SELECT c FROM CfRoleGroupConn c")
    , @NamedQuery(name = "CfRoleGroupConn.findByRoleGroupConnId", query = "SELECT c FROM CfRoleGroupConn c WHERE c.roleGroupConnId = :roleGroupConnId")
    , @NamedQuery(name = "CfRoleGroupConn.findByUpdateUserName", query = "SELECT c FROM CfRoleGroupConn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfRoleGroupConn.findByUpdateLast", query = "SELECT c FROM CfRoleGroupConn c WHERE c.updateLast = :updateLast")})
public class CfRoleGroupConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "roleGroupConnId")
    private Integer roleGroupConnId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "membershipGroupId", referencedColumnName = "membershipGroupId")
    @ManyToOne(optional = false)
    private CfMembershipGroup membershipGroupId;
    @JoinColumn(name = "membershipRoleId", referencedColumnName = "membershipRoleId")
    @ManyToOne(optional = false)
    private CfMembershipRole membershipRoleId;

    public CfRoleGroupConn() {
    }

    public CfRoleGroupConn(Integer roleGroupConnId) {
        this.roleGroupConnId = roleGroupConnId;
    }

    public Integer getRoleGroupConnId() {
        return roleGroupConnId;
    }

    public void setRoleGroupConnId(Integer roleGroupConnId) {
        this.roleGroupConnId = roleGroupConnId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfMembershipGroup getMembershipGroupId() {
        return membershipGroupId;
    }

    public void setMembershipGroupId(CfMembershipGroup membershipGroupId) {
        this.membershipGroupId = membershipGroupId;
    }

    public CfMembershipRole getMembershipRoleId() {
        return membershipRoleId;
    }

    public void setMembershipRoleId(CfMembershipRole membershipRoleId) {
        this.membershipRoleId = membershipRoleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleGroupConnId != null ? roleGroupConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfRoleGroupConn)) {
            return false;
        }
        CfRoleGroupConn other = (CfRoleGroupConn) object;
        if ((this.roleGroupConnId == null && other.roleGroupConnId != null) || (this.roleGroupConnId != null && !this.roleGroupConnId.equals(other.roleGroupConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfRoleGroupConn[ roleGroupConnId=" + roleGroupConnId + " ]";
    }
    
}
