/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMSchedule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMSchedule.findAll", query = "SELECT p FROM PPMSchedule p")
    , @NamedQuery(name = "PPMSchedule.findByPPMScheduleId", query = "SELECT p FROM PPMSchedule p WHERE p.pPMScheduleId = :pPMScheduleId")
    , @NamedQuery(name = "PPMSchedule.findByName", query = "SELECT p FROM PPMSchedule p WHERE p.name = :name")
    , @NamedQuery(name = "PPMSchedule.findByCode", query = "SELECT p FROM PPMSchedule p WHERE p.code = :code")
    , @NamedQuery(name = "PPMSchedule.findByUpdateUserName", query = "SELECT p FROM PPMSchedule p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMSchedule.findByUpdateLast", query = "SELECT p FROM PPMSchedule p WHERE p.updateLast = :updateLast")})
public class PPMSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMScheduleId")
    private Integer pPMScheduleId;
    @Size(max = 255)
    @Column(name = "Name")
    private String name;
    @Size(max = 60)
    @Column(name = "Code")
    private String code;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "ppmScheduleIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "pPMScheduleIdFk")
    private Collection<PPMEvent> pPMEventCollection;

    public PPMSchedule() {
    }

    public PPMSchedule(Integer pPMScheduleId) {
        this.pPMScheduleId = pPMScheduleId;
    }

    public Integer getPPMScheduleId() {
        return pPMScheduleId;
    }

    public void setPPMScheduleId(Integer pPMScheduleId) {
        this.pPMScheduleId = pPMScheduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<PPMEvent> getPPMEventCollection() {
        return pPMEventCollection;
    }

    public void setPPMEventCollection(Collection<PPMEvent> pPMEventCollection) {
        this.pPMEventCollection = pPMEventCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMScheduleId != null ? pPMScheduleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMSchedule)) {
            return false;
        }
        PPMSchedule other = (PPMSchedule) object;
        if ((this.pPMScheduleId == null && other.pPMScheduleId != null) || (this.pPMScheduleId != null && !this.pPMScheduleId.equals(other.pPMScheduleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMSchedule[ pPMScheduleId=" + pPMScheduleId + " ]";
    }
    
}
