/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtClaimWithholdMethod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtClaimWithholdMethod.findAll", query = "SELECT d FROM DtClaimWithholdMethod d")
    , @NamedQuery(name = "DtClaimWithholdMethod.findByClaimWithholdMethodId", query = "SELECT d FROM DtClaimWithholdMethod d WHERE d.claimWithholdMethodId = :claimWithholdMethodId")
    , @NamedQuery(name = "DtClaimWithholdMethod.findByDescription", query = "SELECT d FROM DtClaimWithholdMethod d WHERE d.description = :description")})
public class DtClaimWithholdMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimWithholdMethodId")
    private Integer claimWithholdMethodId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claimWithholdMethodInd")
    private Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claimWithholdMethodInd")
    private Collection<CancelRequest> cancelRequestCollection;

    public DtClaimWithholdMethod() {
    }

    public DtClaimWithholdMethod(Integer claimWithholdMethodId) {
        this.claimWithholdMethodId = claimWithholdMethodId;
    }

    public Integer getClaimWithholdMethodId() {
        return claimWithholdMethodId;
    }

    public void setClaimWithholdMethodId(Integer claimWithholdMethodId) {
        this.claimWithholdMethodId = claimWithholdMethodId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroupRule> getCfCancelMethodGroupRuleCollection() {
        return cfCancelMethodGroupRuleCollection;
    }

    public void setCfCancelMethodGroupRuleCollection(Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection) {
        this.cfCancelMethodGroupRuleCollection = cfCancelMethodGroupRuleCollection;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimWithholdMethodId != null ? claimWithholdMethodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtClaimWithholdMethod)) {
            return false;
        }
        DtClaimWithholdMethod other = (DtClaimWithholdMethod) object;
        if ((this.claimWithholdMethodId == null && other.claimWithholdMethodId != null) || (this.claimWithholdMethodId != null && !this.claimWithholdMethodId.equals(other.claimWithholdMethodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtClaimWithholdMethod[ claimWithholdMethodId=" + claimWithholdMethodId + " ]";
    }
    
}
