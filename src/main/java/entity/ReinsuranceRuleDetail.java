/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceRuleDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceRuleDetail.findAll", query = "SELECT r FROM ReinsuranceRuleDetail r")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByReinsuranceRuleDetailId", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.reinsuranceRuleDetailId = :reinsuranceRuleDetailId")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByLevel", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.level = :level")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByReinsuranceTypeInd", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.reinsuranceTypeInd = :reinsuranceTypeInd")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByReinsuranceGroupCode", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.reinsuranceGroupCode = :reinsuranceGroupCode")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByEffectiveDate", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByEnteredDate", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.enteredDate = :enteredDate")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByDeletedaInd", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.deletedaInd = :deletedaInd")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByUpdateUserName", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceRuleDetail.findByUpdateLast", query = "SELECT r FROM ReinsuranceRuleDetail r WHERE r.updateLast = :updateLast")})
public class ReinsuranceRuleDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceRuleDetailId")
    private Integer reinsuranceRuleDetailId;
    @Column(name = "level")
    private Integer level;
    @Column(name = "reinsuranceTypeInd")
    private Integer reinsuranceTypeInd;
    @Size(max = 50)
    @Column(name = "reinsuranceGroupCode")
    private String reinsuranceGroupCode;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedaInd")
    private Boolean deletedaInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "reinsuranceRuleDetailIdFk")
    private Collection<ReinsuranceSplit> reinsuranceSplitCollection;
    @JoinColumn(name = "rateBasedRuleGroupIdFk", referencedColumnName = "rateBasedRuleGroupId")
    @ManyToOne
    private RateBasedRuleGroup rateBasedRuleGroupIdFk;
    @JoinColumn(name = "reinsuranceRuleIdFk", referencedColumnName = "reinsuranceRuleId")
    @ManyToOne
    private ReinsuranceRule reinsuranceRuleIdFk;
    @JoinColumn(name = "reinsuranceVersionCodeIdFk", referencedColumnName = "reinsuranceVersionCodeId")
    @ManyToOne
    private ReinsuranceVersionCode reinsuranceVersionCodeIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public ReinsuranceRuleDetail() {
    }

    public ReinsuranceRuleDetail(Integer reinsuranceRuleDetailId) {
        this.reinsuranceRuleDetailId = reinsuranceRuleDetailId;
    }

    public Integer getReinsuranceRuleDetailId() {
        return reinsuranceRuleDetailId;
    }

    public void setReinsuranceRuleDetailId(Integer reinsuranceRuleDetailId) {
        this.reinsuranceRuleDetailId = reinsuranceRuleDetailId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getReinsuranceTypeInd() {
        return reinsuranceTypeInd;
    }

    public void setReinsuranceTypeInd(Integer reinsuranceTypeInd) {
        this.reinsuranceTypeInd = reinsuranceTypeInd;
    }

    public String getReinsuranceGroupCode() {
        return reinsuranceGroupCode;
    }

    public void setReinsuranceGroupCode(String reinsuranceGroupCode) {
        this.reinsuranceGroupCode = reinsuranceGroupCode;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedaInd() {
        return deletedaInd;
    }

    public void setDeletedaInd(Boolean deletedaInd) {
        this.deletedaInd = deletedaInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ReinsuranceSplit> getReinsuranceSplitCollection() {
        return reinsuranceSplitCollection;
    }

    public void setReinsuranceSplitCollection(Collection<ReinsuranceSplit> reinsuranceSplitCollection) {
        this.reinsuranceSplitCollection = reinsuranceSplitCollection;
    }

    public RateBasedRuleGroup getRateBasedRuleGroupIdFk() {
        return rateBasedRuleGroupIdFk;
    }

    public void setRateBasedRuleGroupIdFk(RateBasedRuleGroup rateBasedRuleGroupIdFk) {
        this.rateBasedRuleGroupIdFk = rateBasedRuleGroupIdFk;
    }

    public ReinsuranceRule getReinsuranceRuleIdFk() {
        return reinsuranceRuleIdFk;
    }

    public void setReinsuranceRuleIdFk(ReinsuranceRule reinsuranceRuleIdFk) {
        this.reinsuranceRuleIdFk = reinsuranceRuleIdFk;
    }

    public ReinsuranceVersionCode getReinsuranceVersionCodeIdFk() {
        return reinsuranceVersionCodeIdFk;
    }

    public void setReinsuranceVersionCodeIdFk(ReinsuranceVersionCode reinsuranceVersionCodeIdFk) {
        this.reinsuranceVersionCodeIdFk = reinsuranceVersionCodeIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceRuleDetailId != null ? reinsuranceRuleDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceRuleDetail)) {
            return false;
        }
        ReinsuranceRuleDetail other = (ReinsuranceRuleDetail) object;
        if ((this.reinsuranceRuleDetailId == null && other.reinsuranceRuleDetailId != null) || (this.reinsuranceRuleDetailId != null && !this.reinsuranceRuleDetailId.equals(other.reinsuranceRuleDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceRuleDetail[ reinsuranceRuleDetailId=" + reinsuranceRuleDetailId + " ]";
    }
    
}
