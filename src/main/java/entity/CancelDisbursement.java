/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CancelDisbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CancelDisbursement.findAll", query = "SELECT c FROM CancelDisbursement c")
    , @NamedQuery(name = "CancelDisbursement.findByCancelDisbursementId", query = "SELECT c FROM CancelDisbursement c WHERE c.cancelDisbursementId = :cancelDisbursementId")
    , @NamedQuery(name = "CancelDisbursement.findByCancelDisbursementTypeInd", query = "SELECT c FROM CancelDisbursement c WHERE c.cancelDisbursementTypeInd = :cancelDisbursementTypeInd")
    , @NamedQuery(name = "CancelDisbursement.findByUpdateUserName", query = "SELECT c FROM CancelDisbursement c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CancelDisbursement.findByUpdateLast", query = "SELECT c FROM CancelDisbursement c WHERE c.updateLast = :updateLast")})
public class CancelDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelDisbursementId")
    private Integer cancelDisbursementId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CancelDisbursementTypeInd")
    private int cancelDisbursementTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "cancelDisbursementIdFk")
    private Collection<ReinstatementDisbursement> reinstatementDisbursementCollection;
    @JoinColumn(name = "ContractcancelIdFk", referencedColumnName = "contractCancelId")
    @ManyToOne
    private ContractCancel contractcancelIdFk;
    @JoinColumn(name = "contractDisbursementIdFk", referencedColumnName = "contractDisbursementId")
    @ManyToOne
    private ContractDisbursement contractDisbursementIdFk;
    @JoinColumn(name = "cancelDisbursementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public CancelDisbursement() {
    }

    public CancelDisbursement(Integer cancelDisbursementId) {
        this.cancelDisbursementId = cancelDisbursementId;
    }

    public CancelDisbursement(Integer cancelDisbursementId, int cancelDisbursementTypeInd) {
        this.cancelDisbursementId = cancelDisbursementId;
        this.cancelDisbursementTypeInd = cancelDisbursementTypeInd;
    }

    public Integer getCancelDisbursementId() {
        return cancelDisbursementId;
    }

    public void setCancelDisbursementId(Integer cancelDisbursementId) {
        this.cancelDisbursementId = cancelDisbursementId;
    }

    public int getCancelDisbursementTypeInd() {
        return cancelDisbursementTypeInd;
    }

    public void setCancelDisbursementTypeInd(int cancelDisbursementTypeInd) {
        this.cancelDisbursementTypeInd = cancelDisbursementTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ReinstatementDisbursement> getReinstatementDisbursementCollection() {
        return reinstatementDisbursementCollection;
    }

    public void setReinstatementDisbursementCollection(Collection<ReinstatementDisbursement> reinstatementDisbursementCollection) {
        this.reinstatementDisbursementCollection = reinstatementDisbursementCollection;
    }

    public ContractCancel getContractcancelIdFk() {
        return contractcancelIdFk;
    }

    public void setContractcancelIdFk(ContractCancel contractcancelIdFk) {
        this.contractcancelIdFk = contractcancelIdFk;
    }

    public ContractDisbursement getContractDisbursementIdFk() {
        return contractDisbursementIdFk;
    }

    public void setContractDisbursementIdFk(ContractDisbursement contractDisbursementIdFk) {
        this.contractDisbursementIdFk = contractDisbursementIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelDisbursementId != null ? cancelDisbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CancelDisbursement)) {
            return false;
        }
        CancelDisbursement other = (CancelDisbursement) object;
        if ((this.cancelDisbursementId == null && other.cancelDisbursementId != null) || (this.cancelDisbursementId != null && !this.cancelDisbursementId.equals(other.cancelDisbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CancelDisbursement[ cancelDisbursementId=" + cancelDisbursementId + " ]";
    }
    
}
