/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimMiscPaymentDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimMiscPaymentDetail.findAll", query = "SELECT c FROM ClaimMiscPaymentDetail c")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByClaimMiscPaymentDetailId", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.claimMiscPaymentDetailId = :claimMiscPaymentDetailId")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByDescription", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.description = :description")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByReason", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.reason = :reason")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByAmount", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.amount = :amount")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByEnteredDate", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByDeletedInd", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByPostingHistoryIdFk", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.postingHistoryIdFk = :postingHistoryIdFk")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByPaidAmount", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.paidAmount = :paidAmount")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByShouldHoldPayment", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.shouldHoldPayment = :shouldHoldPayment")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByPaymentAuthorizationIdFk", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.paymentAuthorizationIdFk = :paymentAuthorizationIdFk")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByPaidInd", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.paidInd = :paidInd")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByApprovedInd", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.approvedInd = :approvedInd")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByUpdateUserName", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimMiscPaymentDetail.findByUpdateLast", query = "SELECT c FROM ClaimMiscPaymentDetail c WHERE c.updateLast = :updateLast")})
public class ClaimMiscPaymentDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimMiscPaymentDetailId")
    private Integer claimMiscPaymentDetailId;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @Size(max = 200)
    @Column(name = "reason")
    private String reason;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Column(name = "postingHistoryIdFk")
    private Integer postingHistoryIdFk;
    @Column(name = "paidAmount")
    private BigDecimal paidAmount;
    @Column(name = "shouldHoldPayment")
    private Boolean shouldHoldPayment;
    @Column(name = "paymentAuthorizationIdFk")
    private Integer paymentAuthorizationIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paidInd")
    private boolean paidInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "approvedInd")
    private boolean approvedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "componentIdFk", referencedColumnName = "componentId")
    @ManyToOne
    private CfComponent componentIdFk;
    @JoinColumn(name = "claimMiscPaymentIdFk", referencedColumnName = "claimMiscPaymentId")
    @ManyToOne
    private ClaimMiscPayment claimMiscPaymentIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public ClaimMiscPaymentDetail() {
    }

    public ClaimMiscPaymentDetail(Integer claimMiscPaymentDetailId) {
        this.claimMiscPaymentDetailId = claimMiscPaymentDetailId;
    }

    public ClaimMiscPaymentDetail(Integer claimMiscPaymentDetailId, BigDecimal amount, boolean paidInd, boolean approvedInd) {
        this.claimMiscPaymentDetailId = claimMiscPaymentDetailId;
        this.amount = amount;
        this.paidInd = paidInd;
        this.approvedInd = approvedInd;
    }

    public Integer getClaimMiscPaymentDetailId() {
        return claimMiscPaymentDetailId;
    }

    public void setClaimMiscPaymentDetailId(Integer claimMiscPaymentDetailId) {
        this.claimMiscPaymentDetailId = claimMiscPaymentDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Integer getPostingHistoryIdFk() {
        return postingHistoryIdFk;
    }

    public void setPostingHistoryIdFk(Integer postingHistoryIdFk) {
        this.postingHistoryIdFk = postingHistoryIdFk;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Boolean getShouldHoldPayment() {
        return shouldHoldPayment;
    }

    public void setShouldHoldPayment(Boolean shouldHoldPayment) {
        this.shouldHoldPayment = shouldHoldPayment;
    }

    public Integer getPaymentAuthorizationIdFk() {
        return paymentAuthorizationIdFk;
    }

    public void setPaymentAuthorizationIdFk(Integer paymentAuthorizationIdFk) {
        this.paymentAuthorizationIdFk = paymentAuthorizationIdFk;
    }

    public boolean getPaidInd() {
        return paidInd;
    }

    public void setPaidInd(boolean paidInd) {
        this.paidInd = paidInd;
    }

    public boolean getApprovedInd() {
        return approvedInd;
    }

    public void setApprovedInd(boolean approvedInd) {
        this.approvedInd = approvedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public CfComponent getComponentIdFk() {
        return componentIdFk;
    }

    public void setComponentIdFk(CfComponent componentIdFk) {
        this.componentIdFk = componentIdFk;
    }

    public ClaimMiscPayment getClaimMiscPaymentIdFk() {
        return claimMiscPaymentIdFk;
    }

    public void setClaimMiscPaymentIdFk(ClaimMiscPayment claimMiscPaymentIdFk) {
        this.claimMiscPaymentIdFk = claimMiscPaymentIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimMiscPaymentDetailId != null ? claimMiscPaymentDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimMiscPaymentDetail)) {
            return false;
        }
        ClaimMiscPaymentDetail other = (ClaimMiscPaymentDetail) object;
        if ((this.claimMiscPaymentDetailId == null && other.claimMiscPaymentDetailId != null) || (this.claimMiscPaymentDetailId != null && !this.claimMiscPaymentDetailId.equals(other.claimMiscPaymentDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimMiscPaymentDetail[ claimMiscPaymentDetailId=" + claimMiscPaymentDetailId + " ]";
    }
    
}
