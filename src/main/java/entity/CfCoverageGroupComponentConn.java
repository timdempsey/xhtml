/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCoverageGroupComponentConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCoverageGroupComponentConn.findAll", query = "SELECT c FROM CfCoverageGroupComponentConn c")
    , @NamedQuery(name = "CfCoverageGroupComponentConn.findByCoverageGroupComponentConnId", query = "SELECT c FROM CfCoverageGroupComponentConn c WHERE c.coverageGroupComponentConnId = :coverageGroupComponentConnId")})
public class CfCoverageGroupComponentConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CoverageGroupComponentConnId")
    private Integer coverageGroupComponentConnId;
    @JoinColumn(name = "componentId", referencedColumnName = "componentId")
    @ManyToOne(optional = false)
    private CfComponent componentId;
    @JoinColumn(name = "coverageGroupId", referencedColumnName = "coverageGroupId")
    @ManyToOne(optional = false)
    private CfCoverageGroup coverageGroupId;

    public CfCoverageGroupComponentConn() {
    }

    public CfCoverageGroupComponentConn(Integer coverageGroupComponentConnId) {
        this.coverageGroupComponentConnId = coverageGroupComponentConnId;
    }

    public Integer getCoverageGroupComponentConnId() {
        return coverageGroupComponentConnId;
    }

    public void setCoverageGroupComponentConnId(Integer coverageGroupComponentConnId) {
        this.coverageGroupComponentConnId = coverageGroupComponentConnId;
    }

    public CfComponent getComponentId() {
        return componentId;
    }

    public void setComponentId(CfComponent componentId) {
        this.componentId = componentId;
    }

    public CfCoverageGroup getCoverageGroupId() {
        return coverageGroupId;
    }

    public void setCoverageGroupId(CfCoverageGroup coverageGroupId) {
        this.coverageGroupId = coverageGroupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coverageGroupComponentConnId != null ? coverageGroupComponentConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCoverageGroupComponentConn)) {
            return false;
        }
        CfCoverageGroupComponentConn other = (CfCoverageGroupComponentConn) object;
        if ((this.coverageGroupComponentConnId == null && other.coverageGroupComponentConnId != null) || (this.coverageGroupComponentConnId != null && !this.coverageGroupComponentConnId.equals(other.coverageGroupComponentConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCoverageGroupComponentConn[ coverageGroupComponentConnId=" + coverageGroupComponentConnId + " ]";
    }
    
}
