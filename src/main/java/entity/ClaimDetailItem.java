/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimDetailItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimDetailItem.findAll", query = "SELECT c FROM ClaimDetailItem c")
    , @NamedQuery(name = "ClaimDetailItem.findByDetailItemId", query = "SELECT c FROM ClaimDetailItem c WHERE c.detailItemId = :detailItemId")
    , @NamedQuery(name = "ClaimDetailItem.findByItemIdFk", query = "SELECT c FROM ClaimDetailItem c WHERE c.itemIdFk = :itemIdFk")
    , @NamedQuery(name = "ClaimDetailItem.findByApprovalStatus", query = "SELECT c FROM ClaimDetailItem c WHERE c.approvalStatus = :approvalStatus")
    , @NamedQuery(name = "ClaimDetailItem.findByDescription", query = "SELECT c FROM ClaimDetailItem c WHERE c.description = :description")
    , @NamedQuery(name = "ClaimDetailItem.findByAdjustmentAmt", query = "SELECT c FROM ClaimDetailItem c WHERE c.adjustmentAmt = :adjustmentAmt")
    , @NamedQuery(name = "ClaimDetailItem.findByDeleted", query = "SELECT c FROM ClaimDetailItem c WHERE c.deleted = :deleted")
    , @NamedQuery(name = "ClaimDetailItem.findBySrtCodeLaborOp", query = "SELECT c FROM ClaimDetailItem c WHERE c.srtCodeLaborOp = :srtCodeLaborOp")
    , @NamedQuery(name = "ClaimDetailItem.findByUpdateUserName", query = "SELECT c FROM ClaimDetailItem c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimDetailItem.findByUpdateLast", query = "SELECT c FROM ClaimDetailItem c WHERE c.updateLast = :updateLast")})
public class ClaimDetailItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "detailItemId")
    private Integer detailItemId;
    @Column(name = "itemIdFk")
    private Integer itemIdFk;
    @Size(max = 10)
    @Column(name = "approvalStatus")
    private String approvalStatus;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "adjustmentAmt")
    private BigDecimal adjustmentAmt;
    @Column(name = "deleted")
    private Boolean deleted;
    @Size(max = 60)
    @Column(name = "srtCodeLaborOp")
    private String srtCodeLaborOp;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "detailItemIdFk")
    private Collection<ClaimLabor> claimLaborCollection;
    @OneToMany(mappedBy = "detailItemIdFk")
    private Collection<ClaimPart> claimPartCollection;
    @JoinColumn(name = "claimDetailIdFk", referencedColumnName = "claimDetailId")
    @ManyToOne
    private ClaimDetail claimDetailIdFk;
    @JoinColumn(name = "claimLaborIdFk", referencedColumnName = "claimLaborId")
    @ManyToOne
    private ClaimLabor claimLaborIdFk;
    @JoinColumn(name = "enteredIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredIdFk;

    public ClaimDetailItem() {
    }

    public ClaimDetailItem(Integer detailItemId) {
        this.detailItemId = detailItemId;
    }

    public Integer getDetailItemId() {
        return detailItemId;
    }

    public void setDetailItemId(Integer detailItemId) {
        this.detailItemId = detailItemId;
    }

    public Integer getItemIdFk() {
        return itemIdFk;
    }

    public void setItemIdFk(Integer itemIdFk) {
        this.itemIdFk = itemIdFk;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAdjustmentAmt() {
        return adjustmentAmt;
    }

    public void setAdjustmentAmt(BigDecimal adjustmentAmt) {
        this.adjustmentAmt = adjustmentAmt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getSrtCodeLaborOp() {
        return srtCodeLaborOp;
    }

    public void setSrtCodeLaborOp(String srtCodeLaborOp) {
        this.srtCodeLaborOp = srtCodeLaborOp;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimLabor> getClaimLaborCollection() {
        return claimLaborCollection;
    }

    public void setClaimLaborCollection(Collection<ClaimLabor> claimLaborCollection) {
        this.claimLaborCollection = claimLaborCollection;
    }

    @XmlTransient
    public Collection<ClaimPart> getClaimPartCollection() {
        return claimPartCollection;
    }

    public void setClaimPartCollection(Collection<ClaimPart> claimPartCollection) {
        this.claimPartCollection = claimPartCollection;
    }

    public ClaimDetail getClaimDetailIdFk() {
        return claimDetailIdFk;
    }

    public void setClaimDetailIdFk(ClaimDetail claimDetailIdFk) {
        this.claimDetailIdFk = claimDetailIdFk;
    }

    public ClaimLabor getClaimLaborIdFk() {
        return claimLaborIdFk;
    }

    public void setClaimLaborIdFk(ClaimLabor claimLaborIdFk) {
        this.claimLaborIdFk = claimLaborIdFk;
    }

    public UserMember getEnteredIdFk() {
        return enteredIdFk;
    }

    public void setEnteredIdFk(UserMember enteredIdFk) {
        this.enteredIdFk = enteredIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detailItemId != null ? detailItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimDetailItem)) {
            return false;
        }
        ClaimDetailItem other = (ClaimDetailItem) object;
        if ((this.detailItemId == null && other.detailItemId != null) || (this.detailItemId != null && !this.detailItemId.equals(other.detailItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimDetailItem[ detailItemId=" + detailItemId + " ]";
    }
    
}
