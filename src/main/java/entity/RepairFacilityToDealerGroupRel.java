/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RepairFacilityToDealerGroupRel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepairFacilityToDealerGroupRel.findAll", query = "SELECT r FROM RepairFacilityToDealerGroupRel r")
    , @NamedQuery(name = "RepairFacilityToDealerGroupRel.findByRepairFacilityToDealerGroupRelId", query = "SELECT r FROM RepairFacilityToDealerGroupRel r WHERE r.repairFacilityToDealerGroupRelId = :repairFacilityToDealerGroupRelId")
    , @NamedQuery(name = "RepairFacilityToDealerGroupRel.findByUpdateUserName", query = "SELECT r FROM RepairFacilityToDealerGroupRel r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RepairFacilityToDealerGroupRel.findByUpdateLast", query = "SELECT r FROM RepairFacilityToDealerGroupRel r WHERE r.updateLast = :updateLast")})
public class RepairFacilityToDealerGroupRel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "repairFacilityToDealerGroupRelId")
    private Integer repairFacilityToDealerGroupRelId;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "DealerGroupId", referencedColumnName = "dealerGroupId")
    @ManyToOne(optional = false)
    private DealerGroup dealerGroupId;
    @JoinColumn(name = "RepairFacilityId", referencedColumnName = "repairFacilityId")
    @ManyToOne(optional = false)
    private RepairFacility repairFacilityId;

    public RepairFacilityToDealerGroupRel() {
    }

    public RepairFacilityToDealerGroupRel(Integer repairFacilityToDealerGroupRelId) {
        this.repairFacilityToDealerGroupRelId = repairFacilityToDealerGroupRelId;
    }

    public Integer getRepairFacilityToDealerGroupRelId() {
        return repairFacilityToDealerGroupRelId;
    }

    public void setRepairFacilityToDealerGroupRelId(Integer repairFacilityToDealerGroupRelId) {
        this.repairFacilityToDealerGroupRelId = repairFacilityToDealerGroupRelId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public DealerGroup getDealerGroupId() {
        return dealerGroupId;
    }

    public void setDealerGroupId(DealerGroup dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }

    public RepairFacility getRepairFacilityId() {
        return repairFacilityId;
    }

    public void setRepairFacilityId(RepairFacility repairFacilityId) {
        this.repairFacilityId = repairFacilityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repairFacilityToDealerGroupRelId != null ? repairFacilityToDealerGroupRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepairFacilityToDealerGroupRel)) {
            return false;
        }
        RepairFacilityToDealerGroupRel other = (RepairFacilityToDealerGroupRel) object;
        if ((this.repairFacilityToDealerGroupRelId == null && other.repairFacilityToDealerGroupRelId != null) || (this.repairFacilityToDealerGroupRelId != null && !this.repairFacilityToDealerGroupRelId.equals(other.repairFacilityToDealerGroupRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RepairFacilityToDealerGroupRel[ repairFacilityToDealerGroupRelId=" + repairFacilityToDealerGroupRelId + " ]";
    }
    
}
