/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "UserMember")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserMember.findAll", query = "SELECT u FROM UserMember u")
    , @NamedQuery(name = "UserMember.findByUserMemberId", query = "SELECT u FROM UserMember u WHERE u.userMemberId = :userMemberId")
    , @NamedQuery(name = "UserMember.findByUserName", query = "SELECT u FROM UserMember u WHERE u.userName = :userName")
    , @NamedQuery(name = "UserMember.findByPassword", query = "SELECT u FROM UserMember u WHERE u.password = :password")
    , @NamedQuery(name = "UserMember.findByChallengeQuestion1", query = "SELECT u FROM UserMember u WHERE u.challengeQuestion1 = :challengeQuestion1")
    , @NamedQuery(name = "UserMember.findByChallengeAnswer1", query = "SELECT u FROM UserMember u WHERE u.challengeAnswer1 = :challengeAnswer1")
    , @NamedQuery(name = "UserMember.findByChallengeQuestion2", query = "SELECT u FROM UserMember u WHERE u.challengeQuestion2 = :challengeQuestion2")
    , @NamedQuery(name = "UserMember.findByChallengeAnswer2", query = "SELECT u FROM UserMember u WHERE u.challengeAnswer2 = :challengeAnswer2")
    , @NamedQuery(name = "UserMember.findByChallengeQuestion3", query = "SELECT u FROM UserMember u WHERE u.challengeQuestion3 = :challengeQuestion3")
    , @NamedQuery(name = "UserMember.findByChallengeAnswer3", query = "SELECT u FROM UserMember u WHERE u.challengeAnswer3 = :challengeAnswer3")
    , @NamedQuery(name = "UserMember.findByLockedOut", query = "SELECT u FROM UserMember u WHERE u.lockedOut = :lockedOut")
    , @NamedQuery(name = "UserMember.findByFailedPasswordAttempts", query = "SELECT u FROM UserMember u WHERE u.failedPasswordAttempts = :failedPasswordAttempts")
    , @NamedQuery(name = "UserMember.findByFailedChallengeAnswerAttempts", query = "SELECT u FROM UserMember u WHERE u.failedChallengeAnswerAttempts = :failedChallengeAnswerAttempts")
    , @NamedQuery(name = "UserMember.findByDateLastPasswordChange", query = "SELECT u FROM UserMember u WHERE u.dateLastPasswordChange = :dateLastPasswordChange")
    , @NamedQuery(name = "UserMember.findByDateLastLogin", query = "SELECT u FROM UserMember u WHERE u.dateLastLogin = :dateLastLogin")
    , @NamedQuery(name = "UserMember.findByDateLastActive", query = "SELECT u FROM UserMember u WHERE u.dateLastActive = :dateLastActive")
    , @NamedQuery(name = "UserMember.findByDateLastLockout", query = "SELECT u FROM UserMember u WHERE u.dateLastLockout = :dateLastLockout")
    , @NamedQuery(name = "UserMember.findByIsFiltered", query = "SELECT u FROM UserMember u WHERE u.isFiltered = :isFiltered")
    , @NamedQuery(name = "UserMember.findByThemeOverrideFk", query = "SELECT u FROM UserMember u WHERE u.themeOverrideFk = :themeOverrideFk")
    , @NamedQuery(name = "UserMember.findByUpdateUserName", query = "SELECT u FROM UserMember u WHERE u.updateUserName = :updateUserName")
    , @NamedQuery(name = "UserMember.findByUpdateLast", query = "SELECT u FROM UserMember u WHERE u.updateLast = :updateLast")})
public class UserMember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userMemberId")
    private Integer userMemberId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "userName")
    private String userName;
    @Size(max = 68)
    @Column(name = "password")
    private String password;
    @Size(max = 50)
    @Column(name = "challengeQuestion1")
    private String challengeQuestion1;
    @Size(max = 50)
    @Column(name = "ChallengeAnswer1")
    private String challengeAnswer1;
    @Size(max = 50)
    @Column(name = "challengeQuestion2")
    private String challengeQuestion2;
    @Size(max = 50)
    @Column(name = "challengeAnswer2")
    private String challengeAnswer2;
    @Size(max = 50)
    @Column(name = "challengeQuestion3")
    private String challengeQuestion3;
    @Size(max = 50)
    @Column(name = "challengeAnswer3")
    private String challengeAnswer3;
    @Column(name = "lockedOut")
    private Boolean lockedOut;
    @Column(name = "failedPasswordAttempts")
    private Integer failedPasswordAttempts;
    @Column(name = "failedChallengeAnswerAttempts")
    private Integer failedChallengeAnswerAttempts;
    @Column(name = "dateLastPasswordChange")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastPasswordChange;
    @Column(name = "dateLastLogin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastLogin;
    @Column(name = "dateLastActive")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastActive;
    @Column(name = "dateLastLockout")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastLockout;
    @Column(name = "IsFiltered")
    private Boolean isFiltered;
    @Column(name = "themeOverrideFk")
    private Integer themeOverrideFk;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lockedByIdFk")
    private Collection<CfItemLock> cfItemLockCollection;
    @OneToMany(mappedBy = "releasedByIdFk")
    private Collection<CfItemLock> cfItemLockCollection1;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<AccountKeeper> accountKeeperCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(mappedBy = "enteredIdFk")
    private Collection<ClaimLabor> claimLaborCollection;
    @OneToMany(mappedBy = "soldByUserIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<MarkupDetail> markupDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<RerateSession> rerateSessionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<DeductibleDetail> deductibleDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<ClaimMiscPayment> claimMiscPaymentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<CfUserRoleConn> cfUserRoleConnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<CfFile> cfFileCollection;
    @OneToMany(mappedBy = "rolledBackByIdFk")
    private Collection<RateWizardUpload> rateWizardUploadCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "uploadedByIdFk")
    private Collection<RateWizardUpload> rateWizardUploadCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<CfAddressType> cfAddressTypeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lastModifiedByIdFk")
    private Collection<CfAddressType> cfAddressTypeCollection1;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<ProgramDetail> programDetailCollection;
    @JoinColumn(name = "userMemberId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @JoinColumn(name = "agentIdFk", referencedColumnName = "agentId")
    @ManyToOne
    private Agent agentIdFk;
    @JoinColumn(name = "departmentIdFk", referencedColumnName = "departmentId")
    @ManyToOne
    private CfDepartment departmentIdFk;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "dealerGroupIdFk", referencedColumnName = "dealerGroupId")
    @ManyToOne
    private DealerGroup dealerGroupIdFk;
    @JoinColumn(name = "userTypeInd", referencedColumnName = "userTypeInd")
    @ManyToOne
    private UserType userTypeInd;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<TermDetail> termDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<CfUserWidgetGroupConn> cfUserWidgetGroupConnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "userMemberIdFk")
    private Collection<CfSavedWidgetState> cfSavedWidgetStateCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<CfUserMemberAndDealer> cfUserMemberAndDealerCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @OneToMany(mappedBy = "ignoredByIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<HistoryLog> historyLogCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<VinOverride> vinOverrideCollection;
    @OneToMany(mappedBy = "updatedByIdFk")
    private Collection<VinOverride> vinOverrideCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<AdjustmentDetail> adjustmentDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<PostingHistory> postingHistoryCollection;
    @OneToMany(mappedBy = "reinsuranceCommittedByIdFk")
    private Collection<PostingHistory> postingHistoryCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<ReinsuranceDealerDetail> reinsuranceDealerDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<RateDetail> rateDetailCollection;
    @OneToMany(mappedBy = "enteredIdFk")
    private Collection<ClaimPart> claimPartCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<CancelRequest> cancelRequestCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<ProductDetail> productDetailCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<LimitDetail> limitDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "replacedByIdFk")
    private Collection<ContractReplacement> contractReplacementCollection;
    @OneToMany(mappedBy = "enteredByIdFk")
    private Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<ReportUserConnection> reportUserConnectionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberIdFk")
    private Collection<CfPasswordReset> cfPasswordResetCollection;
    @OneToMany(mappedBy = "enteredIdFk")
    private Collection<ClaimDetailItem> claimDetailItemCollection;
    @OneToMany(mappedBy = "viewedByIdFk")
    private Collection<ClaimViewLog> claimViewLogCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<SurchargeDetail> surchargeDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<CfPDFDetail> cfPDFDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<ReportGroupUserConnection> reportGroupUserConnectionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<ClaimEventPPMDetail> claimEventPPMDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<CfDepartment> cfDepartmentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<CfUserMemberGUITableViewConn> cfUserMemberGUITableViewConnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enteredByIdFk")
    private Collection<ReinsuranceReinsurerDetail> reinsuranceReinsurerDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMemberId")
    private Collection<CfUserGroupConn> cfUserGroupConnCollection;
    @OneToMany(mappedBy = "enteredIdFk")
    private Collection<Claim> claimCollection;
    @OneToMany(mappedBy = "ownedByFk")
    private Collection<Claim> claimCollection1;

    public UserMember() {
    }

    public UserMember(Integer userMemberId) {
        this.userMemberId = userMemberId;
    }

    public UserMember(Integer userMemberId, String userName) {
        this.userMemberId = userMemberId;
        this.userName = userName;
    }

    public Integer getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(Integer userMemberId) {
        this.userMemberId = userMemberId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChallengeQuestion1() {
        return challengeQuestion1;
    }

    public void setChallengeQuestion1(String challengeQuestion1) {
        this.challengeQuestion1 = challengeQuestion1;
    }

    public String getChallengeAnswer1() {
        return challengeAnswer1;
    }

    public void setChallengeAnswer1(String challengeAnswer1) {
        this.challengeAnswer1 = challengeAnswer1;
    }

    public String getChallengeQuestion2() {
        return challengeQuestion2;
    }

    public void setChallengeQuestion2(String challengeQuestion2) {
        this.challengeQuestion2 = challengeQuestion2;
    }

    public String getChallengeAnswer2() {
        return challengeAnswer2;
    }

    public void setChallengeAnswer2(String challengeAnswer2) {
        this.challengeAnswer2 = challengeAnswer2;
    }

    public String getChallengeQuestion3() {
        return challengeQuestion3;
    }

    public void setChallengeQuestion3(String challengeQuestion3) {
        this.challengeQuestion3 = challengeQuestion3;
    }

    public String getChallengeAnswer3() {
        return challengeAnswer3;
    }

    public void setChallengeAnswer3(String challengeAnswer3) {
        this.challengeAnswer3 = challengeAnswer3;
    }

    public Boolean getLockedOut() {
        return lockedOut;
    }

    public void setLockedOut(Boolean lockedOut) {
        this.lockedOut = lockedOut;
    }

    public Integer getFailedPasswordAttempts() {
        return failedPasswordAttempts;
    }

    public void setFailedPasswordAttempts(Integer failedPasswordAttempts) {
        this.failedPasswordAttempts = failedPasswordAttempts;
    }

    public Integer getFailedChallengeAnswerAttempts() {
        return failedChallengeAnswerAttempts;
    }

    public void setFailedChallengeAnswerAttempts(Integer failedChallengeAnswerAttempts) {
        this.failedChallengeAnswerAttempts = failedChallengeAnswerAttempts;
    }

    public Date getDateLastPasswordChange() {
        return dateLastPasswordChange;
    }

    public void setDateLastPasswordChange(Date dateLastPasswordChange) {
        this.dateLastPasswordChange = dateLastPasswordChange;
    }

    public Date getDateLastLogin() {
        return dateLastLogin;
    }

    public void setDateLastLogin(Date dateLastLogin) {
        this.dateLastLogin = dateLastLogin;
    }

    public Date getDateLastActive() {
        return dateLastActive;
    }

    public void setDateLastActive(Date dateLastActive) {
        this.dateLastActive = dateLastActive;
    }

    public Date getDateLastLockout() {
        return dateLastLockout;
    }

    public void setDateLastLockout(Date dateLastLockout) {
        this.dateLastLockout = dateLastLockout;
    }

    public Boolean getIsFiltered() {
        return isFiltered;
    }

    public void setIsFiltered(Boolean isFiltered) {
        this.isFiltered = isFiltered;
    }

    public Integer getThemeOverrideFk() {
        return themeOverrideFk;
    }

    public void setThemeOverrideFk(Integer themeOverrideFk) {
        this.themeOverrideFk = themeOverrideFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfItemLock> getCfItemLockCollection() {
        return cfItemLockCollection;
    }

    public void setCfItemLockCollection(Collection<CfItemLock> cfItemLockCollection) {
        this.cfItemLockCollection = cfItemLockCollection;
    }

    @XmlTransient
    public Collection<CfItemLock> getCfItemLockCollection1() {
        return cfItemLockCollection1;
    }

    public void setCfItemLockCollection1(Collection<CfItemLock> cfItemLockCollection1) {
        this.cfItemLockCollection1 = cfItemLockCollection1;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection() {
        return accountKeeperCollection;
    }

    public void setAccountKeeperCollection(Collection<AccountKeeper> accountKeeperCollection) {
        this.accountKeeperCollection = accountKeeperCollection;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<ClaimMiscPaymentDetail> getClaimMiscPaymentDetailCollection() {
        return claimMiscPaymentDetailCollection;
    }

    public void setClaimMiscPaymentDetailCollection(Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection) {
        this.claimMiscPaymentDetailCollection = claimMiscPaymentDetailCollection;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<ClaimLabor> getClaimLaborCollection() {
        return claimLaborCollection;
    }

    public void setClaimLaborCollection(Collection<ClaimLabor> claimLaborCollection) {
        this.claimLaborCollection = claimLaborCollection;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<MarkupDetail> getMarkupDetailCollection() {
        return markupDetailCollection;
    }

    public void setMarkupDetailCollection(Collection<MarkupDetail> markupDetailCollection) {
        this.markupDetailCollection = markupDetailCollection;
    }

    @XmlTransient
    public Collection<RerateSession> getRerateSessionCollection() {
        return rerateSessionCollection;
    }

    public void setRerateSessionCollection(Collection<RerateSession> rerateSessionCollection) {
        this.rerateSessionCollection = rerateSessionCollection;
    }

    @XmlTransient
    public Collection<DeductibleDetail> getDeductibleDetailCollection() {
        return deductibleDetailCollection;
    }

    public void setDeductibleDetailCollection(Collection<DeductibleDetail> deductibleDetailCollection) {
        this.deductibleDetailCollection = deductibleDetailCollection;
    }

    @XmlTransient
    public Collection<ClaimMiscPayment> getClaimMiscPaymentCollection() {
        return claimMiscPaymentCollection;
    }

    public void setClaimMiscPaymentCollection(Collection<ClaimMiscPayment> claimMiscPaymentCollection) {
        this.claimMiscPaymentCollection = claimMiscPaymentCollection;
    }

    @XmlTransient
    public Collection<CfUserRoleConn> getCfUserRoleConnCollection() {
        return cfUserRoleConnCollection;
    }

    public void setCfUserRoleConnCollection(Collection<CfUserRoleConn> cfUserRoleConnCollection) {
        this.cfUserRoleConnCollection = cfUserRoleConnCollection;
    }

    @XmlTransient
    public Collection<CfFile> getCfFileCollection() {
        return cfFileCollection;
    }

    public void setCfFileCollection(Collection<CfFile> cfFileCollection) {
        this.cfFileCollection = cfFileCollection;
    }

    @XmlTransient
    public Collection<RateWizardUpload> getRateWizardUploadCollection() {
        return rateWizardUploadCollection;
    }

    public void setRateWizardUploadCollection(Collection<RateWizardUpload> rateWizardUploadCollection) {
        this.rateWizardUploadCollection = rateWizardUploadCollection;
    }

    @XmlTransient
    public Collection<RateWizardUpload> getRateWizardUploadCollection1() {
        return rateWizardUploadCollection1;
    }

    public void setRateWizardUploadCollection1(Collection<RateWizardUpload> rateWizardUploadCollection1) {
        this.rateWizardUploadCollection1 = rateWizardUploadCollection1;
    }

    @XmlTransient
    public Collection<CfAddressType> getCfAddressTypeCollection() {
        return cfAddressTypeCollection;
    }

    public void setCfAddressTypeCollection(Collection<CfAddressType> cfAddressTypeCollection) {
        this.cfAddressTypeCollection = cfAddressTypeCollection;
    }

    @XmlTransient
    public Collection<CfAddressType> getCfAddressTypeCollection1() {
        return cfAddressTypeCollection1;
    }

    public void setCfAddressTypeCollection1(Collection<CfAddressType> cfAddressTypeCollection1) {
        this.cfAddressTypeCollection1 = cfAddressTypeCollection1;
    }

    @XmlTransient
    public Collection<ProgramDetail> getProgramDetailCollection() {
        return programDetailCollection;
    }

    public void setProgramDetailCollection(Collection<ProgramDetail> programDetailCollection) {
        this.programDetailCollection = programDetailCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    public Agent getAgentIdFk() {
        return agentIdFk;
    }

    public void setAgentIdFk(Agent agentIdFk) {
        this.agentIdFk = agentIdFk;
    }

    public CfDepartment getDepartmentIdFk() {
        return departmentIdFk;
    }

    public void setDepartmentIdFk(CfDepartment departmentIdFk) {
        this.departmentIdFk = departmentIdFk;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public DealerGroup getDealerGroupIdFk() {
        return dealerGroupIdFk;
    }

    public void setDealerGroupIdFk(DealerGroup dealerGroupIdFk) {
        this.dealerGroupIdFk = dealerGroupIdFk;
    }

    public UserType getUserTypeInd() {
        return userTypeInd;
    }

    public void setUserTypeInd(UserType userTypeInd) {
        this.userTypeInd = userTypeInd;
    }

    @XmlTransient
    public Collection<TermDetail> getTermDetailCollection() {
        return termDetailCollection;
    }

    public void setTermDetailCollection(Collection<TermDetail> termDetailCollection) {
        this.termDetailCollection = termDetailCollection;
    }

    @XmlTransient
    public Collection<CfUserWidgetGroupConn> getCfUserWidgetGroupConnCollection() {
        return cfUserWidgetGroupConnCollection;
    }

    public void setCfUserWidgetGroupConnCollection(Collection<CfUserWidgetGroupConn> cfUserWidgetGroupConnCollection) {
        this.cfUserWidgetGroupConnCollection = cfUserWidgetGroupConnCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<CfSavedWidgetState> getCfSavedWidgetStateCollection() {
        return cfSavedWidgetStateCollection;
    }

    public void setCfSavedWidgetStateCollection(Collection<CfSavedWidgetState> cfSavedWidgetStateCollection) {
        this.cfSavedWidgetStateCollection = cfSavedWidgetStateCollection;
    }

    @XmlTransient
    public Collection<CfUserMemberAndDealer> getCfUserMemberAndDealerCollection() {
        return cfUserMemberAndDealerCollection;
    }

    public void setCfUserMemberAndDealerCollection(Collection<CfUserMemberAndDealer> cfUserMemberAndDealerCollection) {
        this.cfUserMemberAndDealerCollection = cfUserMemberAndDealerCollection;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection1() {
        return disbursementDetailCollection1;
    }

    public void setDisbursementDetailCollection1(Collection<DisbursementDetail> disbursementDetailCollection1) {
        this.disbursementDetailCollection1 = disbursementDetailCollection1;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    @XmlTransient
    public Collection<VinOverride> getVinOverrideCollection() {
        return vinOverrideCollection;
    }

    public void setVinOverrideCollection(Collection<VinOverride> vinOverrideCollection) {
        this.vinOverrideCollection = vinOverrideCollection;
    }

    @XmlTransient
    public Collection<VinOverride> getVinOverrideCollection1() {
        return vinOverrideCollection1;
    }

    public void setVinOverrideCollection1(Collection<VinOverride> vinOverrideCollection1) {
        this.vinOverrideCollection1 = vinOverrideCollection1;
    }

    @XmlTransient
    public Collection<AdjustmentDetail> getAdjustmentDetailCollection() {
        return adjustmentDetailCollection;
    }

    public void setAdjustmentDetailCollection(Collection<AdjustmentDetail> adjustmentDetailCollection) {
        this.adjustmentDetailCollection = adjustmentDetailCollection;
    }

    @XmlTransient
    public Collection<PostingHistory> getPostingHistoryCollection() {
        return postingHistoryCollection;
    }

    public void setPostingHistoryCollection(Collection<PostingHistory> postingHistoryCollection) {
        this.postingHistoryCollection = postingHistoryCollection;
    }

    @XmlTransient
    public Collection<PostingHistory> getPostingHistoryCollection1() {
        return postingHistoryCollection1;
    }

    public void setPostingHistoryCollection1(Collection<PostingHistory> postingHistoryCollection1) {
        this.postingHistoryCollection1 = postingHistoryCollection1;
    }

    @XmlTransient
    public Collection<ReinsuranceDealerDetail> getReinsuranceDealerDetailCollection() {
        return reinsuranceDealerDetailCollection;
    }

    public void setReinsuranceDealerDetailCollection(Collection<ReinsuranceDealerDetail> reinsuranceDealerDetailCollection) {
        this.reinsuranceDealerDetailCollection = reinsuranceDealerDetailCollection;
    }

    @XmlTransient
    public Collection<RateDetail> getRateDetailCollection() {
        return rateDetailCollection;
    }

    public void setRateDetailCollection(Collection<RateDetail> rateDetailCollection) {
        this.rateDetailCollection = rateDetailCollection;
    }

    @XmlTransient
    public Collection<ClaimPart> getClaimPartCollection() {
        return claimPartCollection;
    }

    public void setClaimPartCollection(Collection<ClaimPart> claimPartCollection) {
        this.claimPartCollection = claimPartCollection;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection() {
        return productDetailCollection;
    }

    public void setProductDetailCollection(Collection<ProductDetail> productDetailCollection) {
        this.productDetailCollection = productDetailCollection;
    }

    @XmlTransient
    public Collection<LimitDetail> getLimitDetailCollection() {
        return limitDetailCollection;
    }

    public void setLimitDetailCollection(Collection<LimitDetail> limitDetailCollection) {
        this.limitDetailCollection = limitDetailCollection;
    }

    @XmlTransient
    public Collection<ContractReplacement> getContractReplacementCollection() {
        return contractReplacementCollection;
    }

    public void setContractReplacementCollection(Collection<ContractReplacement> contractReplacementCollection) {
        this.contractReplacementCollection = contractReplacementCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceRuleDetail> getReinsuranceRuleDetailCollection() {
        return reinsuranceRuleDetailCollection;
    }

    public void setReinsuranceRuleDetailCollection(Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection) {
        this.reinsuranceRuleDetailCollection = reinsuranceRuleDetailCollection;
    }

    @XmlTransient
    public Collection<ReportUserConnection> getReportUserConnectionCollection() {
        return reportUserConnectionCollection;
    }

    public void setReportUserConnectionCollection(Collection<ReportUserConnection> reportUserConnectionCollection) {
        this.reportUserConnectionCollection = reportUserConnectionCollection;
    }

    @XmlTransient
    public Collection<CfPasswordReset> getCfPasswordResetCollection() {
        return cfPasswordResetCollection;
    }

    public void setCfPasswordResetCollection(Collection<CfPasswordReset> cfPasswordResetCollection) {
        this.cfPasswordResetCollection = cfPasswordResetCollection;
    }

    @XmlTransient
    public Collection<ClaimDetailItem> getClaimDetailItemCollection() {
        return claimDetailItemCollection;
    }

    public void setClaimDetailItemCollection(Collection<ClaimDetailItem> claimDetailItemCollection) {
        this.claimDetailItemCollection = claimDetailItemCollection;
    }

    @XmlTransient
    public Collection<ClaimViewLog> getClaimViewLogCollection() {
        return claimViewLogCollection;
    }

    public void setClaimViewLogCollection(Collection<ClaimViewLog> claimViewLogCollection) {
        this.claimViewLogCollection = claimViewLogCollection;
    }

    @XmlTransient
    public Collection<SurchargeDetail> getSurchargeDetailCollection() {
        return surchargeDetailCollection;
    }

    public void setSurchargeDetailCollection(Collection<SurchargeDetail> surchargeDetailCollection) {
        this.surchargeDetailCollection = surchargeDetailCollection;
    }

    @XmlTransient
    public Collection<CfPDFDetail> getCfPDFDetailCollection() {
        return cfPDFDetailCollection;
    }

    public void setCfPDFDetailCollection(Collection<CfPDFDetail> cfPDFDetailCollection) {
        this.cfPDFDetailCollection = cfPDFDetailCollection;
    }

    @XmlTransient
    public Collection<ReportGroupUserConnection> getReportGroupUserConnectionCollection() {
        return reportGroupUserConnectionCollection;
    }

    public void setReportGroupUserConnectionCollection(Collection<ReportGroupUserConnection> reportGroupUserConnectionCollection) {
        this.reportGroupUserConnectionCollection = reportGroupUserConnectionCollection;
    }

    @XmlTransient
    public Collection<ClaimEventPPMDetail> getClaimEventPPMDetailCollection() {
        return claimEventPPMDetailCollection;
    }

    public void setClaimEventPPMDetailCollection(Collection<ClaimEventPPMDetail> claimEventPPMDetailCollection) {
        this.claimEventPPMDetailCollection = claimEventPPMDetailCollection;
    }

    @XmlTransient
    public Collection<CfDepartment> getCfDepartmentCollection() {
        return cfDepartmentCollection;
    }

    public void setCfDepartmentCollection(Collection<CfDepartment> cfDepartmentCollection) {
        this.cfDepartmentCollection = cfDepartmentCollection;
    }

    @XmlTransient
    public Collection<CfUserMemberGUITableViewConn> getCfUserMemberGUITableViewConnCollection() {
        return cfUserMemberGUITableViewConnCollection;
    }

    public void setCfUserMemberGUITableViewConnCollection(Collection<CfUserMemberGUITableViewConn> cfUserMemberGUITableViewConnCollection) {
        this.cfUserMemberGUITableViewConnCollection = cfUserMemberGUITableViewConnCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceReinsurerDetail> getReinsuranceReinsurerDetailCollection() {
        return reinsuranceReinsurerDetailCollection;
    }

    public void setReinsuranceReinsurerDetailCollection(Collection<ReinsuranceReinsurerDetail> reinsuranceReinsurerDetailCollection) {
        this.reinsuranceReinsurerDetailCollection = reinsuranceReinsurerDetailCollection;
    }

    @XmlTransient
    public Collection<CfUserGroupConn> getCfUserGroupConnCollection() {
        return cfUserGroupConnCollection;
    }

    public void setCfUserGroupConnCollection(Collection<CfUserGroupConn> cfUserGroupConnCollection) {
        this.cfUserGroupConnCollection = cfUserGroupConnCollection;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection1() {
        return claimCollection1;
    }

    public void setClaimCollection1(Collection<Claim> claimCollection1) {
        this.claimCollection1 = claimCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userMemberId != null ? userMemberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMember)) {
            return false;
        }
        UserMember other = (UserMember) object;
        if ((this.userMemberId == null && other.userMemberId != null) || (this.userMemberId != null && !this.userMemberId.equals(other.userMemberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserMember[ userMemberId=" + userMemberId + " ]";
    }
    
}
