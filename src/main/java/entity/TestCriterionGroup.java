/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "TestCriterionGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestCriterionGroup.findAll", query = "SELECT t FROM TestCriterionGroup t")
    , @NamedQuery(name = "TestCriterionGroup.findByCriterionGroupId", query = "SELECT t FROM TestCriterionGroup t WHERE t.criterionGroupId = :criterionGroupId")
    , @NamedQuery(name = "TestCriterionGroup.findByAndRelationshipInd", query = "SELECT t FROM TestCriterionGroup t WHERE t.andRelationshipInd = :andRelationshipInd")
    , @NamedQuery(name = "TestCriterionGroup.findByCriterionGroupIdFk", query = "SELECT t FROM TestCriterionGroup t WHERE t.criterionGroupIdFk = :criterionGroupIdFk")
    , @NamedQuery(name = "TestCriterionGroup.findByDescription", query = "SELECT t FROM TestCriterionGroup t WHERE t.description = :description")
    , @NamedQuery(name = "TestCriterionGroup.findByTranslated", query = "SELECT t FROM TestCriterionGroup t WHERE t.translated = :translated")
    , @NamedQuery(name = "TestCriterionGroup.findByUpdateUserName", query = "SELECT t FROM TestCriterionGroup t WHERE t.updateUserName = :updateUserName")
    , @NamedQuery(name = "TestCriterionGroup.findByUpdateLast", query = "SELECT t FROM TestCriterionGroup t WHERE t.updateLast = :updateLast")})
public class TestCriterionGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "criterionGroupId")
    private Integer criterionGroupId;
    @Column(name = "andRelationshipInd")
    private Boolean andRelationshipInd;
    @Column(name = "criterionGroupIdFk")
    private Integer criterionGroupIdFk;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 2147483647)
    @Column(name = "translated")
    private String translated;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;

    public TestCriterionGroup() {
    }

    public TestCriterionGroup(Integer criterionGroupId) {
        this.criterionGroupId = criterionGroupId;
    }

    public Integer getCriterionGroupId() {
        return criterionGroupId;
    }

    public void setCriterionGroupId(Integer criterionGroupId) {
        this.criterionGroupId = criterionGroupId;
    }

    public Boolean getAndRelationshipInd() {
        return andRelationshipInd;
    }

    public void setAndRelationshipInd(Boolean andRelationshipInd) {
        this.andRelationshipInd = andRelationshipInd;
    }

    public Integer getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(Integer criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterionGroupId != null ? criterionGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestCriterionGroup)) {
            return false;
        }
        TestCriterionGroup other = (TestCriterionGroup) object;
        if ((this.criterionGroupId == null && other.criterionGroupId != null) || (this.criterionGroupId != null && !this.criterionGroupId.equals(other.criterionGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TestCriterionGroup[ criterionGroupId=" + criterionGroupId + " ]";
    }
    
}
