/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "HoldDisbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HoldDisbursement.findAll", query = "SELECT h FROM HoldDisbursement h")
    , @NamedQuery(name = "HoldDisbursement.findByHoldDisbursementId", query = "SELECT h FROM HoldDisbursement h WHERE h.holdDisbursementId = :holdDisbursementId")
    , @NamedQuery(name = "HoldDisbursement.findByUpdateUserName", query = "SELECT h FROM HoldDisbursement h WHERE h.updateUserName = :updateUserName")
    , @NamedQuery(name = "HoldDisbursement.findByUpdateLast", query = "SELECT h FROM HoldDisbursement h WHERE h.updateLast = :updateLast")})
public class HoldDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "holdDisbursementId")
    private Integer holdDisbursementId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "holdDisbursementIdFk")
    private Collection<ResolveDisbursement> resolveDisbursementCollection;
    @JoinColumn(name = "contractDisbursementIdFk", referencedColumnName = "contractDisbursementId")
    @ManyToOne
    private ContractDisbursement contractDisbursementIdFk;
    @JoinColumn(name = "ContractHoldIdFk", referencedColumnName = "contractHoldId")
    @ManyToOne
    private ContractHold contractHoldIdFk;
    @JoinColumn(name = "holdDisbursementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "reinstatementDisbursementIdFk", referencedColumnName = "reinstatementDisbursementId")
    @ManyToOne
    private ReinstatementDisbursement reinstatementDisbursementIdFk;

    public HoldDisbursement() {
    }

    public HoldDisbursement(Integer holdDisbursementId) {
        this.holdDisbursementId = holdDisbursementId;
    }

    public Integer getHoldDisbursementId() {
        return holdDisbursementId;
    }

    public void setHoldDisbursementId(Integer holdDisbursementId) {
        this.holdDisbursementId = holdDisbursementId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ResolveDisbursement> getResolveDisbursementCollection() {
        return resolveDisbursementCollection;
    }

    public void setResolveDisbursementCollection(Collection<ResolveDisbursement> resolveDisbursementCollection) {
        this.resolveDisbursementCollection = resolveDisbursementCollection;
    }

    public ContractDisbursement getContractDisbursementIdFk() {
        return contractDisbursementIdFk;
    }

    public void setContractDisbursementIdFk(ContractDisbursement contractDisbursementIdFk) {
        this.contractDisbursementIdFk = contractDisbursementIdFk;
    }

    public ContractHold getContractHoldIdFk() {
        return contractHoldIdFk;
    }

    public void setContractHoldIdFk(ContractHold contractHoldIdFk) {
        this.contractHoldIdFk = contractHoldIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public ReinstatementDisbursement getReinstatementDisbursementIdFk() {
        return reinstatementDisbursementIdFk;
    }

    public void setReinstatementDisbursementIdFk(ReinstatementDisbursement reinstatementDisbursementIdFk) {
        this.reinstatementDisbursementIdFk = reinstatementDisbursementIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (holdDisbursementId != null ? holdDisbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HoldDisbursement)) {
            return false;
        }
        HoldDisbursement other = (HoldDisbursement) object;
        if ((this.holdDisbursementId == null && other.holdDisbursementId != null) || (this.holdDisbursementId != null && !this.holdDisbursementId.equals(other.holdDisbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.HoldDisbursement[ holdDisbursementId=" + holdDisbursementId + " ]";
    }
    
}
