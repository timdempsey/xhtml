/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMRecurringEvent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMRecurringEvent.findAll", query = "SELECT p FROM PPMRecurringEvent p")
    , @NamedQuery(name = "PPMRecurringEvent.findByPPMEventIdFk", query = "SELECT p FROM PPMRecurringEvent p WHERE p.pPMEventIdFk = :pPMEventIdFk")
    , @NamedQuery(name = "PPMRecurringEvent.findByMonths", query = "SELECT p FROM PPMRecurringEvent p WHERE p.months = :months")
    , @NamedQuery(name = "PPMRecurringEvent.findByMiles", query = "SELECT p FROM PPMRecurringEvent p WHERE p.miles = :miles")
    , @NamedQuery(name = "PPMRecurringEvent.findByUpperMonthsMargin", query = "SELECT p FROM PPMRecurringEvent p WHERE p.upperMonthsMargin = :upperMonthsMargin")
    , @NamedQuery(name = "PPMRecurringEvent.findByLowerMonthsMargin", query = "SELECT p FROM PPMRecurringEvent p WHERE p.lowerMonthsMargin = :lowerMonthsMargin")
    , @NamedQuery(name = "PPMRecurringEvent.findByUpperMilesMargin", query = "SELECT p FROM PPMRecurringEvent p WHERE p.upperMilesMargin = :upperMilesMargin")
    , @NamedQuery(name = "PPMRecurringEvent.findByLowerMilesMargin", query = "SELECT p FROM PPMRecurringEvent p WHERE p.lowerMilesMargin = :lowerMilesMargin")
    , @NamedQuery(name = "PPMRecurringEvent.findByUpdateUserName", query = "SELECT p FROM PPMRecurringEvent p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMRecurringEvent.findByUpdateLast", query = "SELECT p FROM PPMRecurringEvent p WHERE p.updateLast = :updateLast")})
public class PPMRecurringEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMEventIdFk")
    private Integer pPMEventIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Months")
    private int months;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Miles")
    private int miles;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UpperMonthsMargin")
    private int upperMonthsMargin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LowerMonthsMargin")
    private int lowerMonthsMargin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UpperMilesMargin")
    private int upperMilesMargin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LowerMilesMargin")
    private int lowerMilesMargin;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "PPMEventIdFk", referencedColumnName = "PPMEventId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private PPMEvent pPMEvent;

    public PPMRecurringEvent() {
    }

    public PPMRecurringEvent(Integer pPMEventIdFk) {
        this.pPMEventIdFk = pPMEventIdFk;
    }

    public PPMRecurringEvent(Integer pPMEventIdFk, int months, int miles, int upperMonthsMargin, int lowerMonthsMargin, int upperMilesMargin, int lowerMilesMargin) {
        this.pPMEventIdFk = pPMEventIdFk;
        this.months = months;
        this.miles = miles;
        this.upperMonthsMargin = upperMonthsMargin;
        this.lowerMonthsMargin = lowerMonthsMargin;
        this.upperMilesMargin = upperMilesMargin;
        this.lowerMilesMargin = lowerMilesMargin;
    }

    public Integer getPPMEventIdFk() {
        return pPMEventIdFk;
    }

    public void setPPMEventIdFk(Integer pPMEventIdFk) {
        this.pPMEventIdFk = pPMEventIdFk;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public int getMiles() {
        return miles;
    }

    public void setMiles(int miles) {
        this.miles = miles;
    }

    public int getUpperMonthsMargin() {
        return upperMonthsMargin;
    }

    public void setUpperMonthsMargin(int upperMonthsMargin) {
        this.upperMonthsMargin = upperMonthsMargin;
    }

    public int getLowerMonthsMargin() {
        return lowerMonthsMargin;
    }

    public void setLowerMonthsMargin(int lowerMonthsMargin) {
        this.lowerMonthsMargin = lowerMonthsMargin;
    }

    public int getUpperMilesMargin() {
        return upperMilesMargin;
    }

    public void setUpperMilesMargin(int upperMilesMargin) {
        this.upperMilesMargin = upperMilesMargin;
    }

    public int getLowerMilesMargin() {
        return lowerMilesMargin;
    }

    public void setLowerMilesMargin(int lowerMilesMargin) {
        this.lowerMilesMargin = lowerMilesMargin;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public PPMEvent getPPMEvent() {
        return pPMEvent;
    }

    public void setPPMEvent(PPMEvent pPMEvent) {
        this.pPMEvent = pPMEvent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMEventIdFk != null ? pPMEventIdFk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMRecurringEvent)) {
            return false;
        }
        PPMRecurringEvent other = (PPMRecurringEvent) object;
        if ((this.pPMEventIdFk == null && other.pPMEventIdFk != null) || (this.pPMEventIdFk != null && !this.pPMEventIdFk.equals(other.pPMEventIdFk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMRecurringEvent[ pPMEventIdFk=" + pPMEventIdFk + " ]";
    }
    
}
