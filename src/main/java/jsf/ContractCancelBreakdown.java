/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;

/**
 *
 * @author Jiepi
 */
public class ContractCancelBreakdown {

    public ContractCancelBreakdown() {
    }

    public ContractCancelBreakdown(String contractNo, String product, String plan, String effectiveDate, String expiredDate, String customer, BigDecimal contractPurchasePrice, String cancelDate, String cancelReceivedDate, BigDecimal fee, String cancelReason, String cancelMethod, String cancelMethodGroup, String cancelGroupRuleMethod, String cancelGroupRuleType, String validTo, Integer currentOdometer, BigDecimal refundRatio, BigDecimal coverageCost, BigDecimal adminFee, BigDecimal adminRefund, BigDecimal adminPortion, BigDecimal dealerProfit, BigDecimal dealerRetainedFee, BigDecimal dpRefund, BigDecimal dealerPortion, BigDecimal customerRefund, BigDecimal amountToPay, String payee, BigDecimal unPaidClaim) {
        this.contractNo = contractNo;
        this.product = product;
        this.plan = plan;
        this.effectiveDate = effectiveDate;
        this.expiredDate = expiredDate;
        this.customer = customer;
        this.contractPurchasePrice = contractPurchasePrice;
        this.cancelDate = cancelDate;
        this.cancelReceivedDate = cancelReceivedDate;
        this.fee = fee;
        this.cancelReason = cancelReason;
        this.cancelMethod = cancelMethod;
        this.cancelMethodGroup = cancelMethodGroup;
        this.cancelGroupRuleMethod = cancelGroupRuleMethod;
        this.cancelGroupRuleType = cancelGroupRuleType;
        this.validTo = validTo;
        this.currentOdometer = currentOdometer;
        this.refundRatio = refundRatio;
        this.coverageCost = coverageCost;
        this.adminFee = adminFee;
        this.adminRefund = adminRefund;
        this.adminPortion = adminPortion;
        this.dealerProfit = dealerProfit;
        this.dealerRetainedFee = dealerRetainedFee;
        this.dpRefund = dpRefund;
        this.dealerPortion = dealerPortion;
        this.customerRefund = customerRefund;
        this.amountToPay = amountToPay;
        this.payee = payee;
        this.unPaidClaim = unPaidClaim;
    }

    

    

    private String contractNo;
    private String product;
    private String plan;
    private String effectiveDate;
    private String expiredDate;
    private String customer;
    private BigDecimal contractPurchasePrice;
    private String cancelDate;
    private String cancelReceivedDate;
    private BigDecimal fee;
    private String cancelReason;
    private String cancelMethod;
    private String cancelMethodGroup;
    private String cancelGroupRuleMethod;
    private String cancelGroupRuleType;
    private String validTo;
    private Integer currentOdometer;
    private BigDecimal refundRatio;
    private BigDecimal coverageCost;
    private BigDecimal adminFee;
    private BigDecimal adminRefund;
    private BigDecimal adminPortion;
    private BigDecimal dealerProfit;
    private BigDecimal dealerRetainedFee;
    private BigDecimal dpRefund;
    private BigDecimal dealerPortion;
    private BigDecimal customerRefund;
    private BigDecimal amountToPay;
    private String payee;
    private BigDecimal unPaidClaim;

    public BigDecimal getUnPaidClaim() {
        return unPaidClaim;
    }

    public void setUnPaidClaim(BigDecimal unPaidClaim) {
        this.unPaidClaim = unPaidClaim;
    }
    
    

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public BigDecimal getContractPurchasePrice() {
        return contractPurchasePrice;
    }

    public void setContractPurchasePrice(BigDecimal contractPurchasePrice) {
        this.contractPurchasePrice = contractPurchasePrice;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelReceivedDate() {
        return cancelReceivedDate;
    }

    public void setCancelReceivedDate(String cancelReceivedDate) {
        this.cancelReceivedDate = cancelReceivedDate;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelMethod() {
        return cancelMethod;
    }

    public void setCancelMethod(String cancelMethod) {
        this.cancelMethod = cancelMethod;
    }

    public String getCancelMethodGroup() {
        return cancelMethodGroup;
    }

    public void setCancelMethodGroup(String cancelMethodGroup) {
        this.cancelMethodGroup = cancelMethodGroup;
    }

    public String getCancelGroupRuleMethod() {
        return cancelGroupRuleMethod;
    }

    public void setCancelGroupRuleMethod(String cancelGroupRuleMethod) {
        this.cancelGroupRuleMethod = cancelGroupRuleMethod;
    }

    public String getCancelGroupRuleType() {
        return cancelGroupRuleType;
    }

    public void setCancelGroupRuleType(String cancelGroupRuleType) {
        this.cancelGroupRuleType = cancelGroupRuleType;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public Integer getCurrentOdometer() {
        return currentOdometer;
    }

    public void setCurrentOdometer(Integer currentOdometer) {
        this.currentOdometer = currentOdometer;
    }

    public BigDecimal getRefundRatio() {
        return refundRatio;
    }

    public void setRefundRatio(BigDecimal refundRatio) {
        this.refundRatio = refundRatio;
    }

    public BigDecimal getCoverageCost() {
        return coverageCost;
    }

    public void setCoverageCost(BigDecimal coverageCost) {
        this.coverageCost = coverageCost;
    }

    

    public BigDecimal getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(BigDecimal adminFee) {
        this.adminFee = adminFee;
    }

    public BigDecimal getAdminRefund() {
        return adminRefund;
    }

    public void setAdminRefund(BigDecimal adminRefund) {
        this.adminRefund = adminRefund;
    }

    public BigDecimal getAdminPortion() {
        return adminPortion;
    }

    public void setAdminPortion(BigDecimal adminPortion) {
        this.adminPortion = adminPortion;
    }

    public BigDecimal getDealerProfit() {
        return dealerProfit;
    }

    public void setDealerProfit(BigDecimal dealerProfit) {
        this.dealerProfit = dealerProfit;
    }

    public BigDecimal getDealerRetainedFee() {
        return dealerRetainedFee;
    }

    public void setDealerRetainedFee(BigDecimal dealerRetainedFee) {
        this.dealerRetainedFee = dealerRetainedFee;
    }

    public BigDecimal getDpRefund() {
        return dpRefund;
    }

    public void setDpRefund(BigDecimal dpRefund) {
        this.dpRefund = dpRefund;
    }

    public BigDecimal getDealerPortion() {
        return dealerPortion;
    }

    public void setDealerPortion(BigDecimal dealerPortion) {
        this.dealerPortion = dealerPortion;
    }

    public BigDecimal getCustomerRefund() {
        return customerRefund;
    }

    public void setCustomerRefund(BigDecimal customerRefund) {
        this.customerRefund = customerRefund;
    }

    public BigDecimal getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(BigDecimal amountToPay) {
        this.amountToPay = amountToPay;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public void reset() {
        contractNo = "";
        product = "";
        plan = "";
        effectiveDate = "";
        expiredDate = "";
        customer = "";
        contractPurchasePrice = null;
        cancelDate = "";
        cancelReceivedDate = "";
        fee = null;
        cancelReason = "";
        cancelMethod = "";
        cancelMethodGroup = "";
        cancelGroupRuleMethod = "";
        cancelGroupRuleType = "";
        validTo = "";
        currentOdometer = null;
        refundRatio = null;
        coverageCost = null;
        adminFee = null;
        adminRefund = null;
        adminPortion = null;
        dealerProfit = null;
        dealerRetainedFee = null;
        dpRefund = null;
        dealerPortion = null;
        customerRefund = null;
        amountToPay = null;
        payee = "";
        unPaidClaim = null;
    }
}
