/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ProductType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductType.findAll", query = "SELECT p FROM ProductType p")
    , @NamedQuery(name = "ProductType.findByProductTypeId", query = "SELECT p FROM ProductType p WHERE p.productTypeId = :productTypeId")
    , @NamedQuery(name = "ProductType.findByProductTypeName", query = "SELECT p FROM ProductType p WHERE p.productTypeName = :productTypeName")
    , @NamedQuery(name = "ProductType.findByDescription", query = "SELECT p FROM ProductType p WHERE p.description = :description")})
public class ProductType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "productTypeId")
    private Integer productTypeId;
    @Size(max = 20)
    @Column(name = "productTypeName")
    private String productTypeName;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "productTypeIdFk")
    private Collection<Product> productCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productTypeIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "productTypeIdFk")
    private Collection<TermDetail> termDetailCollection;
    @OneToMany(mappedBy = "productTypeIdFk")
    private Collection<LimitDetail> limitDetailCollection;
    @OneToMany(mappedBy = "productTypeIdFk")
    private Collection<CfCancelMethod> cfCancelMethodCollection;
    @OneToMany(mappedBy = "productTypeIdFk")
    private Collection<Claim> claimCollection;

    public ProductType() {
    }

    public ProductType(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Product> getProductCollection() {
        return productCollection;
    }

    public void setProductCollection(Collection<Product> productCollection) {
        this.productCollection = productCollection;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<TermDetail> getTermDetailCollection() {
        return termDetailCollection;
    }

    public void setTermDetailCollection(Collection<TermDetail> termDetailCollection) {
        this.termDetailCollection = termDetailCollection;
    }

    @XmlTransient
    public Collection<LimitDetail> getLimitDetailCollection() {
        return limitDetailCollection;
    }

    public void setLimitDetailCollection(Collection<LimitDetail> limitDetailCollection) {
        this.limitDetailCollection = limitDetailCollection;
    }

    @XmlTransient
    public Collection<CfCancelMethod> getCfCancelMethodCollection() {
        return cfCancelMethodCollection;
    }

    public void setCfCancelMethodCollection(Collection<CfCancelMethod> cfCancelMethodCollection) {
        this.cfCancelMethodCollection = cfCancelMethodCollection;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productTypeId != null ? productTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductType)) {
            return false;
        }
        ProductType other = (ProductType) object;
        if ((this.productTypeId == null && other.productTypeId != null) || (this.productTypeId != null && !this.productTypeId.equals(other.productTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProductType[ productTypeId=" + productTypeId + " ]";
    }
    
}
