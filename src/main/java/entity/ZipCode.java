/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ZipCode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ZipCode.findAll", query = "SELECT z FROM ZipCode z")
    , @NamedQuery(name = "ZipCode.findByZipCodeId", query = "SELECT z FROM ZipCode z WHERE z.zipCodeId = :zipCodeId")
    , @NamedQuery(name = "ZipCode.findByZipCode", query = "SELECT z FROM ZipCode z WHERE z.zipCode = :zipCode")
    , @NamedQuery(name = "ZipCode.findByCity", query = "SELECT z FROM ZipCode z WHERE z.city = :city")
    , @NamedQuery(name = "ZipCode.findByState", query = "SELECT z FROM ZipCode z WHERE z.state = :state")
    , @NamedQuery(name = "ZipCode.findByCounty", query = "SELECT z FROM ZipCode z WHERE z.county = :county")
    , @NamedQuery(name = "ZipCode.findByAreaCode", query = "SELECT z FROM ZipCode z WHERE z.areaCode = :areaCode")
    , @NamedQuery(name = "ZipCode.findByCityType", query = "SELECT z FROM ZipCode z WHERE z.cityType = :cityType")
    , @NamedQuery(name = "ZipCode.findByCityAliasAbbreviation", query = "SELECT z FROM ZipCode z WHERE z.cityAliasAbbreviation = :cityAliasAbbreviation")
    , @NamedQuery(name = "ZipCode.findByCityAliasName", query = "SELECT z FROM ZipCode z WHERE z.cityAliasName = :cityAliasName")
    , @NamedQuery(name = "ZipCode.findByLatitude", query = "SELECT z FROM ZipCode z WHERE z.latitude = :latitude")
    , @NamedQuery(name = "ZipCode.findByLongitude", query = "SELECT z FROM ZipCode z WHERE z.longitude = :longitude")
    , @NamedQuery(name = "ZipCode.findByTimeZone", query = "SELECT z FROM ZipCode z WHERE z.timeZone = :timeZone")
    , @NamedQuery(name = "ZipCode.findByElevation", query = "SELECT z FROM ZipCode z WHERE z.elevation = :elevation")
    , @NamedQuery(name = "ZipCode.findByCountyFIPS", query = "SELECT z FROM ZipCode z WHERE z.countyFIPS = :countyFIPS")
    , @NamedQuery(name = "ZipCode.findByDayLightSaving", query = "SELECT z FROM ZipCode z WHERE z.dayLightSaving = :dayLightSaving")
    , @NamedQuery(name = "ZipCode.findByPreferredLastLineKey", query = "SELECT z FROM ZipCode z WHERE z.preferredLastLineKey = :preferredLastLineKey")
    , @NamedQuery(name = "ZipCode.findByClassificationCode", query = "SELECT z FROM ZipCode z WHERE z.classificationCode = :classificationCode")
    , @NamedQuery(name = "ZipCode.findByMultiCounty", query = "SELECT z FROM ZipCode z WHERE z.multiCounty = :multiCounty")
    , @NamedQuery(name = "ZipCode.findByStateFIPS", query = "SELECT z FROM ZipCode z WHERE z.stateFIPS = :stateFIPS")
    , @NamedQuery(name = "ZipCode.findByCityStateKey", query = "SELECT z FROM ZipCode z WHERE z.cityStateKey = :cityStateKey")
    , @NamedQuery(name = "ZipCode.findByCityAliasCode", query = "SELECT z FROM ZipCode z WHERE z.cityAliasCode = :cityAliasCode")
    , @NamedQuery(name = "ZipCode.findByPrimaryRecord", query = "SELECT z FROM ZipCode z WHERE z.primaryRecord = :primaryRecord")
    , @NamedQuery(name = "ZipCode.findByCityMixedCase", query = "SELECT z FROM ZipCode z WHERE z.cityMixedCase = :cityMixedCase")
    , @NamedQuery(name = "ZipCode.findByCityAliasMixedCase", query = "SELECT z FROM ZipCode z WHERE z.cityAliasMixedCase = :cityAliasMixedCase")
    , @NamedQuery(name = "ZipCode.findByStateANSI", query = "SELECT z FROM ZipCode z WHERE z.stateANSI = :stateANSI")
    , @NamedQuery(name = "ZipCode.findByCountyANSI", query = "SELECT z FROM ZipCode z WHERE z.countyANSI = :countyANSI")
    , @NamedQuery(name = "ZipCode.findByFacilityCode", query = "SELECT z FROM ZipCode z WHERE z.facilityCode = :facilityCode")
    , @NamedQuery(name = "ZipCode.findByCityDeliveryIndicator", query = "SELECT z FROM ZipCode z WHERE z.cityDeliveryIndicator = :cityDeliveryIndicator")
    , @NamedQuery(name = "ZipCode.findByCarrierRouteRateSortation", query = "SELECT z FROM ZipCode z WHERE z.carrierRouteRateSortation = :carrierRouteRateSortation")
    , @NamedQuery(name = "ZipCode.findByFinanceNumber", query = "SELECT z FROM ZipCode z WHERE z.financeNumber = :financeNumber")
    , @NamedQuery(name = "ZipCode.findByUniqueZIPName", query = "SELECT z FROM ZipCode z WHERE z.uniqueZIPName = :uniqueZIPName")
    , @NamedQuery(name = "ZipCode.findByUpdateUserName", query = "SELECT z FROM ZipCode z WHERE z.updateUserName = :updateUserName")
    , @NamedQuery(name = "ZipCode.findByUpdateLast", query = "SELECT z FROM ZipCode z WHERE z.updateLast = :updateLast")})
public class ZipCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "zipCodeId")
    private Integer zipCodeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "zipCode")
    private String zipCode;
    @Size(max = 35)
    @Column(name = "city")
    private String city;
    @Size(max = 2)
    @Column(name = "state")
    private String state;
    @Size(max = 45)
    @Column(name = "county")
    private String county;
    @Size(max = 55)
    @Column(name = "areaCode")
    private String areaCode;
    @Column(name = "cityType")
    private Character cityType;
    @Size(max = 13)
    @Column(name = "cityAliasAbbreviation")
    private String cityAliasAbbreviation;
    @Size(max = 35)
    @Column(name = "cityAliasName")
    private String cityAliasName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private BigDecimal latitude;
    @Column(name = "longitude")
    private BigDecimal longitude;
    @Size(max = 2)
    @Column(name = "timeZone")
    private String timeZone;
    @Column(name = "elevation")
    private Integer elevation;
    @Size(max = 5)
    @Column(name = "countyFIPS")
    private String countyFIPS;
    @Column(name = "dayLightSaving")
    private Character dayLightSaving;
    @Size(max = 10)
    @Column(name = "preferredLastLineKey")
    private String preferredLastLineKey;
    @Column(name = "classificationCode")
    private Character classificationCode;
    @Column(name = "multiCounty")
    private Character multiCounty;
    @Size(max = 2)
    @Column(name = "stateFIPS")
    private String stateFIPS;
    @Size(max = 6)
    @Column(name = "cityStateKey")
    private String cityStateKey;
    @Size(max = 5)
    @Column(name = "cityAliasCode")
    private String cityAliasCode;
    @Column(name = "primaryRecord")
    private Character primaryRecord;
    @Size(max = 35)
    @Column(name = "cityMixedCase")
    private String cityMixedCase;
    @Size(max = 35)
    @Column(name = "cityAliasMixedCase")
    private String cityAliasMixedCase;
    @Size(max = 2)
    @Column(name = "stateANSI")
    private String stateANSI;
    @Size(max = 3)
    @Column(name = "countyANSI")
    private String countyANSI;
    @Size(max = 1)
    @Column(name = "facilityCode")
    private String facilityCode;
    @Size(max = 1)
    @Column(name = "cityDeliveryIndicator")
    private String cityDeliveryIndicator;
    @Size(max = 1)
    @Column(name = "carrierRouteRateSortation")
    private String carrierRouteRateSortation;
    @Size(max = 6)
    @Column(name = "financeNumber")
    private String financeNumber;
    @Size(max = 1)
    @Column(name = "uniqueZIPName")
    private String uniqueZIPName;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;

    public ZipCode() {
    }

    public ZipCode(Integer zipCodeId) {
        this.zipCodeId = zipCodeId;
    }

    public ZipCode(Integer zipCodeId, String zipCode) {
        this.zipCodeId = zipCodeId;
        this.zipCode = zipCode;
    }

    public Integer getZipCodeId() {
        return zipCodeId;
    }

    public void setZipCodeId(Integer zipCodeId) {
        this.zipCodeId = zipCodeId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Character getCityType() {
        return cityType;
    }

    public void setCityType(Character cityType) {
        this.cityType = cityType;
    }

    public String getCityAliasAbbreviation() {
        return cityAliasAbbreviation;
    }

    public void setCityAliasAbbreviation(String cityAliasAbbreviation) {
        this.cityAliasAbbreviation = cityAliasAbbreviation;
    }

    public String getCityAliasName() {
        return cityAliasName;
    }

    public void setCityAliasName(String cityAliasName) {
        this.cityAliasName = cityAliasName;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public String getCountyFIPS() {
        return countyFIPS;
    }

    public void setCountyFIPS(String countyFIPS) {
        this.countyFIPS = countyFIPS;
    }

    public Character getDayLightSaving() {
        return dayLightSaving;
    }

    public void setDayLightSaving(Character dayLightSaving) {
        this.dayLightSaving = dayLightSaving;
    }

    public String getPreferredLastLineKey() {
        return preferredLastLineKey;
    }

    public void setPreferredLastLineKey(String preferredLastLineKey) {
        this.preferredLastLineKey = preferredLastLineKey;
    }

    public Character getClassificationCode() {
        return classificationCode;
    }

    public void setClassificationCode(Character classificationCode) {
        this.classificationCode = classificationCode;
    }

    public Character getMultiCounty() {
        return multiCounty;
    }

    public void setMultiCounty(Character multiCounty) {
        this.multiCounty = multiCounty;
    }

    public String getStateFIPS() {
        return stateFIPS;
    }

    public void setStateFIPS(String stateFIPS) {
        this.stateFIPS = stateFIPS;
    }

    public String getCityStateKey() {
        return cityStateKey;
    }

    public void setCityStateKey(String cityStateKey) {
        this.cityStateKey = cityStateKey;
    }

    public String getCityAliasCode() {
        return cityAliasCode;
    }

    public void setCityAliasCode(String cityAliasCode) {
        this.cityAliasCode = cityAliasCode;
    }

    public Character getPrimaryRecord() {
        return primaryRecord;
    }

    public void setPrimaryRecord(Character primaryRecord) {
        this.primaryRecord = primaryRecord;
    }

    public String getCityMixedCase() {
        return cityMixedCase;
    }

    public void setCityMixedCase(String cityMixedCase) {
        this.cityMixedCase = cityMixedCase;
    }

    public String getCityAliasMixedCase() {
        return cityAliasMixedCase;
    }

    public void setCityAliasMixedCase(String cityAliasMixedCase) {
        this.cityAliasMixedCase = cityAliasMixedCase;
    }

    public String getStateANSI() {
        return stateANSI;
    }

    public void setStateANSI(String stateANSI) {
        this.stateANSI = stateANSI;
    }

    public String getCountyANSI() {
        return countyANSI;
    }

    public void setCountyANSI(String countyANSI) {
        this.countyANSI = countyANSI;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getCityDeliveryIndicator() {
        return cityDeliveryIndicator;
    }

    public void setCityDeliveryIndicator(String cityDeliveryIndicator) {
        this.cityDeliveryIndicator = cityDeliveryIndicator;
    }

    public String getCarrierRouteRateSortation() {
        return carrierRouteRateSortation;
    }

    public void setCarrierRouteRateSortation(String carrierRouteRateSortation) {
        this.carrierRouteRateSortation = carrierRouteRateSortation;
    }

    public String getFinanceNumber() {
        return financeNumber;
    }

    public void setFinanceNumber(String financeNumber) {
        this.financeNumber = financeNumber;
    }

    public String getUniqueZIPName() {
        return uniqueZIPName;
    }

    public void setUniqueZIPName(String uniqueZIPName) {
        this.uniqueZIPName = uniqueZIPName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zipCodeId != null ? zipCodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZipCode)) {
            return false;
        }
        ZipCode other = (ZipCode) object;
        if ((this.zipCodeId == null && other.zipCodeId != null) || (this.zipCodeId != null && !this.zipCodeId.equals(other.zipCodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ZipCode[ zipCodeId=" + zipCodeId + " ]";
    }
    
}
