/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfUserWidgetGroupConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfUserWidgetGroupConn.findAll", query = "SELECT c FROM CfUserWidgetGroupConn c")
    , @NamedQuery(name = "CfUserWidgetGroupConn.findByUserWidgetGroupConnId", query = "SELECT c FROM CfUserWidgetGroupConn c WHERE c.userWidgetGroupConnId = :userWidgetGroupConnId")
    , @NamedQuery(name = "CfUserWidgetGroupConn.findByUpdateUserName", query = "SELECT c FROM CfUserWidgetGroupConn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfUserWidgetGroupConn.findByUpdateLast", query = "SELECT c FROM CfUserWidgetGroupConn c WHERE c.updateLast = :updateLast")})
public class CfUserWidgetGroupConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userWidgetGroupConnId")
    private Integer userWidgetGroupConnId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "widgetGroupId", referencedColumnName = "widgetGroupId")
    @ManyToOne(optional = false)
    private CfWidgetGroup widgetGroupId;
    @JoinColumn(name = "userMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public CfUserWidgetGroupConn() {
    }

    public CfUserWidgetGroupConn(Integer userWidgetGroupConnId) {
        this.userWidgetGroupConnId = userWidgetGroupConnId;
    }

    public Integer getUserWidgetGroupConnId() {
        return userWidgetGroupConnId;
    }

    public void setUserWidgetGroupConnId(Integer userWidgetGroupConnId) {
        this.userWidgetGroupConnId = userWidgetGroupConnId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfWidgetGroup getWidgetGroupId() {
        return widgetGroupId;
    }

    public void setWidgetGroupId(CfWidgetGroup widgetGroupId) {
        this.widgetGroupId = widgetGroupId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userWidgetGroupConnId != null ? userWidgetGroupConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfUserWidgetGroupConn)) {
            return false;
        }
        CfUserWidgetGroupConn other = (CfUserWidgetGroupConn) object;
        if ((this.userWidgetGroupConnId == null && other.userWidgetGroupConnId != null) || (this.userWidgetGroupConnId != null && !this.userWidgetGroupConnId.equals(other.userWidgetGroupConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfUserWidgetGroupConn[ userWidgetGroupConnId=" + userWidgetGroupConnId + " ]";
    }
    
}
