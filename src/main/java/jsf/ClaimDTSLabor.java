/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Id;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimDTSLabor {
    
   private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public ClaimDTSLabor() {
    }



    public ClaimDTSLabor(Integer detailItemId, String updateUserName, String updateLast, BigDecimal laborTime, String srtCodeLaborOp, String laborDesc, BigDecimal LaborRate, BigDecimal laborSubTotal, List<ClaimDTSPart> claimDTSPart) {
        this.detailItemId = detailItemId;
        this.updateUserName = updateUserName;
        this.updateLast = updateLast;
        this.laborTime = laborTime;
        this.srtCodeLaborOp = srtCodeLaborOp;
        this.laborDesc = laborDesc;
        this.LaborRate = LaborRate;
        this.laborSubTotal = laborSubTotal;
        this.claimDTSPart = claimDTSPart;
    }
    
    
    
    
    @Id
    @Column(name="detailItemId", table="ClaimDetailItem")
    private Integer detailItemId;
    @Column(name = "updateUserName", table="ClaimLabor")
    private String updateUserName;
    @Column(name = "updateLast", table="ClaimLabor")
    private String updateLast;
    @Column(name = "laborTime", table="ClaimLabor")
    private BigDecimal laborTime;
    @Column(name = "srtCodeLaborOp", table="ClaimDetailItem")
    private String srtCodeLaborOp;
    @Column(name = "laborDesc", table="ClaimDetailItem")
    private String laborDesc;
    @Column(name = "LaborRate", table="ClaimLabor")
    private BigDecimal LaborRate;
    BigDecimal laborSubTotal;   // sum of part's subTotal
    List<ClaimDTSPart> claimDTSPart;
    
    public void reset() {
        this.detailItemId = null;
        this.updateUserName = null;
        this.updateLast = null;
        this.laborTime = null;
        this.srtCodeLaborOp = null;
        this.laborDesc = null;
        this.LaborRate = null;
        this.laborSubTotal = null;
        this.claimDTSPart = null;
    }

    public Integer getDetailItemId() {
        return detailItemId;
    }

    public void setDetailItemId(Integer detailItemId) {
        this.detailItemId = detailItemId;
    }
    
    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public String getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(String updateLast) {
        this.updateLast = updateLast;
    }

    public BigDecimal getLaborTime() {
        return laborTime;
    }

    public String getSrtCodeLaborOp() {
        return srtCodeLaborOp;
    }

    public void setSrtCodeLaborOp(String srtCodeLaborOp) {
        this.srtCodeLaborOp = srtCodeLaborOp;
    }
    
    public void setLaborTime(BigDecimal laborTime) {
        this.laborTime = laborTime;
    }

    public String getLaborDesc() {
        return laborDesc;
    }

    public void setLaborDesc(String laborDesc) {
        this.laborDesc = laborDesc;
    }

    public BigDecimal getLaborRate() {
        return LaborRate;
    }

    public void setLaborRate(BigDecimal LaborRate) {
        this.LaborRate = LaborRate;
    }

    public BigDecimal getLaborSubTotal() {
        return laborSubTotal;
    }

    public void setLaborSubTotal(BigDecimal laborSubTotal) {
        this.laborSubTotal = laborSubTotal;
    }

    public List<ClaimDTSPart> getClaimDTSPart() {
        return claimDTSPart;
    }

    public void setClaimDTSPart(List<ClaimDTSPart> claimDTSPart) {
        this.claimDTSPart = claimDTSPart;
    }
    
    

}
