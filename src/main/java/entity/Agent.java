/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Agent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a")
    , @NamedQuery(name = "Agent.findByAgentId", query = "SELECT a FROM Agent a WHERE a.agentId = :agentId")
    , @NamedQuery(name = "Agent.findByAgentNumber", query = "SELECT a FROM Agent a WHERE a.agentNumber = :agentNumber")
    , @NamedQuery(name = "Agent.findByUpdateUserName", query = "SELECT a FROM Agent a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "Agent.findByUpdateLast", query = "SELECT a FROM Agent a WHERE a.updateLast = :updateLast")})
public class Agent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "agentId")
    private Integer agentId;
    @Size(max = 50)
    @Column(name = "agentNumber")
    private String agentNumber;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "agentIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "agentIdFk")
    private Collection<UserMember> userMemberCollection;
    @JoinColumn(name = "agentId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @JoinColumn(name = "DisbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @OneToMany(mappedBy = "directAgentConditionIdFk")
    private Collection<Disbursement> disbursementCollection;
    @OneToMany(mappedBy = "agentIdFk")
    private Collection<Dealer> dealerCollection;

    public Agent() {
    }

    public Agent(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<UserMember> getUserMemberCollection() {
        return userMemberCollection;
    }

    public void setUserMemberCollection(Collection<UserMember> userMemberCollection) {
        this.userMemberCollection = userMemberCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    @XmlTransient
    public Collection<Disbursement> getDisbursementCollection() {
        return disbursementCollection;
    }

    public void setDisbursementCollection(Collection<Disbursement> disbursementCollection) {
        this.disbursementCollection = disbursementCollection;
    }

    @XmlTransient
    public Collection<Dealer> getDealerCollection() {
        return dealerCollection;
    }

    public void setDealerCollection(Collection<Dealer> dealerCollection) {
        this.dealerCollection = dealerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agentId != null ? agentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agent)) {
            return false;
        }
        Agent other = (Agent) object;
        if ((this.agentId == null && other.agentId != null) || (this.agentId != null && !this.agentId.equals(other.agentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Agent[ agentId=" + agentId + " ]";
    }
    
}
