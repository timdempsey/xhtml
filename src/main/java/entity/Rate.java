/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Rate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rate.findAll", query = "SELECT r FROM Rate r")
    , @NamedQuery(name = "Rate.findByRateId", query = "SELECT r FROM Rate r WHERE r.rateId = :rateId")})
public class Rate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RateId")
    private Integer rateId;
    @OneToMany(mappedBy = "rateIdFk")
    private Collection<PPMEventRate> pPMEventRateCollection;
    @JoinColumn(name = "limitIdFk", referencedColumnName = "limitId")
    @ManyToOne
    private Limit limitIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @JoinColumn(name = "termIdFk", referencedColumnName = "TermId")
    @ManyToOne
    private Term termIdFk;
    @JoinColumn(name = "classCodeIdFk", referencedColumnName = "vinClassCodeId")
    @ManyToOne
    private VINClassCode classCodeIdFk;
    @OneToMany(mappedBy = "rateIdFk")
    private Collection<RateDetail> rateDetailCollection;

    public Rate() {
    }

    public Rate(Integer rateId) {
        this.rateId = rateId;
    }

    public Integer getRateId() {
        return rateId;
    }

    public void setRateId(Integer rateId) {
        this.rateId = rateId;
    }

    @XmlTransient
    public Collection<PPMEventRate> getPPMEventRateCollection() {
        return pPMEventRateCollection;
    }

    public void setPPMEventRateCollection(Collection<PPMEventRate> pPMEventRateCollection) {
        this.pPMEventRateCollection = pPMEventRateCollection;
    }

    public Limit getLimitIdFk() {
        return limitIdFk;
    }

    public void setLimitIdFk(Limit limitIdFk) {
        this.limitIdFk = limitIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public Term getTermIdFk() {
        return termIdFk;
    }

    public void setTermIdFk(Term termIdFk) {
        this.termIdFk = termIdFk;
    }

    public VINClassCode getClassCodeIdFk() {
        return classCodeIdFk;
    }

    public void setClassCodeIdFk(VINClassCode classCodeIdFk) {
        this.classCodeIdFk = classCodeIdFk;
    }

    @XmlTransient
    public Collection<RateDetail> getRateDetailCollection() {
        return rateDetailCollection;
    }

    public void setRateDetailCollection(Collection<RateDetail> rateDetailCollection) {
        this.rateDetailCollection = rateDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateId != null ? rateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rate)) {
            return false;
        }
        Rate other = (Rate) object;
        if ((this.rateId == null && other.rateId != null) || (this.rateId != null && !this.rateId.equals(other.rateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Rate[ rateId=" + rateId + " ]";
    }
    
}
