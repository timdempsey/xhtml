/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Product")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
    , @NamedQuery(name = "Product.findByProductId", query = "SELECT p FROM Product p WHERE p.productId = :productId")
    , @NamedQuery(name = "Product.findByProductName", query = "SELECT p FROM Product p WHERE p.productName = :productName")
    , @NamedQuery(name = "Product.findByProductCode", query = "SELECT p FROM Product p WHERE p.productCode = :productCode")
    , @NamedQuery(name = "Product.findByProductSubTypeFk", query = "SELECT p FROM Product p WHERE p.productSubTypeFk = :productSubTypeFk")
    , @NamedQuery(name = "Product.findByProductUsageTypeFk", query = "SELECT p FROM Product p WHERE p.productUsageTypeFk = :productUsageTypeFk")
    , @NamedQuery(name = "Product.findByUpdateUserName", query = "SELECT p FROM Product p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "Product.findByUpdateLast", query = "SELECT p FROM Product p WHERE p.updateLast = :updateLast")
    , @NamedQuery(name = "Product.findByFinanceLengthBasedTermInd", query = "SELECT p FROM Product p WHERE p.financeLengthBasedTermInd = :financeLengthBasedTermInd")
    , @NamedQuery(name = "Product.findByMileageBasedTermInd", query = "SELECT p FROM Product p WHERE p.mileageBasedTermInd = :mileageBasedTermInd")
    , @NamedQuery(name = "Product.findByRenewableInd", query = "SELECT p FROM Product p WHERE p.renewableInd = :renewableInd")})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "productId")
    private Integer productId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "productName")
    private String productName;
    @Size(max = 50)
    @Column(name = "productCode")
    private String productCode;
    @Column(name = "productSubTypeFk")
    private Integer productSubTypeFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ProductUsageTypeFk")
    private int productUsageTypeFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @Basic(optional = false)
    @NotNull
    @Column(name = "financeLengthBasedTermInd")
    private boolean financeLengthBasedTermInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mileageBasedTermInd")
    private boolean mileageBasedTermInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renewableInd")
    private boolean renewableInd;
    @JoinColumn(name = "contractFormIdFk", referencedColumnName = "contractFormId")
    @ManyToOne
    private ContractForm contractFormIdFk;
    @JoinColumn(name = "insurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne(optional = false)
    private Insurer insurerIdFk;
    @JoinColumn(name = "productTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne
    private ProductType productTypeIdFk;
    @JoinColumn(name = "programIdFk", referencedColumnName = "programId")
    @ManyToOne(optional = false)
    private Program programIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<PlanTable> planTableCollection;
    @OneToMany(mappedBy = "productIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<PPMProductService> pPMProductServiceCollection;
    @OneToMany(mappedBy = "productIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "productIdFk")
    private Collection<RateBasedRule> rateBasedRuleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<Deductible> deductibleCollection;
    @OneToMany(mappedBy = "productIdFk")
    private Collection<Markup> markupCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<CfContractEntryField> cfContractEntryFieldCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<Adjustment> adjustmentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<ProductDetail> productDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productIdFk")
    private Collection<Surcharge> surchargeCollection;

    public Product() {
    }

    public Product(Integer productId) {
        this.productId = productId;
    }

    public Product(Integer productId, String productName, int productUsageTypeFk, boolean financeLengthBasedTermInd, boolean mileageBasedTermInd, boolean renewableInd) {
        this.productId = productId;
        this.productName = productName;
        this.productUsageTypeFk = productUsageTypeFk;
        this.financeLengthBasedTermInd = financeLengthBasedTermInd;
        this.mileageBasedTermInd = mileageBasedTermInd;
        this.renewableInd = renewableInd;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getProductSubTypeFk() {
        return productSubTypeFk;
    }

    public void setProductSubTypeFk(Integer productSubTypeFk) {
        this.productSubTypeFk = productSubTypeFk;
    }

    public int getProductUsageTypeFk() {
        return productUsageTypeFk;
    }

    public void setProductUsageTypeFk(int productUsageTypeFk) {
        this.productUsageTypeFk = productUsageTypeFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public boolean getFinanceLengthBasedTermInd() {
        return financeLengthBasedTermInd;
    }

    public void setFinanceLengthBasedTermInd(boolean financeLengthBasedTermInd) {
        this.financeLengthBasedTermInd = financeLengthBasedTermInd;
    }

    public boolean getMileageBasedTermInd() {
        return mileageBasedTermInd;
    }

    public void setMileageBasedTermInd(boolean mileageBasedTermInd) {
        this.mileageBasedTermInd = mileageBasedTermInd;
    }

    public boolean getRenewableInd() {
        return renewableInd;
    }

    public void setRenewableInd(boolean renewableInd) {
        this.renewableInd = renewableInd;
    }

    public ContractForm getContractFormIdFk() {
        return contractFormIdFk;
    }

    public void setContractFormIdFk(ContractForm contractFormIdFk) {
        this.contractFormIdFk = contractFormIdFk;
    }

    public Insurer getInsurerIdFk() {
        return insurerIdFk;
    }

    public void setInsurerIdFk(Insurer insurerIdFk) {
        this.insurerIdFk = insurerIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public Program getProgramIdFk() {
        return programIdFk;
    }

    public void setProgramIdFk(Program programIdFk) {
        this.programIdFk = programIdFk;
    }

    @XmlTransient
    public Collection<PlanTable> getPlanTableCollection() {
        return planTableCollection;
    }

    public void setPlanTableCollection(Collection<PlanTable> planTableCollection) {
        this.planTableCollection = planTableCollection;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<PPMProductService> getPPMProductServiceCollection() {
        return pPMProductServiceCollection;
    }

    public void setPPMProductServiceCollection(Collection<PPMProductService> pPMProductServiceCollection) {
        this.pPMProductServiceCollection = pPMProductServiceCollection;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<RateBasedRule> getRateBasedRuleCollection() {
        return rateBasedRuleCollection;
    }

    public void setRateBasedRuleCollection(Collection<RateBasedRule> rateBasedRuleCollection) {
        this.rateBasedRuleCollection = rateBasedRuleCollection;
    }

    @XmlTransient
    public Collection<Deductible> getDeductibleCollection() {
        return deductibleCollection;
    }

    public void setDeductibleCollection(Collection<Deductible> deductibleCollection) {
        this.deductibleCollection = deductibleCollection;
    }

    @XmlTransient
    public Collection<Markup> getMarkupCollection() {
        return markupCollection;
    }

    public void setMarkupCollection(Collection<Markup> markupCollection) {
        this.markupCollection = markupCollection;
    }

    @XmlTransient
    public Collection<CfContractEntryField> getCfContractEntryFieldCollection() {
        return cfContractEntryFieldCollection;
    }

    public void setCfContractEntryFieldCollection(Collection<CfContractEntryField> cfContractEntryFieldCollection) {
        this.cfContractEntryFieldCollection = cfContractEntryFieldCollection;
    }

    @XmlTransient
    public Collection<Adjustment> getAdjustmentCollection() {
        return adjustmentCollection;
    }

    public void setAdjustmentCollection(Collection<Adjustment> adjustmentCollection) {
        this.adjustmentCollection = adjustmentCollection;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection() {
        return productDetailCollection;
    }

    public void setProductDetailCollection(Collection<ProductDetail> productDetailCollection) {
        this.productDetailCollection = productDetailCollection;
    }

    @XmlTransient
    public Collection<Surcharge> getSurchargeCollection() {
        return surchargeCollection;
    }

    public void setSurchargeCollection(Collection<Surcharge> surchargeCollection) {
        this.surchargeCollection = surchargeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Product[ productId=" + productId + " ]";
    }
    
}
