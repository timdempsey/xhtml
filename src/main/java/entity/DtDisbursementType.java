/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtDisbursementType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtDisbursementType.findAll", query = "SELECT d FROM DtDisbursementType d")
    , @NamedQuery(name = "DtDisbursementType.findByDisbursementTypeId", query = "SELECT d FROM DtDisbursementType d WHERE d.disbursementTypeId = :disbursementTypeId")
    , @NamedQuery(name = "DtDisbursementType.findByDescription", query = "SELECT d FROM DtDisbursementType d WHERE d.description = :description")})
public class DtDisbursementType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DisbursementTypeId")
    private Integer disbursementTypeId;
    @Size(max = 50)
    @Column(name = "Description")
    private String description;

    public DtDisbursementType() {
    }

    public DtDisbursementType(Integer disbursementTypeId) {
        this.disbursementTypeId = disbursementTypeId;
    }

    public Integer getDisbursementTypeId() {
        return disbursementTypeId;
    }

    public void setDisbursementTypeId(Integer disbursementTypeId) {
        this.disbursementTypeId = disbursementTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementTypeId != null ? disbursementTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtDisbursementType)) {
            return false;
        }
        DtDisbursementType other = (DtDisbursementType) object;
        if ((this.disbursementTypeId == null && other.disbursementTypeId != null) || (this.disbursementTypeId != null && !this.disbursementTypeId.equals(other.disbursementTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtDisbursementType[ disbursementTypeId=" + disbursementTypeId + " ]";
    }
    
}
