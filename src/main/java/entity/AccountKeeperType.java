/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AccountKeeperType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountKeeperType.findAll", query = "SELECT a FROM AccountKeeperType a")
    , @NamedQuery(name = "AccountKeeperType.findByAccountKeeperTypeId", query = "SELECT a FROM AccountKeeperType a WHERE a.accountKeeperTypeId = :accountKeeperTypeId")
    , @NamedQuery(name = "AccountKeeperType.findByAccountKeeperTypeDesc", query = "SELECT a FROM AccountKeeperType a WHERE a.accountKeeperTypeDesc = :accountKeeperTypeDesc")})
public class AccountKeeperType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AccountKeeperTypeId")
    private Integer accountKeeperTypeId;
    @Size(max = 50)
    @Column(name = "AccountKeeperTypeDesc")
    private String accountKeeperTypeDesc;
    @OneToMany(mappedBy = "accountKeeperTypeIdFk")
    private Collection<AccountKeeper> accountKeeperCollection;

    public AccountKeeperType() {
    }

    public AccountKeeperType(Integer accountKeeperTypeId) {
        this.accountKeeperTypeId = accountKeeperTypeId;
    }

    public Integer getAccountKeeperTypeId() {
        return accountKeeperTypeId;
    }

    public void setAccountKeeperTypeId(Integer accountKeeperTypeId) {
        this.accountKeeperTypeId = accountKeeperTypeId;
    }

    public String getAccountKeeperTypeDesc() {
        return accountKeeperTypeDesc;
    }

    public void setAccountKeeperTypeDesc(String accountKeeperTypeDesc) {
        this.accountKeeperTypeDesc = accountKeeperTypeDesc;
    }

    @XmlTransient
    public Collection<AccountKeeper> getAccountKeeperCollection() {
        return accountKeeperCollection;
    }

    public void setAccountKeeperCollection(Collection<AccountKeeper> accountKeeperCollection) {
        this.accountKeeperCollection = accountKeeperCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountKeeperTypeId != null ? accountKeeperTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountKeeperType)) {
            return false;
        }
        AccountKeeperType other = (AccountKeeperType) object;
        if ((this.accountKeeperTypeId == null && other.accountKeeperTypeId != null) || (this.accountKeeperTypeId != null && !this.accountKeeperTypeId.equals(other.accountKeeperTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AccountKeeperType[ accountKeeperTypeId=" + accountKeeperTypeId + " ]";
    }
    
}
