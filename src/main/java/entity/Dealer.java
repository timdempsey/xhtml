/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Dealer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dealer.findAll", query = "SELECT d FROM Dealer d")
    , @NamedQuery(name = "Dealer.findByDealerId", query = "SELECT d FROM Dealer d WHERE d.dealerId = :dealerId")
    , @NamedQuery(name = "Dealer.findByDealerNumber", query = "SELECT d FROM Dealer d WHERE d.dealerNumber = :dealerNumber")
    , @NamedQuery(name = "Dealer.findByProducerId", query = "SELECT d FROM Dealer d WHERE d.producerId = :producerId")
    , @NamedQuery(name = "Dealer.findByClientCodeIdFk", query = "SELECT d FROM Dealer d WHERE d.clientCodeIdFk = :clientCodeIdFk")
    , @NamedQuery(name = "Dealer.findByEffectiveDate", query = "SELECT d FROM Dealer d WHERE d.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "Dealer.findByUpdateUserName", query = "SELECT d FROM Dealer d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "Dealer.findByUpdateLast", query = "SELECT d FROM Dealer d WHERE d.updateLast = :updateLast")
    , @NamedQuery(name = "Dealer.findByDealerTypeIdFk", query = "SELECT d FROM Dealer d WHERE d.dealerTypeIdFk = :dealerTypeIdFk")})
public class Dealer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerId")
    private Integer dealerId;
    @Size(max = 50)
    @Column(name = "dealerNumber")
    private String dealerNumber;
    @Size(max = 50)
    @Column(name = "producerId")
    private String producerId;
    @Column(name = "clientCodeIdFk")
    private Integer clientCodeIdFk;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @Column(name = "dealerTypeIdFk")
    private Integer dealerTypeIdFk;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<Markup> markupCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<UserMember> userMemberCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<DealerRateVisibilityRel> dealerRateVisibilityRelCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<ReinsuranceDealer> reinsuranceDealerCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dealerId")
    private Collection<CfUserMemberAndDealer> cfUserMemberAndDealerCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dealerId")
    private Collection<RepairFacilityToDealerRel> repairFacilityToDealerRelCollection;
    @OneToMany(mappedBy = "dealerIdFk")
    private Collection<DealerSRP> dealerSRPCollection;
    @OneToMany(mappedBy = "dealerConditionIdFk")
    private Collection<Disbursement> disbursementCollection;
    @JoinColumn(name = "dealerId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @JoinColumn(name = "agentIdFk", referencedColumnName = "agentId")
    @ManyToOne
    private Agent agentIdFk;
    @JoinColumn(name = "dealerGroupIdFk", referencedColumnName = "dealerGroupId")
    @ManyToOne
    private DealerGroup dealerGroupIdFk;
    @JoinColumn(name = "DisbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "insurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer insurerIdFk;
    @JoinColumn(name = "rateVisibilityIdFk", referencedColumnName = "rateVisibilityId")
    @ManyToOne
    private RateVisibility rateVisibilityIdFk;

    public Dealer() {
    }

    public Dealer(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public Integer getClientCodeIdFk() {
        return clientCodeIdFk;
    }

    public void setClientCodeIdFk(Integer clientCodeIdFk) {
        this.clientCodeIdFk = clientCodeIdFk;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Integer getDealerTypeIdFk() {
        return dealerTypeIdFk;
    }

    public void setDealerTypeIdFk(Integer dealerTypeIdFk) {
        this.dealerTypeIdFk = dealerTypeIdFk;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<Markup> getMarkupCollection() {
        return markupCollection;
    }

    public void setMarkupCollection(Collection<Markup> markupCollection) {
        this.markupCollection = markupCollection;
    }

    @XmlTransient
    public Collection<UserMember> getUserMemberCollection() {
        return userMemberCollection;
    }

    public void setUserMemberCollection(Collection<UserMember> userMemberCollection) {
        this.userMemberCollection = userMemberCollection;
    }

    @XmlTransient
    public Collection<DealerRateVisibilityRel> getDealerRateVisibilityRelCollection() {
        return dealerRateVisibilityRelCollection;
    }

    public void setDealerRateVisibilityRelCollection(Collection<DealerRateVisibilityRel> dealerRateVisibilityRelCollection) {
        this.dealerRateVisibilityRelCollection = dealerRateVisibilityRelCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceDealer> getReinsuranceDealerCollection() {
        return reinsuranceDealerCollection;
    }

    public void setReinsuranceDealerCollection(Collection<ReinsuranceDealer> reinsuranceDealerCollection) {
        this.reinsuranceDealerCollection = reinsuranceDealerCollection;
    }

    @XmlTransient
    public Collection<CfUserMemberAndDealer> getCfUserMemberAndDealerCollection() {
        return cfUserMemberAndDealerCollection;
    }

    public void setCfUserMemberAndDealerCollection(Collection<CfUserMemberAndDealer> cfUserMemberAndDealerCollection) {
        this.cfUserMemberAndDealerCollection = cfUserMemberAndDealerCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceContractFee> getReinsuranceContractFeeCollection() {
        return reinsuranceContractFeeCollection;
    }

    public void setReinsuranceContractFeeCollection(Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection) {
        this.reinsuranceContractFeeCollection = reinsuranceContractFeeCollection;
    }

    @XmlTransient
    public Collection<RepairFacilityToDealerRel> getRepairFacilityToDealerRelCollection() {
        return repairFacilityToDealerRelCollection;
    }

    public void setRepairFacilityToDealerRelCollection(Collection<RepairFacilityToDealerRel> repairFacilityToDealerRelCollection) {
        this.repairFacilityToDealerRelCollection = repairFacilityToDealerRelCollection;
    }

    @XmlTransient
    public Collection<DealerSRP> getDealerSRPCollection() {
        return dealerSRPCollection;
    }

    public void setDealerSRPCollection(Collection<DealerSRP> dealerSRPCollection) {
        this.dealerSRPCollection = dealerSRPCollection;
    }

    @XmlTransient
    public Collection<Disbursement> getDisbursementCollection() {
        return disbursementCollection;
    }

    public void setDisbursementCollection(Collection<Disbursement> disbursementCollection) {
        this.disbursementCollection = disbursementCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    public Agent getAgentIdFk() {
        return agentIdFk;
    }

    public void setAgentIdFk(Agent agentIdFk) {
        this.agentIdFk = agentIdFk;
    }

    public DealerGroup getDealerGroupIdFk() {
        return dealerGroupIdFk;
    }

    public void setDealerGroupIdFk(DealerGroup dealerGroupIdFk) {
        this.dealerGroupIdFk = dealerGroupIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Insurer getInsurerIdFk() {
        return insurerIdFk;
    }

    public void setInsurerIdFk(Insurer insurerIdFk) {
        this.insurerIdFk = insurerIdFk;
    }

    public RateVisibility getRateVisibilityIdFk() {
        return rateVisibilityIdFk;
    }

    public void setRateVisibilityIdFk(RateVisibility rateVisibilityIdFk) {
        this.rateVisibilityIdFk = rateVisibilityIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dealerId != null ? dealerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dealer)) {
            return false;
        }
        Dealer other = (Dealer) object;
        if ((this.dealerId == null && other.dealerId != null) || (this.dealerId != null && !this.dealerId.equals(other.dealerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Dealer[ dealerId=" + dealerId + " ]";
    }
    
}
