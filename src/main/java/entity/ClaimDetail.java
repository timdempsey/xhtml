/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimDetail.findAll", query = "SELECT c FROM ClaimDetail c")
    , @NamedQuery(name = "ClaimDetail.findByClaimDetailId", query = "SELECT c FROM ClaimDetail c WHERE c.claimDetailId = :claimDetailId")})
public class ClaimDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimDetailId")
    private Integer claimDetailId;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "claimDetailSubIdFk", referencedColumnName = "claimDetailSubId")
    @ManyToOne
    private ClaimDetailSub claimDetailSubIdFk;
    @OneToMany(mappedBy = "claimDetailIdFk")
    private Collection<ClaimMiscPayment> claimMiscPaymentCollection;
    @OneToMany(mappedBy = "claimDetailIdFk")
    private Collection<ClaimDetailSub> claimDetailSubCollection;
    @OneToMany(mappedBy = "claimDetailIdFk")
    private Collection<ClaimDetailItem> claimDetailItemCollection;

    public ClaimDetail() {
    }

    public ClaimDetail(Integer claimDetailId) {
        this.claimDetailId = claimDetailId;
    }

    public Integer getClaimDetailId() {
        return claimDetailId;
    }

    public void setClaimDetailId(Integer claimDetailId) {
        this.claimDetailId = claimDetailId;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public ClaimDetailSub getClaimDetailSubIdFk() {
        return claimDetailSubIdFk;
    }

    public void setClaimDetailSubIdFk(ClaimDetailSub claimDetailSubIdFk) {
        this.claimDetailSubIdFk = claimDetailSubIdFk;
    }

    @XmlTransient
    public Collection<ClaimMiscPayment> getClaimMiscPaymentCollection() {
        return claimMiscPaymentCollection;
    }

    public void setClaimMiscPaymentCollection(Collection<ClaimMiscPayment> claimMiscPaymentCollection) {
        this.claimMiscPaymentCollection = claimMiscPaymentCollection;
    }

    @XmlTransient
    public Collection<ClaimDetailSub> getClaimDetailSubCollection() {
        return claimDetailSubCollection;
    }

    public void setClaimDetailSubCollection(Collection<ClaimDetailSub> claimDetailSubCollection) {
        this.claimDetailSubCollection = claimDetailSubCollection;
    }

    @XmlTransient
    public Collection<ClaimDetailItem> getClaimDetailItemCollection() {
        return claimDetailItemCollection;
    }

    public void setClaimDetailItemCollection(Collection<ClaimDetailItem> claimDetailItemCollection) {
        this.claimDetailItemCollection = claimDetailItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimDetailId != null ? claimDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimDetail)) {
            return false;
        }
        ClaimDetail other = (ClaimDetail) object;
        if ((this.claimDetailId == null && other.claimDetailId != null) || (this.claimDetailId != null && !this.claimDetailId.equals(other.claimDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimDetail[ claimDetailId=" + claimDetailId + " ]";
    }
    
}
