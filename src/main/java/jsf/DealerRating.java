/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class DealerRating {

    private static final Logger LOGGER = LogManager.getLogger(DealerRating.class);
    

    public DealerRating() {
        LOGGER.info("default DealerRating constructor");
        dprodList = new ArrayList<>();
    }

    public DealerRating(String vinNo, String dealerName, Integer dealerId, Integer mileage, BigDecimal totalAmount, BigDecimal surcharge, BigDecimal deductible, List<DealerProduct> dprodList) {
        this.vinNo = vinNo;
        this.dealerName = dealerName;
        this.dealerId = dealerId;
        this.mileage = mileage;
        this.totalAmount = totalAmount;
        this.surcharge = surcharge;
        this.deductible = deductible;
        this.dprodList = dprodList;
    }

    
    

    private String vinNo;
    private String dealerName;
    private Integer dealerId;
    private Integer mileage;
    private BigDecimal totalAmount;
    private BigDecimal surcharge;
    private BigDecimal deductible;
    private List<DealerProduct> dprodList;
    
    
    public void reset() {
        vinNo = "";
        dealerName = "";
        dealerId = null;
        mileage = null;
        totalAmount = null;
        surcharge = null;
        deductible = null;
        for( DealerProduct dp : dprodList ) {
            dp.reset();
        }
        dprodList = new ArrayList<>();
    }

    public String getVinNo() {
        return vinNo;
    }

    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(BigDecimal surcharge) {
        this.surcharge = surcharge;
    }

    public BigDecimal getDeductible() {
        return deductible;
    }

    public void setDeductible(BigDecimal deductible) {
        this.deductible = deductible;
    }

    public List<DealerProduct> getDprodList() {
        return dprodList;
    }

    public void setDprodList(List<DealerProduct> dprodList) {
        this.dprodList = dprodList;
    }

    
    
}
