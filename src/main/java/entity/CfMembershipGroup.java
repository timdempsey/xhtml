/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfMembershipGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfMembershipGroup.findAll", query = "SELECT c FROM CfMembershipGroup c")
    , @NamedQuery(name = "CfMembershipGroup.findByMembershipGroupId", query = "SELECT c FROM CfMembershipGroup c WHERE c.membershipGroupId = :membershipGroupId")
    , @NamedQuery(name = "CfMembershipGroup.findByName", query = "SELECT c FROM CfMembershipGroup c WHERE c.name = :name")
    , @NamedQuery(name = "CfMembershipGroup.findByDescription", query = "SELECT c FROM CfMembershipGroup c WHERE c.description = :description")
    , @NamedQuery(name = "CfMembershipGroup.findByUpdateUserName", query = "SELECT c FROM CfMembershipGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfMembershipGroup.findByUpdateLast", query = "SELECT c FROM CfMembershipGroup c WHERE c.updateLast = :updateLast")})
public class CfMembershipGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "membershipGroupId")
    private Integer membershipGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membershipGroupId")
    private Collection<CfUserGroupConn> cfUserGroupConnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membershipGroupId")
    private Collection<CfRoleGroupConn> cfRoleGroupConnCollection;

    public CfMembershipGroup() {
    }

    public CfMembershipGroup(Integer membershipGroupId) {
        this.membershipGroupId = membershipGroupId;
    }

    public CfMembershipGroup(Integer membershipGroupId, String name) {
        this.membershipGroupId = membershipGroupId;
        this.name = name;
    }

    public Integer getMembershipGroupId() {
        return membershipGroupId;
    }

    public void setMembershipGroupId(Integer membershipGroupId) {
        this.membershipGroupId = membershipGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfUserGroupConn> getCfUserGroupConnCollection() {
        return cfUserGroupConnCollection;
    }

    public void setCfUserGroupConnCollection(Collection<CfUserGroupConn> cfUserGroupConnCollection) {
        this.cfUserGroupConnCollection = cfUserGroupConnCollection;
    }

    @XmlTransient
    public Collection<CfRoleGroupConn> getCfRoleGroupConnCollection() {
        return cfRoleGroupConnCollection;
    }

    public void setCfRoleGroupConnCollection(Collection<CfRoleGroupConn> cfRoleGroupConnCollection) {
        this.cfRoleGroupConnCollection = cfRoleGroupConnCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (membershipGroupId != null ? membershipGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfMembershipGroup)) {
            return false;
        }
        CfMembershipGroup other = (CfMembershipGroup) object;
        if ((this.membershipGroupId == null && other.membershipGroupId != null) || (this.membershipGroupId != null && !this.membershipGroupId.equals(other.membershipGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfMembershipGroup[ membershipGroupId=" + membershipGroupId + " ]";
    }
    
}
