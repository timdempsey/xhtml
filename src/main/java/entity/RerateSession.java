/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RerateSession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RerateSession.findAll", query = "SELECT r FROM RerateSession r")
    , @NamedQuery(name = "RerateSession.findByRerateSessionId", query = "SELECT r FROM RerateSession r WHERE r.rerateSessionId = :rerateSessionId")
    , @NamedQuery(name = "RerateSession.findByRerateSessionStatusInd", query = "SELECT r FROM RerateSession r WHERE r.rerateSessionStatusInd = :rerateSessionStatusInd")
    , @NamedQuery(name = "RerateSession.findByDescription", query = "SELECT r FROM RerateSession r WHERE r.description = :description")
    , @NamedQuery(name = "RerateSession.findByCreateAmendment", query = "SELECT r FROM RerateSession r WHERE r.createAmendment = :createAmendment")
    , @NamedQuery(name = "RerateSession.findByAmendmentMemo", query = "SELECT r FROM RerateSession r WHERE r.amendmentMemo = :amendmentMemo")
    , @NamedQuery(name = "RerateSession.findByEnteredDate", query = "SELECT r FROM RerateSession r WHERE r.enteredDate = :enteredDate")
    , @NamedQuery(name = "RerateSession.findByUpdateUserName", query = "SELECT r FROM RerateSession r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RerateSession.findByUpdateLast", query = "SELECT r FROM RerateSession r WHERE r.updateLast = :updateLast")})
public class RerateSession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rerateSessionId")
    private Integer rerateSessionId;
    @Column(name = "rerateSessionStatusInd")
    private Integer rerateSessionStatusInd;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Column(name = "createAmendment")
    private Boolean createAmendment;
    @Size(max = 255)
    @Column(name = "amendmentMemo")
    private String amendmentMemo;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;
    @OneToMany(mappedBy = "rerateSessionIdFk")
    private Collection<RerateSessionContract> rerateSessionContractCollection;

    public RerateSession() {
    }

    public RerateSession(Integer rerateSessionId) {
        this.rerateSessionId = rerateSessionId;
    }

    public Integer getRerateSessionId() {
        return rerateSessionId;
    }

    public void setRerateSessionId(Integer rerateSessionId) {
        this.rerateSessionId = rerateSessionId;
    }

    public Integer getRerateSessionStatusInd() {
        return rerateSessionStatusInd;
    }

    public void setRerateSessionStatusInd(Integer rerateSessionStatusInd) {
        this.rerateSessionStatusInd = rerateSessionStatusInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreateAmendment() {
        return createAmendment;
    }

    public void setCreateAmendment(Boolean createAmendment) {
        this.createAmendment = createAmendment;
    }

    public String getAmendmentMemo() {
        return amendmentMemo;
    }

    public void setAmendmentMemo(String amendmentMemo) {
        this.amendmentMemo = amendmentMemo;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @XmlTransient
    public Collection<RerateSessionContract> getRerateSessionContractCollection() {
        return rerateSessionContractCollection;
    }

    public void setRerateSessionContractCollection(Collection<RerateSessionContract> rerateSessionContractCollection) {
        this.rerateSessionContractCollection = rerateSessionContractCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rerateSessionId != null ? rerateSessionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RerateSession)) {
            return false;
        }
        RerateSession other = (RerateSession) object;
        if ((this.rerateSessionId == null && other.rerateSessionId != null) || (this.rerateSessionId != null && !this.rerateSessionId.equals(other.rerateSessionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RerateSession[ rerateSessionId=" + rerateSessionId + " ]";
    }
    
}
