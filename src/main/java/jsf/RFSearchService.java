/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Claim;
import entity.CfRegion;
import entity.RepairFacility;
import entity.RepairFacilityContactPerson;
import entity.UserMember;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "rfSearchService")
@ApplicationScoped
public class RFSearchService {

    private static final Logger LOGGER = LogManager.getLogger(RFSearchService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of RFSearchService
     */
    public RFSearchService() {
    }

    @Inject
    transient private ClaimMisc claimMisc;

    @Inject
    ClaimGeneralService claimGeneralService;

    private List<RFSearch> rfs;
    RFSearch selectedRF;
    RFSearch rfsearch;

    String srfname;
    String srfphone;
    String scity;
    String sregion;
    String szip;
    BigDecimal slabortax;
    BigDecimal spartstax;
    BigDecimal slaborrate;

    String claimNumber;

    private List<String> regionCodes;

    private ClaimGeneral rfcg;

    public ClaimGeneral getRfcg() {
        return rfcg;
    }

    public void setRfcg(ClaimGeneral rfcg) {
        this.rfcg = rfcg;
    }

    public List<String> getRegionCodes() {
        return regionCodes;
    }

    public void setRegionCodes(List<String> regionCodes) {
        this.regionCodes = regionCodes;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getSrfname() {
        return srfname;
    }

    public void setSrfname(String srfname) {
        this.srfname = srfname;
    }

    public String getSrfphone() {
        return srfphone;
    }

    public void setSrfphone(String srfphone) {
        this.srfphone = srfphone;
    }

    public String getScity() {
        return scity;
    }

    public void setScity(String scity) {
        this.scity = scity;
    }

    public String getSregion() {
        return sregion;
    }

    public void setSregion(String sregion) {
        this.sregion = sregion;
    }

    public String getSzip() {
        return szip;
    }

    public void setSzip(String szip) {
        this.szip = szip;
    }

    public BigDecimal getSlabortax() {
        return slabortax;
    }

    public void setSlabortax(BigDecimal slabortax) {
        this.slabortax = slabortax;
    }

    public BigDecimal getSpartstax() {
        return spartstax;
    }

    public void setSpartstax(BigDecimal spartstax) {
        this.spartstax = spartstax;
    }

    public BigDecimal getSlaborrate() {
        return slaborrate;
    }

    public void setSlaborrate(BigDecimal slaborrate) {
        this.slaborrate = slaborrate;
    }

    public RFSearch getSelectedRF() {
        return selectedRF;
    }

    public void setSelectedRF(RFSearch selectedRF) {
        this.selectedRF = selectedRF;
    }

    public List<RFSearch> getRfs() {
        return rfs;
    }

    public void setRfs(List<RFSearch> rfs) {
        this.rfs = rfs;
    }

    public RFSearch getRfsearch() {
        return rfsearch;
    }

    public void setRfsearch(RFSearch rfsearch) {
        this.rfsearch = rfsearch;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("init from RFSearchService");
        rfs = new ArrayList<>();
        selectedRF = new RFSearch();
        rfsearch = new RFSearch();

        //cg = claimGeneralService.getCg();
        List<CfRegion> regions = claimMisc.getRegionByAll();
        regionCodes = new ArrayList<>();
        for (CfRegion region : regions) {
            regionCodes.add(region.getRegionCode());
        }
    }

    public void reset() {
        srfname = "";
        srfphone = "";
        scity = "";
        sregion = "";
        szip = "";
        slabortax = null;
        spartstax = null;
        slaborrate = null;
    }

    public void createRFSearchView() {
        try {
            //List<RFSearch> rfList = new ArrayList<>();

            LOGGER.info("srfname=" + srfname);
            LOGGER.info("srfphone=" + srfphone);
            LOGGER.info("scity=" + scity);
            LOGGER.info("sregion=" + sregion);
            LOGGER.info("szip=" + szip);
            LOGGER.info("slabortax=" + slabortax);
            LOGGER.info("spartstax=" + spartstax);
            LOGGER.info("slaborrate=" + slaborrate);
            rfsearch.rfName = srfname;
            rfsearch.rfPhone = srfphone;
            rfsearch.city = scity;
            rfsearch.state = sregion;
            rfsearch.zip = szip;
            rfsearch.laborTax = slabortax;
            rfsearch.partTax = spartstax;
            rfsearch.laborRate = slaborrate;

            rfs.clear();

            rfs = claimMisc.getRFSearch(rfsearch);

            if (rfs != null) {
                LOGGER.info("size of rfs=" + rfs.size());
            }

        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void searchRF(ActionEvent event) {

        /*
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("searchRF=" + claimNumber);
         */
        rfs.clear();
        reset();
        //contractNo = (String) event.getComponent().getAttributes().get("contractNo");

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1400");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("searchRF", options, null);
    }

    public void viewNewEditFacility() {
        /*
         rfs.clear();
         reset();
         */
        LOGGER.info("in viewNewEditFacility...");
        rfcg = new ClaimGeneral(claimGeneralService.getCg());
        LOGGER.info("in view, claimNumber=" + rfcg.getClaimNumber());

        //rfcg.setOdometer(111111);
        //LOGGER.info("in view, odometer=" + rfcg.getOdometer());

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("Facility", options, null);
    }

    public void facilityreset() {
        LOGGER.info("in facilityreset...");
        LOGGER.info("before reset, odometer=" + claimGeneralService.getCg().getOdometer());
        rfcg.rfreset();
        LOGGER.info("after reset, odometer=" + claimGeneralService.getCg().getOdometer());
    }

    public void closeViewNewEditFacility() {
        LOGGER.info("in closeViewNewEditFacility");
        //LOGGER.info("before close, cgs.odometer=" + claimGeneralService.getCg().getOdometer());
        //LOGGER.info("before close, rf.odometer=" + rfcg.getOdometer());
        //claimGeneralService.setCg(rfcg);
        RequestContext.getCurrentInstance().closeDialog("Facility");
    }

    public void saveCloseViewNewEditFacility() {
        LOGGER.info("in saveCloseViewNewEditFacility...");

        try {
            LOGGER.info("rfcg claimNumber=" + rfcg.getClaimNumber());

            utx.begin();
            Integer akid = null;
            Integer repairFacilityContactIdFk = null;
            Integer ownedByFk = null;
            RepairFacilityContactPerson rfcp = null;

            //ClaimGeneral origCG = claimGeneralService.getCg();
            String rfName = rfcg.getRfName();

            if (rfName != null && rfName.length() > 0) {
                LOGGER.info("rfname=" + rfcg.getRfName());

                AccountKeeper ak = claimMisc.getAccoutKeeperByRFname(rfName);
                if (ak == null) {   // new Facility entry
                    // add to RepairFacilityToDealerRel
                    // AccountKeeper
                    // Claim if available
                    // Region
                    // Address
                    // RepairFacility
                    // RepairFacilityContactPerson
                    LOGGER.info("insert into Address...");
                    Query query = em.createNativeQuery("insert into Address (addressName, address1, address2, city, zipCode, phoneNumber, fax, regionIdFk, addressTypeIdFk ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9 )");
                    query.setParameter(1, "Physical");
                    query.setParameter(2, rfcg.getLine1());
                    query.setParameter(3, rfcg.getLine2());
                    query.setParameter(4, rfcg.getCity());
                    query.setParameter(5, rfcg.getZip());
                    query.setParameter(6, claimMisc.phoneTodb(rfcg.rfPhone) );
                    query.setParameter(7, claimMisc.phoneTodb(rfcg.rfFax) );
                    query.setParameter(8, claimMisc.getRegionByRegionCode(rfcg.getState()).getRegionId());
                    query.setParameter(9, 1);
                    int ret = query.executeUpdate();
                    if (ret == 1) {
                        Integer addressId = claimMisc.getLatestInsertedId("Address");
                        LOGGER.info("addressId=" + addressId);
                        // AccountKeeper
                        LOGGER.info("insert into AccountKeeper...");
                        query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, taxId, active, accountHolderTypeInd, emailAddress, billingAddressIdFk, physicalAddressIdFk ) values (?1, ?2, ?3, ?4, ?5, ?6, ?7 )");
                        query.setParameter(1, rfcg.getRfName());
                        query.setParameter(2, rfcg.getTaxId());
                        query.setParameter(3, 1);   // bit
                        query.setParameter(4, 4);   // later
                        query.setParameter(5, rfcg.getEmailAddress());
                        query.setParameter(6, addressId);   // billing address is the same as to physical address for now, until otherwise
                        query.setParameter(7, addressId);

                        ret = query.executeUpdate();
                        if (ret == 1) {
                            akid = claimMisc.getLatestInsertedId("AccountKeeper");
                            LOGGER.info("akid=" + akid);

                            // RepairFacility
                            LOGGER.info("insert into RepairFacility");
                            query = em.createNativeQuery("insert into RepairFacility (repairFacilityId, partWarrantyMonths, partWarrantyMiles, laborTaxPct, partsTaxPct, laborRate ) values (?1, ?2, ?3, ?4, ?5, ?6 )");
                            query.setParameter(1, akid);
                            query.setParameter(2, rfcg.getPartWarrantyMonths());
                            query.setParameter(3, rfcg.getPartWarrantyMiles());
                            query.setParameter(4, rfcg.getLaborTax());
                            query.setParameter(5, rfcg.getPartTax());
                            query.setParameter(6, rfcg.getLaborRate());
                            query.executeUpdate();

                            // RepairFacilityToDealerRel
                            LOGGER.info("insert into RepairFacilityToDealerRel");
                            query = em.createNativeQuery("insert into RepairFacilityToDealerRel (repairFacilityId, dealerId ) values (?1, ?2 )");
                            query.setParameter(1, akid);
                            query.setParameter(2, claimGeneralService.getContract().getDealerIdFk().getDealerId());
                            query.executeUpdate();

                        }
                    }
                } else {
                    // AccountKeeper
                    LOGGER.info("update AccountKeeper");
                    akid = ak.getAccountKeeperId();
                    ak = claimMisc.getAccountKeeperById(akid);
                    ak.setTaxId(rfcg.getTaxId());
                    ak.setEmailAddress(rfcg.getEmailAddress());
                    em.merge(ak);

                    // RepairFacility
                    RepairFacility rf = claimMisc.getRepairFacilityById(akid);
                    rf.setPartWarrantyMonths(rfcg.getPartWarrantyMonths());
                    rf.setPartWarrantyMiles(rfcg.getPartWarrantyMiles());
                    rf.setLaborTaxPct(rfcg.getLaborTax());
                    rf.setPartsTaxPct(rfcg.getPartTax());
                    rf.setLaborRate(rfcg.getLaborRate());
                    em.merge(rf);

                }
                LOGGER.info("accountKeeperId=" + akid);

                if (rfcg.getContactName() != null && rfcg.getContactName().length() > 0) {

                    List<RepairFacilityContactPerson> rfcps = (List<RepairFacilityContactPerson>) em.createNativeQuery("select b.* from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk", RepairFacilityContactPerson.class).setParameter(1, akid).getResultList();
                    LOGGER.info("length of rfcps=" + rfcps.size());
                    if (rfcps.size() != 0) {
                        for (RepairFacilityContactPerson rp : rfcps) {
                            LOGGER.info("rp=" + rp.getPhoneNumber());
                            rp.setName(rfcg.getContactName());
                            rp.setPhoneNumber(claimMisc.phoneTodb(rfcg.getContactPhone() ));
                            em.merge(rp);
                            rfcp = rp;
                            repairFacilityContactIdFk = rp.getRepairFacilityContactPersonId();
                            //LOGGER.info("merge rfcp");
                            LOGGER.info("rfcp=" + repairFacilityContactIdFk);
                            break;
                        }
                    } else {    // add new record in RepairFacilityContactPerson
                        LOGGER.info("new rfcp, 2=" + rfcg.getContactPhone());
                        Query query = em.createNativeQuery("insert into RepairFacilityContactPerson (name, PhoneNumber, repairFacilityIdFk) values (?1, ?2, ?3)");
                        query.setParameter(1, rfcg.getContactName());
                        query.setParameter(2, claimMisc.phoneTodb(rfcg.getContactPhone()) );
                        query.setParameter(3, akid);
                        int ret = query.executeUpdate();
                        LOGGER.info("insert to rfcp, ret=" + ret);
                        repairFacilityContactIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('RepairFacilityContactPerson')").getSingleResult()).intValueExact();
                        rfcp = em.createNamedQuery("RepairFacilityContactPerson.findByRepairFacilityContactPersonId", RepairFacilityContactPerson.class).setParameter("repairFacilityContactPersonId", repairFacilityContactIdFk).getSingleResult();
                        LOGGER.info("latest id=" + repairFacilityContactIdFk);
                    }
                }

                String assignTo = rfcg.getAssignedTo();
                if (assignTo != null && assignTo.length() > 0) {

                    UserMember um = claimMisc.getUserMemberByName(assignTo);
                    ownedByFk = um.getUserMemberId();
                    LOGGER.info("ownedByFk=" + ownedByFk);

                }

                if (claimNumber != null && claimNumber.length() > 0) {
                    Claim claim = claimMisc.getClaimByNumber(claimNumber);

                    RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                    claim.setRepairFacilityIdFk(rf);
                    claim.setRepairFacilityContactIdFk(rfcp);
                    claim.setPaidToFk(claimMisc.getAccountKeeperById(akid));
                    em.merge(claim);
                    LOGGER.info("merge claim");
                }

                utx.commit();
                LOGGER.info("end of commit");
                /*
                rfcg.setClaimNumber(origCG.getClaimNumber());
                rfcg.setOdometer(origCG.getOdometer());
                rfcg.setRoNumber(origCG.getRoNumber());
                rfcg.setRoDate(origCG.getRoDate());
                rfcg.setAssignedTo(origCG.getAssignedTo());
                 */
                LOGGER.info("after commit, rfcg claimNumber=" + rfcg.getClaimNumber());
                claimGeneralService.setCg(rfcg);
            }
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

        RequestContext.getCurrentInstance().closeDialog("Facility");
    }

    /* original
    public void onRowSelect(SelectEvent event) {
        selectedRF = (RFSearch) event.getObject();
        
        FacesMessage msg = new FacesMessage("RFSearch Selected", ((RFSearch) event.getObject()).getRfName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        LOGGER.info("RFSearch, claimNumber=" + claimNumber);
        
        ClaimGeneral cg = claimGeneralService.createClaimView(claimNumber);
        LOGGER.info("pzip=" + cg.getZip());
        LOGGER.info("pname="+cg.getRfName());
        LOGGER.info("azip="+selectedRF.getZip());
        LOGGER.info("aname=" +selectedRF.getRfName());
        cg.setRfName(selectedRF.getRfName());
        cg.setRfPhone(selectedRF.getRfPhone());
        cg.setCity(selectedRF.getCity());
        cg.setState(selectedRF.getState());
        cg.setZip(selectedRF.getZip());
        cg.setLaborRate(selectedRF.getLaborRate());
        cg.setLaborTax(selectedRF.getLaborTax());
        cg.setPartTax(selectedRF.getPartTax());
        cg.setLine1(selectedRF.getAddress1());
        cg.setLine2(selectedRF.getAddress2());
        claimGeneralService.setCg(cg);
        //FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove("searchRF");
        RequestContext.getCurrentInstance().closeDialog("searchRF");
        
    }
     */
    public void onRowSelect(SelectEvent event) {
        selectedRF = (RFSearch) event.getObject();
        LOGGER.info("onRowSelect, rfname=" + selectedRF.getRfName());

        claimGeneralService.setRfsearch(selectedRF);
        /*
        FacesMessage msg = new FacesMessage("RFSearch Selected", ((RFSearch) event.getObject()).getRfName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        LOGGER.info("RFSearch, claimNumber=" + claimNumber);
        
        ClaimGeneral cg = claimGeneralService.createClaimView(claimNumber);
        LOGGER.info("pzip=" + cg.getZip());
        LOGGER.info("pname="+cg.getRfName());
        LOGGER.info("azip="+selectedRF.getZip());
        LOGGER.info("aname=" +selectedRF.getRfName());
        cg.setRfName(selectedRF.getRfName());
        cg.setRfPhone(selectedRF.getRfPhone());
        cg.setCity(selectedRF.getCity());
        cg.setState(selectedRF.getState());
        cg.setZip(selectedRF.getZip());
        cg.setLaborRate(selectedRF.getLaborRate());
        cg.setLaborTax(selectedRF.getLaborTax());
        cg.setPartTax(selectedRF.getPartTax());
        cg.setLine1(selectedRF.getAddress1());
        cg.setLine2(selectedRF.getAddress2());
        claimGeneralService.setCg(cg);
         */
        //FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove("searchRF");
        RequestContext.getCurrentInstance().closeDialog("searchRF");

    }

    public void checkRFname() {
        LOGGER.info("in checkRFname, rfname=" + rfcg.getRfName());
        if (claimMisc.getAccountKeeperNameCount(rfcg.getRfName()) > 0) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "\"", rfcg.getRfName() + "\" already exists.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

}
