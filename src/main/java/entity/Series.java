/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Series")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Series.findAll", query = "SELECT s FROM Series s")
    , @NamedQuery(name = "Series.findBySeriesId", query = "SELECT s FROM Series s WHERE s.seriesId = :seriesId")
    , @NamedQuery(name = "Series.findByName", query = "SELECT s FROM Series s WHERE s.name = :name")
    , @NamedQuery(name = "Series.findByActive", query = "SELECT s FROM Series s WHERE s.active = :active")
    , @NamedQuery(name = "Series.findByUpdateUserName", query = "SELECT s FROM Series s WHERE s.updateUserName = :updateUserName")
    , @NamedQuery(name = "Series.findByUpdateLast", query = "SELECT s FROM Series s WHERE s.updateLast = :updateLast")})
public class Series implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "seriesId")
    private Integer seriesId;
    @Size(max = 250)
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    private Boolean active;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "seriesIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "seriesIdFk")
    private Collection<VinDesc> vinDescCollection;
    @JoinColumn(name = "modelIdFk", referencedColumnName = "ModelId")
    @ManyToOne
    private Model modelIdFk;

    public Series() {
    }

    public Series(Integer seriesId) {
        this.seriesId = seriesId;
    }

    public Integer getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Integer seriesId) {
        this.seriesId = seriesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<VinDesc> getVinDescCollection() {
        return vinDescCollection;
    }

    public void setVinDescCollection(Collection<VinDesc> vinDescCollection) {
        this.vinDescCollection = vinDescCollection;
    }

    public Model getModelIdFk() {
        return modelIdFk;
    }

    public void setModelIdFk(Model modelIdFk) {
        this.modelIdFk = modelIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seriesId != null ? seriesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Series)) {
            return false;
        }
        Series other = (Series) object;
        if ((this.seriesId == null && other.seriesId != null) || (this.seriesId != null && !this.seriesId.equals(other.seriesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Series[ seriesId=" + seriesId + " ]";
    }
    
}
