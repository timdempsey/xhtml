/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.List;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.faces.event.ActionEvent;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "disbursementService")
@ApplicationScoped
public class DisbursementService {

    /**
     * Creates a new instance of DisbursementService
     */
    public DisbursementService() {
        ddList = new ArrayList<>();
        details = new DetailedDisbursements();
        fromDealerId = null;
        selectedDetailedDisbursments = null;
    }

    private static final Logger LOGGER = LogManager.getLogger(DisbursementService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    Integer disbursementDetailId;

    DetailedDisbursements details;
    List<DetailedDisbursements> ddList;
    DetailedDisbursements selectedDetailedDisbursments;
    Integer detailedDisbursementId;
    
    Integer fromDealerId;

    public Integer getFromDealerId() {
        return fromDealerId;
    }

    public void setFromDealerId(Integer fromDealerId) {
        this.fromDealerId = fromDealerId;
    }

    
    

    public DetailedDisbursements getSelectedDetailedDisbursments() {
        return selectedDetailedDisbursments;
    }

    public void setSelectedDetailedDisbursments(DetailedDisbursements selectedDetailedDisbursments) {
        this.selectedDetailedDisbursments = selectedDetailedDisbursments;
    }

    public Integer getDetailedDisbursementId() {
        return detailedDisbursementId;
    }

    public void setDetailedDisbursementId(Integer detailedDisbursementId) {
        this.detailedDisbursementId = detailedDisbursementId;
    }

    public Integer getDisbursementDetailId() {
        return disbursementDetailId;
    }

    public void setDisbursementDetailId(Integer disbursementDetailId) {
        this.disbursementDetailId = disbursementDetailId;
    }

    public DetailedDisbursements getDetails() {
        return details;
    }

    public void setDetails(DetailedDisbursements details) {
        this.details = details;
    }

    public List<DetailedDisbursements> getDdList() {
        return ddList;
    }

    public void setDdList(List<DetailedDisbursements> ddList) {
        this.ddList = ddList;
    }

    public void reset() {
        details = new DetailedDisbursements();
    }

    public void addDisbursement(ActionEvent event) {
        details.reset();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "800");
        options.put("width", "600");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDisbursement", options, null);

    }

    public void editDealer() {
        LOGGER.info("detailedDisbursementId=" + selectedDetailedDisbursments.getDisbursementDetailId());

        details = selectedDetailedDisbursments;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDisbursment", options, null);

    }

    /*
    public String deleteDealer() {
        detailedDisbursementId = selectedDetailedDisbursments.getDisbursementDetailId();
        LOGGER.info("in deleteDealer, detailedDisbursementId=" + detailedDisbursementId);
        claimMisc.deleteAccountKeeper(detailedDisbursementId);
        if (dg == null) {
            dealersList = claimMisc.getDealers();
        } else {
            dealersList = claimMisc.getDealersByGroupFkId(dg.getDealerGroupId());
        }
        return "Dealers";
    }
     */
    public void saveDisbursement() {
        LOGGER.info("in saveDisbursement");

        if (selectedDetailedDisbursments != null) {  // update
            LOGGER.info("updating disbursement=" + selectedDetailedDisbursments.getDisbursementDetailId());
            claimMisc.saveDisbursement(details, selectedDetailedDisbursments.getDisbursementDetailId());
        } else {
            LOGGER.info("save new disbursement");
            claimMisc.saveDisbursement(details, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddDisbursement");

    }

    public void createDisbursementTab(Integer dealerId) {
        ddList = claimMisc.getDetailedDisbursementByDealerId(dealerId);
    }

    public void addDisbursementDialogClosed(SelectEvent event) {
        LOGGER.info("in addDisbursementDialogClosed");
        createDisbursementTab(fromDealerId);
        //if (dg == null) {
        //dealersList = claimMisc.getDealers();
        //} else {
        //   dealersList = claimMisc.getDealersByGroupFkId(dg.getDealerGroupId());
        //}
    }
    
    public void cancel() {
        RequestContext.getCurrentInstance().closeDialog("AddDisbursement");
    }
}
