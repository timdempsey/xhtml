/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Program;
import entity.ProgramDetail;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "programService")
@ApplicationScoped
public class ProgramService {

    private static final Logger LOGGER = LogManager.getLogger(ProgramService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ProgramService
     */
    public ProgramService() {
    }

    ProgramSearch ps;
    List<ProgramSearch> lps;

    ProgramSearch selectedProgram;

    private List<String> adminCompanies;
    private List<String> disbursementTypes;

    Integer programId;
    Integer productId;

    ProductSearch prods;
    List<ProductSearch> lprods;

    Integer progActiveIndex;
    
    ProductSearch selectedProduct;

    public ProductSearch getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(ProductSearch selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
    
    

    public Integer getProgActiveIndex() {
        return progActiveIndex;
    }

    public void setProgActiveIndex(Integer progActiveIndex) {
        this.progActiveIndex = progActiveIndex;
    }

    

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public List<ProductSearch> getLprods() {
        return lprods;
    }

    public void setLprods(List<ProductSearch> lprods) {
        this.lprods = lprods;
    }

    public ProductSearch getProds() {
        return prods;
    }

    public void setProds(ProductSearch prods) {
        this.prods = prods;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public List<String> getDisbursementTypes() {
        return disbursementTypes;
    }

    public void setDisbursementTypes(List<String> disbursementTypes) {
        this.disbursementTypes = disbursementTypes;
    }

    public List<String> getAdminCompanies() {
        return adminCompanies;
    }

    public void setAdminCompanies(List<String> adminCompanies) {
        this.adminCompanies = adminCompanies;
    }

    public ProgramSearch getSelectedProgram() {
        return selectedProgram;
    }

    public void setSelectedProgram(ProgramSearch selectedProgram) {
        this.selectedProgram = selectedProgram;
    }

    public List<ProgramSearch> getLps() {
        return lps;
    }

    public void setLps(List<ProgramSearch> lps) {
        this.lps = lps;
    }

    public ProgramSearch getPs() {
        return ps;
    }

    public void setPs(ProgramSearch ps) {
        this.ps = ps;
    }

    public void createProgramView() {

    }

    /*
    @PostConstruct
    public void init() {
        LOGGER.info("init in ProgramService");
        lps = claimMisc.getProgramSearchs();
        selectedProgram = new ProgramSearch();
        adminCompanies = setAdminCompanies();
        disbursementTypes = setDisbursmentTypes();
        ps = new ProgramSearch();
        prods = new ProductSearch();
        programId = null;
    }
*/
    
    public void reset() {
        selectedProgram = new ProgramSearch();
        ps = new ProgramSearch();
        prods = new ProductSearch();
        programId = null;
    }
    
     public String getProgramSearch() {
        LOGGER.info(" in getProgramSearch");
        reset();
        lps = claimMisc.getProgramSearchs();
        adminCompanies = setAdminCompanies();
        disbursementTypes = setDisbursmentTypes();
        
        return "ProgramSearch";
    }

    private List<String> setDisbursmentTypes() {
        List<String> dts = new ArrayList<>();
        dts.add("[Direct Disbursements]");
        return dts;
    }

    private List<String> setAdminCompanies() {
        List<String> acs = new ArrayList<>();
        acs.add("Dealer Owned");
        return acs;
    }

    public void onRowSelect(SelectEvent event) {
        selectedProgram = (ProgramSearch) event.getObject();

        FacesMessage msg = new FacesMessage("Program Selected", ((ProgramSearch) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void addProgram(ActionEvent event) {

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addProgram", options, null);

    }

    public void saveProgram() {
        LOGGER.info("in saveProgram, programId=" + programId);
        try {
            utx.begin();
            if (programId == null) {
                Query query = em.createNativeQuery("insert into Program (name, code,  updateUserName) values (?1, ?2, ?3)");
                query.setParameter(1, ps.getName());
                query.setParameter(2, ps.getCode());
                String updateUserName = "JP";
                query.setParameter(3, updateUserName);  // later

                int ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                if (ret == 1) {
                    Integer programIdFk = claimMisc.getLatestInsertedId("Program");
                    Integer enteredByIdFk = 21367; // later
                    LOGGER.info("effectiveDate=" + ps.getEffectiveDate());
                    query = em.createNativeQuery("insert into ProgramDetail (programIdFk, effectiveDate,  enteredByIdFk, enteredDate, deletedInd, description, disbursementCenterIdFk, updateUserName) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)");
                    query.setParameter(1, programIdFk);
                    query.setParameter(2, claimMisc.convertFromUtilDate(ps.getEffectiveDate()));
                    query.setParameter(3, enteredByIdFk);
                    query.setParameter(4, claimMisc.getCurrnetDateTime());
                    query.setParameter(5, 0);
                    query.setParameter(6, ps.getDescription());
                    query.setParameter(7, 1);
                    query.setParameter(8, updateUserName);
                    ret = query.executeUpdate();
                    LOGGER.info("ProgramDetail insertion, ret=" + ret);
                }
            } else {
                Program program = claimMisc.getProgramById(programId);
                ProgramDetail pd = claimMisc.getProgramDetailByFkId(programId);
                program.setName(ps.getName());
                program.setCode(ps.getCode());
                pd.setDescription(ps.getDescription());
                pd.setEffectiveDate(ps.getEffectiveDate());
                em.merge(program);
                em.merge(pd);

            }
            utx.commit();
            RequestContext.getCurrentInstance().closeDialog("newProgram");

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        lps = claimMisc.getProgramSearchs();
    }

    public void checkProgramName() {
        if (claimMisc.getProgramByName(ps.getName()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This program name already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void checkProgramCode() {
        if (claimMisc.getProgramByCode(ps.getCode()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This program code already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void getProgramIdAttr(ActionEvent event) {
        programId = (Integer) event.getComponent().getAttributes().get("programId");
        LOGGER.info("in getProgramIdAttr, programId=" + programId);
    }

    public String getProgramProduct() {
        programId = selectedProgram.getProgramId();
        LOGGER.info("in getProgramProduct, programId=" + programId);
        lprods = claimMisc.getProductSearchByProgramId(programId);
        return "ProgramProduct";
    }

    public String deleteProgram() {
        programId = selectedProgram.getProgramId();
        LOGGER.info("in deleteProgram, programId=" + programId);
        claimMisc.deleteProgram(programId);
        lps = claimMisc.getProgramSearchs();
        return "ProgramSearch";
    }

    public void editProgram() {
        LOGGER.info("in editProgram");

        ps.setAdminCompany(selectedProgram.getAdminCompany());
        ps.setName(selectedProgram.getName());
        ps.setCode(selectedProgram.getCode());
        ps.setDescription(selectedProgram.getDescription());
        ps.setDisbursementType(selectedProgram.getDisbursementType());
        ps.setEffectiveDate(selectedProgram.getEffectiveDate());
        ps.setExpireDate(selectedProgram.getExpireDate());

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addProgram", options, null);

    }

    public void programEditClosed() {
        LOGGER.info("in programEditClosed, description=" + ps.getDescription());
        selectedProgram.setAdminCompany(ps.getAdminCompany());
        selectedProgram.setName(ps.getName());
        selectedProgram.setCode(ps.getCode());
        selectedProgram.setDescription(ps.getDescription());
        selectedProgram.setDisbursementType(ps.getDisbursementType());
        selectedProgram.setEffectiveDate(ps.getEffectiveDate());
        selectedProgram.setExpireDate(ps.getExpireDate());
    }

    public String getProductPlan() {
        LOGGER.info("later" + productId);
       return "";
    }

    public String deleteProduct(ActionEvent event) {
        Integer productId = (Integer) event.getComponent().getAttributes().get("productId");
        LOGGER.info("in deleteProduct, productId" + productId);
        return "ProductSearch";
    }
    
    public void getProductIdAttr(ActionEvent event) {
        productId = (Integer) event.getComponent().getAttributes().get("productId");
        LOGGER.info("in getProductIdAttr, productId=" + productId);
    }

}
