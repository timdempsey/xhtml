/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractForm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractForm.findAll", query = "SELECT c FROM ContractForm c")
    , @NamedQuery(name = "ContractForm.findByContractFormId", query = "SELECT c FROM ContractForm c WHERE c.contractFormId = :contractFormId")
    , @NamedQuery(name = "ContractForm.findByDescription", query = "SELECT c FROM ContractForm c WHERE c.description = :description")})
public class ContractForm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractFormId")
    private Integer contractFormId;
    @Size(max = 30)
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "contractFormIdFk")
    private Collection<Product> productCollection;

    public ContractForm() {
    }

    public ContractForm(Integer contractFormId) {
        this.contractFormId = contractFormId;
    }

    public Integer getContractFormId() {
        return contractFormId;
    }

    public void setContractFormId(Integer contractFormId) {
        this.contractFormId = contractFormId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Product> getProductCollection() {
        return productCollection;
    }

    public void setProductCollection(Collection<Product> productCollection) {
        this.productCollection = productCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractFormId != null ? contractFormId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractForm)) {
            return false;
        }
        ContractForm other = (ContractForm) object;
        if ((this.contractFormId == null && other.contractFormId != null) || (this.contractFormId != null && !this.contractFormId.equals(other.contractFormId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractForm[ contractFormId=" + contractFormId + " ]";
    }
    
}
