/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfGUITableColumn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfGUITableColumn.findAll", query = "SELECT c FROM CfGUITableColumn c")
    , @NamedQuery(name = "CfGUITableColumn.findByGUITableColumnId", query = "SELECT c FROM CfGUITableColumn c WHERE c.gUITableColumnId = :gUITableColumnId")
    , @NamedQuery(name = "CfGUITableColumn.findByColumnName", query = "SELECT c FROM CfGUITableColumn c WHERE c.columnName = :columnName")
    , @NamedQuery(name = "CfGUITableColumn.findByDefaultSortInd", query = "SELECT c FROM CfGUITableColumn c WHERE c.defaultSortInd = :defaultSortInd")
    , @NamedQuery(name = "CfGUITableColumn.findBySortAscInd", query = "SELECT c FROM CfGUITableColumn c WHERE c.sortAscInd = :sortAscInd")
    , @NamedQuery(name = "CfGUITableColumn.findByUpdateUserName", query = "SELECT c FROM CfGUITableColumn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfGUITableColumn.findByUpdateLast", query = "SELECT c FROM CfGUITableColumn c WHERE c.updateLast = :updateLast")})
public class CfGUITableColumn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GUITableColumnId")
    private Integer gUITableColumnId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "columnName")
    private String columnName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "defaultSortInd")
    private boolean defaultSortInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sortAscInd")
    private boolean sortAscInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "GUITableViewIdFk", referencedColumnName = "GUITableViewId")
    @ManyToOne(optional = false)
    private CfGUITableView gUITableViewIdFk;

    public CfGUITableColumn() {
    }

    public CfGUITableColumn(Integer gUITableColumnId) {
        this.gUITableColumnId = gUITableColumnId;
    }

    public CfGUITableColumn(Integer gUITableColumnId, String columnName, boolean defaultSortInd, boolean sortAscInd) {
        this.gUITableColumnId = gUITableColumnId;
        this.columnName = columnName;
        this.defaultSortInd = defaultSortInd;
        this.sortAscInd = sortAscInd;
    }

    public Integer getGUITableColumnId() {
        return gUITableColumnId;
    }

    public void setGUITableColumnId(Integer gUITableColumnId) {
        this.gUITableColumnId = gUITableColumnId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public boolean getDefaultSortInd() {
        return defaultSortInd;
    }

    public void setDefaultSortInd(boolean defaultSortInd) {
        this.defaultSortInd = defaultSortInd;
    }

    public boolean getSortAscInd() {
        return sortAscInd;
    }

    public void setSortAscInd(boolean sortAscInd) {
        this.sortAscInd = sortAscInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfGUITableView getGUITableViewIdFk() {
        return gUITableViewIdFk;
    }

    public void setGUITableViewIdFk(CfGUITableView gUITableViewIdFk) {
        this.gUITableViewIdFk = gUITableViewIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gUITableColumnId != null ? gUITableColumnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfGUITableColumn)) {
            return false;
        }
        CfGUITableColumn other = (CfGUITableColumn) object;
        if ((this.gUITableColumnId == null && other.gUITableColumnId != null) || (this.gUITableColumnId != null && !this.gUITableColumnId.equals(other.gUITableColumnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfGUITableColumn[ gUITableColumnId=" + gUITableColumnId + " ]";
    }
    
}
