/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class AcctTransDetail {
    
    private static final Logger LOGGER = LogManager.getLogger(AcctRecUpper.class);

    public AcctTransDetail() {
    }

    public AcctTransDetail(String transactionNumber, Integer transactionId, String transType, String transStatus, BigDecimal transactionAmount, BigDecimal appliedAmount, String accountName, String adminCompany, String insurer, String sentDate, String ReceivedDate) {
        this.transactionNumber = transactionNumber;
        this.transactionId = transactionId;
        this.transType = transType;
        this.transStatus = transStatus;
        this.transactionAmount = transactionAmount;
        this.appliedAmount = appliedAmount;
        this.accountName = accountName;
        this.adminCompany = adminCompany;
        this.insurer = insurer;
        this.sentDate = sentDate;
        this.ReceivedDate = ReceivedDate;
    }

   
   
    private String transactionNumber;
    private Integer transactionId;
    private String transType;
    private String transStatus;
    private BigDecimal transactionAmount;
    private BigDecimal appliedAmount;
    private String accountName;
    private String adminCompany;
    private String insurer;
    private String sentDate;
    private String ReceivedDate;

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(BigDecimal appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getReceivedDate() {
        return ReceivedDate;
    }

    public void setReceivedDate(String ReceivedDate) {
        this.ReceivedDate = ReceivedDate;
    }

   

    public void reset() {
        BigDecimal BIGZERO = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
        accountName = "";
        adminCompany = "";
        transactionNumber = "";
        transType = "";
        transStatus = "";
        sentDate = "";
        ReceivedDate = "";
        transactionAmount = BIGZERO;
       transactionId = null;
       appliedAmount = BIGZERO;
       insurer = "";
     
    }
    
}
