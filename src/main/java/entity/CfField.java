/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfField")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfField.findAll", query = "SELECT c FROM CfField c")
    , @NamedQuery(name = "CfField.findByFieldId", query = "SELECT c FROM CfField c WHERE c.fieldId = :fieldId")
    , @NamedQuery(name = "CfField.findByName", query = "SELECT c FROM CfField c WHERE c.name = :name")
    , @NamedQuery(name = "CfField.findByPDFName", query = "SELECT c FROM CfField c WHERE c.pDFName = :pDFName")
    , @NamedQuery(name = "CfField.findByRequiredInd", query = "SELECT c FROM CfField c WHERE c.requiredInd = :requiredInd")
    , @NamedQuery(name = "CfField.findByFieldTypeInd", query = "SELECT c FROM CfField c WHERE c.fieldTypeInd = :fieldTypeInd")
    , @NamedQuery(name = "CfField.findByDisplayOrder", query = "SELECT c FROM CfField c WHERE c.displayOrder = :displayOrder")
    , @NamedQuery(name = "CfField.findByMinimumCharacters", query = "SELECT c FROM CfField c WHERE c.minimumCharacters = :minimumCharacters")
    , @NamedQuery(name = "CfField.findByMaximumCharacters", query = "SELECT c FROM CfField c WHERE c.maximumCharacters = :maximumCharacters")
    , @NamedQuery(name = "CfField.findByMinimumValue", query = "SELECT c FROM CfField c WHERE c.minimumValue = :minimumValue")
    , @NamedQuery(name = "CfField.findByMaximumValue", query = "SELECT c FROM CfField c WHERE c.maximumValue = :maximumValue")
    , @NamedQuery(name = "CfField.findByDeletedInd", query = "SELECT c FROM CfField c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfField.findByGlobalInd", query = "SELECT c FROM CfField c WHERE c.globalInd = :globalInd")
    , @NamedQuery(name = "CfField.findByCertifiedDealPullFieldInd", query = "SELECT c FROM CfField c WHERE c.certifiedDealPullFieldInd = :certifiedDealPullFieldInd")
    , @NamedQuery(name = "CfField.findByUpdateUserName", query = "SELECT c FROM CfField c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfField.findByUpdateLast", query = "SELECT c FROM CfField c WHERE c.updateLast = :updateLast")})
public class CfField implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FieldId")
    private Integer fieldId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PDFName")
    private String pDFName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "requiredInd")
    private boolean requiredInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fieldTypeInd")
    private int fieldTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "displayOrder")
    private int displayOrder;
    @Column(name = "minimumCharacters")
    private Integer minimumCharacters;
    @Column(name = "maximumCharacters")
    private Integer maximumCharacters;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "minimumValue")
    private BigDecimal minimumValue;
    @Column(name = "maximumValue")
    private BigDecimal maximumValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "globalInd")
    private boolean globalInd;
    @Column(name = "certifiedDealPullFieldInd")
    private Integer certifiedDealPullFieldInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "formIdFk", referencedColumnName = "formId")
    @ManyToOne(optional = false)
    private CfForm formIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fieldIdFk")
    private Collection<ClaimField> claimFieldCollection;
    @OneToMany(mappedBy = "fieldIdFk")
    private Collection<CfContractEntryField> cfContractEntryFieldCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fieldIdFk")
    private Collection<ContractField> contractFieldCollection;

    public CfField() {
    }

    public CfField(Integer fieldId) {
        this.fieldId = fieldId;
    }

    public CfField(Integer fieldId, String name, String pDFName, boolean requiredInd, int fieldTypeInd, int displayOrder, boolean deletedInd, boolean globalInd) {
        this.fieldId = fieldId;
        this.name = name;
        this.pDFName = pDFName;
        this.requiredInd = requiredInd;
        this.fieldTypeInd = fieldTypeInd;
        this.displayOrder = displayOrder;
        this.deletedInd = deletedInd;
        this.globalInd = globalInd;
    }

    public Integer getFieldId() {
        return fieldId;
    }

    public void setFieldId(Integer fieldId) {
        this.fieldId = fieldId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPDFName() {
        return pDFName;
    }

    public void setPDFName(String pDFName) {
        this.pDFName = pDFName;
    }

    public boolean getRequiredInd() {
        return requiredInd;
    }

    public void setRequiredInd(boolean requiredInd) {
        this.requiredInd = requiredInd;
    }

    public int getFieldTypeInd() {
        return fieldTypeInd;
    }

    public void setFieldTypeInd(int fieldTypeInd) {
        this.fieldTypeInd = fieldTypeInd;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Integer getMinimumCharacters() {
        return minimumCharacters;
    }

    public void setMinimumCharacters(Integer minimumCharacters) {
        this.minimumCharacters = minimumCharacters;
    }

    public Integer getMaximumCharacters() {
        return maximumCharacters;
    }

    public void setMaximumCharacters(Integer maximumCharacters) {
        this.maximumCharacters = maximumCharacters;
    }

    public BigDecimal getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(BigDecimal minimumValue) {
        this.minimumValue = minimumValue;
    }

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public boolean getGlobalInd() {
        return globalInd;
    }

    public void setGlobalInd(boolean globalInd) {
        this.globalInd = globalInd;
    }

    public Integer getCertifiedDealPullFieldInd() {
        return certifiedDealPullFieldInd;
    }

    public void setCertifiedDealPullFieldInd(Integer certifiedDealPullFieldInd) {
        this.certifiedDealPullFieldInd = certifiedDealPullFieldInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public CfForm getFormIdFk() {
        return formIdFk;
    }

    public void setFormIdFk(CfForm formIdFk) {
        this.formIdFk = formIdFk;
    }

    @XmlTransient
    public Collection<ClaimField> getClaimFieldCollection() {
        return claimFieldCollection;
    }

    public void setClaimFieldCollection(Collection<ClaimField> claimFieldCollection) {
        this.claimFieldCollection = claimFieldCollection;
    }

    @XmlTransient
    public Collection<CfContractEntryField> getCfContractEntryFieldCollection() {
        return cfContractEntryFieldCollection;
    }

    public void setCfContractEntryFieldCollection(Collection<CfContractEntryField> cfContractEntryFieldCollection) {
        this.cfContractEntryFieldCollection = cfContractEntryFieldCollection;
    }

    @XmlTransient
    public Collection<ContractField> getContractFieldCollection() {
        return contractFieldCollection;
    }

    public void setContractFieldCollection(Collection<ContractField> contractFieldCollection) {
        this.contractFieldCollection = contractFieldCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fieldId != null ? fieldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfField)) {
            return false;
        }
        CfField other = (CfField) object;
        if ((this.fieldId == null && other.fieldId != null) || (this.fieldId != null && !this.fieldId.equals(other.fieldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfField[ fieldId=" + fieldId + " ]";
    }
    
}
