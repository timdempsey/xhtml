/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DisbursementGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisbursementGroup.findAll", query = "SELECT d FROM DisbursementGroup d")
    , @NamedQuery(name = "DisbursementGroup.findByDisbursementGroupId", query = "SELECT d FROM DisbursementGroup d WHERE d.disbursementGroupId = :disbursementGroupId")
    , @NamedQuery(name = "DisbursementGroup.findByName", query = "SELECT d FROM DisbursementGroup d WHERE d.name = :name")
    , @NamedQuery(name = "DisbursementGroup.findByDescription", query = "SELECT d FROM DisbursementGroup d WHERE d.description = :description")})
public class DisbursementGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementGroupId")
    private Integer disbursementGroupId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "disbursementGroupIdFk")
    private Collection<DisbursementCode> disbursementCodeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "disbursementGroupIdFk")
    private Collection<CfDisbursementRemitRule> cfDisbursementRemitRuleCollection;
    @OneToMany(mappedBy = "disbursementGroupIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;
    @OneToMany(mappedBy = "disbursementGroupIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;

    public DisbursementGroup() {
    }

    public DisbursementGroup(Integer disbursementGroupId) {
        this.disbursementGroupId = disbursementGroupId;
    }

    public Integer getDisbursementGroupId() {
        return disbursementGroupId;
    }

    public void setDisbursementGroupId(Integer disbursementGroupId) {
        this.disbursementGroupId = disbursementGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<DisbursementCode> getDisbursementCodeCollection() {
        return disbursementCodeCollection;
    }

    public void setDisbursementCodeCollection(Collection<DisbursementCode> disbursementCodeCollection) {
        this.disbursementCodeCollection = disbursementCodeCollection;
    }

    @XmlTransient
    public Collection<CfDisbursementRemitRule> getCfDisbursementRemitRuleCollection() {
        return cfDisbursementRemitRuleCollection;
    }

    public void setCfDisbursementRemitRuleCollection(Collection<CfDisbursementRemitRule> cfDisbursementRemitRuleCollection) {
        this.cfDisbursementRemitRuleCollection = cfDisbursementRemitRuleCollection;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementGroupId != null ? disbursementGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisbursementGroup)) {
            return false;
        }
        DisbursementGroup other = (DisbursementGroup) object;
        if ((this.disbursementGroupId == null && other.disbursementGroupId != null) || (this.disbursementGroupId != null && !this.disbursementGroupId.equals(other.disbursementGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DisbursementGroup[ disbursementGroupId=" + disbursementGroupId + " ]";
    }
    
}
