/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Claim;
import entity.Contracts;
import entity.VinDesc;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimCVDService")
@ApplicationScoped
public class ClaimCVDService {

    private static final Logger LOGGER = LogManager.getLogger(ClaimCVDService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimCVDService
     */
    public ClaimCVDService() {
    }

    private ClaimContract claimContract;
    private ClaimVehicleInfo claimVehicleInfo;
    private ClaimDealer claimDealer;

    private String claimNumber;
    private Claim claim;
    private String contractNo;
    private Contracts contract;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Contracts getContract() {
        return contract;
    }

    public void setContract(Contracts contract) {
        this.contract = contract;
    }

    public ClaimContract getClaimContract() {
        return claimContract;
    }

    public void setClaimContract(ClaimContract claimContract) {
        this.claimContract = claimContract;
    }

    public ClaimVehicleInfo getClaimVehicleInfo() {
        return claimVehicleInfo;
    }

    public void setClaimVehicleInfo(ClaimVehicleInfo claimVehicleInfo) {
        this.claimVehicleInfo = claimVehicleInfo;
    }

    public ClaimDealer getClaimDealer() {
        return claimDealer;
    }

    public void setClaimDealer(ClaimDealer claimDealer) {
        this.claimDealer = claimDealer;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("init from ClaimCVDService=" + contractNo);
        //claimContract = createClaimContractView(contractNo);
        //claimDts = createClaimDtsView(claimNumber);
        // reset();
    }

    public ClaimContract createClaimContractView(String no) {
        LOGGER.info("in createClaimContractView=" + no);
        contract = claimMisc.getContractsByNo(no);

        claimContract = new ClaimContract();

        claimContract.setContractNo(no);
        claimContract.setContractStatus(contract.getContractStatusIdFk().getContractStatusName());
        claimContract.setCustomer(contract.getCustomerIdFk().getAccountKeeper().getAccountKeeperName());
        claimContract.setPhone(contract.getCustomerIdFk().getAccountKeeper().getPhysicalAddressIdFk().getPhoneNumber());
        claimContract.setProgram(contract.getProductIdFk().getProgramIdFk().getName());
        claimContract.setProduct(contract.getProductIdFk().getProductName()); // later
        claimContract.setPlan(contract.getPlanIdFk().getPlanName());     // later
        String termInfo=null;
        if( ! claimMisc.isPPM(contract) ) {
            termInfo = claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0).getDescription();
        } else {
            termInfo = claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0).getPpmTime() + "/" + claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0).getPpmUsage();
        }
        claimContract.setTerm(termInfo);   // later
        claimContract.setClassCode(contract.getClassCode());
        if (contract.getDeductibleIdFk() != null) {
            claimContract.setDeductible(claimMisc.getDeductibleById(contract.getDeductibleIdFk().getDeductibleId()).getDeductibleAmount().toString());   // later
        }
        claimContract.setEffectiveDate(claimMisc.getJavaDate(contract.getEffectiveDate()));
        claimContract.setContractExpirationDate(claimMisc.getJavaDate(contract.getExpirationDate()));
        claimContract.setOdometer(contract.getOdometer());
        claimContract.setClaimStartUsage(contract.getClaimStartUsage());
        claimContract.setExpirationusage(contract.getExpirationUsage());
        claimContract.setContractPurchasePrice(contract.getContractPurchasePrice());    // later

        return claimContract;
    }

    public ClaimVehicleInfo createClaimVehicleInfoView(String cno) {

        LOGGER.info("in createClaimVehicleInfoView=" + cno);
        contract = claimMisc.getContractsByNo(cno);

        ClaimVehicleInfo claimVehicleInfo = new ClaimVehicleInfo();

        VinDesc vinDesc = claimMisc.getVinDescByContract(cno);
        claimVehicleInfo.setVinFull(contract.getVinFull());
        claimVehicleInfo.setMake(vinDesc.getMake());
        claimVehicleInfo.setModel(vinDesc.getModel());
        claimVehicleInfo.setSeries(vinDesc.getSeries());
        claimVehicleInfo.setVehicleYear(vinDesc.getVehicleYear());
        claimVehicleInfo.setVehicleType(vinDesc.getDriveType());
        claimVehicleInfo.setCylinders(vinDesc.getCylinders());
        claimVehicleInfo.setCubicInchDisplacement(vinDesc.getCubicInchDisplacement());
        claimVehicleInfo.setCubicCentimeterDisplacement(vinDesc.getCubicCentimeterDisplacement());
        claimVehicleInfo.setLiterDisplacemant(vinDesc.getLiterDisplacemant());
        claimVehicleInfo.setVehicleWeightRating(vinDesc.getVehicleWeightRating());
        claimVehicleInfo.setFuelType(vinDesc.getFuelType());
        claimVehicleInfo.setFuelDelivery(vinDesc.getFuelDelivery());
        claimVehicleInfo.setTonRating(vinDesc.getTonRating());
        claimVehicleInfo.setVehicleType(vinDesc.getVehicleType());
        claimVehicleInfo.setBasicWarranty(vinDesc.getBasicManufacturerWarrantyMileage() + "/" + vinDesc.getBasicManufacturerWarrantyTerm());
        claimVehicleInfo.setPowertrainWarranty(vinDesc.getPowerTrainWarrantyMiles() + "/" + vinDesc.getPowerTrainWarrantyMonths());
        claimVehicleInfo.setRustWarranty(vinDesc.getRustWarrantyMiles() + "/" + vinDesc.getRustWarrantyMonths());

        return claimVehicleInfo;

    }

    public ClaimDealer createDealerView(String cno) {

        LOGGER.info("in createDealerView=" + cno);
        contract = claimMisc.getContractsByNo(cno);

        ClaimDealer claimDealer = new ClaimDealer();

        Object[] tmpL = (Object[]) em.createNativeQuery("select b.accountKeeperName, c.dealerNumber from Contracts a, AccountKeeper b, Dealer c where c.dealerId=b.accountKeeperId and a.dealerIdFk=c.dealerId and  a.contractNo=?1").setParameter(1, cno).getSingleResult();
        claimDealer.setDealName((String) tmpL[0]);
        claimDealer.setDealerNumber((String) tmpL[1]);

        AccountKeeper ak = claimMisc.getAccountKeeperByContractDealer(cno);

        ClaimAddress pAddr = claimMisc.getPhysicalAddressById(ak.getAccountKeeperId());
        ClaimAddress bAddr = claimMisc.getBillingAddressById(ak.getAccountKeeperId());

        String paddr = null;
        String baddr = null;
        if (pAddr != null) {
            claimDealer.setPhoneNumber(pAddr.getPhoneNumber());
            paddr = pAddr.getAddress1() + pAddr.getAddress2() + "\n" + pAddr.getCity() + ", " + pAddr.getRegionCode() + " " + pAddr.getZipCode();
        }
        if (bAddr != null) {
            baddr = bAddr.getAddress1() + bAddr.getAddress2() + "\n" + bAddr.getCity() + ", " + bAddr.getRegionCode() + " " + bAddr.getZipCode();
        }
        claimDealer.setPhysicalAddress(paddr);
        claimDealer.setBillingAddress(baddr);
        
        LOGGER.info("end of createDealerView" );

        return claimDealer;
    }

}
