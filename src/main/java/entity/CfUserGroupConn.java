/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfUserGroupConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfUserGroupConn.findAll", query = "SELECT c FROM CfUserGroupConn c")
    , @NamedQuery(name = "CfUserGroupConn.findByUserGroupConnId", query = "SELECT c FROM CfUserGroupConn c WHERE c.userGroupConnId = :userGroupConnId")
    , @NamedQuery(name = "CfUserGroupConn.findByUpdateUserName", query = "SELECT c FROM CfUserGroupConn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfUserGroupConn.findByUpdateLast", query = "SELECT c FROM CfUserGroupConn c WHERE c.updateLast = :updateLast")})
public class CfUserGroupConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userGroupConnId")
    private Integer userGroupConnId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "MembershipGroupId", referencedColumnName = "membershipGroupId")
    @ManyToOne(optional = false)
    private CfMembershipGroup membershipGroupId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public CfUserGroupConn() {
    }

    public CfUserGroupConn(Integer userGroupConnId) {
        this.userGroupConnId = userGroupConnId;
    }

    public Integer getUserGroupConnId() {
        return userGroupConnId;
    }

    public void setUserGroupConnId(Integer userGroupConnId) {
        this.userGroupConnId = userGroupConnId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfMembershipGroup getMembershipGroupId() {
        return membershipGroupId;
    }

    public void setMembershipGroupId(CfMembershipGroup membershipGroupId) {
        this.membershipGroupId = membershipGroupId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userGroupConnId != null ? userGroupConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfUserGroupConn)) {
            return false;
        }
        CfUserGroupConn other = (CfUserGroupConn) object;
        if ((this.userGroupConnId == null && other.userGroupConnId != null) || (this.userGroupConnId != null && !this.userGroupConnId.equals(other.userGroupConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfUserGroupConn[ userGroupConnId=" + userGroupConnId + " ]";
    }
    
}
