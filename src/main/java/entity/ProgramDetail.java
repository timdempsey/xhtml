/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ProgramDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProgramDetail.findAll", query = "SELECT p FROM ProgramDetail p")
    , @NamedQuery(name = "ProgramDetail.findByProgramDetailId", query = "SELECT p FROM ProgramDetail p WHERE p.programDetailId = :programDetailId")
    , @NamedQuery(name = "ProgramDetail.findByEffectiveDate", query = "SELECT p FROM ProgramDetail p WHERE p.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "ProgramDetail.findByEnteredDate", query = "SELECT p FROM ProgramDetail p WHERE p.enteredDate = :enteredDate")
    , @NamedQuery(name = "ProgramDetail.findByDeletedInd", query = "SELECT p FROM ProgramDetail p WHERE p.deletedInd = :deletedInd")
    , @NamedQuery(name = "ProgramDetail.findByDescription", query = "SELECT p FROM ProgramDetail p WHERE p.description = :description")
    , @NamedQuery(name = "ProgramDetail.findByUpdateUserName", query = "SELECT p FROM ProgramDetail p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "ProgramDetail.findByUpdateLast", query = "SELECT p FROM ProgramDetail p WHERE p.updateLast = :updateLast")})
public class ProgramDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "programDetailId")
    private Integer programDetailId;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "programIdFk", referencedColumnName = "programId")
    @ManyToOne
    private Program programIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public ProgramDetail() {
    }

    public ProgramDetail(Integer programDetailId) {
        this.programDetailId = programDetailId;
    }

    public ProgramDetail(Integer programDetailId, boolean deletedInd) {
        this.programDetailId = programDetailId;
        this.deletedInd = deletedInd;
    }

    public Integer getProgramDetailId() {
        return programDetailId;
    }

    public void setProgramDetailId(Integer programDetailId) {
        this.programDetailId = programDetailId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Program getProgramIdFk() {
        return programIdFk;
    }

    public void setProgramIdFk(Program programIdFk) {
        this.programIdFk = programIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (programDetailId != null ? programDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProgramDetail)) {
            return false;
        }
        ProgramDetail other = (ProgramDetail) object;
        if ((this.programDetailId == null && other.programDetailId != null) || (this.programDetailId != null && !this.programDetailId.equals(other.programDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProgramDetail[ programDetailId=" + programDetailId + " ]";
    }
    
}
