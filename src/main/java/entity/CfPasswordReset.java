/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfPasswordReset")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfPasswordReset.findAll", query = "SELECT c FROM CfPasswordReset c")
    , @NamedQuery(name = "CfPasswordReset.findByPasswordResetId", query = "SELECT c FROM CfPasswordReset c WHERE c.passwordResetId = :passwordResetId")
    , @NamedQuery(name = "CfPasswordReset.findByResetToken", query = "SELECT c FROM CfPasswordReset c WHERE c.resetToken = :resetToken")
    , @NamedQuery(name = "CfPasswordReset.findByEnteredDate", query = "SELECT c FROM CfPasswordReset c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CfPasswordReset.findByEmail", query = "SELECT c FROM CfPasswordReset c WHERE c.email = :email")
    , @NamedQuery(name = "CfPasswordReset.findByTokenUsed", query = "SELECT c FROM CfPasswordReset c WHERE c.tokenUsed = :tokenUsed")
    , @NamedQuery(name = "CfPasswordReset.findByUsedDate", query = "SELECT c FROM CfPasswordReset c WHERE c.usedDate = :usedDate")
    , @NamedQuery(name = "CfPasswordReset.findByUpdateUserName", query = "SELECT c FROM CfPasswordReset c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfPasswordReset.findByUpdateLast", query = "SELECT c FROM CfPasswordReset c WHERE c.updateLast = :updateLast")})
public class CfPasswordReset implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "passwordResetId")
    private Integer passwordResetId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "resetToken")
    private String resetToken;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tokenUsed")
    private boolean tokenUsed;
    @Column(name = "usedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date usedDate;
    @Size(max = 30)
    @Column(name = "update_user_name")
    private String updateUserName;
    @Column(name = "update_last")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "userMemberIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberIdFk;

    public CfPasswordReset() {
    }

    public CfPasswordReset(Integer passwordResetId) {
        this.passwordResetId = passwordResetId;
    }

    public CfPasswordReset(Integer passwordResetId, String resetToken, Date enteredDate, String email, boolean tokenUsed) {
        this.passwordResetId = passwordResetId;
        this.resetToken = resetToken;
        this.enteredDate = enteredDate;
        this.email = email;
        this.tokenUsed = tokenUsed;
    }

    public Integer getPasswordResetId() {
        return passwordResetId;
    }

    public void setPasswordResetId(Integer passwordResetId) {
        this.passwordResetId = passwordResetId;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getTokenUsed() {
        return tokenUsed;
    }

    public void setTokenUsed(boolean tokenUsed) {
        this.tokenUsed = tokenUsed;
    }

    public Date getUsedDate() {
        return usedDate;
    }

    public void setUsedDate(Date usedDate) {
        this.usedDate = usedDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getUserMemberIdFk() {
        return userMemberIdFk;
    }

    public void setUserMemberIdFk(UserMember userMemberIdFk) {
        this.userMemberIdFk = userMemberIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (passwordResetId != null ? passwordResetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfPasswordReset)) {
            return false;
        }
        CfPasswordReset other = (CfPasswordReset) object;
        if ((this.passwordResetId == null && other.passwordResetId != null) || (this.passwordResetId != null && !this.passwordResetId.equals(other.passwordResetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfPasswordReset[ passwordResetId=" + passwordResetId + " ]";
    }
    
}
