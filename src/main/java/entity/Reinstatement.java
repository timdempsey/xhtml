/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Reinstatement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reinstatement.findAll", query = "SELECT r FROM Reinstatement r")
    , @NamedQuery(name = "Reinstatement.findByReinstatementId", query = "SELECT r FROM Reinstatement r WHERE r.reinstatementId = :reinstatementId")
    , @NamedQuery(name = "Reinstatement.findByRefundCancelFee", query = "SELECT r FROM Reinstatement r WHERE r.refundCancelFee = :refundCancelFee")
    , @NamedQuery(name = "Reinstatement.findByRefundCoverageCostOnly", query = "SELECT r FROM Reinstatement r WHERE r.refundCoverageCostOnly = :refundCoverageCostOnly")
    , @NamedQuery(name = "Reinstatement.findByUpdateUserName", query = "SELECT r FROM Reinstatement r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "Reinstatement.findByUpdateLast", query = "SELECT r FROM Reinstatement r WHERE r.updateLast = :updateLast")})
public class Reinstatement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatementId")
    private Integer reinstatementId;
    @Column(name = "refundCancelFee")
    private Boolean refundCancelFee;
    @Column(name = "RefundCoverageCostOnly")
    private Boolean refundCoverageCostOnly;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "relatedReinstatementIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "reinstatementIdFk")
    private Collection<ReinstatementDisbursement> reinstatementDisbursementCollection;
    @OneToMany(mappedBy = "reinstatementIdFk")
    private Collection<ContractHold> contractHoldCollection;
    @JoinColumn(name = "ContractCancelIdFk", referencedColumnName = "contractCancelId")
    @ManyToOne
    private ContractCancel contractCancelIdFk;
    @JoinColumn(name = "reinstatementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public Reinstatement() {
    }

    public Reinstatement(Integer reinstatementId) {
        this.reinstatementId = reinstatementId;
    }

    public Integer getReinstatementId() {
        return reinstatementId;
    }

    public void setReinstatementId(Integer reinstatementId) {
        this.reinstatementId = reinstatementId;
    }

    public Boolean getRefundCancelFee() {
        return refundCancelFee;
    }

    public void setRefundCancelFee(Boolean refundCancelFee) {
        this.refundCancelFee = refundCancelFee;
    }

    public Boolean getRefundCoverageCostOnly() {
        return refundCoverageCostOnly;
    }

    public void setRefundCoverageCostOnly(Boolean refundCoverageCostOnly) {
        this.refundCoverageCostOnly = refundCoverageCostOnly;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<ReinstatementDisbursement> getReinstatementDisbursementCollection() {
        return reinstatementDisbursementCollection;
    }

    public void setReinstatementDisbursementCollection(Collection<ReinstatementDisbursement> reinstatementDisbursementCollection) {
        this.reinstatementDisbursementCollection = reinstatementDisbursementCollection;
    }

    @XmlTransient
    public Collection<ContractHold> getContractHoldCollection() {
        return contractHoldCollection;
    }

    public void setContractHoldCollection(Collection<ContractHold> contractHoldCollection) {
        this.contractHoldCollection = contractHoldCollection;
    }

    public ContractCancel getContractCancelIdFk() {
        return contractCancelIdFk;
    }

    public void setContractCancelIdFk(ContractCancel contractCancelIdFk) {
        this.contractCancelIdFk = contractCancelIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinstatementId != null ? reinstatementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reinstatement)) {
            return false;
        }
        Reinstatement other = (Reinstatement) object;
        if ((this.reinstatementId == null && other.reinstatementId != null) || (this.reinstatementId != null && !this.reinstatementId.equals(other.reinstatementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Reinstatement[ reinstatementId=" + reinstatementId + " ]";
    }
    
}
