/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "eMTestBean")
@ApplicationScoped
public class EMTestBean  {
    
    private static final Logger LOGGER = LogManager.getLogger(ClaimTabView.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    /**
     * Creates a new instance of EMTestBean
     */
    public EMTestBean() {
    }
    
    public void test() {
        try {
        LOGGER.info("test is called");
        if( em == null ) {
            LOGGER.info("count, em is null.....");
        }
        int count = (Integer)em.createNativeQuery("select count(*) from ProductType").getSingleResult();
        LOGGER.info("count================"+count);
        } catch (Exception ex) {
            LOGGER.info("count is null");
        }
    }
    
}
