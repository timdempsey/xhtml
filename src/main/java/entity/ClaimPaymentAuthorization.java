/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimPaymentAuthorization")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimPaymentAuthorization.findAll", query = "SELECT c FROM ClaimPaymentAuthorization c")
    , @NamedQuery(name = "ClaimPaymentAuthorization.findByPaymentAuthorizationId", query = "SELECT c FROM ClaimPaymentAuthorization c WHERE c.paymentAuthorizationId = :paymentAuthorizationId")
    , @NamedQuery(name = "ClaimPaymentAuthorization.findByPaymentAuthorizationCode", query = "SELECT c FROM ClaimPaymentAuthorization c WHERE c.paymentAuthorizationCode = :paymentAuthorizationCode")
    , @NamedQuery(name = "ClaimPaymentAuthorization.findByUpdateUserName", query = "SELECT c FROM ClaimPaymentAuthorization c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimPaymentAuthorization.findByUpdateLast", query = "SELECT c FROM ClaimPaymentAuthorization c WHERE c.updateLast = :updateLast")})
public class ClaimPaymentAuthorization implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "paymentAuthorizationId")
    private Integer paymentAuthorizationId;
    @Size(max = 50)
    @Column(name = "paymentAuthorizationCode")
    private String paymentAuthorizationCode;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;
    @JoinColumn(name = "paymentAuthorizationId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public ClaimPaymentAuthorization() {
    }

    public ClaimPaymentAuthorization(Integer paymentAuthorizationId) {
        this.paymentAuthorizationId = paymentAuthorizationId;
    }

    public Integer getPaymentAuthorizationId() {
        return paymentAuthorizationId;
    }

    public void setPaymentAuthorizationId(Integer paymentAuthorizationId) {
        this.paymentAuthorizationId = paymentAuthorizationId;
    }

    public String getPaymentAuthorizationCode() {
        return paymentAuthorizationCode;
    }

    public void setPaymentAuthorizationCode(String paymentAuthorizationCode) {
        this.paymentAuthorizationCode = paymentAuthorizationCode;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentAuthorizationId != null ? paymentAuthorizationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimPaymentAuthorization)) {
            return false;
        }
        ClaimPaymentAuthorization other = (ClaimPaymentAuthorization) object;
        if ((this.paymentAuthorizationId == null && other.paymentAuthorizationId != null) || (this.paymentAuthorizationId != null && !this.paymentAuthorizationId.equals(other.paymentAuthorizationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimPaymentAuthorization[ paymentAuthorizationId=" + paymentAuthorizationId + " ]";
    }
    
}
