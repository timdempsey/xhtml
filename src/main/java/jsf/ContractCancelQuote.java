/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class ContractCancelQuote {

    public ContractCancelQuote() {
    }

    private Integer odometer;
    private Date cancelDate;
    private Date receivedDate;
    private String cancelReason;
    private String cancelMethod;

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelMethod() {
        return cancelMethod;
    }

    public void reset() {
        odometer = null;
    }

    public void setCancelMethod(String cancelMethod) {
        this.cancelMethod = cancelMethod;
        cancelDate = null;
        receivedDate = null;
        cancelReason = "";
        cancelMethod = "";
    }

}
