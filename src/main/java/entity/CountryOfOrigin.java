/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CountryOfOrigin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CountryOfOrigin.findAll", query = "SELECT c FROM CountryOfOrigin c")
    , @NamedQuery(name = "CountryOfOrigin.findByCountryOfOriginId", query = "SELECT c FROM CountryOfOrigin c WHERE c.countryOfOriginId = :countryOfOriginId")
    , @NamedQuery(name = "CountryOfOrigin.findByName", query = "SELECT c FROM CountryOfOrigin c WHERE c.name = :name")
    , @NamedQuery(name = "CountryOfOrigin.findByUpdateUserName", query = "SELECT c FROM CountryOfOrigin c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CountryOfOrigin.findByUpdateLast", query = "SELECT c FROM CountryOfOrigin c WHERE c.updateLast = :updateLast")})
public class CountryOfOrigin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "countryOfOriginId")
    private Integer countryOfOriginId;
    @Size(max = 100)
    @Column(name = "Name")
    private String name;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "countryOfOriginIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "countryOfOriginIdFk")
    private Collection<VinDesc> vinDescCollection;

    public CountryOfOrigin() {
    }

    public CountryOfOrigin(Integer countryOfOriginId) {
        this.countryOfOriginId = countryOfOriginId;
    }

    public Integer getCountryOfOriginId() {
        return countryOfOriginId;
    }

    public void setCountryOfOriginId(Integer countryOfOriginId) {
        this.countryOfOriginId = countryOfOriginId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<VinDesc> getVinDescCollection() {
        return vinDescCollection;
    }

    public void setVinDescCollection(Collection<VinDesc> vinDescCollection) {
        this.vinDescCollection = vinDescCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countryOfOriginId != null ? countryOfOriginId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CountryOfOrigin)) {
            return false;
        }
        CountryOfOrigin other = (CountryOfOrigin) object;
        if ((this.countryOfOriginId == null && other.countryOfOriginId != null) || (this.countryOfOriginId != null && !this.countryOfOriginId.equals(other.countryOfOriginId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CountryOfOrigin[ countryOfOriginId=" + countryOfOriginId + " ]";
    }
    
}
