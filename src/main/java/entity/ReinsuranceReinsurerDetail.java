/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceReinsurerDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceReinsurerDetail.findAll", query = "SELECT r FROM ReinsuranceReinsurerDetail r")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByReinsuranceReinsurerDetailId", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.reinsuranceReinsurerDetailId = :reinsuranceReinsurerDetailId")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByDisbursementTypeInd", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.disbursementTypeInd = :disbursementTypeInd")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByCedePercent", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.cedePercent = :cedePercent")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByParticipationNumber", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.participationNumber = :participationNumber")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByEffectiveDate", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByEnteredDate", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.enteredDate = :enteredDate")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByIsDeleted", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.isDeleted = :isDeleted")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByUpdateUserName", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceReinsurerDetail.findByUpdateLast", query = "SELECT r FROM ReinsuranceReinsurerDetail r WHERE r.updateLast = :updateLast")})
public class ReinsuranceReinsurerDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceReinsurerDetailId")
    private Integer reinsuranceReinsurerDetailId;
    @Column(name = "disbursementTypeInd")
    private Integer disbursementTypeInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "cedePercent")
    private BigDecimal cedePercent;
    @Size(max = 50)
    @Column(name = "participationNumber")
    private String participationNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isDeleted")
    private boolean isDeleted;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "reinsuranceReinsurerIdFk", referencedColumnName = "reinsuranceReinsurerId")
    @ManyToOne(optional = false)
    private ReinsuranceReinsurer reinsuranceReinsurerIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public ReinsuranceReinsurerDetail() {
    }

    public ReinsuranceReinsurerDetail(Integer reinsuranceReinsurerDetailId) {
        this.reinsuranceReinsurerDetailId = reinsuranceReinsurerDetailId;
    }

    public ReinsuranceReinsurerDetail(Integer reinsuranceReinsurerDetailId, BigDecimal cedePercent, Date effectiveDate, Date enteredDate, boolean isDeleted) {
        this.reinsuranceReinsurerDetailId = reinsuranceReinsurerDetailId;
        this.cedePercent = cedePercent;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.isDeleted = isDeleted;
    }

    public Integer getReinsuranceReinsurerDetailId() {
        return reinsuranceReinsurerDetailId;
    }

    public void setReinsuranceReinsurerDetailId(Integer reinsuranceReinsurerDetailId) {
        this.reinsuranceReinsurerDetailId = reinsuranceReinsurerDetailId;
    }

    public Integer getDisbursementTypeInd() {
        return disbursementTypeInd;
    }

    public void setDisbursementTypeInd(Integer disbursementTypeInd) {
        this.disbursementTypeInd = disbursementTypeInd;
    }

    public BigDecimal getCedePercent() {
        return cedePercent;
    }

    public void setCedePercent(BigDecimal cedePercent) {
        this.cedePercent = cedePercent;
    }

    public String getParticipationNumber() {
        return participationNumber;
    }

    public void setParticipationNumber(String participationNumber) {
        this.participationNumber = participationNumber;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ReinsuranceReinsurer getReinsuranceReinsurerIdFk() {
        return reinsuranceReinsurerIdFk;
    }

    public void setReinsuranceReinsurerIdFk(ReinsuranceReinsurer reinsuranceReinsurerIdFk) {
        this.reinsuranceReinsurerIdFk = reinsuranceReinsurerIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceReinsurerDetailId != null ? reinsuranceReinsurerDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceReinsurerDetail)) {
            return false;
        }
        ReinsuranceReinsurerDetail other = (ReinsuranceReinsurerDetail) object;
        if ((this.reinsuranceReinsurerDetailId == null && other.reinsuranceReinsurerDetailId != null) || (this.reinsuranceReinsurerDetailId != null && !this.reinsuranceReinsurerDetailId.equals(other.reinsuranceReinsurerDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceReinsurerDetail[ reinsuranceReinsurerDetailId=" + reinsuranceReinsurerDetailId + " ]";
    }
    
}
