/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinstatementDisbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinstatementDisbursement.findAll", query = "SELECT r FROM ReinstatementDisbursement r")
    , @NamedQuery(name = "ReinstatementDisbursement.findByReinstatementDisbursementId", query = "SELECT r FROM ReinstatementDisbursement r WHERE r.reinstatementDisbursementId = :reinstatementDisbursementId")
    , @NamedQuery(name = "ReinstatementDisbursement.findByUpdateUserName", query = "SELECT r FROM ReinstatementDisbursement r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinstatementDisbursement.findByUpdateLast", query = "SELECT r FROM ReinstatementDisbursement r WHERE r.updateLast = :updateLast")})
public class ReinstatementDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatementDisbursementId")
    private Integer reinstatementDisbursementId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "relatedReinstatementDisbursementIdFk")
    private Collection<Ledger> ledgerCollection;
    @JoinColumn(name = "cancelDisbursementIdFk", referencedColumnName = "cancelDisbursementId")
    @ManyToOne
    private CancelDisbursement cancelDisbursementIdFk;
    @JoinColumn(name = "reinstatementDisbursementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "reinstatementIdFk", referencedColumnName = "reinstatementId")
    @ManyToOne
    private Reinstatement reinstatementIdFk;
    @OneToMany(mappedBy = "reinstatementDisbursementIdFk")
    private Collection<HoldDisbursement> holdDisbursementCollection;

    public ReinstatementDisbursement() {
    }

    public ReinstatementDisbursement(Integer reinstatementDisbursementId) {
        this.reinstatementDisbursementId = reinstatementDisbursementId;
    }

    public Integer getReinstatementDisbursementId() {
        return reinstatementDisbursementId;
    }

    public void setReinstatementDisbursementId(Integer reinstatementDisbursementId) {
        this.reinstatementDisbursementId = reinstatementDisbursementId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    public CancelDisbursement getCancelDisbursementIdFk() {
        return cancelDisbursementIdFk;
    }

    public void setCancelDisbursementIdFk(CancelDisbursement cancelDisbursementIdFk) {
        this.cancelDisbursementIdFk = cancelDisbursementIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Reinstatement getReinstatementIdFk() {
        return reinstatementIdFk;
    }

    public void setReinstatementIdFk(Reinstatement reinstatementIdFk) {
        this.reinstatementIdFk = reinstatementIdFk;
    }

    @XmlTransient
    public Collection<HoldDisbursement> getHoldDisbursementCollection() {
        return holdDisbursementCollection;
    }

    public void setHoldDisbursementCollection(Collection<HoldDisbursement> holdDisbursementCollection) {
        this.holdDisbursementCollection = holdDisbursementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinstatementDisbursementId != null ? reinstatementDisbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinstatementDisbursement)) {
            return false;
        }
        ReinstatementDisbursement other = (ReinstatementDisbursement) object;
        if ((this.reinstatementDisbursementId == null && other.reinstatementDisbursementId != null) || (this.reinstatementDisbursementId != null && !this.reinstatementDisbursementId.equals(other.reinstatementDisbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinstatementDisbursement[ reinstatementDisbursementId=" + reinstatementDisbursementId + " ]";
    }
    
}
