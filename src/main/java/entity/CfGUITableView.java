/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfGUITableView")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfGUITableView.findAll", query = "SELECT c FROM CfGUITableView c")
    , @NamedQuery(name = "CfGUITableView.findByGUITableViewId", query = "SELECT c FROM CfGUITableView c WHERE c.gUITableViewId = :gUITableViewId")
    , @NamedQuery(name = "CfGUITableView.findByGUITableName", query = "SELECT c FROM CfGUITableView c WHERE c.gUITableName = :gUITableName")
    , @NamedQuery(name = "CfGUITableView.findByViewName", query = "SELECT c FROM CfGUITableView c WHERE c.viewName = :viewName")
    , @NamedQuery(name = "CfGUITableView.findByGlobalInd", query = "SELECT c FROM CfGUITableView c WHERE c.globalInd = :globalInd")
    , @NamedQuery(name = "CfGUITableView.findByUpdateUserName", query = "SELECT c FROM CfGUITableView c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfGUITableView.findByUpdateLast", query = "SELECT c FROM CfGUITableView c WHERE c.updateLast = :updateLast")})
public class CfGUITableView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GUITableViewId")
    private Integer gUITableViewId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "GUITableName")
    private String gUITableName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "viewName")
    private String viewName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "globalInd")
    private boolean globalInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gUITableViewIdFk")
    private Collection<CfGUITableColumn> cfGUITableColumnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gUITableViewId")
    private Collection<CfUserMemberGUITableViewConn> cfUserMemberGUITableViewConnCollection;

    public CfGUITableView() {
    }

    public CfGUITableView(Integer gUITableViewId) {
        this.gUITableViewId = gUITableViewId;
    }

    public CfGUITableView(Integer gUITableViewId, String gUITableName, String viewName, boolean globalInd) {
        this.gUITableViewId = gUITableViewId;
        this.gUITableName = gUITableName;
        this.viewName = viewName;
        this.globalInd = globalInd;
    }

    public Integer getGUITableViewId() {
        return gUITableViewId;
    }

    public void setGUITableViewId(Integer gUITableViewId) {
        this.gUITableViewId = gUITableViewId;
    }

    public String getGUITableName() {
        return gUITableName;
    }

    public void setGUITableName(String gUITableName) {
        this.gUITableName = gUITableName;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public boolean getGlobalInd() {
        return globalInd;
    }

    public void setGlobalInd(boolean globalInd) {
        this.globalInd = globalInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfGUITableColumn> getCfGUITableColumnCollection() {
        return cfGUITableColumnCollection;
    }

    public void setCfGUITableColumnCollection(Collection<CfGUITableColumn> cfGUITableColumnCollection) {
        this.cfGUITableColumnCollection = cfGUITableColumnCollection;
    }

    @XmlTransient
    public Collection<CfUserMemberGUITableViewConn> getCfUserMemberGUITableViewConnCollection() {
        return cfUserMemberGUITableViewConnCollection;
    }

    public void setCfUserMemberGUITableViewConnCollection(Collection<CfUserMemberGUITableViewConn> cfUserMemberGUITableViewConnCollection) {
        this.cfUserMemberGUITableViewConnCollection = cfUserMemberGUITableViewConnCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gUITableViewId != null ? gUITableViewId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfGUITableView)) {
            return false;
        }
        CfGUITableView other = (CfGUITableView) object;
        if ((this.gUITableViewId == null && other.gUITableViewId != null) || (this.gUITableViewId != null && !this.gUITableViewId.equals(other.gUITableViewId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfGUITableView[ gUITableViewId=" + gUITableViewId + " ]";
    }
    
}
