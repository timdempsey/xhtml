/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfUserRoleConn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfUserRoleConn.findAll", query = "SELECT c FROM CfUserRoleConn c")
    , @NamedQuery(name = "CfUserRoleConn.findByUserRoleConnId", query = "SELECT c FROM CfUserRoleConn c WHERE c.userRoleConnId = :userRoleConnId")
    , @NamedQuery(name = "CfUserRoleConn.findByUpdateUserName", query = "SELECT c FROM CfUserRoleConn c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfUserRoleConn.findByUpdateLast", query = "SELECT c FROM CfUserRoleConn c WHERE c.updateLast = :updateLast")})
public class CfUserRoleConn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "UserRoleConnId")
    private Integer userRoleConnId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "MembershipRoleId", referencedColumnName = "membershipRoleId")
    @ManyToOne(optional = false)
    private CfMembershipRole membershipRoleId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public CfUserRoleConn() {
    }

    public CfUserRoleConn(Integer userRoleConnId) {
        this.userRoleConnId = userRoleConnId;
    }

    public Integer getUserRoleConnId() {
        return userRoleConnId;
    }

    public void setUserRoleConnId(Integer userRoleConnId) {
        this.userRoleConnId = userRoleConnId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfMembershipRole getMembershipRoleId() {
        return membershipRoleId;
    }

    public void setMembershipRoleId(CfMembershipRole membershipRoleId) {
        this.membershipRoleId = membershipRoleId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userRoleConnId != null ? userRoleConnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfUserRoleConn)) {
            return false;
        }
        CfUserRoleConn other = (CfUserRoleConn) object;
        if ((this.userRoleConnId == null && other.userRoleConnId != null) || (this.userRoleConnId != null && !this.userRoleConnId.equals(other.userRoleConnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfUserRoleConn[ userRoleConnId=" + userRoleConnId + " ]";
    }
    
}
