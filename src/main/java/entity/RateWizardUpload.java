/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateWizardUpload")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateWizardUpload.findAll", query = "SELECT r FROM RateWizardUpload r")
    , @NamedQuery(name = "RateWizardUpload.findByRateWizardUploadId", query = "SELECT r FROM RateWizardUpload r WHERE r.rateWizardUploadId = :rateWizardUploadId")
    , @NamedQuery(name = "RateWizardUpload.findByUploadedDate", query = "SELECT r FROM RateWizardUpload r WHERE r.uploadedDate = :uploadedDate")
    , @NamedQuery(name = "RateWizardUpload.findByRolledBackInd", query = "SELECT r FROM RateWizardUpload r WHERE r.rolledBackInd = :rolledBackInd")
    , @NamedQuery(name = "RateWizardUpload.findByRolledBackDate", query = "SELECT r FROM RateWizardUpload r WHERE r.rolledBackDate = :rolledBackDate")
    , @NamedQuery(name = "RateWizardUpload.findByUpdateUserName", query = "SELECT r FROM RateWizardUpload r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateWizardUpload.findByUpdateLast", query = "SELECT r FROM RateWizardUpload r WHERE r.updateLast = :updateLast")})
public class RateWizardUpload implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RateWizardUploadId")
    private Integer rateWizardUploadId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "uploadedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rolledBackInd")
    private boolean rolledBackInd;
    @Column(name = "rolledBackDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rolledBackDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rateWizardUploadIdFk")
    private Collection<RateWizardUploadLog> rateWizardUploadLogCollection;
    @JoinColumn(name = "fileIdFk", referencedColumnName = "fileId")
    @ManyToOne(optional = false)
    private CfFile fileIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne(optional = false)
    private PlanTable planIdFk;
    @JoinColumn(name = "rolledBackByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember rolledBackByIdFk;
    @JoinColumn(name = "uploadedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember uploadedByIdFk;
    @OneToMany(mappedBy = "rateWizardUploadIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @OneToMany(mappedBy = "rateWizardUploadIdFk")
    private Collection<RateDetail> rateDetailCollection;

    public RateWizardUpload() {
    }

    public RateWizardUpload(Integer rateWizardUploadId) {
        this.rateWizardUploadId = rateWizardUploadId;
    }

    public RateWizardUpload(Integer rateWizardUploadId, Date uploadedDate, boolean rolledBackInd) {
        this.rateWizardUploadId = rateWizardUploadId;
        this.uploadedDate = uploadedDate;
        this.rolledBackInd = rolledBackInd;
    }

    public Integer getRateWizardUploadId() {
        return rateWizardUploadId;
    }

    public void setRateWizardUploadId(Integer rateWizardUploadId) {
        this.rateWizardUploadId = rateWizardUploadId;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public boolean getRolledBackInd() {
        return rolledBackInd;
    }

    public void setRolledBackInd(boolean rolledBackInd) {
        this.rolledBackInd = rolledBackInd;
    }

    public Date getRolledBackDate() {
        return rolledBackDate;
    }

    public void setRolledBackDate(Date rolledBackDate) {
        this.rolledBackDate = rolledBackDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<RateWizardUploadLog> getRateWizardUploadLogCollection() {
        return rateWizardUploadLogCollection;
    }

    public void setRateWizardUploadLogCollection(Collection<RateWizardUploadLog> rateWizardUploadLogCollection) {
        this.rateWizardUploadLogCollection = rateWizardUploadLogCollection;
    }

    public CfFile getFileIdFk() {
        return fileIdFk;
    }

    public void setFileIdFk(CfFile fileIdFk) {
        this.fileIdFk = fileIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public UserMember getRolledBackByIdFk() {
        return rolledBackByIdFk;
    }

    public void setRolledBackByIdFk(UserMember rolledBackByIdFk) {
        this.rolledBackByIdFk = rolledBackByIdFk;
    }

    public UserMember getUploadedByIdFk() {
        return uploadedByIdFk;
    }

    public void setUploadedByIdFk(UserMember uploadedByIdFk) {
        this.uploadedByIdFk = uploadedByIdFk;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @XmlTransient
    public Collection<RateDetail> getRateDetailCollection() {
        return rateDetailCollection;
    }

    public void setRateDetailCollection(Collection<RateDetail> rateDetailCollection) {
        this.rateDetailCollection = rateDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateWizardUploadId != null ? rateWizardUploadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateWizardUpload)) {
            return false;
        }
        RateWizardUpload other = (RateWizardUpload) object;
        if ((this.rateWizardUploadId == null && other.rateWizardUploadId != null) || (this.rateWizardUploadId != null && !this.rateWizardUploadId.equals(other.rateWizardUploadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateWizardUpload[ rateWizardUploadId=" + rateWizardUploadId + " ]";
    }
    
}
