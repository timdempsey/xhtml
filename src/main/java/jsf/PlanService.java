/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.ClassTable;
import entity.ContractForm;
import entity.DtPlanEarningMethod;
import entity.DtPlanExpireTime;
import entity.DtPlanLimitDeductibleDefault;
import entity.DtPlanStartSchedule;
import entity.FinanceType;
import entity.PPMSchedule;
import entity.PlanDetail;
import entity.PlanTable;
import entity.PlanType;
import entity.Product;
import entity.ProductDetail;
import entity.ProductSalesGroup;
import entity.ProductSubType;
import entity.ProductType;
import entity.Program;
import entity.VehiclePurchaseType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "planService")
@ApplicationScoped
public class PlanService {

    /**
     * Creates a new instance of PlanService
     */
    public PlanService() {
    }

    private static final Logger LOGGER = LogManager.getLogger(PlanService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of productService
     */
    PlanSearch selectedPlan;

    private List<String> adminCompanies;

    Integer planId;

    PlanSearch plans;
    List<PlanSearch> lplans;

    //PlanSearch plans;
    //List<PlanSearch> lplans;
    Integer planActiveIndex;

    private List<String> programs;
    private List<String> insurers;

    private List<String> productSubTypes;
    private List<String> claimSettings;
    private List<String> productSalesGroup;
    private List<String> contractForms;

    private List<String> planSalesGroups;
    private List<String> planTypes;
    private List<String> vinClassingTables;
    private List<String> disbursementTypes;
    private List<String> vehiclePurchaseTypes;
    private List<String> availableFinanceTypes;
    private List<String> ppmSchedules;
    private List<String> startSchedules;
    private List<String> earningMethods;
    private List<String> defaultDeductibles;
    private List<String> limitDeductibletoDefaults;
    private List<String> planExpireTimeTypeInds;
    private List<String> products;

    Integer productId;
    
    private String myurl="test";

    public String getMyurl() {
        return myurl;
    }

    public void setMyurl(String myurl) {
        this.myurl = myurl;
    }
    
    

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public List<String> getPlanExpireTimeTypeInds() {
        return planExpireTimeTypeInds;
    }

    public void setPlanExpireTimeTypeInds(List<String> planExpireTimeTypeInds) {
        this.planExpireTimeTypeInds = planExpireTimeTypeInds;
    }

    public List<String> getDefaultDeductibles() {
        return defaultDeductibles;
    }

    public void setDefaultDeductibles(List<String> defaultDeductibles) {
        this.defaultDeductibles = defaultDeductibles;
    }

    public List<String> getLimitDeductibletoDefaults() {
        return limitDeductibletoDefaults;
    }

    public void setLimitDeductibletoDefaults(List<String> limitDeductibletoDefaults) {
        this.limitDeductibletoDefaults = limitDeductibletoDefaults;
    }

    public List<String> getEarningMethods() {
        return earningMethods;
    }

    public void setEarningMethods(List<String> earningMethods) {
        this.earningMethods = earningMethods;
    }

    public List<String> getStartSchedules() {
        return startSchedules;
    }

    public void setStartSchedules(List<String> startSchedules) {
        this.startSchedules = startSchedules;
    }

    public List<String> getPpmSchedules() {
        return ppmSchedules;
    }

    public void setPpmSchedules(List<String> ppmSchedules) {
        this.ppmSchedules = ppmSchedules;
    }

    public List<String> getAvailableFinanceTypes() {
        return availableFinanceTypes;
    }

    public void setAvailableFinanceTypes(List<String> availableFinanceTypes) {
        this.availableFinanceTypes = availableFinanceTypes;
    }

    public List<String> getVehiclePurchaseTypes() {
        return vehiclePurchaseTypes;
    }

    public void setVehiclePurchaseTypes(List<String> vehiclePurchaseTypes) {
        this.vehiclePurchaseTypes = vehiclePurchaseTypes;
    }

    //PlanSearch selectedPlan;
    public List<String> getVinClassingTables() {
        return vinClassingTables;
    }

    public void setVinClassingTables(List<String> vinClassingTables) {
        this.vinClassingTables = vinClassingTables;
    }

    public List<String> getPlanSalesGroups() {
        return planSalesGroups;
    }

    public void setPlanSalesGroups(List<String> planSalesGroups) {
        this.planSalesGroups = planSalesGroups;
    }

    public PlanSearch getSelectedPlan() {
        return selectedPlan;
    }

    public void setSelectedPlan(PlanSearch selectedPlan) {
        this.selectedPlan = selectedPlan;
    }

    public PlanSearch getPlans() {
        return plans;
    }

    public void setPlans(PlanSearch plans) {
        this.plans = plans;
    }

    public List<PlanSearch> getLplans() {
        return lplans;
    }

    public void setLplans(List<PlanSearch> lplans) {
        this.lplans = lplans;
    }

    public List<String> getProductSalesGroup() {
        return productSalesGroup;
    }

    public void setProductSalesGroup(List<String> productSalesGroup) {
        this.productSalesGroup = productSalesGroup;
    }

    public Integer getPlanActiveIndex() {
        return planActiveIndex;
    }

    public void setPlanActiveIndex(Integer planActiveIndex) {
        this.planActiveIndex = planActiveIndex;
    }

    public List<String> getPrograms() {
        return programs;
    }

    public void setPrograms(List<String> programs) {
        this.programs = programs;
    }

    public List<String> getInsurers() {
        return insurers;
    }

    public void setInsurers(List<String> insurers) {
        this.insurers = insurers;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public List<String> getPlanTypes() {
        return planTypes;
    }

    public void setPlanTypes(List<String> planTypes) {
        this.planTypes = planTypes;
    }

    public List<String> getProductSubTypes() {
        return productSubTypes;
    }

    public void setProductSubTypes(List<String> productSubTypes) {
        this.productSubTypes = productSubTypes;
    }

    public List<String> getClaimSettings() {
        return claimSettings;
    }

    public void setClaimSettings(List<String> claimSettings) {
        this.claimSettings = claimSettings;
    }

    public List<String> getContractForms() {
        return contractForms;
    }

    public void setContractForms(List<String> contractForms) {
        this.contractForms = contractForms;
    }

    public List<String> getDisbursementTypes() {
        return disbursementTypes;
    }

    public void setDisbursementTypes(List<String> disbursementTypes) {
        this.disbursementTypes = disbursementTypes;
    }

    public List<String> getAdminCompanies() {
        return adminCompanies;
    }

    public void setAdminCompanies(List<String> adminCompanies) {
        this.adminCompanies = adminCompanies;
    }

    public void reset() {
        selectedPlan = new PlanSearch();
        plans = new PlanSearch();
        planId = null;
        productId = null;
    }

    private void init() {

        lplans = claimMisc.getPlanSearchs();

        disbursementTypes = setDisbursmentTypes();
        programs = getProgramNames();
        insurers = getIns();
        productSubTypes = getSubTypes();
        claimSettings = setClaimSettings();
        productSalesGroup = setProductSalesGroup();
        contractForms = setContractForms();

        planSalesGroups = setPlanSalesGroups();
        planTypes = getTypes();
        vinClassingTables = setVinClassingTables();
        vehiclePurchaseTypes = setVehiclePurchaseTypes();
        availableFinanceTypes = setAvailableFinanceTypes();
        ppmSchedules = setPPMSchedules();
        startSchedules = setStartSchedules();
        earningMethods = setEarningMethods();
        defaultDeductibles = setDefaultDeductibles();
        limitDeductibletoDefaults = setLimitDeductibletoDefaults();
        planExpireTimeTypeInds = setPlanExpireTimeTypeInds();

        products = getProductNames();
    }

    List<String> getProductNames() {
        LOGGER.info("in getProductNames, productId=" + productId);
        List<String> names = new ArrayList<>();

        if (productId != null) {
            String name = claimMisc.getProductById(productId).getProductName();
            names.add(name);
            plans.setProductName(name);
        } else {
            List<Product> lps = claimMisc.getProductByAll();
            for (Product product : lps) {
                names.add(product.getProductName());
            }
        }
        return names;
    }

    public void getProductIdAttr(ActionEvent event) {
        productId = (Integer) event.getComponent().getAttributes().get("productId");
        LOGGER.info("in getProductIdAttr, productId=" + productId);
    }

    public String getPlanSearch() {
        LOGGER.info(" in getPlanSearch");

        init();
        reset();

        return "PlanSearch";
    }

    private List<String> setPlanExpireTimeTypeInds() {
        List<String> types = new ArrayList<>();
        List<DtPlanExpireTime> ptl = claimMisc.getDtPlanExpireTimeByAll();
        for (DtPlanExpireTime pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setLimitDeductibletoDefaults() {
        List<String> types = new ArrayList<>();
        List<DtPlanLimitDeductibleDefault> ptl = claimMisc.getDtPlanLimitDeductibleDefaultByAll();
        for (DtPlanLimitDeductibleDefault pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setDefaultDeductibles() {
        List<String> dts = new ArrayList<>();
        dts.add("None");
        return dts;
    }

    private List<String> setEarningMethods() {
        List<String> types = new ArrayList<>();
        List<DtPlanEarningMethod> ptl = claimMisc.getDtPlanEarningMethodByAll();
        for (DtPlanEarningMethod pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setStartSchedules() {
        List<String> types = new ArrayList<>();
        List<DtPlanStartSchedule> ptl = claimMisc.getDtPlanStartScheduleByAll();
        for (DtPlanStartSchedule pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setPPMSchedules() {
        List<String> types = new ArrayList<>();
        List<PPMSchedule> ptl = claimMisc.getPPMScheduleByAll();
        for (PPMSchedule pt : ptl) {
            types.add(pt.getName());
        }
        return types;
    }

    private List<String> setAvailableFinanceTypes() {
        List<String> types = new ArrayList<>();
        List<FinanceType> ptl = claimMisc.getFinanceTypeByAll();
        for (FinanceType pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setVehiclePurchaseTypes() {
        List<String> types = new ArrayList<>();
        List<VehiclePurchaseType> ptl = claimMisc.getVehiclePurchaseTypeByAll();
        for (VehiclePurchaseType pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setPlanSalesGroups() {
        List<String> dts = new ArrayList<>();
        dts.add("None");
        return dts;
    }

    private List<String> setVinClassingTables() {
        List<String> types = new ArrayList<>();
        List<ClassTable> ptl = claimMisc.getClassTableByAll();
        for (ClassTable pt : ptl) {
            types.add(pt.getName());
        }
        return types;
    }

    private List<String> setProductSalesGroup() {
        List<String> types = new ArrayList<>();
        List<ProductSalesGroup> ptl = claimMisc.getProductSalesGroupByAll();
        for (ProductSalesGroup pt : ptl) {
            types.add(pt.getName());
        }
        return types;
    }

    private List<String> setContractForms() {
        List<String> types = new ArrayList<>();
        List<ContractForm> ptl = claimMisc.getContractFormByAll();
        for (ContractForm pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setClaimSettings() {
        List<String> dts = new ArrayList<>();
        dts.add("None");
        return dts;
    }

    private List<String> setDisbursmentTypes() {
        List<String> dts = new ArrayList<>();
        dts.add("[Direct Disbursements]");
        return dts;
    }

    List<String> getProgramNames() {
        List<String> names = new ArrayList<>();
        List<Program> lps = claimMisc.getProgramByAll();
        for (Program program : lps) {
            names.add(program.getName());
        }
        return names;
    }

    List<String> getIns() {
        List<String> ins = new ArrayList<>();
        List<AccountKeeper> akl = claimMisc.getAccountKeeperByInsurer();
        for (AccountKeeper ak : akl) {
            ins.add(ak.getAccountKeeperName());
        }
        return ins;
    }

    List<String> getTypes() {
        List<String> types = new ArrayList<>();
        List<PlanType> ptl = claimMisc.getPlanTypeByAll();
        for (PlanType pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    List<String> getSubTypes() {
        List<String> types = new ArrayList<>();
        List<ProductSubType> ptl = claimMisc.getProductSubTypeByAll();
        for (ProductSubType pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    public void onRowSelect(SelectEvent event) {
        selectedPlan = (PlanSearch) event.getObject();

        FacesMessage msg = new FacesMessage("Plan Selected", ((PlanSearch) event.getObject()).getPlanName());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void addPlan() {
        LOGGER.info("in addPlan");

        if (plans != null) {
            plans.reset();
        } else {
            plans = new PlanSearch();
        }

        init();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "650");
        options.put("width", "2000");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addPlan", options, null);

    }

    public void savePlan() {

        LOGGER.info("in savePlan, planId=" + planId);
        try {
            utx.begin();
            if (planId == null) {
                String updateUserName = "JP";
                BigDecimal planOrder = null;
                productId = claimMisc.getProductByName(plans.getProductName()).getProductId();

                Query query = em.createNativeQuery("insert into PlanTable (planName, planCode, planOrder, productIdFk, updateUserName ) values (?1, ?2, ?3, ?4, ?5)");
                query.setParameter(1, plans.getPlanName());
                query.setParameter(2, plans.getPlanCode());
                query.setParameter(3, planOrder);
                query.setParameter(4, productId);
                query.setParameter(5, updateUserName);

                int ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                if (ret == 1) {
                    Integer planIdFk = claimMisc.getLatestInsertedId("PlanTable");
                    Integer enteredByIdFk = 21367; // later
                    LOGGER.info("effectiveDate=" + plans.getEffectiveDate());
                    Integer deletedInd = 0;
                    Integer extendExpirationByWait = 0;
                    Integer disbursementTable = 1;
                    Integer defaultDeductibleIdFk = null;    // later

                    query = em.createNativeQuery("insert into PlanDetail (productTypeIdFk, PurchaseTypeInd, availableVehiclePurchaseTypes, waitDays, earningMethodInd, effectiveDate,  enteredByIdFk, enteredDate, deletedInd, defaultTransferFee, "
                            + "planExpireTimeTypeInd, cancelRequestValidDays, cancelRequestValidMiles, extendExpirationByWait, description, planIdFk, classTableIdFk, disbursementCenterIdFk, defaultDeductibleIdFk, updateUserName, expireDate) "
                            + "values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21)");
                    query.setParameter(1, claimMisc.getProductById(productId).getProductTypeIdFk().getProductTypeId());
                    if (plans.getAvailableFinanceType() != null) {
                        query.setParameter(2, claimMisc.getFinanceTypeByName(plans.getAvailableFinanceType()).getFinanceTypeId());
                    } else {
                        query.setParameter(2, "");
                    }
                    if( plans.getVehiclePurchaseType() != null ) {
                    query.setParameter(3, claimMisc.getVehiclePurchaseTypeByName(plans.getVehiclePurchaseType()).getVehiclePurchaseTypeId());
                    } else {
                        query.setParameter(3, "");
                    }
                    query.setParameter(4, plans.getWaitDays());
                    if(plans.getEarningMethod()!=null ){
                    query.setParameter(5, claimMisc.getDtPlanEarningMethodByName(plans.getEarningMethod()).getEarningMethodId());
                    } else {
                        query.setParameter(5, "");
                    }
                    query.setParameter(6, claimMisc.convertFromUtilDate(plans.getEffectiveDate()));
                    query.setParameter(7, enteredByIdFk);
                    query.setParameter(8, claimMisc.getCurrnetDateTime());
                    query.setParameter(9, deletedInd);
                    query.setParameter(10, plans.getDefaultTransferFee());
                    Integer planExpireTimeTypeInd =  null;
                    if( plans.getPlanExpireTimeTypeInd() != null ) {
                        planExpireTimeTypeInd = claimMisc.getDtPlanExpireTimeByName(plans.getPlanExpireTimeTypeInd()).getExpireTimeId();
                    }
                    LOGGER.info("planExpireTimeTypeInd=" + planExpireTimeTypeInd);
                    query.setParameter(11, planExpireTimeTypeInd);
                    query.setParameter(12, plans.getCancelRequestValidDays());
                    query.setParameter(13, plans.getCancelRequestValidMiles());
                    query.setParameter(14, extendExpirationByWait);
                    query.setParameter(15, plans.getDescription());
                    query.setParameter(16, planIdFk);
                    Integer classTableId = null;
                    if(plans.getVinClassingTable()!=null ) {
                        classTableId = claimMisc.getClassTableByName(plans.getVinClassingTable()).getClassTableId();
                    }
                    query.setParameter(17, classTableId);
                    query.setParameter(18, disbursementTable);
                    query.setParameter(19, defaultDeductibleIdFk);
                    query.setParameter(20, updateUserName);
                    if (plans.getExpireDate() != null) {
                        query.setParameter(21, claimMisc.convertFromUtilDate(plans.getExpireDate()));
                    } else {
                        query.setParameter(21, plans.getExpireDate());
                    }

                    ret = query.executeUpdate();
                    LOGGER.info("PlanDetail insertion, ret=" + ret);
                }
            } else {
                PlanTable plan = claimMisc.getPlanById(planId);
                PlanDetail pd = claimMisc.getPlanDetailByFkId(planId);
                plan.setPlanName(plans.getPlanName());
                plan.setPlanCode(plans.getPlanCode());
                pd.setDescription(plans.getDescription());
                pd.setEffectiveDate(plans.getEffectiveDate());
                em.merge(plan);
                em.merge(pd);

            }
            utx.commit();
            RequestContext.getCurrentInstance().closeDialog("newProduct");

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        lplans = claimMisc.getPlanSearchByProductId(productId);
    }

    public void checkPlanName() {
        if (claimMisc.getProgramByName(plans.getPlanName()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This plan name already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void checkPlanCode() {
        if (claimMisc.getProductByCode(plans.getPlanCode()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This plan code already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void getPlanIdAttr(ActionEvent event) {
        planId = (Integer) event.getComponent().getAttributes().get("planId");
        LOGGER.info("in getPlanIdAttr, planId=" + planId);
    }

    /*
    public String getPlanRate() {
        planId = selectedPlan.getPlanId();
        LOGGER.info("in getPlanRate, planId=" + planId);
        lrates = claimMisc.getRateSearchByPlanId(planId);
        return "PlanRate";
    }
     */
    public String deletePlan() {
        planId = selectedPlan.getPlanId();
        LOGGER.info("in deletePlan, planId=" + planId);
        claimMisc.deletePlan(planId);
        lplans = claimMisc.getPlanSearchs();
        return "PlanSearch";
    }

    public void editPlan() {
        LOGGER.info("in editPlan");

        plans.setAdmimCompany(selectedPlan.getAdmimCompany());
        plans.setDeductibleDescription(selectedPlan.getDeductibleDescription());
        plans.setDescription(selectedPlan.getDescription());
        plans.setEffectiveDate(selectedPlan.getEffectiveDate());
        plans.setExpireDate(selectedPlan.getExpireDate());
        plans.setInsurer(selectedPlan.getInsurer());
        plans.setPlanCode(selectedPlan.getPlanCode());
        plans.setPlanId(selectedPlan.getPlanId());
        plans.setPlanName(selectedPlan.getPlanName());
        plans.setPlanType(selectedPlan.getPlanType());
        plans.setProduct(selectedPlan.getProduct());
        plans.setProgram(selectedPlan.getProgram());
        plans.setVinClassingTable(selectedPlan.getVinClassingTable());

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addPlan", options, null);

    }

    public void planEditClosed() {

        LOGGER.info("in planEditClosed, description=" + plans.getDescription());

        selectedPlan.setAdmimCompany(plans.getAdmimCompany());
        selectedPlan.setDeductibleDescription(plans.getDeductibleDescription());
        selectedPlan.setDescription(plans.getDescription());
        selectedPlan.setEffectiveDate(plans.getEffectiveDate());
        selectedPlan.setExpireDate(plans.getExpireDate());
        selectedPlan.setInsurer(plans.getInsurer());
        selectedPlan.setPlanCode(plans.getPlanCode());
        selectedPlan.setPlanId(plans.getPlanId());
        selectedPlan.setPlanName(plans.getPlanName());
        selectedPlan.setPlanType(plans.getPlanType());
        selectedPlan.setProduct(plans.getProduct());
        selectedPlan.setProgram(plans.getProgram());
        selectedPlan.setVinClassingTable(plans.getVinClassingTable());

    }

}
