/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfPDF")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfPDF.findAll", query = "SELECT c FROM CfPDF c")
    , @NamedQuery(name = "CfPDF.findByPDFId", query = "SELECT c FROM CfPDF c WHERE c.pDFId = :pDFId")
    , @NamedQuery(name = "CfPDF.findByName", query = "SELECT c FROM CfPDF c WHERE c.name = :name")
    , @NamedQuery(name = "CfPDF.findByDescription", query = "SELECT c FROM CfPDF c WHERE c.description = :description")
    , @NamedQuery(name = "CfPDF.findByTypeInd", query = "SELECT c FROM CfPDF c WHERE c.typeInd = :typeInd")
    , @NamedQuery(name = "CfPDF.findByUpdateUserName", query = "SELECT c FROM CfPDF c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfPDF.findByUpdateLast", query = "SELECT c FROM CfPDF c WHERE c.updateLast = :updateLast")})
public class CfPDF implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PDFId")
    private Integer pDFId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "typeInd")
    private int typeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pDFIdFk")
    private Collection<CfPDFDetail> cfPDFDetailCollection;

    public CfPDF() {
    }

    public CfPDF(Integer pDFId) {
        this.pDFId = pDFId;
    }

    public CfPDF(Integer pDFId, String name, String description, int typeInd) {
        this.pDFId = pDFId;
        this.name = name;
        this.description = description;
        this.typeInd = typeInd;
    }

    public Integer getPDFId() {
        return pDFId;
    }

    public void setPDFId(Integer pDFId) {
        this.pDFId = pDFId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTypeInd() {
        return typeInd;
    }

    public void setTypeInd(int typeInd) {
        this.typeInd = typeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfPDFDetail> getCfPDFDetailCollection() {
        return cfPDFDetailCollection;
    }

    public void setCfPDFDetailCollection(Collection<CfPDFDetail> cfPDFDetailCollection) {
        this.cfPDFDetailCollection = cfPDFDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pDFId != null ? pDFId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfPDF)) {
            return false;
        }
        CfPDF other = (CfPDF) object;
        if ((this.pDFId == null && other.pDFId != null) || (this.pDFId != null && !this.pDFId.equals(other.pDFId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfPDF[ pDFId=" + pDFId + " ]";
    }
    
}
