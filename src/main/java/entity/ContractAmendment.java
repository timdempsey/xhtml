/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractAmendment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractAmendment.findAll", query = "SELECT c FROM ContractAmendment c")
    , @NamedQuery(name = "ContractAmendment.findByContractAmendmentId", query = "SELECT c FROM ContractAmendment c WHERE c.contractAmendmentId = :contractAmendmentId")
    , @NamedQuery(name = "ContractAmendment.findByAmendmentTypeInd", query = "SELECT c FROM ContractAmendment c WHERE c.amendmentTypeInd = :amendmentTypeInd")
    , @NamedQuery(name = "ContractAmendment.findByRemitAmount", query = "SELECT c FROM ContractAmendment c WHERE c.remitAmount = :remitAmount")
    , @NamedQuery(name = "ContractAmendment.findByUpdateUserName", query = "SELECT c FROM ContractAmendment c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractAmendment.findByUpdateLast", query = "SELECT c FROM ContractAmendment c WHERE c.updateLast = :updateLast")})
public class ContractAmendment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ContractAmendmentId")
    private Integer contractAmendmentId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amendmentTypeInd")
    private int amendmentTypeInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "RemitAmount")
    private BigDecimal remitAmount;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "contractAmendmentIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;
    @OneToMany(mappedBy = "relatedAmendmentIdFk")
    private Collection<Ledger> ledgerCollection;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "ContractAmendmentId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public ContractAmendment() {
    }

    public ContractAmendment(Integer contractAmendmentId) {
        this.contractAmendmentId = contractAmendmentId;
    }

    public ContractAmendment(Integer contractAmendmentId, int amendmentTypeInd) {
        this.contractAmendmentId = contractAmendmentId;
        this.amendmentTypeInd = amendmentTypeInd;
    }

    public Integer getContractAmendmentId() {
        return contractAmendmentId;
    }

    public void setContractAmendmentId(Integer contractAmendmentId) {
        this.contractAmendmentId = contractAmendmentId;
    }

    public int getAmendmentTypeInd() {
        return amendmentTypeInd;
    }

    public void setAmendmentTypeInd(int amendmentTypeInd) {
        this.amendmentTypeInd = amendmentTypeInd;
    }

    public BigDecimal getRemitAmount() {
        return remitAmount;
    }

    public void setRemitAmount(BigDecimal remitAmount) {
        this.remitAmount = remitAmount;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractAmendmentId != null ? contractAmendmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractAmendment)) {
            return false;
        }
        ContractAmendment other = (ContractAmendment) object;
        if ((this.contractAmendmentId == null && other.contractAmendmentId != null) || (this.contractAmendmentId != null && !this.contractAmendmentId.equals(other.contractAmendmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractAmendment[ contractAmendmentId=" + contractAmendmentId + " ]";
    }
    
}
