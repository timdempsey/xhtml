/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class QuickRater {

    private static final Logger LOGGER = LogManager.getLogger(QuickRater.class);

    public QuickRater() {
    }
    

    public QuickRater(String vinNo, String dealerName, Integer dealerId, Integer mileage, String purchaseType) {
        this.vinNo = vinNo;
        this.dealerName = dealerName;
        this.dealerId = dealerId;
        this.mileage = mileage;
        this.purchaseType = purchaseType;
    }

    private String vinNo;
    private String dealerName;
    private Integer dealerId;
    private Integer mileage;
    private String purchaseType;

    public String getVinNo() {
        return vinNo;
    }

    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public void reset() {
        vinNo = "";
        dealerName = "";
        dealerId = null;
        mileage = null;
        purchaseType = "";
    }

}
