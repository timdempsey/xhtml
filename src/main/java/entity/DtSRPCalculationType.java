/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtSRPCalculationType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtSRPCalculationType.findAll", query = "SELECT d FROM DtSRPCalculationType d")
    , @NamedQuery(name = "DtSRPCalculationType.findBySRPCalculationTypeId", query = "SELECT d FROM DtSRPCalculationType d WHERE d.sRPCalculationTypeId = :sRPCalculationTypeId")
    , @NamedQuery(name = "DtSRPCalculationType.findByDescription", query = "SELECT d FROM DtSRPCalculationType d WHERE d.description = :description")})
public class DtSRPCalculationType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRPCalculationTypeId")
    private Integer sRPCalculationTypeId;
    @Size(max = 60)
    @Column(name = "Description")
    private String description;

    public DtSRPCalculationType() {
    }

    public DtSRPCalculationType(Integer sRPCalculationTypeId) {
        this.sRPCalculationTypeId = sRPCalculationTypeId;
    }

    public Integer getSRPCalculationTypeId() {
        return sRPCalculationTypeId;
    }

    public void setSRPCalculationTypeId(Integer sRPCalculationTypeId) {
        this.sRPCalculationTypeId = sRPCalculationTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sRPCalculationTypeId != null ? sRPCalculationTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtSRPCalculationType)) {
            return false;
        }
        DtSRPCalculationType other = (DtSRPCalculationType) object;
        if ((this.sRPCalculationTypeId == null && other.sRPCalculationTypeId != null) || (this.sRPCalculationTypeId != null && !this.sRPCalculationTypeId.equals(other.sRPCalculationTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtSRPCalculationType[ sRPCalculationTypeId=" + sRPCalculationTypeId + " ]";
    }
    
}
