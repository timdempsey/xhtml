/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ContractFirstLine implements Serializable {
    

    private static final Logger LOGGER = LogManager.getLogger(ContractFirstLine.class);

    public ContractFirstLine() {
    }
    
    

    public ContractFirstLine(String product, String productType, String effectDate, String expireDate, String contractNo, String contractStatus, String customer, String vin, String accountHolder, String agent, BigDecimal purchasePrice) {
        this.product = product;
        this.productType = productType;
        this.effectDate = effectDate;
        this.expireDate = expireDate;
        this.contractNo = contractNo;
        this.contractStatus = contractStatus;
        this.customer = customer;
        this.vin = vin;
        this.accountHolder = accountHolder;
        this.agent = agent;
        this.purchasePrice = purchasePrice;
    }

    
    
    
  
    private String product;
    private String productType;
    private String effectDate;
    private String expireDate;
    private String contractNo;
    private String contractStatus;
    private String customer;
    private String vin;
    private String accountHolder;
    private String agent;
    private BigDecimal  purchasePrice;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(String effectDate) {
        this.effectDate = effectDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    


    
}