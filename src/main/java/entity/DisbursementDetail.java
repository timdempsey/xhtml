/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DisbursementDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisbursementDetail.findAll", query = "SELECT d FROM DisbursementDetail d")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementDetailId", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementDetailId = :disbursementDetailId")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementAmount", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementAmount = :disbursementAmount")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementTypeInd", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementTypeInd = :disbursementTypeInd")
    , @NamedQuery(name = "DisbursementDetail.findByCededReserveInd", query = "SELECT d FROM DisbursementDetail d WHERE d.cededReserveInd = :cededReserveInd")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementAmountTypeInd", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementAmountTypeInd = :disbursementAmountTypeInd")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementRefundTypeInd", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementRefundTypeInd = :disbursementRefundTypeInd")
    , @NamedQuery(name = "DisbursementDetail.findByHiddenInd", query = "SELECT d FROM DisbursementDetail d WHERE d.hiddenInd = :hiddenInd")
    , @NamedQuery(name = "DisbursementDetail.findByDisbursementMemo", query = "SELECT d FROM DisbursementDetail d WHERE d.disbursementMemo = :disbursementMemo")
    , @NamedQuery(name = "DisbursementDetail.findByMinimumAmount", query = "SELECT d FROM DisbursementDetail d WHERE d.minimumAmount = :minimumAmount")
    , @NamedQuery(name = "DisbursementDetail.findByMaximumAmount", query = "SELECT d FROM DisbursementDetail d WHERE d.maximumAmount = :maximumAmount")
    , @NamedQuery(name = "DisbursementDetail.findByCalculatedOnMinimumApplies", query = "SELECT d FROM DisbursementDetail d WHERE d.calculatedOnMinimumApplies = :calculatedOnMinimumApplies")
    , @NamedQuery(name = "DisbursementDetail.findByCalculatedOnMaximumApplies", query = "SELECT d FROM DisbursementDetail d WHERE d.calculatedOnMaximumApplies = :calculatedOnMaximumApplies")
    , @NamedQuery(name = "DisbursementDetail.findByPerAmountFinanced", query = "SELECT d FROM DisbursementDetail d WHERE d.perAmountFinanced = :perAmountFinanced")
    , @NamedQuery(name = "DisbursementDetail.findByReduceCalculatedOn", query = "SELECT d FROM DisbursementDetail d WHERE d.reduceCalculatedOn = :reduceCalculatedOn")
    , @NamedQuery(name = "DisbursementDetail.findByRenewableInd", query = "SELECT d FROM DisbursementDetail d WHERE d.renewableInd = :renewableInd")
    , @NamedQuery(name = "DisbursementDetail.findByEffectiveDate", query = "SELECT d FROM DisbursementDetail d WHERE d.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "DisbursementDetail.findByEnteredDate", query = "SELECT d FROM DisbursementDetail d WHERE d.enteredDate = :enteredDate")
    , @NamedQuery(name = "DisbursementDetail.findByDeletedInd", query = "SELECT d FROM DisbursementDetail d WHERE d.deletedInd = :deletedInd")
    , @NamedQuery(name = "DisbursementDetail.findByIgnoredInd", query = "SELECT d FROM DisbursementDetail d WHERE d.ignoredInd = :ignoredInd")
    , @NamedQuery(name = "DisbursementDetail.findByIgnoredDate", query = "SELECT d FROM DisbursementDetail d WHERE d.ignoredDate = :ignoredDate")
    , @NamedQuery(name = "DisbursementDetail.findByNoChargeBackGroupIdFk", query = "SELECT d FROM DisbursementDetail d WHERE d.noChargeBackGroupIdFk = :noChargeBackGroupIdFk")
    , @NamedQuery(name = "DisbursementDetail.findByUpdateUserName", query = "SELECT d FROM DisbursementDetail d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DisbursementDetail.findByUpdateLast", query = "SELECT d FROM DisbursementDetail d WHERE d.updateLast = :updateLast")
    , @NamedQuery(name = "DisbursementDetail.findByExpireDate", query = "SELECT d FROM DisbursementDetail d WHERE d.expireDate = :expireDate")})
public class DisbursementDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementDetailId")
    private Integer disbursementDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "disbursementAmount")
    private BigDecimal disbursementAmount;
    @Column(name = "disbursementTypeInd")
    private Integer disbursementTypeInd;
    @Column(name = "cededReserveInd")
    private Boolean cededReserveInd;
    @Column(name = "disbursementAmountTypeInd")
    private Integer disbursementAmountTypeInd;
    @Column(name = "disbursementRefundTypeInd")
    private Integer disbursementRefundTypeInd;
    @Column(name = "hiddenInd")
    private Boolean hiddenInd;
    @Size(max = 100)
    @Column(name = "disbursementMemo")
    private String disbursementMemo;
    @Column(name = "minimumAmount")
    private BigDecimal minimumAmount;
    @Column(name = "maximumAmount")
    private BigDecimal maximumAmount;
    @Column(name = "calculatedOnMinimumApplies")
    private BigDecimal calculatedOnMinimumApplies;
    @Column(name = "calculatedOnMaximumApplies")
    private BigDecimal calculatedOnMaximumApplies;
    @Column(name = "perAmountFinanced")
    private BigDecimal perAmountFinanced;
    @Column(name = "reduceCalculatedOn")
    private Boolean reduceCalculatedOn;
    @Column(name = "renewableInd")
    private Boolean renewableInd;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Column(name = "ignoredInd")
    private Boolean ignoredInd;
    @Column(name = "ignoredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ignoredDate;
    @Column(name = "noChargeBackGroupIdFk")
    private Integer noChargeBackGroupIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @Column(name = "expireDate")
    @Temporal(TemporalType.DATE)
    private Date expireDate;
    @OneToMany(mappedBy = "disbursementDetailIdFk")
    private Collection<Note> noteCollection;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "disbursementIdFk", referencedColumnName = "DisbursementId")
    @ManyToOne
    private Disbursement disbursementIdFk;
    @JoinColumn(name = "disbursementCodeIdFk", referencedColumnName = "disbursementCodeId")
    @ManyToOne
    private DisbursementCode disbursementCodeIdFk;
    @JoinColumn(name = "calculatedOnDisbursementCodeIdFk", referencedColumnName = "disbursementCodeId")
    @ManyToOne
    private DisbursementCode calculatedOnDisbursementCodeIdFk;
    @JoinColumn(name = "disbursementGroupIdFk", referencedColumnName = "disbursementGroupId")
    @ManyToOne
    private DisbursementGroup disbursementGroupIdFk;
    @JoinColumn(name = "rateWizardUploadIdFk", referencedColumnName = "RateWizardUploadId")
    @ManyToOne
    private RateWizardUpload rateWizardUploadIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;
    @JoinColumn(name = "ignoredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember ignoredByIdFk;
    @OneToMany(mappedBy = "disbursementDetailIdFk")
    private Collection<HistoryLog> historyLogCollection;

    public DisbursementDetail() {
    }

    public DisbursementDetail(Integer disbursementDetailId) {
        this.disbursementDetailId = disbursementDetailId;
    }

    public Integer getDisbursementDetailId() {
        return disbursementDetailId;
    }

    public void setDisbursementDetailId(Integer disbursementDetailId) {
        this.disbursementDetailId = disbursementDetailId;
    }

    public BigDecimal getDisbursementAmount() {
        return disbursementAmount;
    }

    public void setDisbursementAmount(BigDecimal disbursementAmount) {
        this.disbursementAmount = disbursementAmount;
    }

    public Integer getDisbursementTypeInd() {
        return disbursementTypeInd;
    }

    public void setDisbursementTypeInd(Integer disbursementTypeInd) {
        this.disbursementTypeInd = disbursementTypeInd;
    }

    public Boolean getCededReserveInd() {
        return cededReserveInd;
    }

    public void setCededReserveInd(Boolean cededReserveInd) {
        this.cededReserveInd = cededReserveInd;
    }

    public Integer getDisbursementAmountTypeInd() {
        return disbursementAmountTypeInd;
    }

    public void setDisbursementAmountTypeInd(Integer disbursementAmountTypeInd) {
        this.disbursementAmountTypeInd = disbursementAmountTypeInd;
    }

    public Integer getDisbursementRefundTypeInd() {
        return disbursementRefundTypeInd;
    }

    public void setDisbursementRefundTypeInd(Integer disbursementRefundTypeInd) {
        this.disbursementRefundTypeInd = disbursementRefundTypeInd;
    }

    public Boolean getHiddenInd() {
        return hiddenInd;
    }

    public void setHiddenInd(Boolean hiddenInd) {
        this.hiddenInd = hiddenInd;
    }

    public String getDisbursementMemo() {
        return disbursementMemo;
    }

    public void setDisbursementMemo(String disbursementMemo) {
        this.disbursementMemo = disbursementMemo;
    }

    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public BigDecimal getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(BigDecimal maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public BigDecimal getCalculatedOnMinimumApplies() {
        return calculatedOnMinimumApplies;
    }

    public void setCalculatedOnMinimumApplies(BigDecimal calculatedOnMinimumApplies) {
        this.calculatedOnMinimumApplies = calculatedOnMinimumApplies;
    }

    public BigDecimal getCalculatedOnMaximumApplies() {
        return calculatedOnMaximumApplies;
    }

    public void setCalculatedOnMaximumApplies(BigDecimal calculatedOnMaximumApplies) {
        this.calculatedOnMaximumApplies = calculatedOnMaximumApplies;
    }

    public BigDecimal getPerAmountFinanced() {
        return perAmountFinanced;
    }

    public void setPerAmountFinanced(BigDecimal perAmountFinanced) {
        this.perAmountFinanced = perAmountFinanced;
    }

    public Boolean getReduceCalculatedOn() {
        return reduceCalculatedOn;
    }

    public void setReduceCalculatedOn(Boolean reduceCalculatedOn) {
        this.reduceCalculatedOn = reduceCalculatedOn;
    }

    public Boolean getRenewableInd() {
        return renewableInd;
    }

    public void setRenewableInd(Boolean renewableInd) {
        this.renewableInd = renewableInd;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Boolean getIgnoredInd() {
        return ignoredInd;
    }

    public void setIgnoredInd(Boolean ignoredInd) {
        this.ignoredInd = ignoredInd;
    }

    public Date getIgnoredDate() {
        return ignoredDate;
    }

    public void setIgnoredDate(Date ignoredDate) {
        this.ignoredDate = ignoredDate;
    }

    public Integer getNoChargeBackGroupIdFk() {
        return noChargeBackGroupIdFk;
    }

    public void setNoChargeBackGroupIdFk(Integer noChargeBackGroupIdFk) {
        this.noChargeBackGroupIdFk = noChargeBackGroupIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public Disbursement getDisbursementIdFk() {
        return disbursementIdFk;
    }

    public void setDisbursementIdFk(Disbursement disbursementIdFk) {
        this.disbursementIdFk = disbursementIdFk;
    }

    public DisbursementCode getDisbursementCodeIdFk() {
        return disbursementCodeIdFk;
    }

    public void setDisbursementCodeIdFk(DisbursementCode disbursementCodeIdFk) {
        this.disbursementCodeIdFk = disbursementCodeIdFk;
    }

    public DisbursementCode getCalculatedOnDisbursementCodeIdFk() {
        return calculatedOnDisbursementCodeIdFk;
    }

    public void setCalculatedOnDisbursementCodeIdFk(DisbursementCode calculatedOnDisbursementCodeIdFk) {
        this.calculatedOnDisbursementCodeIdFk = calculatedOnDisbursementCodeIdFk;
    }

    public DisbursementGroup getDisbursementGroupIdFk() {
        return disbursementGroupIdFk;
    }

    public void setDisbursementGroupIdFk(DisbursementGroup disbursementGroupIdFk) {
        this.disbursementGroupIdFk = disbursementGroupIdFk;
    }

    public RateWizardUpload getRateWizardUploadIdFk() {
        return rateWizardUploadIdFk;
    }

    public void setRateWizardUploadIdFk(RateWizardUpload rateWizardUploadIdFk) {
        this.rateWizardUploadIdFk = rateWizardUploadIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public UserMember getIgnoredByIdFk() {
        return ignoredByIdFk;
    }

    public void setIgnoredByIdFk(UserMember ignoredByIdFk) {
        this.ignoredByIdFk = ignoredByIdFk;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementDetailId != null ? disbursementDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisbursementDetail)) {
            return false;
        }
        DisbursementDetail other = (DisbursementDetail) object;
        if ((this.disbursementDetailId == null && other.disbursementDetailId != null) || (this.disbursementDetailId != null && !this.disbursementDetailId.equals(other.disbursementDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DisbursementDetail[ disbursementDetailId=" + disbursementDetailId + " ]";
    }
    
}
