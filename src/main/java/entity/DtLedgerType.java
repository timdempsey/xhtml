/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtLedgerType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtLedgerType.findAll", query = "SELECT d FROM DtLedgerType d")
    , @NamedQuery(name = "DtLedgerType.findByLedgerTypeId", query = "SELECT d FROM DtLedgerType d WHERE d.ledgerTypeId = :ledgerTypeId")
    , @NamedQuery(name = "DtLedgerType.findByDescription", query = "SELECT d FROM DtLedgerType d WHERE d.description = :description")})
public class DtLedgerType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ledgerTypeId")
    private Integer ledgerTypeId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;

    public DtLedgerType() {
    }

    public DtLedgerType(Integer ledgerTypeId) {
        this.ledgerTypeId = ledgerTypeId;
    }

    public Integer getLedgerTypeId() {
        return ledgerTypeId;
    }

    public void setLedgerTypeId(Integer ledgerTypeId) {
        this.ledgerTypeId = ledgerTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ledgerTypeId != null ? ledgerTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtLedgerType)) {
            return false;
        }
        DtLedgerType other = (DtLedgerType) object;
        if ((this.ledgerTypeId == null && other.ledgerTypeId != null) || (this.ledgerTypeId != null && !this.ledgerTypeId.equals(other.ledgerTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtLedgerType[ ledgerTypeId=" + ledgerTypeId + " ]";
    }
    
}
