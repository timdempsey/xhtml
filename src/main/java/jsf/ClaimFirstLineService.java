/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Claim;
import entity.Contracts;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimFirstLineService")
@ApplicationScoped
public class ClaimFirstLineService implements Serializable {

    //private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLineService.class.getName());
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLineService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    /**
     * Creates a new instance of ClaimFirstLineService
     */
    public ClaimFirstLineService() {

    }

    @Inject
    transient private ClaimMisc claimMisc;

    /**
     *
     * @param cl
     * @return
     */
    public List<?> createClaimView(List<Claim> cl) {

        List<ClaimFirstLine> list = new ArrayList<>();

        for (Claim claim : cl) {

            String claimNumber = claim.getClaimNumber();
            //LOGGER.info("createClaimView claimNumber=" + claimNumber);

            int claimId = claim.getClaimId();
            String product = null;
            if (claim.getProductTypeIdFk() != null) {
                product = claimMisc.getProductTypeByClaimNumber(claimNumber).getProductTypeName();
            }
            //String assignedTo = claim.getAssignedTo();
            String assignedTo = null;
            if (claim.getOwnedByFk() != null) {
                assignedTo = claim.getOwnedByFk().getUserName();
            }
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String InceptionDate = null;
            if (claim.getInceptionDate() != null) {
                InceptionDate = df.format(claim.getInceptionDate());
            }
            String claimStatus = claim.getClaimStatus().getClaimStatusDesc();
            String claimSubStatus = claim.getClaimSubStatus().getClaimSubstatusDesc();
            Contracts contract = claimMisc.getContractsByClaimNum(claimNumber);
            String contractNo = null;
            String contractStatus = null;
            String customer = null;
            String paidTo = null;
            if (contract != null) {
                contractNo = contract.getContractNo();
                contractStatus = contract.getContractStatusIdFk().getContractStatusName();
                AccountKeeper ak = claimMisc.getAccountKeeperByContractNumber(contract.getContractNo());
                customer = ak.getAccountKeeperName();
                ak = claimMisc.getAccountKeeperByClaimNumber(claimNumber);
                paidTo = ak.getAccountKeeperName();
            }
            Float amount = null;
            if (claim.getAmount() != null) {
                amount = claim.getAmount().floatValue();
            }
            //LOGGER.info("amount=" + amount);

            list.add(new ClaimFirstLine(product, claimNumber, assignedTo, InceptionDate, claimStatus, claimSubStatus, contractNo, contractStatus, customer, paidTo, amount));
            //LOGGER.info("======end of loop for " + claimNumber);

        }

        LOGGER.info("size of list=" + list.size());
        return list;
    }

}
