/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Contracts;
import entity.VinDesc;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractGeneralService")
@ApplicationScoped
public class ContractGeneralService {

    //private static final Logger LOGGER = LogManager.getLogger(ClaimDTSService.class);
    private static final Logger LOGGER = LogManager.getLogger(ContractGeneralService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    @Inject
    ClaimFirstLineView claimFirstLineView;

    @Inject
    ContractNoteService contractNoteService;

    @Inject
    ContractRatingService contractRatingService;

    @Inject
    ContractFieldService contractFieldService;
    
    @Inject
    ContractCancelService contractCancelService;
    

    /**
     * Creates a new instance of ContractDetailService
     */
    public ContractGeneralService() {
    }

    private String contractNo;
    private Contracts contract;

    private ContractGeneral cd;

    private String type;

    boolean VSC;
    boolean ANC;
    boolean PPM;
    boolean GAP;

    Integer activeIndex;
    
    Boolean cancelContract;
    
    public Boolean getCancelContract() {
        if (contract != null) {
            return claimMisc.isContractCancellable(contract.getContractNo());
        } else {
            return false;
        }
    }

    public void setCancelContract(Boolean cancelContract) {
        this.cancelContract = cancelContract;
    }
    
    

    public Integer getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(Integer activeIndex) {
        this.activeIndex = activeIndex;
    }

    public boolean isANC() {
        if (contract != null) {
            return claimMisc.isANC(contract);
        } else {
            return false;
        }
    }

    public void setANC(boolean ANC) {
        this.ANC = ANC;
    }

    public boolean isPPM() {
        if (contract != null) {
            return claimMisc.isPPM(contract);
        } else {
            return false;
        }
    }

    public void setPPM(boolean PPM) {
        this.PPM = PPM;
    }

    public boolean isGAP() {
        if (contract != null) {
            return claimMisc.isGAP(contract);
        } else {
            return false;
        }
    }

    public void setGAP(boolean GAP) {
        this.GAP = GAP;
    }

    public boolean isVSC() {
        if (contract != null) {
            return claimMisc.isVSC(contract);
        } else {
            return false;
        }
    }

    public void setVSC(boolean VSC) {
        this.VSC = VSC;
    }

    public String getType() {
        if (contract != null && contract.getProductIdFk() != null) {
            return contract.getProductIdFk().getProductTypeIdFk().getProductTypeName();
        }
        return "";
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Contracts getContract() {
        return contract;
    }

    public void setContract(Contracts contract) {
        this.contract = contract;
    }

    public ContractGeneral getCd() {
        return cd;
    }

    public void setCd(ContractGeneral cd) {
        this.cd = cd;
    }

    public void attrListener(ActionEvent event) {
        contractNo = (String) event.getComponent().getAttributes().get("contractNo");
        LOGGER.info("in attrListener, contractNo=" + contractNo);
    }

    public String viewContractTab() {

        LOGGER.info("viewContratTab, contractNo=" + contractNo);

        if (contractNo != null) {

            activeIndex = 0;

            cd = createContractGeneralView(contractNo);
            /*
            Map<String, Object> options = new HashMap<>();
            options.put("resizable", true);
            options.put("draggable", true);
            options.put("contentHeight", "100%");
            options.put("contentWidth", "100%");
            options.put("height", "1000");
            options.put("width", "2000");
            options.put("modal", true);
             */
            //RequestContext.getCurrentInstance().openDialog("ContractGeneral", options, null);
            LOGGER.info("calling findClaims");
            claimFirstLineView.findClaims(contractNo);

            //LOGGER.info("calling createContractNotesByContractNo");
            //contractNoteService.createContractNotesByContractNo(contractNo);
        }
        return "ContractGeneral";
    }

    public void viewDisbursementTab() {
        LOGGER.info("viewDisbursementTab, contractNo=" + contractNo);
        if (contractNo != null) {
            contractRatingService.findRates(cd);
        }
    }

    public ContractGeneral createContractGeneralView(String contractNo) {
        LOGGER.info("in createContractDetailView, contractNo=" + contractNo);
        contract = claimMisc.getContractsByNo(contractNo);

        BigDecimal paidClaims = null;
        paidClaims = claimMisc.getPaidClaimsByContractNo(contractNo);

        Integer termId = contract.getTermIdFk().getTermId();
        LOGGER.info("termId=" + termId);

        String termInfo = claimMisc.getTermDetailByFk(termId).get(0).getDescription();
        String[] termstr = termInfo.split("/");
        String termMonths = null;
        String termMiles = null;
        if (termInfo.indexOf("/") > 0) {
            termMonths = termstr[0];
            termMiles = termstr[1];
        } else {
            termMonths = termInfo;
        }

        BigDecimal deductible = new BigDecimal(0);
        if (contract.getDeductibleIdFk() != null) {
            deductible = claimMisc.getDeductibleById(contract.getDeductibleIdFk().getDeductibleId()).getDeductibleAmount();
        }

        String lienHolder = "";
        if (contract.getLienHolderIdFk() != null) {
            lienHolder = contract.getLienHolderIdFk().getAccountKeeper().getAccountKeeperName();
        }

        VinDesc vinDesc = claimMisc.getVinDescByContract(contractNo);
        BigDecimal coverageCost = claimMisc.getContractCoverageCost(contractNo).setScale(2, RoundingMode.HALF_UP);

        return new ContractGeneral(
                contractNo,
                contract.getContractStatusIdFk().getContractStatusName(),
                claimMisc.getJavaDate(contract.getEffectiveDate()),
                claimMisc.getJavaDate(contract.getClaimStartDate()),
                "",
                claimMisc.getJavaDate(contract.getExpirationDate()),
                contract.getContractPurchasePrice().setScale(2, RoundingMode.HALF_UP),
                coverageCost,

                "Dealer Owned", // later
                contract.getProductIdFk().getProgramIdFk().getName(),
                "insurer", // later
                contract.getProductIdFk().getProductName(),
                contract.getPlanIdFk().getPlanName(),
                contract.getClassCode(),
                claimMisc.getAccountKeeperByContractDealer(contractNo).getAccountKeeperName(),
                contract.getCustomerIdFk().getAccountKeeper().getAccountKeeperName(),
                lienHolder,
                contract.getSoldByUserIdFk().getAccountKeeper().getAccountKeeperName(),
                contract.getExternalUserName(),
                paidClaims,
                contract.getIsEsigned() != null ? "Yes" : "No",
                contract.getOdometer(),
                termMonths,
                termMiles,
                deductible,
                claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0).getWaitDays(),
                claimMisc.getTermDetailByFk(contract.getTermIdFk().getTermId()).get(0).getWaitMiles(),
                contract.getClaimStartUsage(),
                contract.getExpirationUsage(),
                contract.getVinFull(),
                vinDesc.getVehicleYear(),
                vinDesc.getMake(),
                vinDesc.getModel(),
                vinDesc.getSeries(),
                claimMisc.getDriveTypeById(Integer.parseInt(vinDesc.getDriveType())).getName(),
                vinDesc.getCylinders(),
                vinDesc.getLiterDisplacemant(),
                vinDesc.getCubicInchDisplacement(),
                vinDesc.getBasicManufacturerWarrantyTerm() + "/" + vinDesc.getBasicManufacturerWarrantyMileage(),
                vinDesc.getPowerTrainWarrantyMonths() + "/" + vinDesc.getPowerTrainWarrantyMiles(),
                vinDesc.getRustWarrantyMonths() + "/" + vinDesc.getRustWarrantyMiles()
        );

    }

    public void viewFieldTab() {

    }

    public void tabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);

        if (contractNo != null) {
            switch (tabId) {
                case "contractClaimTab":
                    viewContractTab();
                    activeIndex = 0;
                    break;
                case "ccNotesTab":
                    contractNoteService.createContractNotesByContractNo(contractNo);
                    activeIndex = 1;
                    break;
                case "cddTab":
                    viewDisbursementTab();
                    activeIndex = 2;
                    break;
                case "cfTab":
                    contractFieldService.createContractFields(contractNo);
                    activeIndex = 3;
                    break;
                default:
                    viewContractTab();
                    contractNoteService.createContractNotesByContractNo(contractNo);
                    break;
            }
        }
    }

    public void cancelContractQuote() {
        LOGGER.info("in cancelContractQuote, contractNo=" + contractNo);
        contractCancelService.setContractNo(contractNo);
        contractCancelService.getContractCancelQuote().reset();

        // check the 
        //claimMisc.isContractCancellable(contractNo);
        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "600");
        options.put("width", "800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("ContractCancelQuote", options, null);
        

        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "contractNo: ", contractNo);
       //FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
