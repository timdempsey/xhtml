/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceDealer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceDealer.findAll", query = "SELECT r FROM ReinsuranceDealer r")
    , @NamedQuery(name = "ReinsuranceDealer.findByReinsuranceDealerId", query = "SELECT r FROM ReinsuranceDealer r WHERE r.reinsuranceDealerId = :reinsuranceDealerId")
    , @NamedQuery(name = "ReinsuranceDealer.findByUpdateUserName", query = "SELECT r FROM ReinsuranceDealer r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceDealer.findByUpdateLast", query = "SELECT r FROM ReinsuranceDealer r WHERE r.updateLast = :updateLast")})
public class ReinsuranceDealer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceDealerId")
    private Integer reinsuranceDealerId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "dealerIdFk", referencedColumnName = "dealerId")
    @ManyToOne
    private Dealer dealerIdFk;
    @JoinColumn(name = "reinsuranceRuleIdFk", referencedColumnName = "reinsuranceRuleId")
    @ManyToOne
    private ReinsuranceRule reinsuranceRuleIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reinsuranceDealerIdFk")
    private Collection<ReinsuranceDealerDetail> reinsuranceDealerDetailCollection;

    public ReinsuranceDealer() {
    }

    public ReinsuranceDealer(Integer reinsuranceDealerId) {
        this.reinsuranceDealerId = reinsuranceDealerId;
    }

    public Integer getReinsuranceDealerId() {
        return reinsuranceDealerId;
    }

    public void setReinsuranceDealerId(Integer reinsuranceDealerId) {
        this.reinsuranceDealerId = reinsuranceDealerId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Dealer getDealerIdFk() {
        return dealerIdFk;
    }

    public void setDealerIdFk(Dealer dealerIdFk) {
        this.dealerIdFk = dealerIdFk;
    }

    public ReinsuranceRule getReinsuranceRuleIdFk() {
        return reinsuranceRuleIdFk;
    }

    public void setReinsuranceRuleIdFk(ReinsuranceRule reinsuranceRuleIdFk) {
        this.reinsuranceRuleIdFk = reinsuranceRuleIdFk;
    }

    @XmlTransient
    public Collection<ReinsuranceDealerDetail> getReinsuranceDealerDetailCollection() {
        return reinsuranceDealerDetailCollection;
    }

    public void setReinsuranceDealerDetailCollection(Collection<ReinsuranceDealerDetail> reinsuranceDealerDetailCollection) {
        this.reinsuranceDealerDetailCollection = reinsuranceDealerDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceDealerId != null ? reinsuranceDealerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceDealer)) {
            return false;
        }
        ReinsuranceDealer other = (ReinsuranceDealer) object;
        if ((this.reinsuranceDealerId == null && other.reinsuranceDealerId != null) || (this.reinsuranceDealerId != null && !this.reinsuranceDealerId.equals(other.reinsuranceDealerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceDealer[ reinsuranceDealerId=" + reinsuranceDealerId + " ]";
    }
    
}
