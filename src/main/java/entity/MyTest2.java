/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "MyTest2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MyTest2.findAll", query = "SELECT m FROM MyTest2 m")
    , @NamedQuery(name = "MyTest2.findByMyKey", query = "SELECT m FROM MyTest2 m WHERE m.myKey = :myKey")
    , @NamedQuery(name = "MyTest2.findByMyValue", query = "SELECT m FROM MyTest2 m WHERE m.myValue = :myValue")})
public class MyTest2 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "myKey")
    private Integer myKey;
    @Column(name = "myValue")
    private Integer myValue;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "TS")
    private byte[] ts;

    public MyTest2() {
    }

    public MyTest2(Integer myKey) {
        this.myKey = myKey;
    }

    public MyTest2(Integer myKey, byte[] ts) {
        this.myKey = myKey;
        this.ts = ts;
    }

    public Integer getMyKey() {
        return myKey;
    }

    public void setMyKey(Integer myKey) {
        this.myKey = myKey;
    }

    public Integer getMyValue() {
        return myValue;
    }

    public void setMyValue(Integer myValue) {
        this.myValue = myValue;
    }

    public byte[] getTs() {
        return ts;
    }

    public void setTs(byte[] ts) {
        this.ts = ts;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (myKey != null ? myKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MyTest2)) {
            return false;
        }
        MyTest2 other = (MyTest2) object;
        if ((this.myKey == null && other.myKey != null) || (this.myKey != null && !this.myKey.equals(other.myKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MyTest2[ myKey=" + myKey + " ]";
    }
    
}
