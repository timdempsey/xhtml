/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;

/**
 *
 * @author Jiepi
 */
public class ProductSearch {

    public ProductSearch(Integer productId, String productName, String productCode, String productType, String productSubType, String disbursementType, String adminCompany, Date effectiveDate, String expireDate, String insurer, String programName, boolean financeBasedTerm, boolean mileageBasedTerm, boolean isRenewable, String claimSetting, String salesGroup, String contractForm, String description) {
        this.productId = productId;
        this.productName = productName;
        this.productCode = productCode;
        this.productType = productType;
        this.productSubType = productSubType;
        this.disbursementType = disbursementType;
        this.adminCompany = adminCompany;
        this.effectiveDate = effectiveDate;
        this.expireDate = expireDate;
        this.insurer = insurer;
        this.programName = programName;
        this.financeBasedTerm = financeBasedTerm;
        this.mileageBasedTerm = mileageBasedTerm;
        this.isRenewable = isRenewable;
        this.claimSetting = claimSetting;
        this.salesGroup = salesGroup;
        this.contractForm = contractForm;
        this.description = description;
    }

    

    

    public ProductSearch() {
    }

    @Id
    @Column(name = "productId", table = "Product")
    private Integer productId;
    @Column(name = "productName", table = "Product")
    private String productName;
    @Column(name = "productCode", table = "Product")
    private String productCode;
    @Column(name = "productTypeIdFk", table = "Product")
    private String productType;
    @Column(name = "productSubTypeFk", table = "Product")
    private String productSubType;
    @Column(name = "disbursementType", table = "DisbursementCenter")
    private String disbursementType;
    private String adminCompany;
    @Column(name = "effectiveDate", table = "ProductDetail")
    private Date effectiveDate;
    private String expireDate;
    @Column(name = "insurerIdFk", table = "Product")
    private String insurer;
    @Column(name = "name", table = "Program")
    private String programName;
    private boolean financeBasedTerm;
    private boolean mileageBasedTerm;
    private boolean isRenewable;
    private String claimSetting;
    private String salesGroup;
    private String contractForm;
    @Column(name = "description", table = "ProductDetail")
    private String description;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductSubType() {
        return productSubType;
    }

    public void setProductSubType(String productSubType) {
        this.productSubType = productSubType;
    }

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public boolean isFinanceBasedTerm() {
        return financeBasedTerm;
    }

    public void setFinanceBasedTerm(boolean financeBasedTerm) {
        this.financeBasedTerm = financeBasedTerm;
    }

    public boolean isMileageBasedTerm() {
        return mileageBasedTerm;
    }

    public void setMileageBasedTerm(boolean mileageBasedTerm) {
        this.mileageBasedTerm = mileageBasedTerm;
    }

    public boolean isIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    public String getClaimSetting() {
        return claimSetting;
    }

    public void setClaimSetting(String claimSetting) {
        this.claimSetting = claimSetting;
    }

    public String getSalesGroup() {
        return salesGroup;
    }

    public void setSalesGroup(String salesGroup) {
        this.salesGroup = salesGroup;
    }

    public String getContractForm() {
        return contractForm;
    }

    public void setContractForm(String contractForm) {
        this.contractForm = contractForm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    

    public void reset() {
        this.productId = null;
        this.productName = "";
        this.productCode = "";
        this.productType = "";
        this.productSubType = "";
        this.disbursementType = "";
        this.adminCompany = "";
        this.effectiveDate = null;
        this.expireDate = null;
        this.insurer = "";
        this.programName = "";
        financeBasedTerm = false;
        mileageBasedTerm = false;
        isRenewable = false;
        claimSetting = "";
        salesGroup = "";
        contractForm = "";
        description = "";

    }

}
