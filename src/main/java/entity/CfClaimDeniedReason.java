/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfClaimDeniedReason")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfClaimDeniedReason.findAll", query = "SELECT c FROM CfClaimDeniedReason c")
    , @NamedQuery(name = "CfClaimDeniedReason.findByClaimDeniedReasonId", query = "SELECT c FROM CfClaimDeniedReason c WHERE c.claimDeniedReasonId = :claimDeniedReasonId")
    , @NamedQuery(name = "CfClaimDeniedReason.findByReason", query = "SELECT c FROM CfClaimDeniedReason c WHERE c.reason = :reason")})
public class CfClaimDeniedReason implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimDeniedReasonId")
    private Integer claimDeniedReasonId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "reason")
    private String reason;

    public CfClaimDeniedReason() {
    }

    public CfClaimDeniedReason(Integer claimDeniedReasonId) {
        this.claimDeniedReasonId = claimDeniedReasonId;
    }

    public CfClaimDeniedReason(Integer claimDeniedReasonId, String reason) {
        this.claimDeniedReasonId = claimDeniedReasonId;
        this.reason = reason;
    }

    public Integer getClaimDeniedReasonId() {
        return claimDeniedReasonId;
    }

    public void setClaimDeniedReasonId(Integer claimDeniedReasonId) {
        this.claimDeniedReasonId = claimDeniedReasonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimDeniedReasonId != null ? claimDeniedReasonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfClaimDeniedReason)) {
            return false;
        }
        CfClaimDeniedReason other = (CfClaimDeniedReason) object;
        if ((this.claimDeniedReasonId == null && other.claimDeniedReasonId != null) || (this.claimDeniedReasonId != null && !this.claimDeniedReasonId.equals(other.claimDeniedReasonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfClaimDeniedReason[ claimDeniedReasonId=" + claimDeniedReasonId + " ]";
    }
    
}
