/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractEarning")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractEarning.findAll", query = "SELECT c FROM ContractEarning c")
    , @NamedQuery(name = "ContractEarning.findByContractEarningId", query = "SELECT c FROM ContractEarning c WHERE c.contractEarningId = :contractEarningId")
    , @NamedQuery(name = "ContractEarning.findByEarningDate", query = "SELECT c FROM ContractEarning c WHERE c.earningDate = :earningDate")
    , @NamedQuery(name = "ContractEarning.findByAmountEarned", query = "SELECT c FROM ContractEarning c WHERE c.amountEarned = :amountEarned")
    , @NamedQuery(name = "ContractEarning.findByEarningMethodInd", query = "SELECT c FROM ContractEarning c WHERE c.earningMethodInd = :earningMethodInd")
    , @NamedQuery(name = "ContractEarning.findByEnteredDate", query = "SELECT c FROM ContractEarning c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "ContractEarning.findByPostingHistoryIdFk", query = "SELECT c FROM ContractEarning c WHERE c.postingHistoryIdFk = :postingHistoryIdFk")
    , @NamedQuery(name = "ContractEarning.findByUpdateUserName", query = "SELECT c FROM ContractEarning c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractEarning.findByUpdateLast", query = "SELECT c FROM ContractEarning c WHERE c.updateLast = :updateLast")})
public class ContractEarning implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractEarningId")
    private Integer contractEarningId;
    @Column(name = "earningDate")
    @Temporal(TemporalType.DATE)
    private Date earningDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AmountEarned")
    private BigDecimal amountEarned;
    @Column(name = "EarningMethodInd")
    private Integer earningMethodInd;
    @Column(name = "EnteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "postingHistoryIdFk")
    private Integer postingHistoryIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "contractDisbursementIdFk", referencedColumnName = "contractDisbursementId")
    @ManyToOne
    private ContractDisbursement contractDisbursementIdFk;

    public ContractEarning() {
    }

    public ContractEarning(Integer contractEarningId) {
        this.contractEarningId = contractEarningId;
    }

    public Integer getContractEarningId() {
        return contractEarningId;
    }

    public void setContractEarningId(Integer contractEarningId) {
        this.contractEarningId = contractEarningId;
    }

    public Date getEarningDate() {
        return earningDate;
    }

    public void setEarningDate(Date earningDate) {
        this.earningDate = earningDate;
    }

    public BigDecimal getAmountEarned() {
        return amountEarned;
    }

    public void setAmountEarned(BigDecimal amountEarned) {
        this.amountEarned = amountEarned;
    }

    public Integer getEarningMethodInd() {
        return earningMethodInd;
    }

    public void setEarningMethodInd(Integer earningMethodInd) {
        this.earningMethodInd = earningMethodInd;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Integer getPostingHistoryIdFk() {
        return postingHistoryIdFk;
    }

    public void setPostingHistoryIdFk(Integer postingHistoryIdFk) {
        this.postingHistoryIdFk = postingHistoryIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ContractDisbursement getContractDisbursementIdFk() {
        return contractDisbursementIdFk;
    }

    public void setContractDisbursementIdFk(ContractDisbursement contractDisbursementIdFk) {
        this.contractDisbursementIdFk = contractDisbursementIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractEarningId != null ? contractEarningId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractEarning)) {
            return false;
        }
        ContractEarning other = (ContractEarning) object;
        if ((this.contractEarningId == null && other.contractEarningId != null) || (this.contractEarningId != null && !this.contractEarningId.equals(other.contractEarningId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractEarning[ contractEarningId=" + contractEarningId + " ]";
    }
    
}
