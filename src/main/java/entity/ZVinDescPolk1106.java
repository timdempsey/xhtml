/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "z_VinDescPolk_1106")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ZVinDescPolk1106.findAll", query = "SELECT z FROM ZVinDescPolk1106 z")
    , @NamedQuery(name = "ZVinDescPolk1106.findByVinDescId", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.vinDescId = :vinDescId")
    , @NamedQuery(name = "ZVinDescPolk1106.findByVinPartial", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.vinPartial = :vinPartial")
    , @NamedQuery(name = "ZVinDescPolk1106.findByVehicleYear", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.vehicleYear = :vehicleYear")
    , @NamedQuery(name = "ZVinDescPolk1106.findByVehicleType", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.vehicleType = :vehicleType")
    , @NamedQuery(name = "ZVinDescPolk1106.findByMake", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.make = :make")
    , @NamedQuery(name = "ZVinDescPolk1106.findByModel", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.model = :model")
    , @NamedQuery(name = "ZVinDescPolk1106.findBySeries", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.series = :series")
    , @NamedQuery(name = "ZVinDescPolk1106.findByDriveType", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.driveType = :driveType")
    , @NamedQuery(name = "ZVinDescPolk1106.findByFuelType", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.fuelType = :fuelType")
    , @NamedQuery(name = "ZVinDescPolk1106.findByFuelDelivery", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.fuelDelivery = :fuelDelivery")
    , @NamedQuery(name = "ZVinDescPolk1106.findByFuelTurbineDelivery", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.fuelTurbineDelivery = :fuelTurbineDelivery")
    , @NamedQuery(name = "ZVinDescPolk1106.findByCylinders", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.cylinders = :cylinders")
    , @NamedQuery(name = "ZVinDescPolk1106.findByTonRating", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.tonRating = :tonRating")
    , @NamedQuery(name = "ZVinDescPolk1106.findByCubicCentimeterDisplacement", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.cubicCentimeterDisplacement = :cubicCentimeterDisplacement")
    , @NamedQuery(name = "ZVinDescPolk1106.findByCubicInchDisplacement", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.cubicInchDisplacement = :cubicInchDisplacement")
    , @NamedQuery(name = "ZVinDescPolk1106.findByVehicleWeightRating", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.vehicleWeightRating = :vehicleWeightRating")
    , @NamedQuery(name = "ZVinDescPolk1106.findByGvwr", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.gvwr = :gvwr")
    , @NamedQuery(name = "ZVinDescPolk1106.findByCountryOfOrigin", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.countryOfOrigin = :countryOfOrigin")
    , @NamedQuery(name = "ZVinDescPolk1106.findByBasicManufacturerWarrantyMileage", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.basicManufacturerWarrantyMileage = :basicManufacturerWarrantyMileage")
    , @NamedQuery(name = "ZVinDescPolk1106.findByBasicManufacturerWarrantyTerm", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.basicManufacturerWarrantyTerm = :basicManufacturerWarrantyTerm")
    , @NamedQuery(name = "ZVinDescPolk1106.findByMsrp", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.msrp = :msrp")
    , @NamedQuery(name = "ZVinDescPolk1106.findByLiterDisplacemant", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.literDisplacemant = :literDisplacemant")
    , @NamedQuery(name = "ZVinDescPolk1106.findByPowerTrainWarrantyMonths", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.powerTrainWarrantyMonths = :powerTrainWarrantyMonths")
    , @NamedQuery(name = "ZVinDescPolk1106.findByPowerTrainWarrantyMiles", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.powerTrainWarrantyMiles = :powerTrainWarrantyMiles")
    , @NamedQuery(name = "ZVinDescPolk1106.findByRustWarrantyMonths", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.rustWarrantyMonths = :rustWarrantyMonths")
    , @NamedQuery(name = "ZVinDescPolk1106.findByRustWarrantyMiles", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.rustWarrantyMiles = :rustWarrantyMiles")
    , @NamedQuery(name = "ZVinDescPolk1106.findByValid", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.valid = :valid")
    , @NamedQuery(name = "ZVinDescPolk1106.findByOverride", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.override = :override")
    , @NamedQuery(name = "ZVinDescPolk1106.findByMotorCycleUsage", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.motorCycleUsage = :motorCycleUsage")
    , @NamedQuery(name = "ZVinDescPolk1106.findByMakeIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.makeIdFk = :makeIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findByModelIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.modelIdFk = :modelIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findBySeriesIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.seriesIdFk = :seriesIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findByFuelTypeIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.fuelTypeIdFk = :fuelTypeIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findByFuelDeliveryIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.fuelDeliveryIdFk = :fuelDeliveryIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findByCountryOfOriginIdFk", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.countryOfOriginIdFk = :countryOfOriginIdFk")
    , @NamedQuery(name = "ZVinDescPolk1106.findByUpdateUserName", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.updateUserName = :updateUserName")
    , @NamedQuery(name = "ZVinDescPolk1106.findByUpdateLast", query = "SELECT z FROM ZVinDescPolk1106 z WHERE z.updateLast = :updateLast")})
public class ZVinDescPolk1106 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinDescId")
    private Integer vinDescId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "vinPartial")
    private String vinPartial;
    @Column(name = "vehicleYear")
    private Integer vehicleYear;
    @Size(max = 20)
    @Column(name = "vehicleType")
    private String vehicleType;
    @Size(max = 50)
    @Column(name = "make")
    private String make;
    @Size(max = 50)
    @Column(name = "model")
    private String model;
    @Size(max = 250)
    @Column(name = "series")
    private String series;
    @Size(max = 25)
    @Column(name = "driveType")
    private String driveType;
    @Size(max = 50)
    @Column(name = "fuelType")
    private String fuelType;
    @Size(max = 50)
    @Column(name = "fuelDelivery")
    private String fuelDelivery;
    @Size(max = 50)
    @Column(name = "fuelTurbineDelivery")
    private String fuelTurbineDelivery;
    @Size(max = 2)
    @Column(name = "cylinders")
    private String cylinders;
    @Size(max = 50)
    @Column(name = "tonRating")
    private String tonRating;
    @Size(max = 20)
    @Column(name = "cubicCentimeterDisplacement")
    private String cubicCentimeterDisplacement;
    @Size(max = 20)
    @Column(name = "cubicInchDisplacement")
    private String cubicInchDisplacement;
    @Size(max = 20)
    @Column(name = "vehicleWeightRating")
    private String vehicleWeightRating;
    @Column(name = "gvwr")
    private Integer gvwr;
    @Size(max = 50)
    @Column(name = "countryOfOrigin")
    private String countryOfOrigin;
    @Size(max = 50)
    @Column(name = "basicManufacturerWarrantyMileage")
    private String basicManufacturerWarrantyMileage;
    @Column(name = "basicManufacturerWarrantyTerm")
    private Integer basicManufacturerWarrantyTerm;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "msrp")
    private BigDecimal msrp;
    @Size(max = 50)
    @Column(name = "literDisplacemant")
    private String literDisplacemant;
    @Column(name = "powerTrainWarrantyMonths")
    private Integer powerTrainWarrantyMonths;
    @Size(max = 50)
    @Column(name = "powerTrainWarrantyMiles")
    private String powerTrainWarrantyMiles;
    @Column(name = "rustWarrantyMonths")
    private Integer rustWarrantyMonths;
    @Size(max = 50)
    @Column(name = "rustWarrantyMiles")
    private String rustWarrantyMiles;
    @Column(name = "valid")
    private Boolean valid;
    @Column(name = "override")
    private Boolean override;
    @Column(name = "motorCycleUsage")
    private Integer motorCycleUsage;
    @Column(name = "makeIdFk")
    private Integer makeIdFk;
    @Column(name = "modelIdFk")
    private Integer modelIdFk;
    @Column(name = "seriesIdFk")
    private Integer seriesIdFk;
    @Column(name = "fuelTypeIdFk")
    private Integer fuelTypeIdFk;
    @Column(name = "fuelDeliveryIdFk")
    private Integer fuelDeliveryIdFk;
    @Column(name = "countryOfOriginIdFk")
    private Integer countryOfOriginIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;

    public ZVinDescPolk1106() {
    }

    public ZVinDescPolk1106(Integer vinDescId) {
        this.vinDescId = vinDescId;
    }

    public ZVinDescPolk1106(Integer vinDescId, String vinPartial) {
        this.vinDescId = vinDescId;
        this.vinPartial = vinPartial;
    }

    public Integer getVinDescId() {
        return vinDescId;
    }

    public void setVinDescId(Integer vinDescId) {
        this.vinDescId = vinDescId;
    }

    public String getVinPartial() {
        return vinPartial;
    }

    public void setVinPartial(String vinPartial) {
        this.vinPartial = vinPartial;
    }

    public Integer getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelDelivery() {
        return fuelDelivery;
    }

    public void setFuelDelivery(String fuelDelivery) {
        this.fuelDelivery = fuelDelivery;
    }

    public String getFuelTurbineDelivery() {
        return fuelTurbineDelivery;
    }

    public void setFuelTurbineDelivery(String fuelTurbineDelivery) {
        this.fuelTurbineDelivery = fuelTurbineDelivery;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getTonRating() {
        return tonRating;
    }

    public void setTonRating(String tonRating) {
        this.tonRating = tonRating;
    }

    public String getCubicCentimeterDisplacement() {
        return cubicCentimeterDisplacement;
    }

    public void setCubicCentimeterDisplacement(String cubicCentimeterDisplacement) {
        this.cubicCentimeterDisplacement = cubicCentimeterDisplacement;
    }

    public String getCubicInchDisplacement() {
        return cubicInchDisplacement;
    }

    public void setCubicInchDisplacement(String cubicInchDisplacement) {
        this.cubicInchDisplacement = cubicInchDisplacement;
    }

    public String getVehicleWeightRating() {
        return vehicleWeightRating;
    }

    public void setVehicleWeightRating(String vehicleWeightRating) {
        this.vehicleWeightRating = vehicleWeightRating;
    }

    public Integer getGvwr() {
        return gvwr;
    }

    public void setGvwr(Integer gvwr) {
        this.gvwr = gvwr;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getBasicManufacturerWarrantyMileage() {
        return basicManufacturerWarrantyMileage;
    }

    public void setBasicManufacturerWarrantyMileage(String basicManufacturerWarrantyMileage) {
        this.basicManufacturerWarrantyMileage = basicManufacturerWarrantyMileage;
    }

    public Integer getBasicManufacturerWarrantyTerm() {
        return basicManufacturerWarrantyTerm;
    }

    public void setBasicManufacturerWarrantyTerm(Integer basicManufacturerWarrantyTerm) {
        this.basicManufacturerWarrantyTerm = basicManufacturerWarrantyTerm;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public String getLiterDisplacemant() {
        return literDisplacemant;
    }

    public void setLiterDisplacemant(String literDisplacemant) {
        this.literDisplacemant = literDisplacemant;
    }

    public Integer getPowerTrainWarrantyMonths() {
        return powerTrainWarrantyMonths;
    }

    public void setPowerTrainWarrantyMonths(Integer powerTrainWarrantyMonths) {
        this.powerTrainWarrantyMonths = powerTrainWarrantyMonths;
    }

    public String getPowerTrainWarrantyMiles() {
        return powerTrainWarrantyMiles;
    }

    public void setPowerTrainWarrantyMiles(String powerTrainWarrantyMiles) {
        this.powerTrainWarrantyMiles = powerTrainWarrantyMiles;
    }

    public Integer getRustWarrantyMonths() {
        return rustWarrantyMonths;
    }

    public void setRustWarrantyMonths(Integer rustWarrantyMonths) {
        this.rustWarrantyMonths = rustWarrantyMonths;
    }

    public String getRustWarrantyMiles() {
        return rustWarrantyMiles;
    }

    public void setRustWarrantyMiles(String rustWarrantyMiles) {
        this.rustWarrantyMiles = rustWarrantyMiles;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Boolean getOverride() {
        return override;
    }

    public void setOverride(Boolean override) {
        this.override = override;
    }

    public Integer getMotorCycleUsage() {
        return motorCycleUsage;
    }

    public void setMotorCycleUsage(Integer motorCycleUsage) {
        this.motorCycleUsage = motorCycleUsage;
    }

    public Integer getMakeIdFk() {
        return makeIdFk;
    }

    public void setMakeIdFk(Integer makeIdFk) {
        this.makeIdFk = makeIdFk;
    }

    public Integer getModelIdFk() {
        return modelIdFk;
    }

    public void setModelIdFk(Integer modelIdFk) {
        this.modelIdFk = modelIdFk;
    }

    public Integer getSeriesIdFk() {
        return seriesIdFk;
    }

    public void setSeriesIdFk(Integer seriesIdFk) {
        this.seriesIdFk = seriesIdFk;
    }

    public Integer getFuelTypeIdFk() {
        return fuelTypeIdFk;
    }

    public void setFuelTypeIdFk(Integer fuelTypeIdFk) {
        this.fuelTypeIdFk = fuelTypeIdFk;
    }

    public Integer getFuelDeliveryIdFk() {
        return fuelDeliveryIdFk;
    }

    public void setFuelDeliveryIdFk(Integer fuelDeliveryIdFk) {
        this.fuelDeliveryIdFk = fuelDeliveryIdFk;
    }

    public Integer getCountryOfOriginIdFk() {
        return countryOfOriginIdFk;
    }

    public void setCountryOfOriginIdFk(Integer countryOfOriginIdFk) {
        this.countryOfOriginIdFk = countryOfOriginIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vinDescId != null ? vinDescId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZVinDescPolk1106)) {
            return false;
        }
        ZVinDescPolk1106 other = (ZVinDescPolk1106) object;
        if ((this.vinDescId == null && other.vinDescId != null) || (this.vinDescId != null && !this.vinDescId.equals(other.vinDescId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ZVinDescPolk1106[ vinDescId=" + vinDescId + " ]";
    }
    
}
