/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AdjustmentDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdjustmentDetail.findAll", query = "SELECT a FROM AdjustmentDetail a")
    , @NamedQuery(name = "AdjustmentDetail.findByAdjustmentDetailId", query = "SELECT a FROM AdjustmentDetail a WHERE a.adjustmentDetailId = :adjustmentDetailId")
    , @NamedQuery(name = "AdjustmentDetail.findBySuggestedRetailPrice", query = "SELECT a FROM AdjustmentDetail a WHERE a.suggestedRetailPrice = :suggestedRetailPrice")
    , @NamedQuery(name = "AdjustmentDetail.findByEffectiveDate", query = "SELECT a FROM AdjustmentDetail a WHERE a.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "AdjustmentDetail.findByEnteredDate", query = "SELECT a FROM AdjustmentDetail a WHERE a.enteredDate = :enteredDate")
    , @NamedQuery(name = "AdjustmentDetail.findByDeletedInd", query = "SELECT a FROM AdjustmentDetail a WHERE a.deletedInd = :deletedInd")
    , @NamedQuery(name = "AdjustmentDetail.findByQuickRateVisibleInd", query = "SELECT a FROM AdjustmentDetail a WHERE a.quickRateVisibleInd = :quickRateVisibleInd")
    , @NamedQuery(name = "AdjustmentDetail.findByUpdateUserName", query = "SELECT a FROM AdjustmentDetail a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "AdjustmentDetail.findByUpdateLast", query = "SELECT a FROM AdjustmentDetail a WHERE a.updateLast = :updateLast")})
public class AdjustmentDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "adjustmentDetailId")
    private Integer adjustmentDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "suggestedRetailPrice")
    private BigDecimal suggestedRetailPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quickRateVisibleInd")
    private boolean quickRateVisibleInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "adjustmentIdFk", referencedColumnName = "adjustmentId")
    @ManyToOne(optional = false)
    private Adjustment adjustmentIdFk;
    @JoinColumn(name = "coverageGroupIdFk", referencedColumnName = "coverageGroupId")
    @ManyToOne
    private CfCoverageGroup coverageGroupIdFk;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public AdjustmentDetail() {
    }

    public AdjustmentDetail(Integer adjustmentDetailId) {
        this.adjustmentDetailId = adjustmentDetailId;
    }

    public AdjustmentDetail(Integer adjustmentDetailId, Date effectiveDate, Date enteredDate, boolean quickRateVisibleInd) {
        this.adjustmentDetailId = adjustmentDetailId;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.quickRateVisibleInd = quickRateVisibleInd;
    }

    public Integer getAdjustmentDetailId() {
        return adjustmentDetailId;
    }

    public void setAdjustmentDetailId(Integer adjustmentDetailId) {
        this.adjustmentDetailId = adjustmentDetailId;
    }

    public BigDecimal getSuggestedRetailPrice() {
        return suggestedRetailPrice;
    }

    public void setSuggestedRetailPrice(BigDecimal suggestedRetailPrice) {
        this.suggestedRetailPrice = suggestedRetailPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public boolean getQuickRateVisibleInd() {
        return quickRateVisibleInd;
    }

    public void setQuickRateVisibleInd(boolean quickRateVisibleInd) {
        this.quickRateVisibleInd = quickRateVisibleInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Adjustment getAdjustmentIdFk() {
        return adjustmentIdFk;
    }

    public void setAdjustmentIdFk(Adjustment adjustmentIdFk) {
        this.adjustmentIdFk = adjustmentIdFk;
    }

    public CfCoverageGroup getCoverageGroupIdFk() {
        return coverageGroupIdFk;
    }

    public void setCoverageGroupIdFk(CfCoverageGroup coverageGroupIdFk) {
        this.coverageGroupIdFk = coverageGroupIdFk;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adjustmentDetailId != null ? adjustmentDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdjustmentDetail)) {
            return false;
        }
        AdjustmentDetail other = (AdjustmentDetail) object;
        if ((this.adjustmentDetailId == null && other.adjustmentDetailId != null) || (this.adjustmentDetailId != null && !this.adjustmentDetailId.equals(other.adjustmentDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AdjustmentDetail[ adjustmentDetailId=" + adjustmentDetailId + " ]";
    }
    
}
