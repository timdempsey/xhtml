/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanExpireTime")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanExpireTime.findAll", query = "SELECT d FROM DtPlanExpireTime d")
    , @NamedQuery(name = "DtPlanExpireTime.findByExpireTimeId", query = "SELECT d FROM DtPlanExpireTime d WHERE d.expireTimeId = :expireTimeId")
    , @NamedQuery(name = "DtPlanExpireTime.findByDescription", query = "SELECT d FROM DtPlanExpireTime d WHERE d.description = :description")})
public class DtPlanExpireTime implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "expireTimeId")
    private Integer expireTimeId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public DtPlanExpireTime() {
    }

    public DtPlanExpireTime(Integer expireTimeId) {
        this.expireTimeId = expireTimeId;
    }

    public Integer getExpireTimeId() {
        return expireTimeId;
    }

    public void setExpireTimeId(Integer expireTimeId) {
        this.expireTimeId = expireTimeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (expireTimeId != null ? expireTimeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanExpireTime)) {
            return false;
        }
        DtPlanExpireTime other = (DtPlanExpireTime) object;
        if ((this.expireTimeId == null && other.expireTimeId != null) || (this.expireTimeId != null && !this.expireTimeId.equals(other.expireTimeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanExpireTime[ expireTimeId=" + expireTimeId + " ]";
    }
    
}
