/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class DetailedDisbursements {

    private static final Logger LOGGER = LogManager.getLogger(DetailedDisbursements.class);

    public DetailedDisbursements() {
    }

    public DetailedDisbursements(Integer disbursementDetailId, String disbursementType, String accountHolder, String description, String disbursementGroup, BigDecimal disbursementAmount) {
        this.disbursementDetailId = disbursementDetailId;
        this.disbursementType = disbursementType;
        this.accountHolder = accountHolder;
        this.description = description;
        this.disbursementGroup = disbursementGroup;
        this.disbursementAmount = disbursementAmount;
    }

    public DetailedDisbursements(Integer disbursementDetailId, String disbursementType, String accountHolder, String description, String disbursementGroup, BigDecimal disbursementAmount, String balanceType, String code, String refundType, String agent, String dealer, String dealerGroup, String amountType, BigDecimal amount, String calculatedOn, Boolean isRenewable, String memo, Date effectiveDate, Date expiresDate, Integer criterionGroupIdFk) {
        this.disbursementDetailId = disbursementDetailId;
        this.disbursementType = disbursementType;
        this.accountHolder = accountHolder;
        this.description = description;
        this.disbursementGroup = disbursementGroup;
        this.disbursementAmount = disbursementAmount;
        this.balanceType = balanceType;
        this.code = code;
        this.refundType = refundType;
        this.agent = agent;
        this.dealer = dealer;
        this.dealerGroup = dealerGroup;
        this.amountType = amountType;
        this.amount = amount;
        this.calculatedOn = calculatedOn;
        this.isRenewable = isRenewable;
        this.memo = memo;
        this.effectiveDate = effectiveDate;
        this.expiresDate = expiresDate;
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    
    
    
    
    

    private Integer disbursementDetailId;
    private String disbursementType;
    private String accountHolder;
    private String description;
    private String disbursementGroup;
    private BigDecimal disbursementAmount;
    // added for creating Disbursement 
    private String balanceType;
    private String code;
    private String refundType;
    private String agent;
    private String dealer;
    private String dealerGroup;
    private String amountType;
    private BigDecimal amount;
    private String calculatedOn;
    private Boolean isRenewable;
    private String memo;
    private Date effectiveDate;
    private Date expiresDate;
    private Integer criterionGroupIdFk;

    public Integer getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(Integer criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }
    
    

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    
    
    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(String dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCalculatedOn() {
        return calculatedOn;
    }

    public void setCalculatedOn(String calculatedOn) {
        this.calculatedOn = calculatedOn;
    }

    public Boolean getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(Boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpiresDate() {
        return expiresDate;
    }

    public void setExpiresDate(Date expiresDate) {
        this.expiresDate = expiresDate;
    }

    public Integer getDisbursementDetailId() {
        return disbursementDetailId;
    }

    public void setDisbursementDetailId(Integer disbursementDetailId) {
        this.disbursementDetailId = disbursementDetailId;
    }

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisbursementGroup() {
        return disbursementGroup;
    }

    public void setDisbursementGroup(String disbursementGroup) {
        this.disbursementGroup = disbursementGroup;
    }

    public BigDecimal getDisbursementAmount() {
        return disbursementAmount;
    }

    public void setDisbursementAmount(BigDecimal disbursementAmount) {
        this.disbursementAmount = disbursementAmount;
    }

    public void reset() {
        disbursementDetailId = null;
        disbursementType = "";
        accountHolder = "";
        description = "";
        disbursementGroup = "";
        disbursementAmount = null;
        //
        balanceType = "";
        code = "";
        refundType = "";
        agent = "";
        dealer = "";
        dealerGroup = "";
        amountType = "";
        amount = null;
        calculatedOn = "";
        isRenewable = false;
        memo = "";
        effectiveDate = null;
        expiresDate = null;
        criterionGroupIdFk = null;
    }

}
