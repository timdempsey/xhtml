/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfItemLock")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfItemLock.findAll", query = "SELECT c FROM CfItemLock c")
    , @NamedQuery(name = "CfItemLock.findByItemLockId", query = "SELECT c FROM CfItemLock c WHERE c.itemLockId = :itemLockId")
    , @NamedQuery(name = "CfItemLock.findByLockTypeInd", query = "SELECT c FROM CfItemLock c WHERE c.lockTypeInd = :lockTypeInd")
    , @NamedQuery(name = "CfItemLock.findByItemId", query = "SELECT c FROM CfItemLock c WHERE c.itemId = :itemId")
    , @NamedQuery(name = "CfItemLock.findByLockedDate", query = "SELECT c FROM CfItemLock c WHERE c.lockedDate = :lockedDate")
    , @NamedQuery(name = "CfItemLock.findByReleasedInd", query = "SELECT c FROM CfItemLock c WHERE c.releasedInd = :releasedInd")
    , @NamedQuery(name = "CfItemLock.findByReleaseDate", query = "SELECT c FROM CfItemLock c WHERE c.releaseDate = :releaseDate")
    , @NamedQuery(name = "CfItemLock.findByUpdateUserName", query = "SELECT c FROM CfItemLock c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfItemLock.findByUpdateLast", query = "SELECT c FROM CfItemLock c WHERE c.updateLast = :updateLast")})
public class CfItemLock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "itemLockId")
    private Integer itemLockId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lockTypeInd")
    private int lockTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "itemId")
    private int itemId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lockedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "releasedInd")
    private boolean releasedInd;
    @Column(name = "releaseDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "lockedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember lockedByIdFk;
    @JoinColumn(name = "releasedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember releasedByIdFk;

    public CfItemLock() {
    }

    public CfItemLock(Integer itemLockId) {
        this.itemLockId = itemLockId;
    }

    public CfItemLock(Integer itemLockId, int lockTypeInd, int itemId, Date lockedDate, boolean releasedInd) {
        this.itemLockId = itemLockId;
        this.lockTypeInd = lockTypeInd;
        this.itemId = itemId;
        this.lockedDate = lockedDate;
        this.releasedInd = releasedInd;
    }

    public Integer getItemLockId() {
        return itemLockId;
    }

    public void setItemLockId(Integer itemLockId) {
        this.itemLockId = itemLockId;
    }

    public int getLockTypeInd() {
        return lockTypeInd;
    }

    public void setLockTypeInd(int lockTypeInd) {
        this.lockTypeInd = lockTypeInd;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public Date getLockedDate() {
        return lockedDate;
    }

    public void setLockedDate(Date lockedDate) {
        this.lockedDate = lockedDate;
    }

    public boolean getReleasedInd() {
        return releasedInd;
    }

    public void setReleasedInd(boolean releasedInd) {
        this.releasedInd = releasedInd;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getLockedByIdFk() {
        return lockedByIdFk;
    }

    public void setLockedByIdFk(UserMember lockedByIdFk) {
        this.lockedByIdFk = lockedByIdFk;
    }

    public UserMember getReleasedByIdFk() {
        return releasedByIdFk;
    }

    public void setReleasedByIdFk(UserMember releasedByIdFk) {
        this.releasedByIdFk = releasedByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemLockId != null ? itemLockId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfItemLock)) {
            return false;
        }
        CfItemLock other = (CfItemLock) object;
        if ((this.itemLockId == null && other.itemLockId != null) || (this.itemLockId != null && !this.itemLockId.equals(other.itemLockId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfItemLock[ itemLockId=" + itemLockId + " ]";
    }
    
}
