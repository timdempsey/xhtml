/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCancelMethod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCancelMethod.findAll", query = "SELECT c FROM CfCancelMethod c")
    , @NamedQuery(name = "CfCancelMethod.findByCancelMethodId", query = "SELECT c FROM CfCancelMethod c WHERE c.cancelMethodId = :cancelMethodId")
    , @NamedQuery(name = "CfCancelMethod.findByDescription", query = "SELECT c FROM CfCancelMethod c WHERE c.description = :description")
    , @NamedQuery(name = "CfCancelMethod.findByDeletedInd", query = "SELECT c FROM CfCancelMethod c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfCancelMethod.findByContractEffectiveDateStart", query = "SELECT c FROM CfCancelMethod c WHERE c.contractEffectiveDateStart = :contractEffectiveDateStart")
    , @NamedQuery(name = "CfCancelMethod.findByContractEffectiveDateEnd", query = "SELECT c FROM CfCancelMethod c WHERE c.contractEffectiveDateEnd = :contractEffectiveDateEnd")
    , @NamedQuery(name = "CfCancelMethod.findByCancelDateStart", query = "SELECT c FROM CfCancelMethod c WHERE c.cancelDateStart = :cancelDateStart")
    , @NamedQuery(name = "CfCancelMethod.findByCancelDateEnd", query = "SELECT c FROM CfCancelMethod c WHERE c.cancelDateEnd = :cancelDateEnd")
    , @NamedQuery(name = "CfCancelMethod.findByUpdateUserName", query = "SELECT c FROM CfCancelMethod c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfCancelMethod.findByUpdateLast", query = "SELECT c FROM CfCancelMethod c WHERE c.updateLast = :updateLast")})
public class CfCancelMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelMethodId")
    private Integer cancelMethodId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Column(name = "contractEffectiveDateStart")
    @Temporal(TemporalType.DATE)
    private Date contractEffectiveDateStart;
    @Column(name = "contractEffectiveDateEnd")
    @Temporal(TemporalType.DATE)
    private Date contractEffectiveDateEnd;
    @Column(name = "cancelDateStart")
    @Temporal(TemporalType.DATE)
    private Date cancelDateStart;
    @Column(name = "cancelDateEnd")
    @Temporal(TemporalType.DATE)
    private Date cancelDateEnd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "cancelReasonIdFk", referencedColumnName = "CancelReasonId")
    @ManyToOne
    private CfCancelReason cancelReasonIdFk;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "productTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne
    private ProductType productTypeIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelMethodIdFk")
    private Collection<CfCancelMethodGroup> cfCancelMethodGroupCollection;

    public CfCancelMethod() {
    }

    public CfCancelMethod(Integer cancelMethodId) {
        this.cancelMethodId = cancelMethodId;
    }

    public CfCancelMethod(Integer cancelMethodId, String description, boolean deletedInd) {
        this.cancelMethodId = cancelMethodId;
        this.description = description;
        this.deletedInd = deletedInd;
    }

    public Integer getCancelMethodId() {
        return cancelMethodId;
    }

    public void setCancelMethodId(Integer cancelMethodId) {
        this.cancelMethodId = cancelMethodId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Date getContractEffectiveDateStart() {
        return contractEffectiveDateStart;
    }

    public void setContractEffectiveDateStart(Date contractEffectiveDateStart) {
        this.contractEffectiveDateStart = contractEffectiveDateStart;
    }

    public Date getContractEffectiveDateEnd() {
        return contractEffectiveDateEnd;
    }

    public void setContractEffectiveDateEnd(Date contractEffectiveDateEnd) {
        this.contractEffectiveDateEnd = contractEffectiveDateEnd;
    }

    public Date getCancelDateStart() {
        return cancelDateStart;
    }

    public void setCancelDateStart(Date cancelDateStart) {
        this.cancelDateStart = cancelDateStart;
    }

    public Date getCancelDateEnd() {
        return cancelDateEnd;
    }

    public void setCancelDateEnd(Date cancelDateEnd) {
        this.cancelDateEnd = cancelDateEnd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCancelReason getCancelReasonIdFk() {
        return cancelReasonIdFk;
    }

    public void setCancelReasonIdFk(CfCancelReason cancelReasonIdFk) {
        this.cancelReasonIdFk = cancelReasonIdFk;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroup> getCfCancelMethodGroupCollection() {
        return cfCancelMethodGroupCollection;
    }

    public void setCfCancelMethodGroupCollection(Collection<CfCancelMethodGroup> cfCancelMethodGroupCollection) {
        this.cfCancelMethodGroupCollection = cfCancelMethodGroupCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelMethodId != null ? cancelMethodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCancelMethod)) {
            return false;
        }
        CfCancelMethod other = (CfCancelMethod) object;
        if ((this.cancelMethodId == null && other.cancelMethodId != null) || (this.cancelMethodId != null && !this.cancelMethodId.equals(other.cancelMethodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCancelMethod[ cancelMethodId=" + cancelMethodId + " ]";
    }
    
}
