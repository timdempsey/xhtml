/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractDisbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractDisbursement.findAll", query = "SELECT c FROM ContractDisbursement c")
    , @NamedQuery(name = "ContractDisbursement.findByContractDisbursementId", query = "SELECT c FROM ContractDisbursement c WHERE c.contractDisbursementId = :contractDisbursementId")
    , @NamedQuery(name = "ContractDisbursement.findByDisbursementTypeInd", query = "SELECT c FROM ContractDisbursement c WHERE c.disbursementTypeInd = :disbursementTypeInd")
    , @NamedQuery(name = "ContractDisbursement.findByDisbursementRefundTypeInd", query = "SELECT c FROM ContractDisbursement c WHERE c.disbursementRefundTypeInd = :disbursementRefundTypeInd")
    , @NamedQuery(name = "ContractDisbursement.findBySystemCreatedInd", query = "SELECT c FROM ContractDisbursement c WHERE c.systemCreatedInd = :systemCreatedInd")
    , @NamedQuery(name = "ContractDisbursement.findByRenewableInd", query = "SELECT c FROM ContractDisbursement c WHERE c.renewableInd = :renewableInd")
    , @NamedQuery(name = "ContractDisbursement.findByCededReserveInd", query = "SELECT c FROM ContractDisbursement c WHERE c.cededReserveInd = :cededReserveInd")
    , @NamedQuery(name = "ContractDisbursement.findByCreatedFromDisbursementIdFk", query = "SELECT c FROM ContractDisbursement c WHERE c.createdFromDisbursementIdFk = :createdFromDisbursementIdFk")
    , @NamedQuery(name = "ContractDisbursement.findByRelatedExceptionFeeIdFk", query = "SELECT c FROM ContractDisbursement c WHERE c.relatedExceptionFeeIdFk = :relatedExceptionFeeIdFk")
    , @NamedQuery(name = "ContractDisbursement.findByNoChargeBackGroupIdFk", query = "SELECT c FROM ContractDisbursement c WHERE c.noChargeBackGroupIdFk = :noChargeBackGroupIdFk")
    , @NamedQuery(name = "ContractDisbursement.findByUpdateUserName", query = "SELECT c FROM ContractDisbursement c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractDisbursement.findByUpdateLast", query = "SELECT c FROM ContractDisbursement c WHERE c.updateLast = :updateLast")})
public class ContractDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractDisbursementId")
    private Integer contractDisbursementId;
    @Column(name = "disbursementTypeInd")
    private Integer disbursementTypeInd;
    @Column(name = "disbursementRefundTypeInd")
    private Integer disbursementRefundTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "systemCreatedInd")
    private boolean systemCreatedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renewableInd")
    private boolean renewableInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cededReserveInd")
    private boolean cededReserveInd;
    @Column(name = "createdFromDisbursementIdFk")
    private Integer createdFromDisbursementIdFk;
    @Column(name = "relatedExceptionFeeIdFk")
    private Integer relatedExceptionFeeIdFk;
    @Column(name = "noChargeBackGroupIdFk")
    private Integer noChargeBackGroupIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "relatedAdjustmentIdFk", referencedColumnName = "adjustmentId")
    @ManyToOne
    private Adjustment relatedAdjustmentIdFk;
    @JoinColumn(name = "ContractAmendmentIdFk", referencedColumnName = "ContractAmendmentId")
    @ManyToOne
    private ContractAmendment contractAmendmentIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "relatedDeductibleIdFk", referencedColumnName = "deductibleId")
    @ManyToOne
    private Deductible relatedDeductibleIdFk;
    @JoinColumn(name = "disbursementGroupIdFk", referencedColumnName = "disbursementGroupId")
    @ManyToOne
    private DisbursementGroup disbursementGroupIdFk;
    @JoinColumn(name = "contractDisbursementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "relatedSurchargeIdFk", referencedColumnName = "surchargeId")
    @ManyToOne
    private Surcharge relatedSurchargeIdFk;
    @OneToMany(mappedBy = "relatedContractDisbursementIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "contractDisbursementIdFk")
    private Collection<HoldDisbursement> holdDisbursementCollection;
    @OneToMany(mappedBy = "contractDisbursementIdFk")
    private Collection<CancelDisbursement> cancelDisbursementCollection;
    @OneToMany(mappedBy = "contractDisbursementIdFk")
    private Collection<ContractEarning> contractEarningCollection;

    public ContractDisbursement() {
    }

    public ContractDisbursement(Integer contractDisbursementId) {
        this.contractDisbursementId = contractDisbursementId;
    }

    public ContractDisbursement(Integer contractDisbursementId, boolean systemCreatedInd, boolean renewableInd, boolean cededReserveInd) {
        this.contractDisbursementId = contractDisbursementId;
        this.systemCreatedInd = systemCreatedInd;
        this.renewableInd = renewableInd;
        this.cededReserveInd = cededReserveInd;
    }

    public Integer getContractDisbursementId() {
        return contractDisbursementId;
    }

    public void setContractDisbursementId(Integer contractDisbursementId) {
        this.contractDisbursementId = contractDisbursementId;
    }

    public Integer getDisbursementTypeInd() {
        return disbursementTypeInd;
    }

    public void setDisbursementTypeInd(Integer disbursementTypeInd) {
        this.disbursementTypeInd = disbursementTypeInd;
    }

    public Integer getDisbursementRefundTypeInd() {
        return disbursementRefundTypeInd;
    }

    public void setDisbursementRefundTypeInd(Integer disbursementRefundTypeInd) {
        this.disbursementRefundTypeInd = disbursementRefundTypeInd;
    }

    public boolean getSystemCreatedInd() {
        return systemCreatedInd;
    }

    public void setSystemCreatedInd(boolean systemCreatedInd) {
        this.systemCreatedInd = systemCreatedInd;
    }

    public boolean getRenewableInd() {
        return renewableInd;
    }

    public void setRenewableInd(boolean renewableInd) {
        this.renewableInd = renewableInd;
    }

    public boolean getCededReserveInd() {
        return cededReserveInd;
    }

    public void setCededReserveInd(boolean cededReserveInd) {
        this.cededReserveInd = cededReserveInd;
    }

    public Integer getCreatedFromDisbursementIdFk() {
        return createdFromDisbursementIdFk;
    }

    public void setCreatedFromDisbursementIdFk(Integer createdFromDisbursementIdFk) {
        this.createdFromDisbursementIdFk = createdFromDisbursementIdFk;
    }

    public Integer getRelatedExceptionFeeIdFk() {
        return relatedExceptionFeeIdFk;
    }

    public void setRelatedExceptionFeeIdFk(Integer relatedExceptionFeeIdFk) {
        this.relatedExceptionFeeIdFk = relatedExceptionFeeIdFk;
    }

    public Integer getNoChargeBackGroupIdFk() {
        return noChargeBackGroupIdFk;
    }

    public void setNoChargeBackGroupIdFk(Integer noChargeBackGroupIdFk) {
        this.noChargeBackGroupIdFk = noChargeBackGroupIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Adjustment getRelatedAdjustmentIdFk() {
        return relatedAdjustmentIdFk;
    }

    public void setRelatedAdjustmentIdFk(Adjustment relatedAdjustmentIdFk) {
        this.relatedAdjustmentIdFk = relatedAdjustmentIdFk;
    }

    public ContractAmendment getContractAmendmentIdFk() {
        return contractAmendmentIdFk;
    }

    public void setContractAmendmentIdFk(ContractAmendment contractAmendmentIdFk) {
        this.contractAmendmentIdFk = contractAmendmentIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Deductible getRelatedDeductibleIdFk() {
        return relatedDeductibleIdFk;
    }

    public void setRelatedDeductibleIdFk(Deductible relatedDeductibleIdFk) {
        this.relatedDeductibleIdFk = relatedDeductibleIdFk;
    }

    public DisbursementGroup getDisbursementGroupIdFk() {
        return disbursementGroupIdFk;
    }

    public void setDisbursementGroupIdFk(DisbursementGroup disbursementGroupIdFk) {
        this.disbursementGroupIdFk = disbursementGroupIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Surcharge getRelatedSurchargeIdFk() {
        return relatedSurchargeIdFk;
    }

    public void setRelatedSurchargeIdFk(Surcharge relatedSurchargeIdFk) {
        this.relatedSurchargeIdFk = relatedSurchargeIdFk;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<HoldDisbursement> getHoldDisbursementCollection() {
        return holdDisbursementCollection;
    }

    public void setHoldDisbursementCollection(Collection<HoldDisbursement> holdDisbursementCollection) {
        this.holdDisbursementCollection = holdDisbursementCollection;
    }

    @XmlTransient
    public Collection<CancelDisbursement> getCancelDisbursementCollection() {
        return cancelDisbursementCollection;
    }

    public void setCancelDisbursementCollection(Collection<CancelDisbursement> cancelDisbursementCollection) {
        this.cancelDisbursementCollection = cancelDisbursementCollection;
    }

    @XmlTransient
    public Collection<ContractEarning> getContractEarningCollection() {
        return contractEarningCollection;
    }

    public void setContractEarningCollection(Collection<ContractEarning> contractEarningCollection) {
        this.contractEarningCollection = contractEarningCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractDisbursementId != null ? contractDisbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractDisbursement)) {
            return false;
        }
        ContractDisbursement other = (ContractDisbursement) object;
        if ((this.contractDisbursementId == null && other.contractDisbursementId != null) || (this.contractDisbursementId != null && !this.contractDisbursementId.equals(other.contractDisbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractDisbursement[ contractDisbursementId=" + contractDisbursementId + " ]";
    }
    
}
