/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class Notes {

    public Notes() {
    }

    public Notes(Integer noteId, String note, String enteredBy, String enteredDate) {
        this.noteId = noteId;
        this.note = note;
        this.enteredBy = enteredBy;
        this.enteredDate = enteredDate;
    }

    
    
    

    private Integer noteId;
    private String note;
    private String enteredBy;
    private String enteredDate;

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public String getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }

    

    public void reset() {
        noteId = null;
        note = "";
        enteredBy = "";
        enteredDate = "";

    }

}
