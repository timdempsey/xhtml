/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Insurer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insurer.findAll", query = "SELECT i FROM Insurer i")
    , @NamedQuery(name = "Insurer.findByInsurerId", query = "SELECT i FROM Insurer i WHERE i.insurerId = :insurerId")
    , @NamedQuery(name = "Insurer.findByInsurerType", query = "SELECT i FROM Insurer i WHERE i.insurerType = :insurerType")
    , @NamedQuery(name = "Insurer.findByUpdateUserName", query = "SELECT i FROM Insurer i WHERE i.updateUserName = :updateUserName")
    , @NamedQuery(name = "Insurer.findByUpdateLast", query = "SELECT i FROM Insurer i WHERE i.updateLast = :updateLast")})
public class Insurer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "insurerId")
    private Integer insurerId;
    @Column(name = "insurerType")
    private Integer insurerType;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insurerIdFk")
    private Collection<Product> productCollection;
    @OneToMany(mappedBy = "insurerIdFk")
    private Collection<ReinsuranceSplit> reinsuranceSplitCollection;
    @OneToMany(mappedBy = "reinsurerIdFk")
    private Collection<ReinsuranceReinsurer> reinsuranceReinsurerCollection;
    @OneToMany(mappedBy = "insurerIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "insurerIdFk")
    private Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection;
    @OneToMany(mappedBy = "reinsurerIdFk")
    private Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection1;
    @JoinColumn(name = "insurerId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @OneToMany(mappedBy = "insurerIdFk")
    private Collection<ReinsuranceFee> reinsuranceFeeCollection;
    @OneToMany(mappedBy = "reinsurerIdFk")
    private Collection<ReinsuranceFee> reinsuranceFeeCollection1;
    @OneToMany(mappedBy = "insurerIdFk")
    private Collection<Dealer> dealerCollection;

    public Insurer() {
    }

    public Insurer(Integer insurerId) {
        this.insurerId = insurerId;
    }

    public Integer getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Integer insurerId) {
        this.insurerId = insurerId;
    }

    public Integer getInsurerType() {
        return insurerType;
    }

    public void setInsurerType(Integer insurerType) {
        this.insurerType = insurerType;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Product> getProductCollection() {
        return productCollection;
    }

    public void setProductCollection(Collection<Product> productCollection) {
        this.productCollection = productCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceSplit> getReinsuranceSplitCollection() {
        return reinsuranceSplitCollection;
    }

    public void setReinsuranceSplitCollection(Collection<ReinsuranceSplit> reinsuranceSplitCollection) {
        this.reinsuranceSplitCollection = reinsuranceSplitCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceReinsurer> getReinsuranceReinsurerCollection() {
        return reinsuranceReinsurerCollection;
    }

    public void setReinsuranceReinsurerCollection(Collection<ReinsuranceReinsurer> reinsuranceReinsurerCollection) {
        this.reinsuranceReinsurerCollection = reinsuranceReinsurerCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceCustodialAccountBalance> getReinsuranceCustodialAccountBalanceCollection() {
        return reinsuranceCustodialAccountBalanceCollection;
    }

    public void setReinsuranceCustodialAccountBalanceCollection(Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection) {
        this.reinsuranceCustodialAccountBalanceCollection = reinsuranceCustodialAccountBalanceCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceCustodialAccountBalance> getReinsuranceCustodialAccountBalanceCollection1() {
        return reinsuranceCustodialAccountBalanceCollection1;
    }

    public void setReinsuranceCustodialAccountBalanceCollection1(Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection1) {
        this.reinsuranceCustodialAccountBalanceCollection1 = reinsuranceCustodialAccountBalanceCollection1;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    @XmlTransient
    public Collection<ReinsuranceFee> getReinsuranceFeeCollection() {
        return reinsuranceFeeCollection;
    }

    public void setReinsuranceFeeCollection(Collection<ReinsuranceFee> reinsuranceFeeCollection) {
        this.reinsuranceFeeCollection = reinsuranceFeeCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceFee> getReinsuranceFeeCollection1() {
        return reinsuranceFeeCollection1;
    }

    public void setReinsuranceFeeCollection1(Collection<ReinsuranceFee> reinsuranceFeeCollection1) {
        this.reinsuranceFeeCollection1 = reinsuranceFeeCollection1;
    }

    @XmlTransient
    public Collection<Dealer> getDealerCollection() {
        return dealerCollection;
    }

    public void setDealerCollection(Collection<Dealer> dealerCollection) {
        this.dealerCollection = dealerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insurerId != null ? insurerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insurer)) {
            return false;
        }
        Insurer other = (Insurer) object;
        if ((this.insurerId == null && other.insurerId != null) || (this.insurerId != null && !this.insurerId.equals(other.insurerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Insurer[ insurerId=" + insurerId + " ]";
    }
    
}
