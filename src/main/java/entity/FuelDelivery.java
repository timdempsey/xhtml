/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "FuelDelivery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FuelDelivery.findAll", query = "SELECT f FROM FuelDelivery f")
    , @NamedQuery(name = "FuelDelivery.findByFuelDeliveryId", query = "SELECT f FROM FuelDelivery f WHERE f.fuelDeliveryId = :fuelDeliveryId")
    , @NamedQuery(name = "FuelDelivery.findByDescription", query = "SELECT f FROM FuelDelivery f WHERE f.description = :description")})
public class FuelDelivery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fuelDeliveryId")
    private Integer fuelDeliveryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Description")
    private String description;
    @OneToMany(mappedBy = "fuelDeliveryIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "fuelDeliveryIdFk")
    private Collection<VinDesc> vinDescCollection;

    public FuelDelivery() {
    }

    public FuelDelivery(Integer fuelDeliveryId) {
        this.fuelDeliveryId = fuelDeliveryId;
    }

    public FuelDelivery(Integer fuelDeliveryId, String description) {
        this.fuelDeliveryId = fuelDeliveryId;
        this.description = description;
    }

    public Integer getFuelDeliveryId() {
        return fuelDeliveryId;
    }

    public void setFuelDeliveryId(Integer fuelDeliveryId) {
        this.fuelDeliveryId = fuelDeliveryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<VinDesc> getVinDescCollection() {
        return vinDescCollection;
    }

    public void setVinDescCollection(Collection<VinDesc> vinDescCollection) {
        this.vinDescCollection = vinDescCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fuelDeliveryId != null ? fuelDeliveryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FuelDelivery)) {
            return false;
        }
        FuelDelivery other = (FuelDelivery) object;
        if ((this.fuelDeliveryId == null && other.fuelDeliveryId != null) || (this.fuelDeliveryId != null && !this.fuelDeliveryId.equals(other.fuelDeliveryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.FuelDelivery[ fuelDeliveryId=" + fuelDeliveryId + " ]";
    }
    
}
