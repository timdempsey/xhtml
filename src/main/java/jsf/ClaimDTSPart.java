/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimDTSPart {
    
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public ClaimDTSPart() {
    }


    public ClaimDTSPart(Integer claimPartId, String updateUserName, String updateLast, Integer partQty, String partNumber, String partDesc, BigDecimal partCost, BigDecimal partSubTotal) {
        this.claimPartId = claimPartId;
        this.updateUserName = updateUserName;
        this.updateLast = updateLast;
        this.partQty = partQty;
        this.partNumber = partNumber;
        this.partDesc = partDesc;
        this.partCost = partCost;
        this.partSubTotal = partSubTotal;
    }
    
    
    
    @Id
    @Column(name = "claimPartId", table="ClaimPart")
    private Integer claimPartId;
    @Column(name = "updateUserName", table="ClaimPart")
    private String updateUserName;
    @Column(name = "updateLast", table="ClaimPart")
    private String updateLast;
    @Column(name="partQty", table="ClaimPart")
    Integer partQty;
    @Column(name="partNumber", table="ClaimPart")
    String partNumber;
    @Column(name="partDesc", table="ClaimPart")
    String partDesc;
    @Column(name="partCost", table="ClaimPart")
    BigDecimal partCost;
    @Column(name="partSubTotal", table="ClaimPart")
    BigDecimal partSubTotal;

    public void reset() {
        this.claimPartId = null;
        this.updateUserName = null;
        this.updateLast = null;
        this.partQty = null;
        this.partNumber = null;
        this.partDesc = null;
        this.partCost = null;
        this.partSubTotal = null;
    }
    
    public Integer getClaimPartId() {
        return claimPartId;
    }

    public void setClaimPartId(Integer claimPartId) {
        this.claimPartId = claimPartId;
    }

    
    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public String getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(String updateLast) {
        this.updateLast = updateLast;
    }

    public Integer getPartQty() {
        return partQty;
    }

    public void setPartQty(Integer partQty) {
        this.partQty = partQty;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDesc() {
        return partDesc;
    }

    public void setPartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    public BigDecimal getPartCost() {
        return partCost;
    }

    public void setPartCost(BigDecimal partCost) {
        this.partCost = partCost;
    }

    public BigDecimal getPartSubTotal() {
        return partSubTotal;
    }

    public void setPartSubTotal(BigDecimal partSubTotal) {
        this.partSubTotal = partSubTotal;
    }
    
    
}
