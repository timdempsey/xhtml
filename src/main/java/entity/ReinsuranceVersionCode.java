/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceVersionCode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceVersionCode.findAll", query = "SELECT r FROM ReinsuranceVersionCode r")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByReinsuranceVersionCodeId", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.reinsuranceVersionCodeId = :reinsuranceVersionCodeId")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByCode", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.code = :code")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByDescription", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.description = :description")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByDeletedInd", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.deletedInd = :deletedInd")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByUpdateUserName", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceVersionCode.findByUpdateLast", query = "SELECT r FROM ReinsuranceVersionCode r WHERE r.updateLast = :updateLast")})
public class ReinsuranceVersionCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceVersionCodeId")
    private Integer reinsuranceVersionCodeId;
    @Size(max = 50)
    @Column(name = "code")
    private String code;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "reinsuranceVersionCodeIdFk")
    private Collection<ReinsuranceSplit> reinsuranceSplitCollection;
    @OneToMany(mappedBy = "reinsuranceVersionCodeIdFk")
    private Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection;
    @OneToMany(mappedBy = "reinsuranceVersionCodeIdFk")
    private Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection;

    public ReinsuranceVersionCode() {
    }

    public ReinsuranceVersionCode(Integer reinsuranceVersionCodeId) {
        this.reinsuranceVersionCodeId = reinsuranceVersionCodeId;
    }

    public Integer getReinsuranceVersionCodeId() {
        return reinsuranceVersionCodeId;
    }

    public void setReinsuranceVersionCodeId(Integer reinsuranceVersionCodeId) {
        this.reinsuranceVersionCodeId = reinsuranceVersionCodeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ReinsuranceSplit> getReinsuranceSplitCollection() {
        return reinsuranceSplitCollection;
    }

    public void setReinsuranceSplitCollection(Collection<ReinsuranceSplit> reinsuranceSplitCollection) {
        this.reinsuranceSplitCollection = reinsuranceSplitCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceContractFee> getReinsuranceContractFeeCollection() {
        return reinsuranceContractFeeCollection;
    }

    public void setReinsuranceContractFeeCollection(Collection<ReinsuranceContractFee> reinsuranceContractFeeCollection) {
        this.reinsuranceContractFeeCollection = reinsuranceContractFeeCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceRuleDetail> getReinsuranceRuleDetailCollection() {
        return reinsuranceRuleDetailCollection;
    }

    public void setReinsuranceRuleDetailCollection(Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection) {
        this.reinsuranceRuleDetailCollection = reinsuranceRuleDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceVersionCodeId != null ? reinsuranceVersionCodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceVersionCode)) {
            return false;
        }
        ReinsuranceVersionCode other = (ReinsuranceVersionCode) object;
        if ((this.reinsuranceVersionCodeId == null && other.reinsuranceVersionCodeId != null) || (this.reinsuranceVersionCodeId != null && !this.reinsuranceVersionCodeId.equals(other.reinsuranceVersionCodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceVersionCode[ reinsuranceVersionCodeId=" + reinsuranceVersionCodeId + " ]";
    }
    
}
