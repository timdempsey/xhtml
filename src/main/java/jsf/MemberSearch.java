/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class MemberSearch implements Serializable {
    private static final Logger LOGGER = LogManager.getLogger(MemberSearch.class);

    public MemberSearch() {
    }

    public MemberSearch(Integer userMemberId, String shortName, Integer accountKeeperId, String longName ) {
        this.userMemberId = userMemberId;
        this.shortName = shortName;
        this.accountKeeperId = accountKeeperId;
        this.longName = longName;
    }
    
    
    
    @Id
    @Column(name = "userMemberId", table = "UserMember")
    Integer userMemberId;
    @Column(name = "userName", table = "UserMember")
    String shortName;
    @Column(name = "accountKeeperId", table = "AccountKeeper")
    Integer accountKeeperId;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    String longName;
    

    public Integer getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(Integer userMemberId) {
        this.userMemberId = userMemberId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getAccountKeeperId() {
        return accountKeeperId;
    }

    public void setAccountKeeperId(Integer accountKeeperId) {
        this.accountKeeperId = accountKeeperId;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }
    
}
