/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Ledger")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ledger.findAll", query = "SELECT l FROM Ledger l")
    , @NamedQuery(name = "Ledger.findByLedgerId", query = "SELECT l FROM Ledger l WHERE l.ledgerId = :ledgerId")
    , @NamedQuery(name = "Ledger.findByAmount", query = "SELECT l FROM Ledger l WHERE l.amount = :amount")
    , @NamedQuery(name = "Ledger.findByCurrentAmount", query = "SELECT l FROM Ledger l WHERE l.currentAmount = :currentAmount")
    , @NamedQuery(name = "Ledger.findByEnteredDate", query = "SELECT l FROM Ledger l WHERE l.enteredDate = :enteredDate")
    , @NamedQuery(name = "Ledger.findByLedgerDate", query = "SELECT l FROM Ledger l WHERE l.ledgerDate = :ledgerDate")
    , @NamedQuery(name = "Ledger.findByLedgerTypeInd", query = "SELECT l FROM Ledger l WHERE l.ledgerTypeInd = :ledgerTypeInd")
    , @NamedQuery(name = "Ledger.findByRelatedLedgerTypeInd", query = "SELECT l FROM Ledger l WHERE l.relatedLedgerTypeInd = :relatedLedgerTypeInd")
    , @NamedQuery(name = "Ledger.findByLowersAdminCompanyBalanceInd", query = "SELECT l FROM Ledger l WHERE l.lowersAdminCompanyBalanceInd = :lowersAdminCompanyBalanceInd")
    , @NamedQuery(name = "Ledger.findByPayableInd", query = "SELECT l FROM Ledger l WHERE l.payableInd = :payableInd")
    , @NamedQuery(name = "Ledger.findBySetLedgerDate", query = "SELECT l FROM Ledger l WHERE l.setLedgerDate = :setLedgerDate")
    , @NamedQuery(name = "Ledger.findByExemptInd", query = "SELECT l FROM Ledger l WHERE l.exemptInd = :exemptInd")
    , @NamedQuery(name = "Ledger.findByDescription", query = "SELECT l FROM Ledger l WHERE l.description = :description")
    , @NamedQuery(name = "Ledger.findByCashTransactionIdFk", query = "SELECT l FROM Ledger l WHERE l.cashTransactionIdFk = :cashTransactionIdFk")
    , @NamedQuery(name = "Ledger.findByBalanceTypeIdFk", query = "SELECT l FROM Ledger l WHERE l.balanceTypeIdFk = :balanceTypeIdFk")
    , @NamedQuery(name = "Ledger.findByUpdateUserName", query = "SELECT l FROM Ledger l WHERE l.updateUserName = :updateUserName")
    , @NamedQuery(name = "Ledger.findByUpdateLast", query = "SELECT l FROM Ledger l WHERE l.updateLast = :updateLast")})
public class Ledger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ledgerId")
    private Integer ledgerId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private BigDecimal amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "currentAmount")
    private BigDecimal currentAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "ledgerDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ledgerDate;
    @Column(name = "ledgerTypeInd")
    private Integer ledgerTypeInd;
    @Column(name = "relatedLedgerTypeInd")
    private Integer relatedLedgerTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lowersAdminCompanyBalanceInd")
    private boolean lowersAdminCompanyBalanceInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payableInd")
    private boolean payableInd;
    @Column(name = "setLedgerDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date setLedgerDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exemptInd")
    private boolean exemptInd;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Column(name = "cashTransactionIdFk")
    private Integer cashTransactionIdFk;
    @Column(name = "balanceTypeIdFk")
    private Integer balanceTypeIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private CashTransaction cashTransaction;
    @OneToMany(mappedBy = "ledgerIdFk")
    private Collection<Note> noteCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ResolveDisbursement resolveDisbursement;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ContractDisbursement contractDisbursement;
    @JoinColumn(name = "relatedCancelIdFk", referencedColumnName = "contractCancelId")
    @ManyToOne
    private ContractCancel relatedCancelIdFk;
    @JoinColumn(name = "CurrentAccountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne(optional = false)
    private AccountKeeper currentAccountKeeperIdFk;
    @JoinColumn(name = "originalAccountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne(optional = false)
    private AccountKeeper originalAccountKeeperIdFk;
    @JoinColumn(name = "adminCompanyIdFk", referencedColumnName = "adminCompanyId")
    @ManyToOne(optional = false)
    private AdminCompany adminCompanyIdFk;
    @JoinColumn(name = "relatedClaimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim relatedClaimIdFk;
    @JoinColumn(name = "relatedAmendmentIdFk", referencedColumnName = "ContractAmendmentId")
    @ManyToOne
    private ContractAmendment relatedAmendmentIdFk;
    @JoinColumn(name = "relatedContractDisbursementIdFk", referencedColumnName = "contractDisbursementId")
    @ManyToOne
    private ContractDisbursement relatedContractDisbursementIdFk;
    @JoinColumn(name = "relatedReinstatementDisbursementIdFk", referencedColumnName = "reinstatementDisbursementId")
    @ManyToOne
    private ReinstatementDisbursement relatedReinstatementDisbursementIdFk;
    @JoinColumn(name = "relatedContractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts relatedContractIdFk;
    @JoinColumn(name = "relatedReinstatementIdFk", referencedColumnName = "reinstatementId")
    @ManyToOne
    private Reinstatement relatedReinstatementIdFk;
    @JoinColumn(name = "insurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer insurerIdFk;
    @OneToMany(mappedBy = "relatedLedgerIdFk")
    private Collection<Ledger> ledgerCollection;
    @JoinColumn(name = "relatedLedgerIdFk", referencedColumnName = "ledgerId")
    @ManyToOne
    private Ledger relatedLedgerIdFk;
    @JoinColumn(name = "postingHistoryIdFk", referencedColumnName = "postingHistoryId")
    @ManyToOne
    private PostingHistory postingHistoryIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ReinstatementDisbursement reinstatementDisbursement;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ContractCancel contractCancel;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private LedgerEdit ledgerEdit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "editedLedgerIdFk")
    private Collection<LedgerEdit> ledgerEditCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private HoldDisbursement holdDisbursement;
    @OneToMany(mappedBy = "ledgerIdFk")
    private Collection<HistoryLog> historyLogCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ContractHold contractHold;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ContractResolve contractResolve;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private Reinstatement reinstatement;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ClaimPaymentAuthorization claimPaymentAuthorization;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private ContractAmendment contractAmendment;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private CancelDisbursement cancelDisbursement;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ledger")
    private LedgerDelete ledgerDelete;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "deletedLedgerIdFk")
    private LedgerDelete ledgerDelete1;

    public Ledger() {
    }

    public Ledger(Integer ledgerId) {
        this.ledgerId = ledgerId;
    }

    public Ledger(Integer ledgerId, BigDecimal amount, BigDecimal currentAmount, Date enteredDate, boolean lowersAdminCompanyBalanceInd, boolean payableInd, boolean exemptInd) {
        this.ledgerId = ledgerId;
        this.amount = amount;
        this.currentAmount = currentAmount;
        this.enteredDate = enteredDate;
        this.lowersAdminCompanyBalanceInd = lowersAdminCompanyBalanceInd;
        this.payableInd = payableInd;
        this.exemptInd = exemptInd;
    }

    public Integer getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Integer ledgerId) {
        this.ledgerId = ledgerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Date getLedgerDate() {
        return ledgerDate;
    }

    public void setLedgerDate(Date ledgerDate) {
        this.ledgerDate = ledgerDate;
    }

    public Integer getLedgerTypeInd() {
        return ledgerTypeInd;
    }

    public void setLedgerTypeInd(Integer ledgerTypeInd) {
        this.ledgerTypeInd = ledgerTypeInd;
    }

    public Integer getRelatedLedgerTypeInd() {
        return relatedLedgerTypeInd;
    }

    public void setRelatedLedgerTypeInd(Integer relatedLedgerTypeInd) {
        this.relatedLedgerTypeInd = relatedLedgerTypeInd;
    }

    public boolean getLowersAdminCompanyBalanceInd() {
        return lowersAdminCompanyBalanceInd;
    }

    public void setLowersAdminCompanyBalanceInd(boolean lowersAdminCompanyBalanceInd) {
        this.lowersAdminCompanyBalanceInd = lowersAdminCompanyBalanceInd;
    }

    public boolean getPayableInd() {
        return payableInd;
    }

    public void setPayableInd(boolean payableInd) {
        this.payableInd = payableInd;
    }

    public Date getSetLedgerDate() {
        return setLedgerDate;
    }

    public void setSetLedgerDate(Date setLedgerDate) {
        this.setLedgerDate = setLedgerDate;
    }

    public boolean getExemptInd() {
        return exemptInd;
    }

    public void setExemptInd(boolean exemptInd) {
        this.exemptInd = exemptInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCashTransactionIdFk() {
        return cashTransactionIdFk;
    }

    public void setCashTransactionIdFk(Integer cashTransactionIdFk) {
        this.cashTransactionIdFk = cashTransactionIdFk;
    }

    public Integer getBalanceTypeIdFk() {
        return balanceTypeIdFk;
    }

    public void setBalanceTypeIdFk(Integer balanceTypeIdFk) {
        this.balanceTypeIdFk = balanceTypeIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CashTransaction getCashTransaction() {
        return cashTransaction;
    }

    public void setCashTransaction(CashTransaction cashTransaction) {
        this.cashTransaction = cashTransaction;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    public ResolveDisbursement getResolveDisbursement() {
        return resolveDisbursement;
    }

    public void setResolveDisbursement(ResolveDisbursement resolveDisbursement) {
        this.resolveDisbursement = resolveDisbursement;
    }

    public ContractDisbursement getContractDisbursement() {
        return contractDisbursement;
    }

    public void setContractDisbursement(ContractDisbursement contractDisbursement) {
        this.contractDisbursement = contractDisbursement;
    }

    public ContractCancel getRelatedCancelIdFk() {
        return relatedCancelIdFk;
    }

    public void setRelatedCancelIdFk(ContractCancel relatedCancelIdFk) {
        this.relatedCancelIdFk = relatedCancelIdFk;
    }

    public AccountKeeper getCurrentAccountKeeperIdFk() {
        return currentAccountKeeperIdFk;
    }

    public void setCurrentAccountKeeperIdFk(AccountKeeper currentAccountKeeperIdFk) {
        this.currentAccountKeeperIdFk = currentAccountKeeperIdFk;
    }

    public AccountKeeper getOriginalAccountKeeperIdFk() {
        return originalAccountKeeperIdFk;
    }

    public void setOriginalAccountKeeperIdFk(AccountKeeper originalAccountKeeperIdFk) {
        this.originalAccountKeeperIdFk = originalAccountKeeperIdFk;
    }

    public AdminCompany getAdminCompanyIdFk() {
        return adminCompanyIdFk;
    }

    public void setAdminCompanyIdFk(AdminCompany adminCompanyIdFk) {
        this.adminCompanyIdFk = adminCompanyIdFk;
    }

    public Claim getRelatedClaimIdFk() {
        return relatedClaimIdFk;
    }

    public void setRelatedClaimIdFk(Claim relatedClaimIdFk) {
        this.relatedClaimIdFk = relatedClaimIdFk;
    }

    public ContractAmendment getRelatedAmendmentIdFk() {
        return relatedAmendmentIdFk;
    }

    public void setRelatedAmendmentIdFk(ContractAmendment relatedAmendmentIdFk) {
        this.relatedAmendmentIdFk = relatedAmendmentIdFk;
    }

    public ContractDisbursement getRelatedContractDisbursementIdFk() {
        return relatedContractDisbursementIdFk;
    }

    public void setRelatedContractDisbursementIdFk(ContractDisbursement relatedContractDisbursementIdFk) {
        this.relatedContractDisbursementIdFk = relatedContractDisbursementIdFk;
    }

    public ReinstatementDisbursement getRelatedReinstatementDisbursementIdFk() {
        return relatedReinstatementDisbursementIdFk;
    }

    public void setRelatedReinstatementDisbursementIdFk(ReinstatementDisbursement relatedReinstatementDisbursementIdFk) {
        this.relatedReinstatementDisbursementIdFk = relatedReinstatementDisbursementIdFk;
    }

    public Contracts getRelatedContractIdFk() {
        return relatedContractIdFk;
    }

    public void setRelatedContractIdFk(Contracts relatedContractIdFk) {
        this.relatedContractIdFk = relatedContractIdFk;
    }

    public Reinstatement getRelatedReinstatementIdFk() {
        return relatedReinstatementIdFk;
    }

    public void setRelatedReinstatementIdFk(Reinstatement relatedReinstatementIdFk) {
        this.relatedReinstatementIdFk = relatedReinstatementIdFk;
    }

    public Insurer getInsurerIdFk() {
        return insurerIdFk;
    }

    public void setInsurerIdFk(Insurer insurerIdFk) {
        this.insurerIdFk = insurerIdFk;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    public Ledger getRelatedLedgerIdFk() {
        return relatedLedgerIdFk;
    }

    public void setRelatedLedgerIdFk(Ledger relatedLedgerIdFk) {
        this.relatedLedgerIdFk = relatedLedgerIdFk;
    }

    public PostingHistory getPostingHistoryIdFk() {
        return postingHistoryIdFk;
    }

    public void setPostingHistoryIdFk(PostingHistory postingHistoryIdFk) {
        this.postingHistoryIdFk = postingHistoryIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public ReinstatementDisbursement getReinstatementDisbursement() {
        return reinstatementDisbursement;
    }

    public void setReinstatementDisbursement(ReinstatementDisbursement reinstatementDisbursement) {
        this.reinstatementDisbursement = reinstatementDisbursement;
    }

    public ContractCancel getContractCancel() {
        return contractCancel;
    }

    public void setContractCancel(ContractCancel contractCancel) {
        this.contractCancel = contractCancel;
    }

    public LedgerEdit getLedgerEdit() {
        return ledgerEdit;
    }

    public void setLedgerEdit(LedgerEdit ledgerEdit) {
        this.ledgerEdit = ledgerEdit;
    }

    @XmlTransient
    public Collection<LedgerEdit> getLedgerEditCollection() {
        return ledgerEditCollection;
    }

    public void setLedgerEditCollection(Collection<LedgerEdit> ledgerEditCollection) {
        this.ledgerEditCollection = ledgerEditCollection;
    }

    public HoldDisbursement getHoldDisbursement() {
        return holdDisbursement;
    }

    public void setHoldDisbursement(HoldDisbursement holdDisbursement) {
        this.holdDisbursement = holdDisbursement;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    public ContractHold getContractHold() {
        return contractHold;
    }

    public void setContractHold(ContractHold contractHold) {
        this.contractHold = contractHold;
    }

    public ContractResolve getContractResolve() {
        return contractResolve;
    }

    public void setContractResolve(ContractResolve contractResolve) {
        this.contractResolve = contractResolve;
    }

    public Reinstatement getReinstatement() {
        return reinstatement;
    }

    public void setReinstatement(Reinstatement reinstatement) {
        this.reinstatement = reinstatement;
    }

    public ClaimPaymentAuthorization getClaimPaymentAuthorization() {
        return claimPaymentAuthorization;
    }

    public void setClaimPaymentAuthorization(ClaimPaymentAuthorization claimPaymentAuthorization) {
        this.claimPaymentAuthorization = claimPaymentAuthorization;
    }

    public ContractAmendment getContractAmendment() {
        return contractAmendment;
    }

    public void setContractAmendment(ContractAmendment contractAmendment) {
        this.contractAmendment = contractAmendment;
    }

    public CancelDisbursement getCancelDisbursement() {
        return cancelDisbursement;
    }

    public void setCancelDisbursement(CancelDisbursement cancelDisbursement) {
        this.cancelDisbursement = cancelDisbursement;
    }

    public LedgerDelete getLedgerDelete() {
        return ledgerDelete;
    }

    public void setLedgerDelete(LedgerDelete ledgerDelete) {
        this.ledgerDelete = ledgerDelete;
    }

    public LedgerDelete getLedgerDelete1() {
        return ledgerDelete1;
    }

    public void setLedgerDelete1(LedgerDelete ledgerDelete1) {
        this.ledgerDelete1 = ledgerDelete1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ledgerId != null ? ledgerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ledger)) {
            return false;
        }
        Ledger other = (Ledger) object;
        if ((this.ledgerId == null && other.ledgerId != null) || (this.ledgerId != null && !this.ledgerId.equals(other.ledgerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Ledger[ ledgerId=" + ledgerId + " ]";
    }
    
}
