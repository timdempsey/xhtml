/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfComponent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfComponent.findAll", query = "SELECT c FROM CfComponent c")
    , @NamedQuery(name = "CfComponent.findByComponentId", query = "SELECT c FROM CfComponent c WHERE c.componentId = :componentId")
    , @NamedQuery(name = "CfComponent.findByComponentCode", query = "SELECT c FROM CfComponent c WHERE c.componentCode = :componentCode")
    , @NamedQuery(name = "CfComponent.findByName", query = "SELECT c FROM CfComponent c WHERE c.name = :name")
    , @NamedQuery(name = "CfComponent.findByDeductibleIgnoreInd", query = "SELECT c FROM CfComponent c WHERE c.deductibleIgnoreInd = :deductibleIgnoreInd")
    , @NamedQuery(name = "CfComponent.findByUpdateUserName", query = "SELECT c FROM CfComponent c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfComponent.findByUpdateLast", query = "SELECT c FROM CfComponent c WHERE c.updateLast = :updateLast")})
public class CfComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "componentId")
    private Integer componentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "componentCode")
    private String componentCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deductibleIgnoreInd")
    private boolean deductibleIgnoreInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "componentIdFk")
    private Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection;
    @OneToMany(mappedBy = "componentIdFk")
    private Collection<PPMProductService> pPMProductServiceCollection;
    @JoinColumn(name = "componentGroupIdFk", referencedColumnName = "componentGroupId")
    @ManyToOne(optional = false)
    private CfComponentGroup componentGroupIdFk;
    @OneToMany(mappedBy = "componentIdFk")
    private Collection<ContractPPMDetailComponent> contractPPMDetailComponentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "componentId")
    private Collection<CfCoverageGroupComponentConn> cfCoverageGroupComponentConnCollection;

    public CfComponent() {
    }

    public CfComponent(Integer componentId) {
        this.componentId = componentId;
    }

    public CfComponent(Integer componentId, String componentCode, String name, boolean deductibleIgnoreInd) {
        this.componentId = componentId;
        this.componentCode = componentCode;
        this.name = name;
        this.deductibleIgnoreInd = deductibleIgnoreInd;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getComponentCode() {
        return componentCode;
    }

    public void setComponentCode(String componentCode) {
        this.componentCode = componentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getDeductibleIgnoreInd() {
        return deductibleIgnoreInd;
    }

    public void setDeductibleIgnoreInd(boolean deductibleIgnoreInd) {
        this.deductibleIgnoreInd = deductibleIgnoreInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimMiscPaymentDetail> getClaimMiscPaymentDetailCollection() {
        return claimMiscPaymentDetailCollection;
    }

    public void setClaimMiscPaymentDetailCollection(Collection<ClaimMiscPaymentDetail> claimMiscPaymentDetailCollection) {
        this.claimMiscPaymentDetailCollection = claimMiscPaymentDetailCollection;
    }

    @XmlTransient
    public Collection<PPMProductService> getPPMProductServiceCollection() {
        return pPMProductServiceCollection;
    }

    public void setPPMProductServiceCollection(Collection<PPMProductService> pPMProductServiceCollection) {
        this.pPMProductServiceCollection = pPMProductServiceCollection;
    }

    public CfComponentGroup getComponentGroupIdFk() {
        return componentGroupIdFk;
    }

    public void setComponentGroupIdFk(CfComponentGroup componentGroupIdFk) {
        this.componentGroupIdFk = componentGroupIdFk;
    }

    @XmlTransient
    public Collection<ContractPPMDetailComponent> getContractPPMDetailComponentCollection() {
        return contractPPMDetailComponentCollection;
    }

    public void setContractPPMDetailComponentCollection(Collection<ContractPPMDetailComponent> contractPPMDetailComponentCollection) {
        this.contractPPMDetailComponentCollection = contractPPMDetailComponentCollection;
    }

    @XmlTransient
    public Collection<CfCoverageGroupComponentConn> getCfCoverageGroupComponentConnCollection() {
        return cfCoverageGroupComponentConnCollection;
    }

    public void setCfCoverageGroupComponentConnCollection(Collection<CfCoverageGroupComponentConn> cfCoverageGroupComponentConnCollection) {
        this.cfCoverageGroupComponentConnCollection = cfCoverageGroupComponentConnCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (componentId != null ? componentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfComponent)) {
            return false;
        }
        CfComponent other = (CfComponent) object;
        if ((this.componentId == null && other.componentId != null) || (this.componentId != null && !this.componentId.equals(other.componentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfComponent[ componentId=" + componentId + " ]";
    }
    
}
