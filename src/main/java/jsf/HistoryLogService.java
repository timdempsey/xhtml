/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Dealer;
import entity.DealerGroup;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "hlService")
@ApplicationScoped
public class HistoryLogService {
    
    private static final Logger LOGGER = LogManager.getLogger(NoteService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of HistoryLogService
     */
    public HistoryLogService() {
    }
    
    List<HistoryLogs> hlList;
    
    private Dealer dealer;
    private DealerGroup dealerGroup;

    public List<HistoryLogs> getHlList() {
        return hlList;
    }

    public void setHlList(List<HistoryLogs> hlList) {
        this.hlList = hlList;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public DealerGroup getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(DealerGroup dealerGroup) {
        this.dealerGroup = dealerGroup;
    }
    
     public void reset() {
        dealerGroup = null;
        dealer = null;
    }
    
    @PostConstruct
    public void init() {
        dealerGroup = null;
        dealer = null;
    }
    
    /*
    * methods
    */
    
    public void generateHistoryLogsListByDealer(Dealer dealer) {
        LOGGER.info("in generateHistoryLogsListByDealer, dealerId=" + dealer.getDealerId());
        hlList = claimMisc.getHistoryLogsByDealerId(dealer.getDealerId());
    }
    
    public void generateHistoryLogsListByDG(DealerGroup dg) {
        LOGGER.info("in generateHistoryLogsListByDG, id=" + dg.getDealerGroupId());
        hlList = claimMisc.getHistoryLogsByDGId(dg.getDealerGroupId());
    }
    
}
