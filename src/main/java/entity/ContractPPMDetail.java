/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractPPMDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractPPMDetail.findAll", query = "SELECT c FROM ContractPPMDetail c")
    , @NamedQuery(name = "ContractPPMDetail.findByContractPPMDetailId", query = "SELECT c FROM ContractPPMDetail c WHERE c.contractPPMDetailId = :contractPPMDetailId")
    , @NamedQuery(name = "ContractPPMDetail.findByDescription", query = "SELECT c FROM ContractPPMDetail c WHERE c.description = :description")
    , @NamedQuery(name = "ContractPPMDetail.findByUpperMile", query = "SELECT c FROM ContractPPMDetail c WHERE c.upperMile = :upperMile")
    , @NamedQuery(name = "ContractPPMDetail.findByLowerMile", query = "SELECT c FROM ContractPPMDetail c WHERE c.lowerMile = :lowerMile")
    , @NamedQuery(name = "ContractPPMDetail.findByUpperDate", query = "SELECT c FROM ContractPPMDetail c WHERE c.upperDate = :upperDate")
    , @NamedQuery(name = "ContractPPMDetail.findByLowerDate", query = "SELECT c FROM ContractPPMDetail c WHERE c.lowerDate = :lowerDate")
    , @NamedQuery(name = "ContractPPMDetail.findByCost", query = "SELECT c FROM ContractPPMDetail c WHERE c.cost = :cost")
    , @NamedQuery(name = "ContractPPMDetail.findByServiceGroup", query = "SELECT c FROM ContractPPMDetail c WHERE c.serviceGroup = :serviceGroup")
    , @NamedQuery(name = "ContractPPMDetail.findByServiceMinIntervalDay", query = "SELECT c FROM ContractPPMDetail c WHERE c.serviceMinIntervalDay = :serviceMinIntervalDay")
    , @NamedQuery(name = "ContractPPMDetail.findByUpdateUserName", query = "SELECT c FROM ContractPPMDetail c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractPPMDetail.findByUpdateLast", query = "SELECT c FROM ContractPPMDetail c WHERE c.updateLast = :updateLast")})
public class ContractPPMDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractPPMDetailId")
    private Integer contractPPMDetailId;
    @Size(max = 300)
    @Column(name = "description")
    private String description;
    @Column(name = "upperMile")
    private Integer upperMile;
    @Column(name = "lowerMile")
    private Integer lowerMile;
    @Column(name = "upperDate")
    @Temporal(TemporalType.DATE)
    private Date upperDate;
    @Column(name = "lowerDate")
    @Temporal(TemporalType.DATE)
    private Date lowerDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "cost")
    private BigDecimal cost;
    @Size(max = 100)
    @Column(name = "serviceGroup")
    private String serviceGroup;
    @Column(name = "serviceMinIntervalDay")
    private Integer serviceMinIntervalDay;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contractPPMDetailIdFk")
    private Collection<ClaimEventPPM> claimEventPPMCollection;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @OneToMany(mappedBy = "contractPPMDetailIdFk")
    private Collection<ContractPPMDetailComponent> contractPPMDetailComponentCollection;

    public ContractPPMDetail() {
    }

    public ContractPPMDetail(Integer contractPPMDetailId) {
        this.contractPPMDetailId = contractPPMDetailId;
    }

    public ContractPPMDetail(Integer contractPPMDetailId, BigDecimal cost) {
        this.contractPPMDetailId = contractPPMDetailId;
        this.cost = cost;
    }

    public Integer getContractPPMDetailId() {
        return contractPPMDetailId;
    }

    public void setContractPPMDetailId(Integer contractPPMDetailId) {
        this.contractPPMDetailId = contractPPMDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUpperMile() {
        return upperMile;
    }

    public void setUpperMile(Integer upperMile) {
        this.upperMile = upperMile;
    }

    public Integer getLowerMile() {
        return lowerMile;
    }

    public void setLowerMile(Integer lowerMile) {
        this.lowerMile = lowerMile;
    }

    public Date getUpperDate() {
        return upperDate;
    }

    public void setUpperDate(Date upperDate) {
        this.upperDate = upperDate;
    }

    public Date getLowerDate() {
        return lowerDate;
    }

    public void setLowerDate(Date lowerDate) {
        this.lowerDate = lowerDate;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public Integer getServiceMinIntervalDay() {
        return serviceMinIntervalDay;
    }

    public void setServiceMinIntervalDay(Integer serviceMinIntervalDay) {
        this.serviceMinIntervalDay = serviceMinIntervalDay;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ClaimEventPPM> getClaimEventPPMCollection() {
        return claimEventPPMCollection;
    }

    public void setClaimEventPPMCollection(Collection<ClaimEventPPM> claimEventPPMCollection) {
        this.claimEventPPMCollection = claimEventPPMCollection;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    @XmlTransient
    public Collection<ContractPPMDetailComponent> getContractPPMDetailComponentCollection() {
        return contractPPMDetailComponentCollection;
    }

    public void setContractPPMDetailComponentCollection(Collection<ContractPPMDetailComponent> contractPPMDetailComponentCollection) {
        this.contractPPMDetailComponentCollection = contractPPMDetailComponentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractPPMDetailId != null ? contractPPMDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractPPMDetail)) {
            return false;
        }
        ContractPPMDetail other = (ContractPPMDetail) object;
        if ((this.contractPPMDetailId == null && other.contractPPMDetailId != null) || (this.contractPPMDetailId != null && !this.contractPPMDetailId.equals(other.contractPPMDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractPPMDetail[ contractPPMDetailId=" + contractPPMDetailId + " ]";
    }
    
}
