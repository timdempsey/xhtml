/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class DealerPlan {

    private static final Logger LOGGER = LogManager.getLogger(DealerPlan.class);

    public DealerPlan(Integer dplanId, String dplanName, List<String> classCodeList, List<Integer> termIdList) {
        this.dplanId = dplanId;
        this.dplanName = dplanName;
        this.classCodeList = classCodeList;
        this.termIdList = termIdList;
    }

    

   

    public DealerPlan() {
        LOGGER.info("default DealerPlan constructor");
        termIdList = new ArrayList<>();
        classCodeList = new ArrayList<>();
    }

    private Integer dplanId;
    private String dplanName;
    private List<String> classCodeList;
    private List<Integer> termIdList;

    public Integer getDplanId() {
        return dplanId;
    }

    public void setDplanId(Integer dplanId) {
        this.dplanId = dplanId;
    }

    public String getDplanName() {
        return dplanName;
    }

    public void setDplanName(String dplanName) {
        this.dplanName = dplanName;
    }

    public List<String> getClassCodeList() {
        return classCodeList;
    }

    public void setClassCodeList(List<String> classCodeList) {
        this.classCodeList = classCodeList;
    }

    public List<Integer> getTermIdList() {
        return termIdList;
    }

    public void setTermIdList(List<Integer> termIdList) {
        this.termIdList = termIdList;
    }

   
    
    

    public void reset() {
        dplanId = null;
        dplanName = "";
        for (String classCode : classCodeList) {
            classCode = null;
        }
        classCodeList = new ArrayList<>();
        for (Integer termId : termIdList) {
            termId = null;
        }
        termIdList = new ArrayList<>();
    }

}
