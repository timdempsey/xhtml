/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractReplacement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractReplacement.findAll", query = "SELECT c FROM ContractReplacement c")
    , @NamedQuery(name = "ContractReplacement.findByContractReplacementId", query = "SELECT c FROM ContractReplacement c WHERE c.contractReplacementId = :contractReplacementId")
    , @NamedQuery(name = "ContractReplacement.findByReplacedDate", query = "SELECT c FROM ContractReplacement c WHERE c.replacedDate = :replacedDate")
    , @NamedQuery(name = "ContractReplacement.findByUpdateUserName", query = "SELECT c FROM ContractReplacement c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractReplacement.findByUpdateLast", query = "SELECT c FROM ContractReplacement c WHERE c.updateLast = :updateLast")})
public class ContractReplacement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractReplacementId")
    private Integer contractReplacementId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "replacedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date replacedDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "newContractIdFk", referencedColumnName = "contractId")
    @OneToOne(optional = false)
    private Contracts newContractIdFk;
    @JoinColumn(name = "oldContractIdFk", referencedColumnName = "contractId")
    @OneToOne(optional = false)
    private Contracts oldContractIdFk;
    @JoinColumn(name = "replacedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember replacedByIdFk;

    public ContractReplacement() {
    }

    public ContractReplacement(Integer contractReplacementId) {
        this.contractReplacementId = contractReplacementId;
    }

    public ContractReplacement(Integer contractReplacementId, Date replacedDate) {
        this.contractReplacementId = contractReplacementId;
        this.replacedDate = replacedDate;
    }

    public Integer getContractReplacementId() {
        return contractReplacementId;
    }

    public void setContractReplacementId(Integer contractReplacementId) {
        this.contractReplacementId = contractReplacementId;
    }

    public Date getReplacedDate() {
        return replacedDate;
    }

    public void setReplacedDate(Date replacedDate) {
        this.replacedDate = replacedDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Contracts getNewContractIdFk() {
        return newContractIdFk;
    }

    public void setNewContractIdFk(Contracts newContractIdFk) {
        this.newContractIdFk = newContractIdFk;
    }

    public Contracts getOldContractIdFk() {
        return oldContractIdFk;
    }

    public void setOldContractIdFk(Contracts oldContractIdFk) {
        this.oldContractIdFk = oldContractIdFk;
    }

    public UserMember getReplacedByIdFk() {
        return replacedByIdFk;
    }

    public void setReplacedByIdFk(UserMember replacedByIdFk) {
        this.replacedByIdFk = replacedByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractReplacementId != null ? contractReplacementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractReplacement)) {
            return false;
        }
        ContractReplacement other = (ContractReplacement) object;
        if ((this.contractReplacementId == null && other.contractReplacementId != null) || (this.contractReplacementId != null && !this.contractReplacementId.equals(other.contractReplacementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractReplacement[ contractReplacementId=" + contractReplacementId + " ]";
    }
    
}
