/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Limit;
import entity.PlanTable;
import entity.Product;
import entity.Program;
import entity.RateBasedRule;
import entity.Term;
import entity.VINClassCode;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Jiepi
 */
@Named(value = "rvTreeService")
@ApplicationScoped
public class RVTreeService implements Serializable {

    private static final Logger LOGGER = LogManager.getLogger(RVTreeService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of RVTreeService
     */
    public RVTreeService() {
    }

    private TreeNode root;
    private TreeNode[] selectedNodes;

    private Integer rvId;

    public Integer getRvId() {
        return rvId;
    }

    public void setRvId(Integer rvId) {
        this.rvId = rvId;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("rvTreeService init");
        reset();
    }

    public TreeNode createCheckboxRVList() {
        //TreeNode root = new CheckboxTreeNode("root node", null);

        List<Program> programList;
        List<Product> prodList;
        List<PlanTable> planList;
        List<Term> termList;
        List<Limit> limitList;
        List<VINClassCode> codeList;

        programList = claimMisc.getProgramByAll();
        String type;
        Integer nodeCount =0;
        for (Program prog : programList) {

            String progType = prog.getProgramId().toString();
            type = progType + "|||||";
            //TreeNode programs = new CheckboxTreeNode(new RVTree(prog.getName(), prog.getProgramId(), progType), root);
            TreeNode programs = new CheckboxTreeNode(new RVTree(prog.getName(), prog.getProgramId(), type), root);
            ++nodeCount;

            prodList = claimMisc.getProductListByProgId(prog.getProgramId());
            for (Product prod : prodList) {
                //String prodType = progType  + prod.getProductId().toString();
                String prodType = prod.getProductId().toString();
                type = progType + "|" + prodType + "||||";
                //TreeNode products = new CheckboxTreeNode(new RVTree(prod.getProductName(), prod.getProductId(), prodType), programs);
                TreeNode products = new CheckboxTreeNode(new RVTree(prod.getProductName(), prod.getProductId(), type), programs);
                ++nodeCount;

                planList = claimMisc.getPlanByProdId(prod.getProductId());
                for (PlanTable plan : planList) {
                    //String planType = prodType + plan.getPlanId().toString();
                    //TreeNode plans = new CheckboxTreeNode(new RVTree(plan.getPlanName(), plan.getPlanId(), planType), products);
                    String planType = plan.getPlanId().toString();
                    type = progType + "|" + prodType + "|" + planType + "|||";
                    TreeNode plans = new CheckboxTreeNode(new RVTree(plan.getPlanName(), plan.getPlanId(), type), products);
                    ++nodeCount;

                    //LOGGER.info("planId=" + plan.getPlanId());
                    termList = claimMisc.getTermListByPlanId(plan.getPlanId());
                    //LOGGER.info("size of termList=" + termList.size());
                    for (Term term : termList) {
                        //LOGGER.info("termId=" + term.getTermId());
                        //String termType = planType  + term.getTermId().toString();
                        //String termName = "Term " + claimMisc.getTermDetailByTermIdFk(term.getTermId()).getDescription();
                        String termType = term.getTermId().toString();
                        type = progType + "|" + prodType + "|" + planType + "|" + termType + "||";
                        String termName = "Term " + claimMisc.getTermDetailByTermIdFk(term.getTermId()).getDescription();

                        TreeNode terms = new CheckboxTreeNode(new RVTree(termName, term.getTermId(), type), plans);
                        ++nodeCount;

                        limitList = claimMisc.getLimitListByPlanId(plan.getPlanId());
                        for (Limit limit : limitList) {
                            //String limitType = termType  +  limit.getLimitId().toString();
                            //String limitName = claimMisc.getLimitDetailByLimitIdFk(limit.getLimitId()).getDescription();
                            String limitType = limit.getLimitId().toString();
                            String limitName = claimMisc.getLimitDetailByLimitIdFk(limit.getLimitId()).getDescription();
                            type = progType + "|" + prodType + "|" + planType + "|" + termType + "|" + limitType + "|";
                            TreeNode limits = new CheckboxTreeNode(new RVTree(limitName, limit.getLimitId(), type), terms);
                            ++nodeCount;

                            //limits.setSelected(true);
                            codeList = claimMisc.getVINClassCodeByAll();
                            for (VINClassCode code : codeList) {
                                //String codeType = limitType  + code.getVinCode();
                                String codeName = "Class Code " + code.getVinCode();
                                //TreeNode codes = new CheckboxTreeNode(new RVTree(codeName, code.getVinClassCodeId(), codeType), limits);
                                String codeType = code.getVinCode();
                                type = progType + "|" + prodType + "|" + planType + "|" + termType + "|" + limitType + "|" + codeType;
                                TreeNode codes = new CheckboxTreeNode(new RVTree(codeName, code.getVinClassCodeId(), type), limits);
                                ++nodeCount;
                                //codes.setSelected(true);
                            }
                        }
                    }
                }
            }

            LOGGER.info("end of each program: " + prog.getName() + ", nodeCount=" + nodeCount);
            nodeCount=0;
        }   // end of each program

        if (rvId != null) {
            LOGGER.info("editing existed RV, rvId = " + rvId);
            List<RateBasedRule> rbrList = claimMisc.getRateBasedRuleByFkId(rvId);
            LOGGER.info("rbrList=" + rbrList.size());
            for (RateBasedRule rbr : rbrList) {
                String stype = generateTypeField(rbr);
                root = setTreeNodeState(root, stype, true);
            }
        }

        LOGGER.info("end of createCheckboxRVList");
        return root;
    }

    public String generateTypeField(RateBasedRule rbr) {
        String progType = "";
        String prodType = "";
        String planType = "";
        String termType = "";
        String limitType = "";
        String classType = "";

        if (rbr.getProgramIdFk() != null) {
            progType = rbr.getProgramIdFk().getProgramId().toString();
        }
        if (rbr.getProductIdFk() != null) {
            prodType = rbr.getProductIdFk().getProductId().toString();
        }
        if (rbr.getPlanIdFk() != null) {
            planType = rbr.getPlanIdFk().getPlanId().toString();
        }
        if (rbr.getTermIdFk() != null) {
            termType = rbr.getTermIdFk().getTermId().toString();
        }
        if (rbr.getLimitIdFk() != null) {
            limitType = rbr.getLimitIdFk().getLimitId().toString();
        }
        if (rbr.getClassCode() != null) {
            classType = rbr.getClassCode();
        }

        //String type = progType  + prodType  + planType  + termType  + limitType + classType;
        String type = progType + "|" + prodType + "|" + planType + "|" + termType + "|" + limitType + "|" + classType;
        return type;
    }

    public TreeNode setTreeNodeState(TreeNode treeNode, String type, Boolean nodeState) {
        List<TreeNode> childNodes = treeNode.getChildren();
        for (TreeNode node : childNodes) {
            String nodeType = ((RVTree) node.getData()).getType();
            //LOGGER.info("nodeType=" + nodeType);
            //LOGGER.info("type=" + type);
            if (nodeType.equalsIgnoreCase(type)) {
                node.setSelected(nodeState);
                break;
            } else {
                setTreeNodeState(node, type, nodeState);
            }
        }
        return treeNode;
    }

    public Boolean isSelectedNode(String nodeType) {
        if (selectedNodes != null && selectedNodes.length > 0) {

            for (TreeNode node : selectedNodes) {
                //LOGGER.info("node is expanded=" + node.isExpanded());
                //LOGGER.info("node isLeaf=" + node.isLeaf());
                //LOGGER.info("node is partial=" + node.isPartialSelected());
                //LOGGER.info("node is selected=" + node.isSelected());
                String type = ((RVTree) node.getData()).getType();
                if (type.equalsIgnoreCase(nodeType)) {
                    LOGGER.info("matched: " + nodeType);
                    return true;
                }
            }
        }
        return false;
    }

    public TreeNode saveSelectedTreeNode(TreeNode treeNode) {
        List<TreeNode> childNodes = treeNode.getChildren();
        for (TreeNode node : childNodes) {
            String nodeType = ((RVTree) node.getData()).getType();
            //LOGGER.info("nodeType=" + nodeType);
            //LOGGER.info("type=" + type);
            if (isSelectedNode(nodeType)) {
                LOGGER.info("is selectedNode: " + nodeType);
                claimMisc.saveTreeNode(nodeType, rvId);
                //break;
            } else {
                saveSelectedTreeNode(node);
            }
        }
        return treeNode;
    }

    public void displaySelectedRV(TreeNode[] nodes) {
        LOGGER.info("in displaySelectedRV");
        if (nodes != null && nodes.length > 0) {
            StringBuilder builder = new StringBuilder();

            for (TreeNode node : nodes) {
                String str = ((RVTree) node.getData()).getId() + "|" + ((RVTree) node.getData()).getType();
                builder.append(str);
                builder.append("<br />");
            }

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", builder.toString());
            FacesContext.getCurrentInstance().addMessage(null, message);
            LOGGER.info("selectedNodes: \n" + builder.toString());
        }
    }

    public void save() {
        LOGGER.info("in save");
        if (selectedNodes != null && selectedNodes.length > 0) {
            // first to check whether it's updating or new one
            claimMisc.deleteRateBasedRule(rvId);
            root = saveSelectedTreeNode(root);
            RequestContext.getCurrentInstance().closeDialog("RVTree");
        }
    }

    public String getRVTree(ActionEvent event) {
        reset();
        LOGGER.info("getRVTree event");
        return getRVTree();
    }

    public String getRVTree() {
        LOGGER.info("in getRVTree");

        root = createCheckboxRVList();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1400");
        options.put("width", "1000");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("RVTree", options, null);

        LOGGER.info("start RVTree");

        return "RVTree";
    }

    public void rvTreeClosed(ActionEvent event) {
        LOGGER.info("rvTreeClosed");
    }

    public void onNodeExpand(NodeExpandEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Expanded", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Collapsed", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onNodeSelect(NodeSelectEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onNodeUnselect(NodeUnselectEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void deleteProductVisibility() {

    }

    public void getRVIdAttr(ActionEvent event) {
        rvId = (Integer) event.getComponent().getAttributes().get("rvId");
        LOGGER.info("in getRVIdAttr, rvId=" + rvId);
    }

    public void cancelRVtree() {
        LOGGER.info("in cancelRVtree");
        reset();
        RequestContext.getCurrentInstance().closeDialog("RVTree");
    }
    
    public void resetRVTree() {
        LOGGER.info("in resetRVTree");
        root.setSelected(false);
        //root = createCheckboxRVList();
    }

    public void reset() {
        root = new CheckboxTreeNode("root node", null);
        rvId = null;
    }

}
