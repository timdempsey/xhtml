/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Adjustment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adjustment.findAll", query = "SELECT a FROM Adjustment a")
    , @NamedQuery(name = "Adjustment.findByAdjustmentId", query = "SELECT a FROM Adjustment a WHERE a.adjustmentId = :adjustmentId")
    , @NamedQuery(name = "Adjustment.findByInternalName", query = "SELECT a FROM Adjustment a WHERE a.internalName = :internalName")
    , @NamedQuery(name = "Adjustment.findByDescription", query = "SELECT a FROM Adjustment a WHERE a.description = :description")
    , @NamedQuery(name = "Adjustment.findByUpdateUserName", query = "SELECT a FROM Adjustment a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "Adjustment.findByUpdateLast", query = "SELECT a FROM Adjustment a WHERE a.updateLast = :updateLast")})
public class Adjustment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "adjustmentId")
    private Integer adjustmentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "internalName")
    private String internalName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "relatedAdjustmentIdFk")
    private Collection<ContractCoverageGroup> contractCoverageGroupCollection;
    @OneToMany(mappedBy = "adjustmentIdFk")
    private Collection<ContractAdjustment> contractAdjustmentCollection;
    @OneToMany(mappedBy = "relatedAdjustmentIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;
    @OneToMany(mappedBy = "adjustmentIdFk")
    private Collection<CfContractEntryField> cfContractEntryFieldCollection;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adjustmentIdFk")
    private Collection<AdjustmentDetail> adjustmentDetailCollection;

    public Adjustment() {
    }

    public Adjustment(Integer adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public Adjustment(Integer adjustmentId, String internalName, String description) {
        this.adjustmentId = adjustmentId;
        this.internalName = internalName;
        this.description = description;
    }

    public Integer getAdjustmentId() {
        return adjustmentId;
    }

    public void setAdjustmentId(Integer adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractCoverageGroup> getContractCoverageGroupCollection() {
        return contractCoverageGroupCollection;
    }

    public void setContractCoverageGroupCollection(Collection<ContractCoverageGroup> contractCoverageGroupCollection) {
        this.contractCoverageGroupCollection = contractCoverageGroupCollection;
    }

    @XmlTransient
    public Collection<ContractAdjustment> getContractAdjustmentCollection() {
        return contractAdjustmentCollection;
    }

    public void setContractAdjustmentCollection(Collection<ContractAdjustment> contractAdjustmentCollection) {
        this.contractAdjustmentCollection = contractAdjustmentCollection;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @XmlTransient
    public Collection<CfContractEntryField> getCfContractEntryFieldCollection() {
        return cfContractEntryFieldCollection;
    }

    public void setCfContractEntryFieldCollection(Collection<CfContractEntryField> cfContractEntryFieldCollection) {
        this.cfContractEntryFieldCollection = cfContractEntryFieldCollection;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<AdjustmentDetail> getAdjustmentDetailCollection() {
        return adjustmentDetailCollection;
    }

    public void setAdjustmentDetailCollection(Collection<AdjustmentDetail> adjustmentDetailCollection) {
        this.adjustmentDetailCollection = adjustmentDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adjustmentId != null ? adjustmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adjustment)) {
            return false;
        }
        Adjustment other = (Adjustment) object;
        if ((this.adjustmentId == null && other.adjustmentId != null) || (this.adjustmentId != null && !this.adjustmentId.equals(other.adjustmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Adjustment[ adjustmentId=" + adjustmentId + " ]";
    }
    
}
