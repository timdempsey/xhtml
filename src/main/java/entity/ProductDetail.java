/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ProductDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductDetail.findAll", query = "SELECT p FROM ProductDetail p")
    , @NamedQuery(name = "ProductDetail.findByProductDetailId", query = "SELECT p FROM ProductDetail p WHERE p.productDetailId = :productDetailId")
    , @NamedQuery(name = "ProductDetail.findByEffectiveDate", query = "SELECT p FROM ProductDetail p WHERE p.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "ProductDetail.findByEnteredDate", query = "SELECT p FROM ProductDetail p WHERE p.enteredDate = :enteredDate")
    , @NamedQuery(name = "ProductDetail.findByDeletedInd", query = "SELECT p FROM ProductDetail p WHERE p.deletedInd = :deletedInd")
    , @NamedQuery(name = "ProductDetail.findByDefaultClaimPayeeInd", query = "SELECT p FROM ProductDetail p WHERE p.defaultClaimPayeeInd = :defaultClaimPayeeInd")
    , @NamedQuery(name = "ProductDetail.findByDescription", query = "SELECT p FROM ProductDetail p WHERE p.description = :description")
    , @NamedQuery(name = "ProductDetail.findByUpdateUserName", query = "SELECT p FROM ProductDetail p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "ProductDetail.findByUpdateLast", query = "SELECT p FROM ProductDetail p WHERE p.updateLast = :updateLast")})
public class ProductDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "productDetailId")
    private Integer productDetailId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Column(name = "defaultClaimPayeeInd")
    private Integer defaultClaimPayeeInd;
    @Size(max = 300)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "claimFormIdFk", referencedColumnName = "formId")
    @ManyToOne
    private CfForm claimFormIdFk;
    @JoinColumn(name = "formIdFk", referencedColumnName = "formId")
    @ManyToOne
    private CfForm formIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @JoinColumn(name = "productSalesGroupIdFk", referencedColumnName = "productSalesGroupId")
    @ManyToOne
    private ProductSalesGroup productSalesGroupIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public ProductDetail() {
    }

    public ProductDetail(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public ProductDetail(Integer productDetailId, Date effectiveDate, Date enteredDate, boolean deletedInd) {
        this.productDetailId = productDetailId;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.deletedInd = deletedInd;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Integer getDefaultClaimPayeeInd() {
        return defaultClaimPayeeInd;
    }

    public void setDefaultClaimPayeeInd(Integer defaultClaimPayeeInd) {
        this.defaultClaimPayeeInd = defaultClaimPayeeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfForm getClaimFormIdFk() {
        return claimFormIdFk;
    }

    public void setClaimFormIdFk(CfForm claimFormIdFk) {
        this.claimFormIdFk = claimFormIdFk;
    }

    public CfForm getFormIdFk() {
        return formIdFk;
    }

    public void setFormIdFk(CfForm formIdFk) {
        this.formIdFk = formIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    public ProductSalesGroup getProductSalesGroupIdFk() {
        return productSalesGroupIdFk;
    }

    public void setProductSalesGroupIdFk(ProductSalesGroup productSalesGroupIdFk) {
        this.productSalesGroupIdFk = productSalesGroupIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productDetailId != null ? productDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductDetail)) {
            return false;
        }
        ProductDetail other = (ProductDetail) object;
        if ((this.productDetailId == null && other.productDetailId != null) || (this.productDetailId != null && !this.productDetailId.equals(other.productDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProductDetail[ productDetailId=" + productDetailId + " ]";
    }
    
}
