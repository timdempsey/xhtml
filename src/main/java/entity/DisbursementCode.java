/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DisbursementCode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisbursementCode.findAll", query = "SELECT d FROM DisbursementCode d")
    , @NamedQuery(name = "DisbursementCode.findByDisbursementCodeId", query = "SELECT d FROM DisbursementCode d WHERE d.disbursementCodeId = :disbursementCodeId")
    , @NamedQuery(name = "DisbursementCode.findByName", query = "SELECT d FROM DisbursementCode d WHERE d.name = :name")
    , @NamedQuery(name = "DisbursementCode.findByDescription", query = "SELECT d FROM DisbursementCode d WHERE d.description = :description")
    , @NamedQuery(name = "DisbursementCode.findByDisbursementTypeInd", query = "SELECT d FROM DisbursementCode d WHERE d.disbursementTypeInd = :disbursementTypeInd")
    , @NamedQuery(name = "DisbursementCode.findByDisbursementRefundTypeInd", query = "SELECT d FROM DisbursementCode d WHERE d.disbursementRefundTypeInd = :disbursementRefundTypeInd")
    , @NamedQuery(name = "DisbursementCode.findByRenewableInd", query = "SELECT d FROM DisbursementCode d WHERE d.renewableInd = :renewableInd")
    , @NamedQuery(name = "DisbursementCode.findByCededReserveInd", query = "SELECT d FROM DisbursementCode d WHERE d.cededReserveInd = :cededReserveInd")
    , @NamedQuery(name = "DisbursementCode.findByUpdateUserName", query = "SELECT d FROM DisbursementCode d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DisbursementCode.findByUpdateLast", query = "SELECT d FROM DisbursementCode d WHERE d.updateLast = :updateLast")})
public class DisbursementCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementCodeId")
    private Integer disbursementCodeId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @Column(name = "disbursementTypeInd")
    private Integer disbursementTypeInd;
    @Column(name = "disbursementRefundTypeInd")
    private Integer disbursementRefundTypeInd;
    @Column(name = "renewableInd")
    private Boolean renewableInd;
    @Column(name = "cededReserveInd")
    private Boolean cededReserveInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "disbursementGroupIdFk", referencedColumnName = "disbursementGroupId")
    @ManyToOne
    private DisbursementGroup disbursementGroupIdFk;
    @OneToMany(mappedBy = "disbursementCodeIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection;
    @OneToMany(mappedBy = "calculatedOnDisbursementCodeIdFk")
    private Collection<DisbursementDetail> disbursementDetailCollection1;

    public DisbursementCode() {
    }

    public DisbursementCode(Integer disbursementCodeId) {
        this.disbursementCodeId = disbursementCodeId;
    }

    public Integer getDisbursementCodeId() {
        return disbursementCodeId;
    }

    public void setDisbursementCodeId(Integer disbursementCodeId) {
        this.disbursementCodeId = disbursementCodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDisbursementTypeInd() {
        return disbursementTypeInd;
    }

    public void setDisbursementTypeInd(Integer disbursementTypeInd) {
        this.disbursementTypeInd = disbursementTypeInd;
    }

    public Integer getDisbursementRefundTypeInd() {
        return disbursementRefundTypeInd;
    }

    public void setDisbursementRefundTypeInd(Integer disbursementRefundTypeInd) {
        this.disbursementRefundTypeInd = disbursementRefundTypeInd;
    }

    public Boolean getRenewableInd() {
        return renewableInd;
    }

    public void setRenewableInd(Boolean renewableInd) {
        this.renewableInd = renewableInd;
    }

    public Boolean getCededReserveInd() {
        return cededReserveInd;
    }

    public void setCededReserveInd(Boolean cededReserveInd) {
        this.cededReserveInd = cededReserveInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public DisbursementGroup getDisbursementGroupIdFk() {
        return disbursementGroupIdFk;
    }

    public void setDisbursementGroupIdFk(DisbursementGroup disbursementGroupIdFk) {
        this.disbursementGroupIdFk = disbursementGroupIdFk;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection() {
        return disbursementDetailCollection;
    }

    public void setDisbursementDetailCollection(Collection<DisbursementDetail> disbursementDetailCollection) {
        this.disbursementDetailCollection = disbursementDetailCollection;
    }

    @XmlTransient
    public Collection<DisbursementDetail> getDisbursementDetailCollection1() {
        return disbursementDetailCollection1;
    }

    public void setDisbursementDetailCollection1(Collection<DisbursementDetail> disbursementDetailCollection1) {
        this.disbursementDetailCollection1 = disbursementDetailCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementCodeId != null ? disbursementCodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisbursementCode)) {
            return false;
        }
        DisbursementCode other = (DisbursementCode) object;
        if ((this.disbursementCodeId == null && other.disbursementCodeId != null) || (this.disbursementCodeId != null && !this.disbursementCodeId.equals(other.disbursementCodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DisbursementCode[ disbursementCodeId=" + disbursementCodeId + " ]";
    }
    
}
