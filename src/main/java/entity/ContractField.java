/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractField")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractField.findAll", query = "SELECT c FROM ContractField c")
    , @NamedQuery(name = "ContractField.findByContractFieldId", query = "SELECT c FROM ContractField c WHERE c.contractFieldId = :contractFieldId")
    , @NamedQuery(name = "ContractField.findByNumberValue", query = "SELECT c FROM ContractField c WHERE c.numberValue = :numberValue")
    , @NamedQuery(name = "ContractField.findByStringValue", query = "SELECT c FROM ContractField c WHERE c.stringValue = :stringValue")
    , @NamedQuery(name = "ContractField.findByDateValue", query = "SELECT c FROM ContractField c WHERE c.dateValue = :dateValue")
    , @NamedQuery(name = "ContractField.findByUpdateUserName", query = "SELECT c FROM ContractField c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractField.findByUpdateLast", query = "SELECT c FROM ContractField c WHERE c.updateLast = :updateLast")})
public class ContractField implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractFieldId")
    private Integer contractFieldId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "numberValue")
    private BigDecimal numberValue;
    @Size(max = 200)
    @Column(name = "stringValue")
    private String stringValue;
    @Column(name = "dateValue")
    @Temporal(TemporalType.DATE)
    private Date dateValue;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "fieldIdFk", referencedColumnName = "FieldId")
    @ManyToOne(optional = false)
    private CfField fieldIdFk;
    @JoinColumn(name = "regionIdFk", referencedColumnName = "regionId")
    @ManyToOne
    private CfRegion regionIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne(optional = false)
    private Contracts contractIdFk;

    public ContractField() {
    }

    public ContractField(Integer contractFieldId) {
        this.contractFieldId = contractFieldId;
    }

    public Integer getContractFieldId() {
        return contractFieldId;
    }

    public void setContractFieldId(Integer contractFieldId) {
        this.contractFieldId = contractFieldId;
    }

    public BigDecimal getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(BigDecimal numberValue) {
        this.numberValue = numberValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfField getFieldIdFk() {
        return fieldIdFk;
    }

    public void setFieldIdFk(CfField fieldIdFk) {
        this.fieldIdFk = fieldIdFk;
    }

    public CfRegion getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(CfRegion regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractFieldId != null ? contractFieldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractField)) {
            return false;
        }
        ContractField other = (ContractField) object;
        if ((this.contractFieldId == null && other.contractFieldId != null) || (this.contractFieldId != null && !this.contractFieldId.equals(other.contractFieldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractField[ contractFieldId=" + contractFieldId + " ]";
    }
    
}
