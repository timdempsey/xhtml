/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfRegion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfRegion.findAll", query = "SELECT c FROM CfRegion c")
    , @NamedQuery(name = "CfRegion.findByRegionId", query = "SELECT c FROM CfRegion c WHERE c.regionId = :regionId")
    , @NamedQuery(name = "CfRegion.findByRegionCode", query = "SELECT c FROM CfRegion c WHERE c.regionCode = :regionCode")
    , @NamedQuery(name = "CfRegion.findByRegionName", query = "SELECT c FROM CfRegion c WHERE c.regionName = :regionName")})
public class CfRegion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "regionId")
    private Integer regionId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "regionCode")
    private String regionCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "regionName")
    private String regionName;
    @JoinColumn(name = "countryIdFk", referencedColumnName = "countryId")
    @ManyToOne(optional = false)
    private CfCountry countryIdFk;
    @OneToMany(mappedBy = "regionIdFk")
    private Collection<AccountKeeperField> accountKeeperFieldCollection;
    @OneToMany(mappedBy = "regionIdFk")
    private Collection<Address> addressCollection;
    @OneToMany(mappedBy = "regionIdFk")
    private Collection<ContractField> contractFieldCollection;
    @OneToMany(mappedBy = "regionIdFk")
    private Collection<ReinsuranceRegionTax> reinsuranceRegionTaxCollection;

    public CfRegion() {
    }

    public CfRegion(Integer regionId) {
        this.regionId = regionId;
    }

    public CfRegion(Integer regionId, String regionCode, String regionName) {
        this.regionId = regionId;
        this.regionCode = regionCode;
        this.regionName = regionName;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public CfCountry getCountryIdFk() {
        return countryIdFk;
    }

    public void setCountryIdFk(CfCountry countryIdFk) {
        this.countryIdFk = countryIdFk;
    }

    @XmlTransient
    public Collection<AccountKeeperField> getAccountKeeperFieldCollection() {
        return accountKeeperFieldCollection;
    }

    public void setAccountKeeperFieldCollection(Collection<AccountKeeperField> accountKeeperFieldCollection) {
        this.accountKeeperFieldCollection = accountKeeperFieldCollection;
    }

    @XmlTransient
    public Collection<Address> getAddressCollection() {
        return addressCollection;
    }

    public void setAddressCollection(Collection<Address> addressCollection) {
        this.addressCollection = addressCollection;
    }

    @XmlTransient
    public Collection<ContractField> getContractFieldCollection() {
        return contractFieldCollection;
    }

    public void setContractFieldCollection(Collection<ContractField> contractFieldCollection) {
        this.contractFieldCollection = contractFieldCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceRegionTax> getReinsuranceRegionTaxCollection() {
        return reinsuranceRegionTaxCollection;
    }

    public void setReinsuranceRegionTaxCollection(Collection<ReinsuranceRegionTax> reinsuranceRegionTaxCollection) {
        this.reinsuranceRegionTaxCollection = reinsuranceRegionTaxCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regionId != null ? regionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfRegion)) {
            return false;
        }
        CfRegion other = (CfRegion) object;
        if ((this.regionId == null && other.regionId != null) || (this.regionId != null && !this.regionId.equals(other.regionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfRegion[ regionId=" + regionId + " ]";
    }
    
}
