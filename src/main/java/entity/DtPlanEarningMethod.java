/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanEarningMethod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanEarningMethod.findAll", query = "SELECT d FROM DtPlanEarningMethod d")
    , @NamedQuery(name = "DtPlanEarningMethod.findByEarningMethodId", query = "SELECT d FROM DtPlanEarningMethod d WHERE d.earningMethodId = :earningMethodId")
    , @NamedQuery(name = "DtPlanEarningMethod.findByDescription", query = "SELECT d FROM DtPlanEarningMethod d WHERE d.description = :description")})
public class DtPlanEarningMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "earningMethodId")
    private Integer earningMethodId;
    @Size(max = 30)
    @Column(name = "description")
    private String description;

    public DtPlanEarningMethod() {
    }

    public DtPlanEarningMethod(Integer earningMethodId) {
        this.earningMethodId = earningMethodId;
    }

    public Integer getEarningMethodId() {
        return earningMethodId;
    }

    public void setEarningMethodId(Integer earningMethodId) {
        this.earningMethodId = earningMethodId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (earningMethodId != null ? earningMethodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanEarningMethod)) {
            return false;
        }
        DtPlanEarningMethod other = (DtPlanEarningMethod) object;
        if ((this.earningMethodId == null && other.earningMethodId != null) || (this.earningMethodId != null && !this.earningMethodId.equals(other.earningMethodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanEarningMethod[ earningMethodId=" + earningMethodId + " ]";
    }
    
}
