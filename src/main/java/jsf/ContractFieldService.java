/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.List;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractFieldService")
@ApplicationScoped
public class ContractFieldService {
    
    private static final Logger LOGGER = LogManager.getLogger(ContractFieldService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ContractFieldService
     */
    public ContractFieldService() {
    }
    
    List<ContractField> cfList;

    public List<ContractField> getCfList() {
        return cfList;
    }

    public void setCfList(List<ContractField> cfList) {
        this.cfList = cfList;
    }
    
    public void createContractFields(String cno) {
        LOGGER.info("in createContractFields, cno=" + cno);
        cfList = claimMisc.getContractFieldByContractNo(cno);
    }
    
}
