/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.ProductType;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimFirstLineView")
@ApplicationScoped
public class ClaimFirstLineView implements Serializable {

    /**
     * Creates a new instance of ClaimFirstLineView
     */
    public ClaimFirstLineView() {

    }

    //private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLineView.class.getName());
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLineView.class);

    @PersistenceContext // JSF m
    private EntityManager em;

    /*
    * bean property
     */
    String txt1;
    String contractNo;
    String claimNumber;
    ClaimFirstLine selectedClaim;

    EMTestBean emTestBean;

    public EMTestBean getEmTestBean() {
        return emTestBean;
    }

    public void setEmTestBean(EMTestBean emTestBean) {
        this.emTestBean = emTestBean;
    }

    
    private ClaimSearch cs;
    private List<String> claimTypes;

    public ClaimSearch getCs() {
        return cs;
    }

    public void setCs(ClaimSearch cs) {
        this.cs = cs;
    }

    public List<String> getClaimTypes() {
        return claimTypes;
    }

    public void setClaimTypes(List<String> claimTypes) {
        this.claimTypes = claimTypes;
    }

    
    public List<String> completeText(String query) {
        List<String> results = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            results.add(query + i);
        }

        return results;
    }

    public List<String> completeVin(String vin) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select vinFull from Contracts where vinFull like ?1");
        results = (List<String>) query.setParameter(1, vin + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }

    public List<String> completeAssignedTo(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select userName from UserMember where userName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }

    public List<String> completeDealerName(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select a.accountKeeperName from AccountKeeper a, Dealer b where a.accountKeeperId=b.dealerId and a.accountKeeperName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }

    public List<String> completeClaimNumber(String claimNo) {
        List<String> results = new ArrayList<>();

        LOGGER.info("select c.claimNumber from Claim c where c.claimNumber like ...");
        //TypedQuery<Claim> query = em.createQuery("select c from Claim c where c.claimNumber like :no", Claim.class);
        Query query = em.createNativeQuery("select claimNumber from Claim a where a.claimNumber like ?1");
        results = (List<String>) query.setParameter(1, claimNo + "%").getResultList();
        //LOGGER.info("cl.size=" + cl.size());

        LOGGER.info("results.size====" + results.size());
        return results;
    }

    public String getTxt1() {
        return txt1;
    }

    public void setTxt1(String txt1) {
        this.txt1 = txt1;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public ClaimFirstLine getSelectedClaim() {
        //LOGGER.info("getter getSelectedClaim.getClaimNumber=");
        return selectedClaim;
    }

    public void setSelectedClaim(ClaimFirstLine selectedClaim) {

        this.selectedClaim = selectedClaim;
    }

    public List<String> getClaimNumbers() {
        return claimNumbers;
    }

    public void setClaimNumbers(List<String> claimNumbers) {
        this.claimNumbers = claimNumbers;
    }

    /*jxwww
    private List<ClaimFirstLine> claims;
     */
    private LazyDataModel<ClaimFirstLine> claims;
    private List<String> claimNumbers;

    @Inject
    private ClaimFirstLineService service;

    @Inject
    private ClaimMisc claimMisc;

    /* jxwww
    public List<ClaimFirstLine> getClaims() {
        return claims;
    }
     */
    public LazyDataModel<ClaimFirstLine> getClaims() {
        return claims;
    }

    public void setClaims(LazyDataModel<ClaimFirstLine> claims) {
        this.claims = claims;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("init from ClaimFirstLineView");
        /* jxwww
        claims = new ArrayList<>();
         */
        // claims = new Lazy

        selectedClaim = new ClaimFirstLine();
        cs = new ClaimSearch();
        List<ProductType> ptList = claimMisc.getProductTypeByAll();
        claimTypes = new ArrayList<>();
        for (ProductType pt : ptList) {
            claimTypes.add(pt.getProductTypeName());
        }
    }
    
    public void findClaims(String contractNo) {
        LOGGER.info("ClaimFirstLineView, findClaims:contractNo=" + contractNo);
        reset();
        cs.setContractNo(contractNo);
        
        findClaims();
        
    }

    public void findClaims() {
        try {
           
            //claimNumbers = new ArrayList<>();

            LOGGER.info("vin=" + cs.getVinFull());
            LOGGER.info("Number=" + cs.getClaimNumber());
            LOGGER.info("Assigned=" + cs.getUserName());
            LOGGER.info("Number=" + cs.getContractNo());
            LOGGER.info("Start Date=" + cs.getEffectiveDate());
            LOGGER.info("End Date=" + cs.getContractExpirationDate());
            LOGGER.info("Name=" + cs.getCustomer());
            LOGGER.info("Phone=" + cs.getPhoneNumber());
            LOGGER.info("Name=" + cs.getDealerName());
            LOGGER.info("Phone=" + cs.getDealerNumber());

           
            claims = new LazyDataModel<ClaimFirstLine>() {
                @Override
                public ClaimFirstLine getRowData(String rowKey) {
                    for (ClaimFirstLine claimFirstLine : claims) {
                        if (claimFirstLine.getClaimNumber().equals(rowKey)) {
                            return claimFirstLine;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(ClaimFirstLine claimFirstLine) {
                    return claimFirstLine.getClaimNumber();
                }

                @Override
                public List<ClaimFirstLine> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    List<Claim> cl = claimMisc.searchClaims(cs, first, pageSize);
                    return (List<ClaimFirstLine>) service.createClaimView(cl);
                }

            };

            claims.setRowCount(claimMisc.getSearchClaimsCount(cs));
            
        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }

    public void onRowSelect(SelectEvent event) {
        selectedClaim = (ClaimFirstLine) event.getObject();

        FacesMessage msg = new FacesMessage("Claim Selected", ((ClaimFirstLine) event.getObject()).getClaimNumber());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        /*
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        
        String date = format.format(event.getObject());
         */
        Date dt = (Date) event.getObject();
        LOGGER.info("onDateSelect, date=" + dt.toString());
        LOGGER.info("before cs date=" + cs.getContractExpirationDate());
        cs.setContractExpirationDate(dt);
        LOGGER.info("after cs date=" + cs.getContractExpirationDate());
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", dt.toString()));
    }
    
    public String getClaimSearch()  {
        LOGGER.info("getClaimSearch");
        reset();
        claims = new LazyDataModel<ClaimFirstLine>() {
                @Override
                public ClaimFirstLine getRowData(String rowKey) {
                    return null;
                }

                @Override
                public Object getRowKey(ClaimFirstLine claimFirstLine) {
                    return null;
                }

                @Override
                public List<ClaimFirstLine> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    return null;
                }

            };

            claims.setRowCount(0);
        
        return "claimSearch";
    }
    
    public void reset() {
        cs.setVinFull(null);
        cs.setClaimId(null);
        cs.setClaimNumber(null);
        cs.setContractExpirationDate(null);
        cs.setContractNo(null);
        cs.setCustomer(null);
        cs.setDealerName(null);
        cs.setDealerNumber(null);
        cs.setEffectiveDate(null);
        cs.setPhoneNumber(null);
        cs.setProductTypeName(null);
        cs.setUserName(null);
        cs.setVinFull( null);
        
        claims = null;
        
    }
    
    public void dialogClosed() {
        LOGGER.info("in claimFirstLineView, dialogClosed");
        findClaims();
    }

}
