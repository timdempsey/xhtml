/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class RF {

    public RF() {
    }

    public RF(Integer rfId, String rfName, String phoneNumber, String taxId, Date effectiveDate, String email, Boolean emailOptIn, Boolean holdPayables, String prefpayMethod, String billTo, Boolean isActive, MyAddress myAddress, BigDecimal laborTax, BigDecimal partTax, BigDecimal laborRate, Integer partWarrantyMonths, Integer partWarrantyMiles) {
        this.rfId = rfId;
        this.rfName = rfName;
        this.phoneNumber = phoneNumber;
        this.taxId = taxId;
        this.effectiveDate = effectiveDate;
        this.email = email;
        this.emailOptIn = emailOptIn;
        this.holdPayables = holdPayables;
        this.prefpayMethod = prefpayMethod;
        this.billTo = billTo;
        this.isActive = isActive;
        this.myAddress = myAddress;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.laborRate = laborRate;
        this.partWarrantyMonths = partWarrantyMonths;
        this.partWarrantyMiles = partWarrantyMiles;
    }

    private Integer rfId;
    private String rfName;
    private String phoneNumber;
    private String taxId;
    private Date effectiveDate;
    private String email;
    private Boolean emailOptIn;
    private Boolean holdPayables;
    private String prefpayMethod;
    private String billTo;
    private Boolean isActive;
    private MyAddress myAddress;
    private BigDecimal laborTax;
    private BigDecimal partTax;
    private BigDecimal laborRate;
    private Integer partWarrantyMonths;
    private Integer partWarrantyMiles;

    public Integer getRfId() {
        return rfId;
    }

    public void setRfId(Integer rfId) {
        this.rfId = rfId;
    }

    public String getRfName() {
        return rfName;
    }

    public void setRfName(String rfName) {
        this.rfName = rfName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Boolean getHoldPayables() {
        return holdPayables;
    }

    public void setHoldPayables(Boolean holdPayables) {
        this.holdPayables = holdPayables;
    }

    public String getPrefpayMethod() {
        return prefpayMethod;
    }

    public void setPrefpayMethod(String prefpayMethod) {
        this.prefpayMethod = prefpayMethod;
    }

    public String getBillTo() {
        return billTo;
    }

    public void setBillTo(String billTo) {
        this.billTo = billTo;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public MyAddress getMyAddress() {
        return myAddress;
    }

    public void setMyAddress(MyAddress myAddress) {
        this.myAddress = myAddress;
    }

    public BigDecimal getLaborTax() {
        return laborTax;
    }

    public void setLaborTax(BigDecimal laborTax) {
        this.laborTax = laborTax;
    }

    public BigDecimal getPartTax() {
        return partTax;
    }

    public void setPartTax(BigDecimal partTax) {
        this.partTax = partTax;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }

    public void reset() {
        rfId = null;
        rfName = "";
        phoneNumber = "";
        taxId = "";
        effectiveDate = null;
        email = "";
        emailOptIn = false;
        holdPayables = false;
        prefpayMethod = "";
        billTo = "";
        isActive = true;
        myAddress = null;
        laborTax = null;
        partTax = null;
        laborRate = null;
        partWarrantyMonths = null;
        partWarrantyMiles = null;
    }

}
