/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.VehiclePurchaseType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "quickRaterService")
@ApplicationScoped
public class QuickRaterService {
    
    private static final Logger LOGGER = LogManager.getLogger(ContractRatingService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of QuickRaterService
     */
    public QuickRaterService() {
        qr = new QuickRater();
        countryOfOrigin = null;
    }
    
    private QuickRater qr;
    private List<VehiclePurchaseType> vptList;
    private HashMap<String, Integer> vptMap;
    private Boolean vinEntered;
    private ClaimVehicleInfo cvi;
    private String countryOfOrigin;
    
    /*
    getter/setter
    */

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }
    
    

    public ClaimVehicleInfo getCvi() {
        return cvi;
    }

    public void setCvi(ClaimVehicleInfo cvi) {
        this.cvi = cvi;
    }

   

    
    

    public Boolean getVinEntered() {
        return vinEntered;
    }

    public void setVinEntered(Boolean vinEntered) {
        this.vinEntered = vinEntered;
    }
    
    
    
    public QuickRater getQr() {
        return qr;
    }

    public void setQr(QuickRater qr) {
        this.qr = qr;
    }

    public List<VehiclePurchaseType> getVptList() {
        return vptList;
    }

    public void setVptList(List<VehiclePurchaseType> vptList) {
        this.vptList = vptList;
    }

    public HashMap<String, Integer> getVptMap() {
        return vptMap;
    }

    public void setVptMap(HashMap<String, Integer> vptMap) {
        this.vptMap = vptMap;
    }
    
    
    
    /*
    functions
    */
    @PostConstruct
    public void init() {
        LOGGER.info("init from QuickRaterService" );
        vptList = new ArrayList<>();
        vptList = claimMisc.getVehiclePurchaseTypeByAll();
        vinEntered = false;
       
    }
    
    ;
    private void getVPTMap() {
        vptList = claimMisc.getVehiclePurchaseTypeByAll();
        vptMap = new HashMap<>();
        for( VehiclePurchaseType vpt : vptList ) {
            vptMap.put(vpt.getDescription(), vpt.getVehiclePurchaseTypeId());
        }
    }
    
    public String getQR() {
        LOGGER.info("in getQuickRaterSearch");
        return "QuickRater";
    }
    
    public void getRateDebugger() {
        LOGGER.info("in getRateDebugger");
    }
    
    public void getRates() {
        LOGGER.info("in getRates");
    }
    
    public void checkVin() {
        String vinFull = qr.getVinNo();
        LOGGER.info("in checkVin, vinFull=" + vinFull);
        if( vinFull != null && vinFull.length() > 0 ) {
            cvi = claimMisc.setClaimVehicleInfo(vinFull);
            vinEntered = true;
            LOGGER.info("vinEntered=" + vinEntered);
            LOGGER.info("make=" + cvi.getMake());
        }
    }
    
    public void reset() {
        qr.reset();
    }
    
}
