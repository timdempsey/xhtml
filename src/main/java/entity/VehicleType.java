/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "vehicleType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleType.findAll", query = "SELECT v FROM VehicleType v")
    , @NamedQuery(name = "VehicleType.findByVehicleTypeId", query = "SELECT v FROM VehicleType v WHERE v.vehicleTypeId = :vehicleTypeId")
    , @NamedQuery(name = "VehicleType.findByName", query = "SELECT v FROM VehicleType v WHERE v.name = :name")})
public class VehicleType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vehicleTypeId")
    private Integer vehicleTypeId;
    @Size(max = 30)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "vehicleTypeInd")
    private Collection<ClassItem> classItemCollection;

    public VehicleType() {
    }

    public VehicleType(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleTypeId != null ? vehicleTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleType)) {
            return false;
        }
        VehicleType other = (VehicleType) object;
        if ((this.vehicleTypeId == null && other.vehicleTypeId != null) || (this.vehicleTypeId != null && !this.vehicleTypeId.equals(other.vehicleTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VehicleType[ vehicleTypeId=" + vehicleTypeId + " ]";
    }
    
}
