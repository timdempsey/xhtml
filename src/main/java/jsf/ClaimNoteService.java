/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.Note;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimNoteService")
@ApplicationScoped
public class ClaimNoteService {
    
     private static final Logger LOGGER = LogManager.getLogger(ClaimNoteService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimNoteService
     */
    public ClaimNoteService() {
    }
    
    private String claimNumber;
    private Claim claim;
    
    private ClaimNotes claimNotes;
    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public ClaimNotes getClaimNotes() {
        return claimNotes;
    }

    public void setClaimNotes(ClaimNotes claimNotes) {
        this.claimNotes = claimNotes;
    }
    
    public void chooseClaimNotes(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("chooseClaimNotes=" + claimNumber);

        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
        
        claimNotes = createClaimNotesView(claimNumber);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);

        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);
        
        RequestContext.getCurrentInstance().openDialog("claimNote", options, null);
    }
    
    public ClaimNotes createClaimNotesView(String claimNumber) {
        LOGGER.info("in createClaimNotesView, claimNumber="+claimNumber);
        
        claim = claimMisc.getClaimByNumber(claimNumber);
       
        List<ClaimNote> list = new ArrayList<>();

        List<Note> notes = claimMisc.getNoteByClaimNumber(claimNumber);
        
        for (Note note : notes) {   // ClaimTopLine
            
            list.add(new ClaimNote(note.getNoteId(), note.getNote(), claimMisc.getAccountKeeperById(note.getEnteredByIdFk().getUserMemberId()).getAccountKeeperName(), note.getEnteredDate()));
            
        }

        LOGGER.info("claimNumber="+claim.getClaimNumber());
        return new ClaimNotes(claim.getClaimId(), claim.getClaimNumber(), list);

    }
    
    public void addClaimNote(ActionEvent event) {
        LOGGER.info("in addClaimNote=" + claimNumber);
        /*
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("chooseClaimNotes=" + claimNumber);

        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();
        
        claimNotes = createClaimNotesView(claimNumber);
        */
        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);

        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);
        
        RequestContext.getCurrentInstance().openDialog("addClaimNote", options, null);
    }
    
    public void saveClaimNote(ActionEvent event) {
        LOGGER.info("in saveClaimNote=" + claimNumber);
        LOGGER.info("note=" + note);
        

        try {
            utx.begin();
            Query query = em.createNativeQuery("insert into Note (note, enteredDate, noteTypeInd, enteredByIdFk, claimIdFk) values (?1, ?2, ?3, ?4, ?5)");
                query.setParameter(1, note);
                
                query.setParameter(2, claimMisc.getCurrnetDateTime());
                Integer noteTypeInd = 1;    // need to be clearified
                query.setParameter(3, noteTypeInd);
                Integer enteredByIdFk = 14; // should not be hardcoded
                query.setParameter(4, enteredByIdFk);
                query.setParameter(5, claim.getClaimId());
                
                int ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                utx.commit();
                claimNotes = createClaimNotesView(claimNumber);
                LOGGER.info("notes=" + claimNotes.claimNotes.size());
                note = null;
                RequestContext.getCurrentInstance().closeDialog("addClaimNote");
               
                
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }
    
    public void dialogClosed( SelectEvent event) {
        LOGGER.info("save dialog closed=" + claimNumber);
        claimNotes = createClaimNotesView(claimNumber);
    }
    
}
