/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceCustodialAccountBalance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceCustodialAccountBalance.findAll", query = "SELECT r FROM ReinsuranceCustodialAccountBalance r")
    , @NamedQuery(name = "ReinsuranceCustodialAccountBalance.findByReinsuranceCustodialAccountBalanceId", query = "SELECT r FROM ReinsuranceCustodialAccountBalance r WHERE r.reinsuranceCustodialAccountBalanceId = :reinsuranceCustodialAccountBalanceId")
    , @NamedQuery(name = "ReinsuranceCustodialAccountBalance.findByBalance", query = "SELECT r FROM ReinsuranceCustodialAccountBalance r WHERE r.balance = :balance")
    , @NamedQuery(name = "ReinsuranceCustodialAccountBalance.findByUpdateUserName", query = "SELECT r FROM ReinsuranceCustodialAccountBalance r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceCustodialAccountBalance.findByUpdateLast", query = "SELECT r FROM ReinsuranceCustodialAccountBalance r WHERE r.updateLast = :updateLast")})
public class ReinsuranceCustodialAccountBalance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceCustodialAccountBalanceId")
    private Integer reinsuranceCustodialAccountBalanceId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Balance")
    private BigDecimal balance;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "insurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer insurerIdFk;
    @JoinColumn(name = "reinsurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer reinsurerIdFk;
    @JoinColumn(name = "postingHistoryIdFk", referencedColumnName = "postingHistoryId")
    @ManyToOne
    private PostingHistory postingHistoryIdFk;
    @JoinColumn(name = "applicableProductIdFk", referencedColumnName = "rateBasedRuleGroupId")
    @ManyToOne
    private RateBasedRuleGroup applicableProductIdFk;

    public ReinsuranceCustodialAccountBalance() {
    }

    public ReinsuranceCustodialAccountBalance(Integer reinsuranceCustodialAccountBalanceId) {
        this.reinsuranceCustodialAccountBalanceId = reinsuranceCustodialAccountBalanceId;
    }

    public Integer getReinsuranceCustodialAccountBalanceId() {
        return reinsuranceCustodialAccountBalanceId;
    }

    public void setReinsuranceCustodialAccountBalanceId(Integer reinsuranceCustodialAccountBalanceId) {
        this.reinsuranceCustodialAccountBalanceId = reinsuranceCustodialAccountBalanceId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Insurer getInsurerIdFk() {
        return insurerIdFk;
    }

    public void setInsurerIdFk(Insurer insurerIdFk) {
        this.insurerIdFk = insurerIdFk;
    }

    public Insurer getReinsurerIdFk() {
        return reinsurerIdFk;
    }

    public void setReinsurerIdFk(Insurer reinsurerIdFk) {
        this.reinsurerIdFk = reinsurerIdFk;
    }

    public PostingHistory getPostingHistoryIdFk() {
        return postingHistoryIdFk;
    }

    public void setPostingHistoryIdFk(PostingHistory postingHistoryIdFk) {
        this.postingHistoryIdFk = postingHistoryIdFk;
    }

    public RateBasedRuleGroup getApplicableProductIdFk() {
        return applicableProductIdFk;
    }

    public void setApplicableProductIdFk(RateBasedRuleGroup applicableProductIdFk) {
        this.applicableProductIdFk = applicableProductIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceCustodialAccountBalanceId != null ? reinsuranceCustodialAccountBalanceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceCustodialAccountBalance)) {
            return false;
        }
        ReinsuranceCustodialAccountBalance other = (ReinsuranceCustodialAccountBalance) object;
        if ((this.reinsuranceCustodialAccountBalanceId == null && other.reinsuranceCustodialAccountBalanceId != null) || (this.reinsuranceCustodialAccountBalanceId != null && !this.reinsuranceCustodialAccountBalanceId.equals(other.reinsuranceCustodialAccountBalanceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceCustodialAccountBalance[ reinsuranceCustodialAccountBalanceId=" + reinsuranceCustodialAccountBalanceId + " ]";
    }
    
}
