/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

/**
 *
 * @author Jiepi
 */
public class SearchCriterion {

    public SearchCriterion() {
    }

    public SearchCriterion(String dealerName, String programName, String productName, String planName, Long days) {
        this.dealerName = dealerName;
        this.programName = programName;
        this.productName = productName;
        this.planName = planName;
        this.days = days;
    }

    

    private String dealerName;
    private String programName;
    private String productName;
    private String planName;
    private Long days;

    public Long getDays() {
        return days;
    }

    public void setDays(Long days) {
        this.days = days;
    }
    
    

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public void reset() {
        dealerName = "";
        programName = "";
        productName = "";
        planName = "";
        days = null;
    }

}
