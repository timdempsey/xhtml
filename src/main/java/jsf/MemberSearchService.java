/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "memberSearchService")
@ApplicationScoped
public class MemberSearchService {
    private static final Logger LOGGER = LogManager.getLogger(MemberSearchService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of MemberSearchService
     */
    public MemberSearchService() {
    }
    
    @Inject
    transient private ClaimMisc claimMisc;
    
    @Inject
    ClaimGeneralService claimGeneralService;
    
    private List<MemberSearch> msList; // rfs
    MemberSearch selectedMS;    // selecteRF
    MemberSearch mssearch;  // rfsearch
    
    String shortName;
    String longName;
    
    String claimNumber;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public List<MemberSearch> getMsList() {
        return msList;
    }

    public void setMsList(List<MemberSearch> msList) {
        this.msList = msList;
    }

    public MemberSearch getSelectedMS() {
        return selectedMS;
    }

    public void setSelectedMS(MemberSearch selectedMS) {
        this.selectedMS = selectedMS;
    }

    public MemberSearch getMssearch() {
        return mssearch;
    }

    public void setMssearch(MemberSearch mssearch) {
        this.mssearch = mssearch;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }
    
    
    @PostConstruct
    public void init() {
        LOGGER.info("init from MemberSearchService");
        msList = new ArrayList<>();
        selectedMS = new MemberSearch();
        mssearch = new MemberSearch();
    }
    
    public void reset() {
        shortName = "";
        longName = "";
    }
    
    public void createMemberSearchView() {
        try {
            //List<RFSearch> rfList = new ArrayList<>();
            
            LOGGER.info("shortName=" + shortName);
            LOGGER.info("longName=" + longName);
            
            mssearch.shortName = shortName;
            mssearch.longName = longName;
            
            msList.clear();
            
            msList = claimMisc.getMemberSearch(mssearch);
            
            if (msList != null) {
                LOGGER.info("size of msList=" + msList.size());           
            }
            
        } catch (Exception ex) {
            ex.getStackTrace();
        }
    }
    
     //public void searchRF(ActionEvent event) {
    public void searchMember(ActionEvent event) {
        
         /*
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("searchRF=" + claimNumber);
        */
        msList.clear();
        reset();
        //contractNo = (String) event.getComponent().getAttributes().get("contractNo");
        

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1400");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("searchMember", options, null);
    }
    
     /* original
    public void onRowSelect(SelectEvent event) {
        selectedRF = (RFSearch) event.getObject();
        
        FacesMessage msg = new FacesMessage("RFSearch Selected", ((RFSearch) event.getObject()).getRfName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        LOGGER.info("RFSearch, claimNumber=" + claimNumber);
        
        ClaimGeneral cg = claimGeneralService.createClaimView(claimNumber);
        LOGGER.info("pzip=" + cg.getZip());
        LOGGER.info("pname="+cg.getRfName());
        LOGGER.info("azip="+selectedRF.getZip());
        LOGGER.info("aname=" +selectedRF.getRfName());
        cg.setRfName(selectedRF.getRfName());
        cg.setRfPhone(selectedRF.getRfPhone());
        cg.setCity(selectedRF.getCity());
        cg.setState(selectedRF.getState());
        cg.setZip(selectedRF.getZip());
        cg.setLaborRate(selectedRF.getLaborRate());
        cg.setLaborTax(selectedRF.getLaborTax());
        cg.setPartTax(selectedRF.getPartTax());
        cg.setLine1(selectedRF.getAddress1());
        cg.setLine2(selectedRF.getAddress2());
        claimGeneralService.setCg(cg);
        //FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove("searchRF");
        RequestContext.getCurrentInstance().closeDialog("searchRF");
        
    }
*/
     public void onRowSelect(SelectEvent event) {
        selectedMS = (MemberSearch) event.getObject();
        LOGGER.info("onRowSelect, fullName=" + selectedMS.getLongName());
        
        claimGeneralService.setMemberSearch(selectedMS);
        /*
        FacesMessage msg = new FacesMessage("RFSearch Selected", ((RFSearch) event.getObject()).getRfName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        LOGGER.info("RFSearch, claimNumber=" + claimNumber);
        
        ClaimGeneral cg = claimGeneralService.createClaimView(claimNumber);
        LOGGER.info("pzip=" + cg.getZip());
        LOGGER.info("pname="+cg.getRfName());
        LOGGER.info("azip="+selectedRF.getZip());
        LOGGER.info("aname=" +selectedRF.getRfName());
        cg.setRfName(selectedRF.getRfName());
        cg.setRfPhone(selectedRF.getRfPhone());
        cg.setCity(selectedRF.getCity());
        cg.setState(selectedRF.getState());
        cg.setZip(selectedRF.getZip());
        cg.setLaborRate(selectedRF.getLaborRate());
        cg.setLaborTax(selectedRF.getLaborTax());
        cg.setPartTax(selectedRF.getPartTax());
        cg.setLine1(selectedRF.getAddress1());
        cg.setLine2(selectedRF.getAddress2());
        claimGeneralService.setCg(cg);
        */
        //FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove("searchRF");
        RequestContext.getCurrentInstance().closeDialog("searchMember");
        
        
    }
    
}
