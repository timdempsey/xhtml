/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Jiepi
 */
@Named
@ApplicationScoped
public class LazyFirstLineDataModel extends LazyDataModel<ClaimFirstLine>   {
    
    private static final Logger LOGGER = LogManager.getLogger(LazyFirstLineDataModel.class);

    @PersistenceContext
    private EntityManager em;
      
    
    public LazyFirstLineDataModel(ClaimSearch cs) {
        LOGGER.info("cs=" + cs.getUserName());
        this.cs = cs;
        claimMisc = new ClaimMisc();
        service = new ClaimFirstLineService();
        
    }

    public LazyFirstLineDataModel() {
    }

    @PostConstruct
    public void init() {
        LOGGER.info("iaminLazyFirstLineDataModel post=======");
    }
    

    private List<ClaimFirstLine> claims;
    private ClaimSearch cs;

     @Inject
    private ClaimMisc claimMisc;
     @Inject
    private ClaimFirstLineService service;
    
    
    @Override
    public ClaimFirstLine getRowData(String rowKey) {
        for (ClaimFirstLine claimFirstLine : claims) {
            if (claimFirstLine.getClaimNumber().equals(rowKey)) {
                return claimFirstLine;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(ClaimFirstLine claimFirstLine) {
        return claimFirstLine.getClaimNumber();
    }

    @Override
    public List<ClaimFirstLine> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        /*
        LOGGER.info("first="+first);
        LOGGER.info("pageSize="+pageSize);
        LOGGER.info("sortField="+sortField);
        LOGGER.info("sortOrder="+sortOrder);
        LOGGER.info("size of filters="+filters.size());
         */
        try {
        
            List<ClaimFirstLine> data = new ArrayList<ClaimFirstLine>();
            if (claimMisc == null) {
                LOGGER.info("why claimMisc  is null in load?");
            }
            
            List<Claim> cl = claimMisc.searchClaims(cs, first, pageSize);

            if( service == null ) {
                LOGGER.info("why service in load is null?");
            }
            claims = (List<ClaimFirstLine>) service.createClaimView(cl);
            LOGGER.info("size of cliams=" + claims.size());
            // filter
            for (ClaimFirstLine claimFirstLine : claims) {
                boolean match = true;

                if (filters != null) {
                    for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                        try {
                            String filterProperty = it.next();
                            Object filterValue = filters.get(filterProperty);
                            String fieldValue = String.valueOf(claimFirstLine.getClass().getField(filterProperty).get(claimFirstLine));

                            if (filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                                match = true;
                            } else {
                                match = false;
                                break;
                            }
                        } catch (Exception e) {
                            match = false;
                        }
                    }
                }

                if (match) {
                    data.add(claimFirstLine);
                }
            }

            //sort
            LOGGER.info("sortField=" + sortField);
            if (sortField != null) {
                Collections.sort(data, new LazyClaimFirstLineSorter(sortField, sortOrder));
            }

            //rowCount
            int dataSize = data.size();
            this.setRowCount(dataSize);
            LOGGER.info("dataSize=" + dataSize);
            //paginate
            if (dataSize > pageSize) {

                return data.subList(first, first + pageSize);

            } else {
                return data;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Claim> searchClaims(ClaimSearch cs, int first, int pageSize) {
        
        try {
            List<Claim> cl = new ArrayList<>();
            String qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e,  ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerId=e.dealerId and  a.productTypeIdFk=g.productTypeId ";
            
            //LOGGER.info("qs=" + qs);
            String vinStr = "";
            String claimStr = "";
            String assignedToStr = "";
            String contractStr = "";
            String startStr = "";
            String endStr = "";
            String customerStr = "";
            String phoneStr = "";
            String dealerNameStr = "";
            String dealerNoStr = "";
            String typeStr = "";

            if (cs.getVinFull() != null && cs.getVinFull().length() > 0) {
                LOGGER.info("vin=" + cs.getVinFull());
                vinStr = "  and b.vinFull= '" + cs.getVinFull() + "'  ";
            }
            if (cs.getClaimNumber() != null && cs.getClaimNumber().length() > 0) {
                LOGGER.info("claimNumber=" + cs.getClaimNumber());
                claimStr = " and a.claimNumber= '" + cs.getClaimNumber() + "'  ";
            }
            if (cs.getUserName() != null && cs.getUserName().length() > 0) {
                 qs = "select a.* from Claim a, Contracts b, AccountKeeper c, Address d, Dealer e, UserMember f, ProductType g where a.contractIdFk=b.contractId and b.customerIdFk=c.accountKeeperId and c.physicalAddressIdFk=d.addressId and b.dealerId=e.dealerId and a.ownedByFk=f.userMemberId and a.productTypeIdFk=g.productTypeId ";
                assignedToStr = " and f.userName= '" + cs.getUserName() + "'  ";
            }
            if (cs.getContractNo() != null && cs.getContractNo().length() > 0) {
                contractStr = "  and b.contractNo= '" + cs.getContractNo() + "'  ";
            }
            if (cs.getEffectiveDate() != null && cs.getEffectiveDate().toString().length() > 0) {
                LOGGER.info("effect date=" + cs.getEffectiveDate());
                String sd = claimMisc.getJavaDate(cs.getEffectiveDate());
                startStr = " and b.effectiveDate >=  '" + sd + "'  ";
            }
            if (cs.getContractExpirationDate() != null && cs.getContractExpirationDate().toString().length() > 0) {
                LOGGER.info("expire date=" + cs.getContractExpirationDate());
                String ed = claimMisc.getJavaDate(cs.getContractExpirationDate());
                endStr = " and b.contractExpirationDate <= '" + ed + "'  ";
            }
            if (cs.getCustomer() != null && cs.getCustomer().length() > 0) {
                customerStr = " and c.accountKeeperName like '%" + cs.getCustomer() + "%'  ";
            }
            if (cs.getPhoneNumber() != null && cs.getPhoneNumber().length() > 0) {
                phoneStr = " and d.phoneNumber= '" + cs.getPhoneNumber() + "'  ";
            }
            if (cs.getDealerName() != null && cs.getDealerName().length() > 0) {   // accountHolderTypeInd=1
                Integer dealId = (Integer) em.createNativeQuery("select accountKeeperId from AccountKeeper where accountKeeperName=?1 and  accountHolderTypeInd=1").setParameter(1, cs.getDealerName()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + dealId + "' ";
            }
            if (cs.getDealerNumber() != null && cs.getDealerNumber().length() > 0) {   // accountHolderTypeInd=1
                Integer id = (Integer) em.createNativeQuery("select dealerId from Dealer where dealerNumber=?1").setParameter(1, cs.getDealerNumber()).getSingleResult();
                dealerNameStr = " and e.dealerId= '" + id + "' ";
            }
            if (cs.getProductTypeName() != null && cs.getProductTypeName().length() > 0) {   // accountHolderTypeInd=1
                typeStr = " and g.productTypeName= '" + cs.getProductTypeName() + "' ";
            }

            String endstr = " order by a.claimId desc";
            //LOGGER.info("endstr=" + endstr);

            String query = qs + vinStr + claimStr + assignedToStr + contractStr + startStr + endStr + customerStr + phoneStr + dealerNameStr + dealerNoStr + typeStr + endstr;

            LOGGER.info("query=" + query);

            // new
            if (em == null) {
                LOGGER.info("em is null, why");
            }
            
            Query q = em.createNativeQuery(query, Claim.class);

            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            return  (List<Claim>) q.getResultList();
            //return  (List<Claim>) em.createNativeQuery(query, Claim.class).getResultList();
        } catch (NoResultException e) {
            LOGGER.info("searchClaims is null");
            return null;
        } catch (Exception ex) {
            LOGGER.info("=====1====");
            ex.printStackTrace();
            LOGGER.info("message=" + ex.getMessage());
            LOGGER.info("=====2====");
            return null;
        }
    }
}
