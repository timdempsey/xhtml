package jsf;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import entity.AccountKeeper;
import entity.Address;
import entity.CfRegion;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiep
 */
@Named(value = "customerInfoService")
@ApplicationScoped
public class CustomerInfoService {

    private static final Logger LOGGER = LogManager.getLogger(CustomerInfoService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of CustomerInfoService
     */
    public CustomerInfoService() {
        cust = new CustomerInfo();
        cust.reset();
        selectedCust = new CustomerInfo();
        selectedCust.reset();
        selectedCustomer = new AccountKeeper();
        //customerList = new ArrayList<>();
        stateMap = new TreeMap<>();
        sameAsPaddr = true;
        customerId = null;
    }
    
    public void customerListReset() {
        
    }

    private Integer preferedTransactioinTypeInd = 1;

    List<CustomerInfo> custList;
    CustomerInfo selectedCust;
    CustomerInfo cust;
    TreeMap<String, String> stateMap;

    public TreeMap<String, String> getStateMap() {
        return stateMap;
    }

    public void setStateMap(TreeMap<String, String> stateMap) {
        this.stateMap = stateMap;
    }

    public List<CustomerInfo> getCustList() {
        return custList;
    }

    public void setCustList(List<CustomerInfo> custList) {
        this.custList = custList;
    }

    public CustomerInfo getSelectedCust() {
        return selectedCust;
    }

    public void setSelectedCust(CustomerInfo selectedCust) {
        this.selectedCust = selectedCust;
    }

    public CustomerInfo getCust() {
        return cust;
    }

    public void setCust(CustomerInfo cust) {
        this.cust = cust;
    }

    /*
    private AccountKeeper customer;
    private Address paddr;
    private Address baddr;
    private String firstName;
    private String lastName;
   
    private CfRegion regionP;
    private CfRegion regionB;
    private boolean sameAsPaddr;
    private String businessName;
    private String fullName;
     */
    private boolean sameAsPaddr;
    private String regionCodeP;
    private String regionCodeB;

    public String getRegionCodeP() {
        return regionCodeP;
    }

    public void setRegionCodeP(String regionCodeP) {
        this.regionCodeP = regionCodeP;
    }

    public String getRegionCodeB() {
        return regionCodeB;
    }

    public void setRegionCodeB(String regionCodeB) {
        this.regionCodeB = regionCodeB;
    }

    public Integer getPreferedTransactioinTypeInd() {
        return preferedTransactioinTypeInd;
    }

    public void setPreferedTransactioinTypeInd(Integer preferedTransactioinTypeInd) {
        this.preferedTransactioinTypeInd = preferedTransactioinTypeInd;
    }

    
    public boolean isSameAsPaddr() {
        return sameAsPaddr;
    }

    public void setSameAsPaddr(boolean sameAsPaddr) {
        this.sameAsPaddr = sameAsPaddr;
    }

    /*
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

   

    public CfRegion getRegionP() {
        return regionP;
    }

    public void setRegionP(CfRegion regionP) {
        this.regionP = regionP;
    }

    public CfRegion getRegionB() {
        return regionB;
    }

    public void setRegionB(CfRegion regionB) {
        this.regionB = regionB;
    }

    

    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AccountKeeper getCustomer() {
        return customer;
    }

    public void setCustomer(AccountKeeper customer) {
        this.customer = customer;
    }

    public Address getPaddr() {
        return paddr;
    }

    public void setPaddr(Address paddr) {
        this.paddr = paddr;
    }

    public Address getBaddr() {
        return baddr;
    }

    public void setBaddr(Address baddr) {
        this.baddr = baddr;
    }
     */
    private LazyDataModel<AccountKeeper> customerList;
    AccountKeeper selectedCustomer;
    Integer customerId;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public LazyDataModel<AccountKeeper> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(LazyDataModel<AccountKeeper> customerList) {
        this.customerList = customerList;
    }

   

    public AccountKeeper getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(AccountKeeper selectedCustomer) {
        this.selectedCustomer = selectedCustomer;
    }

    /*
    * core methods
     */
    @PostConstruct
    public void init() {
        loadStateMap();
    }

    public void loadStateMap() {
        stateMap.put("AK", "AK");
        stateMap.put("AL", "AL");
        stateMap.put("AR", "AR");
        stateMap.put("AZ", "AZ");
        stateMap.put("CA", "CA");
        stateMap.put("CO", "CO");
        stateMap.put("CT", "CT");
        stateMap.put("DE", "DE");
        stateMap.put("FL", "FL");
        stateMap.put("GA", "GA");
        stateMap.put("HI", "HI");
        stateMap.put("IA", "IA");
        stateMap.put("ID", "ID");
        stateMap.put("IL", "IL");
        stateMap.put("IN", "IN");
        stateMap.put("IA", "IA");
        stateMap.put("KS", "KS");
        stateMap.put("KY", "KY");
        stateMap.put("LA", "LA");
        stateMap.put("ME", "ME");
        stateMap.put("MD", "MD");
        stateMap.put("MA", "MA");
        stateMap.put("MI", "MI");
        stateMap.put("MN", "MN");
        stateMap.put("MS", "MS");
        stateMap.put("MO", "MO");
        stateMap.put("MT", "MT");
        stateMap.put("NE", "NE");
        stateMap.put("NV", "NV");
        stateMap.put("NH", "NH");
        stateMap.put("NJ", "NJ");
        stateMap.put("NM", "NM");
        stateMap.put("NY", "NY");
        stateMap.put("NC", "NC");
        stateMap.put("ND", "ND");
        stateMap.put("OH", "OH");
        stateMap.put("OK", "OK");
        stateMap.put("OR", "OR");
        stateMap.put("PA", "PA");
        stateMap.put("RI", "RI");
        stateMap.put("SC", "SC");
        stateMap.put("SD", "SD");
        stateMap.put("TN", "TN");
        stateMap.put("TX", "TX");
        stateMap.put("UT", "UT");
        stateMap.put("VT", "VT");
        stateMap.put("VA", "VA");
        stateMap.put("WA", "WA");
        stateMap.put("WV", "WV");
        stateMap.put("WI", "WI");
        stateMap.put("WY", "WY");

    }
    
    public void createCustomerList() {
        LOGGER.info("in createCustomerList");
        customerList = new LazyDataModel<AccountKeeper>() {
                @Override
                public AccountKeeper getRowData(String rowKey) {
                    for (AccountKeeper ak : customerList) {
                        if (ak.getAccountKeeperId().toString().equals(rowKey)) {
                            return ak;
                        }
                    }
                    LOGGER.info("return null.................");
                    return null;
                }

                @Override
                public Object getRowKey(AccountKeeper ak) {
                    return ak.getAccountKeeperId();
                }
                @Override
                public List<AccountKeeper> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    //return claimMisc.searchContracts(cs, first, pageSize);
                    //return (List<ContractFirstLine>) service.createContractView(cl);
                    return claimMisc.getAccountKeeperByCustomerType(first, pageSize);
                }

            };

            customerList.setRowCount(claimMisc.getAKByCTCount());
    }

    public String getCustomerSearch() {
        LOGGER.info(" in getCustomerSearch");
        //reset();
        //customerList = claimMisc.getAccountKeeperByCustomerType();
        
        createCustomerList();
        /*
        customerList = new LazyDataModel<AccountKeeper>() {
                @Override
                public AccountKeeper getRowData(String rowKey) {
                    for (AccountKeeper ak : customerList) {
                        if (ak.getAccountKeeperId().toString().equals(rowKey)) {
                            return ak;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(AccountKeeper ak) {
                    return ak.getAccountKeeperId();
                }
                @Override
                public List<AccountKeeper> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    //return claimMisc.searchContracts(cs, first, pageSize);
                    //return (List<ContractFirstLine>) service.createContractView(cl);
                    return claimMisc.getAccountKeeperByCustomerType(first, pageSize);
                }

            };

            customerList.setRowCount(claimMisc.getAKByCTCount());
        */

        return "CustomerInfo";
    }

    public void addCustomer(ActionEvent event) {

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1600");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddCustomerInfo", options, null);

    }

    public void editCustomer() {
        cust.reset();
        LOGGER.info("in editCustomer, customer=" + selectedCustomer.getAccountKeeperName());
        LOGGER.info("cust fkid=" + cust.getCustomer().getPhysicalAddressIdFk());
        LOGGER.info("selectedCustomer fkid=" + selectedCustomer.getPhysicalAddressIdFk().getAddressId());

        Boolean sapa = true;
        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedCustomer.getAccountKeeperId());

        cust.setBusinessName(ak.getAccountKeeperName());

        customerId = ak.getAccountKeeperId();
        LOGGER.info("customerId=" + customerId);
        if (ak.getPhysicalAddressIdFk().getAddressId().intValue() != ak.getBillingAddressIdFk().getAddressId().intValue()) {
            sapa = false;
        }

        ak = new AccountKeeper();
        ak.setEmailAddress(selectedCustomer.getEmailAddress());
        ak.setEmailOptIn(selectedCustomer.getEmailOptIn());
        cust.setCustomer(ak);

        if (selectedCustomer.getPhysicalAddressIdFk() != null) {
            cust.setPaddr(selectedCustomer.getPhysicalAddressIdFk());
            regionCodeP = selectedCustomer.getPhysicalAddressIdFk().getRegionIdFk().getRegionCode();
        }

        if (selectedCustomer.getBillingAddressIdFk() != null) {
            cust.setBaddr(selectedCustomer.getBillingAddressIdFk());
            regionCodeB = selectedCustomer.getBillingAddressIdFk().getRegionIdFk().getRegionCode();

        }
        /*
        cust.getCustomer().getPhysicalAddressIdFk().setAddress1(selectedCustomer.getPhysicalAddressIdFk().getAddress1());
        cust.getCustomer().getPhysicalAddressIdFk().setAddress2(selectedCustomer.getPhysicalAddressIdFk().getAddress2());
        cust.getCustomer().getPhysicalAddressIdFk().setZipCode(selectedCustomer.getPhysicalAddressIdFk().getZipCode());
        cust.getCustomer().getPhysicalAddressIdFk().setCity(selectedCustomer.getPhysicalAddressIdFk().getCity());
        cust.getCustomer().getPhysicalAddressIdFk().getRegionIdFk().setRegionCode(selectedCustomer.getPhysicalAddressIdFk().getRegionIdFk().getRegionCode());
        cust.getCustomer().getPhysicalAddressIdFk().setPhoneNumber(selectedCustomer.getPhysicalAddressIdFk().getPhoneNumber());
        cust.getCustomer().getPhysicalAddressIdFk().setExtension(selectedCustomer.getPhysicalAddressIdFk().getExtension());
        cust.getCustomer().getPhysicalAddressIdFk().setFax(selectedCustomer.getPhysicalAddressIdFk().getExtension());
         */
        cust.setSameAsPaddr(sapa);
        /*
        if (!sapa) {
            cust.getCustomer().getBillingAddressIdFk().setAddress1(selectedCustomer.getBillingAddressIdFk().getAddress1());
            cust.getCustomer().getBillingAddressIdFk().setAddress2(selectedCustomer.getBillingAddressIdFk().getAddress2());
            cust.getCustomer().getBillingAddressIdFk().setZipCode(selectedCustomer.getBillingAddressIdFk().getZipCode());
            cust.getCustomer().getBillingAddressIdFk().setCity(selectedCustomer.getBillingAddressIdFk().getCity());
            cust.getCustomer().getBillingAddressIdFk().getRegionIdFk().setRegionCode(selectedCustomer.getBillingAddressIdFk().getRegionIdFk().getRegionCode());
            cust.getCustomer().getBillingAddressIdFk().setPhoneNumber(selectedCustomer.getBillingAddressIdFk().getPhoneNumber());
            cust.getCustomer().getBillingAddressIdFk().setExtension(selectedCustomer.getBillingAddressIdFk().getExtension());
            cust.getCustomer().getBillingAddressIdFk().setFax(selectedCustomer.getBillingAddressIdFk().getExtension());
        }*/

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1600");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddCustomerInfo", options, null);

    }

    public void deleteCustomer() {
        LOGGER.info("in deleteCustomer");
        claimMisc.deleteCustomer(selectedCustomer.getAccountKeeperId());
        createCustomerList();
    }

    public void saveDialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");

        customerId = null;

        //saveCustomer();
        //customerList = claimMisc.getAccountKeeperByCustomerType();
        createCustomerList();
    }

    public void updateDialogClosed(SelectEvent event) {
        LOGGER.info("in updateDialogClosed");

        updateCustomer();

        //customerList = claimMisc.getAccountKeeperByCustomerType();
        createCustomerList();
    }

    public void verifyCustomer() {
        String fullName = cust.getFirstName() + " " + cust.getLastName();
        cust.setFullName(fullName);
        LOGGER.info("in verifyCustomer, fullName=" + fullName);
        if (fullName.length() < 1) {
            fullName = cust.getBusinessName();
        }
        if (fullName.length() > 2) {
            AccountKeeper customer = claimMisc.getAccountKeeperForCustomer(fullName);
            if (customer != null && customer.getAccountKeeperName() != null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "name exist, please verify: ", fullName);
                FacesContext.getCurrentInstance().addMessage(null, msg);

                cust.setCustomer(customer);

                Address paddr = customer.getPhysicalAddressIdFk();
                if (paddr != null) {
                    cust.setPaddr(paddr);
                    CfRegion regionP = paddr.getRegionIdFk();
                    cust.setRegionP(regionP);
                }

                Address baddr = customer.getBillingAddressIdFk();
                if (baddr != null) {
                    cust.setBaddr(baddr);
                    CfRegion regionB = baddr.getRegionIdFk();
                    cust.setRegionB(regionB);
                }

            }
        }
    }

    public void saveCustomer() {
        LOGGER.info("in saveCustomer, customerId=" + customerId);

        try {
            utx.begin();
            
            String lastName = cust.getLastName();
            String firstName = cust.getFirstName();
            String fullName;
            if (lastName.length() < 1 || firstName.length() < 1) {
                fullName = cust.getBusinessName();
            } else {
                fullName = firstName + " " + lastName;
            }
            LOGGER.info("fullName=" + fullName);
            LOGGER.info("same billing address=" + sameAsPaddr);
            // no matter new or old customer, update the addres anyway
            String line1 = cust.getPaddr().getAddress1();
            LOGGER.info("line1=" + line1);
            Address paddr = new Address();
            Address baddr;
            CfRegion rp;
            CfRegion rb;
            Integer paddrFkId = null;
            Integer baddrFkId = null;
            if (line1 != null && line1.length() > 0 && customerId == null) {  // adding Address and Region information
                if (regionCodeP.length() > 0) {
                    LOGGER.info("regionCodeP=" + regionCodeP);
                    rp = claimMisc.getRegionByRegionCode(regionCodeP);
                    cust.getPaddr().setRegionIdFk(claimMisc.getRegionById(rp.getRegionId()));
                }
                cust.getPaddr().setAddressName("Physical");
                paddrFkId = claimMisc.insertAddress(cust.getPaddr());
                LOGGER.info("paddrFkId=" + paddrFkId);
                LOGGER.info("paddr.line1=" + paddr.getAddress1());
                if (sameAsPaddr) {
                    baddrFkId = paddrFkId;
                } else {
                    if (regionCodeB.length() > 0) {
                        rb = claimMisc.getRegionByRegionCode(regionCodeB);
                        cust.getBaddr().setRegionIdFk(claimMisc.getRegionById(rb.getRegionId()));
                    }
                    cust.getBaddr().setAddressName("Billing");
                    baddrFkId = claimMisc.insertAddress(cust.getBaddr());
                    LOGGER.info("baddrFkId=" + baddrFkId);
                }
            }

            //utx.begin();

            if (customerId == null) {
                // create new customer
                LOGGER.info("create new customer");
                //ak = cust.getCustomer();
                Query query = em.createNativeQuery("insert into AccountKeeper (accountKeeperName, active, accountKeeperTypeIdFk, emailAddress, emailOptIn, billingAddressIdFk, physicalAddressIdFk) values (?1, ?2, ?3,?4,?5,?6,?7)");
                Integer akType = claimMisc.getAccountKeeperTypeByName("Customer").getAccountKeeperTypeId();

                query.setParameter(1, fullName);
                query.setParameter(2, true);
                query.setParameter(3, akType);
                query.setParameter(4, cust.getCustomer().getEmailAddress());
                query.setParameter(5, cust.getCustomer().getEmailOptIn());
                query.setParameter(6, baddrFkId);
                query.setParameter(7, paddrFkId);
                query.executeUpdate();

                LOGGER.info("succesful");
            } else {    // updating customer
                LOGGER.info("updating customer");
                AccountKeeper ak = claimMisc.getAccountKeeperById(customerId);

                //LOGGER.info("paddr.idfk=" + cust.getCustomer().getPhysicalAddressIdFk() );
                LOGGER.info("regionCodeP=" + regionCodeP);
                if (cust.getPaddr().getAddress1() != null) {
                    //paddr = ak.getPhysicalAddressIdFk();
                    //LOGGER.info("addr id=" + paddr.getAddressId());

                    paddr = cust.getPaddr();
                    LOGGER.info("paddr.id=" + paddr.getAddressId());

                    rp = claimMisc.getRegionByRegionCode(regionCodeP);
                    LOGGER.info("rp=" + rp.getRegionCode());
                    paddr.setRegionIdFk(rp);
                    em.merge(paddr);
                }

                LOGGER.info("regionCodeB=" + regionCodeB);
                LOGGER.info("sameAsPaddr=" + sameAsPaddr);

                if (!sameAsPaddr) {
                    baddr = cust.getBaddr();
                    LOGGER.info("baddr.id=" + baddr.getAddressId());
                    if (baddr.getAddressId().intValue() == paddr.getAddressId().intValue()) {
                        // now need to create own Address
                        LOGGER.info("regionCodeB=" + regionCodeB);
                        if (regionCodeB.length() > 0) {
                            rb = claimMisc.getRegionByRegionCode(regionCodeB);
                            cust.getBaddr().setRegionIdFk(claimMisc.getRegionById(rb.getRegionId()));
                        }
                        cust.getBaddr().setAddressName("Billing");
                        baddrFkId = claimMisc.insertAddress(cust.getBaddr());
                        LOGGER.info("baddrFkId=" + baddrFkId);
                        //cust.getBaddr().setAddressId(baddrFkId);
                        baddr.setAddressId(baddrFkId);
                    } else {
                        rb = claimMisc.getRegionByRegionCode(regionCodeB);
                        LOGGER.info("rb=" + rb.getRegionCode());
                        baddr.setRegionIdFk(rb);
                        em.merge(baddr);
                    }
                } else {
                    baddr = paddr;
                }

                LOGGER.info("fullName=" + fullName);
                ak.setAccountKeeperName(fullName);
                ak.setEmailAddress(cust.getCustomer().getEmailAddress());
                ak.setEmailOptIn(cust.getCustomer().getEmailOptIn());
                //ak.setPhysicalAddressIdFk(paddr);
                LOGGER.info("baddrId=" + baddr.getAddressId());
                //ak.setBillingAddressIdFk(baddr);
                LOGGER.info("bid before merge=" + ak.getBillingAddressIdFk().getAddressId());
                em.merge(ak);
                
                LOGGER.info("bid=" + ak.getBillingAddressIdFk().getAddressId());
                LOGGER.info("pid=" + ak.getPhysicalAddressIdFk().getAddressId());
                
                Query query = em.createNativeQuery("update AccountKeeper set billingAddressIdFk=?1, physicalAddressIdFk=?2 where accountKeeperId=?3");
                query.setParameter(1, baddr.getAddressId());
                query.setParameter(2, ak.getPhysicalAddressIdFk().getAddressId());
                query.setParameter(3, ak.getAccountKeeperId());
                query.executeUpdate();
                
                LOGGER.info("bid after native query=" + ak.getBillingAddressIdFk().getAddressId());
                
            }

            utx.commit();
            

            RequestContext.getCurrentInstance().closeDialog("newCust");

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key;

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                //utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        } 

    }

    public void updateCustomer() {
        LOGGER.info("in updateCustomer, id=" + selectedCustomer.getAccountKeeperId());
        LOGGER.info("pid=" + selectedCustomer.getPhysicalAddressIdFk().getAddressId());

        try {
            /*
            AccountKeeper ak = claimMisc.getAccountKeeperForCustomer(fullName);
            if (ak != null) {  // update existing customer

                LOGGER.info("updating customer");
                ak.setPhysicalAddressIdFk(paddr);
                ak.setBillingAddressIdFk(baddr);
                ak.setEmailAddress(cust.getCustomer().getEmailAddress());
                ak.setEmailOptIn(cust.getCustomer().getEmailOptIn());
                em.merge(ak);

            }
             */
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                //utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

    }

    public void reset() {
        cust = new CustomerInfo();
        cust.reset();
        selectedCust = new CustomerInfo();
        selectedCust.reset();
        sameAsPaddr = true;
        customerId = null;
        /*
        customer = new AccountKeeper();
        paddr = new Address();
        baddr = new Address();
        sameAsPaddr = true;
        regionB = new CfRegion();
        regionP = new CfRegion();
        businessName = "";
        fullName = "";
         */
    }

    public void cancel() {
        LOGGER.info("in cancel");
        reset();
        RequestContext.getCurrentInstance().closeDialog("AddCustomerInfo");
    }

    public boolean sameAddr(Address oa, Address na) {
        boolean result = true;

        return result;
    }

}
