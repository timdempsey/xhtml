/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfDisbursementRemitRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfDisbursementRemitRule.findAll", query = "SELECT c FROM CfDisbursementRemitRule c")
    , @NamedQuery(name = "CfDisbursementRemitRule.findByDisbursementRemitRuleId", query = "SELECT c FROM CfDisbursementRemitRule c WHERE c.disbursementRemitRuleId = :disbursementRemitRuleId")
    , @NamedQuery(name = "CfDisbursementRemitRule.findByUpdateUserName", query = "SELECT c FROM CfDisbursementRemitRule c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfDisbursementRemitRule.findByUpdateLast", query = "SELECT c FROM CfDisbursementRemitRule c WHERE c.updateLast = :updateLast")})
public class CfDisbursementRemitRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementRemitRuleId")
    private Integer disbursementRemitRuleId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "remitRuleIdFk", referencedColumnName = "remitRuleId")
    @ManyToOne(optional = false)
    private CfRemitRule remitRuleIdFk;
    @JoinColumn(name = "disbursementGroupIdFk", referencedColumnName = "disbursementGroupId")
    @ManyToOne(optional = false)
    private DisbursementGroup disbursementGroupIdFk;
    @JoinColumn(name = "disbursementRemitRuleId", referencedColumnName = "rateBasedRuleGroupId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private RateBasedRuleGroup rateBasedRuleGroup;

    public CfDisbursementRemitRule() {
    }

    public CfDisbursementRemitRule(Integer disbursementRemitRuleId) {
        this.disbursementRemitRuleId = disbursementRemitRuleId;
    }

    public Integer getDisbursementRemitRuleId() {
        return disbursementRemitRuleId;
    }

    public void setDisbursementRemitRuleId(Integer disbursementRemitRuleId) {
        this.disbursementRemitRuleId = disbursementRemitRuleId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfRemitRule getRemitRuleIdFk() {
        return remitRuleIdFk;
    }

    public void setRemitRuleIdFk(CfRemitRule remitRuleIdFk) {
        this.remitRuleIdFk = remitRuleIdFk;
    }

    public DisbursementGroup getDisbursementGroupIdFk() {
        return disbursementGroupIdFk;
    }

    public void setDisbursementGroupIdFk(DisbursementGroup disbursementGroupIdFk) {
        this.disbursementGroupIdFk = disbursementGroupIdFk;
    }

    public RateBasedRuleGroup getRateBasedRuleGroup() {
        return rateBasedRuleGroup;
    }

    public void setRateBasedRuleGroup(RateBasedRuleGroup rateBasedRuleGroup) {
        this.rateBasedRuleGroup = rateBasedRuleGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementRemitRuleId != null ? disbursementRemitRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfDisbursementRemitRule)) {
            return false;
        }
        CfDisbursementRemitRule other = (CfDisbursementRemitRule) object;
        if ((this.disbursementRemitRuleId == null && other.disbursementRemitRuleId != null) || (this.disbursementRemitRuleId != null && !this.disbursementRemitRuleId.equals(other.disbursementRemitRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfDisbursementRemitRule[ disbursementRemitRuleId=" + disbursementRemitRuleId + " ]";
    }
    
}
