/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AccountKeeperBalance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountKeeperBalance.findAll", query = "SELECT a FROM AccountKeeperBalance a")
    , @NamedQuery(name = "AccountKeeperBalance.findByBalanceId", query = "SELECT a FROM AccountKeeperBalance a WHERE a.balanceId = :balanceId")
    , @NamedQuery(name = "AccountKeeperBalance.findByAdminCompanyIdFk", query = "SELECT a FROM AccountKeeperBalance a WHERE a.adminCompanyIdFk = :adminCompanyIdFk")
    , @NamedQuery(name = "AccountKeeperBalance.findByPostingHistoryIdFk", query = "SELECT a FROM AccountKeeperBalance a WHERE a.postingHistoryIdFk = :postingHistoryIdFk")
    , @NamedQuery(name = "AccountKeeperBalance.findByCurrentBalance", query = "SELECT a FROM AccountKeeperBalance a WHERE a.currentBalance = :currentBalance")
    , @NamedQuery(name = "AccountKeeperBalance.findByPreviousBalance", query = "SELECT a FROM AccountKeeperBalance a WHERE a.previousBalance = :previousBalance")
    , @NamedQuery(name = "AccountKeeperBalance.findByUpdateUserName", query = "SELECT a FROM AccountKeeperBalance a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "AccountKeeperBalance.findByUpdateLast", query = "SELECT a FROM AccountKeeperBalance a WHERE a.updateLast = :updateLast")})
public class AccountKeeperBalance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "balanceId")
    private Integer balanceId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminCompanyIdFk")
    private int adminCompanyIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postingHistoryIdFk")
    private int postingHistoryIdFk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CurrentBalance")
    private BigDecimal currentBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PreviousBalance")
    private BigDecimal previousBalance;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "AccountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne(optional = false)
    private AccountKeeper accountKeeperIdFk;

    public AccountKeeperBalance() {
    }

    public AccountKeeperBalance(Integer balanceId) {
        this.balanceId = balanceId;
    }

    public AccountKeeperBalance(Integer balanceId, int adminCompanyIdFk, int postingHistoryIdFk, BigDecimal currentBalance, BigDecimal previousBalance) {
        this.balanceId = balanceId;
        this.adminCompanyIdFk = adminCompanyIdFk;
        this.postingHistoryIdFk = postingHistoryIdFk;
        this.currentBalance = currentBalance;
        this.previousBalance = previousBalance;
    }

    public Integer getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Integer balanceId) {
        this.balanceId = balanceId;
    }

    public int getAdminCompanyIdFk() {
        return adminCompanyIdFk;
    }

    public void setAdminCompanyIdFk(int adminCompanyIdFk) {
        this.adminCompanyIdFk = adminCompanyIdFk;
    }

    public int getPostingHistoryIdFk() {
        return postingHistoryIdFk;
    }

    public void setPostingHistoryIdFk(int postingHistoryIdFk) {
        this.postingHistoryIdFk = postingHistoryIdFk;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (balanceId != null ? balanceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountKeeperBalance)) {
            return false;
        }
        AccountKeeperBalance other = (AccountKeeperBalance) object;
        if ((this.balanceId == null && other.balanceId != null) || (this.balanceId != null && !this.balanceId.equals(other.balanceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AccountKeeperBalance[ balanceId=" + balanceId + " ]";
    }
    
}
