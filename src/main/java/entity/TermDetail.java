/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "TermDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TermDetail.findAll", query = "SELECT t FROM TermDetail t")
    , @NamedQuery(name = "TermDetail.findByTermDetailId", query = "SELECT t FROM TermDetail t WHERE t.termDetailId = :termDetailId")
    , @NamedQuery(name = "TermDetail.findByEffectiveDate", query = "SELECT t FROM TermDetail t WHERE t.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "TermDetail.findByEnteredDate", query = "SELECT t FROM TermDetail t WHERE t.enteredDate = :enteredDate")
    , @NamedQuery(name = "TermDetail.findByDeletedInd", query = "SELECT t FROM TermDetail t WHERE t.deletedInd = :deletedInd")
    , @NamedQuery(name = "TermDetail.findByWaitDays", query = "SELECT t FROM TermDetail t WHERE t.waitDays = :waitDays")
    , @NamedQuery(name = "TermDetail.findByDescription", query = "SELECT t FROM TermDetail t WHERE t.description = :description")
    , @NamedQuery(name = "TermDetail.findByOtherDescription", query = "SELECT t FROM TermDetail t WHERE t.otherDescription = :otherDescription")
    , @NamedQuery(name = "TermDetail.findByVscExpireTime", query = "SELECT t FROM TermDetail t WHERE t.vscExpireTime = :vscExpireTime")
    , @NamedQuery(name = "TermDetail.findByVscExpireUsage", query = "SELECT t FROM TermDetail t WHERE t.vscExpireUsage = :vscExpireUsage")
    , @NamedQuery(name = "TermDetail.findByWaitMiles", query = "SELECT t FROM TermDetail t WHERE t.waitMiles = :waitMiles")
    , @NamedQuery(name = "TermDetail.findByAncMonths", query = "SELECT t FROM TermDetail t WHERE t.ancMonths = :ancMonths")
    , @NamedQuery(name = "TermDetail.findByGapUpperTime", query = "SELECT t FROM TermDetail t WHERE t.gapUpperTime = :gapUpperTime")
    , @NamedQuery(name = "TermDetail.findByGapLowerTime", query = "SELECT t FROM TermDetail t WHERE t.gapLowerTime = :gapLowerTime")
    , @NamedQuery(name = "TermDetail.findByPpmTime", query = "SELECT t FROM TermDetail t WHERE t.ppmTime = :ppmTime")
    , @NamedQuery(name = "TermDetail.findByPpmUsage", query = "SELECT t FROM TermDetail t WHERE t.ppmUsage = :ppmUsage")
    , @NamedQuery(name = "TermDetail.findByUpdateUserName", query = "SELECT t FROM TermDetail t WHERE t.updateUserName = :updateUserName")
    , @NamedQuery(name = "TermDetail.findByUpdateLast", query = "SELECT t FROM TermDetail t WHERE t.updateLast = :updateLast")})
public class TermDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "termDetailId")
    private Integer termDetailId;
    @Column(name = "EffectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Column(name = "waitDays")
    private Integer waitDays;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "otherDescription")
    private String otherDescription;
    @Column(name = "vscExpireTime")
    private Integer vscExpireTime;
    @Column(name = "vscExpireUsage")
    private Integer vscExpireUsage;
    @Column(name = "waitMiles")
    private Integer waitMiles;
    @Column(name = "ancMonths")
    private Integer ancMonths;
    @Column(name = "gapUpperTime")
    private Integer gapUpperTime;
    @Column(name = "gapLowerTime")
    private Integer gapLowerTime;
    @Column(name = "ppmTime")
    private Integer ppmTime;
    @Column(name = "ppmUsage")
    private Integer ppmUsage;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "productTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne
    private ProductType productTypeIdFk;
    @JoinColumn(name = "termIdFk", referencedColumnName = "TermId")
    @ManyToOne
    private Term termIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "termDetailIdFk")
    private Collection<PPMTermService> pPMTermServiceCollection;

    public TermDetail() {
    }

    public TermDetail(Integer termDetailId) {
        this.termDetailId = termDetailId;
    }

    public TermDetail(Integer termDetailId, boolean deletedInd) {
        this.termDetailId = termDetailId;
        this.deletedInd = deletedInd;
    }

    public Integer getTermDetailId() {
        return termDetailId;
    }

    public void setTermDetailId(Integer termDetailId) {
        this.termDetailId = termDetailId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Integer getWaitDays() {
        return waitDays;
    }

    public void setWaitDays(Integer waitDays) {
        this.waitDays = waitDays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOtherDescription() {
        return otherDescription;
    }

    public void setOtherDescription(String otherDescription) {
        this.otherDescription = otherDescription;
    }

    public Integer getVscExpireTime() {
        return vscExpireTime;
    }

    public void setVscExpireTime(Integer vscExpireTime) {
        this.vscExpireTime = vscExpireTime;
    }

    public Integer getVscExpireUsage() {
        return vscExpireUsage;
    }

    public void setVscExpireUsage(Integer vscExpireUsage) {
        this.vscExpireUsage = vscExpireUsage;
    }

    public Integer getWaitMiles() {
        return waitMiles;
    }

    public void setWaitMiles(Integer waitMiles) {
        this.waitMiles = waitMiles;
    }

    public Integer getAncMonths() {
        return ancMonths;
    }

    public void setAncMonths(Integer ancMonths) {
        this.ancMonths = ancMonths;
    }

    public Integer getGapUpperTime() {
        return gapUpperTime;
    }

    public void setGapUpperTime(Integer gapUpperTime) {
        this.gapUpperTime = gapUpperTime;
    }

    public Integer getGapLowerTime() {
        return gapLowerTime;
    }

    public void setGapLowerTime(Integer gapLowerTime) {
        this.gapLowerTime = gapLowerTime;
    }

    public Integer getPpmTime() {
        return ppmTime;
    }

    public void setPpmTime(Integer ppmTime) {
        this.ppmTime = ppmTime;
    }

    public Integer getPpmUsage() {
        return ppmUsage;
    }

    public void setPpmUsage(Integer ppmUsage) {
        this.ppmUsage = ppmUsage;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public Term getTermIdFk() {
        return termIdFk;
    }

    public void setTermIdFk(Term termIdFk) {
        this.termIdFk = termIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @XmlTransient
    public Collection<PPMTermService> getPPMTermServiceCollection() {
        return pPMTermServiceCollection;
    }

    public void setPPMTermServiceCollection(Collection<PPMTermService> pPMTermServiceCollection) {
        this.pPMTermServiceCollection = pPMTermServiceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (termDetailId != null ? termDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TermDetail)) {
            return false;
        }
        TermDetail other = (TermDetail) object;
        if ((this.termDetailId == null && other.termDetailId != null) || (this.termDetailId != null && !this.termDetailId.equals(other.termDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TermDetail[ termDetailId=" + termDetailId + " ]";
    }
    
}
