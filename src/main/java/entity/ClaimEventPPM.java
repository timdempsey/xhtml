/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimEventPPM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimEventPPM.findAll", query = "SELECT c FROM ClaimEventPPM c")
    , @NamedQuery(name = "ClaimEventPPM.findByClaimEventPPMId", query = "SELECT c FROM ClaimEventPPM c WHERE c.claimEventPPMId = :claimEventPPMId")
    , @NamedQuery(name = "ClaimEventPPM.findByCurrentHistoryIdFk", query = "SELECT c FROM ClaimEventPPM c WHERE c.currentHistoryIdFk = :currentHistoryIdFk")
    , @NamedQuery(name = "ClaimEventPPM.findByUpdateUserName", query = "SELECT c FROM ClaimEventPPM c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimEventPPM.findByUpdateLast", query = "SELECT c FROM ClaimEventPPM c WHERE c.updateLast = :updateLast")})
public class ClaimEventPPM implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimEventPPMId")
    private Integer claimEventPPMId;
    @Column(name = "currentHistoryIdFk")
    private Integer currentHistoryIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne(optional = false)
    private Claim claimIdFk;
    @JoinColumn(name = "contractPPMDetailIdFk", referencedColumnName = "contractPPMDetailId")
    @ManyToOne(optional = false)
    private ContractPPMDetail contractPPMDetailIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claimEventPPMIdFk")
    private Collection<ClaimEventPPMDetail> claimEventPPMDetailCollection;

    public ClaimEventPPM() {
    }

    public ClaimEventPPM(Integer claimEventPPMId) {
        this.claimEventPPMId = claimEventPPMId;
    }

    public Integer getClaimEventPPMId() {
        return claimEventPPMId;
    }

    public void setClaimEventPPMId(Integer claimEventPPMId) {
        this.claimEventPPMId = claimEventPPMId;
    }

    public Integer getCurrentHistoryIdFk() {
        return currentHistoryIdFk;
    }

    public void setCurrentHistoryIdFk(Integer currentHistoryIdFk) {
        this.currentHistoryIdFk = currentHistoryIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    public ContractPPMDetail getContractPPMDetailIdFk() {
        return contractPPMDetailIdFk;
    }

    public void setContractPPMDetailIdFk(ContractPPMDetail contractPPMDetailIdFk) {
        this.contractPPMDetailIdFk = contractPPMDetailIdFk;
    }

    @XmlTransient
    public Collection<ClaimEventPPMDetail> getClaimEventPPMDetailCollection() {
        return claimEventPPMDetailCollection;
    }

    public void setClaimEventPPMDetailCollection(Collection<ClaimEventPPMDetail> claimEventPPMDetailCollection) {
        this.claimEventPPMDetailCollection = claimEventPPMDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimEventPPMId != null ? claimEventPPMId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimEventPPM)) {
            return false;
        }
        ClaimEventPPM other = (ClaimEventPPM) object;
        if ((this.claimEventPPMId == null && other.claimEventPPMId != null) || (this.claimEventPPMId != null && !this.claimEventPPMId.equals(other.claimEventPPMId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimEventPPM[ claimEventPPMId=" + claimEventPPMId + " ]";
    }
    
}
