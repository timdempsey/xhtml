/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReportGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportGroup.findAll", query = "SELECT r FROM ReportGroup r")
    , @NamedQuery(name = "ReportGroup.findByReportGroupId", query = "SELECT r FROM ReportGroup r WHERE r.reportGroupId = :reportGroupId")
    , @NamedQuery(name = "ReportGroup.findByName", query = "SELECT r FROM ReportGroup r WHERE r.name = :name")
    , @NamedQuery(name = "ReportGroup.findByDescription", query = "SELECT r FROM ReportGroup r WHERE r.description = :description")
    , @NamedQuery(name = "ReportGroup.findByUpdateUserName", query = "SELECT r FROM ReportGroup r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReportGroup.findByUpdateLast", query = "SELECT r FROM ReportGroup r WHERE r.updateLast = :updateLast")})
public class ReportGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reportGroupId")
    private Integer reportGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reportGroupId")
    private Collection<ReportConnection> reportConnectionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reportGroupId")
    private Collection<ReportGroupUserConnection> reportGroupUserConnectionCollection;

    public ReportGroup() {
    }

    public ReportGroup(Integer reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public ReportGroup(Integer reportGroupId, String name) {
        this.reportGroupId = reportGroupId;
        this.name = name;
    }

    public Integer getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(Integer reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ReportConnection> getReportConnectionCollection() {
        return reportConnectionCollection;
    }

    public void setReportConnectionCollection(Collection<ReportConnection> reportConnectionCollection) {
        this.reportConnectionCollection = reportConnectionCollection;
    }

    @XmlTransient
    public Collection<ReportGroupUserConnection> getReportGroupUserConnectionCollection() {
        return reportGroupUserConnectionCollection;
    }

    public void setReportGroupUserConnectionCollection(Collection<ReportGroupUserConnection> reportGroupUserConnectionCollection) {
        this.reportGroupUserConnectionCollection = reportGroupUserConnectionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportGroupId != null ? reportGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportGroup)) {
            return false;
        }
        ReportGroup other = (ReportGroup) object;
        if ((this.reportGroupId == null && other.reportGroupId != null) || (this.reportGroupId != null && !this.reportGroupId.equals(other.reportGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReportGroup[ reportGroupId=" + reportGroupId + " ]";
    }
    
}
