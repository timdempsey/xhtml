/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ContractRating {

    private static final Logger LOGGER = LogManager.getLogger(ContractSearch.class);

    public ContractRating() {
    }

    public ContractRating(String vinNo, String dealerName, Integer vehicleClassCode, String planName, String programName, String productName, Integer programId, Integer productId, Integer planId, Integer ancMonths, Integer expTime, Integer expUsage, Integer ppmTime, Integer ppmUsage, Integer mileage, BigDecimal contractRate, Integer dealerId, BigDecimal contractReserve, BigDecimal contractSRP, BigDecimal contractCoverageCost, Integer lowerTime, Integer upperTime) {
        this.vinNo = vinNo;
        this.dealerName = dealerName;
        this.vehicleClassCode = vehicleClassCode;
        this.planName = planName;
        this.programName = programName;
        this.productName = productName;
        this.programId = programId;
        this.productId = productId;
        this.planId = planId;
        this.ancMonths = ancMonths;
        this.expTime = expTime;
        this.expUsage = expUsage;
        this.ppmTime = ppmTime;
        this.ppmUsage = ppmUsage;
        this.mileage = mileage;
        this.contractRate = contractRate;
        this.dealerId = dealerId;
        this.contractReserve = contractReserve;
        this.contractSRP = contractSRP;
        this.contractCoverageCost = contractCoverageCost;
        this.lowerTime = lowerTime;
        this.upperTime = upperTime;
    }

    

   
    private String vinNo;
    private String dealerName;
    private Integer vehicleClassCode;
    private String planName;
    private String programName;
    private String productName;
    private Integer programId;
    private Integer productId;
    private Integer planId;
    private Integer ancMonths;
    private Integer expTime; 
    private Integer expUsage;
    private Integer ppmTime;
    private Integer ppmUsage;
    private Integer mileage;
    private BigDecimal contractRate;
    private Integer dealerId;
    private BigDecimal contractReserve;
    private BigDecimal contractSRP;
    private BigDecimal contractCoverageCost;    
    private Integer lowerTime;
    private Integer upperTime;

   

    public void reset() {
        vinNo = "";
        dealerName = "";
        programName = "";
        programId = null;
        mileage = null;
        resetProductPlan();
        dealerId = null;
    }

    public void resetProductPlan() {
        vehicleClassCode = null;
        planName = "";
        planId = null;
        productName = "";
        productId = null;
        ancMonths = null;
        expTime = null;
        expUsage = null;
        ppmTime = null;
        ppmUsage = null;
        contractRate = new BigDecimal(0);
        contractReserve = null;
        contractSRP = null;
        contractCoverageCost = null;
        lowerTime = null;
        upperTime = null;
    }

    public Integer getLowerTime() {
        return lowerTime;
    }

    public void setLowerTime(Integer lowerTime) {
        this.lowerTime = lowerTime;
    }

    public Integer getUpperTime() {
        return upperTime;
    }

    public void setUpperTime(Integer upperTime) {
        this.upperTime = upperTime;
    }
    
    
    
     public Integer getVehicleClassCode() {
        return vehicleClassCode;
    }

    public void setVehicleClassCode(Integer vehicleClassCode) {
        this.vehicleClassCode = vehicleClassCode;
    }

    public String getVinNo() {
        return vinNo;
    }

    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getAncMonths() {
        return ancMonths;
    }

    public void setAncMonths(Integer ancMonths) {
        this.ancMonths = ancMonths;
    }

    public Integer getExpTime() {
        return expTime;
    }

    public void setExpTime(Integer expTime) {
        this.expTime = expTime;
    }

    public Integer getExpUsage() {
        return expUsage;
    }

    public void setExpUsage(Integer expUsage) {
        this.expUsage = expUsage;
    }

    public Integer getPpmTime() {
        return ppmTime;
    }

    public void setPpmTime(Integer ppmTime) {
        this.ppmTime = ppmTime;
    }

    public Integer getPpmUsage() {
        return ppmUsage;
    }

    public void setPpmUsage(Integer ppmUsage) {
        this.ppmUsage = ppmUsage;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getContractRate() {
        return contractRate;
    }

    public void setContractRate(BigDecimal contractRate) {
        this.contractRate = contractRate;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public BigDecimal getContractReserve() {
        return contractReserve;
    }

    public void setContractReserve(BigDecimal contractReserve) {
        this.contractReserve = contractReserve;
    }

    public BigDecimal getContractSRP() {
        return contractSRP;
    }

    public void setContractSRP(BigDecimal contractSRP) {
        this.contractSRP = contractSRP;
    }

    public BigDecimal getContractCoverageCost() {
        return contractCoverageCost;
    }

    public void setContractCoverageCost(BigDecimal contractCoverageCost) {
        this.contractCoverageCost = contractCoverageCost;
    }
    
    
    

}
