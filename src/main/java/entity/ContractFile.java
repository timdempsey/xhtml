/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractFile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractFile.findAll", query = "SELECT c FROM ContractFile c")
    , @NamedQuery(name = "ContractFile.findByContractFileId", query = "SELECT c FROM ContractFile c WHERE c.contractFileId = :contractFileId")
    , @NamedQuery(name = "ContractFile.findByContractFileTypeInd", query = "SELECT c FROM ContractFile c WHERE c.contractFileTypeInd = :contractFileTypeInd")})
public class ContractFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractFileId")
    private Integer contractFileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractFileTypeInd")
    private int contractFileTypeInd;
    @JoinColumn(name = "contractFileId", referencedColumnName = "fileId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private CfFile cfFile;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne(optional = false)
    private Contracts contractIdFk;

    public ContractFile() {
    }

    public ContractFile(Integer contractFileId) {
        this.contractFileId = contractFileId;
    }

    public ContractFile(Integer contractFileId, int contractFileTypeInd) {
        this.contractFileId = contractFileId;
        this.contractFileTypeInd = contractFileTypeInd;
    }

    public Integer getContractFileId() {
        return contractFileId;
    }

    public void setContractFileId(Integer contractFileId) {
        this.contractFileId = contractFileId;
    }

    public int getContractFileTypeInd() {
        return contractFileTypeInd;
    }

    public void setContractFileTypeInd(int contractFileTypeInd) {
        this.contractFileTypeInd = contractFileTypeInd;
    }

    public CfFile getCfFile() {
        return cfFile;
    }

    public void setCfFile(CfFile cfFile) {
        this.cfFile = cfFile;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractFileId != null ? contractFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractFile)) {
            return false;
        }
        ContractFile other = (ContractFile) object;
        if ((this.contractFileId == null && other.contractFileId != null) || (this.contractFileId != null && !this.contractFileId.equals(other.contractFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractFile[ contractFileId=" + contractFileId + " ]";
    }
    
}
