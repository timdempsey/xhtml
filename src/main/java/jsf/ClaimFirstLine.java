/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author Jiepi
 */

public class ClaimFirstLine implements Serializable{

    //private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class.getName());
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);
    /**
     * Creates a new instance of ClaimFirstLine
     */

    private String product;
    private String claimNumber;
    private String assignedTo;
    private String inceptionDate;
    private String claimStatus;
    private String claimSubStatus;
    private String contractNo;
    private String contractStatus;
    private String customer;
    private String paidTo;
    private Float amount;
    
    public ClaimFirstLine() {
        
    }

    //private ClaimFirstLine claimFirstLineBean;
    public ClaimFirstLine(String product, String claimNumber, String assignedTo, String inceptionDate, String claimStatus, String claimSubStatus, String contractNo, String contractStatus, String customer, String paidTo, Float amount) {
        this.product = product;
        this.claimNumber = claimNumber;
        this.assignedTo = assignedTo;
        this.inceptionDate = inceptionDate;
        this.claimStatus = claimStatus;
        this.claimSubStatus = claimSubStatus;
        this.contractNo = contractNo;
        this.contractStatus = contractStatus;
        this.customer = customer;
        this.paidTo = paidTo;
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(String InceptionDate) {
        this.inceptionDate = InceptionDate;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public String getClaimSubStatus() {
        return claimSubStatus;
    }

    public void setClaimSubStatus(String claimSubStatus) {
        this.claimSubStatus = claimSubStatus;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(String paidTo) {
        this.paidTo = paidTo;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }


    
}
