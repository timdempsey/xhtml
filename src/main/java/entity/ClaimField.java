/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimField")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimField.findAll", query = "SELECT c FROM ClaimField c")
    , @NamedQuery(name = "ClaimField.findByClaimFieldId", query = "SELECT c FROM ClaimField c WHERE c.claimFieldId = :claimFieldId")
    , @NamedQuery(name = "ClaimField.findByNumberValue", query = "SELECT c FROM ClaimField c WHERE c.numberValue = :numberValue")
    , @NamedQuery(name = "ClaimField.findByStringValue", query = "SELECT c FROM ClaimField c WHERE c.stringValue = :stringValue")
    , @NamedQuery(name = "ClaimField.findByDateValue", query = "SELECT c FROM ClaimField c WHERE c.dateValue = :dateValue")
    , @NamedQuery(name = "ClaimField.findByRegionIdFk", query = "SELECT c FROM ClaimField c WHERE c.regionIdFk = :regionIdFk")
    , @NamedQuery(name = "ClaimField.findByUpdateUserName", query = "SELECT c FROM ClaimField c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimField.findByUpdateLast", query = "SELECT c FROM ClaimField c WHERE c.updateLast = :updateLast")})
public class ClaimField implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ClaimFieldId")
    private Integer claimFieldId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "numberValue")
    private BigDecimal numberValue;
    @Size(max = 200)
    @Column(name = "stringValue")
    private String stringValue;
    @Column(name = "dateValue")
    @Temporal(TemporalType.DATE)
    private Date dateValue;
    @Column(name = "regionIdFk")
    private Integer regionIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "fieldIdFk", referencedColumnName = "FieldId")
    @ManyToOne(optional = false)
    private CfField fieldIdFk;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne(optional = false)
    private Claim claimIdFk;

    public ClaimField() {
    }

    public ClaimField(Integer claimFieldId) {
        this.claimFieldId = claimFieldId;
    }

    public Integer getClaimFieldId() {
        return claimFieldId;
    }

    public void setClaimFieldId(Integer claimFieldId) {
        this.claimFieldId = claimFieldId;
    }

    public BigDecimal getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(BigDecimal numberValue) {
        this.numberValue = numberValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Integer getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(Integer regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfField getFieldIdFk() {
        return fieldIdFk;
    }

    public void setFieldIdFk(CfField fieldIdFk) {
        this.fieldIdFk = fieldIdFk;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimFieldId != null ? claimFieldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimField)) {
            return false;
        }
        ClaimField other = (ClaimField) object;
        if ((this.claimFieldId == null && other.claimFieldId != null) || (this.claimFieldId != null && !this.claimFieldId.equals(other.claimFieldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimField[ claimFieldId=" + claimFieldId + " ]";
    }
    
}
