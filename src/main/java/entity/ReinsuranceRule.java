/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceRule.findAll", query = "SELECT r FROM ReinsuranceRule r")
    , @NamedQuery(name = "ReinsuranceRule.findByReinsuranceRuleId", query = "SELECT r FROM ReinsuranceRule r WHERE r.reinsuranceRuleId = :reinsuranceRuleId")
    , @NamedQuery(name = "ReinsuranceRule.findByDescription", query = "SELECT r FROM ReinsuranceRule r WHERE r.description = :description")
    , @NamedQuery(name = "ReinsuranceRule.findByUpdateUserName", query = "SELECT r FROM ReinsuranceRule r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceRule.findByUpdateLast", query = "SELECT r FROM ReinsuranceRule r WHERE r.updateLast = :updateLast")})
public class ReinsuranceRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceRuleId")
    private Integer reinsuranceRuleId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "reinsuranceRuleIdFk")
    private Collection<ReinsuranceReinsurer> reinsuranceReinsurerCollection;
    @OneToMany(mappedBy = "reinsuranceRuleIdFk")
    private Collection<ReinsuranceDealer> reinsuranceDealerCollection;
    @OneToMany(mappedBy = "reinsuranceRuleIdFk")
    private Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection;

    public ReinsuranceRule() {
    }

    public ReinsuranceRule(Integer reinsuranceRuleId) {
        this.reinsuranceRuleId = reinsuranceRuleId;
    }

    public Integer getReinsuranceRuleId() {
        return reinsuranceRuleId;
    }

    public void setReinsuranceRuleId(Integer reinsuranceRuleId) {
        this.reinsuranceRuleId = reinsuranceRuleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ReinsuranceReinsurer> getReinsuranceReinsurerCollection() {
        return reinsuranceReinsurerCollection;
    }

    public void setReinsuranceReinsurerCollection(Collection<ReinsuranceReinsurer> reinsuranceReinsurerCollection) {
        this.reinsuranceReinsurerCollection = reinsuranceReinsurerCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceDealer> getReinsuranceDealerCollection() {
        return reinsuranceDealerCollection;
    }

    public void setReinsuranceDealerCollection(Collection<ReinsuranceDealer> reinsuranceDealerCollection) {
        this.reinsuranceDealerCollection = reinsuranceDealerCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceRuleDetail> getReinsuranceRuleDetailCollection() {
        return reinsuranceRuleDetailCollection;
    }

    public void setReinsuranceRuleDetailCollection(Collection<ReinsuranceRuleDetail> reinsuranceRuleDetailCollection) {
        this.reinsuranceRuleDetailCollection = reinsuranceRuleDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceRuleId != null ? reinsuranceRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceRule)) {
            return false;
        }
        ReinsuranceRule other = (ReinsuranceRule) object;
        if ((this.reinsuranceRuleId == null && other.reinsuranceRuleId != null) || (this.reinsuranceRuleId != null && !this.reinsuranceRuleId.equals(other.reinsuranceRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceRule[ reinsuranceRuleId=" + reinsuranceRuleId + " ]";
    }
    
}
