/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateWizardUploadLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateWizardUploadLog.findAll", query = "SELECT r FROM RateWizardUploadLog r")
    , @NamedQuery(name = "RateWizardUploadLog.findByRateWizardUploadLogId", query = "SELECT r FROM RateWizardUploadLog r WHERE r.rateWizardUploadLogId = :rateWizardUploadLogId")
    , @NamedQuery(name = "RateWizardUploadLog.findByLog", query = "SELECT r FROM RateWizardUploadLog r WHERE r.log = :log")
    , @NamedQuery(name = "RateWizardUploadLog.findByUpdateUserName", query = "SELECT r FROM RateWizardUploadLog r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateWizardUploadLog.findByUpdateLast", query = "SELECT r FROM RateWizardUploadLog r WHERE r.updateLast = :updateLast")})
public class RateWizardUploadLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateWizardUploadLogId")
    private Integer rateWizardUploadLogId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "log")
    private String log;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "rateWizardUploadIdFk", referencedColumnName = "RateWizardUploadId")
    @ManyToOne(optional = false)
    private RateWizardUpload rateWizardUploadIdFk;

    public RateWizardUploadLog() {
    }

    public RateWizardUploadLog(Integer rateWizardUploadLogId) {
        this.rateWizardUploadLogId = rateWizardUploadLogId;
    }

    public RateWizardUploadLog(Integer rateWizardUploadLogId, String log) {
        this.rateWizardUploadLogId = rateWizardUploadLogId;
        this.log = log;
    }

    public Integer getRateWizardUploadLogId() {
        return rateWizardUploadLogId;
    }

    public void setRateWizardUploadLogId(Integer rateWizardUploadLogId) {
        this.rateWizardUploadLogId = rateWizardUploadLogId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public RateWizardUpload getRateWizardUploadIdFk() {
        return rateWizardUploadIdFk;
    }

    public void setRateWizardUploadIdFk(RateWizardUpload rateWizardUploadIdFk) {
        this.rateWizardUploadIdFk = rateWizardUploadIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateWizardUploadLogId != null ? rateWizardUploadLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateWizardUploadLog)) {
            return false;
        }
        RateWizardUploadLog other = (RateWizardUploadLog) object;
        if ((this.rateWizardUploadLogId == null && other.rateWizardUploadLogId != null) || (this.rateWizardUploadLogId != null && !this.rateWizardUploadLogId.equals(other.rateWizardUploadLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateWizardUploadLog[ rateWizardUploadLogId=" + rateWizardUploadLogId + " ]";
    }
    
}
