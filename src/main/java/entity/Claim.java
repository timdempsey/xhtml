/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Claim")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Claim.findAll", query = "SELECT c FROM Claim c")
    , @NamedQuery(name = "Claim.findByClaimId", query = "SELECT c FROM Claim c WHERE c.claimId = :claimId")
    , @NamedQuery(name = "Claim.findByClaimNumber", query = "SELECT c FROM Claim c WHERE c.claimNumber = :claimNumber")
    , @NamedQuery(name = "Claim.findByInceptionDate", query = "SELECT c FROM Claim c WHERE c.inceptionDate = :inceptionDate")
    , @NamedQuery(name = "Claim.findByAmount", query = "SELECT c FROM Claim c WHERE c.amount = :amount")
    , @NamedQuery(name = "Claim.findByReason", query = "SELECT c FROM Claim c WHERE c.reason = :reason")
    , @NamedQuery(name = "Claim.findByCurrentOdometer", query = "SELECT c FROM Claim c WHERE c.currentOdometer = :currentOdometer")
    , @NamedQuery(name = "Claim.findByClaimStartOdometer", query = "SELECT c FROM Claim c WHERE c.claimStartOdometer = :claimStartOdometer")
    , @NamedQuery(name = "Claim.findByClaimExpirationOdometer", query = "SELECT c FROM Claim c WHERE c.claimExpirationOdometer = :claimExpirationOdometer")
    , @NamedQuery(name = "Claim.findByAssignedTo", query = "SELECT c FROM Claim c WHERE c.assignedTo = :assignedTo")
    , @NamedQuery(name = "Claim.findByRepairOrderNumber", query = "SELECT c FROM Claim c WHERE c.repairOrderNumber = :repairOrderNumber")
    , @NamedQuery(name = "Claim.findByRepairOrderDate", query = "SELECT c FROM Claim c WHERE c.repairOrderDate = :repairOrderDate")
    , @NamedQuery(name = "Claim.findByTpaApprovalCode", query = "SELECT c FROM Claim c WHERE c.tpaApprovalCode = :tpaApprovalCode")
    , @NamedQuery(name = "Claim.findByManualDeductible", query = "SELECT c FROM Claim c WHERE c.manualDeductible = :manualDeductible")
    , @NamedQuery(name = "Claim.findByDeductibleAmt", query = "SELECT c FROM Claim c WHERE c.deductibleAmt = :deductibleAmt")
    , @NamedQuery(name = "Claim.findByFullDeductibleAmt", query = "SELECT c FROM Claim c WHERE c.fullDeductibleAmt = :fullDeductibleAmt")
    , @NamedQuery(name = "Claim.findByLaborTaxRate", query = "SELECT c FROM Claim c WHERE c.laborTaxRate = :laborTaxRate")
    , @NamedQuery(name = "Claim.findByPartTaxRate", query = "SELECT c FROM Claim c WHERE c.partTaxRate = :partTaxRate")
    , @NamedQuery(name = "Claim.findByLaborRate", query = "SELECT c FROM Claim c WHERE c.laborRate = :laborRate")
    , @NamedQuery(name = "Claim.findByPartWarrantyMonths", query = "SELECT c FROM Claim c WHERE c.partWarrantyMonths = :partWarrantyMonths")
    , @NamedQuery(name = "Claim.findByPartWarrantyMiles", query = "SELECT c FROM Claim c WHERE c.partWarrantyMiles = :partWarrantyMiles")
    , @NamedQuery(name = "Claim.findByUpdateUserName", query = "SELECT c FROM Claim c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "Claim.findByUpdateLast", query = "SELECT c FROM Claim c WHERE c.updateLast = :updateLast")})
public class Claim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimId")
    private Integer claimId;
    @Size(max = 30)
    @Column(name = "claimNumber")
    private String claimNumber;
    @Column(name = "inceptionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inceptionDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @Size(max = 1024)
    @Column(name = "reason")
    private String reason;
    @Column(name = "currentOdometer")
    private Integer currentOdometer;
    @Size(max = 10)
    @Column(name = "claimStartOdometer")
    private String claimStartOdometer;
    @Size(max = 10)
    @Column(name = "claimExpirationOdometer")
    private String claimExpirationOdometer;
    @Size(max = 30)
    @Column(name = "assignedTo")
    private String assignedTo;
    @Size(max = 50)
    @Column(name = "repairOrderNumber")
    private String repairOrderNumber;
    @Column(name = "repairOrderDate")
    @Temporal(TemporalType.DATE)
    private Date repairOrderDate;
    @Size(max = 20)
    @Column(name = "tpaApprovalCode")
    private String tpaApprovalCode;
    @Column(name = "manualDeductible")
    private Boolean manualDeductible;
    @Column(name = "deductibleAmt")
    private BigDecimal deductibleAmt;
    @Column(name = "fullDeductibleAmt")
    private BigDecimal fullDeductibleAmt;
    @Column(name = "laborTaxRate")
    private BigDecimal laborTaxRate;
    @Column(name = "partTaxRate")
    private BigDecimal partTaxRate;
    @Column(name = "laborRate")
    private BigDecimal laborRate;
    @Column(name = "partWarrantyMonths")
    private Integer partWarrantyMonths;
    @Column(name = "partWarrantyMiles")
    private Integer partWarrantyMiles;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<Note> noteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claimIdFk")
    private Collection<ClaimEventPPM> claimEventPPMCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimDetail> claimDetailCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimMiscPayment> claimMiscPaymentCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimFile> claimFileCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claimIdFk")
    private Collection<ClaimField> claimFieldCollection;
    @OneToMany(mappedBy = "relatedClaimIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<HistoryLog> historyLogCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimPaymentAuthorization> claimPaymentAuthorizationCollection;
    @OneToMany(mappedBy = "claimIdFk")
    private Collection<ClaimViewLog> claimViewLogCollection;
    @JoinColumn(name = "paidToFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper paidToFk;
    @JoinColumn(name = "claimStatus", referencedColumnName = "claimStatusInd")
    @ManyToOne
    private ClaimStatus claimStatus;
    @JoinColumn(name = "claimSubStatus", referencedColumnName = "claimSubstatusInd")
    @ManyToOne
    private ClaimSubstatus claimSubStatus;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "productTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne
    private ProductType productTypeIdFk;
    @JoinColumn(name = "repairFacilityIdFk", referencedColumnName = "repairFacilityId")
    @ManyToOne
    private RepairFacility repairFacilityIdFk;
    @JoinColumn(name = "repairFacilityContactIdFk", referencedColumnName = "repairFacilityContactPersonId")
    @ManyToOne
    private RepairFacilityContactPerson repairFacilityContactIdFk;
    @JoinColumn(name = "enteredIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredIdFk;
    @JoinColumn(name = "ownedByFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember ownedByFk;

    public Claim() {
    }

    public Claim(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Date getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(Date inceptionDate) {
        this.inceptionDate = inceptionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getCurrentOdometer() {
        return currentOdometer;
    }

    public void setCurrentOdometer(Integer currentOdometer) {
        this.currentOdometer = currentOdometer;
    }

    public String getClaimStartOdometer() {
        return claimStartOdometer;
    }

    public void setClaimStartOdometer(String claimStartOdometer) {
        this.claimStartOdometer = claimStartOdometer;
    }

    public String getClaimExpirationOdometer() {
        return claimExpirationOdometer;
    }

    public void setClaimExpirationOdometer(String claimExpirationOdometer) {
        this.claimExpirationOdometer = claimExpirationOdometer;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getRepairOrderNumber() {
        return repairOrderNumber;
    }

    public void setRepairOrderNumber(String repairOrderNumber) {
        this.repairOrderNumber = repairOrderNumber;
    }

    public Date getRepairOrderDate() {
        return repairOrderDate;
    }

    public void setRepairOrderDate(Date repairOrderDate) {
        this.repairOrderDate = repairOrderDate;
    }

    public String getTpaApprovalCode() {
        return tpaApprovalCode;
    }

    public void setTpaApprovalCode(String tpaApprovalCode) {
        this.tpaApprovalCode = tpaApprovalCode;
    }

    public Boolean getManualDeductible() {
        return manualDeductible;
    }

    public void setManualDeductible(Boolean manualDeductible) {
        this.manualDeductible = manualDeductible;
    }

    public BigDecimal getDeductibleAmt() {
        return deductibleAmt;
    }

    public void setDeductibleAmt(BigDecimal deductibleAmt) {
        this.deductibleAmt = deductibleAmt;
    }

    public BigDecimal getFullDeductibleAmt() {
        return fullDeductibleAmt;
    }

    public void setFullDeductibleAmt(BigDecimal fullDeductibleAmt) {
        this.fullDeductibleAmt = fullDeductibleAmt;
    }

    public BigDecimal getLaborTaxRate() {
        return laborTaxRate;
    }

    public void setLaborTaxRate(BigDecimal laborTaxRate) {
        this.laborTaxRate = laborTaxRate;
    }

    public BigDecimal getPartTaxRate() {
        return partTaxRate;
    }

    public void setPartTaxRate(BigDecimal partTaxRate) {
        this.partTaxRate = partTaxRate;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getPartWarrantyMonths() {
        return partWarrantyMonths;
    }

    public void setPartWarrantyMonths(Integer partWarrantyMonths) {
        this.partWarrantyMonths = partWarrantyMonths;
    }

    public Integer getPartWarrantyMiles() {
        return partWarrantyMiles;
    }

    public void setPartWarrantyMiles(Integer partWarrantyMiles) {
        this.partWarrantyMiles = partWarrantyMiles;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Note> getNoteCollection() {
        return noteCollection;
    }

    public void setNoteCollection(Collection<Note> noteCollection) {
        this.noteCollection = noteCollection;
    }

    @XmlTransient
    public Collection<ClaimEventPPM> getClaimEventPPMCollection() {
        return claimEventPPMCollection;
    }

    public void setClaimEventPPMCollection(Collection<ClaimEventPPM> claimEventPPMCollection) {
        this.claimEventPPMCollection = claimEventPPMCollection;
    }

    @XmlTransient
    public Collection<ClaimDetail> getClaimDetailCollection() {
        return claimDetailCollection;
    }

    public void setClaimDetailCollection(Collection<ClaimDetail> claimDetailCollection) {
        this.claimDetailCollection = claimDetailCollection;
    }

    @XmlTransient
    public Collection<ClaimMiscPayment> getClaimMiscPaymentCollection() {
        return claimMiscPaymentCollection;
    }

    public void setClaimMiscPaymentCollection(Collection<ClaimMiscPayment> claimMiscPaymentCollection) {
        this.claimMiscPaymentCollection = claimMiscPaymentCollection;
    }

    @XmlTransient
    public Collection<ClaimFile> getClaimFileCollection() {
        return claimFileCollection;
    }

    public void setClaimFileCollection(Collection<ClaimFile> claimFileCollection) {
        this.claimFileCollection = claimFileCollection;
    }

    @XmlTransient
    public Collection<ClaimField> getClaimFieldCollection() {
        return claimFieldCollection;
    }

    public void setClaimFieldCollection(Collection<ClaimField> claimFieldCollection) {
        this.claimFieldCollection = claimFieldCollection;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<HistoryLog> getHistoryLogCollection() {
        return historyLogCollection;
    }

    public void setHistoryLogCollection(Collection<HistoryLog> historyLogCollection) {
        this.historyLogCollection = historyLogCollection;
    }

    @XmlTransient
    public Collection<ClaimPaymentAuthorization> getClaimPaymentAuthorizationCollection() {
        return claimPaymentAuthorizationCollection;
    }

    public void setClaimPaymentAuthorizationCollection(Collection<ClaimPaymentAuthorization> claimPaymentAuthorizationCollection) {
        this.claimPaymentAuthorizationCollection = claimPaymentAuthorizationCollection;
    }

    @XmlTransient
    public Collection<ClaimViewLog> getClaimViewLogCollection() {
        return claimViewLogCollection;
    }

    public void setClaimViewLogCollection(Collection<ClaimViewLog> claimViewLogCollection) {
        this.claimViewLogCollection = claimViewLogCollection;
    }

    public AccountKeeper getPaidToFk() {
        return paidToFk;
    }

    public void setPaidToFk(AccountKeeper paidToFk) {
        this.paidToFk = paidToFk;
    }

    public ClaimStatus getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(ClaimStatus claimStatus) {
        this.claimStatus = claimStatus;
    }

    public ClaimSubstatus getClaimSubStatus() {
        return claimSubStatus;
    }

    public void setClaimSubStatus(ClaimSubstatus claimSubStatus) {
        this.claimSubStatus = claimSubStatus;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public RepairFacility getRepairFacilityIdFk() {
        return repairFacilityIdFk;
    }

    public void setRepairFacilityIdFk(RepairFacility repairFacilityIdFk) {
        this.repairFacilityIdFk = repairFacilityIdFk;
    }

    public RepairFacilityContactPerson getRepairFacilityContactIdFk() {
        return repairFacilityContactIdFk;
    }

    public void setRepairFacilityContactIdFk(RepairFacilityContactPerson repairFacilityContactIdFk) {
        this.repairFacilityContactIdFk = repairFacilityContactIdFk;
    }

    public UserMember getEnteredIdFk() {
        return enteredIdFk;
    }

    public void setEnteredIdFk(UserMember enteredIdFk) {
        this.enteredIdFk = enteredIdFk;
    }

    public UserMember getOwnedByFk() {
        return ownedByFk;
    }

    public void setOwnedByFk(UserMember ownedByFk) {
        this.ownedByFk = ownedByFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimId != null ? claimId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Claim)) {
            return false;
        }
        Claim other = (Claim) object;
        if ((this.claimId == null && other.claimId != null) || (this.claimId != null && !this.claimId.equals(other.claimId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Claim[ claimId=" + claimId + " ]";
    }
    
}
