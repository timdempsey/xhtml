/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PlanDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanDetail.findAll", query = "SELECT p FROM PlanDetail p")
    , @NamedQuery(name = "PlanDetail.findByPlanDetailId", query = "SELECT p FROM PlanDetail p WHERE p.planDetailId = :planDetailId")
    , @NamedQuery(name = "PlanDetail.findByPurchaseTypeInd", query = "SELECT p FROM PlanDetail p WHERE p.purchaseTypeInd = :purchaseTypeInd")
    , @NamedQuery(name = "PlanDetail.findByAvailableVehiclePurchaseTypes", query = "SELECT p FROM PlanDetail p WHERE p.availableVehiclePurchaseTypes = :availableVehiclePurchaseTypes")
    , @NamedQuery(name = "PlanDetail.findByWaitDays", query = "SELECT p FROM PlanDetail p WHERE p.waitDays = :waitDays")
    , @NamedQuery(name = "PlanDetail.findByEarningMethodInd", query = "SELECT p FROM PlanDetail p WHERE p.earningMethodInd = :earningMethodInd")
    , @NamedQuery(name = "PlanDetail.findByEffectiveDate", query = "SELECT p FROM PlanDetail p WHERE p.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "PlanDetail.findByExpireDate", query = "SELECT p FROM PlanDetail p WHERE p.expireDate = :expireDate")
    , @NamedQuery(name = "PlanDetail.findByEnteredDate", query = "SELECT p FROM PlanDetail p WHERE p.enteredDate = :enteredDate")
    , @NamedQuery(name = "PlanDetail.findByDeletedInd", query = "SELECT p FROM PlanDetail p WHERE p.deletedInd = :deletedInd")
    , @NamedQuery(name = "PlanDetail.findByDefaultTransferFee", query = "SELECT p FROM PlanDetail p WHERE p.defaultTransferFee = :defaultTransferFee")
    , @NamedQuery(name = "PlanDetail.findByReimbursementAmount", query = "SELECT p FROM PlanDetail p WHERE p.reimbursementAmount = :reimbursementAmount")
    , @NamedQuery(name = "PlanDetail.findByPlanExpireTimeTypeInd", query = "SELECT p FROM PlanDetail p WHERE p.planExpireTimeTypeInd = :planExpireTimeTypeInd")
    , @NamedQuery(name = "PlanDetail.findByCancelRequestValidDays", query = "SELECT p FROM PlanDetail p WHERE p.cancelRequestValidDays = :cancelRequestValidDays")
    , @NamedQuery(name = "PlanDetail.findByCancelRequestValidMiles", query = "SELECT p FROM PlanDetail p WHERE p.cancelRequestValidMiles = :cancelRequestValidMiles")
    , @NamedQuery(name = "PlanDetail.findByExtendExpirationByWait", query = "SELECT p FROM PlanDetail p WHERE p.extendExpirationByWait = :extendExpirationByWait")
    , @NamedQuery(name = "PlanDetail.findByDescription", query = "SELECT p FROM PlanDetail p WHERE p.description = :description")
    , @NamedQuery(name = "PlanDetail.findByCancelFeeDisbursementTypeInd", query = "SELECT p FROM PlanDetail p WHERE p.cancelFeeDisbursementTypeInd = :cancelFeeDisbursementTypeInd")
    , @NamedQuery(name = "PlanDetail.findByMinimumTermMonths", query = "SELECT p FROM PlanDetail p WHERE p.minimumTermMonths = :minimumTermMonths")
    , @NamedQuery(name = "PlanDetail.findByMaxFinanceOfMSRPandNADA", query = "SELECT p FROM PlanDetail p WHERE p.maxFinanceOfMSRPandNADA = :maxFinanceOfMSRPandNADA")
    , @NamedQuery(name = "PlanDetail.findByLimitDeductibleDefault", query = "SELECT p FROM PlanDetail p WHERE p.limitDeductibleDefault = :limitDeductibleDefault")
    , @NamedQuery(name = "PlanDetail.findByWaitMiles", query = "SELECT p FROM PlanDetail p WHERE p.waitMiles = :waitMiles")
    , @NamedQuery(name = "PlanDetail.findByVscMinimumTermMiles", query = "SELECT p FROM PlanDetail p WHERE p.vscMinimumTermMiles = :vscMinimumTermMiles")
    , @NamedQuery(name = "PlanDetail.findByVscPlanExpireUsageTypeInd", query = "SELECT p FROM PlanDetail p WHERE p.vscPlanExpireUsageTypeInd = :vscPlanExpireUsageTypeInd")
    , @NamedQuery(name = "PlanDetail.findByPpmScheduleStartInd", query = "SELECT p FROM PlanDetail p WHERE p.ppmScheduleStartInd = :ppmScheduleStartInd")
    , @NamedQuery(name = "PlanDetail.findByGapMaxTermMonths", query = "SELECT p FROM PlanDetail p WHERE p.gapMaxTermMonths = :gapMaxTermMonths")
    , @NamedQuery(name = "PlanDetail.findByGapMaxFinanceOfMSRP", query = "SELECT p FROM PlanDetail p WHERE p.gapMaxFinanceOfMSRP = :gapMaxFinanceOfMSRP")
    , @NamedQuery(name = "PlanDetail.findByGapAvailableFinanceTypes", query = "SELECT p FROM PlanDetail p WHERE p.gapAvailableFinanceTypes = :gapAvailableFinanceTypes")
    , @NamedQuery(name = "PlanDetail.findByAncReimbursementAmount", query = "SELECT p FROM PlanDetail p WHERE p.ancReimbursementAmount = :ancReimbursementAmount")
    , @NamedQuery(name = "PlanDetail.findByAncAvailableFinanceTypes", query = "SELECT p FROM PlanDetail p WHERE p.ancAvailableFinanceTypes = :ancAvailableFinanceTypes")
    , @NamedQuery(name = "PlanDetail.findByUpdateUserName", query = "SELECT p FROM PlanDetail p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PlanDetail.findByUpdateLast", query = "SELECT p FROM PlanDetail p WHERE p.updateLast = :updateLast")})
public class PlanDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PlanDetailId")
    private Integer planDetailId;
    @Column(name = "PurchaseTypeInd")
    private Integer purchaseTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "availableVehiclePurchaseTypes")
    private int availableVehiclePurchaseTypes;
    @Column(name = "waitDays")
    private Integer waitDays;
    @Column(name = "earningMethodInd")
    private Integer earningMethodInd;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "ExpireDate")
    @Temporal(TemporalType.DATE)
    private Date expireDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "defaultTransferFee")
    private BigDecimal defaultTransferFee;
    @Column(name = "reimbursementAmount")
    private BigDecimal reimbursementAmount;
    @Column(name = "planExpireTimeTypeInd")
    private Integer planExpireTimeTypeInd;
    @Column(name = "cancelRequestValidDays")
    private Integer cancelRequestValidDays;
    @Column(name = "cancelRequestValidMiles")
    private Integer cancelRequestValidMiles;
    @Basic(optional = false)
    @NotNull
    @Column(name = "extendExpirationByWait")
    private boolean extendExpirationByWait;
    @Size(max = 300)
    @Column(name = "description")
    private String description;
    @Column(name = "cancelFeeDisbursementTypeInd")
    private Integer cancelFeeDisbursementTypeInd;
    @Column(name = "minimumTermMonths")
    private Integer minimumTermMonths;
    @Column(name = "maxFinanceOfMSRPandNADA")
    private Integer maxFinanceOfMSRPandNADA;
    @Size(max = 10)
    @Column(name = "limitDeductibleDefault")
    private String limitDeductibleDefault;
    @Column(name = "WaitMiles")
    private Integer waitMiles;
    @Column(name = "vscMinimumTermMiles")
    private Integer vscMinimumTermMiles;
    @Column(name = "vscPlanExpireUsageTypeInd")
    private Integer vscPlanExpireUsageTypeInd;
    @Column(name = "ppmScheduleStartInd")
    private Integer ppmScheduleStartInd;
    @Column(name = "gapMaxTermMonths")
    private Integer gapMaxTermMonths;
    @Column(name = "gapMaxFinanceOfMSRP")
    private BigDecimal gapMaxFinanceOfMSRP;
    @Column(name = "gapAvailableFinanceTypes")
    private Integer gapAvailableFinanceTypes;
    @Column(name = "ancReimbursementAmount")
    private BigDecimal ancReimbursementAmount;
    @Column(name = "ancAvailableFinanceTypes")
    private Integer ancAvailableFinanceTypes;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "cancelFeeAccountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper cancelFeeAccountKeeperIdFk;
    @JoinColumn(name = "classTableIdFk", referencedColumnName = "classTableId")
    @ManyToOne
    private ClassTable classTableIdFk;
    @JoinColumn(name = "defaultDeductibleIdFk", referencedColumnName = "deductibleId")
    @ManyToOne
    private Deductible defaultDeductibleIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @JoinColumn(name = "ppmScheduleIdFk", referencedColumnName = "PPMScheduleId")
    @ManyToOne
    private PPMSchedule ppmScheduleIdFk;
    @JoinColumn(name = "productTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne(optional = false)
    private ProductType productTypeIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public PlanDetail() {
    }

    public PlanDetail(Integer planDetailId) {
        this.planDetailId = planDetailId;
    }

    public PlanDetail(Integer planDetailId, int availableVehiclePurchaseTypes, boolean extendExpirationByWait) {
        this.planDetailId = planDetailId;
        this.availableVehiclePurchaseTypes = availableVehiclePurchaseTypes;
        this.extendExpirationByWait = extendExpirationByWait;
    }

    public Integer getPlanDetailId() {
        return planDetailId;
    }

    public void setPlanDetailId(Integer planDetailId) {
        this.planDetailId = planDetailId;
    }

    public Integer getPurchaseTypeInd() {
        return purchaseTypeInd;
    }

    public void setPurchaseTypeInd(Integer purchaseTypeInd) {
        this.purchaseTypeInd = purchaseTypeInd;
    }

    public int getAvailableVehiclePurchaseTypes() {
        return availableVehiclePurchaseTypes;
    }

    public void setAvailableVehiclePurchaseTypes(int availableVehiclePurchaseTypes) {
        this.availableVehiclePurchaseTypes = availableVehiclePurchaseTypes;
    }

    public Integer getWaitDays() {
        return waitDays;
    }

    public void setWaitDays(Integer waitDays) {
        this.waitDays = waitDays;
    }

    public Integer getEarningMethodInd() {
        return earningMethodInd;
    }

    public void setEarningMethodInd(Integer earningMethodInd) {
        this.earningMethodInd = earningMethodInd;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public BigDecimal getDefaultTransferFee() {
        return defaultTransferFee;
    }

    public void setDefaultTransferFee(BigDecimal defaultTransferFee) {
        this.defaultTransferFee = defaultTransferFee;
    }

    public BigDecimal getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(BigDecimal reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    public Integer getPlanExpireTimeTypeInd() {
        return planExpireTimeTypeInd;
    }

    public void setPlanExpireTimeTypeInd(Integer planExpireTimeTypeInd) {
        this.planExpireTimeTypeInd = planExpireTimeTypeInd;
    }

    public Integer getCancelRequestValidDays() {
        return cancelRequestValidDays;
    }

    public void setCancelRequestValidDays(Integer cancelRequestValidDays) {
        this.cancelRequestValidDays = cancelRequestValidDays;
    }

    public Integer getCancelRequestValidMiles() {
        return cancelRequestValidMiles;
    }

    public void setCancelRequestValidMiles(Integer cancelRequestValidMiles) {
        this.cancelRequestValidMiles = cancelRequestValidMiles;
    }

    public boolean getExtendExpirationByWait() {
        return extendExpirationByWait;
    }

    public void setExtendExpirationByWait(boolean extendExpirationByWait) {
        this.extendExpirationByWait = extendExpirationByWait;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCancelFeeDisbursementTypeInd() {
        return cancelFeeDisbursementTypeInd;
    }

    public void setCancelFeeDisbursementTypeInd(Integer cancelFeeDisbursementTypeInd) {
        this.cancelFeeDisbursementTypeInd = cancelFeeDisbursementTypeInd;
    }

    public Integer getMinimumTermMonths() {
        return minimumTermMonths;
    }

    public void setMinimumTermMonths(Integer minimumTermMonths) {
        this.minimumTermMonths = minimumTermMonths;
    }

    public Integer getMaxFinanceOfMSRPandNADA() {
        return maxFinanceOfMSRPandNADA;
    }

    public void setMaxFinanceOfMSRPandNADA(Integer maxFinanceOfMSRPandNADA) {
        this.maxFinanceOfMSRPandNADA = maxFinanceOfMSRPandNADA;
    }

    public String getLimitDeductibleDefault() {
        return limitDeductibleDefault;
    }

    public void setLimitDeductibleDefault(String limitDeductibleDefault) {
        this.limitDeductibleDefault = limitDeductibleDefault;
    }

    public Integer getWaitMiles() {
        return waitMiles;
    }

    public void setWaitMiles(Integer waitMiles) {
        this.waitMiles = waitMiles;
    }

    public Integer getVscMinimumTermMiles() {
        return vscMinimumTermMiles;
    }

    public void setVscMinimumTermMiles(Integer vscMinimumTermMiles) {
        this.vscMinimumTermMiles = vscMinimumTermMiles;
    }

    public Integer getVscPlanExpireUsageTypeInd() {
        return vscPlanExpireUsageTypeInd;
    }

    public void setVscPlanExpireUsageTypeInd(Integer vscPlanExpireUsageTypeInd) {
        this.vscPlanExpireUsageTypeInd = vscPlanExpireUsageTypeInd;
    }

    public Integer getPpmScheduleStartInd() {
        return ppmScheduleStartInd;
    }

    public void setPpmScheduleStartInd(Integer ppmScheduleStartInd) {
        this.ppmScheduleStartInd = ppmScheduleStartInd;
    }

    public Integer getGapMaxTermMonths() {
        return gapMaxTermMonths;
    }

    public void setGapMaxTermMonths(Integer gapMaxTermMonths) {
        this.gapMaxTermMonths = gapMaxTermMonths;
    }

    public BigDecimal getGapMaxFinanceOfMSRP() {
        return gapMaxFinanceOfMSRP;
    }

    public void setGapMaxFinanceOfMSRP(BigDecimal gapMaxFinanceOfMSRP) {
        this.gapMaxFinanceOfMSRP = gapMaxFinanceOfMSRP;
    }

    public Integer getGapAvailableFinanceTypes() {
        return gapAvailableFinanceTypes;
    }

    public void setGapAvailableFinanceTypes(Integer gapAvailableFinanceTypes) {
        this.gapAvailableFinanceTypes = gapAvailableFinanceTypes;
    }

    public BigDecimal getAncReimbursementAmount() {
        return ancReimbursementAmount;
    }

    public void setAncReimbursementAmount(BigDecimal ancReimbursementAmount) {
        this.ancReimbursementAmount = ancReimbursementAmount;
    }

    public Integer getAncAvailableFinanceTypes() {
        return ancAvailableFinanceTypes;
    }

    public void setAncAvailableFinanceTypes(Integer ancAvailableFinanceTypes) {
        this.ancAvailableFinanceTypes = ancAvailableFinanceTypes;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getCancelFeeAccountKeeperIdFk() {
        return cancelFeeAccountKeeperIdFk;
    }

    public void setCancelFeeAccountKeeperIdFk(AccountKeeper cancelFeeAccountKeeperIdFk) {
        this.cancelFeeAccountKeeperIdFk = cancelFeeAccountKeeperIdFk;
    }

    public ClassTable getClassTableIdFk() {
        return classTableIdFk;
    }

    public void setClassTableIdFk(ClassTable classTableIdFk) {
        this.classTableIdFk = classTableIdFk;
    }

    public Deductible getDefaultDeductibleIdFk() {
        return defaultDeductibleIdFk;
    }

    public void setDefaultDeductibleIdFk(Deductible defaultDeductibleIdFk) {
        this.defaultDeductibleIdFk = defaultDeductibleIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    public PPMSchedule getPpmScheduleIdFk() {
        return ppmScheduleIdFk;
    }

    public void setPpmScheduleIdFk(PPMSchedule ppmScheduleIdFk) {
        this.ppmScheduleIdFk = ppmScheduleIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planDetailId != null ? planDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanDetail)) {
            return false;
        }
        PlanDetail other = (PlanDetail) object;
        if ((this.planDetailId == null && other.planDetailId != null) || (this.planDetailId != null && !this.planDetailId.equals(other.planDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PlanDetail[ planDetailId=" + planDetailId + " ]";
    }
    
}
