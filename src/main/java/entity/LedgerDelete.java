/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "LedgerDelete")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LedgerDelete.findAll", query = "SELECT l FROM LedgerDelete l")
    , @NamedQuery(name = "LedgerDelete.findByLedgerDeleteId", query = "SELECT l FROM LedgerDelete l WHERE l.ledgerDeleteId = :ledgerDeleteId")
    , @NamedQuery(name = "LedgerDelete.findByUpdateUserName", query = "SELECT l FROM LedgerDelete l WHERE l.updateUserName = :updateUserName")
    , @NamedQuery(name = "LedgerDelete.findByUpdateLast", query = "SELECT l FROM LedgerDelete l WHERE l.updateLast = :updateLast")})
public class LedgerDelete implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ledgerDeleteId")
    private Integer ledgerDeleteId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ledgerDeleteId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "deletedLedgerIdFk", referencedColumnName = "ledgerId")
    @OneToOne(optional = false)
    private Ledger deletedLedgerIdFk;

    public LedgerDelete() {
    }

    public LedgerDelete(Integer ledgerDeleteId) {
        this.ledgerDeleteId = ledgerDeleteId;
    }

    public Integer getLedgerDeleteId() {
        return ledgerDeleteId;
    }

    public void setLedgerDeleteId(Integer ledgerDeleteId) {
        this.ledgerDeleteId = ledgerDeleteId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Ledger getDeletedLedgerIdFk() {
        return deletedLedgerIdFk;
    }

    public void setDeletedLedgerIdFk(Ledger deletedLedgerIdFk) {
        this.deletedLedgerIdFk = deletedLedgerIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ledgerDeleteId != null ? ledgerDeleteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LedgerDelete)) {
            return false;
        }
        LedgerDelete other = (LedgerDelete) object;
        if ((this.ledgerDeleteId == null && other.ledgerDeleteId != null) || (this.ledgerDeleteId != null && !this.ledgerDeleteId.equals(other.ledgerDeleteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LedgerDelete[ ledgerDeleteId=" + ledgerDeleteId + " ]";
    }
    
}
