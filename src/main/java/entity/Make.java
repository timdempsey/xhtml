/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Make")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Make.findAll", query = "SELECT m FROM Make m")
    , @NamedQuery(name = "Make.findByMakeId", query = "SELECT m FROM Make m WHERE m.makeId = :makeId")
    , @NamedQuery(name = "Make.findByName", query = "SELECT m FROM Make m WHERE m.name = :name")
    , @NamedQuery(name = "Make.findByActive", query = "SELECT m FROM Make m WHERE m.active = :active")
    , @NamedQuery(name = "Make.findByMakeAbbrv", query = "SELECT m FROM Make m WHERE m.makeAbbrv = :makeAbbrv")
    , @NamedQuery(name = "Make.findByUpdateUserName", query = "SELECT m FROM Make m WHERE m.updateUserName = :updateUserName")
    , @NamedQuery(name = "Make.findByUpdateLast", query = "SELECT m FROM Make m WHERE m.updateLast = :updateLast")})
public class Make implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "makeId")
    private Integer makeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Size(max = 4)
    @Column(name = "makeAbbrv")
    private String makeAbbrv;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "makeIdFk")
    private Collection<CfManufacturerWarranty> cfManufacturerWarrantyCollection;
    @OneToMany(mappedBy = "makeIdFk")
    private Collection<ClassItem> classItemCollection;
    @OneToMany(mappedBy = "makeIdFk")
    private Collection<Model> modelCollection;
    @OneToMany(mappedBy = "makeIdFk")
    private Collection<VinDesc> vinDescCollection;

    public Make() {
    }

    public Make(Integer makeId) {
        this.makeId = makeId;
    }

    public Make(Integer makeId, String name, boolean active) {
        this.makeId = makeId;
        this.name = name;
        this.active = active;
    }

    public Integer getMakeId() {
        return makeId;
    }

    public void setMakeId(Integer makeId) {
        this.makeId = makeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getMakeAbbrv() {
        return makeAbbrv;
    }

    public void setMakeAbbrv(String makeAbbrv) {
        this.makeAbbrv = makeAbbrv;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfManufacturerWarranty> getCfManufacturerWarrantyCollection() {
        return cfManufacturerWarrantyCollection;
    }

    public void setCfManufacturerWarrantyCollection(Collection<CfManufacturerWarranty> cfManufacturerWarrantyCollection) {
        this.cfManufacturerWarrantyCollection = cfManufacturerWarrantyCollection;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @XmlTransient
    public Collection<Model> getModelCollection() {
        return modelCollection;
    }

    public void setModelCollection(Collection<Model> modelCollection) {
        this.modelCollection = modelCollection;
    }

    @XmlTransient
    public Collection<VinDesc> getVinDescCollection() {
        return vinDescCollection;
    }

    public void setVinDescCollection(Collection<VinDesc> vinDescCollection) {
        this.vinDescCollection = vinDescCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (makeId != null ? makeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Make)) {
            return false;
        }
        Make other = (Make) object;
        if ((this.makeId == null && other.makeId != null) || (this.makeId != null && !this.makeId.equals(other.makeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Make[ makeId=" + makeId + " ]";
    }
    
}
