/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtavailableVehiclePurchaseTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtavailableVehiclePurchaseTypes.findAll", query = "SELECT d FROM DtavailableVehiclePurchaseTypes d")
    , @NamedQuery(name = "DtavailableVehiclePurchaseTypes.findByAvailableVehiclePurchaseTypesId", query = "SELECT d FROM DtavailableVehiclePurchaseTypes d WHERE d.availableVehiclePurchaseTypesId = :availableVehiclePurchaseTypesId")
    , @NamedQuery(name = "DtavailableVehiclePurchaseTypes.findByDescription", query = "SELECT d FROM DtavailableVehiclePurchaseTypes d WHERE d.description = :description")})
public class DtavailableVehiclePurchaseTypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "availableVehiclePurchaseTypesId")
    private Integer availableVehiclePurchaseTypesId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public DtavailableVehiclePurchaseTypes() {
    }

    public DtavailableVehiclePurchaseTypes(Integer availableVehiclePurchaseTypesId) {
        this.availableVehiclePurchaseTypesId = availableVehiclePurchaseTypesId;
    }

    public Integer getAvailableVehiclePurchaseTypesId() {
        return availableVehiclePurchaseTypesId;
    }

    public void setAvailableVehiclePurchaseTypesId(Integer availableVehiclePurchaseTypesId) {
        this.availableVehiclePurchaseTypesId = availableVehiclePurchaseTypesId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (availableVehiclePurchaseTypesId != null ? availableVehiclePurchaseTypesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtavailableVehiclePurchaseTypes)) {
            return false;
        }
        DtavailableVehiclePurchaseTypes other = (DtavailableVehiclePurchaseTypes) object;
        if ((this.availableVehiclePurchaseTypesId == null && other.availableVehiclePurchaseTypesId != null) || (this.availableVehiclePurchaseTypesId != null && !this.availableVehiclePurchaseTypesId.equals(other.availableVehiclePurchaseTypesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtavailableVehiclePurchaseTypes[ availableVehiclePurchaseTypesId=" + availableVehiclePurchaseTypesId + " ]";
    }
    
}
