/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfComponentGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfComponentGroup.findAll", query = "SELECT c FROM CfComponentGroup c")
    , @NamedQuery(name = "CfComponentGroup.findByComponentGroupId", query = "SELECT c FROM CfComponentGroup c WHERE c.componentGroupId = :componentGroupId")
    , @NamedQuery(name = "CfComponentGroup.findByName", query = "SELECT c FROM CfComponentGroup c WHERE c.name = :name")
    , @NamedQuery(name = "CfComponentGroup.findByUpdateUserName", query = "SELECT c FROM CfComponentGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfComponentGroup.findByUpdateLast", query = "SELECT c FROM CfComponentGroup c WHERE c.updateLast = :updateLast")})
public class CfComponentGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "componentGroupId")
    private Integer componentGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "componentGroupIdFk")
    private Collection<CfComponent> cfComponentCollection;

    public CfComponentGroup() {
    }

    public CfComponentGroup(Integer componentGroupId) {
        this.componentGroupId = componentGroupId;
    }

    public CfComponentGroup(Integer componentGroupId, String name) {
        this.componentGroupId = componentGroupId;
        this.name = name;
    }

    public Integer getComponentGroupId() {
        return componentGroupId;
    }

    public void setComponentGroupId(Integer componentGroupId) {
        this.componentGroupId = componentGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfComponent> getCfComponentCollection() {
        return cfComponentCollection;
    }

    public void setCfComponentCollection(Collection<CfComponent> cfComponentCollection) {
        this.cfComponentCollection = cfComponentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (componentGroupId != null ? componentGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfComponentGroup)) {
            return false;
        }
        CfComponentGroup other = (CfComponentGroup) object;
        if ((this.componentGroupId == null && other.componentGroupId != null) || (this.componentGroupId != null && !this.componentGroupId.equals(other.componentGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfComponentGroup[ componentGroupId=" + componentGroupId + " ]";
    }
    
}
