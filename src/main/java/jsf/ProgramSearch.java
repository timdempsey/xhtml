/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ProgramSearch {

    private static final Logger LOGGER = LogManager.getLogger(ContractSearch.class);

    public ProgramSearch() {
    }

    public ProgramSearch(Integer programId, String name, String code, String disbursementType, String adminCompany, Date effectiveDate, String expireDate, String description) {
        this.programId = programId;
        this.name = name;
        this.code = code;
        this.disbursementType = disbursementType;
        this.adminCompany = adminCompany;
        this.effectiveDate = effectiveDate;
        this.expireDate = expireDate;
        this.description = description;
    }
    
    

   

    @Id
    @Column(name = "programId", table = "Programs")
    private Integer programId;
    @Column(name = "name", table = "Programs")
    private String name;
    @Column(name = "code", table = "Programs")
    private String code;
    @Column(name = "disbursementType", table = "DisbursementCenter")
    private String disbursementType;
    private String adminCompany;
    @Column(name = "effectiveDate", table = "ProgramDetail")
    private Date effectiveDate;
    private String expireDate;
     @Column(name = "description", table = "ProgramDetail")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
     
     
    

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

   

    public void reset() {
        programId = null;
        name = "";
        code = "";
        disbursementType = "";
        adminCompany = "";
        effectiveDate = null;
        expireDate = null;
        description = "";
    }

}
