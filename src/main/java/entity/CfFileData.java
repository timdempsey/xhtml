/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfFileData")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfFileData.findAll", query = "SELECT c FROM CfFileData c")
    , @NamedQuery(name = "CfFileData.findByFileDataId", query = "SELECT c FROM CfFileData c WHERE c.fileDataId = :fileDataId")
    , @NamedQuery(name = "CfFileData.findByFileStorageMethodInd", query = "SELECT c FROM CfFileData c WHERE c.fileStorageMethodInd = :fileStorageMethodInd")
    , @NamedQuery(name = "CfFileData.findByStorageKey", query = "SELECT c FROM CfFileData c WHERE c.storageKey = :storageKey")
    , @NamedQuery(name = "CfFileData.findByEncryptedInd", query = "SELECT c FROM CfFileData c WHERE c.encryptedInd = :encryptedInd")
    , @NamedQuery(name = "CfFileData.findByCompressedInd", query = "SELECT c FROM CfFileData c WHERE c.compressedInd = :compressedInd")
    , @NamedQuery(name = "CfFileData.findBySalt", query = "SELECT c FROM CfFileData c WHERE c.salt = :salt")
    , @NamedQuery(name = "CfFileData.findByUpdateUserName", query = "SELECT c FROM CfFileData c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfFileData.findByUpdateLast", query = "SELECT c FROM CfFileData c WHERE c.updateLast = :updateLast")})
public class CfFileData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fileDataId")
    private Integer fileDataId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fileStorageMethodInd")
    private int fileStorageMethodInd;
    @Size(max = 50)
    @Column(name = "storageKey")
    private String storageKey;
    @Lob
    @Column(name = "fileContents")
    private byte[] fileContents;
    @Column(name = "encryptedInd")
    private Boolean encryptedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "compressedInd")
    private boolean compressedInd;
    @Size(max = 36)
    @Column(name = "salt")
    private String salt;
    @Lob
    @Column(name = "IV")
    private byte[] iv;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "fileDataId", referencedColumnName = "fileId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private CfFile cfFile;

    public CfFileData() {
    }

    public CfFileData(Integer fileDataId) {
        this.fileDataId = fileDataId;
    }

    public CfFileData(Integer fileDataId, int fileStorageMethodInd, boolean compressedInd) {
        this.fileDataId = fileDataId;
        this.fileStorageMethodInd = fileStorageMethodInd;
        this.compressedInd = compressedInd;
    }

    public Integer getFileDataId() {
        return fileDataId;
    }

    public void setFileDataId(Integer fileDataId) {
        this.fileDataId = fileDataId;
    }

    public int getFileStorageMethodInd() {
        return fileStorageMethodInd;
    }

    public void setFileStorageMethodInd(int fileStorageMethodInd) {
        this.fileStorageMethodInd = fileStorageMethodInd;
    }

    public String getStorageKey() {
        return storageKey;
    }

    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    public byte[] getFileContents() {
        return fileContents;
    }

    public void setFileContents(byte[] fileContents) {
        this.fileContents = fileContents;
    }

    public Boolean getEncryptedInd() {
        return encryptedInd;
    }

    public void setEncryptedInd(Boolean encryptedInd) {
        this.encryptedInd = encryptedInd;
    }

    public boolean getCompressedInd() {
        return compressedInd;
    }

    public void setCompressedInd(boolean compressedInd) {
        this.compressedInd = compressedInd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfFile getCfFile() {
        return cfFile;
    }

    public void setCfFile(CfFile cfFile) {
        this.cfFile = cfFile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileDataId != null ? fileDataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfFileData)) {
            return false;
        }
        CfFileData other = (CfFileData) object;
        if ((this.fileDataId == null && other.fileDataId != null) || (this.fileDataId != null && !this.fileDataId.equals(other.fileDataId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfFileData[ fileDataId=" + fileDataId + " ]";
    }
    
}
