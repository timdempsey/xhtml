/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Surcharge")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Surcharge.findAll", query = "SELECT s FROM Surcharge s")
    , @NamedQuery(name = "Surcharge.findBySurchargeId", query = "SELECT s FROM Surcharge s WHERE s.surchargeId = :surchargeId")
    , @NamedQuery(name = "Surcharge.findByInternalName", query = "SELECT s FROM Surcharge s WHERE s.internalName = :internalName")
    , @NamedQuery(name = "Surcharge.findByDescription", query = "SELECT s FROM Surcharge s WHERE s.description = :description")
    , @NamedQuery(name = "Surcharge.findByUpdateUserName", query = "SELECT s FROM Surcharge s WHERE s.updateUserName = :updateUserName")
    , @NamedQuery(name = "Surcharge.findByUpdateLlast", query = "SELECT s FROM Surcharge s WHERE s.updateLlast = :updateLlast")})
public class Surcharge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "surchargeId")
    private Integer surchargeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "internalName")
    private String internalName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLlast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLlast;
    @OneToMany(mappedBy = "relatedSurchargeIdFk")
    private Collection<ContractCoverageGroup> contractCoverageGroupCollection;
    @OneToMany(mappedBy = "surchargeIdFk")
    private Collection<ContractSurcharge> contractSurchargeCollection;
    @OneToMany(mappedBy = "relatedSurchargeIdFk")
    private Collection<ContractDisbursement> contractDisbursementCollection;
    @OneToMany(mappedBy = "surchargeIdFk")
    private Collection<CfContractEntryField> cfContractEntryFieldCollection;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "surchargeIdFk")
    private Collection<SurchargeDetail> surchargeDetailCollection;

    public Surcharge() {
    }

    public Surcharge(Integer surchargeId) {
        this.surchargeId = surchargeId;
    }

    public Surcharge(Integer surchargeId, String internalName, String description) {
        this.surchargeId = surchargeId;
        this.internalName = internalName;
        this.description = description;
    }

    public Integer getSurchargeId() {
        return surchargeId;
    }

    public void setSurchargeId(Integer surchargeId) {
        this.surchargeId = surchargeId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLlast() {
        return updateLlast;
    }

    public void setUpdateLlast(Date updateLlast) {
        this.updateLlast = updateLlast;
    }

    @XmlTransient
    public Collection<ContractCoverageGroup> getContractCoverageGroupCollection() {
        return contractCoverageGroupCollection;
    }

    public void setContractCoverageGroupCollection(Collection<ContractCoverageGroup> contractCoverageGroupCollection) {
        this.contractCoverageGroupCollection = contractCoverageGroupCollection;
    }

    @XmlTransient
    public Collection<ContractSurcharge> getContractSurchargeCollection() {
        return contractSurchargeCollection;
    }

    public void setContractSurchargeCollection(Collection<ContractSurcharge> contractSurchargeCollection) {
        this.contractSurchargeCollection = contractSurchargeCollection;
    }

    @XmlTransient
    public Collection<ContractDisbursement> getContractDisbursementCollection() {
        return contractDisbursementCollection;
    }

    public void setContractDisbursementCollection(Collection<ContractDisbursement> contractDisbursementCollection) {
        this.contractDisbursementCollection = contractDisbursementCollection;
    }

    @XmlTransient
    public Collection<CfContractEntryField> getCfContractEntryFieldCollection() {
        return cfContractEntryFieldCollection;
    }

    public void setCfContractEntryFieldCollection(Collection<CfContractEntryField> cfContractEntryFieldCollection) {
        this.cfContractEntryFieldCollection = cfContractEntryFieldCollection;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<SurchargeDetail> getSurchargeDetailCollection() {
        return surchargeDetailCollection;
    }

    public void setSurchargeDetailCollection(Collection<SurchargeDetail> surchargeDetailCollection) {
        this.surchargeDetailCollection = surchargeDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (surchargeId != null ? surchargeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Surcharge)) {
            return false;
        }
        Surcharge other = (Surcharge) object;
        if ((this.surchargeId == null && other.surchargeId != null) || (this.surchargeId != null && !this.surchargeId.equals(other.surchargeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Surcharge[ surchargeId=" + surchargeId + " ]";
    }
    
}
