/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractResolve")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractResolve.findAll", query = "SELECT c FROM ContractResolve c")
    , @NamedQuery(name = "ContractResolve.findByContractResolveId", query = "SELECT c FROM ContractResolve c WHERE c.contractResolveId = :contractResolveId")
    , @NamedQuery(name = "ContractResolve.findByRemitAmount", query = "SELECT c FROM ContractResolve c WHERE c.remitAmount = :remitAmount")
    , @NamedQuery(name = "ContractResolve.findByUpdateUserName", query = "SELECT c FROM ContractResolve c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractResolve.findByUpdateLast", query = "SELECT c FROM ContractResolve c WHERE c.updateLast = :updateLast")})
public class ContractResolve implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ContractResolveId")
    private Integer contractResolveId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "RemitAmount")
    private BigDecimal remitAmount;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "contractResolveIdFk")
    private Collection<ResolveDisbursement> resolveDisbursementCollection;
    @JoinColumn(name = "ContractHoldIdFk", referencedColumnName = "contractHoldId")
    @OneToOne(optional = false)
    private ContractHold contractHoldIdFk;
    @JoinColumn(name = "ContractResolveId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public ContractResolve() {
    }

    public ContractResolve(Integer contractResolveId) {
        this.contractResolveId = contractResolveId;
    }

    public Integer getContractResolveId() {
        return contractResolveId;
    }

    public void setContractResolveId(Integer contractResolveId) {
        this.contractResolveId = contractResolveId;
    }

    public BigDecimal getRemitAmount() {
        return remitAmount;
    }

    public void setRemitAmount(BigDecimal remitAmount) {
        this.remitAmount = remitAmount;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ResolveDisbursement> getResolveDisbursementCollection() {
        return resolveDisbursementCollection;
    }

    public void setResolveDisbursementCollection(Collection<ResolveDisbursement> resolveDisbursementCollection) {
        this.resolveDisbursementCollection = resolveDisbursementCollection;
    }

    public ContractHold getContractHoldIdFk() {
        return contractHoldIdFk;
    }

    public void setContractHoldIdFk(ContractHold contractHoldIdFk) {
        this.contractHoldIdFk = contractHoldIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractResolveId != null ? contractResolveId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractResolve)) {
            return false;
        }
        ContractResolve other = (ContractResolve) object;
        if ((this.contractResolveId == null && other.contractResolveId != null) || (this.contractResolveId != null && !this.contractResolveId.equals(other.contractResolveId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractResolve[ contractResolveId=" + contractResolveId + " ]";
    }
    
}
