/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DisbursementAccount")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisbursementAccount.findAll", query = "SELECT d FROM DisbursementAccount d")
    , @NamedQuery(name = "DisbursementAccount.findByDisbursementAccountId", query = "SELECT d FROM DisbursementAccount d WHERE d.disbursementAccountId = :disbursementAccountId")
    , @NamedQuery(name = "DisbursementAccount.findByDescription", query = "SELECT d FROM DisbursementAccount d WHERE d.description = :description")
    , @NamedQuery(name = "DisbursementAccount.findByUpdateUserName", query = "SELECT d FROM DisbursementAccount d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DisbursementAccount.findByUpdateLast", query = "SELECT d FROM DisbursementAccount d WHERE d.updateLast = :updateLast")})
public class DisbursementAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementAccountId")
    private Integer disbursementAccountId;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "disbursementAccountId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;

    public DisbursementAccount() {
    }

    public DisbursementAccount(Integer disbursementAccountId) {
        this.disbursementAccountId = disbursementAccountId;
    }

    public Integer getDisbursementAccountId() {
        return disbursementAccountId;
    }

    public void setDisbursementAccountId(Integer disbursementAccountId) {
        this.disbursementAccountId = disbursementAccountId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementAccountId != null ? disbursementAccountId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisbursementAccount)) {
            return false;
        }
        DisbursementAccount other = (DisbursementAccount) object;
        if ((this.disbursementAccountId == null && other.disbursementAccountId != null) || (this.disbursementAccountId != null && !this.disbursementAccountId.equals(other.disbursementAccountId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DisbursementAccount[ disbursementAccountId=" + disbursementAccountId + " ]";
    }
    
}
