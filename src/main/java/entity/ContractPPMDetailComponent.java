/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractPPMDetailComponent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractPPMDetailComponent.findAll", query = "SELECT c FROM ContractPPMDetailComponent c")
    , @NamedQuery(name = "ContractPPMDetailComponent.findByContractPPMDetailComponentId", query = "SELECT c FROM ContractPPMDetailComponent c WHERE c.contractPPMDetailComponentId = :contractPPMDetailComponentId")})
public class ContractPPMDetailComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractPPMDetailComponentId")
    private Integer contractPPMDetailComponentId;
    @JoinColumn(name = "ComponentIdFk", referencedColumnName = "componentId")
    @ManyToOne
    private CfComponent componentIdFk;
    @JoinColumn(name = "ContractPPMDetailIdFk", referencedColumnName = "contractPPMDetailId")
    @ManyToOne
    private ContractPPMDetail contractPPMDetailIdFk;

    public ContractPPMDetailComponent() {
    }

    public ContractPPMDetailComponent(Integer contractPPMDetailComponentId) {
        this.contractPPMDetailComponentId = contractPPMDetailComponentId;
    }

    public Integer getContractPPMDetailComponentId() {
        return contractPPMDetailComponentId;
    }

    public void setContractPPMDetailComponentId(Integer contractPPMDetailComponentId) {
        this.contractPPMDetailComponentId = contractPPMDetailComponentId;
    }

    public CfComponent getComponentIdFk() {
        return componentIdFk;
    }

    public void setComponentIdFk(CfComponent componentIdFk) {
        this.componentIdFk = componentIdFk;
    }

    public ContractPPMDetail getContractPPMDetailIdFk() {
        return contractPPMDetailIdFk;
    }

    public void setContractPPMDetailIdFk(ContractPPMDetail contractPPMDetailIdFk) {
        this.contractPPMDetailIdFk = contractPPMDetailIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractPPMDetailComponentId != null ? contractPPMDetailComponentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractPPMDetailComponent)) {
            return false;
        }
        ContractPPMDetailComponent other = (ContractPPMDetailComponent) object;
        if ((this.contractPPMDetailComponentId == null && other.contractPPMDetailComponentId != null) || (this.contractPPMDetailComponentId != null && !this.contractPPMDetailComponentId.equals(other.contractPPMDetailComponentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractPPMDetailComponent[ contractPPMDetailComponentId=" + contractPPMDetailComponentId + " ]";
    }
    
}
