/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Address;
import java.util.TreeMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class MyAddress {

    private static final Logger LOGGER = LogManager.getLogger(MyAddress.class);

    private ClaimMisc claimMisc;

    public MyAddress() {
        claimMisc = new ClaimMisc();
        stateMap = claimMisc.loadStateMap();
        sameAsPaddr = true;
        paddr = new Address();
        baddr = new Address();
    }

    public MyAddress(ClaimMisc claimMisc, Address paddr, Address baddr, Boolean sameAsPaddr, String regionP, String regionB, TreeMap<String, String> stateMap, Integer paddrFkId, Integer baddrFkId) {
        this.claimMisc = claimMisc;
        this.paddr = paddr;
        this.baddr = baddr;
        this.sameAsPaddr = sameAsPaddr;
        this.regionP = regionP;
        this.regionB = regionB;
        this.stateMap = stateMap;
        this.paddrFkId = paddrFkId;
        this.baddrFkId = baddrFkId;
    }

    private Address paddr;
    private Address baddr;
    private Boolean sameAsPaddr;
    private String regionP;
    private String regionB;
    private TreeMap<String, String> stateMap;
    private Integer paddrFkId;
    private Integer baddrFkId;

    public ClaimMisc getClaimMisc() {
        return claimMisc;
    }

    public void setClaimMisc(ClaimMisc claimMisc) {
        this.claimMisc = claimMisc;
    }

    public Integer getPaddrFkId() {
        return paddrFkId;
    }

    public void setPaddrFkId(Integer paddrFkId) {
        this.paddrFkId = paddrFkId;
    }

    public Integer getBaddrFkId() {
        return baddrFkId;
    }

    public void setBaddrFkId(Integer baddrFkId) {
        this.baddrFkId = baddrFkId;
    }

    public Address getPaddr() {
        return paddr;
    }

    public void setPaddr(Address paddr) {
        this.paddr = paddr;
    }

    public Address getBaddr() {
        return baddr;
    }

    public void setBaddr(Address baddr) {
        this.baddr = baddr;
    }

    public Boolean getSameAsPaddr() {
        return sameAsPaddr;
    }

    public void setSameAsPaddr(Boolean sameAsPaddr) {
        this.sameAsPaddr = sameAsPaddr;
    }

    public String getRegionP() {
        return regionP;
    }

    public void setRegionP(String regionP) {
        this.regionP = regionP;
    }

    public String getRegionB() {
        return regionB;
    }

    public void setRegionB(String regionB) {
        this.regionB = regionB;
    }

    public TreeMap<String, String> getStateMap() {
        return stateMap;
    }

    public void setStateMap(TreeMap<String, String> stateMap) {
        this.stateMap = stateMap;
    }

    public void reset() {
        paddr = null;
        baddr = null;
        sameAsPaddr = true;
        regionP = "";
        regionB = "";
        paddrFkId = null;
        baddrFkId = null;
    }

}
