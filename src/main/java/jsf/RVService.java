/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.data.FilterEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "rvService")
@ApplicationScoped
public class RVService {

    /**
     * Creates a new instance of RVService
     */
    public RVService() {
        rvList = new ArrayList<>();
    }

    private static final Logger LOGGER = LogManager.getLogger(RVService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;
    
    

    private List<RV> rvList;
    private RV selectedRV;

    private List<RV> filtersList;
    
    private Integer dealerId;
    
    private RV rv;

    public RV getRv() {
        return rv;
    }

    public void setRv(RV rv) {
        this.rv = rv;
    }
    
    

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }
    
    

    public List<RV> getFiltersList() {
        return filtersList;
    }

    public void setFiltersList(List<RV> filtersList) {
        this.filtersList = filtersList;
    }

    public RV getSelectedRV() {
        return selectedRV;
    }

    public void setSelectedRV(RV selectedRV) {
        this.selectedRV = selectedRV;
    }

    public List<RV> getRvList() {
        return rvList;
    }

    public void setRvList(List<RV> rvList) {
        this.rvList = rvList;
    }

    @PostConstruct
    public void init() {
        reset();
    }

    public void createRVTab(Integer dealerId) {
        this.dealerId = dealerId;
        rvList = claimMisc.getRVByDealerId(dealerId);
    }

    public void searchRV(ActionEvent event) {
        LOGGER.info("in searchRV");

        /*
        (String) event.getComponent().getAttributes().get("");
         */
        //reset();

        rvList = claimMisc.getRVByDealerId(-1);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1400");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("SearchRV", options, null);
    }

    public void rvSearchClosed(ActionEvent event) {
        LOGGER.info("in rvSearchClosed...");

    }

    public void onSearchRowSelect(SelectEvent event) {
        selectedRV = (RV) event.getObject();
        LOGGER.info("onSearchRowSelect, RV selected=" + selectedRV.getDescription());

        //claimGeneralService.setRfsearch(selectedRF);
        Integer oid = selectedRV.getDealerId();
        selectedRV.setDealerId(dealerId);
        rvList = claimMisc.connectDealerToRV(oid, selectedRV );

        RequestContext.getCurrentInstance().closeDialog("SearchRV");

    }

    public void filterListener(FilterEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        DataTable table = (DataTable) facesContext.getViewRoot().findComponent("srvForm:srv");
        //table.resetValue();
        //dealersList = table.getFilteredValue();
        LOGGER.info(" list=" + filtersList.size());
    }
    
    public String getRV() {
        LOGGER.info("getRV");
        
        rvList = claimMisc.getRVByDealerId(null);
        
        return "RV";
    }
    
    public void deleteRV() {
        
    }

    public void reset() {
        dealerId = null;
        selectedRV = null;
        filtersList = new ArrayList<>();
        rv = new RV();
    }
    
    public void onRowSelect(SelectEvent event) {
        LOGGER.info("on Row Select, rateVisibilityId=" + selectedRV.getRateVisibilityId());
        LOGGER.info("on Row Select, name =" + selectedRV.getName());
    }
    
    public void addRV(ActionEvent event) {
        reset();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddRV", options, null);

    }
    
    public void editRVClosed(SelectEvent event) {
        LOGGER.info("in editRVClosed");

        rvList = claimMisc.getRVByDealerId(null);

        //RequestContext.getCurrentInstance().closeDialog("AddRV");

    }
    
    public void saveRV() {
        LOGGER.info("in saveRV");
        
        if (selectedRV != null) {  // update
            LOGGER.info("updating RV=" + selectedRV.getRateVisibilityId());
            claimMisc.saveRV(rv, selectedRV.getRateVisibilityId());
        } else {
            LOGGER.info("save new RV");
            claimMisc.saveRV(rv, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddRV");
    }
    
    public void cancelRV() {
        LOGGER.info("in cancelRV");
        
        RequestContext.getCurrentInstance().closeDialog("AddRV");
    }
    
    public void verifyRVName() {
        LOGGER.info("in verifyRVName, rvName=" + rv.getName());
        if (claimMisc.getRateBasedRuleGroupByName(rv.getName()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "the  name already exist: ", rv.getName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void editRV() {
        LOGGER.info("in editRV");
        
        rv = selectedRV;
        
        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "500");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddRV", options, null);
        
    }
    


}
