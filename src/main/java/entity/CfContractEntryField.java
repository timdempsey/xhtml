/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfContractEntryField")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfContractEntryField.findAll", query = "SELECT c FROM CfContractEntryField c")
    , @NamedQuery(name = "CfContractEntryField.findByContractEntryFieldId", query = "SELECT c FROM CfContractEntryField c WHERE c.contractEntryFieldId = :contractEntryFieldId")
    , @NamedQuery(name = "CfContractEntryField.findByRequiredContractEntryFieldInd", query = "SELECT c FROM CfContractEntryField c WHERE c.requiredContractEntryFieldInd = :requiredContractEntryFieldInd")
    , @NamedQuery(name = "CfContractEntryField.findByTabOrder", query = "SELECT c FROM CfContractEntryField c WHERE c.tabOrder = :tabOrder")
    , @NamedQuery(name = "CfContractEntryField.findByRowNumber", query = "SELECT c FROM CfContractEntryField c WHERE c.rowNumber = :rowNumber")
    , @NamedQuery(name = "CfContractEntryField.findByCustomerFieldInd", query = "SELECT c FROM CfContractEntryField c WHERE c.customerFieldInd = :customerFieldInd")
    , @NamedQuery(name = "CfContractEntryField.findByCoBuyerFieldInd", query = "SELECT c FROM CfContractEntryField c WHERE c.coBuyerFieldInd = :coBuyerFieldInd")
    , @NamedQuery(name = "CfContractEntryField.findByTextCaseStyleInd", query = "SELECT c FROM CfContractEntryField c WHERE c.textCaseStyleInd = :textCaseStyleInd")
    , @NamedQuery(name = "CfContractEntryField.findByDeletedInd", query = "SELECT c FROM CfContractEntryField c WHERE c.deletedInd = :deletedInd")
    , @NamedQuery(name = "CfContractEntryField.findBySkipTabInd", query = "SELECT c FROM CfContractEntryField c WHERE c.skipTabInd = :skipTabInd")
    , @NamedQuery(name = "CfContractEntryField.findByPPMProductServiceIdFk", query = "SELECT c FROM CfContractEntryField c WHERE c.pPMProductServiceIdFk = :pPMProductServiceIdFk")
    , @NamedQuery(name = "CfContractEntryField.findByCopyFromFieldIdFk", query = "SELECT c FROM CfContractEntryField c WHERE c.copyFromFieldIdFk = :copyFromFieldIdFk")
    , @NamedQuery(name = "CfContractEntryField.findByUpdateUserName", query = "SELECT c FROM CfContractEntryField c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfContractEntryField.findByUpdateLast", query = "SELECT c FROM CfContractEntryField c WHERE c.updateLast = :updateLast")})
public class CfContractEntryField implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractEntryFieldId")
    private Integer contractEntryFieldId;
    @Column(name = "requiredContractEntryFieldInd")
    private Integer requiredContractEntryFieldInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tabOrder")
    private int tabOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rowNumber")
    private int rowNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "customerFieldInd")
    private boolean customerFieldInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "coBuyerFieldInd")
    private boolean coBuyerFieldInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "textCaseStyleInd")
    private int textCaseStyleInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "skipTabInd")
    private boolean skipTabInd;
    @Column(name = "PPMProductServiceIdFk")
    private Integer pPMProductServiceIdFk;
    @Column(name = "copyFromFieldIdFk")
    private Integer copyFromFieldIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "adjustmentIdFk", referencedColumnName = "adjustmentId")
    @ManyToOne
    private Adjustment adjustmentIdFk;
    @JoinColumn(name = "fieldIdFk", referencedColumnName = "FieldId")
    @ManyToOne
    private CfField fieldIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @JoinColumn(name = "surchargeIdFk", referencedColumnName = "surchargeId")
    @ManyToOne
    private Surcharge surchargeIdFk;

    public CfContractEntryField() {
    }

    public CfContractEntryField(Integer contractEntryFieldId) {
        this.contractEntryFieldId = contractEntryFieldId;
    }

    public CfContractEntryField(Integer contractEntryFieldId, int tabOrder, int rowNumber, boolean customerFieldInd, boolean coBuyerFieldInd, int textCaseStyleInd, boolean deletedInd, boolean skipTabInd) {
        this.contractEntryFieldId = contractEntryFieldId;
        this.tabOrder = tabOrder;
        this.rowNumber = rowNumber;
        this.customerFieldInd = customerFieldInd;
        this.coBuyerFieldInd = coBuyerFieldInd;
        this.textCaseStyleInd = textCaseStyleInd;
        this.deletedInd = deletedInd;
        this.skipTabInd = skipTabInd;
    }

    public Integer getContractEntryFieldId() {
        return contractEntryFieldId;
    }

    public void setContractEntryFieldId(Integer contractEntryFieldId) {
        this.contractEntryFieldId = contractEntryFieldId;
    }

    public Integer getRequiredContractEntryFieldInd() {
        return requiredContractEntryFieldInd;
    }

    public void setRequiredContractEntryFieldInd(Integer requiredContractEntryFieldInd) {
        this.requiredContractEntryFieldInd = requiredContractEntryFieldInd;
    }

    public int getTabOrder() {
        return tabOrder;
    }

    public void setTabOrder(int tabOrder) {
        this.tabOrder = tabOrder;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public boolean getCustomerFieldInd() {
        return customerFieldInd;
    }

    public void setCustomerFieldInd(boolean customerFieldInd) {
        this.customerFieldInd = customerFieldInd;
    }

    public boolean getCoBuyerFieldInd() {
        return coBuyerFieldInd;
    }

    public void setCoBuyerFieldInd(boolean coBuyerFieldInd) {
        this.coBuyerFieldInd = coBuyerFieldInd;
    }

    public int getTextCaseStyleInd() {
        return textCaseStyleInd;
    }

    public void setTextCaseStyleInd(int textCaseStyleInd) {
        this.textCaseStyleInd = textCaseStyleInd;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public boolean getSkipTabInd() {
        return skipTabInd;
    }

    public void setSkipTabInd(boolean skipTabInd) {
        this.skipTabInd = skipTabInd;
    }

    public Integer getPPMProductServiceIdFk() {
        return pPMProductServiceIdFk;
    }

    public void setPPMProductServiceIdFk(Integer pPMProductServiceIdFk) {
        this.pPMProductServiceIdFk = pPMProductServiceIdFk;
    }

    public Integer getCopyFromFieldIdFk() {
        return copyFromFieldIdFk;
    }

    public void setCopyFromFieldIdFk(Integer copyFromFieldIdFk) {
        this.copyFromFieldIdFk = copyFromFieldIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Adjustment getAdjustmentIdFk() {
        return adjustmentIdFk;
    }

    public void setAdjustmentIdFk(Adjustment adjustmentIdFk) {
        this.adjustmentIdFk = adjustmentIdFk;
    }

    public CfField getFieldIdFk() {
        return fieldIdFk;
    }

    public void setFieldIdFk(CfField fieldIdFk) {
        this.fieldIdFk = fieldIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    public Surcharge getSurchargeIdFk() {
        return surchargeIdFk;
    }

    public void setSurchargeIdFk(Surcharge surchargeIdFk) {
        this.surchargeIdFk = surchargeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractEntryFieldId != null ? contractEntryFieldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfContractEntryField)) {
            return false;
        }
        CfContractEntryField other = (CfContractEntryField) object;
        if ((this.contractEntryFieldId == null && other.contractEntryFieldId != null) || (this.contractEntryFieldId != null && !this.contractEntryFieldId.equals(other.contractEntryFieldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfContractEntryField[ contractEntryFieldId=" + contractEntryFieldId + " ]";
    }
    
}
