/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "claimGeneralService")
@ApplicationScoped
public class ClaimGeneralService {

    private static final Logger LOGGER = LogManager.getLogger(ClaimGeneralService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ClaimGeneralService
     */
    public ClaimGeneralService() {
        //cg = new ClaimGeneral();
        //cganc = new ClaimGeneralANC();
        claimMisc = new ClaimMisc();
    }

    private String claimNumber;
    private Claim claim;
    private String ownedBy;
    private String claimStatus;
    private String claimSubStatus;
    private String contractNo;
    private Contracts contract;

    private ClaimGeneral cg;
    private ClaimGeneralANC cganc;

    RFSearch rfsearch;
    MemberSearch memberSearch;

    // 0=new, 1=edit, 2=view
    int ANCeditable;
    int VSCeditable;
    int PPMeditable;
    int GAPeditable;

    boolean VSCreadonly;
    boolean ANCreadonly;
    boolean PPMreadonly;
    boolean GAPreadonly;

    String claimType;

    Integer ppmodometer;

    ClaimGeneral rfcg;

    public ClaimGeneral getRfcg() {
        return rfcg;
    }

    public void setRfcg(ClaimGeneral rfcg) {
        this.rfcg = rfcg;
    }

    public Integer getPpmodometer() {
        return ppmodometer;
    }

    public void setPpmodometer(Integer ppmodometer) {
        this.ppmodometer = ppmodometer;
    }

    public MemberSearch getMemberSearch() {
        return memberSearch;
    }

    public void setMemberSearch(MemberSearch memberSearch) {
        this.memberSearch = memberSearch;
    }

    public RFSearch getRfsearch() {
        return rfsearch;
    }

    public void setRfsearch(RFSearch rfsearch) {
        this.rfsearch = rfsearch;
    }

    public String getClaimType() {
        return claimType;
    }

    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

    public boolean isVSCreadonly() {
        return VSCreadonly;
    }

    public void setVSCreadonly(boolean VSCreadonly) {
        this.VSCreadonly = VSCreadonly;
    }

    public boolean isANCreadonly() {
        return ANCreadonly;
    }

    public void setANCreadonly(boolean ANCreadonly) {
        this.ANCreadonly = ANCreadonly;
    }

    public boolean isPPMreadonly() {
        return PPMreadonly;
    }

    public void setPPMreadonly(boolean PPMreadonly) {
        this.PPMreadonly = PPMreadonly;
    }

    public boolean isGAPreadonly() {
        return GAPreadonly;
    }

    public void setGAPreadonly(boolean GAPreadonly) {
        this.GAPreadonly = GAPreadonly;
    }

    public int getANCeditable() {
        return ANCeditable;
    }

    public void setANCeditable(int ANCeditable) {
        this.ANCeditable = ANCeditable;
    }

    public int getVSCeditable() {
        return VSCeditable;
    }

    public void setVSCeditable(int VSCeditable) {
        this.VSCeditable = VSCeditable;
    }

    public int getPPMeditable() {
        return PPMeditable;
    }

    public void setPPMeditable(int PPMeditable) {
        this.PPMeditable = PPMeditable;
    }

    public int getGAPeditable() {
        return GAPeditable;
    }

    public void setGAPeditable(int GAPeditable) {
        this.GAPeditable = GAPeditable;
    }

    public ClaimGeneralANC getCganc() {
        return cganc;
    }

    public void setCganc(ClaimGeneralANC cganc) {
        this.cganc = cganc;
    }

    public ClaimGeneral getCg() {
        return cg;
    }

    public void setCg(ClaimGeneral cg) {
        this.cg = cg;
    }
    // end of new

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public String getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(String ownedBy) {
        this.ownedBy = ownedBy;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public String getClaimSubStatus() {
        return claimSubStatus;
    }

    public void setClaimSubStatus(String claimSubStatus) {
        this.claimSubStatus = claimSubStatus;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Contracts getContract() {
        return contract;
    }

    public void setContract(Contracts contract) {
        this.contract = contract;
    }

    public void chooseClaimGeneral(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");

        LOGGER.info("chooseClaimGeneral=====" + claimNumber);
        //claim = em.find(Claim.class, claimNumber);
        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();

        cg = createClaimView(claimNumber);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", false);
        options.put("draggable", true);
        /*
        options.put("contentHeight", "'100%'");
        options.put("contentWidth", "'100%'");
         */
        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);
        LOGGER.info("claim id=" + claim.getClaimId());
        RequestContext.getCurrentInstance().openDialog("claimGeneral", options, null);
    }

    @Inject
    transient private ClaimMisc claimMisc;

    public ClaimGeneral createClaimView(String claimNumber) {
        LOGGER.info("in createClaimView=" + claimNumber);
        Claim cl = claimMisc.getClaimByNumber(claimNumber);

        claimStatus = cl.getClaimStatus().getClaimStatusDesc();
        claimSubStatus = cl.getClaimSubStatus().getClaimSubstatusDesc();

        AccountKeeper ak = claimMisc.getAccountKeeperByClaimNumber(claimNumber);

        Address addr = null;
        if (ak != null) {
            addr = claimMisc.getAddressByClaimNumber(ak.getAccountKeeperId());
        }

        RepairFacilityContactPerson rfcp = claimMisc.getRepairFacilityContactPersonByClaimNumber(claimNumber);

        RepairFacility rf = claimMisc.getRepairFacilityByClaimNumber(claimNumber);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String roDate = null;
        if (cl.getRepairOrderDate() != null) {
            roDate = df.format(cl.getRepairOrderDate());
        }
        /*
        if (cl.getInceptionDate() != null) {
            roDate = df.format(cl.getInceptionDate());
        }
         */

        CfRegion region = claimMisc.getRegionByClaimNumber(claimNumber);

        AccountKeeper ako = claimMisc.getAccountKeeperByClaimOwnedBy(claimNumber);
        if (ako != null) {
            ownedBy = ako.getAccountKeeperName();
        }

        LOGGER.info("return CG");
        return new ClaimGeneral(cl.getClaimId(),
                (addr != null ? addr.getPhoneNumber() : ""),
                (rfcp != null ? rfcp.getName() : ""),
                (rfcp != null ? rfcp.getPhoneNumber() : ""),
                (addr != null ? addr.getFax() : ""),
                claimNumber,
                cl.getCurrentOdometer(),
                Optional.ofNullable(cl.getRepairOrderNumber()).orElse(""),
                roDate,
                (ak != null ? ak.getAccountKeeperName() : ""),
                (addr != null ? addr.getAddress1() : ""),
                (addr != null ? addr.getAddress2() : ""),
                (addr != null ? addr.getZipCode() : ""),
                (addr != null ? addr.getCity() : ""),
                (region != null ? region.getRegionCode() : ""),
                (rf != null ? rf.getLaborRate() : null),
                (rf != null ? rf.getLaborTaxPct() : null),
                (rf != null ? rf.getPartsTaxPct() : null),
                (rf != null ? rf.getPartWarrantyMonths() : null),
                (rf != null ? rf.getPartWarrantyMiles() : null),
                null,
                claim.getOwnedByFk().getUserName(),
                (ak != null ? ak.getEmailAddress() : ""),
                (ak != null ? ak.getTaxId() : "")
        );
    }

    public ClaimGeneralANC createClaimViewANC(String claimNumber) {
        LOGGER.info("in createClaimViewANC=" + claimNumber);
        Claim cl = claimMisc.getClaimByNumber(claimNumber);

        cg = createClaimView(claimNumber);  // for edit repairfacility

        claimStatus = cl.getClaimStatus().getClaimStatusDesc();
        claimSubStatus = cl.getClaimSubStatus().getClaimSubstatusDesc();

        AccountKeeper ak = claimMisc.getAccountKeeperByClaimNumber(claimNumber);

        Address addr = null;
        if (ak != null) {
            addr = claimMisc.getAddressByClaimNumber(ak.getAccountKeeperId());
        }

        RepairFacilityContactPerson rfcp = claimMisc.getRepairFacilityContactPersonByClaimNumber(claimNumber);

        RepairFacility rf = claimMisc.getRepairFacilityByClaimNumber(claimNumber);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String roDate = null;
        if (cl.getRepairOrderDate() != null) {
            roDate = df.format(cl.getRepairOrderDate());
        }

        CfRegion region = claimMisc.getRegionByClaimNumber(claimNumber);

        AccountKeeper ako = claimMisc.getAccountKeeperByClaimOwnedBy(claimNumber);
        if (ako != null) {
            ownedBy = ako.getAccountKeeperName();
        }

        String inceptionDate = null;
        if (cl.getInceptionDate() != null) {
            inceptionDate = df.format(cl.getInceptionDate());
        }
        LOGGER.info("return CGANC");
        String assignedTo = null;
        if (cl.getOwnedByFk() != null) {
            assignedTo = cl.getOwnedByFk().getUserName();
        }
        return new ClaimGeneralANC(
                cl.getClaimId(),
                claimNumber,
                cl.getCurrentOdometer(),
                Optional.ofNullable(cl.getRepairOrderNumber()).orElse(""),
                (ak != null ? ak.getAccountKeeperName() : ""),
                cl.getReason(),
                inceptionDate,
                cl.getAmount(),
                assignedTo);

    }

    public boolean isVSC() {
        if (claim != null) {
            return claimMisc.isVSC(claim);
        } else {
            return claimMisc.isVSC(contract);
        }

    }

    public boolean isPPM() {
        if (claim != null) {
            return claimMisc.isPPM(claim);
        } else {
            return claimMisc.isPPM(contract);
        }

    }

    public boolean isANC() {
        if (claim != null) {
            return claimMisc.isANC(claim);
        } else {
            return claimMisc.isANC(contract);
        }

    }

    public boolean isGAP() {
        if (claim != null) {
            return claimMisc.isGAP(claim);
        } else {
            return claimMisc.isGAP(contract);
        }

    }

    public boolean isClaimEditable() {

        if (VSCreadonly || ANCreadonly || PPMreadonly || GAPreadonly) {
            //LOGGER.info("isClaimEditable is false");
            return false;
        } else {
            //LOGGER.info("isClaimEditable is true");
            return true;
        }
    }

    public void checkMaxOdometer(Integer iodometer) {
        Integer maxOdometer = claimMisc.getMaxOdometerByContractNo(contractNo);
        LOGGER.info("maxOdometer=" + maxOdometer);
        if (iodometer < maxOdometer) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "the odometer entered should be greater than current odometer for this vehicle: ", maxOdometer.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void passVSCodometer() {
        checkMaxOdometer(cg.odometer);
    }

    public void passANCodometer() {

        checkMaxOdometer(cganc.odometer);
    }

    public void passPPMOdometer() {

        ppmodometer = cganc.odometer;
        checkMaxOdometer(ppmodometer);
    }

    public List<ClaimGeneral> createClaimViewByContractNo(String num) {

        contractNo = num;
        LOGGER.info("in createCalimViewByContractNo=" + num);
        contract = claimMisc.getContractsByNo(num);

        List<ClaimGeneral> cgList = new ArrayList<>();

        //Contracts contract = em.createNamedQuery("Contracts.findByContractNo", Contracts.class).setParameter("contractNo", contractNo).getSingleResult();
        List<RepairFacilityToDealerRel> rfdrList = claimMisc.getRepairFacilityToDealerRelByContractNo(contractNo);

        Integer repairFacilityId = null;
        for (RepairFacilityToDealerRel rfdr : rfdrList) {
            //repairFacilityId = rfdr.getRepairFacilityToDealerRelPK().getRepairFacilityId();
            repairFacilityId = rfdr.getRepairFacilityId().getRepairFacilityId();

            AccountKeeper ak = em.createNamedQuery("AccountKeeper.findByAccountKeeperId", AccountKeeper.class).setParameter("accountKeeperId", repairFacilityId).getSingleResult();

            Address addr = null;
            CfRegion region = null;
            if (ak != null) {
                addr = claimMisc.getAddressByClaimNumber(ak.getAccountKeeperId());
                region = claimMisc.getRegionByAddressId(addr.getAddressId());
            }

            RepairFacility rf = claimMisc.getRepairFacilityById(repairFacilityId);
            RepairFacilityContactPerson rfcp = null;
            if (rf != null) {
                LOGGER.info("rfid=" + rf.getRepairFacilityId());
                List<RepairFacilityContactPerson> rfcpList = claimMisc.getRepairFacilityContactPersonById(rf.getRepairFacilityId());
                if (rfcpList != null && rfcpList.size() > 0) {
                    rfcp = rfcpList.get(0); // visited later ....................
                }
            }

            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String roDate = df.format(claimMisc.getCurrentDate());

            cgList.add(new ClaimGeneral(null,
                    (addr != null ? addr.getPhoneNumber() : ""),
                    (rfcp != null ? rfcp.getName() : ""),
                    (rfcp != null ? rfcp.getPhoneNumber() : ""),
                    (addr != null ? addr.getFax() : ""),
                    "TBD",
                    null,
                    null,
                    roDate,
                    (ak != null ? ak.getAccountKeeperName() : ""),
                    (addr != null ? addr.getAddress1() : ""),
                    (addr != null ? addr.getAddress2() : ""),
                    (addr != null ? addr.getZipCode() : ""),
                    (addr != null ? addr.getCity() : ""),
                    (region != null ? region.getRegionCode() : ""),
                    (rf != null ? rf.getLaborRate() : null),
                    (rf != null ? rf.getLaborTaxPct() : null),
                    (rf != null ? rf.getPartsTaxPct() : null),
                    (rf != null ? rf.getPartWarrantyMonths() : null),
                    (rf != null ? rf.getPartWarrantyMiles() : null)));
        }
        return cgList;
    }

    public List<ClaimGeneralANC> createClaimViewByContractNoANC(String num) {

        contractNo = num;
        LOGGER.info("in createCalimViewByContractNoANC=" + num);
        contract = claimMisc.getContractsByNo(num);

        List<ClaimGeneralANC> cgList = new ArrayList<>();

        Contracts contract = em.createNamedQuery("Contracts.findByContractNo", Contracts.class).setParameter("contractNo", contractNo).getSingleResult();

        List<RepairFacilityToDealerRel> rfdrList = claimMisc.getRepairFacilityToDealerRelByContractNo(contractNo);

        Integer repairFacilityId = null;
        for (RepairFacilityToDealerRel rfdr : rfdrList) {
            //repairFacilityId = rfdr.getRepairFacilityToDealerRelPK().getRepairFacilityId();
            repairFacilityId = rfdr.getRepairFacilityId().getRepairFacilityId();

            AccountKeeper ak = em.createNamedQuery("AccountKeeper.findByAccountKeeperId", AccountKeeper.class).setParameter("accountKeeperId", repairFacilityId).getSingleResult();

            Address addr = null;
            CfRegion region = null;
            if (ak != null) {
                addr = claimMisc.getAddressByClaimNumber(ak.getAccountKeeperId());
                region = claimMisc.getRegionByAddressId(addr.getAddressId());
            }

            RepairFacility rf = claimMisc.getRepairFacilityById(repairFacilityId);
            RepairFacilityContactPerson rfcp = null;
            if (rf != null) {
                LOGGER.info("rfid=" + rf.getRepairFacilityId());
                List<RepairFacilityContactPerson> rfcpList = claimMisc.getRepairFacilityContactPersonById(rf.getRepairFacilityId());
                if (rfcpList != null && rfcpList.size() > 0) {
                    rfcp = rfcpList.get(0); // visited later ....................
                }
            }

            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String inceptionDate = df.format(claimMisc.getCurrentDate());

            cgList.add(new ClaimGeneralANC(
                    null,
                    "TBD",
                    null,
                    null,
                    (ak != null ? ak.getAccountKeeperName() : ""),
                    null,
                    inceptionDate,
                    null,
                    null));

            cg = new ClaimGeneral(null,
                    (addr != null ? addr.getPhoneNumber() : ""),
                    (rfcp != null ? rfcp.getName() : ""),
                    (rfcp != null ? rfcp.getPhoneNumber() : ""),
                    (addr != null ? addr.getFax() : ""),
                    "TBD",
                    null,
                    null,
                    inceptionDate,
                    (ak != null ? ak.getAccountKeeperName() : ""),
                    (addr != null ? addr.getAddress1() : ""),
                    (addr != null ? addr.getAddress2() : ""),
                    (addr != null ? addr.getZipCode() : ""),
                    (addr != null ? addr.getCity() : ""),
                    (region != null ? region.getRegionCode() : ""),
                    (rf != null ? rf.getLaborRate() : null),
                    (rf != null ? rf.getLaborTaxPct() : null),
                    (rf != null ? rf.getPartsTaxPct() : null),
                    (rf != null ? rf.getPartWarrantyMonths() : null),
                    (rf != null ? rf.getPartWarrantyMiles() : null));

            break; // later

        }
        return cgList;
    }

    public void save() {
        LOGGER.info("in saving...");
        //Claim claim = new Claim();

        try {

            utx.begin();
            Integer akid = null;
            Integer repairFacilityContactIdFk = null;
            Integer ownedByFk = null;
            RepairFacilityContactPerson rfcp = null;
            if (cg.getRfName() != null && cg.getRfName().length() > 0) {
                LOGGER.info("rfname=" + cg.getRfName());

                AccountKeeper ak = claimMisc.getAccoutKeeperByRFname(cg.getRfName());
                akid = ak.getAccountKeeperId();
                LOGGER.info("accountKeeperId=" + akid);

                if (cg.getContactName() != null && cg.getContactName().length() > 0) {

                    List<RepairFacilityContactPerson> rfcps = (List<RepairFacilityContactPerson>) em.createNativeQuery("select b.* from RepairFacility a, RepairFacilityContactPerson b where a.repairFacilityId=?1 and a.repairFacilityId=b.repairFacilityIdFk", RepairFacilityContactPerson.class).setParameter(1, akid).getResultList();
                    LOGGER.info("length of rfcps=" + rfcps.size());
                    if (rfcps.size() != 0) {
                        for (RepairFacilityContactPerson rp : rfcps) {
                            LOGGER.info("rp=" + rp.getPhoneNumber());
                            rp.setName(cg.getContactName());
                            rp.setPhoneNumber(cg.getContactPhone());
                            em.merge(rp);
                            rfcp = rp;
                            repairFacilityContactIdFk = rfcp.getRepairFacilityContactPersonId();
                            LOGGER.info("merge rfcp");
                            LOGGER.info("rfcp=" + repairFacilityContactIdFk);
                            break;
                        }
                    } else {    // add new record in RepairFacilityContactPerson
                        LOGGER.info("new rfcp, 2=" + cg.getContactPhone());
                        Query query = em.createNativeQuery("insert into RepairFacilityContactPerson (name, PhoneNumber, repairFacilityIdFk) values (?1, ?2, ?3)");
                        query.setParameter(1, cg.getContactName());
                        query.setParameter(2, cg.getContactPhone());
                        query.setParameter(3, akid);
                        int ret = query.executeUpdate();
                        LOGGER.info("insert to rfcp, ret=" + ret);
                        repairFacilityContactIdFk = ((BigDecimal) em.createNativeQuery("select IDENT_CURRENT('RepairFacilityContactPerson')").getSingleResult()).intValueExact();
                        rfcp = em.createNamedQuery("RepairFacilityContactPerson.findByRepairFacilityContactPersonId", RepairFacilityContactPerson.class).setParameter("repairFacilityContactPersonId", repairFacilityContactIdFk).getSingleResult();
                        LOGGER.info("latest id=" + repairFacilityContactIdFk);
                    }
                }
            }

            String assignTo = cg.getAssignedTo();
            if (assignTo != null && assignTo.length() > 0) {

                UserMember um = claimMisc.getUserMemberByName(assignTo);
                ownedByFk = um.getUserMemberId();

            } else {
                UserMember um = claimMisc.getUserMemberByName(claimMisc.getCurrentUser());
                ownedByFk = um.getUserMemberId();
            }
            LOGGER.info("ownedByFk=" + ownedByFk);

            if (cg.getClaimNumber().contains("TBD")) {  // New Claim
                int ret;
                LOGGER.info("inserting new claim...");
                LOGGER.info("roDate=" + cg.getRoDate());

                Integer userMemberId = 1; // in the future, need to be get from Login

                Query query = em.createNativeQuery("insert into Claim (currentOdometer, repairOrderNumber, repairOrderDate, repairFacilityIdFk, repairFacilityContactIdFk, productTypeIdFk, claimStatus, claimSubStatus, contractIdFk, paidToFk, inceptionDate, ownedByFk) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)");
                query.setParameter(1, cg.getOdometer());
                query.setParameter(2, cg.getRoNumber());
                query.setParameter(3, claimMisc.convSTDateWS(cg.getRoDate()));
                LOGGER.info("akid=" + akid);
                LOGGER.info("repairFacilityContactIdFk=" + repairFacilityContactIdFk);
                query.setParameter(4, akid);
                query.setParameter(5, repairFacilityContactIdFk);
                query.setParameter(6, 1);   // VSC
                query.setParameter(7, "1");   //  1, Pending
                query.setParameter(8, "1");   //  1, Pending Approval
                LOGGER.info("contractIdFk===" + contract.getContractId());
                query.setParameter(9, contract.getContractId());
                query.setParameter(10, akid);
                query.setParameter(11, claimMisc.getCurrnetDateTime());
                query.setParameter(12, ownedByFk);
                ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                Integer claimId = null;
                if (ret == 1) {
                    claimId = claimMisc.getLatestInsertedId("Claim");
                    LOGGER.info("claimId===" + claimId);
                    claim = claimMisc.getClaimById(claimId);
                    claimNumber = claim.getClaimNumber();
                    LOGGER.info("claimNumber=" + claimNumber);
                    claimStatus = claim.getClaimStatus().getClaimStatusDesc();
                    claimSubStatus = claim.getClaimSubStatus().getClaimSubstatusDesc();

                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "new ClaimNumber is created: ", claimNumber);
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                    LOGGER.info("call createClaimView");

                    cg = createClaimView(claimNumber);

                    LOGGER.info("new claimNumber=" + cg.getClaimNumber());
                    //RequestContext.getCurrentInstance().update("tabs:0:claimGeneralForm");
                }
            } else {    // Existing Claim
                Claim claim = claimMisc.getClaimByNumber(cg.getClaimNumber());
                LOGGER.info("CLAIM id=" + claim.getClaimId());
                claim.setCurrentOdometer(cg.getOdometer());
                claim.setRepairOrderNumber(cg.getRoNumber());
                claim.setRepairOrderDate(claimMisc.convSTDateWS(cg.getRoDate()));
                RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                claim.setRepairFacilityIdFk(rf);
                claim.setRepairFacilityContactIdFk(rfcp);
                claim.setPaidToFk(claimMisc.getAccountKeeperById(akid));
                em.merge(claim);
                LOGGER.info("merge claim");
            }

            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }

    }

    public void pendingClaim() {
        LOGGER.info("in pendingClaim...");
        try {

            if (!cganc.getClaimNumber().contains("TBD")) {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();

                claimMisc.pendingClaim(claim);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        } catch (Exception ex) {
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

        }
    }

    public void deniedClaim() {
        LOGGER.info("in deniedClaim...");
        try {

            if (!cganc.getClaimNumber().contains("TBD")) {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();

                claimMisc.deniedClaim(claim);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        } catch (Exception ex) {
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());
        }
    }

    public void voidedClaim() {
        LOGGER.info("in voidedClaim...");
        try {

            if (!cganc.getClaimNumber().contains("TBD")) {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();

                claimMisc.voidedClaim(claim);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } catch (Exception ex) {
            LOGGER.info(ex.getStackTrace());
        }
    }

    public void approvedClaim() {
        LOGGER.info("in approvedClaim...");
        try {

            if (!cganc.getClaimNumber().contains("TBD")) {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();

                claimMisc.approvedClaim(claim);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } catch (Exception ex) {
            LOGGER.info("apporvedClaim has exception: \n" + ex.getMessage());
        }
    }

    public void saveANDreload() {
        LOGGER.info("in saveANDreload...");
        LOGGER.info("claimType=" + claimType);
        switch (claimType) {
            case "VSC":
                save();
                reloadGT();
                break;
            case "ANC":
                saveANC();
                reloadGTANC(claimNumber);
                break;
            case "PPM":
                savePPM();
                reloadGTANC(claimNumber);
                break;
            case "GAP":
                saveGAP();
                break;
            default:
                LOGGER.info("No claim type found, please verify......");
                break;
        }
    }

    public void saveANDclose() {
        LOGGER.info("in saveANDclose...");
        switch (claimType) {
            case "VSC":
                save();
                break;
            case "ANC":
                saveANC();
                break;
            case "PPM":
                savePPM();
                break;
            case "GAP":
                saveGAP();
                break;
            default:
                LOGGER.info("No claim type found, please verify......");
                break;
        }
        RequestContext.getCurrentInstance().closeDialog("claimTabView");
    }

    public void savePPM() {
        LOGGER.info("in saveANC...");
        try {
            utx.begin();

            Integer akid = null;
            Integer ownedByFk = null;
            Integer repairFacilityContactIdFk = null;
            RepairFacilityContactPerson rfcp = null;

            String paidTo = cganc.getPaidTo();

            if (paidTo != null && paidTo.length() > 0) {
                LOGGER.info("paidTo=" + paidTo);

                AccountKeeper ak = claimMisc.getAccoutKeeperByRFname(paidTo);
                akid = ak.getAccountKeeperId();
                LOGGER.info("accountKeeperId=" + akid);

            }

            String assignTo = cganc.getAssignedTo();
            if (assignTo != null && assignTo.length() > 0) {

                UserMember um = claimMisc.getUserMemberByName(assignTo);
                ownedByFk = um.getUserMemberId();
                LOGGER.info("ownedByFk=" + ownedByFk);

            }

            if (cganc.getClaimNumber().contains("TBD")) {  // New Claim
                int ret;
                LOGGER.info("inserting new claim...");

                Query query = em.createNativeQuery("insert into Claim (currentOdometer, repairOrderNumber, repairOrderDate, repairFacilityIdFk, productTypeIdFk, claimStatus, claimSubStatus, contractIdFk, paidToFk, inceptionDate, ownedByFk, reason, amount) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13)");
                LOGGER.info("odometer=" + cganc.getOdometer());
                LOGGER.info("odometer=" + cganc.getRoNumber());
                LOGGER.info("odometer=" + claimMisc.getCurrentDate());
                LOGGER.info("odometer=" + akid);
                LOGGER.info("odometer=" + claimMisc.getProductTypeByName("PPM").getProductTypeId());
                LOGGER.info("odometer=" + claimMisc.getClaimStatusByDesc("Pending").getClaimStatusInd());
                LOGGER.info("odometer=" + claimMisc.getClaimSubStatusByDesc("PendingApproval").getClaimSubstatusInd());
                LOGGER.info("odometer=" + contract.getContractId());
                LOGGER.info("odometer=" + claimMisc.getCurrnetDateTime());
                LOGGER.info("odometer=" + ownedByFk);
                LOGGER.info("odometer=" + cganc.getReason());
                LOGGER.info("odometer=" + cganc.getAmount());

                query.setParameter(1, cganc.getOdometer());
                query.setParameter(2, cganc.getRoNumber());
                query.setParameter(3, claimMisc.getCurrentDate());
                query.setParameter(4, akid);
                query.setParameter(5, claimMisc.getProductTypeByName("PPM").getProductTypeId());
                query.setParameter(6, claimMisc.getClaimStatusByDesc("Pending").getClaimStatusInd());   //  1, Pending
                query.setParameter(7, claimMisc.getClaimSubStatusByDesc("PendingApproval").getClaimSubstatusInd());   //  1, Pending Approval
                query.setParameter(8, contract.getContractId());
                query.setParameter(9, akid);
                query.setParameter(10, claimMisc.getCurrnetDateTime());
                query.setParameter(11, ownedByFk); // later
                query.setParameter(12, null);
                query.setParameter(13, cganc.getAmount());
                ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                Integer claimId = null;
                if (ret == 1) {
                    claimId = claimMisc.getLatestInsertedId("Claim");
                    claim = claimMisc.getClaimById(claimId);
                    claimNumber = claim.getClaimNumber();
                    LOGGER.info("claimNumber=" + claimNumber);
                    claimStatus = claim.getClaimStatus().getClaimStatusDesc();
                    claimSubStatus = claim.getClaimSubStatus().getClaimSubstatusDesc();

                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "new ClaimNumber is created: ", claimNumber);
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                    LOGGER.info("call createClaimViewANC");

                    cganc = createClaimViewANC(claimNumber);
                    LOGGER.info("new claimNumber=" + cganc.getClaimNumber());
                    //RequestContext.getCurrentInstance().update("tabs:0:claimGeneralForm");
                }
            } else {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();
                LOGGER.info("claimId=" + claim.getClaimId());
                claim.setCurrentOdometer(cganc.getOdometer());
                claim.setRepairOrderNumber(cganc.getRoNumber());
                RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                claim.setRepairFacilityIdFk(rf);
//                claim.setRepairFacilityContactIdFk(rfcp);
                claim.setPaidToFk(claimMisc.getAccountKeeperById(akid));
                claim.setOwnedByFk(claimMisc.getUserMemberByName(assignTo));
                claim.setAmount(cganc.getAmount());
                claim.setReason(cganc.getReason());
                em.merge(claim);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                LOGGER.info("merge claim");
            }

            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void saveGAP() {
    }

    public void saveANC() {
        LOGGER.info("in saveANC...");
        try {
            utx.begin();

            Integer akid = null;
            Integer ownedByFk = null;
            Integer repairFacilityContactIdFk = null;
            RepairFacilityContactPerson rfcp = null;

            String paidTo = cganc.getPaidTo();

            if (paidTo != null && paidTo.length() > 0) {
                LOGGER.info("paidTo=" + paidTo);

                AccountKeeper ak = claimMisc.getAccoutKeeperByRFname(paidTo);
                akid = ak.getAccountKeeperId();
                LOGGER.info("accountKeeperId=" + akid);

            }

            String assignTo = cganc.getAssignedTo();
            if (assignTo != null && assignTo.length() > 0) {

                UserMember um = claimMisc.getUserMemberByName(assignTo);
                ownedByFk = um.getUserMemberId();
                LOGGER.info("ownedByFk=" + ownedByFk);

            }

            if (cganc.getClaimNumber().contains("TBD")) {  // New Claim
                int ret;
                LOGGER.info("inserting new claim...");

                Query query = em.createNativeQuery("insert into Claim (currentOdometer, repairOrderNumber, repairOrderDate, repairFacilityIdFk, productTypeIdFk, claimStatus, claimSubStatus, contractIdFk, paidToFk, inceptionDate, ownedByFk, reason, amount) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13)");
                LOGGER.info("odometer=" + cganc.getOdometer());
                LOGGER.info("odometer=" + cganc.getRoNumber());
                LOGGER.info("odometer=" + claimMisc.getCurrentDate());
                LOGGER.info("odometer=" + akid);
                LOGGER.info("odometer=" + claimMisc.getProductTypeByName("ANC").getProductTypeId());
                LOGGER.info("odometer=" + claimMisc.getClaimStatusByDesc("Pending").getClaimStatusInd());
                LOGGER.info("odometer=" + claimMisc.getClaimSubStatusByDesc("PendingApproval").getClaimSubstatusInd());
                LOGGER.info("odometer=" + contract.getContractId());
                LOGGER.info("odometer=" + claimMisc.getCurrnetDateTime());
                LOGGER.info("odometer=" + ownedByFk);
                LOGGER.info("odometer=" + cganc.getReason());
                LOGGER.info("odometer=" + cganc.getAmount());

                query.setParameter(1, cganc.getOdometer());
                query.setParameter(2, cganc.getRoNumber());
                query.setParameter(3, claimMisc.getCurrentDate());
                query.setParameter(4, akid);
                query.setParameter(5, claimMisc.getProductTypeByName("ANC").getProductTypeId());
                query.setParameter(6, claimMisc.getClaimStatusByDesc("Pending").getClaimStatusInd());   //  1, Pending
                query.setParameter(7, claimMisc.getClaimSubStatusByDesc("PendingApproval").getClaimSubstatusInd());   //  1, Pending Approval
                query.setParameter(8, contract.getContractId());
                query.setParameter(9, akid);
                query.setParameter(10, claimMisc.getCurrnetDateTime());
                query.setParameter(11, ownedByFk); // later
                query.setParameter(12, cganc.getReason());
                query.setParameter(13, cganc.getAmount());
                ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                Integer claimId = null;
                if (ret == 1) {
                    claimId = claimMisc.getLatestInsertedId("Claim");
                    claim = claimMisc.getClaimById(claimId);
                    claimNumber = claim.getClaimNumber();
                    LOGGER.info("claimNumber=" + claimNumber);
                    claimStatus = claim.getClaimStatus().getClaimStatusDesc();
                    claimSubStatus = claim.getClaimSubStatus().getClaimSubstatusDesc();

                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "new ClaimNumber is created: ", claimNumber);
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                    LOGGER.info("call createClaimViewANC");

                    cganc = createClaimViewANC(claimNumber);
                    LOGGER.info("new claimNumber=" + cganc.getClaimNumber());
                    //RequestContext.getCurrentInstance().update("tabs:0:claimGeneralForm");
                }
            } else {    // Existing Claim
                claim = claimMisc.getClaimByNumber(cganc.getClaimNumber());
                claimNumber = cganc.getClaimNumber();
                LOGGER.info("claimId=" + claim.getClaimId());
                claim.setCurrentOdometer(cganc.getOdometer());
                claim.setRepairOrderNumber(cganc.getRoNumber());
                RepairFacility rf = em.createNamedQuery("RepairFacility.findByRepairFacilityId", RepairFacility.class).setParameter("repairFacilityId", akid).getSingleResult();
                claim.setRepairFacilityIdFk(rf);
//                claim.setRepairFacilityContactIdFk(rfcp);
                claim.setPaidToFk(claimMisc.getAccountKeeperById(akid));
                claim.setOwnedByFk(claimMisc.getUserMemberByName(assignTo));
                claim.setAmount(cganc.getAmount());
                claim.setReason(cganc.getReason());
                em.merge(claim);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, " Claim is updated: ", claimNumber);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                LOGGER.info("merge claim");
            }

            utx.commit();
            LOGGER.info("end of commit");
        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void reloadGT() {
        LOGGER.info("in claimGeneralServce, reloadGT, claimNumber=" + claimNumber);
        if (claimNumber != null && claimNumber.length() > 0) {
            LOGGER.info("in reloadGT=" + claimNumber);
            setClaimNumber(claimNumber);
            claim = claimMisc.getClaimByNumber(claimNumber);
            setClaim(claim);
            cg = createClaimView(claimNumber);
            //setCg(cg);
        }
    }

    public void reloadGTANC(String claimNumber) {
        if (claimNumber != null && claimNumber.length() > 0) {
            LOGGER.info("in reloadGTANC=" + claimNumber);
            setClaimNumber(claimNumber);
            claim = claimMisc.getClaimByNumber(claimNumber);
            setClaim(claim);
            cganc = createClaimViewANC(claimNumber);
            setCganc(cganc);
        }
    }

    public void reloadPPMup(String claimNumber) {
        LOGGER.info("reloadPPMup in construction");
    }

    public void vscDialogClosed(SelectEvent event) {
        LOGGER.info("in vscDialogClosed...");

        cg.setRfName(rfsearch.getRfName());
        cg.setRfPhone(rfsearch.getRfPhone());
        cg.setCity(rfsearch.getCity());
        cg.setState(rfsearch.getState());
        cg.setZip(rfsearch.getZip());
        cg.setLaborRate(rfsearch.getLaborRate());
        cg.setLaborTax(rfsearch.getLaborTax());
        cg.setPartTax(rfsearch.getPartTax());
        cg.setLine1(rfsearch.getAddress1());
        cg.setLine2(rfsearch.getAddress2());
    }

    public void ancDialogClosed(SelectEvent event) {
        LOGGER.info("in ancDialogClosed...");

        cganc.setPaidTo(rfsearch.getRfName());
        /*
        cg.setRfName(rfsearch.getRfName());
        cg.setRfPhone(rfsearch.getRfPhone());
        cg.setCity(rfsearch.getCity());
        cg.setState(rfsearch.getState());
        cg.setZip(rfsearch.getZip());
        cg.setLaborRate(rfsearch.getLaborRate());
        cg.setLaborTax(rfsearch.getLaborTax());
        cg.setPartTax(rfsearch.getPartTax());
        cg.setLine1(rfsearch.getAddress1());
        cg.setLine2(rfsearch.getAddress2());
         */
    }

    public void ancMSDialogClosed(SelectEvent event) {
        LOGGER.info("in ancMSDialogClosed...");

        cganc.setAssignedTo(memberSearch.getShortName());
    }

    public void ancUnAssign() {
        cganc.setAssignedTo("");
    }

    public void ancAssignSelf() {

    }

    public void vscMSDialogClosed(SelectEvent event) {
        LOGGER.info("in vscMSDialogClosed...");

        cg.setAssignedTo(memberSearch.getShortName());
    }

    public void vscUnAssign() {
        cg.setAssignedTo("");
    }

    public void vscViewNewEditFacilityClosed() {
        LOGGER.info("in vscViewNewEditFacilityClosed...");
        //cg.setOdometer(rfcg.getOdometer());
        //cg  = rfcg;
    }

    public void ancViewNewEditFacilityClosed() {
        LOGGER.info("in ancViewNewEditFacilityClosed...");
        cganc.setPaidTo(cg.getRfName());
    }

}
