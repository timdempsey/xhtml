/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "InsurerType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsurerType.findAll", query = "SELECT i FROM InsurerType i")
    , @NamedQuery(name = "InsurerType.findByInsurerTypeId", query = "SELECT i FROM InsurerType i WHERE i.insurerTypeId = :insurerTypeId")
    , @NamedQuery(name = "InsurerType.findByDescription", query = "SELECT i FROM InsurerType i WHERE i.description = :description")})
public class InsurerType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "InsurerTypeId")
    private Integer insurerTypeId;
    @Size(max = 30)
    @Column(name = "Description")
    private String description;

    public InsurerType() {
    }

    public InsurerType(Integer insurerTypeId) {
        this.insurerTypeId = insurerTypeId;
    }

    public Integer getInsurerTypeId() {
        return insurerTypeId;
    }

    public void setInsurerTypeId(Integer insurerTypeId) {
        this.insurerTypeId = insurerTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insurerTypeId != null ? insurerTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsurerType)) {
            return false;
        }
        InsurerType other = (InsurerType) object;
        if ((this.insurerTypeId == null && other.insurerTypeId != null) || (this.insurerTypeId != null && !this.insurerTypeId.equals(other.insurerTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.InsurerType[ insurerTypeId=" + insurerTypeId + " ]";
    }
    
}
