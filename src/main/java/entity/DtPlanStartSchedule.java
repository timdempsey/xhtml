/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanStartSchedule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanStartSchedule.findAll", query = "SELECT d FROM DtPlanStartSchedule d")
    , @NamedQuery(name = "DtPlanStartSchedule.findByStartScheduleId", query = "SELECT d FROM DtPlanStartSchedule d WHERE d.startScheduleId = :startScheduleId")
    , @NamedQuery(name = "DtPlanStartSchedule.findByDescription", query = "SELECT d FROM DtPlanStartSchedule d WHERE d.description = :description")})
public class DtPlanStartSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "startScheduleId")
    private Integer startScheduleId;
    @Size(max = 20)
    @Column(name = "description")
    private String description;

    public DtPlanStartSchedule() {
    }

    public DtPlanStartSchedule(Integer startScheduleId) {
        this.startScheduleId = startScheduleId;
    }

    public Integer getStartScheduleId() {
        return startScheduleId;
    }

    public void setStartScheduleId(Integer startScheduleId) {
        this.startScheduleId = startScheduleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (startScheduleId != null ? startScheduleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanStartSchedule)) {
            return false;
        }
        DtPlanStartSchedule other = (DtPlanStartSchedule) object;
        if ((this.startScheduleId == null && other.startScheduleId != null) || (this.startScheduleId != null && !this.startScheduleId.equals(other.startScheduleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanStartSchedule[ startScheduleId=" + startScheduleId + " ]";
    }
    
}
