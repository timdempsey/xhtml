/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "pbdfcService")
@ApplicationScoped
public class PBDForClaimService {

    /**
     * Creates a new instance of PBDForClaimService
     */
    public PBDForClaimService() {
    }

    private static final Logger LOGGER = LogManager.getLogger(PBDForClaimService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    PBDForClaim pbdfc;
    PBDForClaim selectedPBD;
    List<PBDForClaim> lpbds;
    Integer dealerId;

    public PBDForClaim getPbdfc() {
        return pbdfc;
    }

    public void setPbdfc(PBDForClaim pbdfc) {
        this.pbdfc = pbdfc;
    }

    public PBDForClaim getSelectedPBD() {
        return selectedPBD;
    }

    public void setSelectedPBD(PBDForClaim selectedPBD) {
        this.selectedPBD = selectedPBD;
    }

    public List<PBDForClaim> getLpbds() {
        return lpbds;
    }

    public void setLpbds(List<PBDForClaim> lpbds) {
        this.lpbds = lpbds;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public void reset() {
        selectedPBD = new PBDForClaim();
        pbdfc = new PBDForClaim();
        dealerId = null;
    }

    @PostConstruct
    public void init() {
        pbdfc = new PBDForClaim();
        dealerId = null;
    }
    
    public void generatePBD() {
        LOGGER.info("in generatePBD");
        lpbds = claimMisc.getPBDforClaim(pbdfc);
    }
    
    

}
