/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DealerGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DealerGroup.findAll", query = "SELECT d FROM DealerGroup d")
    , @NamedQuery(name = "DealerGroup.findByDealerGroupId", query = "SELECT d FROM DealerGroup d WHERE d.dealerGroupId = :dealerGroupId")
    , @NamedQuery(name = "DealerGroup.findByUpdateUserName", query = "SELECT d FROM DealerGroup d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DealerGroup.findByUpdateLast", query = "SELECT d FROM DealerGroup d WHERE d.updateLast = :updateLast")})
public class DealerGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerGroupId")
    private Integer dealerGroupId;
    @Size(max = 50)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "dealerGroupIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "dealerGroupIdFk")
    private Collection<UserMember> userMemberCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dealerGroupId")
    private Collection<RepairFacilityToDealerGroupRel> repairFacilityToDealerGroupRelCollection;
    @OneToMany(mappedBy = "dealerGroupConditionIdFk")
    private Collection<Disbursement> disbursementCollection;
    @JoinColumn(name = "dealerGroupId", referencedColumnName = "accountKeeperId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private AccountKeeper accountKeeper;
    @JoinColumn(name = "DisbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @OneToMany(mappedBy = "dealerGroupIdFk")
    private Collection<Dealer> dealerCollection;

    public DealerGroup() {
    }

    public DealerGroup(Integer dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }

    public Integer getDealerGroupId() {
        return dealerGroupId;
    }

    public void setDealerGroupId(Integer dealerGroupId) {
        this.dealerGroupId = dealerGroupId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<UserMember> getUserMemberCollection() {
        return userMemberCollection;
    }

    public void setUserMemberCollection(Collection<UserMember> userMemberCollection) {
        this.userMemberCollection = userMemberCollection;
    }

    @XmlTransient
    public Collection<RepairFacilityToDealerGroupRel> getRepairFacilityToDealerGroupRelCollection() {
        return repairFacilityToDealerGroupRelCollection;
    }

    public void setRepairFacilityToDealerGroupRelCollection(Collection<RepairFacilityToDealerGroupRel> repairFacilityToDealerGroupRelCollection) {
        this.repairFacilityToDealerGroupRelCollection = repairFacilityToDealerGroupRelCollection;
    }

    @XmlTransient
    public Collection<Disbursement> getDisbursementCollection() {
        return disbursementCollection;
    }

    public void setDisbursementCollection(Collection<Disbursement> disbursementCollection) {
        this.disbursementCollection = disbursementCollection;
    }

    public AccountKeeper getAccountKeeper() {
        return accountKeeper;
    }

    public void setAccountKeeper(AccountKeeper accountKeeper) {
        this.accountKeeper = accountKeeper;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    @XmlTransient
    public Collection<Dealer> getDealerCollection() {
        return dealerCollection;
    }

    public void setDealerCollection(Collection<Dealer> dealerCollection) {
        this.dealerCollection = dealerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dealerGroupId != null ? dealerGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DealerGroup)) {
            return false;
        }
        DealerGroup other = (DealerGroup) object;
        if ((this.dealerGroupId == null && other.dealerGroupId != null) || (this.dealerGroupId != null && !this.dealerGroupId.equals(other.dealerGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DealerGroup[ dealerGroupId=" + dealerGroupId + " ]";
    }
    
}
