/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMProductService")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMProductService.findAll", query = "SELECT p FROM PPMProductService p")
    , @NamedQuery(name = "PPMProductService.findByPPMProductServiceId", query = "SELECT p FROM PPMProductService p WHERE p.pPMProductServiceId = :pPMProductServiceId")
    , @NamedQuery(name = "PPMProductService.findByInternalName", query = "SELECT p FROM PPMProductService p WHERE p.internalName = :internalName")
    , @NamedQuery(name = "PPMProductService.findByDescription", query = "SELECT p FROM PPMProductService p WHERE p.description = :description")
    , @NamedQuery(name = "PPMProductService.findByMinimumDaysBetweenService", query = "SELECT p FROM PPMProductService p WHERE p.minimumDaysBetweenService = :minimumDaysBetweenService")
    , @NamedQuery(name = "PPMProductService.findByUpdateUserName", query = "SELECT p FROM PPMProductService p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMProductService.findByUpdateLast", query = "SELECT p FROM PPMProductService p WHERE p.updateLast = :updateLast")})
public class PPMProductService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMProductServiceId")
    private Integer pPMProductServiceId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "internalName")
    private String internalName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Column(name = "minimumDaysBetweenService")
    private Integer minimumDaysBetweenService;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "componentIdFk", referencedColumnName = "componentId")
    @ManyToOne
    private CfComponent componentIdFk;
    @JoinColumn(name = "productIdFk", referencedColumnName = "productId")
    @ManyToOne(optional = false)
    private Product productIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pPMProductServiceIdFk")
    private Collection<PPMPlanService> pPMPlanServiceCollection;

    public PPMProductService() {
    }

    public PPMProductService(Integer pPMProductServiceId) {
        this.pPMProductServiceId = pPMProductServiceId;
    }

    public PPMProductService(Integer pPMProductServiceId, String internalName, String description) {
        this.pPMProductServiceId = pPMProductServiceId;
        this.internalName = internalName;
        this.description = description;
    }

    public Integer getPPMProductServiceId() {
        return pPMProductServiceId;
    }

    public void setPPMProductServiceId(Integer pPMProductServiceId) {
        this.pPMProductServiceId = pPMProductServiceId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMinimumDaysBetweenService() {
        return minimumDaysBetweenService;
    }

    public void setMinimumDaysBetweenService(Integer minimumDaysBetweenService) {
        this.minimumDaysBetweenService = minimumDaysBetweenService;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfComponent getComponentIdFk() {
        return componentIdFk;
    }

    public void setComponentIdFk(CfComponent componentIdFk) {
        this.componentIdFk = componentIdFk;
    }

    public Product getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(Product productIdFk) {
        this.productIdFk = productIdFk;
    }

    @XmlTransient
    public Collection<PPMPlanService> getPPMPlanServiceCollection() {
        return pPMPlanServiceCollection;
    }

    public void setPPMPlanServiceCollection(Collection<PPMPlanService> pPMPlanServiceCollection) {
        this.pPMPlanServiceCollection = pPMPlanServiceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMProductServiceId != null ? pPMProductServiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMProductService)) {
            return false;
        }
        PPMProductService other = (PPMProductService) object;
        if ((this.pPMProductServiceId == null && other.pPMProductServiceId != null) || (this.pPMProductServiceId != null && !this.pPMProductServiceId.equals(other.pPMProductServiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMProductService[ pPMProductServiceId=" + pPMProductServiceId + " ]";
    }
    
}
