/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMTermService")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMTermService.findAll", query = "SELECT p FROM PPMTermService p")
    , @NamedQuery(name = "PPMTermService.findByPPMTermServiceId", query = "SELECT p FROM PPMTermService p WHERE p.pPMTermServiceId = :pPMTermServiceId")
    , @NamedQuery(name = "PPMTermService.findByServiceCount", query = "SELECT p FROM PPMTermService p WHERE p.serviceCount = :serviceCount")
    , @NamedQuery(name = "PPMTermService.findByUpdateUserName", query = "SELECT p FROM PPMTermService p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMTermService.findByUpdateLast", query = "SELECT p FROM PPMTermService p WHERE p.updateLast = :updateLast")})
public class PPMTermService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMTermServiceId")
    private Integer pPMTermServiceId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ServiceCount")
    private int serviceCount;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "PPMPlanServiceIdFk", referencedColumnName = "PPMPlanServiceId")
    @ManyToOne(optional = false)
    private PPMPlanService pPMPlanServiceIdFk;
    @JoinColumn(name = "termDetailIdFk", referencedColumnName = "termDetailId")
    @ManyToOne(optional = false)
    private TermDetail termDetailIdFk;

    public PPMTermService() {
    }

    public PPMTermService(Integer pPMTermServiceId) {
        this.pPMTermServiceId = pPMTermServiceId;
    }

    public PPMTermService(Integer pPMTermServiceId, int serviceCount) {
        this.pPMTermServiceId = pPMTermServiceId;
        this.serviceCount = serviceCount;
    }

    public Integer getPPMTermServiceId() {
        return pPMTermServiceId;
    }

    public void setPPMTermServiceId(Integer pPMTermServiceId) {
        this.pPMTermServiceId = pPMTermServiceId;
    }

    public int getServiceCount() {
        return serviceCount;
    }

    public void setServiceCount(int serviceCount) {
        this.serviceCount = serviceCount;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public PPMPlanService getPPMPlanServiceIdFk() {
        return pPMPlanServiceIdFk;
    }

    public void setPPMPlanServiceIdFk(PPMPlanService pPMPlanServiceIdFk) {
        this.pPMPlanServiceIdFk = pPMPlanServiceIdFk;
    }

    public TermDetail getTermDetailIdFk() {
        return termDetailIdFk;
    }

    public void setTermDetailIdFk(TermDetail termDetailIdFk) {
        this.termDetailIdFk = termDetailIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMTermServiceId != null ? pPMTermServiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMTermService)) {
            return false;
        }
        PPMTermService other = (PPMTermService) object;
        if ((this.pPMTermServiceId == null && other.pPMTermServiceId != null) || (this.pPMTermServiceId != null && !this.pPMTermServiceId.equals(other.pPMTermServiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMTermService[ pPMTermServiceId=" + pPMTermServiceId + " ]";
    }
    
}
