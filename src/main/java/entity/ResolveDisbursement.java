/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ResolveDisbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResolveDisbursement.findAll", query = "SELECT r FROM ResolveDisbursement r")
    , @NamedQuery(name = "ResolveDisbursement.findByResolveDisbursementId", query = "SELECT r FROM ResolveDisbursement r WHERE r.resolveDisbursementId = :resolveDisbursementId")
    , @NamedQuery(name = "ResolveDisbursement.findByUpdateUserName", query = "SELECT r FROM ResolveDisbursement r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ResolveDisbursement.findByUpdateLast", query = "SELECT r FROM ResolveDisbursement r WHERE r.updateLast = :updateLast")})
public class ResolveDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "resolveDisbursementId")
    private Integer resolveDisbursementId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ContractResolveIdFk", referencedColumnName = "ContractResolveId")
    @ManyToOne
    private ContractResolve contractResolveIdFk;
    @JoinColumn(name = "holdDisbursementIdFk", referencedColumnName = "holdDisbursementId")
    @ManyToOne
    private HoldDisbursement holdDisbursementIdFk;
    @JoinColumn(name = "resolveDisbursementId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public ResolveDisbursement() {
    }

    public ResolveDisbursement(Integer resolveDisbursementId) {
        this.resolveDisbursementId = resolveDisbursementId;
    }

    public Integer getResolveDisbursementId() {
        return resolveDisbursementId;
    }

    public void setResolveDisbursementId(Integer resolveDisbursementId) {
        this.resolveDisbursementId = resolveDisbursementId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ContractResolve getContractResolveIdFk() {
        return contractResolveIdFk;
    }

    public void setContractResolveIdFk(ContractResolve contractResolveIdFk) {
        this.contractResolveIdFk = contractResolveIdFk;
    }

    public HoldDisbursement getHoldDisbursementIdFk() {
        return holdDisbursementIdFk;
    }

    public void setHoldDisbursementIdFk(HoldDisbursement holdDisbursementIdFk) {
        this.holdDisbursementIdFk = holdDisbursementIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resolveDisbursementId != null ? resolveDisbursementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResolveDisbursement)) {
            return false;
        }
        ResolveDisbursement other = (ResolveDisbursement) object;
        if ((this.resolveDisbursementId == null && other.resolveDisbursementId != null) || (this.resolveDisbursementId != null && !this.resolveDisbursementId.equals(other.resolveDisbursementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ResolveDisbursement[ resolveDisbursementId=" + resolveDisbursementId + " ]";
    }
    
}
