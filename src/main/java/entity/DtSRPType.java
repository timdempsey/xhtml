/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtSRPType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtSRPType.findAll", query = "SELECT d FROM DtSRPType d")
    , @NamedQuery(name = "DtSRPType.findBySRPTypeId", query = "SELECT d FROM DtSRPType d WHERE d.sRPTypeId = :sRPTypeId")
    , @NamedQuery(name = "DtSRPType.findByDescription", query = "SELECT d FROM DtSRPType d WHERE d.description = :description")})
public class DtSRPType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRPTypeId")
    private Integer sRPTypeId;
    @Size(max = 60)
    @Column(name = "Description")
    private String description;

    public DtSRPType() {
    }

    public DtSRPType(Integer sRPTypeId) {
        this.sRPTypeId = sRPTypeId;
    }

    public Integer getSRPTypeId() {
        return sRPTypeId;
    }

    public void setSRPTypeId(Integer sRPTypeId) {
        this.sRPTypeId = sRPTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sRPTypeId != null ? sRPTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtSRPType)) {
            return false;
        }
        DtSRPType other = (DtSRPType) object;
        if ((this.sRPTypeId == null && other.sRPTypeId != null) || (this.sRPTypeId != null && !this.sRPTypeId.equals(other.sRPTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtSRPType[ sRPTypeId=" + sRPTypeId + " ]";
    }
    
}
