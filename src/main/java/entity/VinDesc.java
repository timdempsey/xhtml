/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "VinDesc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VinDesc.findAll", query = "SELECT v FROM VinDesc v")
    , @NamedQuery(name = "VinDesc.findByVinDescId", query = "SELECT v FROM VinDesc v WHERE v.vinDescId = :vinDescId")
    , @NamedQuery(name = "VinDesc.findByVinPartial", query = "SELECT v FROM VinDesc v WHERE v.vinPartial = :vinPartial")
    , @NamedQuery(name = "VinDesc.findByVehicleYear", query = "SELECT v FROM VinDesc v WHERE v.vehicleYear = :vehicleYear")
    , @NamedQuery(name = "VinDesc.findByVehicleType", query = "SELECT v FROM VinDesc v WHERE v.vehicleType = :vehicleType")
    , @NamedQuery(name = "VinDesc.findByMake", query = "SELECT v FROM VinDesc v WHERE v.make = :make")
    , @NamedQuery(name = "VinDesc.findByModel", query = "SELECT v FROM VinDesc v WHERE v.model = :model")
    , @NamedQuery(name = "VinDesc.findBySeries", query = "SELECT v FROM VinDesc v WHERE v.series = :series")
    , @NamedQuery(name = "VinDesc.findByDriveType", query = "SELECT v FROM VinDesc v WHERE v.driveType = :driveType")
    , @NamedQuery(name = "VinDesc.findByFuelType", query = "SELECT v FROM VinDesc v WHERE v.fuelType = :fuelType")
    , @NamedQuery(name = "VinDesc.findByFuelDelivery", query = "SELECT v FROM VinDesc v WHERE v.fuelDelivery = :fuelDelivery")
    , @NamedQuery(name = "VinDesc.findByFuelTurbineDelivery", query = "SELECT v FROM VinDesc v WHERE v.fuelTurbineDelivery = :fuelTurbineDelivery")
    , @NamedQuery(name = "VinDesc.findByCylinders", query = "SELECT v FROM VinDesc v WHERE v.cylinders = :cylinders")
    , @NamedQuery(name = "VinDesc.findByTonRating", query = "SELECT v FROM VinDesc v WHERE v.tonRating = :tonRating")
    , @NamedQuery(name = "VinDesc.findByCubicCentimeterDisplacement", query = "SELECT v FROM VinDesc v WHERE v.cubicCentimeterDisplacement = :cubicCentimeterDisplacement")
    , @NamedQuery(name = "VinDesc.findByCubicInchDisplacement", query = "SELECT v FROM VinDesc v WHERE v.cubicInchDisplacement = :cubicInchDisplacement")
    , @NamedQuery(name = "VinDesc.findByVehicleWeightRating", query = "SELECT v FROM VinDesc v WHERE v.vehicleWeightRating = :vehicleWeightRating")
    , @NamedQuery(name = "VinDesc.findByGvwr", query = "SELECT v FROM VinDesc v WHERE v.gvwr = :gvwr")
    , @NamedQuery(name = "VinDesc.findByCountryOfOrigin", query = "SELECT v FROM VinDesc v WHERE v.countryOfOrigin = :countryOfOrigin")
    , @NamedQuery(name = "VinDesc.findBySaleCountryDesc", query = "SELECT v FROM VinDesc v WHERE v.saleCountryDesc = :saleCountryDesc")
    , @NamedQuery(name = "VinDesc.findByBasicManufacturerWarrantyMileage", query = "SELECT v FROM VinDesc v WHERE v.basicManufacturerWarrantyMileage = :basicManufacturerWarrantyMileage")
    , @NamedQuery(name = "VinDesc.findByBasicManufacturerWarrantyTerm", query = "SELECT v FROM VinDesc v WHERE v.basicManufacturerWarrantyTerm = :basicManufacturerWarrantyTerm")
    , @NamedQuery(name = "VinDesc.findByMsrp", query = "SELECT v FROM VinDesc v WHERE v.msrp = :msrp")
    , @NamedQuery(name = "VinDesc.findByLiterDisplacemant", query = "SELECT v FROM VinDesc v WHERE v.literDisplacemant = :literDisplacemant")
    , @NamedQuery(name = "VinDesc.findByPowerTrainWarrantyMonths", query = "SELECT v FROM VinDesc v WHERE v.powerTrainWarrantyMonths = :powerTrainWarrantyMonths")
    , @NamedQuery(name = "VinDesc.findByPowerTrainWarrantyMiles", query = "SELECT v FROM VinDesc v WHERE v.powerTrainWarrantyMiles = :powerTrainWarrantyMiles")
    , @NamedQuery(name = "VinDesc.findByRustWarrantyMonths", query = "SELECT v FROM VinDesc v WHERE v.rustWarrantyMonths = :rustWarrantyMonths")
    , @NamedQuery(name = "VinDesc.findByRustWarrantyMiles", query = "SELECT v FROM VinDesc v WHERE v.rustWarrantyMiles = :rustWarrantyMiles")
    , @NamedQuery(name = "VinDesc.findByApprovedInd", query = "SELECT v FROM VinDesc v WHERE v.approvedInd = :approvedInd")
    , @NamedQuery(name = "VinDesc.findByOverride", query = "SELECT v FROM VinDesc v WHERE v.override = :override")
    , @NamedQuery(name = "VinDesc.findByMotorCycleUsage", query = "SELECT v FROM VinDesc v WHERE v.motorCycleUsage = :motorCycleUsage")
    , @NamedQuery(name = "VinDesc.findByUpdateUserName", query = "SELECT v FROM VinDesc v WHERE v.updateUserName = :updateUserName")
    , @NamedQuery(name = "VinDesc.findByUpdateLast", query = "SELECT v FROM VinDesc v WHERE v.updateLast = :updateLast")})
public class VinDesc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinDescId")
    private Integer vinDescId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "vinPartial")
    private String vinPartial;
    @Column(name = "vehicleYear")
    private Integer vehicleYear;
    @Size(max = 20)
    @Column(name = "vehicleType")
    private String vehicleType;
    @Size(max = 50)
    @Column(name = "make")
    private String make;
    @Size(max = 50)
    @Column(name = "model")
    private String model;
    @Size(max = 250)
    @Column(name = "series")
    private String series;
    @Size(max = 25)
    @Column(name = "driveType")
    private String driveType;
    @Size(max = 50)
    @Column(name = "fuelType")
    private String fuelType;
    @Size(max = 50)
    @Column(name = "fuelDelivery")
    private String fuelDelivery;
    @Size(max = 50)
    @Column(name = "fuelTurbineDelivery")
    private String fuelTurbineDelivery;
    @Size(max = 2)
    @Column(name = "cylinders")
    private String cylinders;
    @Size(max = 50)
    @Column(name = "tonRating")
    private String tonRating;
    @Size(max = 20)
    @Column(name = "cubicCentimeterDisplacement")
    private String cubicCentimeterDisplacement;
    @Size(max = 20)
    @Column(name = "cubicInchDisplacement")
    private String cubicInchDisplacement;
    @Size(max = 20)
    @Column(name = "vehicleWeightRating")
    private String vehicleWeightRating;
    @Column(name = "gvwr")
    private Integer gvwr;
    @Size(max = 50)
    @Column(name = "countryOfOrigin")
    private String countryOfOrigin;
    @Size(max = 50)
    @Column(name = "saleCountryDesc")
    private String saleCountryDesc;
    @Size(max = 50)
    @Column(name = "basicManufacturerWarrantyMileage")
    private String basicManufacturerWarrantyMileage;
    @Column(name = "basicManufacturerWarrantyTerm")
    private Integer basicManufacturerWarrantyTerm;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "msrp")
    private BigDecimal msrp;
    @Size(max = 50)
    @Column(name = "literDisplacemant")
    private String literDisplacemant;
    @Column(name = "powerTrainWarrantyMonths")
    private Integer powerTrainWarrantyMonths;
    @Size(max = 50)
    @Column(name = "powerTrainWarrantyMiles")
    private String powerTrainWarrantyMiles;
    @Column(name = "rustWarrantyMonths")
    private Integer rustWarrantyMonths;
    @Size(max = 50)
    @Column(name = "rustWarrantyMiles")
    private String rustWarrantyMiles;
    @Column(name = "approvedInd")
    private Boolean approvedInd;
    @Column(name = "override")
    private Boolean override;
    @Size(max = 50)
    @Column(name = "motorCycleUsage")
    private String motorCycleUsage;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "vinDescIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "vinDesc")
    private VinOverride vinOverride;
    @JoinColumn(name = "countryOfOriginIdFk", referencedColumnName = "countryOfOriginId")
    @ManyToOne
    private CountryOfOrigin countryOfOriginIdFk;
    @JoinColumn(name = "fuelDeliveryIdFk", referencedColumnName = "fuelDeliveryId")
    @ManyToOne
    private FuelDelivery fuelDeliveryIdFk;
    @JoinColumn(name = "fuelTypeIdFk", referencedColumnName = "fuelTypeId")
    @ManyToOne
    private FuelType fuelTypeIdFk;
    @JoinColumn(name = "makeIdFk", referencedColumnName = "makeId")
    @ManyToOne
    private Make makeIdFk;
    @JoinColumn(name = "modelIdFk", referencedColumnName = "ModelId")
    @ManyToOne
    private Model modelIdFk;
    @JoinColumn(name = "seriesIdFk", referencedColumnName = "seriesId")
    @ManyToOne
    private Series seriesIdFk;

    public VinDesc() {
    }

    public VinDesc(Integer vinDescId) {
        this.vinDescId = vinDescId;
    }

    public VinDesc(Integer vinDescId, String vinPartial) {
        this.vinDescId = vinDescId;
        this.vinPartial = vinPartial;
    }

    public Integer getVinDescId() {
        return vinDescId;
    }

    public void setVinDescId(Integer vinDescId) {
        this.vinDescId = vinDescId;
    }

    public String getVinPartial() {
        return vinPartial;
    }

    public void setVinPartial(String vinPartial) {
        this.vinPartial = vinPartial;
    }

    public Integer getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelDelivery() {
        return fuelDelivery;
    }

    public void setFuelDelivery(String fuelDelivery) {
        this.fuelDelivery = fuelDelivery;
    }

    public String getFuelTurbineDelivery() {
        return fuelTurbineDelivery;
    }

    public void setFuelTurbineDelivery(String fuelTurbineDelivery) {
        this.fuelTurbineDelivery = fuelTurbineDelivery;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getTonRating() {
        return tonRating;
    }

    public void setTonRating(String tonRating) {
        this.tonRating = tonRating;
    }

    public String getCubicCentimeterDisplacement() {
        return cubicCentimeterDisplacement;
    }

    public void setCubicCentimeterDisplacement(String cubicCentimeterDisplacement) {
        this.cubicCentimeterDisplacement = cubicCentimeterDisplacement;
    }

    public String getCubicInchDisplacement() {
        return cubicInchDisplacement;
    }

    public void setCubicInchDisplacement(String cubicInchDisplacement) {
        this.cubicInchDisplacement = cubicInchDisplacement;
    }

    public String getVehicleWeightRating() {
        return vehicleWeightRating;
    }

    public void setVehicleWeightRating(String vehicleWeightRating) {
        this.vehicleWeightRating = vehicleWeightRating;
    }

    public Integer getGvwr() {
        return gvwr;
    }

    public void setGvwr(Integer gvwr) {
        this.gvwr = gvwr;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getSaleCountryDesc() {
        return saleCountryDesc;
    }

    public void setSaleCountryDesc(String saleCountryDesc) {
        this.saleCountryDesc = saleCountryDesc;
    }

    public String getBasicManufacturerWarrantyMileage() {
        return basicManufacturerWarrantyMileage;
    }

    public void setBasicManufacturerWarrantyMileage(String basicManufacturerWarrantyMileage) {
        this.basicManufacturerWarrantyMileage = basicManufacturerWarrantyMileage;
    }

    public Integer getBasicManufacturerWarrantyTerm() {
        return basicManufacturerWarrantyTerm;
    }

    public void setBasicManufacturerWarrantyTerm(Integer basicManufacturerWarrantyTerm) {
        this.basicManufacturerWarrantyTerm = basicManufacturerWarrantyTerm;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public String getLiterDisplacemant() {
        return literDisplacemant;
    }

    public void setLiterDisplacemant(String literDisplacemant) {
        this.literDisplacemant = literDisplacemant;
    }

    public Integer getPowerTrainWarrantyMonths() {
        return powerTrainWarrantyMonths;
    }

    public void setPowerTrainWarrantyMonths(Integer powerTrainWarrantyMonths) {
        this.powerTrainWarrantyMonths = powerTrainWarrantyMonths;
    }

    public String getPowerTrainWarrantyMiles() {
        return powerTrainWarrantyMiles;
    }

    public void setPowerTrainWarrantyMiles(String powerTrainWarrantyMiles) {
        this.powerTrainWarrantyMiles = powerTrainWarrantyMiles;
    }

    public Integer getRustWarrantyMonths() {
        return rustWarrantyMonths;
    }

    public void setRustWarrantyMonths(Integer rustWarrantyMonths) {
        this.rustWarrantyMonths = rustWarrantyMonths;
    }

    public String getRustWarrantyMiles() {
        return rustWarrantyMiles;
    }

    public void setRustWarrantyMiles(String rustWarrantyMiles) {
        this.rustWarrantyMiles = rustWarrantyMiles;
    }

    public Boolean getApprovedInd() {
        return approvedInd;
    }

    public void setApprovedInd(Boolean approvedInd) {
        this.approvedInd = approvedInd;
    }

    public Boolean getOverride() {
        return override;
    }

    public void setOverride(Boolean override) {
        this.override = override;
    }

    public String getMotorCycleUsage() {
        return motorCycleUsage;
    }

    public void setMotorCycleUsage(String motorCycleUsage) {
        this.motorCycleUsage = motorCycleUsage;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    public VinOverride getVinOverride() {
        return vinOverride;
    }

    public void setVinOverride(VinOverride vinOverride) {
        this.vinOverride = vinOverride;
    }

    public CountryOfOrigin getCountryOfOriginIdFk() {
        return countryOfOriginIdFk;
    }

    public void setCountryOfOriginIdFk(CountryOfOrigin countryOfOriginIdFk) {
        this.countryOfOriginIdFk = countryOfOriginIdFk;
    }

    public FuelDelivery getFuelDeliveryIdFk() {
        return fuelDeliveryIdFk;
    }

    public void setFuelDeliveryIdFk(FuelDelivery fuelDeliveryIdFk) {
        this.fuelDeliveryIdFk = fuelDeliveryIdFk;
    }

    public FuelType getFuelTypeIdFk() {
        return fuelTypeIdFk;
    }

    public void setFuelTypeIdFk(FuelType fuelTypeIdFk) {
        this.fuelTypeIdFk = fuelTypeIdFk;
    }

    public Make getMakeIdFk() {
        return makeIdFk;
    }

    public void setMakeIdFk(Make makeIdFk) {
        this.makeIdFk = makeIdFk;
    }

    public Model getModelIdFk() {
        return modelIdFk;
    }

    public void setModelIdFk(Model modelIdFk) {
        this.modelIdFk = modelIdFk;
    }

    public Series getSeriesIdFk() {
        return seriesIdFk;
    }

    public void setSeriesIdFk(Series seriesIdFk) {
        this.seriesIdFk = seriesIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vinDescId != null ? vinDescId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VinDesc)) {
            return false;
        }
        VinDesc other = (VinDesc) object;
        if ((this.vinDescId == null && other.vinDescId != null) || (this.vinDescId != null && !this.vinDescId.equals(other.vinDescId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VinDesc[ vinDescId=" + vinDescId + " ]";
    }
    
}
