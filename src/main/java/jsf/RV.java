/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;

/**
 *
 * @author Jiepi RateVisibility - connecting dealer to products
 */
public class RV {

    public RV() {
    }

    public RV(Integer rateVisibilityId, String description, String allowedProducts, Boolean deletedInd, Date effectiveDate, Date expiredDate, String dealer, Integer dealerId, String name) {
        this.rateVisibilityId = rateVisibilityId;
        this.description = description;
        this.allowedProducts = allowedProducts;
        this.deletedInd = deletedInd;
        this.effectiveDate = effectiveDate;
        this.expiredDate = expiredDate;
        this.dealer = dealer;
        this.dealerId = dealerId;
        this.name = name;
    }

    public RV(Integer rateVisibilityId, String description, String allowedProducts, Boolean deletedInd, Date effectiveDate, Date expiredDate, String dealer, Integer dealerId) {
        this.rateVisibilityId = rateVisibilityId;
        this.description = description;
        this.allowedProducts = allowedProducts;
        this.deletedInd = deletedInd;
        this.effectiveDate = effectiveDate;
        this.expiredDate = expiredDate;
        this.dealer = dealer;
        this.dealerId = dealerId;
    }

   

    

   
    

    private Integer rateVisibilityId;
    private String description;
    private String allowedProducts;
    private Boolean deletedInd;
    private Date effectiveDate;
    private Date expiredDate;
    private String dealer;
    private Integer dealerId;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }
    
    

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    

    public Integer getRateVisibilityId() {
        return rateVisibilityId;
    }

    public void setRateVisibilityId(Integer rateVisibilityId) {
        this.rateVisibilityId = rateVisibilityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAllowedProducts() {
        return allowedProducts;
    }

    public void setAllowedProducts(String allowedProducts) {
        this.allowedProducts = allowedProducts;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    

    public void reset() {
        rateVisibilityId = null;
        description = "";
        allowedProducts = "ANC, GAP, PPM, VSC";
        deletedInd = true;
        effectiveDate = null;
        expiredDate = null;
        dealer = null;
        dealerId = null;
        name = "";
    }

}
