/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtSignatoriesType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtSignatoriesType.findAll", query = "SELECT d FROM DtSignatoriesType d")
    , @NamedQuery(name = "DtSignatoriesType.findBySignatoriesTypeId", query = "SELECT d FROM DtSignatoriesType d WHERE d.signatoriesTypeId = :signatoriesTypeId")
    , @NamedQuery(name = "DtSignatoriesType.findByDescription", query = "SELECT d FROM DtSignatoriesType d WHERE d.description = :description")})
public class DtSignatoriesType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SignatoriesTypeId")
    private Integer signatoriesTypeId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;

    public DtSignatoriesType() {
    }

    public DtSignatoriesType(Integer signatoriesTypeId) {
        this.signatoriesTypeId = signatoriesTypeId;
    }

    public Integer getSignatoriesTypeId() {
        return signatoriesTypeId;
    }

    public void setSignatoriesTypeId(Integer signatoriesTypeId) {
        this.signatoriesTypeId = signatoriesTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (signatoriesTypeId != null ? signatoriesTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtSignatoriesType)) {
            return false;
        }
        DtSignatoriesType other = (DtSignatoriesType) object;
        if ((this.signatoriesTypeId == null && other.signatoriesTypeId != null) || (this.signatoriesTypeId != null && !this.signatoriesTypeId.equals(other.signatoriesTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtSignatoriesType[ signatoriesTypeId=" + signatoriesTypeId + " ]";
    }
    
}
