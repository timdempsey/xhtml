/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Dealer;
import entity.DealerGroup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.util.ComponentUtils;

/**
 *
 * @author Jiepi
 */
@Named(value = "dealerService")
@ApplicationScoped
public class DealerService {

    private static final Logger LOGGER = LogManager.getLogger(DealerService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    @Inject
    private RepairFacilityService rfs;

    @Inject
    private NoteService noteService;

    @Inject
    private HistoryLogService hlService;

    @Inject
    private DisbursementService disbursementService;
    
    @Inject
    private RVService rvService;

    /**
     * Creates a new instance of DealerService
     */
    public DealerService() {
        LOGGER.info("dealerService constructor");
        selectedDealer = new Dealers();
        dealerId = null;
        dealers = new Dealers();
        directAgents = new TreeMap<>();
        dealer = new Dealer();
        filtersList = new ArrayList<>();
        dealersList = new ArrayList<>();
    }

    List<Dealers> dealersList;
    
    List<Dealers> filtersList;

    Integer dealerId;
    Dealers selectedDealer;
    Dealers dealers;

    private TreeMap<String, Integer> directAgents;

    private DealerGroup dg;

    private String paddr;
    private String baddr;

    private String dlStatus;

    private Integer activeIndex;

    private Dealer dealer;

    private List<String> dealerTypes;

    private List<String> dealerGroups;

    private List<String> remitRules;

    public List<Dealers> getFiltersList() {
        return filtersList;
    }

    public void setFiltersList(List<Dealers> filtersList) {
        this.filtersList = filtersList;
    }
    
    

    public List<String> getRemitRules() {
        return remitRules;
    }

    public void setRemitRules(List<String> remitRules) {
        this.remitRules = remitRules;
    }

    public List<String> getDealerGroups() {
        return dealerGroups;
    }

    public void setDealerGroups(List<String> dealerGroups) {
        this.dealerGroups = dealerGroups;
    }

    public List<String> getDealerTypes() {
        return dealerTypes;
    }

    public void setDealerTypes(List<String> dealerTypes) {
        this.dealerTypes = dealerTypes;
    }

    public Dealers getDealers() {
        return dealers;
    }

    public void setDealers(Dealers dealers) {
        this.dealers = dealers;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public Integer getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(Integer activeIndex) {
        this.activeIndex = activeIndex;
    }

    public String getDlStatus() {
        return dlStatus;
    }

    public void setDlStatus(String dlStatus) {
        this.dlStatus = dlStatus;
    }

    public String getPaddr() {
        return paddr;
    }

    public void setPaddr(String paddr) {
        this.paddr = paddr;
    }

    public String getBaddr() {
        return baddr;
    }

    public void setBaddr(String baddr) {
        this.baddr = baddr;
    }

    public DealerGroup getDg() {
        return dg;
    }

    public void setDg(DealerGroup dg) {
        this.dg = dg;
    }

    public TreeMap<String, Integer> getDirectAgents() {
        return directAgents;
    }

    public void setDirectAgents(TreeMap<String, Integer> directAgents) {
        this.directAgents = directAgents;
    }

    public List<Dealers> getDealersList() {
        return dealersList;
    }

    public void setDealersList(List<Dealers> dealersList) {
        this.dealersList = dealersList;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Dealers getSelectedDealer() {
        return selectedDealer;
    }

    public void setSelectedDealer(Dealers selectedDealer) {
        this.selectedDealer = selectedDealer;
    }

    @PostConstruct
    public void init() {
        loadDirectAgent();

        LOGGER.info("the end of init from dealerService");
    }

    public void loadDirectAgent() {
        LOGGER.info("in loadDirectAgent");
        List<AccountKeeper> akList = claimMisc.getAccountKeeperByType(3);
        LOGGER.info("akList=" + akList.size());
        for (AccountKeeper ak : akList) {
            directAgents.put(ak.getAccountKeeperName(), ak.getAccountKeeperId());
        }
    }

    public void reset() {
        dealerId = null;
        selectedDealer = null;
        dealers = new Dealers();
        dg = null;
        paddr = "";
        baddr = "";
        dealer = null;

    }

    public void loadDealers(DealerGroup dealerGroup) {
        LOGGER.info("in loadDealers");
        if (dealerGroup == null) {
            dealersList = claimMisc.getDealers();
        } else {
            dealersList = claimMisc.getDealersByGroupFkId(dealerGroup.getDealerGroupId());
        }
    }

    public String getDealerSearch() {
        LOGGER.info(" in getDealerSearch");
        reset();
        loadDealers(null);

        return "Dealers";

    }

    public void onRowSelect(SelectEvent event) {
        selectedDealer = (Dealers) event.getObject();
        dealerId = selectedDealer.getDealerId();
        // new
        LOGGER.info("selectedDealer=" + selectedDealer.getDealerName());
        LOGGER.info("dealerId=" + selectedDealer.getDealerId());
        paddr = claimMisc.printAddress(selectedDealer.getDealerId(), "P");
        baddr = claimMisc.printAddress(selectedDealer.getDealerId(), "B");
        dealer = claimMisc.getDealerById(selectedDealer.getDealerId());

        rfs.generateRFListByDealer(dealer);
        rfs.setDealer(dealer);
        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedDealer.getDealerId());
        if (ak.getActive()) {
            dlStatus = "DeActivate";
        } else {
            dlStatus = "Activate";
        }

        /*
        FacesMessage msg = new FacesMessage("Program Selected", ((ProgramSearch) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
         */
    }

    public void addDealer(ActionEvent event) {
        dealers.reset();
        selectedDealer = null;

        dealerTypes = claimMisc.loadDealerTypes();
        dealerGroups = claimMisc.loadDealerGroups();
        remitRules = claimMisc.loadRemitRules();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDealer", options, null);

    }

    /*
    public void addDealer() {
        dealers.reset();
        selectedDealer = null;
        
        dealerTypes = claimMisc.loadDealerTypes();
        dealerGroups = claimMisc.loadDealerGroups();
        remitRules = claimMisc.loadRemitRules();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDealer", options, null);

    }
     */
    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        //if (dg == null) {
        dealersList = claimMisc.getDealers();
        LOGGER.info("dealersLIst=" + dealersList.size());
        //} else {
        //   dealersList = claimMisc.getDealersByGroupFkId(dg.getDealerGroupId());
        //}
    }

    public void editDealer() {
        LOGGER.info("dealerId=" + selectedDealer.getDealerId());

        dealers = selectedDealer;

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "1000");
        options.put("width", "1800");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("AddDealer", options, null);

    }

    public String deleteDealer() {
        dealerId = selectedDealer.getDealerId();
        LOGGER.info("in deleteDealer, dealerId=" + dealerId);
        claimMisc.deleteAccountKeeper(dealerId);
        if (dg == null) {
            dealersList = claimMisc.getDealers();
        } else {
            dealersList = claimMisc.getDealersByGroupFkId(dg.getDealerGroupId());
        }
        return "Dealers";
    }

    public void saveDealer() {
        LOGGER.info("in saveDealer");

        if (selectedDealer != null) {  // update
            LOGGER.info("updating dealer=" + selectedDealer.getDealerId());
            claimMisc.saveDealer(dealers, selectedDealer.getDealerId());
        } else {
            LOGGER.info("save new dealer");
            claimMisc.saveDealer(dealers, null);
        }

        RequestContext.getCurrentInstance().closeDialog("AddDealer");

    }

    public void cancel() {
        LOGGER.info("in cancel");
        reset();
        RequestContext.getCurrentInstance().closeDialog("AddDealer");
    }

    public String getDealerGeneral() {
        LOGGER.info("in getDealerGeneral");
        //rfs.setDealer(dealer);
        // LOGGER.info("dealerId=" + rfs.getDealer().getDealerId());

        activeIndex = 0;

        return "DealerGeneral";
    }

    public void changeDLstatus() {
        LOGGER.info("in changeDLstatus");
        AccountKeeper ak = claimMisc.getAccountKeeperById(selectedDealer.getDealerId());
        if (ak.getActive()) {
            dlStatus = "Activate";
            claimMisc.changeAccountKeeperActiveStatus(ak.getAccountKeeperId(), false);
        } else {
            dlStatus = "DeActivate";
            claimMisc.changeAccountKeeperActiveStatus(ak.getAccountKeeperId(), true);
        }
    }

    public void tabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);
        switch (tabId) {
            case "dlNotesTab":
                createNotesTab();
                activeIndex = 0;
                break;
            case "dlHistoryLogsTab":
                createHistoryLogsTab();
                activeIndex = 1;
                break;
            case "dlRatingTab":
                createRatingTab();
                activeIndex = 2;
                break;
            case "dlrfTab":
                createRFTab();
                activeIndex = 3;
                break;
            default:
                createNotesTab();
                break;
        }
    }

    public void createRFTab() {
        if (selectedDealer != null) {

            activeIndex = 0;

            dealer = claimMisc.getDealerById(selectedDealer.getDealerId());

            rfs.generateRFListByDealer(dealer);

        }
    }

    public void createNotesTab() {
        if (selectedDealer != null) {

            activeIndex = 1;

            dealer = claimMisc.getDealerById(selectedDealer.getDealerId());

            noteService.generateNotesListByDealer(dealer);

        }
    }

    public void createHistoryLogsTab() {
        if (selectedDealer != null) {

            activeIndex = 2;

            dealer = claimMisc.getDealerById(selectedDealer.getDealerId());

            hlService.generateHistoryLogsListByDealer(dealer);

        }
    }

    public void createRatingTab() {
        if (selectedDealer != null) {

            activeIndex = 0;

            disbursementService.createDisbursementTab(selectedDealer.getDealerId());

            // later
        }
    }

    public void verifyDealerName() {
        LOGGER.info("in verifyDealerName, dealerName=" + dealers.getDealerName());
        if (claimMisc.getAccountKeeperByNameType(dealers.getDealerName(), 1) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "the dealer name already exist: ", dealers.getDealerName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void ratingTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        LOGGER.info("tab id = " + tabId);
        LOGGER.info("dealerId=" + selectedDealer.getDealerId());
        switch (tabId) {
            case "drdTab":
                disbursementService.setFromDealerId(selectedDealer.getDealerId());
                disbursementService.createDisbursementTab(selectedDealer.getDealerId());
                activeIndex = 0;
                break;
            
            case "drrTab":
                rvService.createRVTab(selectedDealer.getDealerId()); //createRateVisibilityTab();
                activeIndex = 1;
                break;
            case "drgTab":
                //createSRPTab();
                activeIndex = 2;
                break;
             
            default:
                disbursementService.createDisbursementTab(dealerId);
                break;
        }
    }

    public void filterListener(FilterEvent event) {
        LOGGER.info("in fieltering=" + event.getSource().toString());
        FacesContext facesContext = FacesContext.getCurrentInstance();
        DataTable table = (DataTable) facesContext.getViewRoot().findComponent("dealersForm:dealersTbl");
        //table.resetValue();
        //dealersList = table.getFilteredValue();
        LOGGER.info(" list=" + filtersList.size() );
    }

}
