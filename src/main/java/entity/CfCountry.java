/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfCountry")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfCountry.findAll", query = "SELECT c FROM CfCountry c")
    , @NamedQuery(name = "CfCountry.findByCountryId", query = "SELECT c FROM CfCountry c WHERE c.countryId = :countryId")
    , @NamedQuery(name = "CfCountry.findByCountryCode", query = "SELECT c FROM CfCountry c WHERE c.countryCode = :countryCode")
    , @NamedQuery(name = "CfCountry.findByDescription", query = "SELECT c FROM CfCountry c WHERE c.description = :description")})
public class CfCountry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "countryId")
    private Integer countryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "countryCode")
    private String countryCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "countryIdFk")
    private Collection<CfRegion> cfRegionCollection;

    public CfCountry() {
    }

    public CfCountry(Integer countryId) {
        this.countryId = countryId;
    }

    public CfCountry(Integer countryId, String countryCode, String description) {
        this.countryId = countryId;
        this.countryCode = countryCode;
        this.description = description;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CfRegion> getCfRegionCollection() {
        return cfRegionCollection;
    }

    public void setCfRegionCollection(Collection<CfRegion> cfRegionCollection) {
        this.cfRegionCollection = cfRegionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countryId != null ? countryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfCountry)) {
            return false;
        }
        CfCountry other = (CfCountry) object;
        if ((this.countryId == null && other.countryId != null) || (this.countryId != null && !this.countryId.equals(other.countryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfCountry[ countryId=" + countryId + " ]";
    }
    
}
