/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfMembershipRole")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfMembershipRole.findAll", query = "SELECT c FROM CfMembershipRole c")
    , @NamedQuery(name = "CfMembershipRole.findByMembershipRoleId", query = "SELECT c FROM CfMembershipRole c WHERE c.membershipRoleId = :membershipRoleId")
    , @NamedQuery(name = "CfMembershipRole.findByName", query = "SELECT c FROM CfMembershipRole c WHERE c.name = :name")
    , @NamedQuery(name = "CfMembershipRole.findByDescription", query = "SELECT c FROM CfMembershipRole c WHERE c.description = :description")
    , @NamedQuery(name = "CfMembershipRole.findByUpdateUserName", query = "SELECT c FROM CfMembershipRole c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfMembershipRole.findByUpdateLast", query = "SELECT c FROM CfMembershipRole c WHERE c.updateLast = :updateLast")})
public class CfMembershipRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "membershipRoleId")
    private Integer membershipRoleId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 512)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membershipRoleId")
    private Collection<CfUserRoleConn> cfUserRoleConnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "membershipRoleId")
    private Collection<CfRoleGroupConn> cfRoleGroupConnCollection;

    public CfMembershipRole() {
    }

    public CfMembershipRole(Integer membershipRoleId) {
        this.membershipRoleId = membershipRoleId;
    }

    public CfMembershipRole(Integer membershipRoleId, String name, String description) {
        this.membershipRoleId = membershipRoleId;
        this.name = name;
        this.description = description;
    }

    public Integer getMembershipRoleId() {
        return membershipRoleId;
    }

    public void setMembershipRoleId(Integer membershipRoleId) {
        this.membershipRoleId = membershipRoleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfUserRoleConn> getCfUserRoleConnCollection() {
        return cfUserRoleConnCollection;
    }

    public void setCfUserRoleConnCollection(Collection<CfUserRoleConn> cfUserRoleConnCollection) {
        this.cfUserRoleConnCollection = cfUserRoleConnCollection;
    }

    @XmlTransient
    public Collection<CfRoleGroupConn> getCfRoleGroupConnCollection() {
        return cfRoleGroupConnCollection;
    }

    public void setCfRoleGroupConnCollection(Collection<CfRoleGroupConn> cfRoleGroupConnCollection) {
        this.cfRoleGroupConnCollection = cfRoleGroupConnCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (membershipRoleId != null ? membershipRoleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfMembershipRole)) {
            return false;
        }
        CfMembershipRole other = (CfMembershipRole) object;
        if ((this.membershipRoleId == null && other.membershipRoleId != null) || (this.membershipRoleId != null && !this.membershipRoleId.equals(other.membershipRoleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfMembershipRole[ membershipRoleId=" + membershipRoleId + " ]";
    }
    
}
