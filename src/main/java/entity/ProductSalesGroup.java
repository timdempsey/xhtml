/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ProductSalesGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductSalesGroup.findAll", query = "SELECT p FROM ProductSalesGroup p")
    , @NamedQuery(name = "ProductSalesGroup.findByProductSalesGroupId", query = "SELECT p FROM ProductSalesGroup p WHERE p.productSalesGroupId = :productSalesGroupId")
    , @NamedQuery(name = "ProductSalesGroup.findByName", query = "SELECT p FROM ProductSalesGroup p WHERE p.name = :name")
    , @NamedQuery(name = "ProductSalesGroup.findByDescription", query = "SELECT p FROM ProductSalesGroup p WHERE p.description = :description")
    , @NamedQuery(name = "ProductSalesGroup.findByOrder", query = "SELECT p FROM ProductSalesGroup p WHERE p.order = :order")})
public class ProductSalesGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "productSalesGroupId")
    private Integer productSalesGroupId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "order")
    private BigDecimal order;
    @OneToMany(mappedBy = "productSalesGroupIdFk")
    private Collection<ProductDetail> productDetailCollection;

    public ProductSalesGroup() {
    }

    public ProductSalesGroup(Integer productSalesGroupId) {
        this.productSalesGroupId = productSalesGroupId;
    }

    public Integer getProductSalesGroupId() {
        return productSalesGroupId;
    }

    public void setProductSalesGroupId(Integer productSalesGroupId) {
        this.productSalesGroupId = productSalesGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getOrder() {
        return order;
    }

    public void setOrder(BigDecimal order) {
        this.order = order;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection() {
        return productDetailCollection;
    }

    public void setProductDetailCollection(Collection<ProductDetail> productDetailCollection) {
        this.productDetailCollection = productDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productSalesGroupId != null ? productSalesGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductSalesGroup)) {
            return false;
        }
        ProductSalesGroup other = (ProductSalesGroup) object;
        if ((this.productSalesGroupId == null && other.productSalesGroupId != null) || (this.productSalesGroupId != null && !this.productSalesGroupId.equals(other.productSalesGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProductSalesGroup[ productSalesGroupId=" + productSalesGroupId + " ]";
    }
    
}
