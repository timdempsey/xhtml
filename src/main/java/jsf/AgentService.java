/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Jiepi
 */
@Named(value = "agentService")
@ApplicationScoped
public class AgentService {

    /**
     * Creates a new instance of AgentService
     */
    public AgentService() {
    }
    
    public String getAgentSearch() {
        return "Agents";
    }
    
}
