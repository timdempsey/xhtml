/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Term")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Term.findAll", query = "SELECT t FROM Term t")
    , @NamedQuery(name = "Term.findByTermId", query = "SELECT t FROM Term t WHERE t.termId = :termId")})
public class Term implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TermId")
    private Integer termId;
    @OneToMany(mappedBy = "termIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "termIdFk")
    private Collection<RateBasedRule> rateBasedRuleCollection;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne
    private PlanTable planIdFk;
    @OneToMany(mappedBy = "termIdFk")
    private Collection<TermDetail> termDetailCollection;
    @OneToMany(mappedBy = "termIdFk")
    private Collection<WarrantyRule> warrantyRuleCollection;
    @OneToMany(mappedBy = "termIdFk")
    private Collection<Rate> rateCollection;

    public Term() {
    }

    public Term(Integer termId) {
        this.termId = termId;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<RateBasedRule> getRateBasedRuleCollection() {
        return rateBasedRuleCollection;
    }

    public void setRateBasedRuleCollection(Collection<RateBasedRule> rateBasedRuleCollection) {
        this.rateBasedRuleCollection = rateBasedRuleCollection;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    @XmlTransient
    public Collection<TermDetail> getTermDetailCollection() {
        return termDetailCollection;
    }

    public void setTermDetailCollection(Collection<TermDetail> termDetailCollection) {
        this.termDetailCollection = termDetailCollection;
    }

    @XmlTransient
    public Collection<WarrantyRule> getWarrantyRuleCollection() {
        return warrantyRuleCollection;
    }

    public void setWarrantyRuleCollection(Collection<WarrantyRule> warrantyRuleCollection) {
        this.warrantyRuleCollection = warrantyRuleCollection;
    }

    @XmlTransient
    public Collection<Rate> getRateCollection() {
        return rateCollection;
    }

    public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (termId != null ? termId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Term)) {
            return false;
        }
        Term other = (Term) object;
        if ((this.termId == null && other.termId != null) || (this.termId != null && !this.termId.equals(other.termId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Term[ termId=" + termId + " ]";
    }
    
}
