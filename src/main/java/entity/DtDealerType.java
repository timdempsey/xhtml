/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtDealerType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtDealerType.findAll", query = "SELECT d FROM DtDealerType d")
    , @NamedQuery(name = "DtDealerType.findByDealerTypeId", query = "SELECT d FROM DtDealerType d WHERE d.dealerTypeId = :dealerTypeId")
    , @NamedQuery(name = "DtDealerType.findByName", query = "SELECT d FROM DtDealerType d WHERE d.name = :name")})
public class DtDealerType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerTypeId")
    private Integer dealerTypeId;
    @Size(max = 50)
    @Column(name = "name")
    private String name;

    public DtDealerType() {
    }

    public DtDealerType(Integer dealerTypeId) {
        this.dealerTypeId = dealerTypeId;
    }

    public Integer getDealerTypeId() {
        return dealerTypeId;
    }

    public void setDealerTypeId(Integer dealerTypeId) {
        this.dealerTypeId = dealerTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dealerTypeId != null ? dealerTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtDealerType)) {
            return false;
        }
        DtDealerType other = (DtDealerType) object;
        if ((this.dealerTypeId == null && other.dealerTypeId != null) || (this.dealerTypeId != null && !this.dealerTypeId.equals(other.dealerTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtDealerType[ dealerTypeId=" + dealerTypeId + " ]";
    }
    
}
