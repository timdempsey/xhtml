/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CancelRequest")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CancelRequest.findAll", query = "SELECT c FROM CancelRequest c")
    , @NamedQuery(name = "CancelRequest.findByCancelRequestId", query = "SELECT c FROM CancelRequest c WHERE c.cancelRequestId = :cancelRequestId")
    , @NamedQuery(name = "CancelRequest.findByCoverageCostRefundAmount", query = "SELECT c FROM CancelRequest c WHERE c.coverageCostRefundAmount = :coverageCostRefundAmount")
    , @NamedQuery(name = "CancelRequest.findByDealerProfitRefundAmount", query = "SELECT c FROM CancelRequest c WHERE c.dealerProfitRefundAmount = :dealerProfitRefundAmount")
    , @NamedQuery(name = "CancelRequest.findByAdminPortionAmount", query = "SELECT c FROM CancelRequest c WHERE c.adminPortionAmount = :adminPortionAmount")
    , @NamedQuery(name = "CancelRequest.findByDealerPortionAmount", query = "SELECT c FROM CancelRequest c WHERE c.dealerPortionAmount = :dealerPortionAmount")
    , @NamedQuery(name = "CancelRequest.findByWithheldClaimsAmount", query = "SELECT c FROM CancelRequest c WHERE c.withheldClaimsAmount = :withheldClaimsAmount")
    , @NamedQuery(name = "CancelRequest.findByAdminCollectedFee", query = "SELECT c FROM CancelRequest c WHERE c.adminCollectedFee = :adminCollectedFee")
    , @NamedQuery(name = "CancelRequest.findByDealerRetainedFee", query = "SELECT c FROM CancelRequest c WHERE c.dealerRetainedFee = :dealerRetainedFee")
    , @NamedQuery(name = "CancelRequest.findByAdminWithheldClaimAmount", query = "SELECT c FROM CancelRequest c WHERE c.adminWithheldClaimAmount = :adminWithheldClaimAmount")
    , @NamedQuery(name = "CancelRequest.findByDealerWithheldClaimAmount", query = "SELECT c FROM CancelRequest c WHERE c.dealerWithheldClaimAmount = :dealerWithheldClaimAmount")
    , @NamedQuery(name = "CancelRequest.findByCustomerChargedFee", query = "SELECT c FROM CancelRequest c WHERE c.customerChargedFee = :customerChargedFee")
    , @NamedQuery(name = "CancelRequest.findByCustomerRefundAmount", query = "SELECT c FROM CancelRequest c WHERE c.customerRefundAmount = :customerRefundAmount")
    , @NamedQuery(name = "CancelRequest.findByCancelDate", query = "SELECT c FROM CancelRequest c WHERE c.cancelDate = :cancelDate")
    , @NamedQuery(name = "CancelRequest.findByPaidClaimsAmount", query = "SELECT c FROM CancelRequest c WHERE c.paidClaimsAmount = :paidClaimsAmount")
    , @NamedQuery(name = "CancelRequest.findByOdometer", query = "SELECT c FROM CancelRequest c WHERE c.odometer = :odometer")
    , @NamedQuery(name = "CancelRequest.findByReduceReserveByClaimWithholdings", query = "SELECT c FROM CancelRequest c WHERE c.reduceReserveByClaimWithholdings = :reduceReserveByClaimWithholdings")
    , @NamedQuery(name = "CancelRequest.findByFinishMonth", query = "SELECT c FROM CancelRequest c WHERE c.finishMonth = :finishMonth")
    , @NamedQuery(name = "CancelRequest.findByCancelFee", query = "SELECT c FROM CancelRequest c WHERE c.cancelFee = :cancelFee")
    , @NamedQuery(name = "CancelRequest.findByRefundPercent", query = "SELECT c FROM CancelRequest c WHERE c.refundPercent = :refundPercent")
    , @NamedQuery(name = "CancelRequest.findByValidToDate", query = "SELECT c FROM CancelRequest c WHERE c.validToDate = :validToDate")
    , @NamedQuery(name = "CancelRequest.findByValidToMiles", query = "SELECT c FROM CancelRequest c WHERE c.validToMiles = :validToMiles")
    , @NamedQuery(name = "CancelRequest.findByDaysUsed", query = "SELECT c FROM CancelRequest c WHERE c.daysUsed = :daysUsed")
    , @NamedQuery(name = "CancelRequest.findByMilesUsed", query = "SELECT c FROM CancelRequest c WHERE c.milesUsed = :milesUsed")
    , @NamedQuery(name = "CancelRequest.findByTotalDays", query = "SELECT c FROM CancelRequest c WHERE c.totalDays = :totalDays")
    , @NamedQuery(name = "CancelRequest.findByTotalMiles", query = "SELECT c FROM CancelRequest c WHERE c.totalMiles = :totalMiles")
    , @NamedQuery(name = "CancelRequest.findByReceivedDate", query = "SELECT c FROM CancelRequest c WHERE c.receivedDate = :receivedDate")
    , @NamedQuery(name = "CancelRequest.findByMovedToDealerProfitAmount", query = "SELECT c FROM CancelRequest c WHERE c.movedToDealerProfitAmount = :movedToDealerProfitAmount")
    , @NamedQuery(name = "CancelRequest.findByEnteredDate", query = "SELECT c FROM CancelRequest c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CancelRequest.findByUpdateUserName", query = "SELECT c FROM CancelRequest c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CancelRequest.findByUpdateLast", query = "SELECT c FROM CancelRequest c WHERE c.updateLast = :updateLast")})
public class CancelRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelRequestId")
    private Integer cancelRequestId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "coverageCostRefundAmount")
    private BigDecimal coverageCostRefundAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerProfitRefundAmount")
    private BigDecimal dealerProfitRefundAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminPortionAmount")
    private BigDecimal adminPortionAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerPortionAmount")
    private BigDecimal dealerPortionAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withheldClaimsAmount")
    private BigDecimal withheldClaimsAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminCollectedFee")
    private BigDecimal adminCollectedFee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerRetainedFee")
    private BigDecimal dealerRetainedFee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adminWithheldClaimAmount")
    private BigDecimal adminWithheldClaimAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dealerWithheldClaimAmount")
    private BigDecimal dealerWithheldClaimAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "customerChargedFee")
    private BigDecimal customerChargedFee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "customerRefundAmount")
    private BigDecimal customerRefundAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelDate")
    @Temporal(TemporalType.DATE)
    private Date cancelDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paidClaimsAmount")
    private BigDecimal paidClaimsAmount;
    @Column(name = "odometer")
    private Integer odometer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reduceReserveByClaimWithholdings")
    private boolean reduceReserveByClaimWithholdings;
    @Basic(optional = false)
    @NotNull
    @Column(name = "finishMonth")
    private boolean finishMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelFee")
    private BigDecimal cancelFee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refundPercent")
    private BigDecimal refundPercent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "validToDate")
    @Temporal(TemporalType.DATE)
    private Date validToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "validToMiles")
    private int validToMiles;
    @Column(name = "daysUsed")
    private Integer daysUsed;
    @Column(name = "milesUsed")
    private Integer milesUsed;
    @Column(name = "totalDays")
    private Integer totalDays;
    @Column(name = "totalMiles")
    private Integer totalMiles;
    @Column(name = "receivedDate")
    @Temporal(TemporalType.DATE)
    private Date receivedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "movedToDealerProfitAmount")
    private BigDecimal movedToDealerProfitAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "cancelRequestIdFk")
    private Collection<ContractCancel> contractCancelCollection;
    @JoinColumn(name = "cancelMethodGroupRuleIdFk", referencedColumnName = "cancelMethodGroupRuleId")
    @ManyToOne
    private CfCancelMethodGroupRule cancelMethodGroupRuleIdFk;
    @JoinColumn(name = "cancelReasonIdFk", referencedColumnName = "CancelReasonId")
    @ManyToOne(optional = false)
    private CfCancelReason cancelReasonIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne(optional = false)
    private Contracts contractIdFk;
    @JoinColumn(name = "cancelMethodInd", referencedColumnName = "cancelMethodTypeId")
    @ManyToOne(optional = false)
    private DtCancelMethodType cancelMethodInd;
    @JoinColumn(name = "cancelTypeInd", referencedColumnName = "cancelTypeId")
    @ManyToOne(optional = false)
    private DtCancelType cancelTypeInd;
    @JoinColumn(name = "claimWithholdMethodInd", referencedColumnName = "claimWithholdMethodId")
    @ManyToOne(optional = false)
    private DtClaimWithholdMethod claimWithholdMethodInd;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public CancelRequest() {
    }

    public CancelRequest(Integer cancelRequestId) {
        this.cancelRequestId = cancelRequestId;
    }

    public CancelRequest(Integer cancelRequestId, BigDecimal coverageCostRefundAmount, BigDecimal dealerProfitRefundAmount, BigDecimal adminPortionAmount, BigDecimal dealerPortionAmount, BigDecimal withheldClaimsAmount, BigDecimal adminCollectedFee, BigDecimal dealerRetainedFee, BigDecimal adminWithheldClaimAmount, BigDecimal dealerWithheldClaimAmount, BigDecimal customerChargedFee, BigDecimal customerRefundAmount, Date cancelDate, BigDecimal paidClaimsAmount, boolean reduceReserveByClaimWithholdings, boolean finishMonth, BigDecimal cancelFee, BigDecimal refundPercent, Date validToDate, int validToMiles, BigDecimal movedToDealerProfitAmount, Date enteredDate) {
        this.cancelRequestId = cancelRequestId;
        this.coverageCostRefundAmount = coverageCostRefundAmount;
        this.dealerProfitRefundAmount = dealerProfitRefundAmount;
        this.adminPortionAmount = adminPortionAmount;
        this.dealerPortionAmount = dealerPortionAmount;
        this.withheldClaimsAmount = withheldClaimsAmount;
        this.adminCollectedFee = adminCollectedFee;
        this.dealerRetainedFee = dealerRetainedFee;
        this.adminWithheldClaimAmount = adminWithheldClaimAmount;
        this.dealerWithheldClaimAmount = dealerWithheldClaimAmount;
        this.customerChargedFee = customerChargedFee;
        this.customerRefundAmount = customerRefundAmount;
        this.cancelDate = cancelDate;
        this.paidClaimsAmount = paidClaimsAmount;
        this.reduceReserveByClaimWithholdings = reduceReserveByClaimWithholdings;
        this.finishMonth = finishMonth;
        this.cancelFee = cancelFee;
        this.refundPercent = refundPercent;
        this.validToDate = validToDate;
        this.validToMiles = validToMiles;
        this.movedToDealerProfitAmount = movedToDealerProfitAmount;
        this.enteredDate = enteredDate;
    }

    public Integer getCancelRequestId() {
        return cancelRequestId;
    }

    public void setCancelRequestId(Integer cancelRequestId) {
        this.cancelRequestId = cancelRequestId;
    }

    public BigDecimal getCoverageCostRefundAmount() {
        return coverageCostRefundAmount;
    }

    public void setCoverageCostRefundAmount(BigDecimal coverageCostRefundAmount) {
        this.coverageCostRefundAmount = coverageCostRefundAmount;
    }

    public BigDecimal getDealerProfitRefundAmount() {
        return dealerProfitRefundAmount;
    }

    public void setDealerProfitRefundAmount(BigDecimal dealerProfitRefundAmount) {
        this.dealerProfitRefundAmount = dealerProfitRefundAmount;
    }

    public BigDecimal getAdminPortionAmount() {
        return adminPortionAmount;
    }

    public void setAdminPortionAmount(BigDecimal adminPortionAmount) {
        this.adminPortionAmount = adminPortionAmount;
    }

    public BigDecimal getDealerPortionAmount() {
        return dealerPortionAmount;
    }

    public void setDealerPortionAmount(BigDecimal dealerPortionAmount) {
        this.dealerPortionAmount = dealerPortionAmount;
    }

    public BigDecimal getWithheldClaimsAmount() {
        return withheldClaimsAmount;
    }

    public void setWithheldClaimsAmount(BigDecimal withheldClaimsAmount) {
        this.withheldClaimsAmount = withheldClaimsAmount;
    }

    public BigDecimal getAdminCollectedFee() {
        return adminCollectedFee;
    }

    public void setAdminCollectedFee(BigDecimal adminCollectedFee) {
        this.adminCollectedFee = adminCollectedFee;
    }

    public BigDecimal getDealerRetainedFee() {
        return dealerRetainedFee;
    }

    public void setDealerRetainedFee(BigDecimal dealerRetainedFee) {
        this.dealerRetainedFee = dealerRetainedFee;
    }

    public BigDecimal getAdminWithheldClaimAmount() {
        return adminWithheldClaimAmount;
    }

    public void setAdminWithheldClaimAmount(BigDecimal adminWithheldClaimAmount) {
        this.adminWithheldClaimAmount = adminWithheldClaimAmount;
    }

    public BigDecimal getDealerWithheldClaimAmount() {
        return dealerWithheldClaimAmount;
    }

    public void setDealerWithheldClaimAmount(BigDecimal dealerWithheldClaimAmount) {
        this.dealerWithheldClaimAmount = dealerWithheldClaimAmount;
    }

    public BigDecimal getCustomerChargedFee() {
        return customerChargedFee;
    }

    public void setCustomerChargedFee(BigDecimal customerChargedFee) {
        this.customerChargedFee = customerChargedFee;
    }

    public BigDecimal getCustomerRefundAmount() {
        return customerRefundAmount;
    }

    public void setCustomerRefundAmount(BigDecimal customerRefundAmount) {
        this.customerRefundAmount = customerRefundAmount;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public BigDecimal getPaidClaimsAmount() {
        return paidClaimsAmount;
    }

    public void setPaidClaimsAmount(BigDecimal paidClaimsAmount) {
        this.paidClaimsAmount = paidClaimsAmount;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

    public boolean getReduceReserveByClaimWithholdings() {
        return reduceReserveByClaimWithholdings;
    }

    public void setReduceReserveByClaimWithholdings(boolean reduceReserveByClaimWithholdings) {
        this.reduceReserveByClaimWithholdings = reduceReserveByClaimWithholdings;
    }

    public boolean getFinishMonth() {
        return finishMonth;
    }

    public void setFinishMonth(boolean finishMonth) {
        this.finishMonth = finishMonth;
    }

    public BigDecimal getCancelFee() {
        return cancelFee;
    }

    public void setCancelFee(BigDecimal cancelFee) {
        this.cancelFee = cancelFee;
    }

    public BigDecimal getRefundPercent() {
        return refundPercent;
    }

    public void setRefundPercent(BigDecimal refundPercent) {
        this.refundPercent = refundPercent;
    }

    public Date getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(Date validToDate) {
        this.validToDate = validToDate;
    }

    public int getValidToMiles() {
        return validToMiles;
    }

    public void setValidToMiles(int validToMiles) {
        this.validToMiles = validToMiles;
    }

    public Integer getDaysUsed() {
        return daysUsed;
    }

    public void setDaysUsed(Integer daysUsed) {
        this.daysUsed = daysUsed;
    }

    public Integer getMilesUsed() {
        return milesUsed;
    }

    public void setMilesUsed(Integer milesUsed) {
        this.milesUsed = milesUsed;
    }

    public Integer getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }

    public Integer getTotalMiles() {
        return totalMiles;
    }

    public void setTotalMiles(Integer totalMiles) {
        this.totalMiles = totalMiles;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public BigDecimal getMovedToDealerProfitAmount() {
        return movedToDealerProfitAmount;
    }

    public void setMovedToDealerProfitAmount(BigDecimal movedToDealerProfitAmount) {
        this.movedToDealerProfitAmount = movedToDealerProfitAmount;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<ContractCancel> getContractCancelCollection() {
        return contractCancelCollection;
    }

    public void setContractCancelCollection(Collection<ContractCancel> contractCancelCollection) {
        this.contractCancelCollection = contractCancelCollection;
    }

    public CfCancelMethodGroupRule getCancelMethodGroupRuleIdFk() {
        return cancelMethodGroupRuleIdFk;
    }

    public void setCancelMethodGroupRuleIdFk(CfCancelMethodGroupRule cancelMethodGroupRuleIdFk) {
        this.cancelMethodGroupRuleIdFk = cancelMethodGroupRuleIdFk;
    }

    public CfCancelReason getCancelReasonIdFk() {
        return cancelReasonIdFk;
    }

    public void setCancelReasonIdFk(CfCancelReason cancelReasonIdFk) {
        this.cancelReasonIdFk = cancelReasonIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public DtCancelMethodType getCancelMethodInd() {
        return cancelMethodInd;
    }

    public void setCancelMethodInd(DtCancelMethodType cancelMethodInd) {
        this.cancelMethodInd = cancelMethodInd;
    }

    public DtCancelType getCancelTypeInd() {
        return cancelTypeInd;
    }

    public void setCancelTypeInd(DtCancelType cancelTypeInd) {
        this.cancelTypeInd = cancelTypeInd;
    }

    public DtClaimWithholdMethod getClaimWithholdMethodInd() {
        return claimWithholdMethodInd;
    }

    public void setClaimWithholdMethodInd(DtClaimWithholdMethod claimWithholdMethodInd) {
        this.claimWithholdMethodInd = claimWithholdMethodInd;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelRequestId != null ? cancelRequestId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CancelRequest)) {
            return false;
        }
        CancelRequest other = (CancelRequest) object;
        if ((this.cancelRequestId == null && other.cancelRequestId != null) || (this.cancelRequestId != null && !this.cancelRequestId.equals(other.cancelRequestId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CancelRequest[ cancelRequestId=" + cancelRequestId + " ]";
    }
    
}
