/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractCoverageGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractCoverageGroup.findAll", query = "SELECT c FROM ContractCoverageGroup c")
    , @NamedQuery(name = "ContractCoverageGroup.findByContractCoverageGroupId", query = "SELECT c FROM ContractCoverageGroup c WHERE c.contractCoverageGroupId = :contractCoverageGroupId")
    , @NamedQuery(name = "ContractCoverageGroup.findByUpdateUserName", query = "SELECT c FROM ContractCoverageGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractCoverageGroup.findByUpdateLast", query = "SELECT c FROM ContractCoverageGroup c WHERE c.updateLast = :updateLast")})
public class ContractCoverageGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractCoverageGroupId")
    private Integer contractCoverageGroupId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "relatedAdjustmentIdFk", referencedColumnName = "adjustmentId")
    @ManyToOne
    private Adjustment relatedAdjustmentIdFk;
    @JoinColumn(name = "coverageGroupIdFk", referencedColumnName = "coverageGroupId")
    @ManyToOne
    private CfCoverageGroup coverageGroupIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "relatedSurchargeIdFk", referencedColumnName = "surchargeId")
    @ManyToOne
    private Surcharge relatedSurchargeIdFk;

    public ContractCoverageGroup() {
    }

    public ContractCoverageGroup(Integer contractCoverageGroupId) {
        this.contractCoverageGroupId = contractCoverageGroupId;
    }

    public Integer getContractCoverageGroupId() {
        return contractCoverageGroupId;
    }

    public void setContractCoverageGroupId(Integer contractCoverageGroupId) {
        this.contractCoverageGroupId = contractCoverageGroupId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Adjustment getRelatedAdjustmentIdFk() {
        return relatedAdjustmentIdFk;
    }

    public void setRelatedAdjustmentIdFk(Adjustment relatedAdjustmentIdFk) {
        this.relatedAdjustmentIdFk = relatedAdjustmentIdFk;
    }

    public CfCoverageGroup getCoverageGroupIdFk() {
        return coverageGroupIdFk;
    }

    public void setCoverageGroupIdFk(CfCoverageGroup coverageGroupIdFk) {
        this.coverageGroupIdFk = coverageGroupIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Surcharge getRelatedSurchargeIdFk() {
        return relatedSurchargeIdFk;
    }

    public void setRelatedSurchargeIdFk(Surcharge relatedSurchargeIdFk) {
        this.relatedSurchargeIdFk = relatedSurchargeIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractCoverageGroupId != null ? contractCoverageGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractCoverageGroup)) {
            return false;
        }
        ContractCoverageGroup other = (ContractCoverageGroup) object;
        if ((this.contractCoverageGroupId == null && other.contractCoverageGroupId != null) || (this.contractCoverageGroupId != null && !this.contractCoverageGroupId.equals(other.contractCoverageGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractCoverageGroup[ contractCoverageGroupId=" + contractCoverageGroupId + " ]";
    }
    
}
