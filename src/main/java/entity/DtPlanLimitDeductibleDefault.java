/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtPlanLimitDeductibleDefault")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtPlanLimitDeductibleDefault.findAll", query = "SELECT d FROM DtPlanLimitDeductibleDefault d")
    , @NamedQuery(name = "DtPlanLimitDeductibleDefault.findByLimitDeductibleDefaultId", query = "SELECT d FROM DtPlanLimitDeductibleDefault d WHERE d.limitDeductibleDefaultId = :limitDeductibleDefaultId")
    , @NamedQuery(name = "DtPlanLimitDeductibleDefault.findByDescription", query = "SELECT d FROM DtPlanLimitDeductibleDefault d WHERE d.description = :description")})
public class DtPlanLimitDeductibleDefault implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "limitDeductibleDefaultId")
    private Integer limitDeductibleDefaultId;
    @Size(max = 30)
    @Column(name = "description")
    private String description;

    public DtPlanLimitDeductibleDefault() {
    }

    public DtPlanLimitDeductibleDefault(Integer limitDeductibleDefaultId) {
        this.limitDeductibleDefaultId = limitDeductibleDefaultId;
    }

    public Integer getLimitDeductibleDefaultId() {
        return limitDeductibleDefaultId;
    }

    public void setLimitDeductibleDefaultId(Integer limitDeductibleDefaultId) {
        this.limitDeductibleDefaultId = limitDeductibleDefaultId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (limitDeductibleDefaultId != null ? limitDeductibleDefaultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtPlanLimitDeductibleDefault)) {
            return false;
        }
        DtPlanLimitDeductibleDefault other = (DtPlanLimitDeductibleDefault) object;
        if ((this.limitDeductibleDefaultId == null && other.limitDeductibleDefaultId != null) || (this.limitDeductibleDefaultId != null && !this.limitDeductibleDefaultId.equals(other.limitDeductibleDefaultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtPlanLimitDeductibleDefault[ limitDeductibleDefaultId=" + limitDeductibleDefaultId + " ]";
    }
    
}
