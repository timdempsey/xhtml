/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "LimitDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LimitDetail.findAll", query = "SELECT l FROM LimitDetail l")
    , @NamedQuery(name = "LimitDetail.findByLimitDetailId", query = "SELECT l FROM LimitDetail l WHERE l.limitDetailId = :limitDetailId")
    , @NamedQuery(name = "LimitDetail.findByEffectiveDate", query = "SELECT l FROM LimitDetail l WHERE l.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "LimitDetail.findByEnteredDate", query = "SELECT l FROM LimitDetail l WHERE l.enteredDate = :enteredDate")
    , @NamedQuery(name = "LimitDetail.findByDeletedInd", query = "SELECT l FROM LimitDetail l WHERE l.deletedInd = :deletedInd")
    , @NamedQuery(name = "LimitDetail.findByDescription", query = "SELECT l FROM LimitDetail l WHERE l.description = :description")
    , @NamedQuery(name = "LimitDetail.findByGapLowerLimitAmount", query = "SELECT l FROM LimitDetail l WHERE l.gapLowerLimitAmount = :gapLowerLimitAmount")
    , @NamedQuery(name = "LimitDetail.findByGapUpperLimitAmount", query = "SELECT l FROM LimitDetail l WHERE l.gapUpperLimitAmount = :gapUpperLimitAmount")
    , @NamedQuery(name = "LimitDetail.findByLowerLimitTime", query = "SELECT l FROM LimitDetail l WHERE l.lowerLimitTime = :lowerLimitTime")
    , @NamedQuery(name = "LimitDetail.findByLowerLimitUsage", query = "SELECT l FROM LimitDetail l WHERE l.lowerLimitUsage = :lowerLimitUsage")
    , @NamedQuery(name = "LimitDetail.findByUpperLimitTime", query = "SELECT l FROM LimitDetail l WHERE l.upperLimitTime = :upperLimitTime")
    , @NamedQuery(name = "LimitDetail.findByUpperLimitUsage", query = "SELECT l FROM LimitDetail l WHERE l.upperLimitUsage = :upperLimitUsage")
    , @NamedQuery(name = "LimitDetail.findByUpdateUserName", query = "SELECT l FROM LimitDetail l WHERE l.updateUserName = :updateUserName")
    , @NamedQuery(name = "LimitDetail.findByUpdateLast", query = "SELECT l FROM LimitDetail l WHERE l.updateLast = :updateLast")})
public class LimitDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "limitDetailId")
    private Integer limitDetailId;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "gapLowerLimitAmount")
    private BigDecimal gapLowerLimitAmount;
    @Column(name = "gapUpperLimitAmount")
    private BigDecimal gapUpperLimitAmount;
    @Column(name = "lowerLimitTime")
    private Integer lowerLimitTime;
    @Column(name = "lowerLimitUsage")
    private Integer lowerLimitUsage;
    @Column(name = "upperLimitTime")
    private Integer upperLimitTime;
    @Column(name = "upperLimitUsage")
    private Integer upperLimitUsage;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "limitIdFk", referencedColumnName = "limitId")
    @ManyToOne
    private Limit limitIdFk;
    @JoinColumn(name = "ProductTypeIdFk", referencedColumnName = "productTypeId")
    @ManyToOne
    private ProductType productTypeIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;

    public LimitDetail() {
    }

    public LimitDetail(Integer limitDetailId) {
        this.limitDetailId = limitDetailId;
    }

    public Integer getLimitDetailId() {
        return limitDetailId;
    }

    public void setLimitDetailId(Integer limitDetailId) {
        this.limitDetailId = limitDetailId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getGapLowerLimitAmount() {
        return gapLowerLimitAmount;
    }

    public void setGapLowerLimitAmount(BigDecimal gapLowerLimitAmount) {
        this.gapLowerLimitAmount = gapLowerLimitAmount;
    }

    public BigDecimal getGapUpperLimitAmount() {
        return gapUpperLimitAmount;
    }

    public void setGapUpperLimitAmount(BigDecimal gapUpperLimitAmount) {
        this.gapUpperLimitAmount = gapUpperLimitAmount;
    }

    public Integer getLowerLimitTime() {
        return lowerLimitTime;
    }

    public void setLowerLimitTime(Integer lowerLimitTime) {
        this.lowerLimitTime = lowerLimitTime;
    }

    public Integer getLowerLimitUsage() {
        return lowerLimitUsage;
    }

    public void setLowerLimitUsage(Integer lowerLimitUsage) {
        this.lowerLimitUsage = lowerLimitUsage;
    }

    public Integer getUpperLimitTime() {
        return upperLimitTime;
    }

    public void setUpperLimitTime(Integer upperLimitTime) {
        this.upperLimitTime = upperLimitTime;
    }

    public Integer getUpperLimitUsage() {
        return upperLimitUsage;
    }

    public void setUpperLimitUsage(Integer upperLimitUsage) {
        this.upperLimitUsage = upperLimitUsage;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Limit getLimitIdFk() {
        return limitIdFk;
    }

    public void setLimitIdFk(Limit limitIdFk) {
        this.limitIdFk = limitIdFk;
    }

    public ProductType getProductTypeIdFk() {
        return productTypeIdFk;
    }

    public void setProductTypeIdFk(ProductType productTypeIdFk) {
        this.productTypeIdFk = productTypeIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (limitDetailId != null ? limitDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LimitDetail)) {
            return false;
        }
        LimitDetail other = (LimitDetail) object;
        if ((this.limitDetailId == null && other.limitDetailId != null) || (this.limitDetailId != null && !this.limitDetailId.equals(other.limitDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LimitDetail[ limitDetailId=" + limitDetailId + " ]";
    }
    
}
