/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.Claim;
import entity.Contracts;
import entity.DtLedgerType;
import entity.DtcashTransactionType;
import entity.Ledger;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jiepi
 */
@Named(value = "acctRecItemService")
@ApplicationScoped
public class AcctRecItemService {

    private static final Logger LOGGER = LogManager.getLogger(AcctRecItemService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of AcctRecItemService
     */
    public AcctRecItemService() {
        
    }

    private AcctRecUpper acctRecUpper;

    private AcctRecItems acctRecItem;
    //private List<AcctRecItems> acctRecItemsList;
    private LazyDataModel<AcctRecItems> acctRecItemsList;
    private AcctRecItems selectedAcctRecItem;

    private String acctHolderName;
    private Integer acctHolderId;

    private List<String> transTypeList;
    private List<String> ledgerTypeList;

    private List<AcctRecItems> selectedAcctRecItems;

    private BigDecimal newContracts;
    private BigDecimal amendments;
    private BigDecimal cancellations;
    private BigDecimal reinstatements;
    private BigDecimal transfers;
    private BigDecimal newCommissions;
    private BigDecimal amendmentCommissions;
    private BigDecimal cancelledCommissions;
    private BigDecimal reinstatedCommissions;
    private BigDecimal claimPayments;
    private BigDecimal claimInspections;
    private BigDecimal convertedRewardPoints;
    private BigDecimal batchCorrections;
    private BigDecimal prevBal;
    private BigDecimal currCharges;
    private BigDecimal currBalance;
    
    Boolean isReceivable;
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    

    public Boolean getIsReceivable() {
        return isReceivable;
    }

    public void setIsReceivable(Boolean isReceivable) {
        this.isReceivable = isReceivable;
    }
    
    

    public List<AcctRecItems> getSelectedAcctRecItems() {
        return selectedAcctRecItems;
    }

    public void setSelectedAcctRecItems(List<AcctRecItems> selectedAcctRecItems) {
        this.selectedAcctRecItems = selectedAcctRecItems;
    }

    public List<String> getLedgerTypeList() {
        return ledgerTypeList;
    }

    public void setLedgerTypeList(List<String> ledgerTypeList) {
        this.ledgerTypeList = ledgerTypeList;
    }

    public List<String> getTransTypeList() {
        return transTypeList;
    }

    public void setTransTypeList(List<String> transTypeList) {
        this.transTypeList = transTypeList;
    }

    public String getAcctHolderName() {
        return acctHolderName;
    }

    public void setAcctHolderName(String acctHolderName) {
        this.acctHolderName = acctHolderName;
    }

    public Integer getAcctHolderId() {
        return acctHolderId;
    }

    public void setAcctHolderId(Integer acctHolderId) {
        this.acctHolderId = acctHolderId;
    }

    public AcctRecUpper getAcctRecUpper() {
        return acctRecUpper;
    }

    public void setAcctRecUpper(AcctRecUpper acctRecUpper) {
        this.acctRecUpper = acctRecUpper;
    }

    public AcctRecItems getAcctRecItem() {
        return acctRecItem;
    }

    public void setAcctRecItem(AcctRecItems acctRecItem) {
        this.acctRecItem = acctRecItem;
    }

   
    public LazyDataModel<AcctRecItems> getAcctRecItemsList() {
        return acctRecItemsList;
    }

    public void setAcctRecItemsList(LazyDataModel<AcctRecItems> acctRecItemsList) {
        this.acctRecItemsList = acctRecItemsList;
    }

    public AcctRecItems getSelectedAcctRecItem() {
        return selectedAcctRecItem;
    }

    public void setSelectedAcctRecItem(AcctRecItems selectedAcctRecItem) {
        this.selectedAcctRecItem = selectedAcctRecItem;
    }

    @PostConstruct
    public void init() {
        
        LOGGER.info("acctRecItemService init");

        selectedAcctRecItem = new AcctRecItems();
        //acctRecUpper = new AcctRecUpper();
        //acctRecUpper.reset();
        acctRecItem = new AcctRecItems();
        initRecUpper();

        List<DtcashTransactionType> dtList = claimMisc.getDtcashTransactionTypeByAll();
        transTypeList = new ArrayList<>();
        for (DtcashTransactionType dt : dtList) {
            transTypeList.add(dt.getDescription());
        }

        ledgerTypeList = new ArrayList<>();
        List<DtLedgerType> ledgerList = claimMisc.getDtLedgerTypeByAll();
        for (DtLedgerType dl : ledgerList) {
            ledgerTypeList.add(dl.getDescription());
        }

        //loadOptItems(acctHolderId);
    }

    public void initRecUpper() {
         acctRecUpper = new AcctRecUpper();
        acctRecUpper.reset();
        newContracts = claimMisc.BIGZERO;
        amendments = claimMisc.BIGZERO;
        cancellations = claimMisc.BIGZERO;
        reinstatements = claimMisc.BIGZERO;
        transfers = claimMisc.BIGZERO;
        newCommissions = claimMisc.BIGZERO;
        amendmentCommissions = claimMisc.BIGZERO;
        cancelledCommissions = claimMisc.BIGZERO;
        reinstatedCommissions = claimMisc.BIGZERO;
        claimPayments = claimMisc.BIGZERO;
        claimInspections = claimMisc.BIGZERO;
        convertedRewardPoints = claimMisc.BIGZERO;
        batchCorrections = claimMisc.BIGZERO;
        prevBal = claimMisc.BIGZERO;    // later
        currCharges = claimMisc.BIGZERO;
        currBalance = prevBal.add(claimMisc.BIGZERO);
        acctRecUpper.setAccountName(acctHolderName);
    }

    public void getOptItems() {
        LOGGER.info("in getOptItems, id=" + acctHolderId);
        loadOptItems(acctHolderId);
    }

    public void loadOptItems(Integer acctId) {
        LOGGER.info("in loadOptItems, acctId=" + acctId);
        acctHolderId = acctId;

        // acctRecItemsList = claimMisc.createAcctRecItemsList(acctId);
        // LOGGER.info("size of acctRecItemsList=" + acctRecItemsList.size());
        acctRecItemsList = new LazyDataModel<AcctRecItems>() {
            @Override
            public AcctRecItems getRowData(String rowKey) {
                int intRowKey = Integer.parseInt(rowKey);
                for (AcctRecItems at : acctRecItemsList) {
                    if (at.getLedgeId().equals(intRowKey)) {
                        return at;
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(AcctRecItems at) {
                return at.getLedgeId();
            }

            @Override
            public List<AcctRecItems> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List<Ledger> ct = claimMisc.searchLedger(acctHolderId, first, pageSize);
                return (List<AcctRecItems>) createAcctRecItemsList(acctHolderId, ct);
            }

        };

        acctRecItemsList.setRowCount(claimMisc.getSearchLedgerCount(acctId));
    }

    public List<AcctRecItems> createAcctRecItemsList(Integer acctHolderId, List<Ledger> ledgerList) {
        try {
            LOGGER.info("in createAcctRecItemsList, acctHolderId=" + acctHolderId);
            List<AcctRecItems> aritemList = new ArrayList<>();
            AccountKeeper ak = claimMisc.getAccountKeeperById(acctHolderId);
            //List<Ledger> ledgerList = getLedgerByAKFKId(acctHolderId);
            LOGGER.info("size of ledgerList=" + ledgerList.size());
            String ledgerType;
            Contracts contract;
            Claim claim;
            String claimNumber = "";
            BigDecimal adminCredit = claimMisc.BIGZERO;
            BigDecimal adminDebit = claimMisc.BIGZERO;
            BigDecimal adminCommCredit = claimMisc.BIGZERO;
            BigDecimal adminCommDebt = claimMisc.BIGZERO;
            String ledgerDate = "";
            String enteredDate = "";
            for (Ledger ledger : ledgerList) {
                ledgerType = claimMisc.getDtLedgerTypeById(ledger.getLedgerTypeInd()).getDescription();
                if( acctRecItem.getLedgeType()!=null && !acctRecItem.getLedgeType().equalsIgnoreCase("All") && !acctRecItem.getLedgeType().equalsIgnoreCase(ledgerType) ) {
                    continue;
                }
                contract = claimMisc.getContractsById(ledger.getRelatedContractIdFk().getContractId());
                if (acctRecItem.getContractNo() != null && acctRecItem.getContractNo().length() > 0 && !acctRecItem.getContractNo().equalsIgnoreCase(contract.getContractNo())) {
                    continue;
                }
                if (acctRecItem.getVin() != null && acctRecItem.getVin().length() > 0 && !acctRecItem.getVin().equalsIgnoreCase(contract.getVinFull())) {
                    continue;
                }
                if (acctRecItem.getCustLastName() != null && acctRecItem.getCustLastName().length() > 0 && !acctRecItem.getCustLastName().equalsIgnoreCase(contract.getCustomerIdFk().getLastName())) {
                    continue;
                }
                if (ledger.getRelatedClaimIdFk() != null) {
                    claim = ledger.getRelatedClaimIdFk();
                    claimNumber = claim.getClaimNumber();
                    if (acctRecItem.getClaimNo() != null && acctRecItem.getClaimNo().length() > 0 && !acctRecItem.getClaimNo().equalsIgnoreCase(claimNumber)) {
                        continue;
                    }
                }

                switch (ledger.getLedgerTypeInd()) {
                    case 1:
                    case 2:
                        adminDebit = ledger.getCurrentAmount();
                        if (acctRecItem.getAdminDebit() != null && acctRecItem.getAdminDebit() != adminDebit) {
                            continue;
                        }
                        break;
                    case 3:
                    case 14:
                        adminCredit = ledger.getCurrentAmount();
                        if (acctRecItem.getAdminCredit() != null && acctRecItem.getAdminCredit() != adminCredit) {
                            continue;
                        }
                        break;
                    default:
                        adminDebit = null;
                        adminCredit = null;
                        break;
                }
                if (ledger.getLedgerDate() != null) {
                    ledgerDate = claimMisc.getJavaDate(ledger.getLedgerDate());
                    if( acctRecItem.getPostedDate()!=null && acctRecItem.getPostedDate().length()>0 && !acctRecItem.getPostedDate().equalsIgnoreCase(ledgerDate) ) {
                        continue;
                    }
                }
                if( ledger.getEnteredDate() != null ) {
                    enteredDate = claimMisc.getJavaDate(ledger.getEnteredDate());
                    if( acctRecItem.getEditDate()!=null && acctRecItem.getEditDate().length()>0 && !acctRecItem.getEditDate().equalsIgnoreCase(enteredDate)) {
                        continue;
                    }
                }

                aritemList.add(new AcctRecItems(
                        ledger.getLedgerId(),
                        ledgerType,
                        contract.getContractNo(),
                        contract.getVinFull(),
                        contract.getCustomerIdFk().getLastName(),
                        claimNumber,
                        adminCredit,
                        adminDebit,
                        adminCommCredit,
                        adminCommDebt,
                        enteredDate,
                        ledgerDate
                ));
            }

            LOGGER.info("size of aritemList=" + aritemList.size());
            return aritemList;

        } catch (NoResultException e) {
            LOGGER.info("createAcctRecItemsList is null");
            return null;
        }
    }

    public void updateAcctUpper(AcctRecItems ari, Boolean selected) {
        BigDecimal adminCommCredit = Optional.ofNullable(ari.getAdminCommCredit()).orElse(claimMisc.BIGZERO);
        BigDecimal adminCommDebit = Optional.ofNullable(ari.getAdminCommDebit()).orElse(claimMisc.BIGZERO);
        BigDecimal adminCredit = Optional.ofNullable(ari.getAdminCredit()).orElse(claimMisc.BIGZERO);
        BigDecimal adminDebit = Optional.ofNullable(ari.getAdminDebit()).orElse(claimMisc.BIGZERO);

        switch (ari.getLedgeType()) {
            case "Contract":
                LOGGER.info("contract, debit=" + adminDebit);
                LOGGER.info("contract, credit=" + adminCredit);
                if (selected) {
                    newContracts = newContracts.add(adminDebit);
                    newContracts = newContracts.add(adminCommDebit);
                } else {
                    newContracts = newContracts.subtract(adminDebit);
                    newContracts = newContracts.subtract(adminCommDebit);
                }
                LOGGER.info("newContracts=" + newContracts);

                break;
            case "Cancel":
                if (selected) {
                    cancellations = cancellations.subtract(adminCredit);
                    cancelledCommissions = cancelledCommissions.add(adminCommDebit);
                } else {
                    cancellations = cancellations.add(adminCredit);
                    cancelledCommissions = cancelledCommissions.subtract(adminCommDebit);
                }
                currCharges = currCharges.add(cancelledCommissions).add(cancellations);
                currBalance = currBalance.add(cancelledCommissions).add(cancellations);
                break;
            case "Payment Authorization":
                if (selected) {
                    claimPayments = claimPayments.subtract(adminCredit);
                    claimPayments = claimPayments.add(adminDebit);
                } else {
                    claimPayments = claimPayments.add(adminCredit);
                    claimPayments = claimPayments.subtract(adminDebit);
                }
                currCharges = currCharges.add(claimPayments);
                currBalance = currBalance.add(claimPayments);
                break;
            case "Commission":
                if (selected) {
                    newCommissions = newCommissions.subtract(adminCredit);
                } else {
                    newCommissions = newCommissions.add(adminCredit);
                }
                currCharges = currCharges.add(newCommissions);
                currBalance = currBalance.add(newCommissions);
                break;
            case "Cancelled Commission":
                if (selected) {
                    cancelledCommissions = cancelledCommissions.add(adminCommDebit);
                    cancelledCommissions = cancelledCommissions.subtract(adminCredit);
                } else {
                    cancelledCommissions = cancelledCommissions.subtract(adminCommDebit);
                    cancelledCommissions = cancelledCommissions.add(adminCredit);
                }
                currCharges = currCharges.add(cancelledCommissions);
                currBalance = currBalance.add(cancelledCommissions);
                break;
            default:
                LOGGER.info("this options " + ari.getLedgeType() + " is not calculated.");
                break;
        }
        currCharges = newContracts.add(amendments).add(cancellations).add(reinstatements).add(transfers).add(newCommissions).add(amendmentCommissions).add(cancelledCommissions).add(reinstatedCommissions).add(claimPayments).add(claimInspections).add(convertedRewardPoints).add(batchCorrections);
        currBalance = currCharges.add(prevBal);
        acctRecUpper.setNewContracts(newContracts);
        acctRecUpper.setAmendments(amendments);
        acctRecUpper.setCancellations(cancellations);
        acctRecUpper.setReinstatments(reinstatements);
        acctRecUpper.setTransfers(transfers);
        acctRecUpper.setNewCommissions(newCommissions);
        acctRecUpper.setAmendmentCommissions(amendmentCommissions);
        acctRecUpper.setCancelledCommissions(cancelledCommissions);
        acctRecUpper.setReinstatedCommissions(reinstatedCommissions);
        acctRecUpper.setClaimPayments(claimPayments);
        acctRecUpper.setClaimInspections(claimInspections);
        acctRecUpper.setConvertedRewardPoints(convertedRewardPoints);
        acctRecUpper.setBatchCorrections(batchCorrections);
        acctRecUpper.setCurrentCharges(currCharges);
        acctRecUpper.setCurrentBalance(currBalance);
        acctRecUpper.setTransactionAmount(currCharges);
        acctRecUpper.setCurrAcctId(acctHolderId);

    }

    public void onSelectCheckbox(SelectEvent event) {
        LOGGER.info("in onSelectCheckbox,");
        selectedAcctRecItem = (AcctRecItems) event.getObject();
        LOGGER.info("debit=" + selectedAcctRecItem.getAdminDebit() + ", credit=" + selectedAcctRecItem.getAdminCredit());
        LOGGER.info("type=" + selectedAcctRecItem.getLedgeType());
        updateAcctUpper(selectedAcctRecItem, true);

    }

    public void onUnselectCheckbox(UnselectEvent event) {
        LOGGER.info("in onUnselectCheckbox");
        selectedAcctRecItem = (AcctRecItems) event.getObject();
        LOGGER.info("type=" + selectedAcctRecItem.getLedgeType());
        updateAcctUpper(selectedAcctRecItem, false);

    }

    public void onToggleSelect(ToggleSelectEvent tse) {
        LOGGER.info("in toggleSelected");
        if (tse.isSelected()) {
            for (AcctRecItems ari : selectedAcctRecItems) {
                LOGGER.info("type=" + ari.getLedgeType());
                updateAcctUpper(ari, tse.isSelected());
            }
        } else {
            acctRecUpper.reset();
        }
    }

    public void saveReceivable() {
        LOGGER.info("saveReceiivable");
        String transactionNumber = acctRecUpper.getTransactionNumber();
        String type = acctRecUpper.getTransType();
       LOGGER.info("sentDate=" + claimMisc.getJavaDate(acctRecUpper.getSentDate()));
        LOGGER.info("receivedDate=" + acctRecUpper.getReceivedDate());
        LOGGER.info("admin=" + acctRecUpper.getAdminCompany());
        
        
        
        LOGGER.info("transactionNumber=" + transactionNumber);
        LOGGER.info("type=" + type);
        LOGGER.info("transactionAmount=" + acctRecUpper.getTransactionAmount());
        
        claimMisc.createTransactionFromItems(acctRecUpper,  selectedAcctRecItems);
        
        /*
        for (AcctRecItems ari : selectedAcctRecItems) {
                LOGGER.info("type=" + ari.getLedgeType());
                LOGGER.info("contractNo=" + ari.getContractNo());
                
            }
*/
        
        RequestContext.getCurrentInstance().closeDialog("AcctRecItemsPage");

    }

    public void saveANDprint() {

    }

    public void tabChange(TabChangeEvent event) {

    }

    public void reset() {
        selectedAcctRecItem.reset();
        acctRecUpper.reset();
        acctRecItem.reset();
        acctRecItemsList = null;
        acctHolderName = "";
        acctHolderId = null;
        selectedAcctRecItems = new ArrayList<>();
    }

}
