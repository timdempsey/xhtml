/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import javax.persistence.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimDealer implements Serializable{
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);
    
    public  ClaimDealer() {
    }

    public ClaimDealer(String dealName, String dealerNumber, String phoneNumber, String physicalAddress, String billingAddress) {
        this.dealName = dealName;
        this.dealerNumber = dealerNumber;
        this.phoneNumber = phoneNumber;
        this.physicalAddress = physicalAddress;
        this.billingAddress = billingAddress;
    }
    
    
    
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    private String dealName;
    @Column(name = "dealerNumber", table = "Dealer")
    private String dealerNumber;
    @Column(name = "phoneNumber", table = "Addresss")
    private String phoneNumber;
    String physicalAddress;
    String billingAddress;

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }
    
    
    
    
}
