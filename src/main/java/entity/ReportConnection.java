/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReportConnection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportConnection.findAll", query = "SELECT r FROM ReportConnection r")
    , @NamedQuery(name = "ReportConnection.findByReportConnectionId", query = "SELECT r FROM ReportConnection r WHERE r.reportConnectionId = :reportConnectionId")})
public class ReportConnection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ReportConnectionId")
    private Integer reportConnectionId;
    @JoinColumn(name = "ReportId", referencedColumnName = "reportId")
    @ManyToOne(optional = false)
    private Report reportId;
    @JoinColumn(name = "ReportGroupId", referencedColumnName = "reportGroupId")
    @ManyToOne(optional = false)
    private ReportGroup reportGroupId;

    public ReportConnection() {
    }

    public ReportConnection(Integer reportConnectionId) {
        this.reportConnectionId = reportConnectionId;
    }

    public Integer getReportConnectionId() {
        return reportConnectionId;
    }

    public void setReportConnectionId(Integer reportConnectionId) {
        this.reportConnectionId = reportConnectionId;
    }

    public Report getReportId() {
        return reportId;
    }

    public void setReportId(Report reportId) {
        this.reportId = reportId;
    }

    public ReportGroup getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(ReportGroup reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportConnectionId != null ? reportConnectionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportConnection)) {
            return false;
        }
        ReportConnection other = (ReportConnection) object;
        if ((this.reportConnectionId == null && other.reportConnectionId != null) || (this.reportConnectionId != null && !this.reportConnectionId.equals(other.reportConnectionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReportConnection[ reportConnectionId=" + reportConnectionId + " ]";
    }
    
}
