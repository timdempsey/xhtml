/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class Dealers {

    private static final Logger LOGGER = LogManager.getLogger(Dealers.class);

    public Dealers() {
        myAddress = new MyAddress();
    }

    public Dealers(Integer dealerId, String dealerName, String dealerNumber, String directAgent, String dealerGroup, String remitRule, String rateVisibility, String phoneNumber) {
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerNumber = dealerNumber;
        this.directAgent = directAgent;
        this.dealerGroup = dealerGroup;
        this.remitRule = remitRule;
        this.rateVisibility = rateVisibility;
        this.phoneNumber = phoneNumber;
    }

   
    public Dealers(Integer dealerId, String dealerName, String dealerNumber, String directAgent, String dealerGroup, String remitRule, String rateVisibility, String phoneNumber, String taxId, String email, String prefpayMethod, String dealerType, Boolean isActive) {
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerNumber = dealerNumber;
        this.directAgent = directAgent;
        this.dealerGroup = dealerGroup;
        this.remitRule = remitRule;
        this.rateVisibility = rateVisibility;
        this.phoneNumber = phoneNumber;
        this.taxId = taxId;
        this.email = email;
        this.prefpayMethod = prefpayMethod;
        this.dealerType = dealerType;
        this.isActive = isActive;
    }
    
    

    

    public Dealers(Integer dealerId, String dealerName, String dealerNumber, String directAgent, String dealerGroup, String remitRule, String rateVisibility, String phoneNumber, String legalName, String reinsurer, String taxId, String GLcode, Date effectiveDate, String systemCode, String clientCode, String producerId, String email, Boolean emailOptIn, Boolean holdPayables, String lienHolder, String prefpayMethod, String NSD, String TWG, String clipType, String billTo, String dealerType, Boolean isActive, MyAddress myAddress) {
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerNumber = dealerNumber;
        this.directAgent = directAgent;
        this.dealerGroup = dealerGroup;
        this.remitRule = remitRule;
        this.rateVisibility = rateVisibility;
        this.phoneNumber = phoneNumber;
        this.legalName = legalName;
        this.reinsurer = reinsurer;
        this.taxId = taxId;
        this.GLcode = GLcode;
        this.effectiveDate = effectiveDate;
        this.systemCode = systemCode;
        this.clientCode = clientCode;
        this.producerId = producerId;
        this.email = email;
        this.emailOptIn = emailOptIn;
        this.holdPayables = holdPayables;
        this.lienHolder = lienHolder;
        this.prefpayMethod = prefpayMethod;
        this.NSD = NSD;
        this.TWG = TWG;
        this.clipType = clipType;
        this.billTo = billTo;
        this.dealerType = dealerType;
        this.isActive = isActive;
        this.myAddress = myAddress;
    }

    

    

    private Integer dealerId;
    private String dealerName;
    private String dealerNumber;
    private String directAgent;
    private String dealerGroup;
    private String remitRule;
    private String rateVisibility;
    private String phoneNumber;
    private String legalName;
    private String reinsurer;
    private String taxId;
    private String GLcode;
    private Date effectiveDate;
    private String systemCode;
    private String clientCode;
    private String producerId;
    private String email;
    private Boolean emailOptIn;
    private Boolean holdPayables;
    private String lienHolder;
    private String prefpayMethod;
    private String NSD;
    private String TWG;
    private String clipType;
    private String billTo;
    private String dealerType;
    private Boolean isActive;
    private MyAddress myAddress;

    public MyAddress getMyAddress() {
        return myAddress;
    }

    public void setMyAddress(MyAddress myAddress) {
        this.myAddress = myAddress;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getBillTo() {
        return billTo;
    }

    public void setBillTo(String billTo) {
        this.billTo = billTo;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getReinsurer() {
        return reinsurer;
    }

    public void setReinsurer(String reinsurer) {
        this.reinsurer = reinsurer;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getGLcode() {
        return GLcode;
    }

    public void setGLcode(String GLcode) {
        this.GLcode = GLcode;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Boolean getHoldPayables() {
        return holdPayables;
    }

    public void setHoldPayables(Boolean holdPayables) {
        this.holdPayables = holdPayables;
    }

    public String getLienHolder() {
        return lienHolder;
    }

    public void setLienHolder(String lienHolder) {
        this.lienHolder = lienHolder;
    }

    public String getPrefpayMethod() {
        return prefpayMethod;
    }

    public void setPrefpayMethod(String prefpayMethod) {
        this.prefpayMethod = prefpayMethod;
    }

    public String getNSD() {
        return NSD;
    }

    public void setNSD(String NSD) {
        this.NSD = NSD;
    }

    public String getTWG() {
        return TWG;
    }

    public void setTWG(String TWG) {
        this.TWG = TWG;
    }

    public String getClipType() {
        return clipType;
    }

    public void setClipType(String clipType) {
        this.clipType = clipType;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }

    public String getDirectAgent() {
        return directAgent;
    }

    public void setDirectAgent(String directAgent) {
        this.directAgent = directAgent;
    }

    public String getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(String dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    public String getRemitRule() {
        return remitRule;
    }

    public void setRemitRule(String remitRule) {
        this.remitRule = remitRule;
    }

    public String getRateVisibility() {
        return rateVisibility;
    }

    public void setRateVisibility(String rateVisibility) {
        this.rateVisibility = rateVisibility;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void reset() {
        dealerId = null;
        dealerName = "";
        dealerNumber = "";
        directAgent = "";
        dealerGroup = "";
        remitRule = "";
        rateVisibility = "";
        phoneNumber = "";
        legalName = "";
        reinsurer = "";
        taxId = "";
        GLcode = "";
        effectiveDate = null;
        systemCode = "";
        clientCode = "";
        producerId = "";
        email = "";
        emailOptIn = false;
        holdPayables = false;
        lienHolder = "";
        prefpayMethod = "";
        NSD = "";
        TWG = "";
        clipType = "";
        billTo = "";
        dealerType = "";
        isActive = true;
    }

}
