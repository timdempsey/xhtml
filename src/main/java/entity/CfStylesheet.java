/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfStylesheet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfStylesheet.findAll", query = "SELECT c FROM CfStylesheet c")
    , @NamedQuery(name = "CfStylesheet.findByStylesheetId", query = "SELECT c FROM CfStylesheet c WHERE c.stylesheetId = :stylesheetId")
    , @NamedQuery(name = "CfStylesheet.findByName", query = "SELECT c FROM CfStylesheet c WHERE c.name = :name")
    , @NamedQuery(name = "CfStylesheet.findByDescription", query = "SELECT c FROM CfStylesheet c WHERE c.description = :description")
    , @NamedQuery(name = "CfStylesheet.findByUpdateUserName", query = "SELECT c FROM CfStylesheet c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfStylesheet.findByUpdateLast", query = "SELECT c FROM CfStylesheet c WHERE c.updateLast = :updateLast")})
public class CfStylesheet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "stylesheetId")
    private Integer stylesheetId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "stylesheetIdFk")
    private Collection<CfPDFDetail> cfPDFDetailCollection;
    @JoinColumn(name = "fileIdFk", referencedColumnName = "fileId")
    @ManyToOne(optional = false)
    private CfFile fileIdFk;

    public CfStylesheet() {
    }

    public CfStylesheet(Integer stylesheetId) {
        this.stylesheetId = stylesheetId;
    }

    public CfStylesheet(Integer stylesheetId, String name, String description) {
        this.stylesheetId = stylesheetId;
        this.name = name;
        this.description = description;
    }

    public Integer getStylesheetId() {
        return stylesheetId;
    }

    public void setStylesheetId(Integer stylesheetId) {
        this.stylesheetId = stylesheetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfPDFDetail> getCfPDFDetailCollection() {
        return cfPDFDetailCollection;
    }

    public void setCfPDFDetailCollection(Collection<CfPDFDetail> cfPDFDetailCollection) {
        this.cfPDFDetailCollection = cfPDFDetailCollection;
    }

    public CfFile getFileIdFk() {
        return fileIdFk;
    }

    public void setFileIdFk(CfFile fileIdFk) {
        this.fileIdFk = fileIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stylesheetId != null ? stylesheetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfStylesheet)) {
            return false;
        }
        CfStylesheet other = (CfStylesheet) object;
        if ((this.stylesheetId == null && other.stylesheetId != null) || (this.stylesheetId != null && !this.stylesheetId.equals(other.stylesheetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfStylesheet[ stylesheetId=" + stylesheetId + " ]";
    }
    
}
