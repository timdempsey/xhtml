/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "AccountKeeperField")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountKeeperField.findAll", query = "SELECT a FROM AccountKeeperField a")
    , @NamedQuery(name = "AccountKeeperField.findByAccountKeeperFieldId", query = "SELECT a FROM AccountKeeperField a WHERE a.accountKeeperFieldId = :accountKeeperFieldId")
    , @NamedQuery(name = "AccountKeeperField.findByFieldIdFk", query = "SELECT a FROM AccountKeeperField a WHERE a.fieldIdFk = :fieldIdFk")
    , @NamedQuery(name = "AccountKeeperField.findByNumberValue", query = "SELECT a FROM AccountKeeperField a WHERE a.numberValue = :numberValue")
    , @NamedQuery(name = "AccountKeeperField.findByStringValue", query = "SELECT a FROM AccountKeeperField a WHERE a.stringValue = :stringValue")
    , @NamedQuery(name = "AccountKeeperField.findByDateValue", query = "SELECT a FROM AccountKeeperField a WHERE a.dateValue = :dateValue")
    , @NamedQuery(name = "AccountKeeperField.findByDateEffective", query = "SELECT a FROM AccountKeeperField a WHERE a.dateEffective = :dateEffective")
    , @NamedQuery(name = "AccountKeeperField.findByDateExpires", query = "SELECT a FROM AccountKeeperField a WHERE a.dateExpires = :dateExpires")
    , @NamedQuery(name = "AccountKeeperField.findByUpdateUserName", query = "SELECT a FROM AccountKeeperField a WHERE a.updateUserName = :updateUserName")
    , @NamedQuery(name = "AccountKeeperField.findByUpdateLast", query = "SELECT a FROM AccountKeeperField a WHERE a.updateLast = :updateLast")})
public class AccountKeeperField implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "accountKeeperFieldId")
    private Integer accountKeeperFieldId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fieldIdFk")
    private int fieldIdFk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "numberValue")
    private BigDecimal numberValue;
    @Size(max = 200)
    @Column(name = "stringValue")
    private String stringValue;
    @Column(name = "dateValue")
    @Temporal(TemporalType.DATE)
    private Date dateValue;
    @Column(name = "dateEffective")
    @Temporal(TemporalType.DATE)
    private Date dateEffective;
    @Column(name = "dateExpires")
    @Temporal(TemporalType.DATE)
    private Date dateExpires;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne(optional = false)
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "regionIdFk", referencedColumnName = "regionId")
    @ManyToOne
    private CfRegion regionIdFk;

    public AccountKeeperField() {
    }

    public AccountKeeperField(Integer accountKeeperFieldId) {
        this.accountKeeperFieldId = accountKeeperFieldId;
    }

    public AccountKeeperField(Integer accountKeeperFieldId, int fieldIdFk) {
        this.accountKeeperFieldId = accountKeeperFieldId;
        this.fieldIdFk = fieldIdFk;
    }

    public Integer getAccountKeeperFieldId() {
        return accountKeeperFieldId;
    }

    public void setAccountKeeperFieldId(Integer accountKeeperFieldId) {
        this.accountKeeperFieldId = accountKeeperFieldId;
    }

    public int getFieldIdFk() {
        return fieldIdFk;
    }

    public void setFieldIdFk(int fieldIdFk) {
        this.fieldIdFk = fieldIdFk;
    }

    public BigDecimal getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(BigDecimal numberValue) {
        this.numberValue = numberValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public CfRegion getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(CfRegion regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountKeeperFieldId != null ? accountKeeperFieldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountKeeperField)) {
            return false;
        }
        AccountKeeperField other = (AccountKeeperField) object;
        if ((this.accountKeeperFieldId == null && other.accountKeeperFieldId != null) || (this.accountKeeperFieldId != null && !this.accountKeeperFieldId.equals(other.accountKeeperFieldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AccountKeeperField[ accountKeeperFieldId=" + accountKeeperFieldId + " ]";
    }
    
}
