/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfFile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfFile.findAll", query = "SELECT c FROM CfFile c")
    , @NamedQuery(name = "CfFile.findByFileId", query = "SELECT c FROM CfFile c WHERE c.fileId = :fileId")
    , @NamedQuery(name = "CfFile.findByDescription", query = "SELECT c FROM CfFile c WHERE c.description = :description")
    , @NamedQuery(name = "CfFile.findByType", query = "SELECT c FROM CfFile c WHERE c.type = :type")
    , @NamedQuery(name = "CfFile.findBySize", query = "SELECT c FROM CfFile c WHERE c.size = :size")
    , @NamedQuery(name = "CfFile.findByEnteredDate", query = "SELECT c FROM CfFile c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "CfFile.findByUpdateUserName", query = "SELECT c FROM CfFile c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfFile.findByUpdateLast", query = "SELECT c FROM CfFile c WHERE c.updateLast = :updateLast")})
public class CfFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fileId")
    private Integer fileId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "size")
    private int size;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cfFile")
    private CfFileData cfFileData;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fileIdFk")
    private Collection<RateWizardUpload> rateWizardUploadCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cfFile")
    private ClaimFile claimFile;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cfFile")
    private ContractPdfFile contractPdfFile;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fileIdFk")
    private Collection<CfPDFDetail> cfPDFDetailCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cfFile")
    private ContractFile contractFile;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fileIdFk")
    private Collection<CfStylesheet> cfStylesheetCollection;

    public CfFile() {
    }

    public CfFile(Integer fileId) {
        this.fileId = fileId;
    }

    public CfFile(Integer fileId, String description, String type, int size, Date enteredDate) {
        this.fileId = fileId;
        this.description = description;
        this.type = type;
        this.size = size;
        this.enteredDate = enteredDate;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public CfFileData getCfFileData() {
        return cfFileData;
    }

    public void setCfFileData(CfFileData cfFileData) {
        this.cfFileData = cfFileData;
    }

    @XmlTransient
    public Collection<RateWizardUpload> getRateWizardUploadCollection() {
        return rateWizardUploadCollection;
    }

    public void setRateWizardUploadCollection(Collection<RateWizardUpload> rateWizardUploadCollection) {
        this.rateWizardUploadCollection = rateWizardUploadCollection;
    }

    public ClaimFile getClaimFile() {
        return claimFile;
    }

    public void setClaimFile(ClaimFile claimFile) {
        this.claimFile = claimFile;
    }

    public ContractPdfFile getContractPdfFile() {
        return contractPdfFile;
    }

    public void setContractPdfFile(ContractPdfFile contractPdfFile) {
        this.contractPdfFile = contractPdfFile;
    }

    @XmlTransient
    public Collection<CfPDFDetail> getCfPDFDetailCollection() {
        return cfPDFDetailCollection;
    }

    public void setCfPDFDetailCollection(Collection<CfPDFDetail> cfPDFDetailCollection) {
        this.cfPDFDetailCollection = cfPDFDetailCollection;
    }

    public ContractFile getContractFile() {
        return contractFile;
    }

    public void setContractFile(ContractFile contractFile) {
        this.contractFile = contractFile;
    }

    @XmlTransient
    public Collection<CfStylesheet> getCfStylesheetCollection() {
        return cfStylesheetCollection;
    }

    public void setCfStylesheetCollection(Collection<CfStylesheet> cfStylesheetCollection) {
        this.cfStylesheetCollection = cfStylesheetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfFile)) {
            return false;
        }
        CfFile other = (CfFile) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfFile[ fileId=" + fileId + " ]";
    }
    
}
