/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Jiepi
 */
@Named(value = "productService")
@ApplicationScoped
public class ProductService {

    private static final Logger LOGGER = LogManager.getLogger(ProductService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of productService
     */
    public ProductService() {
    }

    ProductSearch selectedProduct;

    private List<String> adminCompanies;
    private List<String> disbursementTypes;

    Integer productId;

    ProductSearch prods;
    List<ProductSearch> lprods;

    PlanSearch plans;
    List<PlanSearch> lplans;

    Integer prodActiveIndex;

    private List<String> programs;
    private List<String> insurers;
    private List<String> productTypes;
    private List<String> productSubTypes;
    private List<String> claimSettings;
    private List<String> productSalesGroup;
    private List<String> contractForms;

    PlanSearch selectedPlan;

    Integer programId;

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public PlanSearch getSelectedPlan() {
        return selectedPlan;
    }

    public void setSelectedPlan(PlanSearch selectedPlan) {
        this.selectedPlan = selectedPlan;
    }

    public PlanSearch getPlans() {
        return plans;
    }

    public void setPlans(PlanSearch plans) {
        this.plans = plans;
    }

    public List<PlanSearch> getLplans() {
        return lplans;
    }

    public void setLplans(List<PlanSearch> lplans) {
        this.lplans = lplans;
    }

    public List<String> getProductSalesGroup() {
        return productSalesGroup;
    }

    public void setProductSalesGroup(List<String> productSalesGroup) {
        this.productSalesGroup = productSalesGroup;
    }

    public Integer getProdActiveIndex() {
        return prodActiveIndex;
    }

    public void setProdActiveIndex(Integer prodActiveIndex) {
        this.prodActiveIndex = prodActiveIndex;
    }

    public List<String> getPrograms() {
        return programs;
    }

    public void setPrograms(List<String> programs) {
        this.programs = programs;
    }

    public List<String> getInsurers() {
        return insurers;
    }

    public void setInsurers(List<String> insurers) {
        this.insurers = insurers;
    }

    public List<String> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<String> productTypes) {
        this.productTypes = productTypes;
    }

    public List<String> getProductSubTypes() {
        return productSubTypes;
    }

    public void setProductSubTypes(List<String> productSubTypes) {
        this.productSubTypes = productSubTypes;
    }

    public List<String> getClaimSettings() {
        return claimSettings;
    }

    public void setClaimSettings(List<String> claimSettings) {
        this.claimSettings = claimSettings;
    }

    public List<String> getContractForms() {
        return contractForms;
    }

    public void setContractForms(List<String> contractForms) {
        this.contractForms = contractForms;
    }

    public ProductSearch getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(ProductSearch selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public List<ProductSearch> getLprods() {
        return lprods;
    }

    public void setLprods(List<ProductSearch> lprods) {
        this.lprods = lprods;
    }

    public ProductSearch getProds() {
        return prods;
    }

    public void setProds(ProductSearch prods) {
        this.prods = prods;
    }

    public List<String> getDisbursementTypes() {
        return disbursementTypes;
    }

    public void setDisbursementTypes(List<String> disbursementTypes) {
        this.disbursementTypes = disbursementTypes;
    }

    public List<String> getAdminCompanies() {
        return adminCompanies;
    }

    public void setAdminCompanies(List<String> adminCompanies) {
        this.adminCompanies = adminCompanies;
    }

    public void reset() {
        selectedProduct = new ProductSearch();
        prods = new ProductSearch();
        productId = null;
        programId = null;
    }

    public String getProductSearch() {
        LOGGER.info(" in getProductSearch");

        reset();
        init();

        return "ProductSearch";
    }

    private void init() {
        productId=null;
        lprods = claimMisc.getProductSearchs();
        disbursementTypes = setDisbursmentTypes();
        programs = getProgramNames();
        insurers = getIns();
        productTypes = getTypes();
        productSubTypes = getSubTypes();
        claimSettings = setClaimSettings();
        productSalesGroup = setProductSalesGroup();
        contractForms = setContractForms();

    }

    private List<String> setProductSalesGroup() {
        List<String> types = new ArrayList<>();
        List<ProductSalesGroup> ptl = claimMisc.getProductSalesGroupByAll();
        for (ProductSalesGroup pt : ptl) {
            types.add(pt.getName());
        }
        return types;
    }

    private List<String> setContractForms() {
        List<String> types = new ArrayList<>();
        List<ContractForm> ptl = claimMisc.getContractFormByAll();
        for (ContractForm pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    private List<String> setClaimSettings() {
        List<String> dts = new ArrayList<>();
        dts.add("None");
        return dts;
    }

    private List<String> setDisbursmentTypes() {
        List<String> dts = new ArrayList<>();
        dts.add("[Direct Disbursements]");
        return dts;
    }

    List<String> getProgramNames() {
        LOGGER.info("in getProgramNames, programId=" + programId);
        List<String> names = new ArrayList<>();

        if (programId != null) {
            String name = claimMisc.getProgramById(programId).getName();
            names.add(name);
            prods.setProgramName(name);
        } else {
            List<Program> lps = claimMisc.getProgramByAll();
            for (Program program : lps) {
                names.add(program.getName());
            }
        }
        return names;
    }

    List<String> getIns() {
        List<String> ins = new ArrayList<>();
        List<AccountKeeper> akl = claimMisc.getAccountKeeperByInsurer();
        for (AccountKeeper ak : akl) {
            ins.add(ak.getAccountKeeperName());
        }
        return ins;
    }

    List<String> getTypes() {
        List<String> types = new ArrayList<>();
        List<ProductType> ptl = claimMisc.getProductTypeByAll();
        for (ProductType pt : ptl) {
            types.add(pt.getProductTypeName());
        }
        return types;
    }

    List<String> getSubTypes() {
        List<String> types = new ArrayList<>();
        List<ProductSubType> ptl = claimMisc.getProductSubTypeByAll();
        for (ProductSubType pt : ptl) {
            types.add(pt.getDescription());
        }
        return types;
    }

    public void onRowSelect(SelectEvent event) {
        selectedProduct = (ProductSearch) event.getObject();

        FacesMessage msg = new FacesMessage("Product Selected", ((ProductSearch) event.getObject()).getProductName());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void addProduct() {

        LOGGER.info("in addProduct");
        if (prods != null) {
            prods.reset();
        } else {
            prods = new ProductSearch();
        }

        init();

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "850");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addProduct", options, null);

    }

    public void saveProduct() {
        LOGGER.info("in saveProduct, productId=" + productId);
        try {
            utx.begin();
            if (productId == null) {
                String updateUserName = "JP";
                boolean financeLengthBasedTermInd = false;
                boolean mileageBasedTermInd = false;
                boolean renewableInd = false;
                Integer productUsageTypeFk = 1;   // later
                Integer contractFormIdFk = null;

                Query query = em.createNativeQuery("insert into Product (productName, productCode, programIdFk, insurerIdFk, productTypeIdFk, productSubTypeFk,  productUsageTypeFk, updateUserName, financeLengthBasedTermInd, mileageBasedTermInd, renewableInd, contractFormIdFk) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)");
                query.setParameter(1, prods.getProductName());
                query.setParameter(2, prods.getProductCode());
                query.setParameter(3, claimMisc.getProgramByName(prods.getProgramName()).getProgramId());
                query.setParameter(4, claimMisc.getInsurerByAccountKeeperName(prods.getInsurer()).getInsurerId());
                query.setParameter(5, claimMisc.getProductTypeByName(prods.getProductType()).getProductTypeId());
                query.setParameter(6, claimMisc.getProductSubTypeByName(prods.getProductSubType()).getProductSubTypeId());
                query.setParameter(7, productUsageTypeFk);
                query.setParameter(8, updateUserName);
                query.setParameter(9, financeLengthBasedTermInd);
                query.setParameter(10, mileageBasedTermInd);
                query.setParameter(11, renewableInd);
                if (prods.getContractForm() != null && prods.getContractForm().length() > 0) {
                    contractFormIdFk = claimMisc.getContractFormByName(prods.getContractForm()).getContractFormId();
                }
                query.setParameter(12, contractFormIdFk);

                int ret = query.executeUpdate();

                LOGGER.info("ret=" + ret);
                if (ret == 1) {
                    Integer productIdFk = claimMisc.getLatestInsertedId("Product");
                    Integer enteredByIdFk = 21367; // later
                    LOGGER.info("effectiveDate=" + prods.getEffectiveDate());
                    query = em.createNativeQuery("insert into ProductDetail (productIdFk, effectiveDate,  enteredByIdFk, enteredDate, deletedInd, description, disbursementCenterIdFk, updateUserName) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)");
                    query.setParameter(1, productIdFk);
                    query.setParameter(2, claimMisc.convertFromUtilDate(prods.getEffectiveDate()));
                    query.setParameter(3, enteredByIdFk);
                    query.setParameter(4, claimMisc.getCurrnetDateTime());
                    query.setParameter(5, 0);
                    query.setParameter(6, prods.getDescription());
                    query.setParameter(7, 1);
                    query.setParameter(8, updateUserName);
                    ret = query.executeUpdate();
                    LOGGER.info("ProductDetail insertion, ret=" + ret);
                }
            } else {
                Product product = claimMisc.getProductById(productId);
                ProductDetail pd = claimMisc.getProductDetailByFkId(productId);
                product.setProductName(prods.getProductName());
                product.setProductCode(prods.getProductCode());
                pd.setDescription(prods.getDescription());
                pd.setEffectiveDate(prods.getEffectiveDate());
                em.merge(product);
                em.merge(pd);

            }
            utx.commit();
            RequestContext.getCurrentInstance().closeDialog("newProduct");

        } catch (ConstraintViolationException e) {

            for (ConstraintViolation violation : e.getConstraintViolations()) {
                String key = "";

                if (violation.getPropertyPath() != null) {
                    key = violation.getPropertyPath().toString();
                    LOGGER.info("key=" + key);
                    LOGGER.info("messages:" + violation.getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.info("ERROR: in persist....");
            LOGGER.info(ex.getStackTrace());
            LOGGER.info(ex.toString());

            try {
                utx.rollback();

            } catch (Exception e) {
                LOGGER.info("error in rollback");
            }
        }
    }

    public void dialogClosed(SelectEvent event) {
        LOGGER.info("in dialogClosed");
        lprods = claimMisc.getProductSearchs();
    }

    public void checkProductName() {
        if (claimMisc.getProgramByName(prods.getProductName()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This product name already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void checkProductCode() {
        if (claimMisc.getProductByCode(prods.getProductCode()) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "This product code already existed.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void getProductIdAttr(ActionEvent event) {
        productId = (Integer) event.getComponent().getAttributes().get("productId");
        LOGGER.info("in getProductIdAttr, productId=" + productId);
    }

    public void getProgramIdAttr(ActionEvent event) {
        programId = (Integer) event.getComponent().getAttributes().get("programId");
        LOGGER.info("in getProgramIdAttr, programId=" + programId);
    }

    public String getProductPlan() {
        if( selectedProduct != null ) {
        productId = selectedProduct.getProductId();
        }
        LOGGER.info("in getProductPlan, productId=" + productId);
        lplans = claimMisc.getPlanSearchByProductId(productId);
        return "ProductPlan";
    }

    public String deleteProduct() {
        productId = selectedProduct.getProductId();
        LOGGER.info("in deleteProduct, productId=" + productId);
        claimMisc.deleteProduct(productId);
        lprods = claimMisc.getProductSearchs();
        return "ProductSearch";
    }

    public void editProduct() {
        LOGGER.info("in editProduct");

        prods.setAdminCompany(selectedProduct.getAdminCompany());
        prods.setClaimSetting(selectedProduct.getClaimSetting());
        prods.setContractForm(selectedProduct.getContractForm());
        prods.setDescription(selectedProduct.getDescription());
        prods.setDisbursementType(selectedProduct.getDisbursementType());
        prods.setEffectiveDate(selectedProduct.getEffectiveDate());
        prods.setExpireDate(selectedProduct.getExpireDate());
        prods.setInsurer(selectedProduct.getInsurer());
        prods.setProductCode(selectedProduct.getProductCode());
        prods.setProductName(selectedProduct.getProductName());
        prods.setProductSubType(selectedProduct.getProductSubType());
        prods.setProductType(selectedProduct.getProductType());
        prods.setProgramName(selectedProduct.getProgramName());
        prods.setDisbursementType(selectedProduct.getDescription());

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("contentHeight", "100%");
        options.put("contentWidth", "100%");
        options.put("height", "500");
        options.put("width", "700");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("addProduct", options, null);

    }

    public void productEditClosed() {

        LOGGER.info("in productEditClosed, description=" + prods.getDescription());
        selectedProduct.setAdminCompany(prods.getAdminCompany());
        selectedProduct.setClaimSetting(prods.getAdminCompany());
        selectedProduct.setContractForm(prods.getContractForm());
        selectedProduct.setDescription(prods.getDescription());
        selectedProduct.setEffectiveDate(prods.getEffectiveDate());
        selectedProduct.setExpireDate(prods.getExpireDate());
        selectedProduct.setInsurer(prods.getInsurer());
        selectedProduct.setProductCode(prods.getProductCode());
        selectedProduct.setProductName(prods.getProductName());
        selectedProduct.setProductSubType(prods.getProductSubType());
        selectedProduct.setProductType(prods.getProductType());
        selectedProduct.setProgramName(prods.getProgramName());
        selectedProduct.setSalesGroup(prods.getSalesGroup());

    }

    public String deleteProduct(ActionEvent event) {
        productId = selectedProduct.getProductId();
        LOGGER.info("in deleteProduct, productId=" + productId);
        claimMisc.deleteProduct(productId);
        lprods = claimMisc.getProductSearchs();
        return "ProductSearch";
    }
    
    

}
