/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import javax.persistence.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimVehicleInfo implements Serializable{
    private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    public ClaimVehicleInfo() {
    }

    public ClaimVehicleInfo(String vinFull, String make, String model, String series, Integer vehicleYear, String driveType, String cylinders, String cubicInchDisplacement, String cubicCentimeterDisplacement, String literDisplacemant, String vehicleWeightRating, String fuelType, String fuelDelivery, String tonRating, String vehicleType, String basicWarranty, String powertrainWarranty, String rustWarranty) {
        this.vinFull = vinFull;
        this.make = make;
        this.model = model;
        this.series = series;
        this.vehicleYear = vehicleYear;
        this.driveType = driveType;
        this.cylinders = cylinders;
        this.cubicInchDisplacement = cubicInchDisplacement;
        this.cubicCentimeterDisplacement = cubicCentimeterDisplacement;
        this.literDisplacemant = literDisplacemant;
        this.vehicleWeightRating = vehicleWeightRating;
        this.fuelType = fuelType;
        this.fuelDelivery = fuelDelivery;
        this.tonRating = tonRating;
        this.vehicleType = vehicleType;
        this.basicWarranty = basicWarranty;
        this.powertrainWarranty = powertrainWarranty;
        this.rustWarranty = rustWarranty;
    }
    
    
    
    @Column(name = "vinFull", table = "Contracts")
    private String vinFull;
    @Column(name = "make", table = "VinDesc")
    private String make;
    @Column(name = "model", table = "VinDesc")
    private String model;
    @Column(name = "series", table = "VinDesc")
    private String series;
    @Column(name = "vehicleYear", table = "VinDesc")
    private Integer vehicleYear;
    @Column(name = "driveType", table = "VinDesc")
    private String driveType;
    @Column(name = "cylinders", table = "VinDesc")
    private String cylinders;
    @Column(name = "cubicInchDisplacement", table = "VinDesc")
    private String cubicInchDisplacement;
    @Column(name = "cubicCentimeterDisplacement", table = "VinDesc")
    private String cubicCentimeterDisplacement;
    @Column(name = "literDisplacemant", table = "VinDesc")
    private String literDisplacemant;
    @Column(name = "vehicleWeightRating", table = "VinDesc")
    private String vehicleWeightRating;
    @Column(name = "fuelTypeIdFk", table = "VinDesc")
    private String fuelType;
    @Column(name = "fuelDeliveryIdFk", table = "VinDesc")
    private String fuelDelivery;
    @Column(name = "tonRating", table = "VinDesc")
    private String tonRating;
    @Column(name = "vehicleType", table = "VinDesc")
    private String vehicleType;
    private String basicWarranty;
    private String powertrainWarranty;
    private String rustWarranty;

    public String getVinFull() {
        return vinFull;
    }

    public void setVinFull(String vinFull) {
        this.vinFull = vinFull;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getCubicInchDisplacement() {
        return cubicInchDisplacement;
    }

    public void setCubicInchDisplacement(String cubicInchDisplacement) {
        this.cubicInchDisplacement = cubicInchDisplacement;
    }

    public String getCubicCentimeterDisplacement() {
        return cubicCentimeterDisplacement;
    }

    public void setCubicCentimeterDisplacement(String cubicCentimeterDisplacement) {
        this.cubicCentimeterDisplacement = cubicCentimeterDisplacement;
    }

    public String getLiterDisplacemant() {
        return literDisplacemant;
    }

    public void setLiterDisplacemant(String literDisplacemant) {
        this.literDisplacemant = literDisplacemant;
    }

    public String getVehicleWeightRating() {
        return vehicleWeightRating;
    }

    public void setVehicleWeightRating(String vehicleWeightRating) {
        this.vehicleWeightRating = vehicleWeightRating;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelDelivery() {
        return fuelDelivery;
    }

    public void setFuelDelivery(String fuelDelivery) {
        this.fuelDelivery = fuelDelivery;
    }

    public String getTonRating() {
        return tonRating;
    }

    public void setTonRating(String tonRating) {
        this.tonRating = tonRating;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getBasicWarranty() {
        return basicWarranty;
    }

    public void setBasicWarranty(String basicWarranty) {
        this.basicWarranty = basicWarranty;
    }

    public String getPowertrainWarranty() {
        return powertrainWarranty;
    }

    public void setPowertrainWarranty(String powertrainWarranty) {
        this.powertrainWarranty = powertrainWarranty;
    }

    public String getRustWarranty() {
        return rustWarranty;
    }

    public void setRustWarranty(String rustWarranty) {
        this.rustWarranty = rustWarranty;
    }
    
    
    
}
