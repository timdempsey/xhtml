/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimEventPPMDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimEventPPMDetail.findAll", query = "SELECT c FROM ClaimEventPPMDetail c")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByClaimEventPPMDetailId", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.claimEventPPMDetailId = :claimEventPPMDetailId")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByAmount", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.amount = :amount")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByEnteredDate", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.enteredDate = :enteredDate")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByDeleted", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.deleted = :deleted")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByUpdateUserName", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimEventPPMDetail.findByUpdateLast", query = "SELECT c FROM ClaimEventPPMDetail c WHERE c.updateLast = :updateLast")})
public class ClaimEventPPMDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimEventPPMDetailId")
    private Integer claimEventPPMDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private BigDecimal amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "deleted")
    private Boolean deleted;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "claimEventPPMIdFk", referencedColumnName = "claimEventPPMId")
    @ManyToOne(optional = false)
    private ClaimEventPPM claimEventPPMIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public ClaimEventPPMDetail() {
    }

    public ClaimEventPPMDetail(Integer claimEventPPMDetailId) {
        this.claimEventPPMDetailId = claimEventPPMDetailId;
    }

    public ClaimEventPPMDetail(Integer claimEventPPMDetailId, BigDecimal amount, Date enteredDate) {
        this.claimEventPPMDetailId = claimEventPPMDetailId;
        this.amount = amount;
        this.enteredDate = enteredDate;
    }

    public Integer getClaimEventPPMDetailId() {
        return claimEventPPMDetailId;
    }

    public void setClaimEventPPMDetailId(Integer claimEventPPMDetailId) {
        this.claimEventPPMDetailId = claimEventPPMDetailId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ClaimEventPPM getClaimEventPPMIdFk() {
        return claimEventPPMIdFk;
    }

    public void setClaimEventPPMIdFk(ClaimEventPPM claimEventPPMIdFk) {
        this.claimEventPPMIdFk = claimEventPPMIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimEventPPMDetailId != null ? claimEventPPMDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimEventPPMDetail)) {
            return false;
        }
        ClaimEventPPMDetail other = (ClaimEventPPMDetail) object;
        if ((this.claimEventPPMDetailId == null && other.claimEventPPMDetailId != null) || (this.claimEventPPMDetailId != null && !this.claimEventPPMDetailId.equals(other.claimEventPPMDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimEventPPMDetail[ claimEventPPMDetailId=" + claimEventPPMDetailId + " ]";
    }
    
}
