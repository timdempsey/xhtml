/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RateVisibility")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RateVisibility.findAll", query = "SELECT r FROM RateVisibility r")
    , @NamedQuery(name = "RateVisibility.findByRateVisibilityId", query = "SELECT r FROM RateVisibility r WHERE r.rateVisibilityId = :rateVisibilityId")
    , @NamedQuery(name = "RateVisibility.findByDescription", query = "SELECT r FROM RateVisibility r WHERE r.description = :description")
    , @NamedQuery(name = "RateVisibility.findByAllowedProductTypes", query = "SELECT r FROM RateVisibility r WHERE r.allowedProductTypes = :allowedProductTypes")
    , @NamedQuery(name = "RateVisibility.findByDeletedInd", query = "SELECT r FROM RateVisibility r WHERE r.deletedInd = :deletedInd")
    , @NamedQuery(name = "RateVisibility.findByUpdateUserName", query = "SELECT r FROM RateVisibility r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RateVisibility.findByUpdateLast", query = "SELECT r FROM RateVisibility r WHERE r.updateLast = :updateLast")
    , @NamedQuery(name = "RateVisibility.findByEffectiveDate", query = "SELECT r FROM RateVisibility r WHERE r.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "RateVisibility.findByExpireDate", query = "SELECT r FROM RateVisibility r WHERE r.expireDate = :expireDate")})
public class RateVisibility implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateVisibilityId")
    private Integer rateVisibilityId;
    @Size(max = 1024)
    @Column(name = "description")
    private String description;
    @Column(name = "allowedProductTypes")
    private Integer allowedProductTypes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "expireDate")
    @Temporal(TemporalType.DATE)
    private Date expireDate;
    @OneToMany(mappedBy = "rateVisibilityIdFk")
    private Collection<DealerRateVisibilityRel> dealerRateVisibilityRelCollection;
    @JoinColumn(name = "rateVisibilityId", referencedColumnName = "rateBasedRuleGroupId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private RateBasedRuleGroup rateBasedRuleGroup;
    @OneToMany(mappedBy = "rateVisibilityIdFk")
    private Collection<Dealer> dealerCollection;

    public RateVisibility() {
    }

    public RateVisibility(Integer rateVisibilityId) {
        this.rateVisibilityId = rateVisibilityId;
    }

    public RateVisibility(Integer rateVisibilityId, boolean deletedInd) {
        this.rateVisibilityId = rateVisibilityId;
        this.deletedInd = deletedInd;
    }

    public Integer getRateVisibilityId() {
        return rateVisibilityId;
    }

    public void setRateVisibilityId(Integer rateVisibilityId) {
        this.rateVisibilityId = rateVisibilityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAllowedProductTypes() {
        return allowedProductTypes;
    }

    public void setAllowedProductTypes(Integer allowedProductTypes) {
        this.allowedProductTypes = allowedProductTypes;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    @XmlTransient
    public Collection<DealerRateVisibilityRel> getDealerRateVisibilityRelCollection() {
        return dealerRateVisibilityRelCollection;
    }

    public void setDealerRateVisibilityRelCollection(Collection<DealerRateVisibilityRel> dealerRateVisibilityRelCollection) {
        this.dealerRateVisibilityRelCollection = dealerRateVisibilityRelCollection;
    }

    public RateBasedRuleGroup getRateBasedRuleGroup() {
        return rateBasedRuleGroup;
    }

    public void setRateBasedRuleGroup(RateBasedRuleGroup rateBasedRuleGroup) {
        this.rateBasedRuleGroup = rateBasedRuleGroup;
    }

    @XmlTransient
    public Collection<Dealer> getDealerCollection() {
        return dealerCollection;
    }

    public void setDealerCollection(Collection<Dealer> dealerCollection) {
        this.dealerCollection = dealerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateVisibilityId != null ? rateVisibilityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RateVisibility)) {
            return false;
        }
        RateVisibility other = (RateVisibility) object;
        if ((this.rateVisibilityId == null && other.rateVisibilityId != null) || (this.rateVisibilityId != null && !this.rateVisibilityId.equals(other.rateVisibilityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RateVisibility[ rateVisibilityId=" + rateVisibilityId + " ]";
    }
    
}
