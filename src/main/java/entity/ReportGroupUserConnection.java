/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReportGroupUserConnection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportGroupUserConnection.findAll", query = "SELECT r FROM ReportGroupUserConnection r")
    , @NamedQuery(name = "ReportGroupUserConnection.findByReportGroupUserConnectionId", query = "SELECT r FROM ReportGroupUserConnection r WHERE r.reportGroupUserConnectionId = :reportGroupUserConnectionId")
    , @NamedQuery(name = "ReportGroupUserConnection.findByUpdateUserName", query = "SELECT r FROM ReportGroupUserConnection r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReportGroupUserConnection.findByUpdateLast", query = "SELECT r FROM ReportGroupUserConnection r WHERE r.updateLast = :updateLast")})
public class ReportGroupUserConnection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ReportGroupUserConnectionId")
    private Integer reportGroupUserConnectionId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ReportGroupId", referencedColumnName = "reportGroupId")
    @ManyToOne(optional = false)
    private ReportGroup reportGroupId;
    @JoinColumn(name = "UserMemberId", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember userMemberId;

    public ReportGroupUserConnection() {
    }

    public ReportGroupUserConnection(Integer reportGroupUserConnectionId) {
        this.reportGroupUserConnectionId = reportGroupUserConnectionId;
    }

    public Integer getReportGroupUserConnectionId() {
        return reportGroupUserConnectionId;
    }

    public void setReportGroupUserConnectionId(Integer reportGroupUserConnectionId) {
        this.reportGroupUserConnectionId = reportGroupUserConnectionId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ReportGroup getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(ReportGroup reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public UserMember getUserMemberId() {
        return userMemberId;
    }

    public void setUserMemberId(UserMember userMemberId) {
        this.userMemberId = userMemberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportGroupUserConnectionId != null ? reportGroupUserConnectionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportGroupUserConnection)) {
            return false;
        }
        ReportGroupUserConnection other = (ReportGroupUserConnection) object;
        if ((this.reportGroupUserConnectionId == null && other.reportGroupUserConnectionId != null) || (this.reportGroupUserConnectionId != null && !this.reportGroupUserConnectionId.equals(other.reportGroupUserConnectionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReportGroupUserConnection[ reportGroupUserConnectionId=" + reportGroupUserConnectionId + " ]";
    }
    
}
