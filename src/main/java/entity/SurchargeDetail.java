/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "SurchargeDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SurchargeDetail.findAll", query = "SELECT s FROM SurchargeDetail s")
    , @NamedQuery(name = "SurchargeDetail.findBySurchargeDetailId", query = "SELECT s FROM SurchargeDetail s WHERE s.surchargeDetailId = :surchargeDetailId")
    , @NamedQuery(name = "SurchargeDetail.findBySuggestedRetailPrice", query = "SELECT s FROM SurchargeDetail s WHERE s.suggestedRetailPrice = :suggestedRetailPrice")
    , @NamedQuery(name = "SurchargeDetail.findByEffectiveDate", query = "SELECT s FROM SurchargeDetail s WHERE s.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "SurchargeDetail.findByEnteredDate", query = "SELECT s FROM SurchargeDetail s WHERE s.enteredDate = :enteredDate")
    , @NamedQuery(name = "SurchargeDetail.findByDeletedInd", query = "SELECT s FROM SurchargeDetail s WHERE s.deletedInd = :deletedInd")
    , @NamedQuery(name = "SurchargeDetail.findByUpdateUserName", query = "SELECT s FROM SurchargeDetail s WHERE s.updateUserName = :updateUserName")
    , @NamedQuery(name = "SurchargeDetail.findByUpdateLast", query = "SELECT s FROM SurchargeDetail s WHERE s.updateLast = :updateLast")})
public class SurchargeDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "surchargeDetailId")
    private Integer surchargeDetailId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "suggestedRetailPrice")
    private BigDecimal suggestedRetailPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deletedInd")
    private boolean deletedInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "coverageGroupIdFk", referencedColumnName = "coverageGroupId")
    @ManyToOne
    private CfCoverageGroup coverageGroupIdFk;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "disbursementCenterIdFk", referencedColumnName = "disbursementCenterId")
    @ManyToOne
    private DisbursementCenter disbursementCenterIdFk;
    @JoinColumn(name = "surchargeIdFk", referencedColumnName = "surchargeId")
    @ManyToOne(optional = false)
    private Surcharge surchargeIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public SurchargeDetail() {
    }

    public SurchargeDetail(Integer surchargeDetailId) {
        this.surchargeDetailId = surchargeDetailId;
    }

    public SurchargeDetail(Integer surchargeDetailId, Date effectiveDate, Date enteredDate, boolean deletedInd) {
        this.surchargeDetailId = surchargeDetailId;
        this.effectiveDate = effectiveDate;
        this.enteredDate = enteredDate;
        this.deletedInd = deletedInd;
    }

    public Integer getSurchargeDetailId() {
        return surchargeDetailId;
    }

    public void setSurchargeDetailId(Integer surchargeDetailId) {
        this.surchargeDetailId = surchargeDetailId;
    }

    public BigDecimal getSuggestedRetailPrice() {
        return suggestedRetailPrice;
    }

    public void setSuggestedRetailPrice(BigDecimal suggestedRetailPrice) {
        this.suggestedRetailPrice = suggestedRetailPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCoverageGroup getCoverageGroupIdFk() {
        return coverageGroupIdFk;
    }

    public void setCoverageGroupIdFk(CfCoverageGroup coverageGroupIdFk) {
        this.coverageGroupIdFk = coverageGroupIdFk;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public DisbursementCenter getDisbursementCenterIdFk() {
        return disbursementCenterIdFk;
    }

    public void setDisbursementCenterIdFk(DisbursementCenter disbursementCenterIdFk) {
        this.disbursementCenterIdFk = disbursementCenterIdFk;
    }

    public Surcharge getSurchargeIdFk() {
        return surchargeIdFk;
    }

    public void setSurchargeIdFk(Surcharge surchargeIdFk) {
        this.surchargeIdFk = surchargeIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (surchargeDetailId != null ? surchargeDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SurchargeDetail)) {
            return false;
        }
        SurchargeDetail other = (SurchargeDetail) object;
        if ((this.surchargeDetailId == null && other.surchargeDetailId != null) || (this.surchargeDetailId != null && !this.surchargeDetailId.equals(other.surchargeDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SurchargeDetail[ surchargeDetailId=" + surchargeDetailId + " ]";
    }
    
}
