/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "MarkupDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarkupDetail.findAll", query = "SELECT m FROM MarkupDetail m")
    , @NamedQuery(name = "MarkupDetail.findByMarkupDetailId", query = "SELECT m FROM MarkupDetail m WHERE m.markupDetailId = :markupDetailId")
    , @NamedQuery(name = "MarkupDetail.findByMarkupTypeInd", query = "SELECT m FROM MarkupDetail m WHERE m.markupTypeInd = :markupTypeInd")
    , @NamedQuery(name = "MarkupDetail.findByMinMarkup", query = "SELECT m FROM MarkupDetail m WHERE m.minMarkup = :minMarkup")
    , @NamedQuery(name = "MarkupDetail.findByMaxMarkup", query = "SELECT m FROM MarkupDetail m WHERE m.maxMarkup = :maxMarkup")
    , @NamedQuery(name = "MarkupDetail.findByMarkupCalculationTypeInd", query = "SELECT m FROM MarkupDetail m WHERE m.markupCalculationTypeInd = :markupCalculationTypeInd")
    , @NamedQuery(name = "MarkupDetail.findByUseAltMarkup", query = "SELECT m FROM MarkupDetail m WHERE m.useAltMarkup = :useAltMarkup")
    , @NamedQuery(name = "MarkupDetail.findByAltMarkupTypeInd", query = "SELECT m FROM MarkupDetail m WHERE m.altMarkupTypeInd = :altMarkupTypeInd")
    , @NamedQuery(name = "MarkupDetail.findByAltMinMarkup", query = "SELECT m FROM MarkupDetail m WHERE m.altMinMarkup = :altMinMarkup")
    , @NamedQuery(name = "MarkupDetail.findByAltMaxMarkup", query = "SELECT m FROM MarkupDetail m WHERE m.altMaxMarkup = :altMaxMarkup")
    , @NamedQuery(name = "MarkupDetail.findByAltMarkupCalculationTypeInd", query = "SELECT m FROM MarkupDetail m WHERE m.altMarkupCalculationTypeInd = :altMarkupCalculationTypeInd")
    , @NamedQuery(name = "MarkupDetail.findByMinMarkupMethodInd", query = "SELECT m FROM MarkupDetail m WHERE m.minMarkupMethodInd = :minMarkupMethodInd")
    , @NamedQuery(name = "MarkupDetail.findByMaxMarkupMethodInd", query = "SELECT m FROM MarkupDetail m WHERE m.maxMarkupMethodInd = :maxMarkupMethodInd")
    , @NamedQuery(name = "MarkupDetail.findByEffectiveDate", query = "SELECT m FROM MarkupDetail m WHERE m.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "MarkupDetail.findByDeletedInd", query = "SELECT m FROM MarkupDetail m WHERE m.deletedInd = :deletedInd")
    , @NamedQuery(name = "MarkupDetail.findByShouldEnforceInContractEntry", query = "SELECT m FROM MarkupDetail m WHERE m.shouldEnforceInContractEntry = :shouldEnforceInContractEntry")
    , @NamedQuery(name = "MarkupDetail.findByEnteredDate", query = "SELECT m FROM MarkupDetail m WHERE m.enteredDate = :enteredDate")
    , @NamedQuery(name = "MarkupDetail.findByUpdateUserName", query = "SELECT m FROM MarkupDetail m WHERE m.updateUserName = :updateUserName")
    , @NamedQuery(name = "MarkupDetail.findByUpdateLast", query = "SELECT m FROM MarkupDetail m WHERE m.updateLast = :updateLast")})
public class MarkupDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "markupDetailId")
    private Integer markupDetailId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "markupTypeInd")
    private int markupTypeInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "minMarkup")
    private BigDecimal minMarkup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxMarkup")
    private BigDecimal maxMarkup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "markupCalculationTypeInd")
    private int markupCalculationTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "useAltMarkup")
    private boolean useAltMarkup;
    @Column(name = "altMarkupTypeInd")
    private Integer altMarkupTypeInd;
    @Column(name = "altMinMarkup")
    private BigDecimal altMinMarkup;
    @Column(name = "altMaxMarkup")
    private BigDecimal altMaxMarkup;
    @Column(name = "altMarkupCalculationTypeInd")
    private Integer altMarkupCalculationTypeInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "minMarkupMethodInd")
    private int minMarkupMethodInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxMarkupMethodInd")
    private int maxMarkupMethodInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "deletedInd")
    private Boolean deletedInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "shouldEnforceInContractEntry")
    private boolean shouldEnforceInContractEntry;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "MarkupIdFk", referencedColumnName = "markupId")
    @ManyToOne(optional = false)
    private Markup markupIdFk;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;

    public MarkupDetail() {
    }

    public MarkupDetail(Integer markupDetailId) {
        this.markupDetailId = markupDetailId;
    }

    public MarkupDetail(Integer markupDetailId, int markupTypeInd, BigDecimal minMarkup, BigDecimal maxMarkup, int markupCalculationTypeInd, boolean useAltMarkup, int minMarkupMethodInd, int maxMarkupMethodInd, Date effectiveDate, boolean shouldEnforceInContractEntry, Date enteredDate) {
        this.markupDetailId = markupDetailId;
        this.markupTypeInd = markupTypeInd;
        this.minMarkup = minMarkup;
        this.maxMarkup = maxMarkup;
        this.markupCalculationTypeInd = markupCalculationTypeInd;
        this.useAltMarkup = useAltMarkup;
        this.minMarkupMethodInd = minMarkupMethodInd;
        this.maxMarkupMethodInd = maxMarkupMethodInd;
        this.effectiveDate = effectiveDate;
        this.shouldEnforceInContractEntry = shouldEnforceInContractEntry;
        this.enteredDate = enteredDate;
    }

    public Integer getMarkupDetailId() {
        return markupDetailId;
    }

    public void setMarkupDetailId(Integer markupDetailId) {
        this.markupDetailId = markupDetailId;
    }

    public int getMarkupTypeInd() {
        return markupTypeInd;
    }

    public void setMarkupTypeInd(int markupTypeInd) {
        this.markupTypeInd = markupTypeInd;
    }

    public BigDecimal getMinMarkup() {
        return minMarkup;
    }

    public void setMinMarkup(BigDecimal minMarkup) {
        this.minMarkup = minMarkup;
    }

    public BigDecimal getMaxMarkup() {
        return maxMarkup;
    }

    public void setMaxMarkup(BigDecimal maxMarkup) {
        this.maxMarkup = maxMarkup;
    }

    public int getMarkupCalculationTypeInd() {
        return markupCalculationTypeInd;
    }

    public void setMarkupCalculationTypeInd(int markupCalculationTypeInd) {
        this.markupCalculationTypeInd = markupCalculationTypeInd;
    }

    public boolean getUseAltMarkup() {
        return useAltMarkup;
    }

    public void setUseAltMarkup(boolean useAltMarkup) {
        this.useAltMarkup = useAltMarkup;
    }

    public Integer getAltMarkupTypeInd() {
        return altMarkupTypeInd;
    }

    public void setAltMarkupTypeInd(Integer altMarkupTypeInd) {
        this.altMarkupTypeInd = altMarkupTypeInd;
    }

    public BigDecimal getAltMinMarkup() {
        return altMinMarkup;
    }

    public void setAltMinMarkup(BigDecimal altMinMarkup) {
        this.altMinMarkup = altMinMarkup;
    }

    public BigDecimal getAltMaxMarkup() {
        return altMaxMarkup;
    }

    public void setAltMaxMarkup(BigDecimal altMaxMarkup) {
        this.altMaxMarkup = altMaxMarkup;
    }

    public Integer getAltMarkupCalculationTypeInd() {
        return altMarkupCalculationTypeInd;
    }

    public void setAltMarkupCalculationTypeInd(Integer altMarkupCalculationTypeInd) {
        this.altMarkupCalculationTypeInd = altMarkupCalculationTypeInd;
    }

    public int getMinMarkupMethodInd() {
        return minMarkupMethodInd;
    }

    public void setMinMarkupMethodInd(int minMarkupMethodInd) {
        this.minMarkupMethodInd = minMarkupMethodInd;
    }

    public int getMaxMarkupMethodInd() {
        return maxMarkupMethodInd;
    }

    public void setMaxMarkupMethodInd(int maxMarkupMethodInd) {
        this.maxMarkupMethodInd = maxMarkupMethodInd;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Boolean getDeletedInd() {
        return deletedInd;
    }

    public void setDeletedInd(Boolean deletedInd) {
        this.deletedInd = deletedInd;
    }

    public boolean getShouldEnforceInContractEntry() {
        return shouldEnforceInContractEntry;
    }

    public void setShouldEnforceInContractEntry(boolean shouldEnforceInContractEntry) {
        this.shouldEnforceInContractEntry = shouldEnforceInContractEntry;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public Markup getMarkupIdFk() {
        return markupIdFk;
    }

    public void setMarkupIdFk(Markup markupIdFk) {
        this.markupIdFk = markupIdFk;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (markupDetailId != null ? markupDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarkupDetail)) {
            return false;
        }
        MarkupDetail other = (MarkupDetail) object;
        if ((this.markupDetailId == null && other.markupDetailId != null) || (this.markupDetailId != null && !this.markupDetailId.equals(other.markupDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MarkupDetail[ markupDetailId=" + markupDetailId + " ]";
    }
    
}
