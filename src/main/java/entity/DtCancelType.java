/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtCancelType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtCancelType.findAll", query = "SELECT d FROM DtCancelType d")
    , @NamedQuery(name = "DtCancelType.findByCancelTypeId", query = "SELECT d FROM DtCancelType d WHERE d.cancelTypeId = :cancelTypeId")
    , @NamedQuery(name = "DtCancelType.findByDescription", query = "SELECT d FROM DtCancelType d WHERE d.description = :description")})
public class DtCancelType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelTypeId")
    private Integer cancelTypeId;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelTypeInd")
    private Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cancelTypeInd")
    private Collection<CancelRequest> cancelRequestCollection;

    public DtCancelType() {
    }

    public DtCancelType(Integer cancelTypeId) {
        this.cancelTypeId = cancelTypeId;
    }

    public Integer getCancelTypeId() {
        return cancelTypeId;
    }

    public void setCancelTypeId(Integer cancelTypeId) {
        this.cancelTypeId = cancelTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<CfCancelMethodGroupRule> getCfCancelMethodGroupRuleCollection() {
        return cfCancelMethodGroupRuleCollection;
    }

    public void setCfCancelMethodGroupRuleCollection(Collection<CfCancelMethodGroupRule> cfCancelMethodGroupRuleCollection) {
        this.cfCancelMethodGroupRuleCollection = cfCancelMethodGroupRuleCollection;
    }

    @XmlTransient
    public Collection<CancelRequest> getCancelRequestCollection() {
        return cancelRequestCollection;
    }

    public void setCancelRequestCollection(Collection<CancelRequest> cancelRequestCollection) {
        this.cancelRequestCollection = cancelRequestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancelTypeId != null ? cancelTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtCancelType)) {
            return false;
        }
        DtCancelType other = (DtCancelType) object;
        if ((this.cancelTypeId == null && other.cancelTypeId != null) || (this.cancelTypeId != null && !this.cancelTypeId.equals(other.cancelTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtCancelType[ cancelTypeId=" + cancelTypeId + " ]";
    }
    
}
