/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "LedgerEdit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LedgerEdit.findAll", query = "SELECT l FROM LedgerEdit l")
    , @NamedQuery(name = "LedgerEdit.findByLedgerEditId", query = "SELECT l FROM LedgerEdit l WHERE l.ledgerEditId = :ledgerEditId")
    , @NamedQuery(name = "LedgerEdit.findByUpdateUserName", query = "SELECT l FROM LedgerEdit l WHERE l.updateUserName = :updateUserName")
    , @NamedQuery(name = "LedgerEdit.findByUpdateLast", query = "SELECT l FROM LedgerEdit l WHERE l.updateLast = :updateLast")})
public class LedgerEdit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ledgerEditId")
    private Integer ledgerEditId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "ledgerEditId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "editedLedgerIdFk", referencedColumnName = "ledgerId")
    @ManyToOne(optional = false)
    private Ledger editedLedgerIdFk;

    public LedgerEdit() {
    }

    public LedgerEdit(Integer ledgerEditId) {
        this.ledgerEditId = ledgerEditId;
    }

    public Integer getLedgerEditId() {
        return ledgerEditId;
    }

    public void setLedgerEditId(Integer ledgerEditId) {
        this.ledgerEditId = ledgerEditId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Ledger getEditedLedgerIdFk() {
        return editedLedgerIdFk;
    }

    public void setEditedLedgerIdFk(Ledger editedLedgerIdFk) {
        this.editedLedgerIdFk = editedLedgerIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ledgerEditId != null ? ledgerEditId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LedgerEdit)) {
            return false;
        }
        LedgerEdit other = (LedgerEdit) object;
        if ((this.ledgerEditId == null && other.ledgerEditId != null) || (this.ledgerEditId != null && !this.ledgerEditId.equals(other.ledgerEditId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LedgerEdit[ ledgerEditId=" + ledgerEditId + " ]";
    }
    
}
