/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DtCriterionRule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DtCriterionRule.findAll", query = "SELECT d FROM DtCriterionRule d")
    , @NamedQuery(name = "DtCriterionRule.findByCriterionRuleId", query = "SELECT d FROM DtCriterionRule d WHERE d.criterionRuleId = :criterionRuleId")
    , @NamedQuery(name = "DtCriterionRule.findByCriterionRuleName", query = "SELECT d FROM DtCriterionRule d WHERE d.criterionRuleName = :criterionRuleName")})
public class DtCriterionRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "criterionRuleId")
    private Integer criterionRuleId;
    @Size(max = 50)
    @Column(name = "criterionRuleName")
    private String criterionRuleName;

    public DtCriterionRule() {
    }

    public DtCriterionRule(Integer criterionRuleId) {
        this.criterionRuleId = criterionRuleId;
    }

    public Integer getCriterionRuleId() {
        return criterionRuleId;
    }

    public void setCriterionRuleId(Integer criterionRuleId) {
        this.criterionRuleId = criterionRuleId;
    }

    public String getCriterionRuleName() {
        return criterionRuleName;
    }

    public void setCriterionRuleName(String criterionRuleName) {
        this.criterionRuleName = criterionRuleName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterionRuleId != null ? criterionRuleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtCriterionRule)) {
            return false;
        }
        DtCriterionRule other = (DtCriterionRule) object;
        if ((this.criterionRuleId == null && other.criterionRuleId != null) || (this.criterionRuleId != null && !this.criterionRuleId.equals(other.criterionRuleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DtCriterionRule[ criterionRuleId=" + criterionRuleId + " ]";
    }
    
}
