/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractCancel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractCancel.findAll", query = "SELECT c FROM ContractCancel c")
    , @NamedQuery(name = "ContractCancel.findByContractCancelId", query = "SELECT c FROM ContractCancel c WHERE c.contractCancelId = :contractCancelId")
    , @NamedQuery(name = "ContractCancel.findByPayDealerPortion", query = "SELECT c FROM ContractCancel c WHERE c.payDealerPortion = :payDealerPortion")
    , @NamedQuery(name = "ContractCancel.findByUpdateUserName", query = "SELECT c FROM ContractCancel c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractCancel.findByUpdateLast", query = "SELECT c FROM ContractCancel c WHERE c.updateLast = :updateLast")})
public class ContractCancel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractCancelId")
    private Integer contractCancelId;
    @Column(name = "payDealerPortion")
    private Boolean payDealerPortion;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "relatedCancelIdFk")
    private Collection<Ledger> ledgerCollection;
    @JoinColumn(name = "cancelRequestIdFk", referencedColumnName = "cancelRequestId")
    @ManyToOne
    private CancelRequest cancelRequestIdFk;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "contractCancelId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @OneToMany(mappedBy = "contractCancelIdFk")
    private Collection<Reinstatement> reinstatementCollection;
    @OneToMany(mappedBy = "contractcancelIdFk")
    private Collection<CancelDisbursement> cancelDisbursementCollection;

    public ContractCancel() {
    }

    public ContractCancel(Integer contractCancelId) {
        this.contractCancelId = contractCancelId;
    }

    public Integer getContractCancelId() {
        return contractCancelId;
    }

    public void setContractCancelId(Integer contractCancelId) {
        this.contractCancelId = contractCancelId;
    }

    public Boolean getPayDealerPortion() {
        return payDealerPortion;
    }

    public void setPayDealerPortion(Boolean payDealerPortion) {
        this.payDealerPortion = payDealerPortion;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    public CancelRequest getCancelRequestIdFk() {
        return cancelRequestIdFk;
    }

    public void setCancelRequestIdFk(CancelRequest cancelRequestIdFk) {
        this.cancelRequestIdFk = cancelRequestIdFk;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @XmlTransient
    public Collection<Reinstatement> getReinstatementCollection() {
        return reinstatementCollection;
    }

    public void setReinstatementCollection(Collection<Reinstatement> reinstatementCollection) {
        this.reinstatementCollection = reinstatementCollection;
    }

    @XmlTransient
    public Collection<CancelDisbursement> getCancelDisbursementCollection() {
        return cancelDisbursementCollection;
    }

    public void setCancelDisbursementCollection(Collection<CancelDisbursement> cancelDisbursementCollection) {
        this.cancelDisbursementCollection = cancelDisbursementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractCancelId != null ? contractCancelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractCancel)) {
            return false;
        }
        ContractCancel other = (ContractCancel) object;
        if ((this.contractCancelId == null && other.contractCancelId != null) || (this.contractCancelId != null && !this.contractCancelId.equals(other.contractCancelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractCancel[ contractCancelId=" + contractCancelId + " ]";
    }
    
}
