/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class DealerProduct {

    private static final Logger LOGGER = LogManager.getLogger(DealerProduct.class);

    public DealerProduct(Integer prodId, String prodName, BigDecimal rateAmount, String contractNo, Integer planIdSelcted, Integer termIdSelectded, Integer limitIdSelected, Integer ancMonthsSelected, Integer expTimeSelected, Integer expUsageSelected, Integer lowerTimeSelected, Integer upperTimeSelected, HashMap<String, DealerPlan> dplanList) {
        this.prodId = prodId;
        this.prodName = prodName;
        this.rateAmount = rateAmount;
        this.contractNo = contractNo;
        this.planIdSelcted = planIdSelcted;
        this.termIdSelectded = termIdSelectded;
        this.limitIdSelected = limitIdSelected;
        this.ancMonthsSelected = ancMonthsSelected;
        this.expTimeSelected = expTimeSelected;
        this.expUsageSelected = expUsageSelected;
        this.lowerTimeSelected = lowerTimeSelected;
        this.upperTimeSelected = upperTimeSelected;
        this.dplanList = dplanList;
    }

   

    

   
    
    public DealerProduct(DealerProduct dp) {
        this.prodId = dp.getProdId();
        this.prodName = dp.getProdName();
        this.rateAmount = dp.rateAmount;
        this.dplanList = dp.getDplanList();
        this.contractNo = dp.contractNo;
        this.planIdSelcted = dp.planIdSelcted;
        this.termIdSelectded = dp.termIdSelectded;
        this.limitIdSelected = dp.limitIdSelected;
        this.ancMonthsSelected = dp.ancMonthsSelected;
        this.expTimeSelected = dp.expTimeSelected;
        this.expUsageSelected = dp.expUsageSelected;
        this.lowerTimeSelected = dp.lowerTimeSelected;
        this.upperTimeSelected = dp.upperTimeSelected;
    }

    public DealerProduct() {
        LOGGER.info("default DealerProduct constructor");
        //dplanList = new ArrayList<>();
        dplanList = new HashMap<>();
    }

    private Integer prodId;
    private String prodName;
    private BigDecimal rateAmount;
    private String contractNo;
    private Integer planIdSelcted;
    private Integer termIdSelectded;
    private Integer limitIdSelected;
    private Integer ancMonthsSelected;
    private Integer expTimeSelected; 
    private Integer expUsageSelected;
    private Integer lowerTimeSelected;
    private Integer upperTimeSelected;
    private HashMap<String, DealerPlan> dplanList;
    //private List<DealerPlan> dplanList;

    public Integer getLowerTimeSelected() {
        return lowerTimeSelected;
    }

    public void setLowerTimeSelected(Integer lowerTimeSelected) {
        this.lowerTimeSelected = lowerTimeSelected;
    }

    public Integer getUpperTimeSelected() {
        return upperTimeSelected;
    }

    public void setUpperTimeSelected(Integer upperTimeSelected) {
        this.upperTimeSelected = upperTimeSelected;
    }
    
    

    public Integer getAncMonthsSelected() {
        return ancMonthsSelected;
    }

    public void setAncMonthsSelected(Integer ancMonthsSelected) {
        this.ancMonthsSelected = ancMonthsSelected;
    }

    public Integer getExpTimeSelected() {
        return expTimeSelected;
    }

    public void setExpTimeSelected(Integer expTimeSelected) {
        this.expTimeSelected = expTimeSelected;
    }

    public Integer getExpUsageSelected() {
        return expUsageSelected;
    }

    public void setExpUsageSelected(Integer expUsageSelected) {
        this.expUsageSelected = expUsageSelected;
    }

   
    

    public Integer getPlanIdSelcted() {
        return planIdSelcted;
    }

    public void setPlanIdSelcted(Integer planIdSelcted) {
        this.planIdSelcted = planIdSelcted;
    }

    public Integer getTermIdSelectded() {
        return termIdSelectded;
    }

    public void setTermIdSelectded(Integer termIdSelectded) {
        this.termIdSelectded = termIdSelectded;
    }

    public Integer getLimitIdSelected() {
        return limitIdSelected;
    }

    public void setLimitIdSelected(Integer limitIdSelected) {
        this.limitIdSelected = limitIdSelected;
    }
    
    

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }
    
    

    public BigDecimal getRateAmount() {
        return rateAmount;
    }

    public void setRateAmount(BigDecimal rateAmount) {
        this.rateAmount = rateAmount;
    }
    
    

    public Integer getProdId() {
        return prodId;
    }

    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public HashMap<String, DealerPlan> getDplanList() {
        return dplanList;
    }

    public void setDplanList(HashMap<String, DealerPlan> dplanList) {
        this.dplanList = dplanList;
    }

    
    

    
    public void reset() {
        prodId = null;
        prodName = "";
        rateAmount = new BigDecimal(0);
        dplanList = new HashMap<>();
        contractNo = "";
        planIdSelcted = null;
        termIdSelectded = null;
        limitIdSelected = null;
        ancMonthsSelected = null;
        expTimeSelected = null;
        expUsageSelected = null;
        lowerTimeSelected = null;
        upperTimeSelected = null;
    }

}
