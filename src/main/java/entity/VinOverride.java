/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "VinOverride")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VinOverride.findAll", query = "SELECT v FROM VinOverride v")
    , @NamedQuery(name = "VinOverride.findByVinDescIdFk", query = "SELECT v FROM VinOverride v WHERE v.vinDescIdFk = :vinDescIdFk")
    , @NamedQuery(name = "VinOverride.findByHasNewMatch", query = "SELECT v FROM VinOverride v WHERE v.hasNewMatch = :hasNewMatch")
    , @NamedQuery(name = "VinOverride.findByEnteredDate", query = "SELECT v FROM VinOverride v WHERE v.enteredDate = :enteredDate")
    , @NamedQuery(name = "VinOverride.findByUpdateUserName", query = "SELECT v FROM VinOverride v WHERE v.updateUserName = :updateUserName")
    , @NamedQuery(name = "VinOverride.findByUpdateLast", query = "SELECT v FROM VinOverride v WHERE v.updateLast = :updateLast")})
public class VinOverride implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "vinDescIdFk")
    private Integer vinDescIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HasNewMatch")
    private boolean hasNewMatch;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EnteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne(optional = false)
    private UserMember enteredByIdFk;
    @JoinColumn(name = "updatedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember updatedByIdFk;
    @JoinColumn(name = "vinDescIdFk", referencedColumnName = "vinDescId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private VinDesc vinDesc;

    public VinOverride() {
    }

    public VinOverride(Integer vinDescIdFk) {
        this.vinDescIdFk = vinDescIdFk;
    }

    public VinOverride(Integer vinDescIdFk, boolean hasNewMatch, Date enteredDate) {
        this.vinDescIdFk = vinDescIdFk;
        this.hasNewMatch = hasNewMatch;
        this.enteredDate = enteredDate;
    }

    public Integer getVinDescIdFk() {
        return vinDescIdFk;
    }

    public void setVinDescIdFk(Integer vinDescIdFk) {
        this.vinDescIdFk = vinDescIdFk;
    }

    public boolean getHasNewMatch() {
        return hasNewMatch;
    }

    public void setHasNewMatch(boolean hasNewMatch) {
        this.hasNewMatch = hasNewMatch;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public UserMember getUpdatedByIdFk() {
        return updatedByIdFk;
    }

    public void setUpdatedByIdFk(UserMember updatedByIdFk) {
        this.updatedByIdFk = updatedByIdFk;
    }

    public VinDesc getVinDesc() {
        return vinDesc;
    }

    public void setVinDesc(VinDesc vinDesc) {
        this.vinDesc = vinDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vinDescIdFk != null ? vinDescIdFk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VinOverride)) {
            return false;
        }
        VinOverride other = (VinOverride) object;
        if ((this.vinDescIdFk == null && other.vinDescIdFk != null) || (this.vinDescIdFk != null && !this.vinDescIdFk.equals(other.vinDescIdFk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VinOverride[ vinDescIdFk=" + vinDescIdFk + " ]";
    }
    
}
