/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "Limit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Limit.findAll", query = "SELECT l FROM Limit l")
    , @NamedQuery(name = "Limit.findByLimitId", query = "SELECT l FROM Limit l WHERE l.limitId = :limitId")})
public class Limit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "limitId")
    private Integer limitId;
    @OneToMany(mappedBy = "limitIdFk")
    private Collection<Contracts> contractsCollection;
    @OneToMany(mappedBy = "limitIdFk")
    private Collection<RateBasedRule> rateBasedRuleCollection;
    @OneToMany(mappedBy = "limitIdFk")
    private Collection<WarrantyRule> warrantyRuleCollection;
    @OneToMany(mappedBy = "limitIdFk")
    private Collection<Rate> rateCollection;
    @OneToMany(mappedBy = "limitIdFk")
    private Collection<LimitDetail> limitDetailCollection;
    @JoinColumn(name = "planIdFk", referencedColumnName = "planId")
    @ManyToOne(optional = false)
    private PlanTable planIdFk;

    public Limit() {
    }

    public Limit(Integer limitId) {
        this.limitId = limitId;
    }

    public Integer getLimitId() {
        return limitId;
    }

    public void setLimitId(Integer limitId) {
        this.limitId = limitId;
    }

    @XmlTransient
    public Collection<Contracts> getContractsCollection() {
        return contractsCollection;
    }

    public void setContractsCollection(Collection<Contracts> contractsCollection) {
        this.contractsCollection = contractsCollection;
    }

    @XmlTransient
    public Collection<RateBasedRule> getRateBasedRuleCollection() {
        return rateBasedRuleCollection;
    }

    public void setRateBasedRuleCollection(Collection<RateBasedRule> rateBasedRuleCollection) {
        this.rateBasedRuleCollection = rateBasedRuleCollection;
    }

    @XmlTransient
    public Collection<WarrantyRule> getWarrantyRuleCollection() {
        return warrantyRuleCollection;
    }

    public void setWarrantyRuleCollection(Collection<WarrantyRule> warrantyRuleCollection) {
        this.warrantyRuleCollection = warrantyRuleCollection;
    }

    @XmlTransient
    public Collection<Rate> getRateCollection() {
        return rateCollection;
    }

    public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }

    @XmlTransient
    public Collection<LimitDetail> getLimitDetailCollection() {
        return limitDetailCollection;
    }

    public void setLimitDetailCollection(Collection<LimitDetail> limitDetailCollection) {
        this.limitDetailCollection = limitDetailCollection;
    }

    public PlanTable getPlanIdFk() {
        return planIdFk;
    }

    public void setPlanIdFk(PlanTable planIdFk) {
        this.planIdFk = planIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (limitId != null ? limitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Limit)) {
            return false;
        }
        Limit other = (Limit) object;
        if ((this.limitId == null && other.limitId != null) || (this.limitId != null && !this.limitId.equals(other.limitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Limit[ limitId=" + limitId + " ]";
    }
    
}
