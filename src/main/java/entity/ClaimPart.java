/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimPart")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimPart.findAll", query = "SELECT c FROM ClaimPart c")
    , @NamedQuery(name = "ClaimPart.findByClaimPartId", query = "SELECT c FROM ClaimPart c WHERE c.claimPartId = :claimPartId")
    , @NamedQuery(name = "ClaimPart.findByPartNumber", query = "SELECT c FROM ClaimPart c WHERE c.partNumber = :partNumber")
    , @NamedQuery(name = "ClaimPart.findByPartDesc", query = "SELECT c FROM ClaimPart c WHERE c.partDesc = :partDesc")
    , @NamedQuery(name = "ClaimPart.findByPartReq", query = "SELECT c FROM ClaimPart c WHERE c.partReq = :partReq")
    , @NamedQuery(name = "ClaimPart.findByPartReqCost", query = "SELECT c FROM ClaimPart c WHERE c.partReqCost = :partReqCost")
    , @NamedQuery(name = "ClaimPart.findByPartLookupCost", query = "SELECT c FROM ClaimPart c WHERE c.partLookupCost = :partLookupCost")
    , @NamedQuery(name = "ClaimPart.findByPartMonths", query = "SELECT c FROM ClaimPart c WHERE c.partMonths = :partMonths")
    , @NamedQuery(name = "ClaimPart.findByPartMiles", query = "SELECT c FROM ClaimPart c WHERE c.partMiles = :partMiles")
    , @NamedQuery(name = "ClaimPart.findByPartQty", query = "SELECT c FROM ClaimPart c WHERE c.partQty = :partQty")
    , @NamedQuery(name = "ClaimPart.findByPartCost", query = "SELECT c FROM ClaimPart c WHERE c.partCost = :partCost")
    , @NamedQuery(name = "ClaimPart.findByPartTaxRate", query = "SELECT c FROM ClaimPart c WHERE c.partTaxRate = :partTaxRate")
    , @NamedQuery(name = "ClaimPart.findByApproveStatus", query = "SELECT c FROM ClaimPart c WHERE c.approveStatus = :approveStatus")
    , @NamedQuery(name = "ClaimPart.findByPaidCost", query = "SELECT c FROM ClaimPart c WHERE c.paidCost = :paidCost")
    , @NamedQuery(name = "ClaimPart.findByPaidQty", query = "SELECT c FROM ClaimPart c WHERE c.paidQty = :paidQty")
    , @NamedQuery(name = "ClaimPart.findByPaidTaxRate", query = "SELECT c FROM ClaimPart c WHERE c.paidTaxRate = :paidTaxRate")
    , @NamedQuery(name = "ClaimPart.findByPaid", query = "SELECT c FROM ClaimPart c WHERE c.paid = :paid")
    , @NamedQuery(name = "ClaimPart.findByDeleted", query = "SELECT c FROM ClaimPart c WHERE c.deleted = :deleted")
    , @NamedQuery(name = "ClaimPart.findByMake", query = "SELECT c FROM ClaimPart c WHERE c.make = :make")
    , @NamedQuery(name = "ClaimPart.findByModel", query = "SELECT c FROM ClaimPart c WHERE c.model = :model")
    , @NamedQuery(name = "ClaimPart.findByVehicleYear", query = "SELECT c FROM ClaimPart c WHERE c.vehicleYear = :vehicleYear")
    , @NamedQuery(name = "ClaimPart.findByPaymentAuthorizationIdFk", query = "SELECT c FROM ClaimPart c WHERE c.paymentAuthorizationIdFk = :paymentAuthorizationIdFk")
    , @NamedQuery(name = "ClaimPart.findByUpdateUserName", query = "SELECT c FROM ClaimPart c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimPart.findByUpdateLast", query = "SELECT c FROM ClaimPart c WHERE c.updateLast = :updateLast")})
public class ClaimPart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimPartId")
    private Integer claimPartId;
    @Size(max = 20)
    @Column(name = "partNumber")
    private String partNumber;
    @Size(max = 200)
    @Column(name = "partDesc")
    private String partDesc;
    @Size(max = 200)
    @Column(name = "partReq")
    private String partReq;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "partReqCost")
    private BigDecimal partReqCost;
    @Column(name = "partLookupCost")
    private BigDecimal partLookupCost;
    @Column(name = "partMonths")
    private Integer partMonths;
    @Column(name = "partMiles")
    private Integer partMiles;
    @Column(name = "partQty")
    private Integer partQty;
    @Column(name = "partCost")
    private BigDecimal partCost;
    @Column(name = "partTaxRate")
    private Long partTaxRate;
    @Size(max = 10)
    @Column(name = "approveStatus")
    private String approveStatus;
    @Column(name = "paidCost")
    private BigDecimal paidCost;
    @Column(name = "paidQty")
    private Integer paidQty;
    @Column(name = "paidTaxRate")
    private BigDecimal paidTaxRate;
    @Column(name = "paid")
    private Integer paid;
    @Column(name = "deleted")
    private Boolean deleted;
    @Size(max = 25)
    @Column(name = "make")
    private String make;
    @Size(max = 25)
    @Column(name = "model")
    private String model;
    @Column(name = "vehicleYear")
    private Integer vehicleYear;
    @Column(name = "paymentAuthorizationIdFk")
    private Integer paymentAuthorizationIdFk;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "detailItemIdFk", referencedColumnName = "detailItemId")
    @ManyToOne
    private ClaimDetailItem detailItemIdFk;
    @JoinColumn(name = "enteredIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredIdFk;

    public ClaimPart() {
    }

    public ClaimPart(Integer claimPartId) {
        this.claimPartId = claimPartId;
    }

    public Integer getClaimPartId() {
        return claimPartId;
    }

    public void setClaimPartId(Integer claimPartId) {
        this.claimPartId = claimPartId;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDesc() {
        return partDesc;
    }

    public void setPartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    public String getPartReq() {
        return partReq;
    }

    public void setPartReq(String partReq) {
        this.partReq = partReq;
    }

    public BigDecimal getPartReqCost() {
        return partReqCost;
    }

    public void setPartReqCost(BigDecimal partReqCost) {
        this.partReqCost = partReqCost;
    }

    public BigDecimal getPartLookupCost() {
        return partLookupCost;
    }

    public void setPartLookupCost(BigDecimal partLookupCost) {
        this.partLookupCost = partLookupCost;
    }

    public Integer getPartMonths() {
        return partMonths;
    }

    public void setPartMonths(Integer partMonths) {
        this.partMonths = partMonths;
    }

    public Integer getPartMiles() {
        return partMiles;
    }

    public void setPartMiles(Integer partMiles) {
        this.partMiles = partMiles;
    }

    public Integer getPartQty() {
        return partQty;
    }

    public void setPartQty(Integer partQty) {
        this.partQty = partQty;
    }

    public BigDecimal getPartCost() {
        return partCost;
    }

    public void setPartCost(BigDecimal partCost) {
        this.partCost = partCost;
    }

    public Long getPartTaxRate() {
        return partTaxRate;
    }

    public void setPartTaxRate(Long partTaxRate) {
        this.partTaxRate = partTaxRate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public BigDecimal getPaidCost() {
        return paidCost;
    }

    public void setPaidCost(BigDecimal paidCost) {
        this.paidCost = paidCost;
    }

    public Integer getPaidQty() {
        return paidQty;
    }

    public void setPaidQty(Integer paidQty) {
        this.paidQty = paidQty;
    }

    public BigDecimal getPaidTaxRate() {
        return paidTaxRate;
    }

    public void setPaidTaxRate(BigDecimal paidTaxRate) {
        this.paidTaxRate = paidTaxRate;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public Integer getPaymentAuthorizationIdFk() {
        return paymentAuthorizationIdFk;
    }

    public void setPaymentAuthorizationIdFk(Integer paymentAuthorizationIdFk) {
        this.paymentAuthorizationIdFk = paymentAuthorizationIdFk;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public ClaimDetailItem getDetailItemIdFk() {
        return detailItemIdFk;
    }

    public void setDetailItemIdFk(ClaimDetailItem detailItemIdFk) {
        this.detailItemIdFk = detailItemIdFk;
    }

    public UserMember getEnteredIdFk() {
        return enteredIdFk;
    }

    public void setEnteredIdFk(UserMember enteredIdFk) {
        this.enteredIdFk = enteredIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimPartId != null ? claimPartId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimPart)) {
            return false;
        }
        ClaimPart other = (ClaimPart) object;
        if ((this.claimPartId == null && other.claimPartId != null) || (this.claimPartId != null && !this.claimPartId.equals(other.claimPartId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimPart[ claimPartId=" + claimPartId + " ]";
    }
    
}
