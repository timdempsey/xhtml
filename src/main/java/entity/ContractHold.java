/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ContractHold")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractHold.findAll", query = "SELECT c FROM ContractHold c")
    , @NamedQuery(name = "ContractHold.findByContractHoldId", query = "SELECT c FROM ContractHold c WHERE c.contractHoldId = :contractHoldId")
    , @NamedQuery(name = "ContractHold.findByHoldTypeInd", query = "SELECT c FROM ContractHold c WHERE c.holdTypeInd = :holdTypeInd")
    , @NamedQuery(name = "ContractHold.findByUpdateUserName", query = "SELECT c FROM ContractHold c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ContractHold.findByUpdateLast", query = "SELECT c FROM ContractHold c WHERE c.updateLast = :updateLast")})
public class ContractHold implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractHoldId")
    private Integer contractHoldId;
    @Column(name = "HoldTypeInd")
    private Integer holdTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "contractHoldIdFk")
    private Collection<HoldDisbursement> holdDisbursementCollection;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "contractHoldId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;
    @JoinColumn(name = "reinstatementIdFk", referencedColumnName = "reinstatementId")
    @ManyToOne
    private Reinstatement reinstatementIdFk;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "contractHoldIdFk")
    private ContractResolve contractResolve;

    public ContractHold() {
    }

    public ContractHold(Integer contractHoldId) {
        this.contractHoldId = contractHoldId;
    }

    public Integer getContractHoldId() {
        return contractHoldId;
    }

    public void setContractHoldId(Integer contractHoldId) {
        this.contractHoldId = contractHoldId;
    }

    public Integer getHoldTypeInd() {
        return holdTypeInd;
    }

    public void setHoldTypeInd(Integer holdTypeInd) {
        this.holdTypeInd = holdTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<HoldDisbursement> getHoldDisbursementCollection() {
        return holdDisbursementCollection;
    }

    public void setHoldDisbursementCollection(Collection<HoldDisbursement> holdDisbursementCollection) {
        this.holdDisbursementCollection = holdDisbursementCollection;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Reinstatement getReinstatementIdFk() {
        return reinstatementIdFk;
    }

    public void setReinstatementIdFk(Reinstatement reinstatementIdFk) {
        this.reinstatementIdFk = reinstatementIdFk;
    }

    public ContractResolve getContractResolve() {
        return contractResolve;
    }

    public void setContractResolve(ContractResolve contractResolve) {
        this.contractResolve = contractResolve;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractHoldId != null ? contractHoldId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractHold)) {
            return false;
        }
        ContractHold other = (ContractHold) object;
        if ((this.contractHoldId == null && other.contractHoldId != null) || (this.contractHoldId != null && !this.contractHoldId.equals(other.contractHoldId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ContractHold[ contractHoldId=" + contractHoldId + " ]";
    }
    
}
