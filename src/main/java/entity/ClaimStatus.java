/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimStatus.findAll", query = "SELECT c FROM ClaimStatus c")
    , @NamedQuery(name = "ClaimStatus.findByClaimStatusInd", query = "SELECT c FROM ClaimStatus c WHERE c.claimStatusInd = :claimStatusInd")
    , @NamedQuery(name = "ClaimStatus.findByClaimStatusDesc", query = "SELECT c FROM ClaimStatus c WHERE c.claimStatusDesc = :claimStatusDesc")})
public class ClaimStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "claimStatusInd")
    private String claimStatusInd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "claimStatusDesc")
    private String claimStatusDesc;
    @OneToMany(mappedBy = "claimStatus")
    private Collection<Claim> claimCollection;

    public ClaimStatus() {
    }

    public ClaimStatus(String claimStatusInd) {
        this.claimStatusInd = claimStatusInd;
    }

    public ClaimStatus(String claimStatusInd, String claimStatusDesc) {
        this.claimStatusInd = claimStatusInd;
        this.claimStatusDesc = claimStatusDesc;
    }

    public String getClaimStatusInd() {
        return claimStatusInd;
    }

    public void setClaimStatusInd(String claimStatusInd) {
        this.claimStatusInd = claimStatusInd;
    }

    public String getClaimStatusDesc() {
        return claimStatusDesc;
    }

    public void setClaimStatusDesc(String claimStatusDesc) {
        this.claimStatusDesc = claimStatusDesc;
    }

    @XmlTransient
    public Collection<Claim> getClaimCollection() {
        return claimCollection;
    }

    public void setClaimCollection(Collection<Claim> claimCollection) {
        this.claimCollection = claimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimStatusInd != null ? claimStatusInd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimStatus)) {
            return false;
        }
        ClaimStatus other = (ClaimStatus) object;
        if ((this.claimStatusInd == null && other.claimStatusInd != null) || (this.claimStatusInd != null && !this.claimStatusInd.equals(other.claimStatusInd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimStatus[ claimStatusInd=" + claimStatusInd + " ]";
    }
    
}
