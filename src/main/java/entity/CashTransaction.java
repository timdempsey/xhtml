/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CashTransaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CashTransaction.findAll", query = "SELECT c FROM CashTransaction c")
    , @NamedQuery(name = "CashTransaction.findByCashTransactionId", query = "SELECT c FROM CashTransaction c WHERE c.cashTransactionId = :cashTransactionId")
    , @NamedQuery(name = "CashTransaction.findByPaymentForTypeInd", query = "SELECT c FROM CashTransaction c WHERE c.paymentForTypeInd = :paymentForTypeInd")
    , @NamedQuery(name = "CashTransaction.findByTransactionNumber", query = "SELECT c FROM CashTransaction c WHERE c.transactionNumber = :transactionNumber")
    , @NamedQuery(name = "CashTransaction.findBySentDate", query = "SELECT c FROM CashTransaction c WHERE c.sentDate = :sentDate")
    , @NamedQuery(name = "CashTransaction.findByBatchIdFk", query = "SELECT c FROM CashTransaction c WHERE c.batchIdFk = :batchIdFk")
    , @NamedQuery(name = "CashTransaction.findByAppliedAmount", query = "SELECT c FROM CashTransaction c WHERE c.appliedAmount = :appliedAmount")
    , @NamedQuery(name = "CashTransaction.findByOverUnder", query = "SELECT c FROM CashTransaction c WHERE c.overUnder = :overUnder")
    , @NamedQuery(name = "CashTransaction.findByForStatementPostingHistoryIdFk", query = "SELECT c FROM CashTransaction c WHERE c.forStatementPostingHistoryIdFk = :forStatementPostingHistoryIdFk")
    , @NamedQuery(name = "CashTransaction.findByPrintedInd", query = "SELECT c FROM CashTransaction c WHERE c.printedInd = :printedInd")
    , @NamedQuery(name = "CashTransaction.findByPrintedDate", query = "SELECT c FROM CashTransaction c WHERE c.printedDate = :printedDate")
    , @NamedQuery(name = "CashTransaction.findByPendingExportUpdateInd", query = "SELECT c FROM CashTransaction c WHERE c.pendingExportUpdateInd = :pendingExportUpdateInd")
    , @NamedQuery(name = "CashTransaction.findByPendingExportDate", query = "SELECT c FROM CashTransaction c WHERE c.pendingExportDate = :pendingExportDate")
    , @NamedQuery(name = "CashTransaction.findByExportedInd", query = "SELECT c FROM CashTransaction c WHERE c.exportedInd = :exportedInd")
    , @NamedQuery(name = "CashTransaction.findByExportedDate", query = "SELECT c FROM CashTransaction c WHERE c.exportedDate = :exportedDate")
    , @NamedQuery(name = "CashTransaction.findByClearedInd", query = "SELECT c FROM CashTransaction c WHERE c.clearedInd = :clearedInd")
    , @NamedQuery(name = "CashTransaction.findByClearedDate", query = "SELECT c FROM CashTransaction c WHERE c.clearedDate = :clearedDate")
    , @NamedQuery(name = "CashTransaction.findByLastImportDate", query = "SELECT c FROM CashTransaction c WHERE c.lastImportDate = :lastImportDate")
    , @NamedQuery(name = "CashTransaction.findBySystemCreatedInd", query = "SELECT c FROM CashTransaction c WHERE c.systemCreatedInd = :systemCreatedInd")
    , @NamedQuery(name = "CashTransaction.findByThirdPartyPaymentProcessorGUID", query = "SELECT c FROM CashTransaction c WHERE c.thirdPartyPaymentProcessorGUID = :thirdPartyPaymentProcessorGUID")
    , @NamedQuery(name = "CashTransaction.findByThirdPartyPaymentProcessorData", query = "SELECT c FROM CashTransaction c WHERE c.thirdPartyPaymentProcessorData = :thirdPartyPaymentProcessorData")
    , @NamedQuery(name = "CashTransaction.findByThirdPartyPaymentProcessorStatusInd", query = "SELECT c FROM CashTransaction c WHERE c.thirdPartyPaymentProcessorStatusInd = :thirdPartyPaymentProcessorStatusInd")
    , @NamedQuery(name = "CashTransaction.findByHoldFromPostingInd", query = "SELECT c FROM CashTransaction c WHERE c.holdFromPostingInd = :holdFromPostingInd")
    , @NamedQuery(name = "CashTransaction.findByExternalSource", query = "SELECT c FROM CashTransaction c WHERE c.externalSource = :externalSource")
    , @NamedQuery(name = "CashTransaction.findByContractCount", query = "SELECT c FROM CashTransaction c WHERE c.contractCount = :contractCount")
    , @NamedQuery(name = "CashTransaction.findByAmendmentCount", query = "SELECT c FROM CashTransaction c WHERE c.amendmentCount = :amendmentCount")
    , @NamedQuery(name = "CashTransaction.findByCancelCount", query = "SELECT c FROM CashTransaction c WHERE c.cancelCount = :cancelCount")
    , @NamedQuery(name = "CashTransaction.findByReinstatementCount", query = "SELECT c FROM CashTransaction c WHERE c.reinstatementCount = :reinstatementCount")
    , @NamedQuery(name = "CashTransaction.findByContractDisbursementCount", query = "SELECT c FROM CashTransaction c WHERE c.contractDisbursementCount = :contractDisbursementCount")
    , @NamedQuery(name = "CashTransaction.findByAmendmentDisbursementCount", query = "SELECT c FROM CashTransaction c WHERE c.amendmentDisbursementCount = :amendmentDisbursementCount")
    , @NamedQuery(name = "CashTransaction.findByCancelDisbursementCount", query = "SELECT c FROM CashTransaction c WHERE c.cancelDisbursementCount = :cancelDisbursementCount")
    , @NamedQuery(name = "CashTransaction.findByReinstatementDisbursementCount", query = "SELECT c FROM CashTransaction c WHERE c.reinstatementDisbursementCount = :reinstatementDisbursementCount")
    , @NamedQuery(name = "CashTransaction.findByTransferCount", query = "SELECT c FROM CashTransaction c WHERE c.transferCount = :transferCount")
    , @NamedQuery(name = "CashTransaction.findByClaimInspectionCount", query = "SELECT c FROM CashTransaction c WHERE c.claimInspectionCount = :claimInspectionCount")
    , @NamedQuery(name = "CashTransaction.findByPaymentAuthorizationCount", query = "SELECT c FROM CashTransaction c WHERE c.paymentAuthorizationCount = :paymentAuthorizationCount")
    , @NamedQuery(name = "CashTransaction.findByPaybackCount", query = "SELECT c FROM CashTransaction c WHERE c.paybackCount = :paybackCount")
    , @NamedQuery(name = "CashTransaction.findByRewardPointCount", query = "SELECT c FROM CashTransaction c WHERE c.rewardPointCount = :rewardPointCount")
    , @NamedQuery(name = "CashTransaction.findByBatchCorrectionCount", query = "SELECT c FROM CashTransaction c WHERE c.batchCorrectionCount = :batchCorrectionCount")
    , @NamedQuery(name = "CashTransaction.findByContractsInd", query = "SELECT c FROM CashTransaction c WHERE c.contractsInd = :contractsInd")
    , @NamedQuery(name = "CashTransaction.findByAmendmentsInd", query = "SELECT c FROM CashTransaction c WHERE c.amendmentsInd = :amendmentsInd")
    , @NamedQuery(name = "CashTransaction.findByCancelsInd", query = "SELECT c FROM CashTransaction c WHERE c.cancelsInd = :cancelsInd")
    , @NamedQuery(name = "CashTransaction.findByReinstatementInds", query = "SELECT c FROM CashTransaction c WHERE c.reinstatementInds = :reinstatementInds")
    , @NamedQuery(name = "CashTransaction.findByContractDisbursementsInd", query = "SELECT c FROM CashTransaction c WHERE c.contractDisbursementsInd = :contractDisbursementsInd")
    , @NamedQuery(name = "CashTransaction.findByAmendmentDisbursementsInd", query = "SELECT c FROM CashTransaction c WHERE c.amendmentDisbursementsInd = :amendmentDisbursementsInd")
    , @NamedQuery(name = "CashTransaction.findByCancelDisbursementsInd", query = "SELECT c FROM CashTransaction c WHERE c.cancelDisbursementsInd = :cancelDisbursementsInd")
    , @NamedQuery(name = "CashTransaction.findByReinstatementDisbursementsInd", query = "SELECT c FROM CashTransaction c WHERE c.reinstatementDisbursementsInd = :reinstatementDisbursementsInd")
    , @NamedQuery(name = "CashTransaction.findByTransfersInd", query = "SELECT c FROM CashTransaction c WHERE c.transfersInd = :transfersInd")
    , @NamedQuery(name = "CashTransaction.findByClaimInspectionsInd", query = "SELECT c FROM CashTransaction c WHERE c.claimInspectionsInd = :claimInspectionsInd")
    , @NamedQuery(name = "CashTransaction.findByPaymentAuthorizationsInd", query = "SELECT c FROM CashTransaction c WHERE c.paymentAuthorizationsInd = :paymentAuthorizationsInd")
    , @NamedQuery(name = "CashTransaction.findByPaybacksInd", query = "SELECT c FROM CashTransaction c WHERE c.paybacksInd = :paybacksInd")
    , @NamedQuery(name = "CashTransaction.findByRewardPointsInd", query = "SELECT c FROM CashTransaction c WHERE c.rewardPointsInd = :rewardPointsInd")
    , @NamedQuery(name = "CashTransaction.findByBatchCorrectionsInd", query = "SELECT c FROM CashTransaction c WHERE c.batchCorrectionsInd = :batchCorrectionsInd")
    , @NamedQuery(name = "CashTransaction.findByPayeeName", query = "SELECT c FROM CashTransaction c WHERE c.payeeName = :payeeName")
    , @NamedQuery(name = "CashTransaction.findByUpdateUserName", query = "SELECT c FROM CashTransaction c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CashTransaction.findByUpdateLast", query = "SELECT c FROM CashTransaction c WHERE c.updateLast = :updateLast")})
public class CashTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cashTransactionId")
    private Integer cashTransactionId;
    @Column(name = "paymentForTypeInd")
    private Integer paymentForTypeInd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "transactionNumber")
    private String transactionNumber;
    @Column(name = "sentDate")
    @Temporal(TemporalType.DATE)
    private Date sentDate;
    @Column(name = "batchIdFk")
    private Integer batchIdFk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "appliedAmount")
    private BigDecimal appliedAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "overUnder")
    private BigDecimal overUnder;
    @Column(name = "forStatementPostingHistoryIdFk")
    private Integer forStatementPostingHistoryIdFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "printedInd")
    private boolean printedInd;
    @Column(name = "printedDate")
    @Temporal(TemporalType.DATE)
    private Date printedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pendingExportUpdateInd")
    private boolean pendingExportUpdateInd;
    @Column(name = "pendingExportDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pendingExportDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exportedInd")
    private boolean exportedInd;
    @Column(name = "exportedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date exportedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clearedInd")
    private boolean clearedInd;
    @Column(name = "clearedDate")
    @Temporal(TemporalType.DATE)
    private Date clearedDate;
    @Column(name = "lastImportDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastImportDate;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "lastChange")
    private byte[] lastChange;
    @Basic(optional = false)
    @NotNull
    @Column(name = "systemCreatedInd")
    private boolean systemCreatedInd;
    @Size(max = 36)
    @Column(name = "thirdPartyPaymentProcessorGUID")
    private String thirdPartyPaymentProcessorGUID;
    @Size(max = 2147483647)
    @Column(name = "thirdPartyPaymentProcessorData")
    private String thirdPartyPaymentProcessorData;
    @Column(name = "thirdPartyPaymentProcessorStatusInd")
    private Integer thirdPartyPaymentProcessorStatusInd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "holdFromPostingInd")
    private boolean holdFromPostingInd;
    @Size(max = 50)
    @Column(name = "externalSource")
    private String externalSource;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractCount")
    private int contractCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amendmentCount")
    private int amendmentCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelCount")
    private int cancelCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatementCount")
    private int reinstatementCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contractDisbursementCount")
    private int contractDisbursementCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amendmentDisbursementCount")
    private int amendmentDisbursementCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cancelDisbursementCount")
    private int cancelDisbursementCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinstatementDisbursementCount")
    private int reinstatementDisbursementCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transferCount")
    private int transferCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimInspectionCount")
    private int claimInspectionCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paymentAuthorizationCount")
    private int paymentAuthorizationCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paybackCount")
    private int paybackCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rewardPointCount")
    private int rewardPointCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "batchCorrectionCount")
    private int batchCorrectionCount;
    @Column(name = "contractsInd")
    private Boolean contractsInd;
    @Column(name = "amendmentsInd")
    private Boolean amendmentsInd;
    @Column(name = "cancelsInd")
    private Boolean cancelsInd;
    @Column(name = "reinstatementInds")
    private Boolean reinstatementInds;
    @Column(name = "contractDisbursementsInd")
    private Boolean contractDisbursementsInd;
    @Column(name = "amendmentDisbursementsInd")
    private Boolean amendmentDisbursementsInd;
    @Column(name = "cancelDisbursementsInd")
    private Boolean cancelDisbursementsInd;
    @Column(name = "reinstatementDisbursementsInd")
    private Boolean reinstatementDisbursementsInd;
    @Column(name = "transfersInd")
    private Boolean transfersInd;
    @Column(name = "claimInspectionsInd")
    private Boolean claimInspectionsInd;
    @Column(name = "paymentAuthorizationsInd")
    private Boolean paymentAuthorizationsInd;
    @Column(name = "paybacksInd")
    private Boolean paybacksInd;
    @Column(name = "rewardPointsInd")
    private Boolean rewardPointsInd;
    @Column(name = "batchCorrectionsInd")
    private Boolean batchCorrectionsInd;
    @Size(max = 250)
    @Column(name = "payeeName")
    private String payeeName;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "addressIdFk", referencedColumnName = "addressId")
    @ManyToOne
    private Address addressIdFk;
    @OneToMany(mappedBy = "reissuedCashTransactionIdFk")
    private Collection<CashTransaction> cashTransactionCollection;
    @JoinColumn(name = "reissuedCashTransactionIdFk", referencedColumnName = "cashTransactionId")
    @ManyToOne
    private CashTransaction reissuedCashTransactionIdFk;
    @JoinColumn(name = "cashTransactionStatusInd", referencedColumnName = "cashTransactionStatusId")
    @ManyToOne(optional = false)
    private DtcashTransactionStatus cashTransactionStatusInd;
    @JoinColumn(name = "cashTransactionTypeInd", referencedColumnName = "cashTransactionTypeId")
    @ManyToOne(optional = false)
    private DtcashTransactionType cashTransactionTypeInd;
    @JoinColumn(name = "cashTransactionId", referencedColumnName = "ledgerId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ledger ledger;

    public CashTransaction() {
    }

    public CashTransaction(Integer cashTransactionId) {
        this.cashTransactionId = cashTransactionId;
    }

    public CashTransaction(Integer cashTransactionId, String transactionNumber, BigDecimal appliedAmount, BigDecimal overUnder, boolean printedInd, boolean pendingExportUpdateInd, boolean exportedInd, boolean clearedInd, byte[] lastChange, boolean systemCreatedInd, boolean holdFromPostingInd, int contractCount, int amendmentCount, int cancelCount, int reinstatementCount, int contractDisbursementCount, int amendmentDisbursementCount, int cancelDisbursementCount, int reinstatementDisbursementCount, int transferCount, int claimInspectionCount, int paymentAuthorizationCount, int paybackCount, int rewardPointCount, int batchCorrectionCount) {
        this.cashTransactionId = cashTransactionId;
        this.transactionNumber = transactionNumber;
        this.appliedAmount = appliedAmount;
        this.overUnder = overUnder;
        this.printedInd = printedInd;
        this.pendingExportUpdateInd = pendingExportUpdateInd;
        this.exportedInd = exportedInd;
        this.clearedInd = clearedInd;
        this.lastChange = lastChange;
        this.systemCreatedInd = systemCreatedInd;
        this.holdFromPostingInd = holdFromPostingInd;
        this.contractCount = contractCount;
        this.amendmentCount = amendmentCount;
        this.cancelCount = cancelCount;
        this.reinstatementCount = reinstatementCount;
        this.contractDisbursementCount = contractDisbursementCount;
        this.amendmentDisbursementCount = amendmentDisbursementCount;
        this.cancelDisbursementCount = cancelDisbursementCount;
        this.reinstatementDisbursementCount = reinstatementDisbursementCount;
        this.transferCount = transferCount;
        this.claimInspectionCount = claimInspectionCount;
        this.paymentAuthorizationCount = paymentAuthorizationCount;
        this.paybackCount = paybackCount;
        this.rewardPointCount = rewardPointCount;
        this.batchCorrectionCount = batchCorrectionCount;
    }

    public Integer getCashTransactionId() {
        return cashTransactionId;
    }

    public void setCashTransactionId(Integer cashTransactionId) {
        this.cashTransactionId = cashTransactionId;
    }

    public Integer getPaymentForTypeInd() {
        return paymentForTypeInd;
    }

    public void setPaymentForTypeInd(Integer paymentForTypeInd) {
        this.paymentForTypeInd = paymentForTypeInd;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Integer getBatchIdFk() {
        return batchIdFk;
    }

    public void setBatchIdFk(Integer batchIdFk) {
        this.batchIdFk = batchIdFk;
    }

    public BigDecimal getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(BigDecimal appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public BigDecimal getOverUnder() {
        return overUnder;
    }

    public void setOverUnder(BigDecimal overUnder) {
        this.overUnder = overUnder;
    }

    public Integer getForStatementPostingHistoryIdFk() {
        return forStatementPostingHistoryIdFk;
    }

    public void setForStatementPostingHistoryIdFk(Integer forStatementPostingHistoryIdFk) {
        this.forStatementPostingHistoryIdFk = forStatementPostingHistoryIdFk;
    }

    public boolean getPrintedInd() {
        return printedInd;
    }

    public void setPrintedInd(boolean printedInd) {
        this.printedInd = printedInd;
    }

    public Date getPrintedDate() {
        return printedDate;
    }

    public void setPrintedDate(Date printedDate) {
        this.printedDate = printedDate;
    }

    public boolean getPendingExportUpdateInd() {
        return pendingExportUpdateInd;
    }

    public void setPendingExportUpdateInd(boolean pendingExportUpdateInd) {
        this.pendingExportUpdateInd = pendingExportUpdateInd;
    }

    public Date getPendingExportDate() {
        return pendingExportDate;
    }

    public void setPendingExportDate(Date pendingExportDate) {
        this.pendingExportDate = pendingExportDate;
    }

    public boolean getExportedInd() {
        return exportedInd;
    }

    public void setExportedInd(boolean exportedInd) {
        this.exportedInd = exportedInd;
    }

    public Date getExportedDate() {
        return exportedDate;
    }

    public void setExportedDate(Date exportedDate) {
        this.exportedDate = exportedDate;
    }

    public boolean getClearedInd() {
        return clearedInd;
    }

    public void setClearedInd(boolean clearedInd) {
        this.clearedInd = clearedInd;
    }

    public Date getClearedDate() {
        return clearedDate;
    }

    public void setClearedDate(Date clearedDate) {
        this.clearedDate = clearedDate;
    }

    public Date getLastImportDate() {
        return lastImportDate;
    }

    public void setLastImportDate(Date lastImportDate) {
        this.lastImportDate = lastImportDate;
    }

    public byte[] getLastChange() {
        return lastChange;
    }

    public void setLastChange(byte[] lastChange) {
        this.lastChange = lastChange;
    }

    public boolean getSystemCreatedInd() {
        return systemCreatedInd;
    }

    public void setSystemCreatedInd(boolean systemCreatedInd) {
        this.systemCreatedInd = systemCreatedInd;
    }

    public String getThirdPartyPaymentProcessorGUID() {
        return thirdPartyPaymentProcessorGUID;
    }

    public void setThirdPartyPaymentProcessorGUID(String thirdPartyPaymentProcessorGUID) {
        this.thirdPartyPaymentProcessorGUID = thirdPartyPaymentProcessorGUID;
    }

    public String getThirdPartyPaymentProcessorData() {
        return thirdPartyPaymentProcessorData;
    }

    public void setThirdPartyPaymentProcessorData(String thirdPartyPaymentProcessorData) {
        this.thirdPartyPaymentProcessorData = thirdPartyPaymentProcessorData;
    }

    public Integer getThirdPartyPaymentProcessorStatusInd() {
        return thirdPartyPaymentProcessorStatusInd;
    }

    public void setThirdPartyPaymentProcessorStatusInd(Integer thirdPartyPaymentProcessorStatusInd) {
        this.thirdPartyPaymentProcessorStatusInd = thirdPartyPaymentProcessorStatusInd;
    }

    public boolean getHoldFromPostingInd() {
        return holdFromPostingInd;
    }

    public void setHoldFromPostingInd(boolean holdFromPostingInd) {
        this.holdFromPostingInd = holdFromPostingInd;
    }

    public String getExternalSource() {
        return externalSource;
    }

    public void setExternalSource(String externalSource) {
        this.externalSource = externalSource;
    }

    public int getContractCount() {
        return contractCount;
    }

    public void setContractCount(int contractCount) {
        this.contractCount = contractCount;
    }

    public int getAmendmentCount() {
        return amendmentCount;
    }

    public void setAmendmentCount(int amendmentCount) {
        this.amendmentCount = amendmentCount;
    }

    public int getCancelCount() {
        return cancelCount;
    }

    public void setCancelCount(int cancelCount) {
        this.cancelCount = cancelCount;
    }

    public int getReinstatementCount() {
        return reinstatementCount;
    }

    public void setReinstatementCount(int reinstatementCount) {
        this.reinstatementCount = reinstatementCount;
    }

    public int getContractDisbursementCount() {
        return contractDisbursementCount;
    }

    public void setContractDisbursementCount(int contractDisbursementCount) {
        this.contractDisbursementCount = contractDisbursementCount;
    }

    public int getAmendmentDisbursementCount() {
        return amendmentDisbursementCount;
    }

    public void setAmendmentDisbursementCount(int amendmentDisbursementCount) {
        this.amendmentDisbursementCount = amendmentDisbursementCount;
    }

    public int getCancelDisbursementCount() {
        return cancelDisbursementCount;
    }

    public void setCancelDisbursementCount(int cancelDisbursementCount) {
        this.cancelDisbursementCount = cancelDisbursementCount;
    }

    public int getReinstatementDisbursementCount() {
        return reinstatementDisbursementCount;
    }

    public void setReinstatementDisbursementCount(int reinstatementDisbursementCount) {
        this.reinstatementDisbursementCount = reinstatementDisbursementCount;
    }

    public int getTransferCount() {
        return transferCount;
    }

    public void setTransferCount(int transferCount) {
        this.transferCount = transferCount;
    }

    public int getClaimInspectionCount() {
        return claimInspectionCount;
    }

    public void setClaimInspectionCount(int claimInspectionCount) {
        this.claimInspectionCount = claimInspectionCount;
    }

    public int getPaymentAuthorizationCount() {
        return paymentAuthorizationCount;
    }

    public void setPaymentAuthorizationCount(int paymentAuthorizationCount) {
        this.paymentAuthorizationCount = paymentAuthorizationCount;
    }

    public int getPaybackCount() {
        return paybackCount;
    }

    public void setPaybackCount(int paybackCount) {
        this.paybackCount = paybackCount;
    }

    public int getRewardPointCount() {
        return rewardPointCount;
    }

    public void setRewardPointCount(int rewardPointCount) {
        this.rewardPointCount = rewardPointCount;
    }

    public int getBatchCorrectionCount() {
        return batchCorrectionCount;
    }

    public void setBatchCorrectionCount(int batchCorrectionCount) {
        this.batchCorrectionCount = batchCorrectionCount;
    }

    public Boolean getContractsInd() {
        return contractsInd;
    }

    public void setContractsInd(Boolean contractsInd) {
        this.contractsInd = contractsInd;
    }

    public Boolean getAmendmentsInd() {
        return amendmentsInd;
    }

    public void setAmendmentsInd(Boolean amendmentsInd) {
        this.amendmentsInd = amendmentsInd;
    }

    public Boolean getCancelsInd() {
        return cancelsInd;
    }

    public void setCancelsInd(Boolean cancelsInd) {
        this.cancelsInd = cancelsInd;
    }

    public Boolean getReinstatementInds() {
        return reinstatementInds;
    }

    public void setReinstatementInds(Boolean reinstatementInds) {
        this.reinstatementInds = reinstatementInds;
    }

    public Boolean getContractDisbursementsInd() {
        return contractDisbursementsInd;
    }

    public void setContractDisbursementsInd(Boolean contractDisbursementsInd) {
        this.contractDisbursementsInd = contractDisbursementsInd;
    }

    public Boolean getAmendmentDisbursementsInd() {
        return amendmentDisbursementsInd;
    }

    public void setAmendmentDisbursementsInd(Boolean amendmentDisbursementsInd) {
        this.amendmentDisbursementsInd = amendmentDisbursementsInd;
    }

    public Boolean getCancelDisbursementsInd() {
        return cancelDisbursementsInd;
    }

    public void setCancelDisbursementsInd(Boolean cancelDisbursementsInd) {
        this.cancelDisbursementsInd = cancelDisbursementsInd;
    }

    public Boolean getReinstatementDisbursementsInd() {
        return reinstatementDisbursementsInd;
    }

    public void setReinstatementDisbursementsInd(Boolean reinstatementDisbursementsInd) {
        this.reinstatementDisbursementsInd = reinstatementDisbursementsInd;
    }

    public Boolean getTransfersInd() {
        return transfersInd;
    }

    public void setTransfersInd(Boolean transfersInd) {
        this.transfersInd = transfersInd;
    }

    public Boolean getClaimInspectionsInd() {
        return claimInspectionsInd;
    }

    public void setClaimInspectionsInd(Boolean claimInspectionsInd) {
        this.claimInspectionsInd = claimInspectionsInd;
    }

    public Boolean getPaymentAuthorizationsInd() {
        return paymentAuthorizationsInd;
    }

    public void setPaymentAuthorizationsInd(Boolean paymentAuthorizationsInd) {
        this.paymentAuthorizationsInd = paymentAuthorizationsInd;
    }

    public Boolean getPaybacksInd() {
        return paybacksInd;
    }

    public void setPaybacksInd(Boolean paybacksInd) {
        this.paybacksInd = paybacksInd;
    }

    public Boolean getRewardPointsInd() {
        return rewardPointsInd;
    }

    public void setRewardPointsInd(Boolean rewardPointsInd) {
        this.rewardPointsInd = rewardPointsInd;
    }

    public Boolean getBatchCorrectionsInd() {
        return batchCorrectionsInd;
    }

    public void setBatchCorrectionsInd(Boolean batchCorrectionsInd) {
        this.batchCorrectionsInd = batchCorrectionsInd;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Address getAddressIdFk() {
        return addressIdFk;
    }

    public void setAddressIdFk(Address addressIdFk) {
        this.addressIdFk = addressIdFk;
    }

    @XmlTransient
    public Collection<CashTransaction> getCashTransactionCollection() {
        return cashTransactionCollection;
    }

    public void setCashTransactionCollection(Collection<CashTransaction> cashTransactionCollection) {
        this.cashTransactionCollection = cashTransactionCollection;
    }

    public CashTransaction getReissuedCashTransactionIdFk() {
        return reissuedCashTransactionIdFk;
    }

    public void setReissuedCashTransactionIdFk(CashTransaction reissuedCashTransactionIdFk) {
        this.reissuedCashTransactionIdFk = reissuedCashTransactionIdFk;
    }

    public DtcashTransactionStatus getCashTransactionStatusInd() {
        return cashTransactionStatusInd;
    }

    public void setCashTransactionStatusInd(DtcashTransactionStatus cashTransactionStatusInd) {
        this.cashTransactionStatusInd = cashTransactionStatusInd;
    }

    public DtcashTransactionType getCashTransactionTypeInd() {
        return cashTransactionTypeInd;
    }

    public void setCashTransactionTypeInd(DtcashTransactionType cashTransactionTypeInd) {
        this.cashTransactionTypeInd = cashTransactionTypeInd;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cashTransactionId != null ? cashTransactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashTransaction)) {
            return false;
        }
        CashTransaction other = (CashTransaction) object;
        if ((this.cashTransactionId == null && other.cashTransactionId != null) || (this.cashTransactionId != null && !this.cashTransactionId.equals(other.cashTransactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CashTransaction[ cashTransactionId=" + cashTransactionId + " ]";
    }
    
}
