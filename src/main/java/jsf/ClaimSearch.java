/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimSearch {
    
    private static final Logger LOGGER = LogManager.getLogger(RFSearch.class);

    public ClaimSearch() {
    }

    public ClaimSearch(Integer claimId, String claimNumber, String userName, String vinFull, String contractNo, Date effectiveDate, Date contractExpirationDate, String customer, String phoneNumber, String dealerName, String dealerNumber, String productTypeName) {
        this.claimId = claimId;
        this.claimNumber = claimNumber;
        this.userName = userName;
        this.vinFull = vinFull;
        this.contractNo = contractNo;
        this.effectiveDate = effectiveDate;
        this.contractExpirationDate = contractExpirationDate;
        this.customer = customer;
        this.phoneNumber = phoneNumber;
        this.dealerName = dealerName;
        this.dealerNumber = dealerNumber;
        this.productTypeName = productTypeName;
    }

    
    @Id
    @Column(name = "claimId", table = "Claim")
    Integer claimId;
    @Column(name = "claimNumber", table = "Claim")
    String claimNumber;
    @Column(name = "userName", table = "UserMember")
    private String userName;
    @Column(name = "vinFull", table = "Contracts")
    private String vinFull;
    @Column(name = "contractNo", table = "Contracts")
    private String contractNo;
     @Column(name = "effectiveDate", table = "Contracts")
    private Date effectiveDate;
    @Column(name = "contractExpirationDate", table = "Contracts")
    private Date contractExpirationDate;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    private String customer;
    @Column(name = "phoneNumber", table = "Address")
    private String phoneNumber;
    String dealerName;
    @Column(name = "dealerNumber", table = "Dealer")
    private String dealerNumber;
    @Column(name = "productTypeName", table = "ProductType")
    private String productTypeName;

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }
    
    

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
    
    

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVinFull() {
        return vinFull;
    }

    public void setVinFull(String vinFull) {
        this.vinFull = vinFull;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getContractExpirationDate() {
        return contractExpirationDate;
    }

    public void setContractExpirationDate(Date contractExpirationDate) {
        this.contractExpirationDate = contractExpirationDate;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }
    
    
    
    
}
