/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jiepi
 */
public class PBDForClaim {

    public PBDForClaim() {
    }

    public PBDForClaim(Integer dealerId, Integer anc, Integer bun, Integer rht, Integer threeFORone, Integer pdr, Integer lux, Integer etch, Integer ewt, Integer key, Integer ppm, Integer gap, Integer vsc, Integer total, BigDecimal avgAmount, BigDecimal totalAmount, Date startDate, Date endDate, String dealer) {
        this.dealerId = dealerId;
        this.anc = anc;
        this.bun = bun;
        this.rht = rht;
        this.threeFORone = threeFORone;
        this.pdr = pdr;
        this.lux = lux;
        this.etch = etch;
        this.ewt = ewt;
        this.key = key;
        this.ppm = ppm;
        this.gap = gap;
        this.vsc = vsc;
        this.total = total;
        this.avgAmount = avgAmount;
        this.totalAmount = totalAmount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.dealer = dealer;
    }

    

    private Integer dealerId;
    private Integer anc;
    private Integer bun;
    private Integer rht;
    private Integer threeFORone;
    private Integer pdr;
    private Integer lux;
    private Integer etch;
    private Integer ewt;
    private Integer key;
    private Integer ppm;
    private Integer gap;
    private Integer vsc;
    private Integer total;
    private BigDecimal avgAmount;
    private BigDecimal totalAmount;
    private Date startDate;
    private Date endDate;
    private String dealer;

    public Integer getVsc() {
        return vsc;
    }

    public void setVsc(Integer vsc) {
        this.vsc = vsc;
    }
    
    

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getAnc() {
        return anc;
    }

    public void setAnc(Integer anc) {
        this.anc = anc;
    }

    public Integer getBun() {
        return bun;
    }

    public void setBun(Integer bun) {
        this.bun = bun;
    }

    public Integer getRht() {
        return rht;
    }

    public void setRht(Integer rht) {
        this.rht = rht;
    }

    public Integer getThreeFORone() {
        return threeFORone;
    }

    public void setThreeFORone(Integer threeFORone) {
        this.threeFORone = threeFORone;
    }

    public Integer getPdr() {
        return pdr;
    }

    public void setPdr(Integer pdr) {
        this.pdr = pdr;
    }

    public Integer getLux() {
        return lux;
    }

    public void setLux(Integer lux) {
        this.lux = lux;
    }

    public Integer getEtch() {
        return etch;
    }

    public void setEtch(Integer etch) {
        this.etch = etch;
    }

    public Integer getEwt() {
        return ewt;
    }

    public void setEwt(Integer ewt) {
        this.ewt = ewt;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public Integer getPpm() {
        return ppm;
    }

    public void setPpm(Integer ppm) {
        this.ppm = ppm;
    }

    public Integer getGap() {
        return gap;
    }

    public void setGap(Integer gap) {
        this.gap = gap;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public BigDecimal getAvgAmount() {
        return avgAmount;
    }

    public void setAvgAmount(BigDecimal avgAmount) {
        this.avgAmount = avgAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public void reset() {
        dealerId = null;
        anc = null;
        bun = null;
        rht = null;
        threeFORone = null;
        pdr = null;
        lux = null;
        etch = null;
        ewt = null;
        key = null;
        ppm = null;
        gap = null;
        avgAmount = null;
        totalAmount = null;
        startDate = null;
        endDate = null;
        dealer = null;

    }

}
