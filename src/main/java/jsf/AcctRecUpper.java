/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class AcctRecUpper {

    private static final Logger LOGGER = LogManager.getLogger(AcctRecUpper.class);

    public AcctRecUpper() {
    }

    public AcctRecUpper(String accountName, String adminCompany, String transactionNumber, String transType, Date sentDate, Date ReceivedDate, BigDecimal transactionAmount, BigDecimal prevBalance, BigDecimal newContracts, BigDecimal amendments, BigDecimal cancellations, BigDecimal reinstatments, BigDecimal transfers, BigDecimal newCommissions, BigDecimal amendmentCommissions, BigDecimal cancelledCommissions, BigDecimal reinstatedCommissions, BigDecimal claimPayments, BigDecimal claimInspections, BigDecimal convertedRewardPoints, BigDecimal batchCorrections, BigDecimal currentCharges, BigDecimal currentBalance, Integer currAcctId) {
        this.accountName = accountName;
        this.adminCompany = adminCompany;
        this.transactionNumber = transactionNumber;
        this.transType = transType;
        this.sentDate = sentDate;
        this.ReceivedDate = ReceivedDate;
        this.transactionAmount = transactionAmount;
        this.prevBalance = prevBalance;
        this.newContracts = newContracts;
        this.amendments = amendments;
        this.cancellations = cancellations;
        this.reinstatments = reinstatments;
        this.transfers = transfers;
        this.newCommissions = newCommissions;
        this.amendmentCommissions = amendmentCommissions;
        this.cancelledCommissions = cancelledCommissions;
        this.reinstatedCommissions = reinstatedCommissions;
        this.claimPayments = claimPayments;
        this.claimInspections = claimInspections;
        this.convertedRewardPoints = convertedRewardPoints;
        this.batchCorrections = batchCorrections;
        this.currentCharges = currentCharges;
        this.currentBalance = currentBalance;
        this.currAcctId = currAcctId;
    }

   

    

   

    private String accountName;
    private String adminCompany;
    private String transactionNumber;
    private String transType;
    private Date sentDate;
    private Date ReceivedDate;
    private BigDecimal transactionAmount;
    private BigDecimal prevBalance;
    private BigDecimal newContracts;
    private BigDecimal amendments;
    private BigDecimal cancellations;
    private BigDecimal reinstatments;
    private BigDecimal transfers;
    private BigDecimal newCommissions;
    private BigDecimal amendmentCommissions;
    private BigDecimal cancelledCommissions;
    private BigDecimal reinstatedCommissions;
    private BigDecimal claimPayments;
    private BigDecimal claimInspections;
    private BigDecimal convertedRewardPoints;
    private BigDecimal batchCorrections;
    private BigDecimal currentCharges;
    private BigDecimal currentBalance;
    private Integer currAcctId;

    public Integer getCurrAcctId() {
        return currAcctId;
    }

    public void setCurrAcctId(Integer currAcctId) {
        this.currAcctId = currAcctId;
    }
    
    

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getReceivedDate() {
        return ReceivedDate;
    }

    public void setReceivedDate(Date ReceivedDate) {
        this.ReceivedDate = ReceivedDate;
    }

    
    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getPrevBalance() {
        return prevBalance;
    }

    public void setPrevBalance(BigDecimal prevBalance) {
        this.prevBalance = prevBalance;
    }

    public BigDecimal getNewContracts() {
        return newContracts;
    }

    public void setNewContracts(BigDecimal newContracts) {
        this.newContracts = newContracts;
    }

    public BigDecimal getAmendments() {
        return amendments;
    }

    public void setAmendments(BigDecimal amendments) {
        this.amendments = amendments;
    }

    public BigDecimal getCancellations() {
        return cancellations;
    }

    public void setCancellations(BigDecimal cancellations) {
        this.cancellations = cancellations;
    }

    public BigDecimal getReinstatments() {
        return reinstatments;
    }

    public void setReinstatments(BigDecimal reinstatments) {
        this.reinstatments = reinstatments;
    }

    public BigDecimal getTransfers() {
        return transfers;
    }

    public void setTransfers(BigDecimal transfers) {
        this.transfers = transfers;
    }

    public BigDecimal getNewCommissions() {
        return newCommissions;
    }

    public void setNewCommissions(BigDecimal newCommissions) {
        this.newCommissions = newCommissions;
    }

    public BigDecimal getAmendmentCommissions() {
        return amendmentCommissions;
    }

    public void setAmendmentCommissions(BigDecimal amendmentCommissions) {
        this.amendmentCommissions = amendmentCommissions;
    }

    public BigDecimal getCancelledCommissions() {
        return cancelledCommissions;
    }

    public void setCancelledCommissions(BigDecimal cancelledCommissions) {
        this.cancelledCommissions = cancelledCommissions;
    }

    public BigDecimal getReinstatedCommissions() {
        return reinstatedCommissions;
    }

    public void setReinstatedCommissions(BigDecimal reinstatedCommissions) {
        this.reinstatedCommissions = reinstatedCommissions;
    }

    public BigDecimal getClaimPayments() {
        return claimPayments;
    }

    public void setClaimPayments(BigDecimal claimPayments) {
        this.claimPayments = claimPayments;
    }

    public BigDecimal getClaimInspections() {
        return claimInspections;
    }

    public void setClaimInspections(BigDecimal claimInspections) {
        this.claimInspections = claimInspections;
    }

    public BigDecimal getConvertedRewardPoints() {
        return convertedRewardPoints;
    }

    public void setConvertedRewardPoints(BigDecimal convertedRewardPoints) {
        this.convertedRewardPoints = convertedRewardPoints;
    }

    public BigDecimal getBatchCorrections() {
        return batchCorrections;
    }

    public void setBatchCorrections(BigDecimal batchCorrections) {
        this.batchCorrections = batchCorrections;
    }

    public BigDecimal getCurrentCharges() {
        return currentCharges;
    }

    public void setCurrentCharges(BigDecimal currentCharges) {
        this.currentCharges = currentCharges;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public void reset() {
        BigDecimal BIGZERO = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
        accountName = "";
        adminCompany = "";
        transactionNumber = "";
        transType = "";
        sentDate = null;
        ReceivedDate = null;
        transactionAmount = BIGZERO;
        prevBalance = BIGZERO;
        newContracts = BIGZERO;
        amendments = BIGZERO;
        cancellations = BIGZERO;
        reinstatments = BIGZERO;
        transfers = BIGZERO;
        newCommissions = BIGZERO;
        amendmentCommissions = BIGZERO;
        cancelledCommissions = BIGZERO;
        reinstatedCommissions = BIGZERO;
        claimPayments = BIGZERO;
        claimInspections = BIGZERO;
        convertedRewardPoints = BIGZERO;
        batchCorrections = BIGZERO;
        currentCharges = BIGZERO;
        currentBalance = BIGZERO;
        currAcctId = null;
    }

}
