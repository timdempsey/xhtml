/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ClaimFile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaimFile.findAll", query = "SELECT c FROM ClaimFile c")
    , @NamedQuery(name = "ClaimFile.findByClaimFileId", query = "SELECT c FROM ClaimFile c WHERE c.claimFileId = :claimFileId")
    , @NamedQuery(name = "ClaimFile.findByClaimFileTypeInd", query = "SELECT c FROM ClaimFile c WHERE c.claimFileTypeInd = :claimFileTypeInd")
    , @NamedQuery(name = "ClaimFile.findByUpdateUserName", query = "SELECT c FROM ClaimFile c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "ClaimFile.findByUpdateLast", query = "SELECT c FROM ClaimFile c WHERE c.updateLast = :updateLast")})
public class ClaimFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimFileId")
    private Integer claimFileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "claimFileTypeInd")
    private int claimFileTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "accountKeeperIdFk", referencedColumnName = "accountKeeperId")
    @ManyToOne
    private AccountKeeper accountKeeperIdFk;
    @JoinColumn(name = "claimFileId", referencedColumnName = "fileId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private CfFile cfFile;
    @JoinColumn(name = "claimIdFk", referencedColumnName = "claimId")
    @ManyToOne
    private Claim claimIdFk;

    public ClaimFile() {
    }

    public ClaimFile(Integer claimFileId) {
        this.claimFileId = claimFileId;
    }

    public ClaimFile(Integer claimFileId, int claimFileTypeInd) {
        this.claimFileId = claimFileId;
        this.claimFileTypeInd = claimFileTypeInd;
    }

    public Integer getClaimFileId() {
        return claimFileId;
    }

    public void setClaimFileId(Integer claimFileId) {
        this.claimFileId = claimFileId;
    }

    public int getClaimFileTypeInd() {
        return claimFileTypeInd;
    }

    public void setClaimFileTypeInd(int claimFileTypeInd) {
        this.claimFileTypeInd = claimFileTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public AccountKeeper getAccountKeeperIdFk() {
        return accountKeeperIdFk;
    }

    public void setAccountKeeperIdFk(AccountKeeper accountKeeperIdFk) {
        this.accountKeeperIdFk = accountKeeperIdFk;
    }

    public CfFile getCfFile() {
        return cfFile;
    }

    public void setCfFile(CfFile cfFile) {
        this.cfFile = cfFile;
    }

    public Claim getClaimIdFk() {
        return claimIdFk;
    }

    public void setClaimIdFk(Claim claimIdFk) {
        this.claimIdFk = claimIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claimFileId != null ? claimFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaimFile)) {
            return false;
        }
        ClaimFile other = (ClaimFile) object;
        if ((this.claimFileId == null && other.claimFileId != null) || (this.claimFileId != null && !this.claimFileId.equals(other.claimFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ClaimFile[ claimFileId=" + claimFileId + " ]";
    }
    
}
