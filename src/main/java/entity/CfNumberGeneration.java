/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfNumberGeneration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfNumberGeneration.findAll", query = "SELECT c FROM CfNumberGeneration c")
    , @NamedQuery(name = "CfNumberGeneration.findByNumberGenerationId", query = "SELECT c FROM CfNumberGeneration c WHERE c.numberGenerationId = :numberGenerationId")
    , @NamedQuery(name = "CfNumberGeneration.findBySuffix", query = "SELECT c FROM CfNumberGeneration c WHERE c.suffix = :suffix")
    , @NamedQuery(name = "CfNumberGeneration.findByNumberGenerationMethodInd", query = "SELECT c FROM CfNumberGeneration c WHERE c.numberGenerationMethodInd = :numberGenerationMethodInd")
    , @NamedQuery(name = "CfNumberGeneration.findByExpression", query = "SELECT c FROM CfNumberGeneration c WHERE c.expression = :expression")
    , @NamedQuery(name = "CfNumberGeneration.findByAlternateExpression", query = "SELECT c FROM CfNumberGeneration c WHERE c.alternateExpression = :alternateExpression")
    , @NamedQuery(name = "CfNumberGeneration.findByCurrentSequence", query = "SELECT c FROM CfNumberGeneration c WHERE c.currentSequence = :currentSequence")
    , @NamedQuery(name = "CfNumberGeneration.findByPriority", query = "SELECT c FROM CfNumberGeneration c WHERE c.priority = :priority")
    , @NamedQuery(name = "CfNumberGeneration.findByNumberGenerationTypeInd", query = "SELECT c FROM CfNumberGeneration c WHERE c.numberGenerationTypeInd = :numberGenerationTypeInd")
    , @NamedQuery(name = "CfNumberGeneration.findByUpdateUserName", query = "SELECT c FROM CfNumberGeneration c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfNumberGeneration.findByUpdateLast", query = "SELECT c FROM CfNumberGeneration c WHERE c.updateLast = :updateLast")})
public class CfNumberGeneration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberGenerationId")
    private Integer numberGenerationId;
    @Size(max = 10)
    @Column(name = "suffix")
    private String suffix;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberGenerationMethodInd")
    private int numberGenerationMethodInd;
    @Size(max = 50)
    @Column(name = "expression")
    private String expression;
    @Size(max = 50)
    @Column(name = "alternateExpression")
    private String alternateExpression;
    @Column(name = "currentSequence")
    private Integer currentSequence;
    @Basic(optional = false)
    @NotNull
    @Column(name = "priority")
    private int priority;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberGenerationTypeInd")
    private int numberGenerationTypeInd;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "criterionGroupIdFk", referencedColumnName = "criterionGroupId")
    @ManyToOne
    private CfCriterionGroup criterionGroupIdFk;
    @JoinColumn(name = "numberGenerationId", referencedColumnName = "rateBasedRuleGroupId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private RateBasedRuleGroup rateBasedRuleGroup;

    public CfNumberGeneration() {
    }

    public CfNumberGeneration(Integer numberGenerationId) {
        this.numberGenerationId = numberGenerationId;
    }

    public CfNumberGeneration(Integer numberGenerationId, int numberGenerationMethodInd, int priority, int numberGenerationTypeInd) {
        this.numberGenerationId = numberGenerationId;
        this.numberGenerationMethodInd = numberGenerationMethodInd;
        this.priority = priority;
        this.numberGenerationTypeInd = numberGenerationTypeInd;
    }

    public Integer getNumberGenerationId() {
        return numberGenerationId;
    }

    public void setNumberGenerationId(Integer numberGenerationId) {
        this.numberGenerationId = numberGenerationId;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public int getNumberGenerationMethodInd() {
        return numberGenerationMethodInd;
    }

    public void setNumberGenerationMethodInd(int numberGenerationMethodInd) {
        this.numberGenerationMethodInd = numberGenerationMethodInd;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getAlternateExpression() {
        return alternateExpression;
    }

    public void setAlternateExpression(String alternateExpression) {
        this.alternateExpression = alternateExpression;
    }

    public Integer getCurrentSequence() {
        return currentSequence;
    }

    public void setCurrentSequence(Integer currentSequence) {
        this.currentSequence = currentSequence;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getNumberGenerationTypeInd() {
        return numberGenerationTypeInd;
    }

    public void setNumberGenerationTypeInd(int numberGenerationTypeInd) {
        this.numberGenerationTypeInd = numberGenerationTypeInd;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public CfCriterionGroup getCriterionGroupIdFk() {
        return criterionGroupIdFk;
    }

    public void setCriterionGroupIdFk(CfCriterionGroup criterionGroupIdFk) {
        this.criterionGroupIdFk = criterionGroupIdFk;
    }

    public RateBasedRuleGroup getRateBasedRuleGroup() {
        return rateBasedRuleGroup;
    }

    public void setRateBasedRuleGroup(RateBasedRuleGroup rateBasedRuleGroup) {
        this.rateBasedRuleGroup = rateBasedRuleGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numberGenerationId != null ? numberGenerationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfNumberGeneration)) {
            return false;
        }
        CfNumberGeneration other = (CfNumberGeneration) object;
        if ((this.numberGenerationId == null && other.numberGenerationId != null) || (this.numberGenerationId != null && !this.numberGenerationId.equals(other.numberGenerationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfNumberGeneration[ numberGenerationId=" + numberGenerationId + " ]";
    }
    
}
