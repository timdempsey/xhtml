/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PPMEvent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PPMEvent.findAll", query = "SELECT p FROM PPMEvent p")
    , @NamedQuery(name = "PPMEvent.findByPPMEventId", query = "SELECT p FROM PPMEvent p WHERE p.pPMEventId = :pPMEventId")
    , @NamedQuery(name = "PPMEvent.findByDescription", query = "SELECT p FROM PPMEvent p WHERE p.description = :description")
    , @NamedQuery(name = "PPMEvent.findByIsAndCondition", query = "SELECT p FROM PPMEvent p WHERE p.isAndCondition = :isAndCondition")
    , @NamedQuery(name = "PPMEvent.findByUpdateUserName", query = "SELECT p FROM PPMEvent p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PPMEvent.findByUpdateLast", query = "SELECT p FROM PPMEvent p WHERE p.updateLast = :updateLast")})
public class PPMEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PPMEventId")
    private Integer pPMEventId;
    @Size(max = 200)
    @Column(name = "Description")
    private String description;
    @Column(name = "IsAndCondition")
    private Boolean isAndCondition;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "pPMEventIdFk")
    private Collection<PPMEventRate> pPMEventRateCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pPMEvent")
    private PPMRecurringEvent pPMRecurringEvent;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pPMEvent")
    private PPMSingleEvent pPMSingleEvent;
    @JoinColumn(name = "PPMScheduleIdFk", referencedColumnName = "PPMScheduleId")
    @ManyToOne
    private PPMSchedule pPMScheduleIdFk;

    public PPMEvent() {
    }

    public PPMEvent(Integer pPMEventId) {
        this.pPMEventId = pPMEventId;
    }

    public Integer getPPMEventId() {
        return pPMEventId;
    }

    public void setPPMEventId(Integer pPMEventId) {
        this.pPMEventId = pPMEventId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsAndCondition() {
        return isAndCondition;
    }

    public void setIsAndCondition(Boolean isAndCondition) {
        this.isAndCondition = isAndCondition;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PPMEventRate> getPPMEventRateCollection() {
        return pPMEventRateCollection;
    }

    public void setPPMEventRateCollection(Collection<PPMEventRate> pPMEventRateCollection) {
        this.pPMEventRateCollection = pPMEventRateCollection;
    }

    public PPMRecurringEvent getPPMRecurringEvent() {
        return pPMRecurringEvent;
    }

    public void setPPMRecurringEvent(PPMRecurringEvent pPMRecurringEvent) {
        this.pPMRecurringEvent = pPMRecurringEvent;
    }

    public PPMSingleEvent getPPMSingleEvent() {
        return pPMSingleEvent;
    }

    public void setPPMSingleEvent(PPMSingleEvent pPMSingleEvent) {
        this.pPMSingleEvent = pPMSingleEvent;
    }

    public PPMSchedule getPPMScheduleIdFk() {
        return pPMScheduleIdFk;
    }

    public void setPPMScheduleIdFk(PPMSchedule pPMScheduleIdFk) {
        this.pPMScheduleIdFk = pPMScheduleIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pPMEventId != null ? pPMEventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PPMEvent)) {
            return false;
        }
        PPMEvent other = (PPMEvent) object;
        if ((this.pPMEventId == null && other.pPMEventId != null) || (this.pPMEventId != null && !this.pPMEventId.equals(other.pPMEventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PPMEvent[ pPMEventId=" + pPMEventId + " ]";
    }
    
}
