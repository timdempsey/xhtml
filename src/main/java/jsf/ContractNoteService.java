/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Claim;
import entity.Contracts;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Jiepi
 */
@Named(value = "contractNoteService")
@ApplicationScoped
public class ContractNoteService {

    private static final Logger LOGGER = LogManager.getLogger(ContractNoteService.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    /**
     * Creates a new instance of ContractNoteService
     */
    public ContractNoteService() {
    }

    private String claimNumber;
    private Claim claim;
    private Contracts contract;

    private ClaimNotes claimNotes;
    private String note;
    
    private List<ContractNote> contractNotes;

    public List<ContractNote> getContractNotes() {
        return contractNotes;
    }

    public void setContractNotes(List<ContractNote> contractNotes) {
        this.contractNotes = contractNotes;
    }

    
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public ClaimNotes getClaimNotes() {
        return claimNotes;
    }

    public void setClaimNotes(ClaimNotes claimNotes) {
        this.claimNotes = claimNotes;
    }

    public void chooseClaimNotes(ActionEvent event) {
        claimNumber = (String) event.getComponent().getAttributes().get("claimNumber");
        LOGGER.info("chooseClaimNotes=" + claimNumber);

        claim = em.createNamedQuery("Claim.findByClaimNumber", Claim.class).setParameter("claimNumber", claimNumber).getSingleResult();

        //claimNotes = createClaimNotesView(claimNumber);

        Map<String, Object> options = new HashMap<>();
        options.put("resizable", true);
        options.put("draggable", true);

        options.put("height", "1000");
        options.put("width", "2000");
        options.put("modal", true);

        RequestContext.getCurrentInstance().openDialog("claimNote", options, null);
    }

    public List<ContractNote> createContractNotesView(String claimNumber) {
        LOGGER.info("in createContractNotesView, claimNumber=" + claimNumber);

        claim = claimMisc.getClaimByNumber(claimNumber);
        
        contract = claimMisc.getContractsByClaimNum(claimNumber);
        
        LOGGER.info("contractNo=" + contract.getContractNo());
        contractNotes =  claimMisc.getContractNotesByContractNo(contract.getContractNo());

        return contractNotes;

    }
    
    public List<ContractNote> createContractNotesByContractNo(String contractNo) {
        LOGGER.info("in createContractNotesByContractNo claimNumber=" + contractNo);
        
        contract = claimMisc.getContractsByNo(contractNo);
        
        contractNotes =  claimMisc.getContractNotesByContractNo(contract.getContractNo());

        return contractNotes;

    }
    
    
/*
    public void dialogClosed(SelectEvent event) {
        LOGGER.info("save dialog closed=" + claimNumber);
        claimNotes = createClaimNotesView(claimNumber);
    }
*/
}
