/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ViewScoped;



/**
 *
 * @author Jiepi
 */
@Named
@RequestScoped
public class SelectionClaim implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(SelectionClaim.class);
    

    /**
     * Creates a new instance of SelectionClaim
     */
    public SelectionClaim() {
    }

    private ArrayList<ClaimFirstLine> claimFirstLines = new ArrayList<>();
    //private ClaimFirstLine selectedClaimFirstLine;

    
    @PostConstruct
    public void init() {
        LOGGER.info("init from selectionClaim");
        claimFirstLines = new ArrayList<>();
        //selectedClaimFirstLine = new ClaimFirstLine();
    }
    

    public List<ClaimFirstLine> getClaimFirstLines() {
        LOGGER.info("getter from selectionClaim");
        return claimFirstLines;
    }

    public void setClaimFirstLines(ArrayList<ClaimFirstLine> claimFirstLines) {
        LOGGER.info("setter from selectionClaim");
        this.claimFirstLines = claimFirstLines;
    }

    /*
    public ClaimFirstLine getSelectedClaimFirstLine() {
        return selectedClaimFirstLine;
    }

    public void setSelectedClaimFirstLine(ClaimFirstLine selectedClaimFirstLine) {
        this.selectedClaimFirstLine = selectedClaimFirstLine;
    }
     */
    public void addClaimFirstLine(ClaimFirstLine cfl) {

        LOGGER.info("inside addClaimFIrstLine " + cfl.getClaimNumber());
        try {
            //claimFirstLines = new ArrayList<>();
            claimFirstLines.add(cfl);
            
            if (claimFirstLines.isEmpty()) {
                LOGGER.info("isempty");
                claimFirstLines = new ArrayList<>();
            } else {
                LOGGER.info("not empty, and size=" + claimFirstLines.size());
                //claimFirstLines.add(cfl);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.info("exceptioins..." + ex.getMessage());
            // ex.printStackTrace();
        }
    }
}
