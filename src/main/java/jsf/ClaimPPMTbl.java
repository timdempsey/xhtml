/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimPPMTbl implements Serializable{
    private static final Logger LOGGER = LogManager.getLogger(ClaimPPMTbl.class);

    public ClaimPPMTbl() {
    }

    public ClaimPPMTbl(Integer contractPPMDetailId, String contractNo, Integer contractId, String description, String lowerDate, String upperDate, String andor, Integer lowerMile, Integer upperMile, BigDecimal cost, String onClaim, String onRO) {
        this.contractPPMDetailId = contractPPMDetailId;
        this.contractNo = contractNo;
        this.contractId = contractId;
        this.description = description;
        this.lowerDate = lowerDate;
        this.upperDate = upperDate;
        this.andor = andor;
        this.lowerMile = lowerMile;
        this.upperMile = upperMile;
        this.cost = cost;
        this.onClaim = onClaim;
        this.onRO = onRO;
    }
    
    
    
    @Id
    @Column(name = "contractPPMDetailId", table = "ContractPPMDetail")
    Integer contractPPMDetailId;
    @Column(name = "contractNo", table = "Contracts")
    String contractNo;
    @Column(name = "contractId", table = "Contracts")
    Integer contractId;
    @Column(name = "description", table = "ContractPPMDetail")
    String description;
    @Column(name = "lowerDate", table = "ContractPPMDetail")
    String lowerDate;
    @Column(name = "uppderDate", table = "ContractPPMDetail")
    String upperDate;
    String andor;
    @Column(name = "lowerMile", table = "ContractPPMDetail")
    Integer lowerMile;
    @Column(name = "uppderMile", table = "ContractPPMDetail")
    Integer upperMile;
    @Column(name = "cost", table = "ContractPPMDetail")
    BigDecimal cost;
    @Column(name = "claimNumber", table = "Claim")
    String onClaim;
    @Column(name = "repairOrderNumber", table = "Claim")
    String onRO;

    public Integer getContractPPMDetailId() {
        return contractPPMDetailId;
    }

    public void setContractPPMDetailId(Integer contractPPMDetailId) {
        this.contractPPMDetailId = contractPPMDetailId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLowerDate() {
        return lowerDate;
    }

    public void setLowerDate(String lowerDate) {
        this.lowerDate = lowerDate;
    }

    public String getUpperDate() {
        return upperDate;
    }

    public void setUpperDate(String upperDate) {
        this.upperDate = upperDate;
    }

    public String getAndor() {
        return andor;
    }

    public void setAndor(String andor) {
        this.andor = andor;
    }

    public Integer getLowerMile() {
        return lowerMile;
    }

    public void setLowerMile(Integer lowerMile) {
        this.lowerMile = lowerMile;
    }

    public Integer getUpperMile() {
        return upperMile;
    }

    public void setUpperMile(Integer upperMile) {
        this.upperMile = upperMile;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getOnClaim() {
        return onClaim;
    }

    public void setOnClaim(String onClaim) {
        this.onClaim = onClaim;
    }

    public String getOnRO() {
        return onRO;
    }

    public void setOnRO(String onRO) {
        this.onRO = onRO;
    }
    
    
    
}


