/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "ReinsuranceReinsurer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReinsuranceReinsurer.findAll", query = "SELECT r FROM ReinsuranceReinsurer r")
    , @NamedQuery(name = "ReinsuranceReinsurer.findByReinsuranceReinsurerId", query = "SELECT r FROM ReinsuranceReinsurer r WHERE r.reinsuranceReinsurerId = :reinsuranceReinsurerId")
    , @NamedQuery(name = "ReinsuranceReinsurer.findByUpdateUserName", query = "SELECT r FROM ReinsuranceReinsurer r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "ReinsuranceReinsurer.findByUpdateLast", query = "SELECT r FROM ReinsuranceReinsurer r WHERE r.updateLast = :updateLast")})
public class ReinsuranceReinsurer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reinsuranceReinsurerId")
    private Integer reinsuranceReinsurerId;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "reinsurerIdFk", referencedColumnName = "insurerId")
    @ManyToOne
    private Insurer reinsurerIdFk;
    @JoinColumn(name = "reinsuranceRuleIdFk", referencedColumnName = "reinsuranceRuleId")
    @ManyToOne
    private ReinsuranceRule reinsuranceRuleIdFk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reinsuranceReinsurerIdFk")
    private Collection<ReinsuranceReinsurerDetail> reinsuranceReinsurerDetailCollection;

    public ReinsuranceReinsurer() {
    }

    public ReinsuranceReinsurer(Integer reinsuranceReinsurerId) {
        this.reinsuranceReinsurerId = reinsuranceReinsurerId;
    }

    public Integer getReinsuranceReinsurerId() {
        return reinsuranceReinsurerId;
    }

    public void setReinsuranceReinsurerId(Integer reinsuranceReinsurerId) {
        this.reinsuranceReinsurerId = reinsuranceReinsurerId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Insurer getReinsurerIdFk() {
        return reinsurerIdFk;
    }

    public void setReinsurerIdFk(Insurer reinsurerIdFk) {
        this.reinsurerIdFk = reinsurerIdFk;
    }

    public ReinsuranceRule getReinsuranceRuleIdFk() {
        return reinsuranceRuleIdFk;
    }

    public void setReinsuranceRuleIdFk(ReinsuranceRule reinsuranceRuleIdFk) {
        this.reinsuranceRuleIdFk = reinsuranceRuleIdFk;
    }

    @XmlTransient
    public Collection<ReinsuranceReinsurerDetail> getReinsuranceReinsurerDetailCollection() {
        return reinsuranceReinsurerDetailCollection;
    }

    public void setReinsuranceReinsurerDetailCollection(Collection<ReinsuranceReinsurerDetail> reinsuranceReinsurerDetailCollection) {
        this.reinsuranceReinsurerDetailCollection = reinsuranceReinsurerDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reinsuranceReinsurerId != null ? reinsuranceReinsurerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReinsuranceReinsurer)) {
            return false;
        }
        ReinsuranceReinsurer other = (ReinsuranceReinsurer) object;
        if ((this.reinsuranceReinsurerId == null && other.reinsuranceReinsurerId != null) || (this.reinsuranceReinsurerId != null && !this.reinsuranceReinsurerId.equals(other.reinsuranceReinsurerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ReinsuranceReinsurer[ reinsuranceReinsurerId=" + reinsuranceReinsurerId + " ]";
    }
    
}
