/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "PostingHistory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PostingHistory.findAll", query = "SELECT p FROM PostingHistory p")
    , @NamedQuery(name = "PostingHistory.findByPostingHistoryId", query = "SELECT p FROM PostingHistory p WHERE p.postingHistoryId = :postingHistoryId")
    , @NamedQuery(name = "PostingHistory.findByPostedDate", query = "SELECT p FROM PostingHistory p WHERE p.postedDate = :postedDate")
    , @NamedQuery(name = "PostingHistory.findByEnteredDate", query = "SELECT p FROM PostingHistory p WHERE p.enteredDate = :enteredDate")
    , @NamedQuery(name = "PostingHistory.findByIsReinsuranceCommitted", query = "SELECT p FROM PostingHistory p WHERE p.isReinsuranceCommitted = :isReinsuranceCommitted")
    , @NamedQuery(name = "PostingHistory.findByReinsuranceCommittedDate", query = "SELECT p FROM PostingHistory p WHERE p.reinsuranceCommittedDate = :reinsuranceCommittedDate")
    , @NamedQuery(name = "PostingHistory.findByUpdateUserName", query = "SELECT p FROM PostingHistory p WHERE p.updateUserName = :updateUserName")
    , @NamedQuery(name = "PostingHistory.findByUpdateLast", query = "SELECT p FROM PostingHistory p WHERE p.updateLast = :updateLast")})
public class PostingHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "postingHistoryId")
    private Integer postingHistoryId;
    @Column(name = "postedDate")
    @Temporal(TemporalType.DATE)
    private Date postedDate;
    @Column(name = "enteredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "isReinsuranceCommitted")
    private Boolean isReinsuranceCommitted;
    @Column(name = "reinsuranceCommittedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reinsuranceCommittedDate;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "postingHistoryIdFk")
    private Collection<Ledger> ledgerCollection;
    @OneToMany(mappedBy = "postingHistoryIdFk")
    private Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection;
    @JoinColumn(name = "enteredByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember enteredByIdFk;
    @JoinColumn(name = "reinsuranceCommittedByIdFk", referencedColumnName = "userMemberId")
    @ManyToOne
    private UserMember reinsuranceCommittedByIdFk;
    @OneToMany(mappedBy = "postingHistoryIdFk")
    private Collection<ReinsuranceFee> reinsuranceFeeCollection;

    public PostingHistory() {
    }

    public PostingHistory(Integer postingHistoryId) {
        this.postingHistoryId = postingHistoryId;
    }

    public Integer getPostingHistoryId() {
        return postingHistoryId;
    }

    public void setPostingHistoryId(Integer postingHistoryId) {
        this.postingHistoryId = postingHistoryId;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Boolean getIsReinsuranceCommitted() {
        return isReinsuranceCommitted;
    }

    public void setIsReinsuranceCommitted(Boolean isReinsuranceCommitted) {
        this.isReinsuranceCommitted = isReinsuranceCommitted;
    }

    public Date getReinsuranceCommittedDate() {
        return reinsuranceCommittedDate;
    }

    public void setReinsuranceCommittedDate(Date reinsuranceCommittedDate) {
        this.reinsuranceCommittedDate = reinsuranceCommittedDate;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<Ledger> getLedgerCollection() {
        return ledgerCollection;
    }

    public void setLedgerCollection(Collection<Ledger> ledgerCollection) {
        this.ledgerCollection = ledgerCollection;
    }

    @XmlTransient
    public Collection<ReinsuranceCustodialAccountBalance> getReinsuranceCustodialAccountBalanceCollection() {
        return reinsuranceCustodialAccountBalanceCollection;
    }

    public void setReinsuranceCustodialAccountBalanceCollection(Collection<ReinsuranceCustodialAccountBalance> reinsuranceCustodialAccountBalanceCollection) {
        this.reinsuranceCustodialAccountBalanceCollection = reinsuranceCustodialAccountBalanceCollection;
    }

    public UserMember getEnteredByIdFk() {
        return enteredByIdFk;
    }

    public void setEnteredByIdFk(UserMember enteredByIdFk) {
        this.enteredByIdFk = enteredByIdFk;
    }

    public UserMember getReinsuranceCommittedByIdFk() {
        return reinsuranceCommittedByIdFk;
    }

    public void setReinsuranceCommittedByIdFk(UserMember reinsuranceCommittedByIdFk) {
        this.reinsuranceCommittedByIdFk = reinsuranceCommittedByIdFk;
    }

    @XmlTransient
    public Collection<ReinsuranceFee> getReinsuranceFeeCollection() {
        return reinsuranceFeeCollection;
    }

    public void setReinsuranceFeeCollection(Collection<ReinsuranceFee> reinsuranceFeeCollection) {
        this.reinsuranceFeeCollection = reinsuranceFeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (postingHistoryId != null ? postingHistoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PostingHistory)) {
            return false;
        }
        PostingHistory other = (PostingHistory) object;
        if ((this.postingHistoryId == null && other.postingHistoryId != null) || (this.postingHistoryId != null && !this.postingHistoryId.equals(other.postingHistoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PostingHistory[ postingHistoryId=" + postingHistoryId + " ]";
    }
    
}
