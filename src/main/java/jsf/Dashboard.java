/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.AccountKeeper;
import entity.UserMember;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BubbleChartModel;
import org.primefaces.model.chart.BubbleChartSeries;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Jiepi
 */
@Named(value = "dashboard")
@ApplicationScoped
public class Dashboard implements Serializable {

    /**
     * Creates a new instance of Dashboard
     */
    public Dashboard() {
    }

    private DashboardModel model;

    private static final Logger LOGGER = LogManager.getLogger(Dashboard.class);
    @PersistenceContext // JSF managed bean to inject the EntityManager
    private EntityManager em;

    @Inject
    transient private ClaimMisc claimMisc;
    @Resource
    UserTransaction utx;

    String userShortName; // admin, mlamotta, bvigario, dhannah
    UserMember userMember;
    AccountKeeper ak;
    String LastLogin;
    String helloName;
    Integer numberOfClaims;
    Integer numberOfContracts;
    HashMap<String, Integer> claimhm;
    

    private BubbleChartModel claimChart;
    private BubbleChartModel contractChart;
    
    private BarChartModel claimBarChart;
    private LineChartModel claimLineChart;
    private PieChartModel claimPieChart;
    
    private BarChartModel contractBarChart;
    private LineChartModel contractLineChart;

    public HashMap<String, Integer> getClaimhm() {
        return claimhm;
    }

    public void setClaimhm(HashMap<String, Integer> claimhm) {
        this.claimhm = claimhm;
    }

    public void setClaimPieChart(PieChartModel claimPieChart) {
        this.claimPieChart = claimPieChart;
    }
    
    

    public PieChartModel getClaimPieChart() {
        LOGGER.info("getClaimPieChart");
        return claimPieChart;
    }
    
    public BarChartModel getContractBarChart() {
        return contractBarChart;
    }

    public LineChartModel getContractLineChart() {
        return contractLineChart;
    }

    public LineChartModel getClaimLineChart() {
        return claimLineChart;
    }
    
    

    public BarChartModel getClaimBarChart() {
        return claimBarChart;
    }

    public BubbleChartModel getClaimChart() {
        return claimChart;
    }

    public BubbleChartModel getContractChart() {
        return contractChart;
    }

    public String getLastLogin() {
        return claimMisc.getJavaDate(userMember.getDateLastLogin());
    }

    public UserMember getUserMember() {
        return userMember;
    }

    public void setUserMember(UserMember userMember) {
        this.userMember = userMember;
    }

    public AccountKeeper getAk() {
        return ak;
    }

    public void setAk(AccountKeeper ak) {
        this.ak = ak;
    }

    public DashboardModel getModel() {
        return model;
    }

    public String getUserShortName() {
        return userShortName;
    }

    public String getHelloName() {
        return ak.getAccountKeeperName().split(" ")[0];
    }

    public Integer getNumberOfClaims() {

        return numberOfClaims;
    }

    public Integer getNumberOfContracts() {

        return numberOfContracts;
    }

    public String getDashBoard() {
        return "Dashboard";
    }
    
     @PostConstruct
    public void init() {
        userShortName = claimMisc.getCurrentUser();
        LOGGER.info("userShortName=" + userShortName);
        userMember = claimMisc.getUserMemberByName(userShortName);
        ak = claimMisc.getAccountKeeperById(userMember.getUserMemberId());

        claimhm = claimMisc.getNumberOfClaimsByUserDt(userShortName, claimMisc.convertFromUtilDate(userMember.getDateLastLogin()));
        numberOfClaims = claimMisc.getNumberOfClaimsByUserName(userShortName, claimMisc.convertFromUtilDate(userMember.getDateLastLogin()));
        numberOfContracts = claimMisc.getNumberOfContractsByUserName(userShortName, claimMisc.convertFromUtilDate(userMember.getDateLastLogin()));
        
        createDashboardModels();

        createBubbleCharts();
        
        createAnimatedModels();
        
        createPieCharts();
        
        LOGGER.info("the end of init");

    }

    //
    public void createDashboardModels() {
        model = new DefaultDashboardModel();
        DashboardColumn column1 = new DefaultDashboardColumn();
        DashboardColumn column2 = new DefaultDashboardColumn();

        column1.addWidget("claims");
        column2.addWidget("contracts");

        model.addColumn(column1);
        model.addColumn(column2);
    }

    public void createBubbleCharts() {
        claimChart = claimBubbleModel();
        claimChart.setTitle("Claim Chart");
        claimChart.setZoom(true);
        /*
        Axis cx = claimChart.getAxis(AxisType.X);
        cx.setLabel("Monthes");
        cx.setMin(0);
        cx.setMax(12);
        */
        
        DateAxis axis = new DateAxis("Dates");
        axis.setTickAngle(-50);
        axis.setMax("2017-12-31");
        axis.setTickFormat("%b %#d, %y");
        claimChart.getAxes().put(AxisType.X, axis);

        Axis cy = claimChart.getAxis(AxisType.Y);
        cy.setLabel("Number Of Claims");
        cy.setMin(0);
        cy.setMax(1000);

        
        /*
        contractChart.setShadow(false);
        contractChart.setBubbleGradients(true);
        contractChart.setBubbleAlpha(0.8);
        contractChart.getAxis(AxisType.X).setTickAngle(-50);
*/
        contractChart = contractBubbleModel();
        contractChart.setTitle("Contract Chart");
        Axis ox = contractChart.getAxis(AxisType.X);
        Axis oy = contractChart.getAxis(AxisType.Y);
        ox.setLabel("Monthes");
        oy.setLabel("Number Of Contracts");
        ox.setMin(0);
        ox.setMax(12);
        
        oy.setMin(0);
        oy.setMax(6000);
        //yAxis.setTickAngle(50);
    }

    
    
    private BubbleChartModel claimBubbleModel() {
        BubbleChartModel bcmodel = new BubbleChartModel();

        bcmodel.add(new BubbleChartSeries("2017-04-30", 4, numberOfClaims, numberOfClaims));
        bcmodel.add(new BubbleChartSeries("2017-01-31", 1, 820, 82));
        bcmodel.add(new BubbleChartSeries("2017-02-28", 2, 643, 64));
        bcmodel.add(new BubbleChartSeries("2017-03-31", 3, 842, 84));
      
        return bcmodel;
    }
    
    private BubbleChartModel contractBubbleModel() {
        BubbleChartModel bcmodel = new BubbleChartModel();
        

        bcmodel.add(new BubbleChartSeries("Current", 4, numberOfContracts, numberOfContracts));
        bcmodel.add(new BubbleChartSeries("4112(Jan)", 1, 4112, 41));
        bcmodel.add(new BubbleChartSeries("4407(Feb)", 2, 4407, 44));
        bcmodel.add(new BubbleChartSeries("5458(Mar)", 3, 5458, 54));
       

        return bcmodel;
    }
    
    private void createAnimatedModels() {
        claimBarChart = claimInitBarModel();
        
        claimBarChart.setTitle(claimMisc.getCurrentUser() + "'s Claim Chart");
        claimBarChart.setAnimate(true);
        claimBarChart.setLegendPosition("ne");
        Axis yAxis = claimBarChart.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(claimMisc.getNumberOfClaimsToDate(claimMisc.convertFromUtilDate(userMember.getDateLastLogin())));
        yAxis.setLabel("No Of Claims");
        
        contractBarChart = contractInitBarModel();
        
        contractBarChart.setTitle("Contract Chart");
        contractBarChart.setAnimate(true);
        contractBarChart.setLegendPosition("ne");
        yAxis = contractBarChart.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(claimMisc.getNumberOfContractsToDate(claimMisc.convertFromUtilDate(userMember.getDateLastLogin())));
        yAxis.setLabel("No Of Contracts");
         
        
    }
    
    private BarChartModel claimInitBarModel() {
        BarChartModel bcModel = new BarChartModel();
 
        ChartSeries user = new ChartSeries();
        user.setLabel(userShortName);
        user.set("Since Last Login", numberOfClaims);
        
        ChartSeries totalc = new ChartSeries();
        totalc.setLabel("Total");
        totalc.set("Since Last Login", claimMisc.getNumberOfClaimsToDate(claimMisc.convertFromUtilDate(userMember.getDateLastLogin())));
 
        bcModel.addSeries(user);
        bcModel.addSeries(totalc);
         
        return bcModel;
    }
    
    private BarChartModel contractInitBarModel() {
        BarChartModel bcModel = new BarChartModel();
 
        ChartSeries user = new ChartSeries();
        user.setLabel(userShortName);
        user.set("Since Last Login", numberOfContracts);
        
        ChartSeries totalc = new ChartSeries();
        totalc.setLabel("Total");
        totalc.set("Since Last Login", claimMisc.getNumberOfContractsToDate(claimMisc.convertFromUtilDate(userMember.getDateLastLogin())));
 
        bcModel.addSeries(user);
        bcModel.addSeries(totalc);
         
        return bcModel;
    }
    
    private void createPieCharts() {
        createClaimPieChart();
    }
    
    private void createClaimPieChart() {
        claimPieChart = new PieChartModel();
        
        LOGGER.info("VSC=" + claimhm.get("VSC"));
        LOGGER.info("ANC=" + claimhm.get("ANC"));
        LOGGER.info("PPM=" + claimhm.get("PPM"));
        LOGGER.info("GAP=" + claimhm.get("GAP"));
        
        claimPieChart.set("VSC", claimhm.get("VSC"));
        claimPieChart.set("ANC", claimhm.get("ANC"));
        claimPieChart.set("PPM", claimhm.get("PPM"));
        claimPieChart.set("GAP", Optional.ofNullable(claimhm.get("GAP")).orElse(0));
                
        claimPieChart.setTitle(claimMisc.getCurrentUser() + "'s Pie Chart");
        claimPieChart.setLegendPosition("w");
        LOGGER.info(claimMisc.getCurrentUser());
        
    }
    

}
