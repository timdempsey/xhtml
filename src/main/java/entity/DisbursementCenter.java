/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "DisbursementCenter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisbursementCenter.findAll", query = "SELECT d FROM DisbursementCenter d")
    , @NamedQuery(name = "DisbursementCenter.findByDisbursementCenterId", query = "SELECT d FROM DisbursementCenter d WHERE d.disbursementCenterId = :disbursementCenterId")
    , @NamedQuery(name = "DisbursementCenter.findByDisbursementType", query = "SELECT d FROM DisbursementCenter d WHERE d.disbursementType = :disbursementType")
    , @NamedQuery(name = "DisbursementCenter.findByUpdateUserName", query = "SELECT d FROM DisbursementCenter d WHERE d.updateUserName = :updateUserName")
    , @NamedQuery(name = "DisbursementCenter.findByUpdateLast", query = "SELECT d FROM DisbursementCenter d WHERE d.updateLast = :updateLast")})
public class DisbursementCenter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "disbursementCenterId")
    private Integer disbursementCenterId;
    @Size(max = 50)
    @Column(name = "disbursementType")
    private String disbursementType;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<PlanDetail> planDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<DeductibleDetail> deductibleDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<ProgramDetail> programDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<TermDetail> termDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<Agent> agentCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<AdjustmentDetail> adjustmentDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<RateDetail> rateDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<ProductDetail> productDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<LimitDetail> limitDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<SurchargeDetail> surchargeDetailCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<Disbursement> disbursementCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<DealerGroup> dealerGroupCollection;
    @OneToMany(mappedBy = "disbursementCenterIdFk")
    private Collection<Dealer> dealerCollection;

    public DisbursementCenter() {
    }

    public DisbursementCenter(Integer disbursementCenterId) {
        this.disbursementCenterId = disbursementCenterId;
    }

    public Integer getDisbursementCenterId() {
        return disbursementCenterId;
    }

    public void setDisbursementCenterId(Integer disbursementCenterId) {
        this.disbursementCenterId = disbursementCenterId;
    }

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<PlanDetail> getPlanDetailCollection() {
        return planDetailCollection;
    }

    public void setPlanDetailCollection(Collection<PlanDetail> planDetailCollection) {
        this.planDetailCollection = planDetailCollection;
    }

    @XmlTransient
    public Collection<DeductibleDetail> getDeductibleDetailCollection() {
        return deductibleDetailCollection;
    }

    public void setDeductibleDetailCollection(Collection<DeductibleDetail> deductibleDetailCollection) {
        this.deductibleDetailCollection = deductibleDetailCollection;
    }

    @XmlTransient
    public Collection<ProgramDetail> getProgramDetailCollection() {
        return programDetailCollection;
    }

    public void setProgramDetailCollection(Collection<ProgramDetail> programDetailCollection) {
        this.programDetailCollection = programDetailCollection;
    }

    @XmlTransient
    public Collection<TermDetail> getTermDetailCollection() {
        return termDetailCollection;
    }

    public void setTermDetailCollection(Collection<TermDetail> termDetailCollection) {
        this.termDetailCollection = termDetailCollection;
    }

    @XmlTransient
    public Collection<Agent> getAgentCollection() {
        return agentCollection;
    }

    public void setAgentCollection(Collection<Agent> agentCollection) {
        this.agentCollection = agentCollection;
    }

    @XmlTransient
    public Collection<AdjustmentDetail> getAdjustmentDetailCollection() {
        return adjustmentDetailCollection;
    }

    public void setAdjustmentDetailCollection(Collection<AdjustmentDetail> adjustmentDetailCollection) {
        this.adjustmentDetailCollection = adjustmentDetailCollection;
    }

    @XmlTransient
    public Collection<RateDetail> getRateDetailCollection() {
        return rateDetailCollection;
    }

    public void setRateDetailCollection(Collection<RateDetail> rateDetailCollection) {
        this.rateDetailCollection = rateDetailCollection;
    }

    @XmlTransient
    public Collection<ProductDetail> getProductDetailCollection() {
        return productDetailCollection;
    }

    public void setProductDetailCollection(Collection<ProductDetail> productDetailCollection) {
        this.productDetailCollection = productDetailCollection;
    }

    @XmlTransient
    public Collection<LimitDetail> getLimitDetailCollection() {
        return limitDetailCollection;
    }

    public void setLimitDetailCollection(Collection<LimitDetail> limitDetailCollection) {
        this.limitDetailCollection = limitDetailCollection;
    }

    @XmlTransient
    public Collection<SurchargeDetail> getSurchargeDetailCollection() {
        return surchargeDetailCollection;
    }

    public void setSurchargeDetailCollection(Collection<SurchargeDetail> surchargeDetailCollection) {
        this.surchargeDetailCollection = surchargeDetailCollection;
    }

    @XmlTransient
    public Collection<Disbursement> getDisbursementCollection() {
        return disbursementCollection;
    }

    public void setDisbursementCollection(Collection<Disbursement> disbursementCollection) {
        this.disbursementCollection = disbursementCollection;
    }

    @XmlTransient
    public Collection<DealerGroup> getDealerGroupCollection() {
        return dealerGroupCollection;
    }

    public void setDealerGroupCollection(Collection<DealerGroup> dealerGroupCollection) {
        this.dealerGroupCollection = dealerGroupCollection;
    }

    @XmlTransient
    public Collection<Dealer> getDealerCollection() {
        return dealerCollection;
    }

    public void setDealerCollection(Collection<Dealer> dealerCollection) {
        this.dealerCollection = dealerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementCenterId != null ? disbursementCenterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisbursementCenter)) {
            return false;
        }
        DisbursementCenter other = (DisbursementCenter) object;
        if ((this.disbursementCenterId == null && other.disbursementCenterId != null) || (this.disbursementCenterId != null && !this.disbursementCenterId.equals(other.disbursementCenterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DisbursementCenter[ disbursementCenterId=" + disbursementCenterId + " ]";
    }
    
}
