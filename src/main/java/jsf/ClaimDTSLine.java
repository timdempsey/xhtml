/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Id;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
public class ClaimDTSLine {
    
   private static final Logger LOGGER = LogManager.getLogger(ClaimFirstLine.class);

    /**
     * Creates a new instance of General
     */
    public ClaimDTSLine() {
    }

    

    public ClaimDTSLine(Integer claimDetailId, String complaintReason, String cause, String correction, List<ClaimDTSLabor> claimDTSLabor) {
        this.claimDetailId = claimDetailId;
        this.complaintReason = complaintReason;
        this.cause = cause;
        this.correction = correction;
        this.claimDTSLabor = claimDTSLabor;
    }

    @Id
   @Column(name="claimDetailId", table="ClaimDetail")
    Integer claimDetailId;
    @Column(name="complaintReason", table="ClaimDetailSub")
    String complaintReason;
    @Column(name="cause", table="ClaimDetailSub")
    String cause;
    @Column(name="correction", table="ClaimDetailSub")
    String correction;
    List<ClaimDTSLabor> claimDTSLabor;
    
    public void reset() {
        LOGGER.info("in line reset");
        this.claimDetailId = null;
        this.complaintReason = null;
        this.cause = null;
        this.correction = null;
        this.claimDTSLabor = null;
    }

    public Integer getClaimDetailId() {
        return claimDetailId;
    }

    public void setClaimDetailId(Integer claimDetailId) {
        this.claimDetailId = claimDetailId;
    }
    
    

    public String getComplaintReason() {
        return complaintReason;
    }

    public void setComplaintReason(String complaintReason) {
        this.complaintReason = complaintReason;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getCorrection() {
        return correction;
    }

    public void setCorrection(String correction) {
        this.correction = correction;
    }

    public List<ClaimDTSLabor> getClaimDTSLabor() {
        return claimDTSLabor;
    }

    public void setClaimDTSLabor(List<ClaimDTSLabor> claimDTSLabor) {
        this.claimDTSLabor = claimDTSLabor;
    }
    
    
    
}