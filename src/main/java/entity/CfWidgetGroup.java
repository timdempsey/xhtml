/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "CfWidgetGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfWidgetGroup.findAll", query = "SELECT c FROM CfWidgetGroup c")
    , @NamedQuery(name = "CfWidgetGroup.findByWidgetGroupId", query = "SELECT c FROM CfWidgetGroup c WHERE c.widgetGroupId = :widgetGroupId")
    , @NamedQuery(name = "CfWidgetGroup.findByName", query = "SELECT c FROM CfWidgetGroup c WHERE c.name = :name")
    , @NamedQuery(name = "CfWidgetGroup.findByDescription", query = "SELECT c FROM CfWidgetGroup c WHERE c.description = :description")
    , @NamedQuery(name = "CfWidgetGroup.findByUpdateUserName", query = "SELECT c FROM CfWidgetGroup c WHERE c.updateUserName = :updateUserName")
    , @NamedQuery(name = "CfWidgetGroup.findByUpdateLast", query = "SELECT c FROM CfWidgetGroup c WHERE c.updateLast = :updateLast")})
public class CfWidgetGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "widgetGroupId")
    private Integer widgetGroupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "widgetGroupId")
    private Collection<CfUserWidgetGroupConn> cfUserWidgetGroupConnCollection;
    @OneToMany(mappedBy = "widgetGroupIdFk")
    private Collection<CfSavedWidgetState> cfSavedWidgetStateCollection;

    public CfWidgetGroup() {
    }

    public CfWidgetGroup(Integer widgetGroupId) {
        this.widgetGroupId = widgetGroupId;
    }

    public CfWidgetGroup(Integer widgetGroupId, String name) {
        this.widgetGroupId = widgetGroupId;
        this.name = name;
    }

    public Integer getWidgetGroupId() {
        return widgetGroupId;
    }

    public void setWidgetGroupId(Integer widgetGroupId) {
        this.widgetGroupId = widgetGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    @XmlTransient
    public Collection<CfUserWidgetGroupConn> getCfUserWidgetGroupConnCollection() {
        return cfUserWidgetGroupConnCollection;
    }

    public void setCfUserWidgetGroupConnCollection(Collection<CfUserWidgetGroupConn> cfUserWidgetGroupConnCollection) {
        this.cfUserWidgetGroupConnCollection = cfUserWidgetGroupConnCollection;
    }

    @XmlTransient
    public Collection<CfSavedWidgetState> getCfSavedWidgetStateCollection() {
        return cfSavedWidgetStateCollection;
    }

    public void setCfSavedWidgetStateCollection(Collection<CfSavedWidgetState> cfSavedWidgetStateCollection) {
        this.cfSavedWidgetStateCollection = cfSavedWidgetStateCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (widgetGroupId != null ? widgetGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfWidgetGroup)) {
            return false;
        }
        CfWidgetGroup other = (CfWidgetGroup) object;
        if ((this.widgetGroupId == null && other.widgetGroupId != null) || (this.widgetGroupId != null && !this.widgetGroupId.equals(other.widgetGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CfWidgetGroup[ widgetGroupId=" + widgetGroupId + " ]";
    }
    
}
