/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;

/**
 *
 * @author Jiepi
 */
public class PlanSearch {

    public PlanSearch() {
    }

    public PlanSearch(Integer planId, String planName, String planCode, String planType, String vinClassingTable, String deductibleDescription, Date effectiveDate, Date expireDate, String admimCompany, String program, String insurer, String product, String description, String planSalesGroup, String disbursementType, String minimumTermMonths, String maxFinance, String vehiclePurchaseType, String availableFinanceType) {
        this.planId = planId;
        this.planName = planName;
        this.planCode = planCode;
        this.planType = planType;
        this.vinClassingTable = vinClassingTable;
        this.deductibleDescription = deductibleDescription;
        this.effectiveDate = effectiveDate;
        this.expireDate = expireDate;
        this.admimCompany = admimCompany;
        this.program = program;
        this.insurer = insurer;
        this.product = product;
        this.description = description;
        this.planSalesGroup = planSalesGroup;
        this.disbursementType = disbursementType;
        this.minimumTermMonths = minimumTermMonths;
        this.maxFinance = maxFinance;
        this.vehiclePurchaseType = vehiclePurchaseType;
        this.availableFinanceType = availableFinanceType;
    }

    // keep the above for now, change the following whenever there is new field

    public PlanSearch(Integer planId, String planName, String planCode, String planType, String vinClassingTable, String deductibleDescription, Date effectiveDate, Date expireDate, String admimCompany, String program, String insurer, String product, String description, String planSalesGroup, String disbursementType, String minimumTermMonths, String maxFinance, String vehiclePurchaseType, String availableFinanceType, BigDecimal reimbursementAmount, String ppmSchedule, String startSchedule, String earningMethod, String earningWaitMonths, String defaultDeductible, String limitDeductibletoDefault, Integer waitDays, BigDecimal defaultTransferFee, String planExpireTimeTypeInd, Boolean extendExpirationByWait, Integer cancelFeeDisbursementTypeInd, Integer cancelRequestValidDays, Integer cancelRequestValidMiles, String productName) {
        this.planId = planId;
        this.planName = planName;
        this.planCode = planCode;
        this.planType = planType;
        this.vinClassingTable = vinClassingTable;
        this.deductibleDescription = deductibleDescription;
        this.effectiveDate = effectiveDate;
        this.expireDate = expireDate;
        this.admimCompany = admimCompany;
        this.program = program;
        this.insurer = insurer;
        this.product = product;
        this.description = description;
        this.planSalesGroup = planSalesGroup;
        this.disbursementType = disbursementType;
        this.minimumTermMonths = minimumTermMonths;
        this.maxFinance = maxFinance;
        this.vehiclePurchaseType = vehiclePurchaseType;
        this.availableFinanceType = availableFinanceType;
        this.reimbursementAmount = reimbursementAmount;
        this.ppmSchedule = ppmSchedule;
        this.startSchedule = startSchedule;
        this.earningMethod = earningMethod;
        this.earningWaitMonths = earningWaitMonths;
        this.defaultDeductible = defaultDeductible;
        this.limitDeductibletoDefault = limitDeductibletoDefault;
        this.waitDays = waitDays;
        this.defaultTransferFee = defaultTransferFee;
        this.planExpireTimeTypeInd = planExpireTimeTypeInd;
        this.extendExpirationByWait = extendExpirationByWait;
        this.cancelFeeDisbursementTypeInd = cancelFeeDisbursementTypeInd;
        this.cancelRequestValidDays = cancelRequestValidDays;
        this.cancelRequestValidMiles = cancelRequestValidMiles;
        this.productName = productName;
    }

    
    
    @Id
    @Column(name = "planId", table = "PlanTable")
    private Integer planId;
    @Column(name = "planName", table = "PlanTable")
    private String planName;
    @Column(name = "planCode", table = "PlanTable")
    private String planCode;
    private String planType;
    private String vinClassingTable;
    @Column(name = "description", table = "Deductible")
    private String deductibleDescription;
    @Column(name = "effectiveDate", table = "ProductDetail")
    private Date effectiveDate;
    private Date expireDate;
    private String admimCompany;
    private String program;
    private String insurer;
    private String product;
    private String description;
    private String planSalesGroup;
    private String disbursementType;
    private String minimumTermMonths;
    private String maxFinance;
    private String vehiclePurchaseType;
    private String availableFinanceType;
    private BigDecimal reimbursementAmount;
    private String ppmSchedule;
    private String startSchedule;
    private String earningMethod;
    private String earningWaitMonths;
    private String defaultDeductible;
    private String  limitDeductibletoDefault;
    private Integer waitDays;
    private BigDecimal defaultTransferFee;
    private String planExpireTimeTypeInd;
    private Boolean  extendExpirationByWait;
    private Integer cancelFeeDisbursementTypeInd;
    private Integer cancelRequestValidDays;
    private Integer cancelRequestValidMiles;
    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    

    public Integer getWaitDays() {
        return waitDays;
    }

    public void setWaitDays(Integer waitDays) {
        this.waitDays = waitDays;
    }

    public BigDecimal getDefaultTransferFee() {
        return defaultTransferFee;
    }

    public void setDefaultTransferFee(BigDecimal defaultTransferFee) {
        this.defaultTransferFee = defaultTransferFee;
    }

    public String getPlanExpireTimeTypeInd() {
        return planExpireTimeTypeInd;
    }

    public void setPlanExpireTimeTypeInd(String planExpireTimeTypeInd) {
        this.planExpireTimeTypeInd = planExpireTimeTypeInd;
    }

    public Boolean getExtendExpirationByWait() {
        return extendExpirationByWait;
    }

    public void setExtendExpirationByWait(Boolean extendExpirationByWait) {
        this.extendExpirationByWait = extendExpirationByWait;
    }

    public Integer getCancelFeeDisbursementTypeInd() {
        return cancelFeeDisbursementTypeInd;
    }

    public void setCancelFeeDisbursementTypeInd(Integer cancelFeeDisbursementTypeInd) {
        this.cancelFeeDisbursementTypeInd = cancelFeeDisbursementTypeInd;
    }

    public Integer getCancelRequestValidDays() {
        return cancelRequestValidDays;
    }

    public void setCancelRequestValidDays(Integer cancelRequestValidDays) {
        this.cancelRequestValidDays = cancelRequestValidDays;
    }

    public Integer getCancelRequestValidMiles() {
        return cancelRequestValidMiles;
    }

    public void setCancelRequestValidMiles(Integer cancelRequestValidMiles) {
        this.cancelRequestValidMiles = cancelRequestValidMiles;
    }
    
    

    public BigDecimal getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(BigDecimal reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    public String getPpmSchedule() {
        return ppmSchedule;
    }

    public void setPpmSchedule(String ppmSchedule) {
        this.ppmSchedule = ppmSchedule;
    }

    public String getStartSchedule() {
        return startSchedule;
    }

    public void setStartSchedule(String startSchedule) {
        this.startSchedule = startSchedule;
    }

    public String getEarningMethod() {
        return earningMethod;
    }

    public void setEarningMethod(String earningMethod) {
        this.earningMethod = earningMethod;
    }

    public String getEarningWaitMonths() {
        return earningWaitMonths;
    }

    public void setEarningWaitMonths(String earningWaitMonths) {
        this.earningWaitMonths = earningWaitMonths;
    }

    public String getDefaultDeductible() {
        return defaultDeductible;
    }

    public void setDefaultDeductible(String defaultDeductible) {
        this.defaultDeductible = defaultDeductible;
    }

    public String getLimitDeductibletoDefault() {
        return limitDeductibletoDefault;
    }

    public void setLimitDeductibletoDefault(String limitDeductibletoDefault) {
        this.limitDeductibletoDefault = limitDeductibletoDefault;
    }
    
    

    public String getMinimumTermMonths() {
        return minimumTermMonths;
    }

    public void setMinimumTermMonths(String minimumTermMonths) {
        this.minimumTermMonths = minimumTermMonths;
    }

    public String getMaxFinance() {
        return maxFinance;
    }

    public void setMaxFinance(String maxFinance) {
        this.maxFinance = maxFinance;
    }

    public String getVehiclePurchaseType() {
        return vehiclePurchaseType;
    }

    public void setVehiclePurchaseType(String vehiclePurchaseType) {
        this.vehiclePurchaseType = vehiclePurchaseType;
    }

    public String getAvailableFinanceType() {
        return availableFinanceType;
    }

    public void setAvailableFinanceType(String availableFinanceType) {
        this.availableFinanceType = availableFinanceType;
    }
    
    
   

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }
    
    

    public String getPlanSalesGroup() {
        return planSalesGroup;
    }

    public void setPlanSalesGroup(String planSalesGroup) {
        this.planSalesGroup = planSalesGroup;
    }
    
    
    

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getVinClassingTable() {
        return vinClassingTable;
    }

    public void setVinClassingTable(String vinClassingTable) {
        this.vinClassingTable = vinClassingTable;
    }

    public String getDeductibleDescription() {
        return deductibleDescription;
    }

    public void setDeductibleDescription(String deductibleDescription) {
        this.deductibleDescription = deductibleDescription;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getAdmimCompany() {
        return admimCompany;
    }

    public void setAdmimCompany(String admimCompany) {
        this.admimCompany = admimCompany;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void reset() {
      planId=null;
      planName="";
      planCode="";
      planType="";
      vinClassingTable="";
      deductibleDescription="";
      effectiveDate=null;
      expireDate=null;
      admimCompany="";
      program="";
      insurer="";
      product="";
      description="";
      planSalesGroup="";
      disbursementType="";
       minimumTermMonths="";
     maxFinance="";
     vehiclePurchaseType="";
     availableFinanceType="";
       reimbursementAmount=null;
      ppmSchedule="";
      startSchedule="";
      earningMethod="";
      earningWaitMonths="";
      defaultDeductible="";
      limitDeductibletoDefault="";
       waitDays=0;
      defaultTransferFee=null;
    planExpireTimeTypeInd="";
      extendExpirationByWait=false;
     cancelFeeDisbursementTypeInd=null;
    cancelRequestValidDays=null;
     cancelRequestValidMiles=null;
     productName = "";
}

}
