/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

/**
 *
 * @author Jiepi
 */
public class ContractField {

    public ContractField() {
    }

    public ContractField(Integer contractFieldId, String name, String value) {
        this.contractFieldId = contractFieldId;
        this.name = name;
        this.value = value;
    }
    
    
    
    Integer contractFieldId;
    String name;
    String value;

    public int getContractFieldId() {
        return contractFieldId;
    }

    public void setContractFieldId(int contractFieldId) {
        this.contractFieldId = contractFieldId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public void reset() {
        contractFieldId=null;
        name = "";
        value = "";
    }
    
}
