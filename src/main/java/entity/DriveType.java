/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "driveType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DriveType.findAll", query = "SELECT d FROM DriveType d")
    , @NamedQuery(name = "DriveType.findByDriveTypeId", query = "SELECT d FROM DriveType d WHERE d.driveTypeId = :driveTypeId")
    , @NamedQuery(name = "DriveType.findByName", query = "SELECT d FROM DriveType d WHERE d.name = :name")})
public class DriveType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "driveTypeId")
    private Integer driveTypeId;
    @Size(max = 30)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "driveTypeInd")
    private Collection<ClassItem> classItemCollection;

    public DriveType() {
    }

    public DriveType(Integer driveTypeId) {
        this.driveTypeId = driveTypeId;
    }

    public Integer getDriveTypeId() {
        return driveTypeId;
    }

    public void setDriveTypeId(Integer driveTypeId) {
        this.driveTypeId = driveTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ClassItem> getClassItemCollection() {
        return classItemCollection;
    }

    public void setClassItemCollection(Collection<ClassItem> classItemCollection) {
        this.classItemCollection = classItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (driveTypeId != null ? driveTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DriveType)) {
            return false;
        }
        DriveType other = (DriveType) object;
        if ((this.driveTypeId == null && other.driveTypeId != null) || (this.driveTypeId != null && !this.driveTypeId.equals(other.driveTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DriveType[ driveTypeId=" + driveTypeId + " ]";
    }
    
}
