/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author demps
 */
@Entity
@Table(name = "RerateSessionContract")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RerateSessionContract.findAll", query = "SELECT r FROM RerateSessionContract r")
    , @NamedQuery(name = "RerateSessionContract.findByRerateSessionContractId", query = "SELECT r FROM RerateSessionContract r WHERE r.rerateSessionContractId = :rerateSessionContractId")
    , @NamedQuery(name = "RerateSessionContract.findByRerateStatusInd", query = "SELECT r FROM RerateSessionContract r WHERE r.rerateStatusInd = :rerateStatusInd")
    , @NamedQuery(name = "RerateSessionContract.findByErrorMessage", query = "SELECT r FROM RerateSessionContract r WHERE r.errorMessage = :errorMessage")
    , @NamedQuery(name = "RerateSessionContract.findByUpdateUserName", query = "SELECT r FROM RerateSessionContract r WHERE r.updateUserName = :updateUserName")
    , @NamedQuery(name = "RerateSessionContract.findByUpdateLast", query = "SELECT r FROM RerateSessionContract r WHERE r.updateLast = :updateLast")})
public class RerateSessionContract implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rerateSessionContractId")
    private Integer rerateSessionContractId;
    @Column(name = "rerateStatusInd")
    private Integer rerateStatusInd;
    @Size(max = 2147483647)
    @Column(name = "errorMessage")
    private String errorMessage;
    @Size(max = 30)
    @Column(name = "updateUserName")
    private String updateUserName;
    @Column(name = "updateLast")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateLast;
    @JoinColumn(name = "contractIdFk", referencedColumnName = "contractId")
    @ManyToOne
    private Contracts contractIdFk;
    @JoinColumn(name = "rerateSessionIdFk", referencedColumnName = "rerateSessionId")
    @ManyToOne
    private RerateSession rerateSessionIdFk;

    public RerateSessionContract() {
    }

    public RerateSessionContract(Integer rerateSessionContractId) {
        this.rerateSessionContractId = rerateSessionContractId;
    }

    public Integer getRerateSessionContractId() {
        return rerateSessionContractId;
    }

    public void setRerateSessionContractId(Integer rerateSessionContractId) {
        this.rerateSessionContractId = rerateSessionContractId;
    }

    public Integer getRerateStatusInd() {
        return rerateStatusInd;
    }

    public void setRerateStatusInd(Integer rerateStatusInd) {
        this.rerateStatusInd = rerateStatusInd;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getUpdateLast() {
        return updateLast;
    }

    public void setUpdateLast(Date updateLast) {
        this.updateLast = updateLast;
    }

    public Contracts getContractIdFk() {
        return contractIdFk;
    }

    public void setContractIdFk(Contracts contractIdFk) {
        this.contractIdFk = contractIdFk;
    }

    public RerateSession getRerateSessionIdFk() {
        return rerateSessionIdFk;
    }

    public void setRerateSessionIdFk(RerateSession rerateSessionIdFk) {
        this.rerateSessionIdFk = rerateSessionIdFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rerateSessionContractId != null ? rerateSessionContractId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RerateSessionContract)) {
            return false;
        }
        RerateSessionContract other = (RerateSessionContract) object;
        if ((this.rerateSessionContractId == null && other.rerateSessionContractId != null) || (this.rerateSessionContractId != null && !this.rerateSessionContractId.equals(other.rerateSessionContractId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RerateSessionContract[ rerateSessionContractId=" + rerateSessionContractId + " ]";
    }
    
}
