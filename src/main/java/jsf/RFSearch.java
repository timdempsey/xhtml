/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */

public class RFSearch implements Serializable {
    private static final Logger LOGGER = LogManager.getLogger(RFSearch.class);

    /**
     * Creates a new instance of General
     */
    public RFSearch() {}

    public RFSearch(Integer accountKeeperId, String rfName, String rfPhone, String city, String state, String zip, BigDecimal laborTax, BigDecimal partTax, BigDecimal laborRate, String address1, String address2) {
        this.accountKeeperId = accountKeeperId;
        this.rfName = rfName;
        this.rfPhone = rfPhone;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.laborTax = laborTax;
        this.partTax = partTax;
        this.laborRate = laborRate;
        this.address1 = address1;
        this.address2 = address2;
    }

   

   
    
    
    
    @Id
    @Column(name = "accountKeeperId", table = "AccountKeeper")
    Integer accountKeeperId;
    @Column(name = "accountKeeperName", table = "AccountKeeper")
    String rfName;
    @Column(name = "phoneNumber", table = "Address")
    String rfPhone;
    @Column(name = "city", table = "Address")
    String city;
    @Column(name = "regionName", table = "Region")
    String state;
    @Column(name = "zipCode", table = "Address")
    String zip;
    @Column(name = "laborTaxPct", table = "RepairFacility")
    BigDecimal laborTax;
    @Column(name = "partsTaxPct", table = "RepairFacility")
    BigDecimal partTax;
    @Column(name = "laborRate", table = "RepairFacility")
    BigDecimal laborRate;
     @Column(name = "address1", table="Address")
    private String address1;
    @Column(name = "address2", table="Address")
    private String address2;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    
    public Integer getAccountKeeperId() {
        return accountKeeperId;
    }

    public void setAccountKeeperId(Integer accountKeeperId) {
        this.accountKeeperId = accountKeeperId;
    }

    public String getRfName() {
        return rfName;
    }

    public void setRfName(String rfName) {
        this.rfName = rfName;
    }

    public String getRfPhone() {
        return rfPhone;
    }

    public void setRfPhone(String rfPhone) {
        this.rfPhone = rfPhone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public BigDecimal getLaborTax() {
        return laborTax;
    }

    public void setLaborTax(BigDecimal laborTax) {
        this.laborTax = laborTax;
    }

    public BigDecimal getPartTax() {
        return partTax;
    }

    public void setPartTax(BigDecimal partTax) {
        this.partTax = partTax;
    }

    public BigDecimal getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(BigDecimal laborRate) {
        this.laborRate = laborRate;
    }

    
    
}
