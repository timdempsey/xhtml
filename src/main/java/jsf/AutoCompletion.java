/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jiepi
 */
@Named(value = "autoCompletion")
@ApplicationScoped
public class AutoCompletion implements Serializable {
    
    private static final Logger LOGGER = LogManager.getLogger(AutoCompletion.class);

    @PersistenceContext // JSF m
    private EntityManager em;

    /**
     * Creates a new instance of AutoCompletion
     */
    public AutoCompletion() {
    }
    
     @Inject
    private ClaimMisc claimMisc;

     /**
      * 
      * @param s
      * @return 
      */
    public List<String> completeDealerName(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select a.accountKeeperName from AccountKeeper a, Dealer b where a.accountKeeperId=b.dealerId and a.accountKeeperName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
    
    public List<String> completeDealerNumber(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select b.dealerNumber from AccountKeeper a, Dealer b where a.accountKeeperId=b.dealerId and b.dealerNumber like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
    
    public List<String> completeAccountKeeperReceivables(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select concat(a.accountKeeperName, '(', b.AccountKeeperTypeDesc, ')') from AccountKeeper a, accountKeeperType b where accountKeeperTypeIdFk=b.AccountKeeperTypeId and accountKeeperTypeIdFk in  ( 1, 3, 5, 7, 8, 11 ) and a.accountKeeperName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
    
    
    public List<String> completeVin(String vin) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select vinFull from Contracts where vinFull like ?1");
        results = (List<String>) query.setParameter(1, "%" + vin ).getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
    
    public List<String> completeAssignedTo(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select userName from UserMember where userName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
    
     public List<String> completeClaimNumber(String claimNo) {
        List<String> results = new ArrayList<>();
        

        LOGGER.info("select c.claimNumber from Claim c where c.claimNumber like ...");
        //TypedQuery<Claim> query = em.createQuery("select c from Claim c where c.claimNumber like :no", Claim.class);
        Query query = em.createNativeQuery("select claimNumber from Claim a where a.claimNumber like ?1");
        results = (List<String>) query.setParameter(1, claimNo + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
     
     public List<String> completeContractNo(String s) {
        List<String> results = new ArrayList<>();
        
        Query query = em.createNativeQuery("select contractNo from Contracts where contractNo like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
     
     public List<String> completePartNumber(String s) {
        List<String> results = new ArrayList<>();
        
        Query query = em.createNativeQuery("select partNumber from ClaimPart where partNumber like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
     
     public List<String> completePartDesc(String s) {
        List<String> results = new ArrayList<>();
        
        Query query = em.createNativeQuery("select partDesc from ClaimPart where partDesc like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
     
     public List<String> completeAccountKeeperName(String s) {
        List<String> results = new ArrayList<>();
        
        Query query = em.createNativeQuery("select accountKeeperName from AccountKeeper where accountKeeperName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
     
     public List<String> completeAgentName(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select b.accountKeeperName from Agent a, AccountKeeper b where a.agentId=b.accountKeeperId and b.accountKeeperName  like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        return results;
    }
     
     public List<String> completePlanName(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select planName from PlanTable where planName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
     
     public List<String> completeProductName(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select productName from Product where productName like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
     
      public List<String> completeTransactionNumber(String s) {
        List<String> results = new ArrayList<>();

        Query query = em.createNativeQuery("select transactionNumber from CashTransaction where transactionNumber like ?1");
        results = (List<String>) query.setParameter(1, s + "%").getResultList();

        LOGGER.info("results.size====" + results.size());
        return results;
    }
    
}
